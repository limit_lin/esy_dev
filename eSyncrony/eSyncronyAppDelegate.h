
#import <UIKit/UIKit.h>

@class eSyncronyViewController;
@class mainMenuViewController;
//#ifdef OFFLINE_TEST_MODE
//#endif

@interface eSyncronyAppDelegate : UIResponder <UIApplicationDelegate>
{
    ;
}

@property (strong, nonatomic) UIWindow *window;

@property(nonatomic, copy) void (^dispatchHandler)(GAIDispatchResult result);

@property (strong, nonatomic) eSyncronyViewController *viewController;
@property (strong, nonatomic) mainMenuViewController *mainMenuViewCtrl;

@property (strong, nonatomic) NSDictionary *refererAppLink;

@property (nonatomic, strong) NSString  *strLoginEmail;
@property (nonatomic, strong) NSString  *strLoginPassword;

@property (nonatomic, strong) NSString  *strFaceBookID;

@property (nonatomic, strong) NSString  *strAccNo;
@property (nonatomic, strong) NSString  *strTokenKey;

//verify Nric,uploadPhoto
@property (strong, nonatomic) NSString* isNoNric;
@property (strong, nonatomic) NSString* isNoPhoto;

@property (nonatomic, strong) NSString  *strHandphone;
@property (nonatomic, strong) NSString  *strName;
@property (nonatomic, strong) NSString  *strNric;
@property (nonatomic, strong) NSDate    *birthDay;
@property (nonatomic, strong) NSString  *membership;
@property (nonatomic, strong) NSString  *paying;//approve match, append change 0318,treat it as global
@property (nonatomic, strong) NSString  *language;//International Treatment
@property (nonatomic, assign) BOOL bVersion;//version reminder
@property (nonatomic, assign) int versionShow;
@property (nonatomic, assign) int show;//renewal & matchtips
@property (nonatomic, assign) int declineTotal;//decline
@property (nonatomic, assign) int years;//register user years
@property (nonatomic, strong) NSString *Tokens;
@property (nonatomic, assign) int   CountryCode;
@property (nonatomic, assign) int   gender;
@property (nonatomic, assign) int   countryId;
@property (nonatomic, assign) BOOL  bIsLoggedIn;
@property (nonatomic, assign) int   nIDVerify;
@property (nonatomic, assign) int   payment;

//vip
@property (nonatomic, strong) NSString  *isVip;
@property (nonatomic, strong) NSString  *checkVipStroy;

//match list filtering
@property (nonatomic, strong) NSString  *strAge;
@property (nonatomic, strong) NSString  *strHeight;
@property (nonatomic, strong) NSString  *strEducation;
@property (nonatomic, strong) NSString  *strMstatus;
@property (nonatomic, strong) NSNumber  *strJobtitle;
@property (nonatomic, strong) NSString  *strIncome;
@property (nonatomic, strong) NSString  *strReligion;
@property (nonatomic, strong) NSString  *strEthnicity;

//socket.io
@property (nonatomic, strong) NSMutableDictionary *dicProfileMatch;//匹配到的用户个人信息

//@property (nonatomic,strong)SVMessageModel *conversationMessageModel;//具体的聊天记录

+ (eSyncronyAppDelegate*)sharedInstance;




- (void)logOut;
- (void)logoutFB;
- (void)saveLoginInfo;
- (void)saveBasicInfoUser;
- (void)loadLoginInfo;

@end

eSyncronyAppDelegate *appDelegate(void);

//
//  VIPStoryStepViewController.m
//  eSyncrony
//
//  Created by ESYNSZ-Limit on 16/2/15.
//  Copyright © 2016年 WonMH. All rights reserved.
//
#import "UtilComm.h"
#import "Global.h"
#import "DSActivityView.h"
#import "eSyncronyAppDelegate.h"
#import "eSyncronyViewController.h"
#import "VIPStoryStepViewController.h"

@interface VIPStoryStepViewController ()

@end

@implementation VIPStoryStepViewController
@synthesize parentView;
@synthesize step;
@synthesize arrStoryContents;

- (void)viewDidLoad {
    [super viewDidLoad];

    self.screenName  = @"VIPMyStoryStepsView";
    if( arrStringForQuestion == nil)
    {
        arrStringForQuestion = [[NSMutableArray alloc] init];
        [arrStringForQuestion addObject:NSLocalizedString(@"What is a successful relationship mean to you?",@"MyVIPStorySteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"What do you think is important that makes a relationship works?",@"MyVIPStorySteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"My ideal relationship is...",@"MyVIPStorySteps")];
    }
    if (arrSubViews == nil) {
        arrSubViews = [[NSMutableArray alloc] init];
        [arrSubViews addObject:subView];
        [arrSubViews addObject:subView];
        [arrSubViews addObject:subView];
    }

    NSString* language = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    if ([language hasPrefix:@"zh-Hant"]||[language hasPrefix:@"zh-HK"]) {//chinese zh-Hant
        
        colorRange[0] = NSMakeRange(6, 2);
        colorRange[1] = NSMakeRange(5, 2);
        colorRange[2] = NSMakeRange(2, 2);
        
    }else//en,not have id
    {
        colorRange[0] = NSMakeRange(10, 10);
        colorRange[1] = NSMakeRange(21, 9);
        colorRange[2] = NSMakeRange(3, 5);
    }
  
    if( self.step == 1 )
    {
        arrStoryContents = [[NSMutableArray alloc] initWithCapacity:0];
        for( int i = 0; i < 3; i++ )
        {
            NSArray*    storyItem = [NSArray arrayWithObjects:@"", @"", @"", nil];
            [arrStoryContents addObject:storyItem];
        }
        [btnBack setHidden:YES];
    }
    else
        [btnBack setHidden:NO];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnTableView:)];
    tapRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapRecognizer];
    
    [self setContentsToStep];
}

- (void)tapOnTableView:(UITapGestureRecognizer *)gesture
{
    [self.view endEditing:YES];
    [self restoreViewPosition];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    subView.frame = scrView.bounds;
}

- (BOOL)checkValidInput
{
    if( txtContent.text.length == 0)
        return NO;
        
    NSArray*    storyItem = [NSArray arrayWithObjects:txtContent.text, nil];
    [arrStoryContents setObject:storyItem atIndexedSubscript:step-1];
    
    return YES;
}

- (void)saveStepInfo
{
    NSArray*    storyItem = [NSArray arrayWithObjects:txtContent.text, nil];
    [arrStoryContents setObject:storyItem atIndexedSubscript:step-1];
}

- (void)initTitle
{
    NSString* title = [arrStringForQuestion objectAtIndex:(step - 1)];
    
    NSMutableAttributedString* string = [[NSMutableAttributedString alloc]initWithString:title];
    
    UIColor*    color = TITLE_REG_COLOR;
    
    [string addAttribute:NSForegroundColorAttributeName value:color range:colorRange[step-1]];

    [txtQuestionTitle setAttributedText:string];

}

- (void)restoreStoryContents
{
    NSArray*    storyItem = [arrStoryContents objectAtIndex:step-1];
    txtContent.text = [storyItem objectAtIndex:0];
}

- (void)setContentsToStep
{
    // removes before views
    UIView* tmpView = [arrSubViews objectAtIndex:step-1];
    [scrView addSubview:tmpView];
    scrView.contentSize = tmpView.frame.size;
    
    [self initTitle];
    [self restoreStoryContents];
}

- (IBAction)didClickBtnBack:(id)sender {
    
    [self saveStepInfo];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)VIPStory_saveStoryInfoProc
{
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    
    NSString    *story[3];
    
    for( int i = 0; i < 3; i++ )
    {
        story[i] = @"";
        
        NSArray*    storyItem = [arrStoryContents objectAtIndex:i];

        story[i] = [storyItem objectAtIndex:0];

    }
    
    [params setObject:story[0] forKey:@"story4"];
    [params setObject:story[1] forKey:@"story5"];
    [params setObject:story[2] forKey:@"story6"];
    
    [params setObject:@"setMyVipStory" forKey:@"cmd"];
    NSDictionary*    result = [UtilComm saveVipStoryInfo:params];
    [DSBezelActivityView removeViewAnimated:NO];
    
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Saving Failure"];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( response == nil )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Saving Failure",)];
        return;
    }
    
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [ErrorProc alertSetMyStoryError:response];
    }
    else
    {
        [eSyncronyAppDelegate sharedInstance].checkVipStroy = @"true";
        [[eSyncronyAppDelegate sharedInstance] saveLoginInfo];
        
        [self moveNextWindow];
    }
}

-(void)moveNextWindow
{
    if (_ismemberLogin) {
        [parentView enterDocCertStep];
    }else
        [parentView enterMainWindow];//avoid logic step in register quiz
}

- (void)saveVIPStoryStepInfoAndMoveNext
{
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Saving...",)];
    [self performSelector:@selector(VIPStory_saveStoryInfoProc) withObject:nil afterDelay:0.01];
}

- (IBAction)didClickBtnNext:(id)sender {
    
    if( [self checkValidInput] == NO )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Please enter your VIP story",@"MyVIPStorySteps") withTitle:NSLocalizedString(@"Field Required",)];
        return;
    }
    
    if( step == 3 )
    {
        [self saveVIPStoryStepInfoAndMoveNext];
        return;
    }
    
    if( self.nextView == nil )
        self.nextView = [[VIPStoryStepViewController alloc] initWithNibName:@"VIPStoryStepViewController" bundle:nil];
    
    self.nextView.parentView = self.parentView;
    self.nextView.step = self.step+1;
    self.nextView.arrStoryContents = self.arrStoryContents;
    
    [self.navigationController pushViewController:self.nextView animated:YES];

}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    CGRect rect = [textView convertRect:textView.bounds toView:self.view];
    
    float   offset;
    
    offset = self.view.frame.size.height - (rect.size.height + 226);
    offset = offset - rect.origin.y;
    
    if( offset > 0 )
        return;
    
    rect = self.view.frame;
    rect.origin.y = offset;
    
    self.view.frame = rect;
}

//-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
//{
//    return YES;
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)dealloc {
    [scrView release];
    [subView release];
    [txtQuestionTitle release];
    [txtContent release];
    [btnBack release];
    
    [arrStringForQuestion release];
    [arrSubViews release];
    
    if( self.step == 1 )
        [arrStoryContents release];
    
    [self.nextView release];
    
    [super dealloc];
}

@end

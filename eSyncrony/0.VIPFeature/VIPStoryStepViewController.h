//
//  VIPStoryStepViewController.h
//  eSyncrony
//
//  Created by ESYNSZ-Limit on 16/2/15.
//  Copyright © 2016年 WonMH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InputBaseViewController.h"

@class eSyncronyViewController;

@interface VIPStoryStepViewController : InputBaseViewController<UITextViewDelegate>
{
    
    IBOutlet UIScrollView *scrView;

    IBOutlet UIButton *btnBack;
    IBOutlet UIView *subView;
    IBOutlet UILabel *txtQuestionTitle;
    
    IBOutlet UITextView *txtContent;
    
    
    NSMutableArray*             arrStringForQuestion;
    NSMutableArray*             arrSubViews;
    
    NSRange     colorRange[3];
    
}
@property (nonatomic, assign) int step;
@property (nonatomic, assign) BOOL ismemberLogin;
@property (nonatomic, assign) NSMutableArray*             arrStoryContents;
@property (nonatomic, assign) VIPStoryStepViewController* nextView;
@property (nonatomic, assign) eSyncronyViewController* parentView;

- (IBAction)didClickBtnBack:(id)sender;
- (IBAction)didClickBtnNext:(id)sender;

@end

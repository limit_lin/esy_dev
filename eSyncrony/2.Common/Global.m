
#import "Global.h"
#import "eSyncronyAppDelegate.h"

@implementation Global

Global* _sharedGlobalInstance = nil;

+ (id)sharedGlobal
{
    if( _sharedGlobalInstance == nil )
        _sharedGlobalInstance = [[Global alloc] init];

    return _sharedGlobalInstance;
}

- (id)init
{
    self = [super init];
    
    _cacheInfos = [[NSMutableDictionary alloc] initWithCapacity:0];
    
    return self;
}

+ (NSString*)getCurDateString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSString*   curDateString = [dateFormatter stringFromDate:[NSDate date]];
    
    [dateFormatter release];
    
    return curDateString;
}

+ (NSString*)getCurDateTimeString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm"];//not 24
    
    NSString*   curDateString = [dateFormatter stringFromDate:[NSDate date]];

    [dateFormatter release];
    
    return curDateString;
}

+ (NSString*)getCurMonthDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMMM dd, yyyy"];
    
    NSString*   curDateString = [dateFormatter stringFromDate:[NSDate date]];
    
    [dateFormatter release];
    
    return curDateString;
}

+ (NSString*)getYearMonthDateFrom:(NSDate*)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];

    NSString*   curDateString = [dateFormatter stringFromDate:date];
    
    [dateFormatter release];
    
    return curDateString;
}

+(NSString *)getCurrentDate{
    
    NSString *returnValue = @"";
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd"];
    returnValue = [df stringFromDate:[NSDate date]];
    NSDateComponents    *c = [[NSCalendar currentCalendar] components:NSCalendarUnitHour|NSCalendarUnitMinute fromDate:[NSDate date]];
    if( c.hour == 12 )
        returnValue = [returnValue stringByAppendingFormat:@" 12:%02d PM", (int)(c.minute)];
    else if( c.hour > 12 )
        returnValue = [returnValue stringByAppendingFormat:@" %02d:%02d PM", (int)(c.hour-12), (int)(c.minute)];
    else
        returnValue = [returnValue stringByAppendingFormat:@" %02d:%02d AM", (int)(c.hour), (int)(c.minute)];
    
    [df release];
    return returnValue;
}

+ (NSString*)getDateTime:(NSDate*)date withFormat:(NSString*)format
{
    NSString *returnValue = @"";
    
    if( [format isEqualToString:@"dd MMM yyyy hh:mm a"] )//english format
    {

        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"dd MMM yyyy"];
        
        returnValue = [df stringFromDate:date];
        
        NSDateComponents    *c = [[NSCalendar currentCalendar] components:NSCalendarUnitHour|NSCalendarUnitMinute fromDate:date];
        if( c.hour == 12 )
            returnValue = [returnValue stringByAppendingFormat:@" 12:%02d PM", (int)(c.minute)];
        else if( c.hour > 12 )
            returnValue = [returnValue stringByAppendingFormat:@" %02d:%02d PM", (int)(c.hour-12), (int)(c.minute)];
        else
            returnValue = [returnValue stringByAppendingFormat:@" %02d:%02d AM", (int)(c.hour), (int)(c.minute)];
        
        [df release];
    }
    else
    {
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:format];

        returnValue = [df stringFromDate:date];
    
        [df release];
    }
    
    return returnValue;
}

+ (NSDate*)convertStringToDate:(NSString*)strDate withFormat:(NSString*)format
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:format];
    
    NSDate *date = [df dateFromString:strDate];
    
    [df release];
    
    return date;
}

#pragma mark - Null or nil
//判断数据是否是Null或者nil
+ (BOOL)isNullOrNilObject:(id)object
{
    if ([object isKindOfClass:[NSNull class]]) {
        return YES;
    }else{
        NSString *str = [NSString stringWithFormat:@"%@",object];
        if ([str isEqualToString:@"nil"]) {
            return YES;
        }
        if ([str isEqualToString:@"Nil"]) {
            return YES;
        }
        if ([str isEqualToString:@"null"]) {
            return YES;
        }
        if ([str isEqualToString:@"NULL"]){
            return YES;
        }
    }
    
    if (object == Nil) {
        return YES;
    }
    
    if (object == nil) {
        return YES;
    }
    return NO;
}


//====================================================================================================//
+(NSArray*)loadHearUsFrom:(NSString*)fileName
{
    
    /*
     NSDictionary*   result = [UtilComm getProfileWithType:@"hearusfrom"];
     if( result == nil )
     return nil;
     
     NSDictionary*   response = [result objectForKey:@"response"];
     NSDictionary*   item = [response objectForKey:@"item"];
     if (item == nil)
     return nil;
     NSArray*        arrQuestion = [item objectForKey:@"hearusfrom"];
     return arrQuestion;
     */
    NSString*       filePath =[[NSBundle mainBundle] pathForResource:fileName ofType:@"plist"];
    NSDictionary*   dicArray = [NSDictionary dictionaryWithContentsOfFile:filePath];
    NSArray*        arrQuestion = [dicArray objectForKey:@"hearusfrom"];
    
    return arrQuestion;
}


+ (NSArray*)loadQuestionArray:(NSString*)fileName
{
    
    NSString*       filePath =[[NSBundle mainBundle] pathForResource:fileName ofType:@"plist"];
    NSDictionary*   dicArray = [NSDictionary dictionaryWithContentsOfFile:filePath];
    
    NSArray*        arrQuestion = [dicArray objectForKey:@"Array"];
    
    //NSLog(@"%@ %@ %@",filePath,dicArray,arrQuestion);

    return arrQuestion;
}

+ (void)loadBasicProfileInfoAndSaveToClient
{
    // check with saved one
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSString*       savedEmail = [defaults objectForKey:@"basicInfoSavedEmail"];
    if( [savedEmail isEqualToString:[eSyncronyAppDelegate sharedInstance].strLoginEmail] )
        return;

    NSDictionary*   result = [UtilComm getProfileWithType:@"basic"];
    if( result == nil )
        return;
    
    NSDictionary*   response = [result objectForKey:@"response"];
    NSDictionary*   basic = [response objectForKey:@"basic"];
    if( basic == nil ||[basic isEqual:@""])
        return;
    
    NSNumber*   cty_id = [basic objectForKey:@"cty_id"];
    NSString*   gender = [basic objectForKey:@"gender"];
    NSString*   nname = [basic objectForKey:@"nname"];
    
    [eSyncronyAppDelegate sharedInstance].strName = nname;
    [eSyncronyAppDelegate sharedInstance].countryId = (int)[cty_id integerValue] - 1;
    [eSyncronyAppDelegate sharedInstance].strNric = [basic objectForKey:@"nric"];
    
    if( [gender isEqualToString:@"M"] )
        [eSyncronyAppDelegate sharedInstance].gender = 0;
    else
        [eSyncronyAppDelegate sharedInstance].gender = 1;
    
    [[eSyncronyAppDelegate sharedInstance] saveLoginInfo];
    [[eSyncronyAppDelegate sharedInstance] saveBasicInfoUser];
}

//====================================================================================================//
//====================================================================================================//
//====================================================================================================//

- (id)getCacheInfoForKey:(NSString*)key
{
    return [_cacheInfos objectForKey:key];
}

- (void)setCacheInfo:(id)info forKey:(NSString*)key
{
    return [_cacheInfos setObject:info forKey:key];
}

@end

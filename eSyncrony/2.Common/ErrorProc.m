
#import "ErrorProc.h"
#import "eSyncronyAppDelegate.h"

@implementation ErrorProc

+ (void)alertMessage:(NSString*)errorMessage withTitle:(NSString*)title
{

//    if( [errorMessage isEqualToString:ERROR_Unknown] )
//    {
//        int kkk = 0;
//        kkk = 1;
//    }
//    NSLog(@"errorMessage =%@\n,title = %@",errorMessage,title);
    
    if ([errorMessage isEqualToString:ERROR_Not_Login]||[errorMessage hasPrefix:NSLocalizedString(@"Session Expired",)]||[errorMessage hasPrefix:NSLocalizedString(@"We have encountered",)]) {
        
        if ([eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl!=nil) {
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:title
                                                         message:errorMessage
                                                        delegate:[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl
                                               cancelButtonTitle:NSLocalizedString(@"OK",@"Error")
                                               otherButtonTitles:nil, nil];
            [av show];
            [av release];
            return;
        
        }else{
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:title
                                                         message:errorMessage
                                                        delegate:[eSyncronyAppDelegate sharedInstance].viewController
                                               cancelButtonTitle:NSLocalizedString(@"OK",@"Error")
                                               otherButtonTitles:nil, nil];
            

            
            [av show];
            [av release];
            return;
        }
        
    }else{
        
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:title
                                                     message:errorMessage
                                                    delegate:nil
                                           cancelButtonTitle:NSLocalizedString(@"OK",@"Error")
                                           otherButtonTitles:nil, nil];
        dispatch_async(dispatch_get_main_queue(), ^{//append 0701
            //Show alert here
            [av show];
            [av release];
            return;
            
        });
    }
    
}

+ (void)alertAuthenFailureMessage
{
    [ErrorProc alertMessage:ERROR_Internet withTitle:ERROR_Authentication_Failure];
}

+ (void)alertToCheckInternetStateTitle:(NSString*)title
{
    [self alertMessage:ERROR_Internet withTitle:title];
}

+ (void)alertRegisterError:(NSString *)error
{
    NSString*   strError = [error substringFromIndex:6];
    NSString*   errorContent = ERROR_Unknown;

    switch ( [strError intValue] )
    {
        case 0:
            errorContent = ERROR_EMPTY_STRING;
            break;
        case 1:
            errorContent = ERROR_Not_Login;
            break;
        case 2:
            errorContent = ERROR_Session_Expired;
            break;
        case 3:
            errorContent = NSLocalizedString(@"Invalid email format or failed domain name validation." ,@"Register Error");
            break;
        case 4:
            errorContent = NSLocalizedString(@"Invalid gender input.",@"Register Error");
            break;
        case 5:
            errorContent = NSLocalizedString(@"Name length needs to be at least 4 chars.",@"Register Error");
            break;
        case 6:
            errorContent = NSLocalizedString(@"Password length needs to be at least 6 chars.",@"Register Error");
            break;
        case 7:
            errorContent = NSLocalizedString(@"Invalid cty_id format",@"Register Error");
            break;
        case 8:
            errorContent = NSLocalizedString(@"Eamil address is already registered",@"Register Error");
            break;
        case 9:
            errorContent = NSLocalizedString(@"Invalid email format or failed domain name validation." ,@"Register Error");;
            break;
        case 10:
            errorContent = NSLocalizedString(@"Invalid gender input.",@"Register Error");
            break;
        case 100:
            errorContent = NSLocalizedString(@"Facebook id already exists",@"Register Error");
            break;
        default:
            break;
    }
    
    [self alertMessage:errorContent withTitle:NSLocalizedString(@"Register Failure",@"Register Error")];
}

+ (void)alertLoginError:(NSString *)error
{
    NSString*   strError = [error substringFromIndex:6];
    NSLog(@"strError == %@",strError);
    NSString*   errorContent = ERROR_Unknown;
    if ([error isEqualToString:@"Not Registered"]) {
        errorContent = ERROR_Account_Not_Exist;
    }
    if ([error isEqualToString:@"Facebook Not Registered"]) {
        errorContent = ERROR_Account_Not_Exist;
    }
    switch ( [strError intValue] )
    {
        case 0:
            errorContent = ERROR_EMPTY_STRING;
            //errorContent = @"Email address or password is empty.";
            break;
        case 7:
            //errorContent = @"account not found(user not registered)";
            errorContent = ERROR_Account_Not_Exist;
            break;
        case 8:
            //errorContent = @"wrong password.";
//            errorContent = NSLocalizedString(@"Invalid Password.",);
            errorContent = NSLocalizedString(@"Your email address or password is incorrect,please re-enter again.If you have forgotten your password,please click 'Forgot Password?' link below.",);
            
            break;
        case 9:
            //errorContent = @"account not found(user not registered)";
            errorContent = ERROR_Account_Not_Exist;
            break;
    }

    [self alertMessage:errorContent withTitle:ERROR_Authentication_Failure];
}

+ (void)alertRetrieveStepError:(NSString *)error
{
    NSString*   strError = [error substringFromIndex:6];
    NSString*   errorContent = ERROR_Unknown;
    
    switch ( [strError intValue] )
    {
        case 0:
            errorContent = ERROR_EMPTY_STRING;
            break;
        case 1:
            errorContent = ERROR_Not_Login;
            break;
        case 2:
            errorContent = ERROR_Session_Expired;
            break;
        case 3:
            errorContent = ERROR_NEED_RELOGIN;
            break;
    }

    [self alertMessage:errorContent withTitle:ERROR_Authentication_Failure];
}

+ (void)alertSetBasicError:(NSString *)error
{
    NSString*   strError = [error substringFromIndex:6];
    NSString*   errorContent = ERROR_Unknown;

    switch ( [strError intValue] )
    {
        case 0:
            errorContent = ERROR_EMPTY_STRING;
            break;
        case 1:
            errorContent = ERROR_Not_Login;
            break;
        case 2:
            errorContent = ERROR_Session_Expired;
            break;
        case 3:
            errorContent = ERROR_NEED_RELOGIN;
            break;
        case 4:
            errorContent = NSLocalizedString(@"invalid value for gender",);
            break;
        case 5:
            errorContent = NSLocalizedString(@"mstatus should be numeric",);
            break;
        case 6:
            errorContent = NSLocalizedString(@"invalid value for mstatus",);
            break;
        case 7:
            errorContent = NSLocalizedString(@"invalid value for ethnicity",);
            break;
        case 8:
            errorContent = NSLocalizedString(@"nationality should be numberic",);
            break;
        case 9:
            errorContent = NSLocalizedString(@"invalid value for nationality",);
            break;
        case 10:
            errorContent = NSLocalizedString(@"education should be numeric",);
            break;
        case 11:
            errorContent = NSLocalizedString(@"invalid value for education",);
            break;
        case 12:
            errorContent = NSLocalizedString(@"DB - insert operation failed",);
            break;
    }
    
    [self alertMessage:errorContent withTitle:NSLocalizedString(@"setBasic Failure",)];
}

+ (void)alertSetLifeStyleError:(NSString *)error
{
    NSString*   strError = [error substringFromIndex:6];
    NSString*   errorContent = ERROR_Unknown;
    
    switch ( [strError intValue] )
    {
        case 0:
            errorContent = ERROR_EMPTY_STRING;
            break;
        case 1:
            errorContent = ERROR_Not_Login;
            break;
        case 2:
            errorContent = ERROR_Session_Expired;
            break;
        case 3:
            errorContent = ERROR_NEED_RELOGIN;
            break;
        case 4:
            errorContent = NSLocalizedString(@"religion should be numeric",);
            break;
        case 5:
            errorContent = NSLocalizedString(@"invalid value for religion",);
            break;
        case 6:
            errorContent = NSLocalizedString(@"jobtitle should be numeric",);
            break;
        case 7:
            errorContent = NSLocalizedString(@"invalid value for jobtitle",);
            break;
        case 8:
            errorContent = NSLocalizedString(@"travel should be numeric",);
            break;
        case 9:
            errorContent = NSLocalizedString(@"invalid value for travel",);
            break;
        case 10:
            errorContent = NSLocalizedString(@"exercise should be numeric",);
            break;
        case 11:
            errorContent = NSLocalizedString(@"invalid value for exercise",);
            break;
        case 12:
            errorContent = NSLocalizedString(@"children_num should be numeric",);
            break;
        case 13:
            errorContent = NSLocalizedString(@"invalid value for children_num",);
            break;
        case 14:
            errorContent = NSLocalizedString(@"want_children should be numeric",);
            break;
        case 15:
            errorContent = NSLocalizedString(@"invalid value for want_children",);
            break;
        case 16:
            errorContent = NSLocalizedString(@"drinking should be numeric",);
            break;
        case 17:
            errorContent = NSLocalizedString(@"invalid value for drinking",);
            break;
        case 18:
            errorContent = NSLocalizedString(@"smoking should be numeric",);
            break;
        case 19:
            errorContent = NSLocalizedString(@"invalid value for smoking",);
            break;
        case 20:
            errorContent = NSLocalizedString(@"DB - insert operation failed",);
            break;
        case 21:
            errorContent = NSLocalizedString(@"income should be numeric",);
            break;
        case 22:
            errorContent = NSLocalizedString(@"invalid value for income",);
            break;
    }
    
    [self alertMessage:errorContent withTitle:NSLocalizedString(@"setLifestyle Failure",)];
}

+ (void)alertSetMatchingCriteriaError:(NSString *)error
{
    NSString*   strError = [error substringFromIndex:6];
    NSString*   errorContent = ERROR_Unknown;
    
    NSString*   errorList[] = {ERROR_EMPTY_STRING,
        ERROR_Not_Login,
        ERROR_Session_Expired,
        ERROR_NEED_RELOGIN,
        NSLocalizedString(@"age must be numeric",),
        NSLocalizedString(@"invalid value for age",),
        NSLocalizedString(@"height must be numeric",),
        NSLocalizedString(@"invalid value for height",),
        NSLocalizedString(@"distance must be numeric",),
        NSLocalizedString(@"invalid value for distance",),
        NSLocalizedString(@"religion must be numeric",),
        NSLocalizedString(@"invalid value for religion",),
        NSLocalizedString(@"ethnicity must be numeric",),
        NSLocalizedString(@"invalid value for ethnicity",),
        NSLocalizedString(@"income must be numeric",),
        NSLocalizedString(@"invalid value for income",),
        NSLocalizedString(@"education must be numeric",),
        NSLocalizedString(@"invalid value for education",),
        NSLocalizedString(@"children must be numeric",),
        NSLocalizedString(@"invalid value for children",),
        NSLocalizedString(@"mstatus must be numeric",),
        NSLocalizedString(@"smoke must be numeric",),
        NSLocalizedString(@"invalid value for smoke",),
        NSLocalizedString(@"drink must be numeric",),
        NSLocalizedString(@"invalid value for drink",),
        NSLocalizedString(@"exercise must be numeric",),
        NSLocalizedString(@"invalid value for exercise",),
        NSLocalizedString(@"DB - insert operation failed",) };
    
    int errorNum = [strError intValue];
    
    if( errorNum >= 0 && errorNum <= 28 )
    {
        errorContent = errorList[errorNum];
    }
    
    [self alertMessage:errorContent withTitle:NSLocalizedString(@"setMatchingCriteria Failure",)];
}

+ (void)alertSetPhysicalError:(NSString *)error
{
    NSString*   strError = [error substringFromIndex:6];
    NSString*   errorContent = ERROR_Unknown;
    
    NSString*   errorList[] = { ERROR_EMPTY_STRING,
        ERROR_Not_Login,
        ERROR_Session_Expired,
        ERROR_NEED_RELOGIN,
        NSLocalizedString(@"all values for Qx must be numeric",),
        NSLocalizedString(@"values for height and weight must be numeric",),
        NSLocalizedString(@"values for body and hair must be numeric",),
        NSLocalizedString(@"invalid value for body",),
        NSLocalizedString(@"invalid value for hair",),
        NSLocalizedString(@"DB - insert operation failed",) };

    int errorNum = [strError intValue];
    
    if( errorNum >= 0 && errorNum <= 9 )
    {
        errorContent = errorList[errorNum];
    }
    
    [self alertMessage:errorContent withTitle:NSLocalizedString(@"setPhysical Failure",)];
}

+ (void)alertSetTICSError:(NSString *)error
{
    NSString*   strError = [error substringFromIndex:6];
    NSString*   errorContent = ERROR_Unknown;
    NSString*   errorList[] = { ERROR_EMPTY_STRING,
        ERROR_Not_Login,
        ERROR_Session_Expired,
        ERROR_NEED_RELOGIN,
        NSLocalizedString(@"incorrect number of values in qValue1",),
        NSLocalizedString(@"invalid values for Q in qValue1",),
        NSLocalizedString(@"incorrect number of values in qValue2",),
        NSLocalizedString(@"invalid values for Q in qValue2",),
        NSLocalizedString(@"incorrect number of values in qValue3",),
        NSLocalizedString(@"invalid values for Q in qValue3",),
        NSLocalizedString(@"incorrect number of values in qValue4",),
        NSLocalizedString(@"invalid values for Q in qValue4",),
        NSLocalizedString(@"incorrect number of values in qValue5",),
        NSLocalizedString(@"invalid values for Q in qValue5",),
        NSLocalizedString(@"incorrect number of values in qValue6",),
        NSLocalizedString(@"nvalid values for Q in qValue6",),
        NSLocalizedString(@"incorrect number of values in qValue7",),
        NSLocalizedString(@"invalid values for Q in qValue7",),
        NSLocalizedString(@"incorrect number of values in qValue8",),
        NSLocalizedString(@"invalid values for Q in qValue8",),
        NSLocalizedString(@"incorrect number of values in qValue9",),
        NSLocalizedString(@"invalid values for Q in qValue9",),
        NSLocalizedString(@"incorrect number of values in qValue10",),
        NSLocalizedString(@"invalid values for Q in qValue10",),
        NSLocalizedString(@"Already has entry in TICS",) };
    
    int errorNum = [strError intValue];

    if( errorNum >= 0 && errorNum <= 23 )
    {
        errorContent = errorList[errorNum];
    }
    else
        errorContent = errorList[24];
    
    [self alertMessage:errorContent withTitle:NSLocalizedString(@"setTICS Failure",)];
}

+ (void)alertSetReadinessError:(NSString *)error
{
    NSString*   strError = [error substringFromIndex:6];
    NSString*   errorContent = ERROR_Unknown;
    
    NSString*   errorList[] = { ERROR_EMPTY_STRING,
        ERROR_Not_Login,
        ERROR_Session_Expired,
        ERROR_NEED_RELOGIN,
        NSLocalizedString(@"all values for Qx must be numeric",),
        NSLocalizedString(@"DB - insert operation failed",) };

    int errorNum = [strError intValue];
    
    if( errorNum >= 0 && errorNum <= 5 )
    {
        errorContent = errorList[errorNum];
    }
    
    [self alertMessage:errorContent withTitle:NSLocalizedString(@"setReadiness Failure",)];
}

+ (void)alertSetCultureError:(NSString *)error
{
    NSString*   strError = [error substringFromIndex:6];
    NSString*   errorContent = ERROR_Unknown;
    
    NSString*   errorList[] = { ERROR_EMPTY_STRING,
        ERROR_Not_Login,
        ERROR_Session_Expired,
        ERROR_NEED_RELOGIN,
        NSLocalizedString(@"all values for Qx must be numeric",),
        NSLocalizedString(@"DB - insert operation failed",) };
    
    int errorNum = [strError intValue];
    
    if( errorNum >= 0 && errorNum <= 5 )
    {
        errorContent = errorList[errorNum];
    }
    
    [self alertMessage:errorContent withTitle:NSLocalizedString(@"setCulture Failure",)];
}

+ (void)alertSetOpennessError:(NSString *)error
{
    NSString*   strError = [error substringFromIndex:6];
    NSString*   errorContent = ERROR_Unknown;
    
    NSString*   errorList[] = { ERROR_EMPTY_STRING,
        ERROR_Not_Login,
        ERROR_Session_Expired,
        ERROR_NEED_RELOGIN,
        NSLocalizedString(@"all values for Qx must be numeric",),
        NSLocalizedString(@"DB - insert operation failed",) };
    
    int errorNum = [strError intValue];
    
    if( errorNum >= 0 && errorNum <= 5 )
    {
        errorContent = errorList[errorNum];
    }
    
    [self alertMessage:errorContent withTitle:NSLocalizedString(@"setOpenness Failure",)];
}

+ (void)alertSetAgreeablenessError:(NSString *)error
{
    NSString*   strError = [error substringFromIndex:6];
    NSString*   errorContent = ERROR_Unknown;
    
    NSString*   errorList[] = { ERROR_EMPTY_STRING,
        ERROR_Not_Login,
        ERROR_Session_Expired,
        ERROR_NEED_RELOGIN,
        NSLocalizedString(@"all values for Qx must be numeric",),
        NSLocalizedString(@"DB - insert operation failed",) };
    
    int errorNum = [strError intValue];
    
    if( errorNum >= 0 && errorNum <= 5 )
    {
        errorContent = errorList[errorNum];
    }
    
    [self alertMessage:errorContent withTitle:NSLocalizedString(@"setAgreeableness Failure",)];
}

+ (void)alertSetMoneyError:(NSString *)error
{
    NSString*   strError = [error substringFromIndex:6];
    NSString*   errorContent = ERROR_Unknown;
    
    NSString*   errorList[] = { ERROR_EMPTY_STRING,
        ERROR_Not_Login,
        ERROR_Session_Expired,
        ERROR_NEED_RELOGIN,
        NSLocalizedString(@"all values for Qx must be numeric",),
        NSLocalizedString(@"DB - insert operation failed",) };
    
    int errorNum = [strError intValue];
    
    if( errorNum >= 0 && errorNum <= 5 )
    {
        errorContent = errorList[errorNum];
    }

    [self alertMessage:errorContent withTitle:NSLocalizedString(@"setMoney Failure",)];
}

+ (void)alertSetAdmirationError:(NSString *)error
{
    NSString*   strError = [error substringFromIndex:6];
    NSString*   errorContent = ERROR_Unknown;
    NSString*   errorList[] = { ERROR_EMPTY_STRING,
        ERROR_Not_Login,
        ERROR_Session_Expired,
        ERROR_NEED_RELOGIN,
        NSLocalizedString(@"incorrect number of values in qValue1",),
        NSLocalizedString(@"invalid values for Q in qValue1",),
        NSLocalizedString(@"incorrect number of values in qValue2",),
        NSLocalizedString(@"invalid values for Q in qValue2",),
        NSLocalizedString(@"incorrect number of values in qValue3",),
        NSLocalizedString(@"invalid values for Q in qValue3",),
        NSLocalizedString(@"incorrect number of values in qValue4",),
        NSLocalizedString(@"invalid values for Q in qValue4",),
        NSLocalizedString(@"incorrect number of values in qValue5",),
        NSLocalizedString(@"invalid values for Q in qValue5",),
        NSLocalizedString(@"incorrect number of values in qValue6",),
        NSLocalizedString(@"nvalid values for Q in qValue6",),
        NSLocalizedString(@"incorrect number of values in qValue7",),
        NSLocalizedString(@"invalid values for Q in qValue7",),
        NSLocalizedString(@"incorrect number of values in qValue8",),
        NSLocalizedString(@"invalid values for Q in qValue8",),
        NSLocalizedString(@"incorrect number of values in qValue9",),
        NSLocalizedString(@"invalid values for Q in qValue9",),
        NSLocalizedString(@"incorrect number of values in qValue10",),
        NSLocalizedString(@"invalid values for Q in qValue10",),
        NSLocalizedString(@"Already has entry in Admiration",) };
    
    int errorNum = [strError intValue];
    
    if( errorNum >= 0 && errorNum <= 23 )
    {
        errorContent = errorList[errorNum];
    }
    else
        errorContent = errorList[24];
    
    [self alertMessage:errorContent withTitle:NSLocalizedString(@"setAdmiration Failure",)];
}

+ (void)alertSetDISCError:(NSString *)error
{
    NSString*   strError = [error substringFromIndex:6];
    NSString*   errorContent = ERROR_Unknown;
    NSString*   errorList[] = { ERROR_EMPTY_STRING,
        ERROR_Not_Login,
        ERROR_Session_Expired,
        ERROR_NEED_RELOGIN,
        NSLocalizedString(@"invalid value for batch_num",),
        NSLocalizedString(@"incorrect number of values in qValue1",),
        NSLocalizedString(@"invalid values for Q in qValue1",),
        NSLocalizedString(@"incorrect number of values in qValue2",),
        NSLocalizedString(@"invalid values for Q in qValue2",),
        NSLocalizedString(@"incorrect number of values in qValue3",),
        NSLocalizedString(@"invalid values for Q in qValue3",),
        NSLocalizedString(@"incorrect number of values in qValue4",),
        NSLocalizedString(@"invalid values for Q in qValue4",),
        NSLocalizedString(@"incorrect number of values in qValue5",),
        NSLocalizedString(@"invalid values for Q in qValue5",),
        NSLocalizedString(@"incorrect number of values in qValue6",),
        NSLocalizedString(@"nvalid values for Q in qValue6",),
        NSLocalizedString(@"incorrect number of values in qValue7",),
        NSLocalizedString(@"invalid values for Q in qValue7",),
        NSLocalizedString(@"incorrect number of values in qValue8",),
        NSLocalizedString(@"invalid values for Q in qValue8",),
        NSLocalizedString(@"total_D, total_I, total_S, total_C must not be empty in batch 3",),
        NSLocalizedString(@"DB - insert operation failed",),
        NSLocalizedString(@" Already has entry in DISC",)
    };
    
    int errorNum = [strError intValue];
    
    if( errorNum >= 0 && errorNum <= 21 )
    {
        errorContent = errorList[errorNum];
    }
    else if( errorNum == 24 )
        errorContent = errorList[22];
    else
        errorContent = errorList[23];

    [self alertMessage:errorContent withTitle:NSLocalizedString(@"setDISC Failure",)];
}

+ (void)alertSetMyStoryError:(NSString *)error
{
    [self alertDefaultError:error withTitle:NSLocalizedString(@"setMyStory Failure",)];
}

//=======================================================================================//

+ (void)alertDefaultError:(NSString*)error withTitle:(NSString*)title
{
    if( [[error substringFromIndex:6] isEqualToString:@"0"] )
    {
        [ErrorProc alertMessage:ERROR_EMPTY_STRING withTitle:NSLocalizedString(@"Info",)];
        
    }
    else if( [[error substringFromIndex:6] isEqualToString:@"1"] )
    {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"eSynchrony"
                                                     message:ERROR_Not_Login
                                                    delegate:[eSyncronyAppDelegate sharedInstance].viewController
                                           cancelButtonTitle:NSLocalizedString(@"OK",)
                                           otherButtonTitles:nil, nil];
        [av show];
        [av release];
    }
    else if( [[error substringFromIndex:6] isEqualToString:@"2"] )
    {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Session Expired",)
                                                     message:NSLocalizedString(@"Please re-login your account.",)
                                                    delegate:[eSyncronyAppDelegate sharedInstance].viewController
                                           cancelButtonTitle:NSLocalizedString(@"OK",)
                                           otherButtonTitles:nil, nil];
        [av show];
        [av release];
    }
    else if( [[error substringFromIndex:6] isEqualToString:@"3"] )
    {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Sorry",)
                                                     message:ERROR_NEED_RELOGIN
                                                    delegate:nil
                                           cancelButtonTitle:NSLocalizedString(@"OK",)
                                           otherButtonTitles:nil, nil];
        [av show];
        [av release];
    }
    else
    {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Sorry",)
                                                     message:ERROR_NEED_RELOGIN
                                                    delegate:nil
                                           cancelButtonTitle:NSLocalizedString(@"OK",)
                                           otherButtonTitles:nil, nil];
        [av show];
        [av release];
    }
}

+ (void)alertError:(NSString *)error withTitle:(NSString*)title
{
    NSString*   errorContent = [error substringFromIndex:7];
 
    [self alertMessage:errorContent withTitle:title];
}

@end

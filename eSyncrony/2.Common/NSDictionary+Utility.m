//
//  NSDictionary+Utility.m
//  eSyncrony
//
//  Created by ESYNSZ-Limit on 16/7/11.
//  Copyright © 2016年 WonMH. All rights reserved.
//

#import "NSDictionary+Utility.h"

@implementation NSDictionary (Utility)

// in case of [NSNull null] values a nil is returned ...
- (id)objectForKeyNotNull:(id)key
{
    id object = [self objectForKey:key];
    if (object == [NSNull null])
        return nil;
    
    return object;
}

@end


#import <Foundation/Foundation.h>

#import "UtilComm.h"

#ifndef eSyncrony_global_h
#define eSyncrony_global_h

#define DEMO_VERSION
#define PHOTO_EDIT_ALLOW_MODE
//#define OFFLINE_TEST_MODE

//单例对象申明
#define SHARED_INSTANCE_DECLARE(className)   +(className *)sharedInstance;
#define SHARED_INSTANCE_DEFINE(className) \

//--------Index of Menu item button-----------//
#define MENU_MY_MATCHES         100
#define MYMATCHES_PROPOSED      101
#define MYMATCHES_PENDING       102
#define MYMATCHES_APPROVED      103
#define MYMATCHES_CANCELLED     104
#define MYMATCHES_REPORT        105
#define MYMATCHES_FILTER        106
#define MYMATCHES_FAVORITED     107

#define MENU_DATES              200
#define DATES_NEW_DATES         201
#define DATES_PAST_DATES        202
#define DATE_FEEDBACK           203

#define MENU_DATINGREPORT       300

#define MENU_MYPROFILE          400
#define PROFILE_PROFILE         401
#define PROFILE_REPORT          402
#define PROFILE_ACCOUNT         403

#define MENU_SETTING            500

#define MENU_UPGRADE            600
#define MENU_UPGRADE_SUCCESS    601

#define MENU_FEEDBACK           700

#define MENU_PHOTO_EXCHANGE     800
#define MENU_LOGOUT             900

//--------Index of Edit Profile button-----------//
#define EDIT_PROFILE_ETHNICITY          1
#define EDIT_PROFILE_NATIONALITY        2
#define EDIT_PROFILE_FIRST_LANG         3
#define EDIT_PROFILE_OTHER_LANG         4
#define EDIT_PROFILE_EDUCATION          5
#define EDIT_PROFILE_INCOME             6
#define EDIT_PROFILE_PROFESSION         7
#define EDIT_PROFILE_TRAVEL             8
#define EDIT_PROFILE_EXERCISE           9
#define EDIT_PROFILE_HAVE_CHILD         10
#define EDIT_PROFILE_WANT_CHILD         11
#define EDIT_PROFILE_SMOKING            12
#define EDIT_PROFILE_DRINKING           13
#define EDIT_PROFILE_RELIGION           14
#define EDIT_PROFILE_BODY_TYPE          15
#define EDIT_PROFILE_HAIR_STYLE         16
#define EDIT_PROFILE_FOODS              17
#define EDIT_PROFILE_HOBBIES            18
#define EDIT_PROFILE_WATCH              19
#define EDIT_PROFILE_LISTEN             20
#define EDIT_PROFILE_SPEND_HOLIDAY      21
#define EDIT_PROFILE_PET                22
#define EDIT_PROFILE_SPORTS             23

//--------Index of FILTER Setting -----------//
#define FILTER_SETTING_AGE               1
#define FILTER_SETTING_HEIGHT            2
#define FILTER_SETTING_EDUCATION         3
#define FILTER_SETTING_MARRIAGE          4
#define FILTER_SETTING_INCOME            5
#define FILTER_SETTING_JOB               6
#define FILTER_SETTING_RELIGION          7
#define FILTER_SETTING_ETHNICITY         8

//--------Index of Match Setting -----------//
#define MATCH_SETTING_AGE               1
#define MATCH_SETTING_HEIGHT            2
#define MATCH_SETTING_DISTANCE          3
#define MATCH_SETTING_RELIGION          4
#define MATCH_SETTING_ETHNICITY         5
#define MATCH_SETTING_INCOME            6
#define MATCH_SETTING_EDUCATION         7
#define MATCH_SETTING_CHIDREN           8
#define MATCH_SETTING_MARIAL            9
#define MATCH_SETTING_SMOKE             10
#define MATCH_SETTING_DRINK             11
#define MATCH_SETTING_EXERCISE          12


//--------Index of Feedback Setting -----------//
#define FEEDBACK_POSITIVE               1
#define FEEDBACK_NEGATIVE               2
#define FEEDBACK_RESTURANT              3
#define FEEDBACK_CONSIDER               4
#define FEEDBACK_PRESENT                5
#define FEEDBACK_CONFIDENCE             6
#define FEEDBACK_INTEREST               7
#define FEEDBACK_CONVERSATION           8
#define FEEDBACK_DINING                 9
#define FEEDBACK_CONTACT                10
#define FEEDBACK_LAST                   11
#define FEEDBACK_EXPERIENC              12
#define FEEDBACK_FEEL                   13
#define FEEDBACK_SECONDDATE             14

//------------------- color macro ----------------------//

#define RGBColor(R, G, B)           [UIColor colorWithRed:R/255.0f green:G/255.0f blue:B/255.0f alpha:1.0f]

#define TITLE_TEXT_COLLOR           RGBColor(0x02, 0x5f, 0x6c)

#define BACKCOLOR_GRAY              RGBColor(0xf0, 0xf0, 0xf2)
#define LINE_TEXT_COLOR             RGBColor(0x74, 0x99, 0x05)

#define BUTTON_TEXT_COLOR           RGBColor(0xff, 0xff, 0xff)

#define LEVEL1_COLOR                RGBColor(0x1f, 0xc1, 0xd7)
#define LEVEL2_COLOR                RGBColor(0x79, 0xdb, 0xe8)
#define LEVEL3_COLOR                RGBColor(0xff, 0xbb, 0x00)
#define LEVEL4_COLOR                RGBColor(0xee, 0x8c, 0xaa)
#define LEVEL5_COLOR                RGBColor(0xe2, 0x3e, 0x70)

#define TITLE_REG_COLOR             RGBColor(0xb0, 0x41, 0x63)

#define OPENESS_TITLE_COLOR         RGBColor(0x1f, 0xa5, 0xd7)

#define RED_COLOR                   RGBColor(0xff, 0x00, 0x00)
#define BACKCOLOR_GRAY2             RGBColor(0xf3, 0xf3, 0xf3)
#define BORDER_GRAY                 RGBColor(0x9f, 0x9f, 0x9f)
#define WHITE                       RGBColor(0xff, 0xff, 0xff)
#define PROFILE_FIELD_VALUE1_COLOR  RGBColor(0x02, 0x5f, 0x6c)
#define PROFILE_FIELD_COLOR1        RGBColor(0x00, 0x00, 0x00)
#define MATCH_COM_COLOR             RGBColor(0x9b, 0xbb, 0x59)
#define EMPTY_COLOR                 RGBColor(0xcd, 0xce, 0xcf)
#define DARK_GRAY                   RGBColor(0x62, 0x5d, 0x5d)
#define MORE_DARK_COLOR             RGBColor(0x42, 0x42, 0x42)

#endif

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

@interface Global : NSObject
{
    NSMutableDictionary     *_cacheInfos;
}

@property (nonatomic, assign) int nProposedMatches;
@property (nonatomic, assign) int nDates;

+ (Global*)sharedGlobal;

- (id)getCacheInfoForKey:(NSString*)key;

- (void)setCacheInfo:(id)info forKey:(NSString*)key;

//socket.io
+(NSString *)getCurrentDate;


+ (NSString*)getCurDateString;
+ (NSString*)getCurDateTimeString;
+ (NSString*)getCurMonthDate;
+ (NSString*)getYearMonthDateFrom:(NSDate*)date;

+ (NSString*)getDateTime:(NSDate*)date withFormat:(NSString*)format;
+ (NSDate*)convertStringToDate:(NSString*)strDate withFormat:(NSString*)format;

+ (BOOL)isNullOrNilObject:(id)object;

+ (NSArray*)loadQuestionArray:(NSString*)fileName;

+ (void)loadBasicProfileInfoAndSaveToClient;

@end


//
//  NSDictionary+Utility.h
//  eSyncrony
//
//  Created by ESYNSZ-Limit on 16/7/11.
//  Copyright © 2016年 WonMH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Utility)

- (id)objectForKeyNotNull:(id)key;

@end

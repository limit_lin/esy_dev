
#import <Foundation/Foundation.h>


@interface XMLParser : NSObject<NSXMLParserDelegate>
{
	NSMutableString *currentElement;
	NSMutableArray  *resultData;		// parsing data
	
	NSMutableDictionary *item;			// key and value item dictionary
	
	NSString *parserTitle;
	
	NSString    *startKey;              // key to be parsing
	NSArray     *elementsArray;
    NSArray     *attributeArray;
}

- (id) initWithContentsOfURL:(NSString *)urlString attributesKey:(NSArray *)key elementsArray:(NSArray *)keyArr title:(NSString *)title;
- (id) initWithContentsOfData:(NSData *)data attributesKey:(NSArray *)key elementsArray:(NSArray *)keyArr title:(NSString *)title;
- (void)deleteCharacters:(NSString *)string;

@property (retain) NSMutableArray *resultData;
@property (retain) NSString *parserTitle;
@property (retain) NSString *attributesKey;
@property (retain) NSArray *elementsArray;
@property (retain) NSArray *attributeArray;
@property (nonatomic, readwrite) BOOL hasElements;

@end

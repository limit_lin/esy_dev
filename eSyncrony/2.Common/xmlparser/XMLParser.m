//
//  XMLParser.m
//  XMLParser
//
//  Created by inchan kang on 11. 2. 25..
//  Copyright 2011 macbugi. All rights reserved.
//

#import "XMLParser.h"


@implementation XMLParser

@synthesize resultData;
@synthesize parserTitle;
@synthesize attributesKey;
@synthesize elementsArray,attributeArray;
@synthesize hasElements;

#pragma mark-
#pragma mark initialize & finalize

- (void)dealloc
{
	[resultData release];
	[attributesKey release];
    [attributeArray release];
	[elementsArray release];
	[parserTitle release];
	
	[super dealloc];
}

- (id) initWithContentsOfURL:(NSString *)urlString attributesKey:(NSArray *)key elementsArray:(NSArray *)keyArr title:(NSString *)title
{
    //151119 append
    self = [super init];
    if (!self) {
        return nil;
    }
    //end
    
	if (self != nil)
    {
		NSMutableArray *_arrRes = [[NSMutableArray alloc] init];
		self.resultData = _arrRes;
        [_arrRes release];
		self.attributeArray = key;
		self.elementsArray = keyArr;
		self.parserTitle = title;
		
        
		NSXMLParser *parser = [[NSXMLParser alloc] initWithContentsOfURL:[NSURL URLWithString:urlString]];
        //NSXMLParser *parser = [[NSXMLParser alloc] initWithData:_dataCode];
        
		[parser setDelegate:self];
		BOOL Y = [parser parse];
		if (!Y) {
		
			self.resultData = nil;
		}
		[parser release];
	}
	return self;
}

- (id) initWithContentsOfData:(NSData *)data attributesKey:(NSArray *)key elementsArray:(NSArray *)keyArr title:(NSString *)title
{
    //151119 append
    self = [super init];
    if (!self) {
        return nil;
    }
    //end
	if (self != nil) {
		
        NSMutableArray *_arrRes = [[NSMutableArray alloc] init];
		self.resultData = _arrRes;
        [_arrRes release];
		self.attributeArray = key;
		self.elementsArray = keyArr;
		self.parserTitle = title;
		
		NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
		[parser setDelegate:self];
		[parser parse];
		[parser release];
		
	}
	return self;
}

#pragma mark -
#pragma mark XMLparser delegate

- (void)parserDidStartDocument:(NSXMLParser *)parser
{
	if (parserTitle) {
		//NSLog(@"<%@> parser Start",parserTitle);
	}	
}


- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
	for (int i = 0; i < [attributeArray count]; i++) {
        if ([elementName isEqualToString:[attributeArray objectAtIndex:i]]) {
            item = [[NSMutableDictionary alloc] init];
        }	
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
	if(!currentElement) {
		currentElement = [[NSMutableString alloc] initWithFormat:@"%@",string];
		
	}else {
		[currentElement appendString:string];
 	}
	
	[self deleteCharacters:@"\n"];
	[self deleteCharacters:@"\t"];
	[self deleteCharacters:@"   "];
	
	NSRange substr = [currentElement rangeOfString:@" "];
    ////NSLog(@"currentElement=%@", currentElement);
	
	if (substr.location == 0 && substr.length > 0 )
	{
		[currentElement replaceCharactersInRange: substr withString:@""];
	}
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    hasElements = YES;
	if (currentElement == nil) {
		currentElement = [[NSMutableString alloc] initWithFormat:@""];
	}
//		//NSLog(@"%@",elementName);
    if (elementsArray) {
        if (![currentElement hasPrefix:@"ERROR"])
        {
            int i;
            for (i = 0; i < [elementsArray count]; i++) {
                if ([elementName isEqualToString:[elementsArray objectAtIndex:i]]) {
                    [item setObject:[NSString stringWithFormat:@"%@",currentElement] forKey:[elementsArray objectAtIndex:i]];
                    break;
                }
            }
            
            for (i = 0; i < [attributeArray count]; i++) {
                if ([elementName isEqualToString:[attributeArray objectAtIndex:i]]) {
                    [self.resultData addObject:item];
                    [item release];
                    item = nil;
                    break;
                }
            }
        }
        else {
            hasElements = NO;
            [self.resultData addObject:currentElement];
        }
        
    }
    else {
        hasElements = NO;
        [self.resultData addObject:currentElement];
    }
	
	[currentElement release];
	currentElement = nil;
}

- (void)parserDidEndDocument:(NSXMLParser *)parser{
	if (parserTitle) {
		//NSLog(@"<%@> parser END",parserTitle);
		//NSLog(@"result : %@",resultData);
	}
}


- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError{
	//NSLog(@"parser ERROR");	
	//NSLog(@"ERROR = %@",parseError);
}

- (void)deleteCharacters:(NSString *)string{
	NSString *search = string;
	NSString *replace = @"";
	
	NSRange substr = [currentElement rangeOfString: search];
	
	while(substr.location != NSNotFound)
	{
		[currentElement replaceCharactersInRange: substr withString: replace];
		substr = [currentElement rangeOfString: search];
	}
}

@end



#import "XMLReader.h"

NSString *const kXMLReaderTextNodeKey = @"text";

@interface XMLReader ()

- (id)initWithError:(NSError **)error;
- (NSDictionary *)objectWithData:(NSData *)data;

@end


@implementation XMLReader

#pragma mark -
#pragma mark Public methods

+ (NSDictionary *)dictionaryForXMLData:(NSData *)data error:(NSError **)error
{
    XMLReader *reader = [[XMLReader alloc] initWithError:error];
    NSDictionary *rootDictionary = [reader objectWithData:data];//leasks
    [reader release];
    return rootDictionary;
}

+ (NSDictionary *)dictionaryForXMLString:(NSString *)string error:(NSError **)error
{
    
//    string = [string stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    return [XMLReader dictionaryForXMLData:data error:error];//leaks
}

#pragma mark -
#pragma mark Parsing

- (id)initWithError:(NSError **)error
{
    if (self = [super init])
    {
        errorPointer = error;
    }
    return self;
}

- (void)dealloc
{
    [dictionaryStack release];
    [textInProgress release];
    [super dealloc];
}

- (NSDictionary *)objectWithData:(NSData *)data
{
    // Clear out any old data
    [dictionaryStack release];
    [textInProgress release];
    
    dictionaryStack = [[NSMutableArray alloc] init];
    textInProgress = [[NSMutableString alloc] init];
    
    // Initialize the stack with a fresh dictionary
    [dictionaryStack addObject:[NSMutableDictionary dictionary]];
    
    // Parse the XML
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    BOOL success = [parser parse];//leaks
    [parser release];
    
    // Return the stack's root dictionary on success
    if (success)
    {
        NSDictionary *resultDict = [dictionaryStack objectAtIndex:0];
        return resultDict;
    }
    
    return nil;
}

#pragma mark -
#pragma mark NSXMLParserDelegate methods

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    // Get the dictionary for the current level in the stack
    NSMutableDictionary *parentDict = [dictionaryStack lastObject];
    
    // Create the child dictionary for the new element, and initilaize it with the attributes
    NSMutableDictionary *childDict = [NSMutableDictionary dictionary];
    [childDict addEntriesFromDictionary:attributeDict];
    
    // If there's already an item for this key, it means we need to create an array
    id existingValue = [parentDict objectForKey:elementName];
    if (existingValue)
    {
        NSMutableArray *array = nil;
        if ([existingValue isKindOfClass:[NSMutableArray class]])
        {
            // The array exists, so use it
            array = (NSMutableArray *) existingValue;
        }
        else
        {
            // Create an array if it doesn't exist
            array = [NSMutableArray array];
            [array addObject:existingValue];
            
            // Replace the child dictionary with an array of children dictionaries
            [parentDict setObject:array forKey:elementName];
        }
        
        // Add the new child dictionary to the array
        [array addObject:childDict];
    }
    else
    {
        // No existing value, so update the dictionary
        [parentDict setObject:childDict forKey:elementName];
    }
    
    // Update the stack
    [dictionaryStack addObject:childDict];
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    // Update the parent dict with text info
    //NSMutableDictionary *dictInProgress = [dictionaryStack lastObject];

    // Set the text property
    if( [textInProgress length] > 0 )
    {
#if 0
        //[dictInProgress setObject:textInProgress forKey:kXMLReaderTextNodeKey];
#else
        [dictionaryStack removeLastObject];

        NSMutableDictionary *lastItem = [dictionaryStack lastObject];
        
        id existingValue = [lastItem objectForKey:elementName];

        if( existingValue != nil && [existingValue isKindOfClass:[NSMutableArray class]] )
        {
            NSMutableArray  *lastArray = (NSMutableArray*)existingValue;
            [lastArray removeLastObject];
            [lastArray addObject:textInProgress];
        }
        else
            [lastItem setObject:textInProgress forKey:elementName];
#endif
        // Reset the text
        [textInProgress release];
        textInProgress = [[NSMutableString alloc] init];
    }
    else
    {
        NSMutableDictionary *lastItem = [dictionaryStack lastObject];
        if( [lastItem count] == 0 )
        {
            [dictionaryStack removeLastObject];
            
            lastItem = [dictionaryStack lastObject];
            
            id existingValue = [lastItem objectForKey:elementName];
            if( existingValue != nil && [existingValue isKindOfClass:[NSMutableArray class]] )
            {
                NSMutableArray  *lastArray = (NSMutableArray*)existingValue;
                [lastArray removeLastObject];
                [lastArray addObject:@""];
            }
            else
            {
                //[lastItem removeObjectForKey:elementName];
                [lastItem setObject:@"" forKey:elementName];
            }
        }
        else
        {
            // Pop the current dict
            [dictionaryStack removeLastObject];
        }
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    NSString *buff = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    buff = [buff stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    
    if( [buff isEqualToString:@""] )
        return;

    // Build the text value
    [textInProgress appendString:string];
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
    // Set the error pointer to the parser's error object
    *errorPointer = parseError;
}

@end
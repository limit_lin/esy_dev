#import "NSDictionary+UrlEncoding.h"

static NSString *toString(id object)
{
    return [NSString stringWithFormat: @"%@", object];
}

static NSString *urlEncode(id object)
{
//    NSLog(@"urlEncode:%@",object);
    NSString *string = toString(object);
    
    string = [string stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
    string = [string stringByReplacingOccurrencesOfString:@"%0A" withString:@"%0D"];

    return string;
}

@implementation NSDictionary (UrlEncoding)

- (NSString*)urlEncodedString
{
    NSMutableArray *parts = [NSMutableArray array];

    for( id key in self )
    {
        id value = [self objectForKey: key];
        NSString *part = [NSString stringWithFormat: @"%@=%@", urlEncode(key), urlEncode(value)];
        [parts addObject: part];
    }
    
    return [parts componentsJoinedByString:@"&"];
}

@end

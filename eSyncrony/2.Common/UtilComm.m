
#import "UtilComm.h"
#import "Global.h"

//#import "ASIFormDataRequest.h"
//#import "AFHTTPSessionManager.h"
#import "AFNetworking.h"

#import "NSDictionary+UrlEncoding.h"
#import "XMLParser.h"
#import "XMLReader.h"
#import "XMLDictionary.h"
#import "eSyncronyAppDelegate.h"
#import "JSON.h"
#import "mainMenuViewController.h"


@implementation UtilComm

// European Central Bank Currency Rates
+ (NSDictionary*)getRatesFromEuropeanCentralBankResult:(NSString*)result
{
    NSMutableDictionary *exchangeRates = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [exchangeRates setObject:@"1.0" forKey:@"EUR"];
    
    NSInteger       nStartPos = 0;
    NSRange         searchRange;
    
    while( 1 )
    {
        searchRange = NSMakeRange( nStartPos, [result length]-nStartPos );
        NSRange range = [result rangeOfString:@"currency='" options:NSCaseInsensitiveSearch range:searchRange];
        if( range.length == 0 )
            break;
        
        nStartPos = range.location + range.length;
        searchRange = NSMakeRange( nStartPos, [result length]-nStartPos );
        
        NSString    *currencyName = [result substringWithRange:NSMakeRange(nStartPos, 3)];
        
        NSRange range1 = [result rangeOfString:@"rate='" options:NSCaseInsensitiveSearch range:searchRange];
        NSRange range2 = [result rangeOfString:@"'/>" options:NSCaseInsensitiveSearch range:searchRange];
        
        nStartPos = range1.location + range1.length;
        range = NSMakeRange( nStartPos, range2.location - nStartPos);
        NSString    *currencyRate = [result substringWithRange:range];
        
        nStartPos = range2.location + range2.length;
        
        [exchangeRates setObject:currencyRate forKey:currencyName];
    }
    
    return exchangeRates;
}

+ (NSDictionary*)getGlobalCurrencyRates
{
    NSString*   urlPath = @"http://www.ecb.int/stats/eurofxref/eurofxref-daily.xml";
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlPath] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0f];

    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [request setTimeoutInterval:30.0f];
    [request setHTTPShouldHandleCookies:YES];
    NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    [cookieStorage setCookieAcceptPolicy:NSHTTPCookieAcceptPolicyAlways];
    
    NSError             *error;
    NSHTTPURLResponse   *response;
    NSData              *urlData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if( urlData == nil )
        return nil;
    
    NSString*   result = [[NSString alloc] initWithData:urlData encoding:NSUTF8StringEncoding];

    return [self getRatesFromEuropeanCentralBankResult:result];
}

+ (float)getCurrencyValue:(float)value from:(NSString*)currencyFrom to:(NSString*)currencyTo
{
    NSDictionary*   exchangeRates = [self getGlobalCurrencyRates];
    if( exchangeRates == nil )
        return 0;

    if( ![exchangeRates objectForKey:currencyFrom] || ![exchangeRates objectForKey:currencyTo] )
        return 0;

    double rateFrom = [[exchangeRates objectForKey:currencyFrom] doubleValue];
    double rateTo = [[exchangeRates objectForKey:currencyTo] doubleValue];

    value = (float)(value*rateTo/rateFrom);

    return value;
}

+ (NSDictionary*)requestToServerPost:(NSString*)serverPath withParams:(NSDictionary*)params//post
{
    NSLog(@"requestToServerPost　serverPath　urlPath:%@",serverPath);
    NSLog(@"requestToServerPost　params　== %@",params);
////    １.创建请求
//    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[serverPath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
//
//
//    [request setRequestMethod:@"POST"];
//   
////    [ASIFormDataRequest setDefaultTimeOutSeconds:30];//append 1126
//    
//    NSArray *keyarray = [NSArray arrayWithArray:params.allKeys];
//    NSLog(@"keyarray:%@",keyarray);
//    for (int i=0; i<keyarray.count; i++) {
//        NSString *key =[keyarray objectAtIndex:i];
//        NSString *value = [NSString stringWithFormat: @"%@", [params objectForKey:key]];
//        
//         NSLog(@"i:%d %@ = %@",i,key,value);
//        [request setPostValue:[NSString stringWithFormat:@"%@",value] forKey:[NSString stringWithFormat:@"%@",key]];
//    }
//    request.numberOfTimesToRetryOnTimeout = 2; //append 151127
//    [request setTimeOutSeconds:30];//超时
//    [request setShouldAttemptPersistentConnection:NO];
////    request.timeOutSeconds = 30;
////    request.shouldAttemptPersistentConnection = NO;
//    
//    //２.发送同步请求
//    [request startSynchronous];
////     [request startAsynchronous];//发送异步请求
//    //3.获得错误信息
//    NSError *error = [request error];
//
//    NSString *response = [request responseString];
//    NSDictionary *result = [XMLReader dictionaryForXMLString:response error:&error];
//    
////    NSLog(@"%@",result);
//    id responsevalue = [result objectForKey:@"response"];
//    if ([responsevalue isKindOfClass:[NSString class]]&&([responsevalue isEqualToString:@"ERROR 0"]||[responsevalue isEqualToString:@"ERROR 1"]||[responsevalue isEqualToString:@"ERROR 2"]||[responsevalue isEqualToString:@"ERROR 3"])&&([eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl!=nil)) {
//        NSLog(@"%@\nresponse:%@",serverPath,responsevalue);
//        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:responsevalue];
//        return nil;
//    }
//    if ([responsevalue isKindOfClass:[NSString class]]&&[response hasPrefix:@"ERROR"]) {
//        NSLog(@"%@\nresponse:%@",serverPath,response);
//        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
//        return nil;
//    }
//        
//    if( error ){
//        NSLog(@"%@\nerror:%@",serverPath,error);
//        [ErrorProc alertMessage:ERROR_NEED_RELOGIN withTitle:@"eSynchrony"];
//        return nil;
//    }
//    
//    if( response == nil ){
//        NSLog(@"%@\nresponse== nil",serverPath);
//        [ErrorProc alertMessage:ERROR_NEED_RELOGIN withTitle:@"eSynchrony"];
//        return nil;
//    }
//    if ([response rangeOfString:@"error"].location !=NSNotFound) {
//        NSLog(@"%@\nresponse:%@",serverPath,response);
//        [ErrorProc alertMessage:ERROR_NEED_RELOGIN withTitle:@"eSynchrony"];
//        return nil;
//    }
//    
//    if( result == nil ){
//        [ErrorProc alertMessage:ERROR_NEED_RELOGIN withTitle:@"eSynchrony"];
//        NSLog(@"%@\nresult == nil\nresponse:%@",serverPath,response);
//        return nil;
//    }
//    NSLog(@"%@\nresult:%@\n",serverPath,result);
//    return result;
    
    
    
//    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
//    [manager POST:serverPath
//       parameters:params
//          success:^(NSURLSessionDataTask *task, id responseObject)
//     {
//         NSLog(@"JSON: %@", responseObject);
//     }
//          failure:^(NSURLSessionDataTask *task, NSError *error)
//     {
//         NSLog(@"Error: %@", error);
//     }];
//    
//    NSArray *keyarray = [NSArray arrayWithArray:params.allKeys];
//    NSLog(@"keyarray:%@",keyarray);
//    for (int i=0; i<keyarray.count; i++) {
//        NSString *key =[keyarray objectAtIndex:i];
//        NSString *value = [NSString stringWithFormat: @"%@", [params objectForKey:key]];
//        
//        NSLog(@"i:%d %@ = %@",i,key,value);
//    }

    

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    __block NSDictionary* dic =[[NSDictionary alloc]init];
    // post请求
    [manager POST:serverPath parameters:params constructingBodyWithBlock:^(id  _Nonnull formData) {
        // 拼接data到请求体，这个block的参数是遵守AFMultipartFormData协议的。
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        // 这里可以获取到目前的数据请求的进度
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        // 请求成功，解析数据
        NSLog(@"%@", responseObject);
        dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers | NSJSONReadingMutableLeaves error:nil];
        
        NSLog(@"%@", dic);
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        // 请求失败
        NSLog(@"%@", [error localizedDescription]);
    }];
    

    return dic;
//    NSURL *URL = [NSURL URLWithString:serverPath];
//    [manager POST:URL.absoluteString parameters:params progress:nil success:^(NSURLSessionTask *task, id responseObject) {
//        NSLog(@"JSON: %@", responseObject);
//       NSDictionary * result = responseObject;
//        
//    } failure:^(NSURLSessionTask *operation, NSError *error) {
//        NSLog(@"Error: %@", error);
//    }];
    
    
}
+ (NSDictionary*)requestToServer:(NSString*)serverPath withParams:(NSDictionary*)params
{
/*
    NSString*   paramString = [params urlEncodedString];
    NSString*   urlPath = [NSString stringWithFormat:@"%@?%@",serverPath,paramString];
//    NSLog(@"urlPath:%@",urlPath);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:urlPath]];
//    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[urlPath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];

    [request setRequestMethod:@"GET"];
    [request setTimeOutSeconds:30];//30.0f-->30
    request.numberOfTimesToRetryOnTimeout = 2; //append 151127
    [request setShouldAttemptPersistentConnection:NO];//appnd 151126
    [request startSynchronous];

    NSError *error = [request error];
    NSString *response = [request responseString];
    
    NSDictionary *result = [XMLReader dictionaryForXMLString:response error:&error];
    //    result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];//http://bbs.csdn.net/topics/390867119   json数据
    
    id responsevalue = [result objectForKey:@"response"];
    if ([responsevalue isKindOfClass:[NSString class]]&&([responsevalue isEqualToString:@"ERROR 0"]||[responsevalue isEqualToString:@"ERROR 1"]||[responsevalue isEqualToString:@"ERROR 2"]||[responsevalue isEqualToString:@"ERROR 3"])&&([eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl!=nil)) {
        NSLog(@"%@\nresponse:%@",urlPath,response);
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:responsevalue];
        return nil;
    }
    
    //    error:Error Domain=ASIHTTPRequestErrorDomain Code=2 "The request timed out" UserInfo=0x16d92260 {NSLocalizedDescription=The request timed out}
    //for test 151110
    if(error){
        NSLog(@"%@\nerror:%@",urlPath,error);
        
        [ErrorProc alertMessage:ERROR_NEED_RELOGIN withTitle:@"eSynchrony"];
        return nil;
    }
    
    
    if( response == nil ){
        NSLog(@"%@\nresponse== nil",urlPath);
        [ErrorProc alertMessage:ERROR_NEED_RELOGIN withTitle:@"eSynchrony"];
        return nil;
    }
    
    
    if( result == nil ){
        [ErrorProc alertMessage:ERROR_NEED_RELOGIN withTitle:@"eSynchrony"];
        NSLog(@"%@\nresult == nil\nresponse:%@",urlPath,response);
        return nil;
    }
    NSLog(@"%@%@\n\n",urlPath,result);
    
    return result;
*/

    NSString*   paramString = [params urlEncodedString];
    NSString*   urlPath = [NSString stringWithFormat:@"%@?%@",serverPath,paramString];
    //    NSLog(@"urlPath:%@",urlPath);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
//    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
//    NSDictionary *result = [[NSDictionary alloc]init];
//    [manager GET:serverPath
//      parameters:nil
//         success:^(NSURLSessionDataTask *task, id responseObject)
//     {
//         
//         NSLog(@"JSON: %@", responseObject);
//     }
//         failure:^(NSURLSessionDataTask *task, NSError *error)
//     {
//         NSLog(@"Error: %@", error);
//     }];
    
    __block NSDictionary* dic = [[NSDictionary alloc]init];
    // Get请求
    [manager GET:urlPath parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        // 这里可以获取到目前的数据请求的进度
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        // 请求成功，解析数据
        NSLog(@"responseObject = %@", responseObject);
       dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers | NSJSONReadingMutableLeaves error:nil];
        NSLog(@"dic = %@", dic);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        // 请求失败
        NSLog(@"error = %@", [error localizedDescription]);
    }];
    
    return dic;

}
+ (NSDictionary*)requestJsonToServerPost:(NSString*)serverPath withParams:(NSDictionary*)params
{
//    NSLog(@"urlPath:%@",serverPath);
//    NSLog(@"%@",params);
/*
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[serverPath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    
    [request setRequestMethod:@"POST"];
    
    NSArray *keyarray = [NSArray arrayWithArray:params.allKeys];
    NSLog(@"keyarray:%@",keyarray);
    for (int i=0; i<keyarray.count; i++) {
        NSString *key =[keyarray objectAtIndex:i];
        NSString *value = [NSString stringWithFormat: @"%@", [params objectForKey:key]];
        
        NSLog(@"i:%d %@ = %@",i,key,value);
        [request setPostValue:[NSString stringWithFormat:@"%@",value] forKey:[NSString stringWithFormat:@"%@",key]];
    }
    request.numberOfTimesToRetryOnTimeout = 2;
    [request setTimeOutSeconds:30];//超时
    [request setShouldAttemptPersistentConnection:NO];
    
    //２.发送同步请求
    [request startSynchronous];
    //     [request startAsynchronous];//发送异步请求
    //3.获得错误信息
    NSError *error = [request error];
    
//    NSString *response = [request responseString];
//    NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [request responseData];
//    NSArray *response = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];//http://bbs.csdn.net/topics/390867119   json数据
    NSDictionary* result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    return result;
*/
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
//    [manager POST:serverPath
//       parameters:params
//          success:^(NSURLSessionDataTask *task, id responseObject)
//     {
//         NSLog(@"JSON: %@", responseObject);
//     }
//          failure:^(NSURLSessionDataTask *task, NSError *error)
//     {
//         NSLog(@"Error: %@", error);
//     }];
    
    __block NSDictionary *dic = [[NSDictionary alloc]init];
    // post请求
    [manager POST:serverPath parameters:params constructingBodyWithBlock:^(id  _Nonnull formData) {
        // 拼接data到请求体，这个block的参数是遵守AFMultipartFormData协议的。
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        // 这里可以获取到目前的数据请求的进度
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        // 请求成功，解析数据
        NSLog(@"json post %@", responseObject);
        dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers | NSJSONReadingMutableLeaves error:nil];
        
        NSLog(@"json post dic %@", dic);
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        // 请求失败
        NSLog(@"json post error %@", [error localizedDescription]);
    }];
    
  
    return dic;
    
//    NSArray *keyarray = [NSArray arrayWithArray:params.allKeys];
//    NSLog(@"keyarray:%@",keyarray);
//    for (int i=0; i<keyarray.count; i++) {
//        NSString *key =[keyarray objectAtIndex:i];
//        NSString *value = [NSString stringWithFormat: @"%@", [params objectForKey:key]];
//        
//        NSLog(@"i:%d %@ = %@",i,key,value);
//    }
//
//    //    NSString *response = [request responseString];
//    //    NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
//    NSData *data = [request responseData];
//    //    NSArray *response = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];//http://bbs.csdn.net/topics/390867119   json数据
//    NSDictionary* result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
//    NSLog(@"%@",result);
    
//    return result;
    
}

//======================ip address==================================//
+(NSDictionary*)getCountryCode:(NSDictionary*)params
{
    NSString*   serverPath = [NSString stringWithFormat:@"%@",WEBSERVICE_CHK_IPAddr];
    return [self requestToServer:serverPath withParams:params];
}
//==================================================================//
//========================= Account Register =======================//
//==================================================================//

+ (NSDictionary*)registerWithParams:(NSDictionary*)params
{
    NSString*   serverPath = [NSString stringWithFormat:@"%@",WEBSERVICE_REGISTER];
    return [self requestToServerPost:serverPath withParams:params];
}
+ (NSDictionary*)requestHearusfrom
{
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    NSString*   serverPath = [NSString stringWithFormat:@"%@",WEBSERVICE_HEARUSFROM];
    return [self requestToServer:serverPath withParams:params];
    
}
+ (NSDictionary*)loginWithParams:(NSDictionary*)params
{
    NSString*   serverPath = [NSString stringWithFormat:@"%@login", WEBSERVICE_BASEURI];

    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)loginWithFaceBook:(NSDictionary*)params
{
    NSString*   serverPath = [NSString stringWithFormat:@"%@fb_login", WEBSERVICE_BASEURI];

    return [self requestToServer:serverPath withParams:params];
}

//+ (NSDictionary*)retrieveStepNo:(NSDictionary*)params//before 1.3.3
//{
//    NSString*   serverPath = [NSString stringWithFormat:@"%@retrieveStep_no", WEBSERVICE_BASEURI];
//    
//    return [self requestToServer:serverPath withParams:params];
//}
+ (NSDictionary*)retrieveStepNo:(NSDictionary*)params//to 1.3.3
{
    NSString*   serverPath = [NSString stringWithFormat:@"%@retrieveStep_noNew", WEBSERVICE_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)saveBasicInfo:(NSDictionary*)params
{
    NSString*   serverPath = [NSString stringWithFormat:@"%@setBasic", WEBSERVICE_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)saveLifeStyleInfo:(NSDictionary*)params
{
//    NSString*   serverPath = [NSString stringWithFormat:@"%@setLifestyle", WEBSERVICE_BASEURI];
//    return [self requestToServer:serverPath withParams:params];
    NSString*   serverPath = [NSString stringWithFormat:@"%@",WEBSERVICE_REGISTER];
    return [self requestToServerPost:serverPath withParams:params];
}

+ (NSDictionary*)saveCriteriaInfo:(NSDictionary*)params
{
    NSString*   serverPath = [NSString stringWithFormat:@"%@setMatchingCriteria", WEBSERVICE_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)savePhysicalInfo:(NSDictionary*)params
{
    NSString*   serverPath = [NSString stringWithFormat:@"%@setPhysical", WEBSERVICE_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)saveInterestInfo:(NSDictionary*)params
{
    NSString*   serverPath = [NSString stringWithFormat:@"%@setInterestHobbies", WEBSERVICE_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)saveMyValue:(NSDictionary*)params
{
    NSString*   serverPath = [NSString stringWithFormat:@"%@setTICS", WEBSERVICE_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)saveReadinessInfo:(NSDictionary*)params
{
    NSString*   serverPath = [NSString stringWithFormat:@"%@setReadiness_NEW", WEBSERVICE_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}
+ (NSDictionary*)saveIntimacyInfo:(NSDictionary*)params
{
    NSString*   serverPath = [NSString stringWithFormat:@"%@setIntimacy", WEBSERVICE_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)saveCultureInfo:(NSDictionary*)params
{
    NSString*   serverPath = [NSString stringWithFormat:@"%@setCulture", WEBSERVICE_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)saveOpennessInfo:(NSDictionary*)params
{
    NSString*   serverPath = [NSString stringWithFormat:@"%@setOpenness", WEBSERVICE_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)saveAgreeableInfo:(NSDictionary*)params
{
    NSString*   serverPath = [NSString stringWithFormat:@"%@setAgreeableness", WEBSERVICE_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)saveMoneyInfo:(NSDictionary*)params
{
    NSString*   serverPath = [NSString stringWithFormat:@"%@setMoney", WEBSERVICE_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)saveAdmiration:(NSDictionary*)params
{
    NSString*   serverPath = [NSString stringWithFormat:@"%@setAdmiration", WEBSERVICE_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)savePersonality:(NSDictionary*)params
{
    NSString*   serverPath = [NSString stringWithFormat:@"%@setDISC", WEBSERVICE_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)saveStoryInfo:(NSDictionary*)params
{
//    NSString*   serverPath = [NSString stringWithFormat:@"%@setMyStory", WEBSERVICE_BASEURI];
//    return [self requestToServer:serverPath withParams:params];
    NSString*   serverPath = [NSString stringWithFormat:@"%@", WEBSERVICE_REGISTER];
    return [self requestToServerPost:serverPath withParams:params];
}
//Vip Stroy
+ (NSDictionary*)saveVipStoryInfo:(NSDictionary*)params
{
//    NSString*   serverPath = [NSString stringWithFormat:@"%@setMyVipStory", WEBSERVICE_BASEURI];
//    return [self requestToServer:serverPath withParams:params];
    NSString*   serverPath = [NSString stringWithFormat:@"%@", WEBSERVICE_REGISTER];
    return [self requestToServerPost:serverPath withParams:params];
}
//self_coordination
+ (NSDictionary*)saveSelfcoord:(NSDictionary*)params
{
    NSString*   serverPath = [NSString stringWithFormat:@"%@", WEBSERVICE_SELF_COORD];
    
    return [self requestToServer:serverPath withParams:params];
}
//self_coordDates
+ (NSDictionary*)saveSelfcoorddates:(NSDictionary*)params
{
    NSString*   serverPath = [NSString stringWithFormat:@"%@", WEBSERVICE_SELF_COORDDATES];
    
    return [self requestToServer:serverPath withParams:params];
}
//self_Check_coordDates
+ (NSDictionary*)CheckSelfcoorddates:(NSDictionary*)params
{
    NSString*   serverPath = [NSString stringWithFormat:@"%@", WEBSERVICE_SELF_CHECKCOORDDATES];
    
    return [self requestToServer:serverPath withParams:params];
}
//================================================================//
//========================= Account Verify =======================//
//================================================================//

+ (NSDictionary*)checkAccountVerify
{
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    
    NSString*   serverPath = [NSString stringWithFormat:@"%@verifyAccount", WEBSERVICE_ACCOUNT_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)smsVerify:(NSDictionary*)params
{
//    NSString*   serverPath = [NSString stringWithFormat:@"%@smsVerify", WEBSERVICE_ACCOUNT_BASEURI];
    
    NSString*   serverPath = [NSString stringWithFormat:@"%@smsVerifyNew", WEBSERVICE_ACCOUNT_BASEURI];
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)smsVerifyCode:(NSDictionary*)params
{
    NSString*   serverPath = [NSString stringWithFormat:@"%@smsVerifyCode", WEBSERVICE_ACCOUNT_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)nricVerify:(NSDictionary*)params
{
    NSString*   serverPath = [NSString stringWithFormat:@"%@nricVerify", WEBSERVICE_ACCOUNT_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)nricVerifyNew:(NSDictionary*)params//quiz nric repair 0119
{
    NSString*   serverPath = [NSString stringWithFormat:@"%@nricVerifyNew", WEBSERVICE_ACCOUNT_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)getDocCertVerifyStatus
{
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];

    NSString*   serverPath = [NSString stringWithFormat:@"%@docCertVerify", WEBSERVICE_ACCOUNT_BASEURI];

    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)uploadDocImage:(UIImage*)imgPhoto withType:(int)nDocType
{
    NSString*   accNo = [eSyncronyAppDelegate sharedInstance].strAccNo;
    NSString*   tokenKey = [eSyncronyAppDelegate sharedInstance].strTokenKey;
    NSString*   fileName = [NSString stringWithFormat:@"%@.png", tokenKey];
    NSData*     pngImgData = UIImagePNGRepresentation(imgPhoto);
    
    NSArray     *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString    *documentsDirectory = [paths objectAtIndex:0];
    NSString    *filePath = [documentsDirectory stringByAppendingPathComponent:fileName];
    
    if( [[NSFileManager defaultManager] fileExistsAtPath:filePath] )
        [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];

    [pngImgData writeToFile:filePath atomically:YES];
    
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    [params setValue:accNo forKey:@"acc_no"];
    [params setValue:@"uploaddoc" forKey:@"cmd"];
    [params setValue:[NSNumber numberWithInt:nDocType] forKey:@"doctype"];
    [params setValue:filePath forKey:@"uploadedfile"];
    
//    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:WEBSERVICE_UPLOAD_DOCS_BASEURI]];
//
//    [request setTimeOutSeconds:30];
//    [request setFile:filePath forKey:@"uploadedfile"];
//    [request setPostValue:@"uploaddoc" forKey:@"cmd"];
//    [request setPostValue:accNo forKey:@"acc_no"];
//    [request setPostValue:[NSNumber numberWithInt:nDocType] forKey:@"doctype"];
//    
//    [request startSynchronous];

//    NSError *error = [request error];
//    if( error )
//        return nil;
//    
//    NSString        *response = [request responseString];
//#if 1
//    NSDictionary    *result = [XMLReader dictionaryForXMLString:response error:&error];
//#else
//    NSData  *urlData = nil;
//    urlData = [response dataUsingEncoding:NSUTF8StringEncoding];
//    if( urlData == nil )
//        return nil;
//    NSError      *e = nil;
//    NSDictionary *result = [NSJSONSerialization JSONObjectWithData:urlData options:NSJSONReadingMutableContainers error:&e];
//#endif
//    
//    return result;
}

+ (NSDictionary*)retrievePrimaryPhotoFilename
{

    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    
    NSString*   serverPath = [NSString stringWithFormat:@"%@retrievePrimaryPhotoFilename", WEBSERVICE_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)uploadPrimaryPhoto:(UIImage*)imgPhoto
{
    NSString*   accNo = [eSyncronyAppDelegate sharedInstance].strAccNo;
    NSString*   tokenKey = [eSyncronyAppDelegate sharedInstance].strTokenKey;
    NSString*   fileName = [NSString stringWithFormat:@"%@.png", tokenKey];
    NSData*     pngImgData = UIImagePNGRepresentation(imgPhoto);
    
    NSArray     *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString    *documentsDirectory = [paths objectAtIndex:0];
    NSString    *filePath = [documentsDirectory stringByAppendingPathComponent:fileName];

    if( [[NSFileManager defaultManager] fileExistsAtPath:filePath] )
        [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];

    [pngImgData writeToFile:filePath atomically:YES];

//    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:WEBSERVICE_UPLOAD_PHOTO_BASEURI]];
//
//    //[request setPostFormat:ASIMultipartFormDataPostFormat];
//    //[request setData:pngImgData withFileName:fileName andContentType:fileName forKey:@"uploadedfile"];
//    
//    [request setTimeOutSeconds:30];
//    [request setFile:filePath forKey:@"uploadedfile"];
//    [request setPostValue:@"uploadphoto" forKey:@"cmd"];
//    [request setPostValue:accNo forKey:@"acc_no"];
//    
//    [request setShouldAttemptPersistentConnection:NO];//append 1126
//    [request startSynchronous];
//    
//    NSError *error = [request error];
//    if( error )
//        return nil;
//    
//    NSString        *response = [request responseString];
//    NSDictionary    *result = [XMLReader dictionaryForXMLString:response error:&error];
//    NSLog(@"%@\nresult:%@",WEBSERVICE_UPLOAD_PHOTO_BASEURI,result);
//    
//    return result;
}

+ (NSDictionary*)uploadNormalPhoto:(UIImage*)imgPhoto
{
    NSString*   accNo = [eSyncronyAppDelegate sharedInstance].strAccNo;
    NSString*   tokenKey = [eSyncronyAppDelegate sharedInstance].strTokenKey;
    NSString*   fileName = [NSString stringWithFormat:@"%@.png", tokenKey];
    NSData*     pngImgData = UIImagePNGRepresentation(imgPhoto);
    
    NSArray     *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString    *documentsDirectory = [paths objectAtIndex:0];
    NSString    *filePath = [documentsDirectory stringByAppendingPathComponent:fileName];
    
    if( [[NSFileManager defaultManager] fileExistsAtPath:filePath] )
        [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
    
    [pngImgData writeToFile:filePath atomically:YES];
    
//    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:WEBSERVICE_UPLOAD_NORMPHOTO_URL]];
//    
//    //[request setPostFormat:ASIMultipartFormDataPostFormat];
//    //[request setData:pngImgData withFileName:fileName andContentType:fileName forKey:@"uploadedfile"];
//    
//    [request setTimeOutSeconds:30];
//    [request setFile:filePath forKey:@"uploadedfile"];
//    [request setPostValue:@"uploadphoto" forKey:@"cmd"];
//    [request setPostValue:accNo forKey:@"acc_no"];
//    [request setPostValue:@"photoname" forKey:@"photoname"];
//    [request setPostValue:@"photo description" forKey:@"description"];
//    
//    [request setShouldAttemptPersistentConnection:NO];//append 1126
//    [request startSynchronous];
//    
//    NSError *error = [request error];
//    if( error )
//        return nil;
//    
//    NSString        *response = [request responseString];
//    NSDictionary    *result = [XMLReader dictionaryForXMLString:response error:&error];
//    NSLog(@"%@\nresult:%@",WEBSERVICE_UPLOAD_NORMPHOTO_URL,result);
//    return result;
}

//================================================================//
//===========================  Profile  ==========================//
//================================================================//

+ (NSDictionary*)getProfileWithType:(NSString*)type
{
    //--------------------------------------------------------------//
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:type forKey:@"type"];
    
    NSString*   serverPath = [NSString stringWithFormat:@"%@view", WEBSERVICE_PROFILE_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)getCommonInterest:(NSString*)macc_no
{
  //  NSString*   accNo = [eSyncronyAppDelegate sharedInstance].strAccNo;
  //  NSString*   tokenKey = [eSyncronyAppDelegate sharedInstance].strTokenKey;
    
    //--------------------------------------------------------------//
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:macc_no forKey:@"macc_no"];
    
    NSString*   serverPath = [NSString stringWithFormat:@"%@commonInterestView", WEBSERVICE_PROFILE_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)viewMatchProfile:(NSString *)macc_no Type:(NSString*)strType
{

    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:strType forKey:@"type"];
    [params setObject:macc_no forKey:@"macc_no"];
    
    NSString*   serverPath = [NSString stringWithFormat:@"%@view", WEBSERVICE_PROFILE_BASEURI];
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)compatibilityPreview:(NSString *)macc_no
{

    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:macc_no forKey:@"macc_no"];
    
    NSString*   serverPath = [NSString stringWithFormat:@"%@compatibilityPreview", WEBSERVICE_PROFILE_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)compatibilityViewDetail:(NSString *)macc_no
{
    
    //--------------------------------------------------------------//
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:macc_no forKey:@"macc_no"];
    
    NSString*   serverPath = [NSString stringWithFormat:@"%@compatibilityViewDetail", WEBSERVICE_PROFILE_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)viewMatchingCriteria
{

    //--------------------------------------------------------------//
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    
    NSString*   serverPath = [NSString stringWithFormat:@"%@viewMatchingCriteria", WEBSERVICE_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)updateMatchingCriteria:(int)age
                                 height:(int)height
                               distance:(int)distance
                               religion:(int)religion
                              ethnicity:(int)ethnicity
                                 income:(int)income
                              education:(int)education
                               children:(int)children
                                mstatus:(int)mstatus
                                  smoke:(int)smoke
                                  drink:(int)drink
                              excercise:(int)excersise
{
  //  NSString*   accNo = [eSyncronyAppDelegate sharedInstance].strAccNo;
  //  NSString*   tokenKey = [eSyncronyAppDelegate sharedInstance].strTokenKey;
    
    //--------------------------------------------------------------//
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[NSString stringWithFormat:@"%d", age] forKey:@"age"];
    [params setObject:[NSString stringWithFormat:@"%d", height] forKey:@"height"];
    [params setObject:[NSString stringWithFormat:@"%d", distance] forKey:@"distance"];
    [params setObject:[NSString stringWithFormat:@"%d", religion] forKey:@"religion"];
    [params setObject:[NSString stringWithFormat:@"%d", ethnicity] forKey:@"ethnicity"];
    [params setObject:[NSString stringWithFormat:@"%d", income] forKey:@"income"];
    [params setObject:[NSString stringWithFormat:@"%d", education] forKey:@"education"];
    [params setObject:[NSString stringWithFormat:@"%d", children] forKey:@"children"];
    [params setObject:[NSString stringWithFormat:@"%d", mstatus] forKey:@"mstatus"];
    [params setObject:[NSString stringWithFormat:@"%d", smoke] forKey:@"smoke"];
    [params setObject:[NSString stringWithFormat:@"%d", drink] forKey:@"drink"];
    [params setObject:[NSString stringWithFormat:@"%d", excersise] forKey:@"exercise"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    
    NSString*   serverPath = [NSString stringWithFormat:@"%@updateMatchingCriteria", WEBSERVICE_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)updateBasicInfo:(NSDictionary*)params
{
    NSString*   serverPath = [NSString stringWithFormat:@"%@updateBasic", WEBSERVICE_PROFILE_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)updateLifestyle:(NSDictionary*)params
{
    NSString*   serverPath = [NSString stringWithFormat:@"%@updateLifestyle", WEBSERVICE_PROFILE_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)updateAppearance:(NSDictionary*)params
{
    NSString*   serverPath = [NSString stringWithFormat:@"%@updateAppearance", WEBSERVICE_PROFILE_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)updateInterest:(NSDictionary*)params
{
    NSString*   serverPath = [NSString stringWithFormat:@"%@updateInterest", WEBSERVICE_PROFILE_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}

//================================================================//
//===========================  Account  ==========================//
//================================================================//

+ (NSDictionary*)viewAccount
{
    //--------------------------------------------------------------//
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    
    NSString*   serverPath = [NSString stringWithFormat:@"%@viewAccount", WEBSERVICE_ACCOUNT_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)updateAccount:(NSString *)nName dob:(NSString *)dob handphone:(NSString *)handphone
{

    //--------------------------------------------------------------//
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:nName forKey:@"nname"];
    [params setObject:dob forKey:@"dob"];
    [params setObject:handphone forKey:@"handphone"];
    
    NSString*   serverPath = [NSString stringWithFormat:@"%@updateAccount", WEBSERVICE_ACCOUNT_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)updatePassword:(NSString *)strPwd
{

    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:strPwd forKey:@"password"];

    NSString*   serverPath = [NSString stringWithFormat:@"%@updatePassword", WEBSERVICE_ACCOUNT_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)personalityTypeReport:(NSString*)maccNo
{
    
    NSString *p_lanague = [eSyncronyAppDelegate sharedInstance].language;
    int lanagueId = 0;
    if ([p_lanague hasPrefix:@"zh-Hant"]||[p_lanague hasPrefix:@"zh-HK"]) {//repair 151224
        lanagueId = 3;
    }else if ([p_lanague hasPrefix:@"id"]) {
        lanagueId = 8;
    }else if ([p_lanague hasPrefix:@"th"])
        lanagueId = 203;
    else
        lanagueId = 0;
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    
    if( !(maccNo == Nil) && (![maccNo isEqualToString:@""]) )
        [params setObject:maccNo forKey:@"macc_no"];
    
    [params setObject:[NSNumber numberWithInt:lanagueId] forKey:@"lang"];//append 151214
    
//    NSString*   serverPath = [NSString stringWithFormat:@"%@personalityTypeReport", WEBSERVICE_PROFILE_BASEURI];
    NSString*   serverPath = [NSString stringWithFormat:@"%@personalityTypeReport", WEBSERVICE_PROFILE_BASEURI1];
   
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)retrieveNewPassword:(NSString*)strEmail
{
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:strEmail forKey:@"email"];
    
    NSString*   serverPath = [NSString stringWithFormat:@"%@retrieveNewPassword", WEBSERVICE_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}

//================================================================//
//===========================  Matches  ==========================//
//================================================================//
+ (NSDictionary*)loadMatchesFavoritedInfo:(NSInteger)page{//matches favorited list
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:[[NSNumber numberWithInteger:page]stringValue] forKey:@"page"];
    
    NSString*   serverPath=[NSString stringWithFormat:@"%@", WEBSERVICE_MATCHES_FAVORITE];
    return [self requestToServer:serverPath withParams:params];
}
+ (NSDictionary*)removeFavoritedMatch:(NSString *)favorNum{//remove favorite match
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:favorNum forKey:@"favor_acc_no"];
    NSString*   serverPath=[NSString stringWithFormat:@"%@", WEBSERVICE_REMOVE_FAVORITED];
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)addFavoritedMatch:(NSString *)favorNum{//add favorite match
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:favorNum forKey:@"favor_acc_no"];
    NSString*   serverPath=[NSString stringWithFormat:@"%@", WEBSERVICE_ADD_FAVORITED];
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)renewalPopup//append renewal
{
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    
     NSString*   serverPath=[NSString stringWithFormat:@"%@renewalPopupChk", WEBSERVICE_RENEWAL_POPUP];

    return [self requestToServer:serverPath withParams:params];//leaks 100%
}
+ (NSDictionary*)matchTips//append matchTips
{
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    
   NSString*   serverPath=[NSString stringWithFormat:@"%@", WEBSERVICE_MATCHTIPS];   
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)retrieveNumberMatches
{
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];

    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    
    NSString*   serverPath = [NSString stringWithFormat:@"%@retrieveNumberMatches", WEBSERVICE_MATCHES_BASEURI];

    return [self requestToServer:serverPath withParams:params];
}
+ (NSDictionary*)loadMatchesInfoWithNewType:(NSString*)type Page:(NSInteger)page View_status:(NSString*)view_status
{
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:type forKey:@"type"];
    [params setObject:[[NSNumber numberWithInteger:page]stringValue] forKey:@"page"];
    [params setObject:view_status forKey:@"view_status"];
    
    NSString*   serverPath = [NSString stringWithFormat:@"%@viewAllMatches", WEBSERVICE_MATCHES_BASEURI];
      return [self requestToServer:serverPath withParams:params];
    
}
+ (NSDictionary*)loadMatchesInfoWithType:(NSString*)type withPage:(NSInteger)page
{
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:type forKey:@"type"];
    [params setObject:[[NSNumber numberWithInteger:page]stringValue] forKey:@"page"];

    
   //append match list filter
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAge forKey:@"age"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strEducation forKey:@"education"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strMstatus forKey:@"mstatus"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strJobtitle forKey:@"jobtitle"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strIncome forKey:@"income"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strReligion forKey:@"religion"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strEthnicity forKey:@"ethnicity"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strHeight  forKey:@"height"];
    NSLog(@"params =%@",params);
    //[params setObject:view_status forKey:@"view_status"];//loadMatchesInfoWithNewType
    
    NSString*   serverPath = [NSString stringWithFormat:@"%@viewAllMatches", WEBSERVICE_MATCHES_BASEURI];
    
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)loadMatchesInfoWithTypeNew:(NSString*)type
{
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:type forKey:@"type"];
    
    NSString*   serverPath = [NSString stringWithFormat:@"%@viewAllMatches", WEBSERVICE_MATCHES_BASEURI];
    return [self requestToServer:serverPath withParams:params];
    
}
+ (NSDictionary*)loadMatchesInfoWithType:(NSString*)type
{
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:type forKey:@"type"];
    
    NSString*   serverPath = [NSString stringWithFormat:@"%@viewMatches", WEBSERVICE_MATCHES_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)retrieveMatchesPhotoFilename:(NSString*)matchAccNo
{
    //--------------------------------------------------------------//
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:matchAccNo forKey:@"match_id"];

    NSString*   serverPath = [NSString stringWithFormat:@"%@retrievePrimaryPhotoFilename", WEBSERVICE_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)approveToMatch:(NSString*)macc_no
{

    //--------------------------------------------------------------//
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:macc_no forKey:@"match_id"];
    [params setObject:@"Y" forKey:@"reply"];

    NSString*   serverPath = [NSString stringWithFormat:@"%@approveDeclineMatch", WEBSERVICE_MATCHES_BASEURI];

    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)declineToMatch:(NSString*)macc_no reasonId:(int)reasonID comment:(NSString*)comment
{
    //--------------------------------------------------------------//
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];

    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:macc_no forKey:@"match_id"];
    [params setObject:@"N" forKey:@"reply"];
    [params setObject:[NSNumber numberWithInt:reasonID] forKey:@"reason_id"];
    [params setObject:comment forKey:@"comment"];

    NSString*   serverPath = [NSString stringWithFormat:@"%@approveDeclineMatch", WEBSERVICE_MATCHES_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)sendReminderToMatch:(NSString*)macc_no
{
    
   // NSString*   accNo = [eSyncronyAppDelegate sharedInstance].strAccNo;
   // NSString*   tokenKey = [eSyncronyAppDelegate sharedInstance].strTokenKey;
    
    //--------------------------------------------------------------//
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:macc_no forKey:@"match_id"];
    
    NSString*   serverPath = [NSString stringWithFormat:@"%@sendReminder", WEBSERVICE_MATCHES_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)getMatchReports
{
    
    //NSString*   accNo = [eSyncronyAppDelegate sharedInstance].strAccNo;
    //NSString*   tokenKey = [eSyncronyAppDelegate sharedInstance].strTokenKey;
    
    //--------------------------------------------------------------//
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];

    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    
    NSString*   serverPath = [NSString stringWithFormat:@"%@viewMatchReport", WEBSERVICE_MATCHES_BASEURI];

    return [self requestToServer:serverPath withParams:params];
}

//================================================================//
//==========================    Dates   ==========================//
//================================================================//

+ (NSDictionary*)retrieveNumberDatesAndPackage
{
  
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    
    NSString*   serverPath = [NSString stringWithFormat:@"%@retrieveNumberDates", WEBSERVICE_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)retrieveNumberDates
{
    //NSString*   accNo = [eSyncronyAppDelegate sharedInstance].strAccNo;
    //NSString*   tokenKey = [eSyncronyAppDelegate sharedInstance].strTokenKey;
    
    //--------------------------------------------------------------//
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    
    NSString*   serverPath = [NSString stringWithFormat:@"%@retrieveNumberDates", WEBSERVICE_MATCHES_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)viewDates:(NSString *)type
{
    //--------------------------------------------------------------//
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:type forKey:@"type"];
    
    NSString*   serverPath = [NSString stringWithFormat:@"%@viewDates", WEBSERVICE_DATES_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)viewDatesReport
{
    //--------------------------------------------------------------//
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];

    NSString*   serverPath = [NSString stringWithFormat:@"%@viewDatesInfo", WEBSERVICE_DATES_BASEURI];

    return [self requestToServer:serverPath withParams:params];
}

//================================================================//
//==========================  Feedback  ==========================//
//================================================================//

+ (NSDictionary*)giveFeedback:(NSString *)macc_no Param1:(NSString *)positive Param2:(NSString *)negative Param3:(NSString *)rest_rating Param4:(NSString *)first_imp1 Param5:(NSString *)first_imp2 Param6:(NSString *)first_imp3 Param7:(NSString *)during_date1 Param8:(NSString *)during_date2 Param9:(NSString *)during_date3 Param10:(NSString *)goodbyes1 Param11:(NSString *)goodbyes2 Param12:(NSString *)overal_txtarea Param13:(NSString *)overall_exp_rating Param14:(NSString *)afterthoughts1 Param15:(NSString *)afterthoughts2 Param16:(NSString *)followupID Param17:(NSString *)additional_feedback Param18:(NSString *)exchange_mail
{
    
    //--------------------------------------------------------------//
    NSNumber *nums=@([goodbyes2 integerValue]);
//    NSLog(@"nums = %@",nums);

    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:macc_no forKey:@"match_id"];
    [params setObject:positive forKey:@"positive"];
    [params setObject:negative forKey:@"negative"];
    [params setObject:rest_rating forKey:@"rest_rating"];
    [params setObject:first_imp1 forKey:@"first_imp1"];
    [params setObject:first_imp2 forKey:@"first_imp2"];
    [params setObject:first_imp3 forKey:@"first_imp3"];
    [params setObject:during_date1 forKey:@"during_date1"];
    [params setObject:during_date2 forKey:@"during_date2"];
    [params setObject:during_date3 forKey:@"during_date3"];
    [params setObject:@"1" forKey:@"goodbyes1"];
//    [params setObject:goodbyes2 forKey:@"goodbyes2"];
    [params setObject:nums forKey:@"goodbyes2"];
    [params setObject:overal_txtarea forKey:@"overall_txtarea"];
    [params setObject:overall_exp_rating forKey:@"overall_exp_rating"];
    [params setObject:@"1" forKey:@"afterthoughts1"];
    [params setObject:@"1" forKey:@"afterthoughts2"];
    [params setObject:followupID forKey:@"followupID"];
    [params setObject:additional_feedback forKey:@"additional_feedback"];
    [params setObject:exchange_mail forKey:@"exchange_mail"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    
    NSString*   serverPath = [NSString stringWithFormat:@"%@feedback", WEBSERVICE_DATES_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}
+ (NSDictionary*)newGiveFeedback:(NSDictionary*)params
{
    
//  NSString*   serverPath = [NSString stringWithFormat:@"%@updateFeedbackApp", WEBSERVICE_BASEURI];
    NSString*   serverPath = [NSString stringWithFormat:@"%@",WEBSERVICE_REGISTER];
    return [self requestToServerPost:serverPath withParams:params];
}

+ (NSDictionary*)updateTmpdate:(NSDictionary*)params
{
    NSString*   serverPath = [NSString stringWithFormat:@"%@updateTemdt", WEBSERVICE_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}
//================================================================//
//==========================   Q n A    ==========================//
//================================================================//

+ (NSDictionary*)retrievePendingQuestion:(NSString*)macc_no
{

    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:macc_no forKey:@"macc_no"];
    
    NSString*   serverPath = [NSString stringWithFormat:@"%@pendingQuestionNew", WEBSERVICE_QNA];
    
    return [self requestToServer:serverPath withParams:params];
}
+ (NSDictionary*)retrievePreviousAnswer:(NSString*)qStr{
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:qStr forKey:@"qStr"];
    
    NSString*   serverPath = [NSString stringWithFormat:@"%@retrievePreviousAnswer", WEBSERVICE_QNA];
    
    return [self requestToServer:serverPath withParams:params];
}
+ (NSDictionary*)loadAskedQuestions:(NSString*)macc_no
{
    NSString *p_lanague = [eSyncronyAppDelegate sharedInstance].language;
    int lanagueId = 0;
    if ([p_lanague hasPrefix:@"zh-Hant"]||[p_lanague hasPrefix:@"zh-HK"]) {//repair 151224
        lanagueId = 3;
    }
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:macc_no forKey:@"macc_no"];
    [params setObject:[NSNumber numberWithInt:lanagueId] forKey:@"lang"];//append 151214
    
    NSString*   serverPath = [NSString stringWithFormat:@"%@questionsAsked", WEBSERVICE_QNA];
    
    return [self requestToServer:serverPath withParams:params];
}

//append Q&A
+ (NSDictionary*)loadFavouriteQuestions:(NSString*)macc_no andCategory:(int)category
{
    NSString *p_lanague = [eSyncronyAppDelegate sharedInstance].language;
    int lanagueId = 0;
    if ([p_lanague hasPrefix:@"zh-Hant"]||[p_lanague hasPrefix:@"zh-HK"]) {//repair 151224
        lanagueId = 3;
    }
   
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:macc_no forKey:@"macc_no"];
    [params setObject:[NSNumber numberWithInt:category] forKey:@"catogory"];
    [params setObject:[NSNumber numberWithInt:lanagueId] forKey:@"lang"];//append 151214
    
    NSString*   serverPath = [NSString stringWithFormat:@"%@viewFaveQuestions", WEBSERVICE_QNA];
    
    return [self requestToServer:serverPath withParams:params];
}
+ (NSDictionary*)loadNormalQuestions:(NSString*)macc_no andCategory:(int)category
{
    // NSString*   accNo = [eSyncronyAppDelegate sharedInstance].strAccNo;
    // NSString*   tokenKey = [eSyncronyAppDelegate sharedInstance].strTokenKey;
    NSString *p_lanague = [eSyncronyAppDelegate sharedInstance].language;
    int lanagueId = 0;
    if ([p_lanague hasPrefix:@"zh-Hant"]||[p_lanague hasPrefix:@"zh-HK"]) {//repair 151224
        lanagueId = 3;
    }
    //--------------------------------------------------------------//
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:macc_no forKey:@"macc_no"];
    [params setObject:[NSNumber numberWithInt:category] forKey:@"catogory"];
    [params setObject:[NSNumber numberWithInt:lanagueId] forKey:@"lang"];//append 151214
    
    NSString*   serverPath = [NSString stringWithFormat:@"%@viewQuestion", WEBSERVICE_QNA];
    
    return [self requestToServer:serverPath withParams:params];
}
//append ending

+ (NSDictionary*)addQuestionToFavourite:(NSString*)questionID
{
    //NSString*   accNo = [eSyncronyAppDelegate sharedInstance].strAccNo;
    //NSString*   tokenKey = [eSyncronyAppDelegate sharedInstance].strTokenKey;
    
    //--------------------------------------------------------------//
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];

    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:questionID forKey:@"question_id"];
    
    NSString*   serverPath = [NSString stringWithFormat:@"%@addToFavorites", WEBSERVICE_QNA];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)removeQuestionFromFavourite:(NSString*)questionID
{
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:questionID forKey:@"question_id"];
    
    NSString*   serverPath = [NSString stringWithFormat:@"%@removeFromFavorites", WEBSERVICE_QNA];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)sendQuestions:(NSString*)strQuestions toMatch:(NSString*)macc_no
{
   
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:macc_no forKey:@"macc_no"];
    [params setObject:strQuestions forKey:@"question"];

    NSString*   serverPath = [NSString stringWithFormat:@"%@sendQuestion", WEBSERVICE_QNA];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)sendAnswer:(int)nAnswer toQuestions:(int)nQuestion toMatch:(NSString*)macc_no
{
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:macc_no forKey:@"macc_no"];
    [params setObject:[NSNumber numberWithInt:nQuestion] forKey:@"rowid"];
    [params setObject:[NSNumber numberWithInt:nAnswer] forKey:@"answer"];

    NSString*   serverPath = [NSString stringWithFormat:@"%@answerQuestion", WEBSERVICE_QNA];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)viewAnswers:(NSString*)macc_no
{
    NSString *p_lanague = [eSyncronyAppDelegate sharedInstance].language;
    int lanagueId = 0;
    if ([p_lanague hasPrefix:@"zh-Hant"]||[p_lanague hasPrefix:@"zh-HK"]) {//repair 151224
        lanagueId = 3;
    }
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:macc_no forKey:@"macc_no"];
    [params setObject:[NSNumber numberWithInt:lanagueId] forKey:@"lang"];//append 151214
    
    NSString*   serverPath = [NSString stringWithFormat:@"%@viewAnswers", WEBSERVICE_QNA];
    
    return [self requestToServer:serverPath withParams:params];
}

//================================================================//
//==========================   Photo    ==========================//
//================================================================//

+ (NSDictionary*)requestPhotoCmdNew:(NSString*)strCmd toMatch:(NSString*)macc_no//append0428
{

    //--------------------------------------------------------------//
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    if (macc_no==nil) {
        macc_no = @"";
    }
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:macc_no forKey:@"match_id"];
    [params setObject:@"Y" forKey:@"update"];
    
    NSString*   serverPath = [NSString stringWithFormat:@"%@%@", WEBSERVICE_MATCHES_BASEURI, strCmd];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)requestPhotoCmd:(NSString*)strCmd toMatch:(NSString*)macc_no
{
    
    
    //--------------------------------------------------------------//
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    if (macc_no==nil) {
        macc_no = @"";
    }
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:macc_no forKey:@"match_id"];
    [params setObject:@"Y" forKey:@"update"];//append
    
    NSString*   serverPath = [NSString stringWithFormat:@"%@%@", WEBSERVICE_MATCHES_BASEURI, strCmd];
    
    return [self requestToServer:serverPath withParams:params];
}
+ (NSDictionary*)viewPhotoSingle:(NSString*)macc_no{
    return [self requestPhotoCmd:@"viewPhotoSingle" toMatch:macc_no];
}

+ (NSDictionary*)viewPhotoRequest:(NSString*)macc_no
{
    return [self requestPhotoCmd:@"viewPhotoRequests" toMatch:macc_no];
}
//repair photo exchange,append update
+ (NSDictionary*)viewPhotoRequestNew:(NSString*)macc_no
{
    return [self requestPhotoCmdNew:@"viewPhotoRequests" toMatch:macc_no];
}


+ (NSDictionary*)exchangePhotoProcess:(NSString*)macc_no
{
    return [self requestPhotoCmd:@"exchangePhotoProcess" toMatch:macc_no];
}

+ (NSDictionary*)rejectPhotoExchange:(NSString*)macc_no
{
    return [self requestPhotoCmd:@"rejectPhotoExchange" toMatch:macc_no];
}

+ (NSDictionary*)sendPhotoReminder:(NSString*)macc_no
{
    return [self requestPhotoCmd:@"sendPhotoReminder" toMatch:macc_no];
}

+ (NSDictionary*)retrieveAllPhotos:(NSString*)match_id
{
 
    //--------------------------------------------------------------//
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    
    if( ![match_id isEqualToString:@""] )
        [params setObject:match_id forKey:@"match_id"];
    
    NSString*   serverPath = [NSString stringWithFormat:@"%@retrieveAllPhotos", WEBSERVICE_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}
+ (NSDictionary*)changePermission:(NSString*)status{
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:status forKey:@"status"];
    
    NSString*   serverPath = [NSString stringWithFormat:@"%@setPhotoStatus", WEBSERVICE_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}
+ (NSDictionary*)setThisToPrimary:(NSString*)photoID
{
   // NSString*   accNo = [eSyncronyAppDelegate sharedInstance].strAccNo;
    //NSString*   tokenKey = [eSyncronyAppDelegate sharedInstance].strTokenKey;
    
    //--------------------------------------------------------------//
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:photoID forKey:@"photo_id"];
    
    NSString*   serverPath = [NSString stringWithFormat:@"%@setThisToPrimary", WEBSERVICE_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)removeThisPhoto:(NSString*)photoID
{
   // NSString*   accNo = [eSyncronyAppDelegate sharedInstance].strAccNo;
   // NSString*   tokenKey = [eSyncronyAppDelegate sharedInstance].strTokenKey;
    
    //--------------------------------------------------------------//
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:photoID forKey:@"photo_id"];
    
    NSString*   serverPath = [NSString stringWithFormat:@"%@removeThisPhoto", WEBSERVICE_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)updatePhotoID:(NSString*)photoID
                      fileName:(NSString*)fileName
                     photoName:(NSString*)photoName
                   description:(NSString*)description
                       visible:(NSString*)visible
                        rating:(NSString*)rating
{
    //NSString*   accNo = [eSyncronyAppDelegate sharedInstance].strAccNo;
   // NSString*   tokenKey = [eSyncronyAppDelegate sharedInstance].strTokenKey;
    
    //--------------------------------------------------------------//
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:photoID forKey:@"photo_id"];
    [params setObject:fileName forKey:@"fileName"];
    [params setObject:photoName forKey:@"photo_name"];
    [params setObject:description forKey:@"description"];
    [params setObject:visible forKey:@"visible"];
    [params setObject:rating forKey:@"rating"];

    NSString*   serverPath = [NSString stringWithFormat:@"%@updatePhotoDetails", WEBSERVICE_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}

//================================================================//
//==========================    PayPal  ==========================//
//================================================================//

+ (NSDictionary*)addPaypal:(NSDictionary*)params
{
    NSString*   serverPath = [NSString stringWithFormat:@"%@add", WEBSERVICE_PAYMENT_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)packageBenefitsList:(NSDictionary*)params
{
   // NSString*   serverPath = [NSString stringWithFormat:@"%@packageItemListView", WEBSERVICE_PACKAGE_BASEURI];
    
    NSString*   serverPath = [NSString stringWithFormat:@"%@packageDetailsView", WEBSERVICE_PACKAGE_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
}
+ (NSDictionary*)packagePrice:(NSDictionary*)params
{
    NSString*   serverPath = [NSString stringWithFormat:@"%@packageListView", WEBSERVICE_PACKAGE_BASEURI];
    
    return [self requestToServer:serverPath withParams:params];
    
}

//=====================================================//
//========================= APN =======================//
//=====================================================//
+ (NSDictionary*)APNRegister:(NSDictionary*)params
{
    NSString*   serverPath = [NSString stringWithFormat:@"%@ios_regid_base", WEBSERVICE_APN_BASEURL];
    return [self requestToServer:serverPath withParams:params];
}

+ (NSDictionary*)MsgNum:(NSDictionary*)params
{
    NSString*   serverPath = [NSString stringWithFormat:@"%@setMsgNum", WEBSERVICE_APN_BASEURL];
    
    return [self requestToServer:serverPath withParams:params];
}
//=========================Reminder============================//
+ (NSDictionary*)Reminder
{    
    NSString*   serverPath = [NSString stringWithFormat:@"%@", WEBSERVICE_REMINDER];
    
    return [self requestToServer:serverPath withParams:nil];
}
//=========================Socket.io============================//
+(NSDictionary*)askSocketHistory{
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setValue:@"chatUnreadHistory" forKey:@"cmd"];
    
    NSString* serverPath = [NSString stringWithFormat:@"%@", WEBSERVICE_ASKSOCKETHISTORY];
    NSLog(@"%@",serverPath);
    return [self requestJsonToServerPost:serverPath withParams:params];
    
}

@end

#import <Foundation/Foundation.h>

#import "ErrorProc.h"

#if 0
#define HOST @"https://www.esynchrony.com/"//for live
#define SOCKETHOST @"www.esynchrony.com"
#define kPayPalEnvironment PayPalEnvironmentProduction

#else  
//#define HOST @"http://dev.esynchrony.com/"//for dev
//#define HOST @"http://52.74.193.53/"  //for test environment
#define HOST @"http://esy.qltx.com/new-design/public/api"
#define SOCKETHOST @"dev.esynchrony.com"
#define kPayPalEnvironment PayPalEnvironmentSandbox

#endif

#define CHATPORT                           3000
#define WEBSERVICE_REGISTER                HOST
#define WEBSERVICE_HEARUSFROM              HOST
#define WEBSERVICE_REMINDER                HOST
#define WEBSERVICE_MATCHTIPS               HOST 
#define WEBSERVICE_MATCHES_FAVORITE        HOST
#define WEBSERVICE_REMOVE_FAVORITED        HOST
#define WEBSERVICE_ADD_FAVORITED           HOST

#define WEBSERVICE_BASEURI                 HOST
#define WEBSERVICE_PROFILE_BASEURI         HOST
#define WEBSERVICE_PROFILE_BASEURI1        HOST

#define WEBSERVICE_MATCHES_BASEURI         HOST
#define WEBSERVICE_MATCHES_BASEURI1        HOST

#define WEBSERVICE_RENEWAL_POPUP           HOST

#define WEBSERVICE_DATES_BASEURI           HOST
#define WEBSERVICE_QNA                     HOST

#define WEBSERVICE_ACCOUNT_BASEURI         HOST

#define WEBSERVICE_PAYMENT_BASEURI         HOST
#define WEBSERVICE_PACKAGE_BASEURI         HOST

#define WEBSERVICE_UPLOAD_DOCS_BASEURI     HOST
#define WEBSERVICE_UPLOAD_PHOTO_BASEURI    HOST
#define WEBSERVICE_UPLOAD_NORMPHOTO_URL    HOST

#define WEBSERVICE_PHOTO_BASEURI            @"https://d2h5mhpjsoim0y.cloudfront.net/"

#define WEBSERVICE_IMAGE_BASEURL            @"https://d1694b94lbm1p.cloudfront.net/images/profileimages/"    // Food

#define WEBSERVICE_PAYPAL_BASEURL          HOST

#define WEBSERVICE_APN_BASEURL             HOST

#define WEBSERVICE_SELF_COORD              HOST

#define WEBSERVICE_SELF_COORDDATES         HOST
#define WEBSERVICE_SELF_CHECKCOORDDATES    HOST
#define WEBSERVICE_CHK_IPAddr              HOST

#define WEBSERVICE_TERMS                   HOST
#define WEBSERVICE_HELPSECTION             HOST
#define WEBSERVICE_EMAILCONTANCT           HOST
#define WEBSERVICE_HELPTOPICS              HOST
//#define WEBSERVICE_PHOTO_BASEURI            HOST @"images/members_photo/"
#define WEBSERVICE_ASKSOCKETHISTORY        HOST

//#define WEBSERVICE_REGISTER                HOST @"api/user/postAPI.php"
//#define WEBSERVICE_HEARUSFROM              HOST @"api/user/index.php/user/hearUsFromOptions"//hear us from
//#define WEBSERVICE_REMINDER                HOST @"api/user/index.php/user/versionReminder"//reminder
//#define WEBSERVICE_MATCHTIPS               HOST @"api/user/index.php/Matches/matchTips"//matchTips
//#define WEBSERVICE_MATCHES_FAVORITE        HOST @"api/user/index.php/Matches/viewFavotoreMatches"//matches favorited list
//#define WEBSERVICE_REMOVE_FAVORITED        HOST @"api/user/index.php/Matches/removeFavoriteMatch"//remove favorited match
//#define WEBSERVICE_ADD_FAVORITED           HOST @"api/user/index.php/Matches/addFavoriteMatch"//add favorite match
//
//#define WEBSERVICE_BASEURI                 HOST @"api/user/index.php/user/"
//#define WEBSERVICE_PROFILE_BASEURI         HOST @"api/user/index.php/profile/"
//#define WEBSERVICE_PROFILE_BASEURI1        HOST @"api/user/index.php/Personality/"//personality
//
//#define WEBSERVICE_MATCHES_BASEURI         HOST @"api/user/index.php/Matches/"
//#define WEBSERVICE_MATCHES_BASEURI1        HOST @"api/user/index.php/matches/"
//
//#define WEBSERVICE_RENEWAL_POPUP           HOST @"api/user/index.php/subscription/"//renewal
//
//#define WEBSERVICE_DATES_BASEURI           HOST @"api/user/index.php/dates/"
//#define WEBSERVICE_QNA                     HOST @"api/user/index.php/QnA/"
//
//#define WEBSERVICE_ACCOUNT_BASEURI         HOST @"api/user/index.php/Account/"
//
//#define WEBSERVICE_PAYMENT_BASEURI         HOST @"api/user/index.php/payment/"
//#define WEBSERVICE_PACKAGE_BASEURI         HOST @"api/user/index.php/Subscription/"
//
//#define WEBSERVICE_UPLOAD_DOCS_BASEURI     HOST @"docs_upload_mobile.php"
//#define WEBSERVICE_UPLOAD_PHOTO_BASEURI    HOST @"uploadprimaryphoto_mobile.php"
//#define WEBSERVICE_UPLOAD_NORMPHOTO_URL    HOST @"uploadnormalphoto_mobile.php"
//
//#define WEBSERVICE_PHOTO_BASEURI            @"https://d2h5mhpjsoim0y.cloudfront.net/"
//
//#define WEBSERVICE_IMAGE_BASEURL            @"https://d1694b94lbm1p.cloudfront.net/images/profileimages/"    // Food
//
//#define WEBSERVICE_PAYPAL_BASEURL          HOST @"api/user/index.php/payment/"
//
//#define WEBSERVICE_APN_BASEURL             HOST @"api/user/index.php/Push/"
//
//#define WEBSERVICE_SELF_COORD              HOST @"api/user/index.php/matches/selfCoord"//self_coordination
//
//#define WEBSERVICE_SELF_COORDDATES         HOST @"api/user/index.php/matches/selfCoordDates"
//#define WEBSERVICE_SELF_CHECKCOORDDATES    HOST @"api/user/index.php/matches/selfCoordChk"
//#define WEBSERVICE_CHK_IPAddr              HOST @"api/user/index.php/user/chkIPAddr"
//
//#define WEBSERVICE_TERMS                   HOST @"tos.php"
//#define WEBSERVICE_HELPSECTION             HOST @"helptop2.php?ans=7"
//#define WEBSERVICE_EMAILCONTANCT           HOST @"contact-us.php"
//#define WEBSERVICE_HELPTOPICS              HOST @"help.php"
////#define WEBSERVICE_PHOTO_BASEURI            HOST @"images/members_photo/"
//#define WEBSERVICE_ASKSOCKETHISTORY        HOST @"chat/chathandler.php?"

@interface UtilComm : NSObject

+ (float)getCurrencyValue:(float)value from:(NSString*)currencyFrom to:(NSString*)currencyTo;

+ (NSDictionary*)requestToServer:(NSString*)serverPath withParams:(NSDictionary*)params;
//===================================//
//vip 
+ (NSDictionary*)saveVipStoryInfo:(NSDictionary*)params;
//======================ip address==================================//150828
+(NSDictionary*)getCountryCode:(NSDictionary*)params;

//====================== Register =========================//
//request hearusfrom
+ (NSDictionary*)requestHearusfrom;
// register user
+ (NSDictionary*)registerWithParams:(NSDictionary*)params;
// user login
+ (NSDictionary*)loginWithParams:(NSDictionary*)params;
// login with facebook
+ (NSDictionary*)loginWithFaceBook:(NSDictionary*)params;
// check register step
+ (NSDictionary*)retrieveStepNo:(NSDictionary*)params;
// setBasic
+ (NSDictionary*)saveBasicInfo:(NSDictionary*)params;
// setLifestyle
+ (NSDictionary*)saveLifeStyleInfo:(NSDictionary*)params;
// setMatchingCriteria
+ (NSDictionary*)saveCriteriaInfo:(NSDictionary*)params;
// setPhysical
+ (NSDictionary*)savePhysicalInfo:(NSDictionary*)params;
// setInterestHobbies
+ (NSDictionary*)saveInterestInfo:(NSDictionary*)params;
// setTICS
+ (NSDictionary*)saveMyValue:(NSDictionary*)params;
// setReadiness
+ (NSDictionary*)saveReadinessInfo:(NSDictionary*)params;
// setCulture
+ (NSDictionary*)saveCultureInfo:(NSDictionary*)params;
// setOpenness
+ (NSDictionary*)saveOpennessInfo:(NSDictionary*)params;
// setAgreeableness
+ (NSDictionary*)saveAgreeableInfo:(NSDictionary*)params;
// setMoney
+ (NSDictionary*)saveMoneyInfo:(NSDictionary*)params;
// setAdmiration
+ (NSDictionary*)saveAdmiration:(NSDictionary*)params;
// saveIntimacy
+ (NSDictionary*)saveIntimacyInfo:(NSDictionary*)params;
// setDISC
+ (NSDictionary*)savePersonality:(NSDictionary*)params;
// setMyStory
+ (NSDictionary*)saveStoryInfo:(NSDictionary*)params;
//self_coord
+ (NSDictionary*)saveSelfcoord:(NSDictionary*)params;
//self_coordDates
+ (NSDictionary*)saveSelfcoorddates:(NSDictionary*)params;
//self_Check_coordDates
+ (NSDictionary*)CheckSelfcoorddates:(NSDictionary*)params;
//========================= Account Verify =======================//

// checkVerify
+ (NSDictionary*)checkAccountVerify;

// smsVerify
+ (NSDictionary*)smsVerify:(NSDictionary*)params;

// smsVerifyCode
+ (NSDictionary*)smsVerifyCode:(NSDictionary*)params;

// nricVerify
+ (NSDictionary*)nricVerify:(NSDictionary*)params;
//nricVerifyNew
+ (NSDictionary*)nricVerifyNew:(NSDictionary*)params;//quiz nric repair 0119

//docCertVerify
+ (NSDictionary*)getDocCertVerifyStatus;

//uploadDocImage
+ (NSDictionary*)uploadDocImage:(UIImage*)imgPhoto withType:(int)nDocType;

//retrievePrimaryPhotoFilename
+ (NSDictionary*)retrievePrimaryPhotoFilename;

//upload primary photo
+ (NSDictionary*)uploadPrimaryPhoto:(UIImage*)imgPhoto;

//upload normal photo
+ (NSDictionary*)uploadNormalPhoto:(UIImage*)imgPhoto;

//============================ Profile ==========================//

+ (NSDictionary*)getProfileWithType:(NSString*)type;

// get common interest
+ (NSDictionary*)getCommonInterest:(NSString*)macc_no;

// view match profile
+ (NSDictionary*)viewMatchProfile:(NSString *)macc_no Type:(NSString*)strType;

// compatibilityPreview
+ (NSDictionary*)compatibilityPreview:(NSString*)macc_no;

// compatibilityViewDetail
+ (NSDictionary*)compatibilityViewDetail:(NSString *)macc_no;

// MatchSetting Save
+ (NSDictionary*)updateMatchingCriteria:(int)age
                                 height:(int)height
                               distance:(int)distance
                               religion:(int)religion
                              ethnicity:(int)ethnicity
                                 income:(int)income
                              education:(int)education
                               children:(int)children
                                mstatus:(int)mstatus
                                  smoke:(int)smoke
                                  drink:(int)drink
                              excercise:(int)excersise;

// MatchSetting Load
+ (NSDictionary*)viewMatchingCriteria;

// update basic info
+ (NSDictionary*)updateBasicInfo:(NSDictionary*)params;

// update life style
+ (NSDictionary*)updateLifestyle:(NSDictionary*)params;

// update Appearance
+ (NSDictionary*)updateAppearance:(NSDictionary*)params;

// update Interest
+ (NSDictionary*)updateInterest:(NSDictionary*)params;
//============================ Account ==========================//

// View Account Info
+ (NSDictionary*)viewAccount;

// Update Account
+ (NSDictionary*)updateAccount:(NSString*)nName dob:(NSString*)dob handphone:(NSString*)handphone;

// Update Password
+ (NSDictionary*)updatePassword:(NSString*)strPwd;

// Load Report
+ (NSDictionary*)personalityTypeReport:(NSString*)maccNo;

// forget password
+ (NSDictionary*)retrieveNewPassword:(NSString*)strEmail;
//============================ Matches ==========================//
+ (NSDictionary*)removeFavoritedMatch:(NSString *)favorNum;//remove favorite matches
+ (NSDictionary*)addFavoritedMatch:(NSString *)favorNum;//add favorite matches
//matched favorited
+ (NSDictionary*)loadMatchesFavoritedInfo:(NSInteger)page;
//matchTips
+ (NSDictionary*)matchTips;
//renewal
+ (NSDictionary*)renewalPopup;
+ (NSDictionary*)retrieveNumberMatches;
+ (NSDictionary*)loadMatchesInfoWithType:(NSString*)type withPage:(NSInteger)page;
+ (NSDictionary*)loadMatchesInfoWithTypeNew:(NSString*)type;
+ (NSDictionary*)loadMatchesInfoWithType:(NSString*)type;
+ (NSDictionary*)loadMatchesInfoWithNewType:(NSString*)type Page:(NSInteger)page View_status:(NSString*)view_status;

// retrievePrimaryPhotoFilename
+ (NSDictionary*)retrieveMatchesPhotoFilename:(NSString*)matchAccNo;

// approve to match
+ (NSDictionary*)approveToMatch:(NSString*)macc_no;

// decline to match
+ (NSDictionary*)declineToMatch:(NSString*)macc_no reasonId:(int)reasonID comment:(NSString*)comment;

// send reminder
+ (NSDictionary*)sendReminderToMatch:(NSString*)macc_no;

// get match reports
+ (NSDictionary*)getMatchReports;

//=============================  Dates ==========================//

// load date number and package
+ (NSDictionary*)retrieveNumberDatesAndPackage;

// LoadNumberDates
+ (NSDictionary*)retrieveNumberDates;

// Load Dates
+ (NSDictionary*)viewDates:(NSString*)type;

// Load Dates Report
+ (NSDictionary*)viewDatesReport;

//============================ Feedback =========================//

// receive feedback info
+ (NSDictionary*)giveFeedback:(NSString*)macc_no Param1:(NSString*)positive Param2:(NSString*)negative Param3:(NSString*)rest_rating Param4:(NSString*)first_imp1 Param5:(NSString*)first_imp2 Param6:(NSString*)first_imp3 Param7:(NSString*)during_date1 Param8:(NSString*)during_date2 Param9:(NSString*)during_date3 Param10:(NSString*)goodbyes1 Param11:(NSString*)goodbyes2 Param12:(NSString*)overal_txtarea Param13:(NSString*)overall_exp_rating Param14:(NSString*)afterthoughts1 Param15:(NSString*)afterthoughts2 Param16:(NSString*)followupID Param17:(NSString*)additional_feedback Param18:(NSString*)exchange_mail;

+ (NSDictionary*)newGiveFeedback:(NSDictionary*)params;
+ (NSDictionary*)updateTmpdate:(NSDictionary*)params;
//==========================   Q n A    ==========================//

+ (NSDictionary*)retrievePendingQuestion:(NSString*)macc_no;
+ (NSDictionary*)retrievePreviousAnswer:(NSString*)qStr;
+ (NSDictionary*)loadAskedQuestions:(NSString*)macc_no;

+ (NSDictionary*)loadFavouriteQuestions:(NSString*)macc_no andCategory:(int)category;
+ (NSDictionary*)loadNormalQuestions:(NSString*)macc_no andCategory:(int)category;

//+ (NSDictionary*)loadFavouriteQuestions:(NSString*)macc_no;
//
//+ (NSDictionary*)loadNormalQuestions:(NSString*)macc_no;

+ (NSDictionary*)addQuestionToFavourite:(NSString*)questionID;

+ (NSDictionary*)removeQuestionFromFavourite:(NSString*)questionID;

+ (NSDictionary*)sendQuestions:(NSString*)strQuestions toMatch:(NSString*)macc_no;

+ (NSDictionary*)sendAnswer:(int)nAnswer toQuestions:(int)nQuestion toMatch:(NSString*)macc_no;

+ (NSDictionary*)viewAnswers:(NSString*)macc_no;

//==========================   Photo    ==========================//
+ (NSDictionary*)viewPhotoSingle:(NSString*)macc_no;
+ (NSDictionary*)viewPhotoRequest:(NSString*)macc_no;
+ (NSDictionary*)viewPhotoRequestNew:(NSString*)macc_no;//append update

+ (NSDictionary*)exchangePhotoProcess:(NSString*)macc_no;

+ (NSDictionary*)rejectPhotoExchange:(NSString*)macc_no;

+ (NSDictionary*)sendPhotoReminder:(NSString*)macc_no;

+ (NSDictionary*)retrieveAllPhotos:(NSString*)match_id;

+ (NSDictionary*)changePermission:(NSString*)status;

+ (NSDictionary*)setThisToPrimary:(NSString*)photoID;

+ (NSDictionary*)removeThisPhoto:(NSString*)photoID;

+ (NSDictionary*)updatePhotoID:(NSString*)photoID
                      fileName:(NSString*)fileName
                     photoName:(NSString*)photoName
                   description:(NSString*)description
                       visible:(NSString*)visible
                        rating:(NSString*)rating;

//==========================   PayPal   ==========================//
+ (NSDictionary*)addPaypal:(NSDictionary*)params;
+ (NSDictionary*)packageBenefitsList:(NSDictionary*)params;
+ (NSDictionary*)packagePrice:(NSDictionary*)params;

//==================================================================//
//========================= APN=====================================//
//==================================================================//
+ (NSDictionary*)APNRegister:(NSDictionary*)params;
+ (NSDictionary*)MsgNum:(NSDictionary*)params;
//=========================Reminder============================//
+ (NSDictionary*)Reminder;

//=========================Socket.io============================//
+(NSDictionary*)askSocketHistory;

@end

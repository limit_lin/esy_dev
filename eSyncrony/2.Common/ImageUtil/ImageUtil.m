
#import "ImageUtil.h"
#import "EncodeUtil.h"
#import "FileUtil.h"
#import "UIImage+Utils.h"
#import "UIImageView+WebCache.h"//append 151223

@implementation ImageUtil

+ (NSString*)extractFileNameFromURL:(NSString*)imgURL
{
    if( imgURL == nil || [imgURL isEqualToString:@""] )
        return @"";
    
    NSRange     range = [imgURL rangeOfString:@"/" options:NSBackwardsSearch];
    NSString    *fileName = nil;
    
    if( range.length != 0 )
        fileName = [imgURL substringFromIndex:range.location+1];
    else
        fileName = imgURL;
    
    return fileName;
}

+(UIImage *) loadImageFromURL:(NSString *)fileURL
{
    
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
    
    
//   NSData *data = [[NSData alloc]initWithContentsOfURL:[NSURL URLWithString:fileURL]];
//   UIImage * result = [[UIImage alloc]initWithData:data];
//    if (data ! = nil)
//    {
//        dispatch_async(dispatch_get_main_queue(), ^{
//           
//        });
//    }
//     return result;

    UIImage * result;
    NSURL *url = [NSURL URLWithString:fileURL];
    NSData * data = [NSData dataWithContentsOfURL:url];
    result = [UIImage imageWithData:data];
//    NSLog(@"data = %@ result =%@",data,result);
    return result;
    
}

+(void) saveImage:(UIImage *)image withFileName:(NSString *)imageName ofType:(NSString *)extension inDirectory:(NSString *)directoryPath
{
    if( [[extension lowercaseString] isEqualToString:@"png"] )
    {
        [UIImagePNGRepresentation(image) writeToFile:[directoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@", imageName, @"png"]] options:NSAtomicWrite error:nil];
    }
    else if( [[extension lowercaseString] isEqualToString:@"jpg"] || [[extension lowercaseString] isEqualToString:@"jpeg"] )
    {
        [UIImageJPEGRepresentation(image, 1.0) writeToFile:[directoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@", imageName, @"jpg"]] options:NSAtomicWrite error:nil];
    }
}

+ (UIImage *)loadImage:(NSString *)fileName ofType:(NSString *)extension inDirectory:(NSString *)directoryPath
{
    UIImage * result = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@.%@", directoryPath, fileName, extension]];
    
    return result;
}

+ (NSString*)getLocalFileTitleFromURL:(NSString*)url
{
    if( url == nil || [url isEqualToString:@""] )
        return @"";
    
    NSRange     range = [url rangeOfString:@"/" options:NSBackwardsSearch];
    NSString    *fileID = nil;
    
    if( range.length != 0 )
    {
        if( url.length-range.location < 30 )
        {
           if( range.location < 10 )
               range.location = 0;
           else
               range.location -= 10;
        }

        fileID = [url substringFromIndex:range.location+1];
    }
    else
        fileID = url;
    
    if( [fileID length] < 10 )
    {
        NSString    *fileName = [EncodeUtil stringToHexArray:fileID];
        return fileName;
    }
    else
    {
        range = [fileID rangeOfString:@"." options:NSBackwardsSearch];
    
        NSString    *fileName;
        if( range.length != 0 )
            fileName = [fileID substringToIndex:range.location];
        else
            fileName = fileID;

        fileName = [fileName stringByReplacingOccurrencesOfString:@"/" withString:@""];

        return fileName;
    }
}

+ (NSString*)getExistLocalImageFilePathFromURL:(NSString*)url
{
    
    NSString*   fileTitle = [ImageUtil getLocalFileTitleFromURL:url];
    if( fileTitle == nil || [fileTitle isEqualToString:@""] )
        return nil;

    NSString    *filePath = [FileUtil getFullPathOfFile:fileTitle ofType:@"jpg"];
    
    if( [FileUtil isFileExits:filePath] )
        return filePath;

    return nil;
}

+ (NSString*)loadImagePathFromURL:(NSString*)url
{
    
    NSString*   fileTitle = [ImageUtil getLocalFileTitleFromURL:url];
    if( fileTitle == nil || [fileTitle isEqualToString:@""] )
        return @"";
    
    NSString    *folderPath = [FileUtil getDirectoryPath];
//    /var/mobile/Containers/Data/Application/E1A2D183-6C79-4565-8018-28BFD15E45AC/Library/Caches
    
//    imageURL == http://d1694b94lbm1p.cloudfront.net/images/profileimages/Food/Japanese.jpg imagePath == /var/mobile/Applications/1403CBFA-67A7-41D1-896D-69FA45BFF4EE/Library/Caches/agesFoodJapanese.jpg
//    imageURL == http://d1694b94lbm1p.cloudfront.net/images/profileimages/OwnPets/Dog.jpg imagePath == /var/mobile/Containers/Data/Application/5B044FF6-1EBA-4125-8BBC-8B690ACB338B/Library/Caches/sOwnPetsDog.jpg
    
    NSString    *filePath = [FileUtil getFullPathOfFile:fileTitle ofType:@"jpg"];
    
    if( [FileUtil isFileExits:filePath] )
        return filePath;

    UIImage *image = [ImageUtil loadImageFromURL:url];
    [ImageUtil saveImage:image withFileName:fileTitle ofType:@"jpg" inDirectory:folderPath];

//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
//        
//       NSURL *fileURL= [NSURL URLWithString:url];
//       NSData *data = [[NSData alloc]initWithContentsOfURL:fileURL];
//       UIImage *image = [[UIImage alloc]initWithData:data];
//        if (data != nil)
//        {
//            dispatch_async(dispatch_get_main_queue(), ^{
////                imageView.image = image;
//            });
//        }
//        [ImageUtil saveImage:image withFileName:fileTitle ofType:@"jpg" inDirectory:folderPath];
//        NSLog(@"image == %@",image);
//        });

    
    return filePath;
}

+ (void)saveImage:(UIImage*)image asURLFile:(NSString*)urlPath
{
    if( urlPath == nil || [urlPath isEqualToString:@""] )
        return;
    
    NSRange     range = [urlPath rangeOfString:@"/" options:NSBackwardsSearch];
    NSString    *fileID = nil;
    
    if( range.length != 0 )
        fileID = [urlPath substringFromIndex:range.location+1];
    else
        fileID = urlPath;
    
#if 0
    NSString    *fileName = [EncodeUtil stringToHexArray:fileID];
#else
    range = [fileID rangeOfString:@"." options:NSBackwardsSearch];
    
    NSString    *fileName;
    if( range.length != 0 )
        fileName = [fileID substringToIndex:range.location];
    else
        fileName = fileID;
#endif
    
    NSString    *folderPath = [FileUtil getDirectoryPath];
    NSString    *filePath = [FileUtil getFullPathOfFile:fileName ofType:@"jpg"];
    
    if( [FileUtil isFileExits:filePath] )
        return;
    
    [ImageUtil saveImage:image withFileName:fileName ofType:@"jpg" inDirectory:folderPath];
}

+ (void)downloadImageToClientFromURL:(NSString*)url
{
    [ImageUtil loadImagePathFromURL:url];
}

+ (UIImage*) loadImageFromClientByURL:(NSString*)url
{
    NSString*   filePath = [ImageUtil loadImagePathFromURL:url];
    UIImage*    image = [UIImage imageWithContentsOfFile:filePath];
    
    return image;
}

+ (NSData*)getRawImageDataFromImage:(UIImage*)image ofType:(NSString*)type {
        
    if( [type isEqualToString:@"png"] || [type isEqualToString:@"PNG"] ) {
        return [NSData dataWithData:UIImagePNGRepresentation(image)];
    }
    
    return [NSData dataWithData:UIImageJPEGRepresentation(image, 1.0)];
}

//===============================================================================================//

+ (UIImage*)loadThumnailImageForVideoID:(NSString*)videoID
{
    NSString    *filePath = [FileUtil getFullPathOfFile:videoID ofType:@"jpg"];
    
    if( ![FileUtil isFileExits:filePath] )
        return nil;
    
    return [UIImage imageWithContentsOfFile:filePath];
}

+ (void)saveThumnailImage:(UIImage*)image ForVideoID:(NSString*)videoID
{
    NSString    *folderPath = [FileUtil getDirectoryPath];

    [ImageUtil saveImage:image withFileName:videoID ofType:@"jpg" inDirectory:folderPath];
}

@end


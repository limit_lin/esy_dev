
#import "InputBaseViewController.h"

@interface InputBaseViewController ()
{
    CGFloat     _fOrgViewFrameTop;
    BOOL        _bInitOrgYPos;
}

@end

@implementation InputBaseViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _bInitOrgYPos = FALSE;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if( _bInitOrgYPos == TRUE )
        return;
    
    _fOrgViewFrameTop = self.view.frame.origin.y;
    _bInitOrgYPos = TRUE;
}

- (void)restoreViewPosition
{
    CGRect  frame = self.view.frame;
    frame.origin.y = _fOrgViewFrameTop;
    self.view.frame = frame;
}

@end

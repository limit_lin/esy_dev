//
//  HearSelectViewController.m
//  eSyncrony
//
//  Created by iosdeveloper on 15/4/24.
//  Copyright (c) 2015年 WonMH. All rights reserved.
//

#import "HearSelectViewController.h"
#import "RadioBoxCell.h"

@interface HearSelectViewController ()

@end

@implementation HearSelectViewController

+ (id)createWithHearList:(NSArray*)dataList selectIndex:(int)nSelIndex withTag:(int)tag
{
    HearSelectViewController* controller = [[HearSelectViewController alloc] initWithHearList:dataList selectIndex:nSelIndex withTag:tag];
    return controller;
}

- (id)initWithHearList:(NSArray*)dataList selectIndex:(int)nSelIndex withTag:(int)tag
{
    self = [self initWithNibName:@"HearSelectViewController" bundle:nil];
    //NSLog(@"dataList==%@",dataList);
    _dataList = dataList;
    _nSelectedIdx = nSelIndex;
    _nTag = tag;
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
     tbleView.layer.cornerRadius = 5;
    /*
     UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnTableView:)];
     tapRecognizer.cancelsTouchesInView = NO;
     [self.view addGestureRecognizer:tapRecognizer];*/
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    CGRect  rect = tbleView.frame;
    
    rect.size.height = 45*MIN(7, [_dataList count]);
    rect.origin.y = (self.view.frame.size.height - rect.size.height)/2;
    
    tbleView.frame = rect;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //NSLog(@"_dataList.count==%d",_dataList.count);
    return [_dataList count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RadioBoxCell *cell;
    
    NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"RadioBoxCell" owner:nil options:nil];
    cell = [arr objectAtIndex:0];
    
    cell.reasonLabel.text = [_dataList objectAtIndex:indexPath.row];
    cell.reasonLabel.adjustsFontSizeToFitWidth = YES;
    cell.reasonLabel.adjustsLetterSpacingToFitWidth = YES;
    UIImage *img;
    
    if( _nSelectedIdx == indexPath.row )
        img = [UIImage imageNamed:@"btnRadio_p"];
    else
        img = [UIImage imageNamed:@"btnRadio_n"];
    
    [cell.radioImgView initWithImage:img];
    
    return cell;
}

- (void)procTblSelectCell
{
    [self.view removeFromSuperview];
    
    if( [self.delegate respondsToSelector:@selector(HearSelectView:didSelectItem:withTag:)] )
        [self.delegate HearSelectView:self didSelectItem:(int)_nSelectedIdx withTag:(int)_nTag];
    //[createAccountView initSetting1];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _nSelectedIdx = indexPath.row;
    [tableView reloadData];
    
    //createAccountView.selectedIdx = indexPath.row;
    
    [self performSelector:@selector(procTblSelectCell) withObject:nil afterDelay:0.2];
}

- (void)dealloc
{
    [tbleView release];
    [super dealloc];
}
@end


#import <UIKit/UIKit.h>

@interface InputBaseViewController : GAITrackedViewController<UIAlertViewDelegate> 

- (void)restoreViewPosition;

@end

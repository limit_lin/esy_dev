//
//  HearSelectViewController.h
//  eSyncrony
//
//  Created by iosdeveloper on 15/4/24.
//  Copyright (c) 2015年 WonMH. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HearSelectViewController;
@protocol HearSelectViewDelegate <NSObject>
@optional

- (void)HearSelectView:(HearSelectViewController*)dataSelectView didSelectItem:(int)itemIndex withTag:(int)tag;

@end

@interface HearSelectViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UITableView    *tbleView;
    
    NSInteger   _nTag;
    NSInteger   _nSelectedIdx;
    NSArray*    _dataList;
}

+ (id)createWithHearList:(NSArray*)dataList selectIndex:(int)nSelIndex withTag:(int)tag;

@property (nonatomic, assign) id<HearSelectViewDelegate> delegate;

@end



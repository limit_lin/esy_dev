
#import <UIKit/UIKit.h>


@interface RadioBoxCell : UITableViewCell
{
    
}

@property(strong,nonatomic) IBOutlet UIImageView *radioImgView;
@property(strong,nonatomic) IBOutlet UILabel *reasonLabel;

@end

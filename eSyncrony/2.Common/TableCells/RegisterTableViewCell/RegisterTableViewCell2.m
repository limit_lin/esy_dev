
#import "RegisterTableViewCell2.h"
#import "Global.h"

@implementation RegisterTableViewCell2

@synthesize  imgBack;
@synthesize  imgHeart;
@synthesize  lbContent;
@synthesize  selectedItemArray;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        bFirst = YES;
        // Initialization code
        //_orgTxtColor = lbContent.textColor;
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    if(bFirst != YES)
    {
        _orgTxtColor = lbContent.textColor;
        bFirst = YES;
    }

    [super setSelected:selected animated:animated];
    
    [imgHeart setHidden:!selected];
    
    if(selected)
    {
        lbContent.textColor = [UIColor whiteColor];
    }
    else
    {
        lbContent.textColor = TITLE_TEXT_COLLOR;
    }
    // Configure the view for the selected state
}

-(void)dealloc
{
    [imgBack release];
    [imgHeart release];
    [lbContent release];
    [selectedItemArray release];

    [super dealloc];
}

@end
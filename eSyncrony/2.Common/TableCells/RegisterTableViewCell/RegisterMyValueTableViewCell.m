
#import "RegisterMyValueTableViewCell.h"

@implementation RegisterMyValueTableViewCell

@synthesize  imgBack;
@synthesize  imgHeart;
@synthesize  lbContent;
@synthesize  lbNumber;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    [imgHeart setHidden:!selected];
    [lbNumber setHidden:!selected];
    
    if( selected )
        lbContent.textColor = [UIColor whiteColor];
    else
        lbContent.textColor = [UIColor colorWithRed:0 green:0.5 blue:0.5 alpha:1.0f];
    
    lbNumber.textColor = [UIColor redColor];
}

- (void)setNumber:(int)aNum
{
    if( aNum >= 0 )
    {
        lbNumber.text = [NSString stringWithFormat:@"%d",aNum+1];
        lbNumber.hidden = NO;
    }
    else
    {
        lbNumber.text = @"";
        lbNumber.hidden = YES;
    }
}

- (void)hide:(BOOL)flag
{
    [imgHeart setHidden:!flag];
    [lbNumber setHidden:!flag];
    
    if(flag)
        lbContent.textColor = [UIColor whiteColor];
    else
        lbContent.textColor = [UIColor blueColor];
}

@end

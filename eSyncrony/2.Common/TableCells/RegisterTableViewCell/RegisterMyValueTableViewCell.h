
#import <UIKit/UIKit.h>

@interface RegisterMyValueTableViewCell : UITableViewCell

@property(strong,nonatomic) IBOutlet UIImageView*           imgBack;
@property(strong,nonatomic) IBOutlet UIImageView*           imgHeart;
@property(strong,nonatomic) IBOutlet UILabel*               lbNumber;
@property(strong,nonatomic) IBOutlet UILabel*               lbContent;

-(void)setNumber:(int)aNum;
-(void)hide:(BOOL)flag;
@end


#import <UIKit/UIKit.h>

@interface RegisterTableViewCell : UITableViewCell
{
    UIColor*    _orgTxtColor;
    BOOL        bFirst;
}
@property(strong, nonatomic) IBOutlet UIImageView *imgBack;
@property(strong, nonatomic) IBOutlet UIImageView *imgHeart;
@property(strong, nonatomic) IBOutlet UILabel *lbContent;
@property(strong, nonatomic) NSMutableArray *selectedItemArray;

@end

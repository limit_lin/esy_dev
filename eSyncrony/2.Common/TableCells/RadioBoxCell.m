//
//  RadioBoxCell

#import "RadioBoxCell.h"

@implementation RadioBoxCell

@synthesize radioImgView, reasonLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    self.reasonLabel.adjustsFontSizeToFitWidth = YES;
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

-(void)dealloc
{
    [radioImgView release];
    [reasonLabel release];
    
    [super dealloc];
}

@end

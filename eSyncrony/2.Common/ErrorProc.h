
#import <Foundation/Foundation.h>
//#define ERROR_NEED_RELOGIN NSLocalizedString(@"We have encountered a problem, please relogin.",@"ERROR_Not_Login")
#define ERROR_NEED_RELOGIN NSLocalizedString(@"We are unable to process your request at this moment. Please try again later.",@"ERROR_Not_Login")

#define ERROR_EMPTY_STRING NSLocalizedString(@"Empty string found in one or more parameters.",@"ERROR_EMPTY_STRING")
#define ERROR_Invalid_Email_Format NSLocalizedString(@"Invalid email format or failed domain name validation.",@"ERROR_Invalid_Email_Format")
#define ERROR_Not_Login NSLocalizedString(@"User has not logged in.",@"ERROR_Not_Login")
#define ERROR_Session_Expired NSLocalizedString(@"Session Expired. Please re-login your account.",@"ERROR_Session_Expired")
#define ERROR_Account_Not_Exist NSLocalizedString(@"Account does not exist.",@"ERROR_Account_Not_Exist")
#define ERROR_Unknown NSLocalizedString(@"Unknown Error",@"ERROR_Unknown")
#define ERROR_Internet NSLocalizedString(@"Unable to connect to server.\nCheck your internet connection.",@"ERROR_Internet")
#define ERROR_Authentication_Failure NSLocalizedString(@"Authentication Failure",@"ERROR_Authentication_Failure")

@interface ErrorProc : NSObject

+ (void)alertMessage:(NSString*)errorMessage withTitle:(NSString*)title;

+ (void)alertAuthenFailureMessage;

+ (void)alertToCheckInternetStateTitle:(NSString*)title;

+ (void)alertRegisterError:(NSString *)error;

+ (void)alertLoginError:(NSString *)error;

+ (void)alertRetrieveStepError:(NSString *)error;

+ (void)alertSetBasicError:(NSString *)error;

+ (void)alertSetLifeStyleError:(NSString *)error;

+ (void)alertSetMatchingCriteriaError:(NSString *)error;

+ (void)alertSetPhysicalError:(NSString *)error;

+ (void)alertSetTICSError:(NSString *)error;

+ (void)alertSetReadinessError:(NSString *)error;

+ (void)alertSetCultureError:(NSString *)error;

+ (void)alertSetOpennessError:(NSString *)error;

+ (void)alertSetAgreeablenessError:(NSString *)error;

+ (void)alertSetMoneyError:(NSString *)error;

+ (void)alertSetAdmirationError:(NSString *)error;

+ (void)alertSetDISCError:(NSString *)error;

+ (void)alertSetMyStoryError:(NSString *)error;

//=====================================================================//

+ (void)alertDefaultError:(NSString*)error withTitle:(NSString*)title;

+ (void)alertError:(NSString *)error withTitle:(NSString*)title;

@end


#import "HelvMdCnTextView.h"

@implementation HelvMdCnTextView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    NSArray *languages = [NSLocale preferredLanguages];
    NSString *currentLanguage = [languages objectAtIndex:0];
    if (![currentLanguage hasPrefix:@"zh-Hant"])
        self.font = [UIFont fontWithName:@"HelveticaNeueLTStd-MdCn" size:self.font.pointSize];
}

@end

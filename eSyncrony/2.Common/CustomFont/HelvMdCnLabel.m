
#import "HelvMdCnLabel.h"
#import "Global.h"

@implementation HelvMdCnLabel

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    NSArray *languages = [NSLocale preferredLanguages];
    NSString *currentLanguage = [languages objectAtIndex:0];

    if (!([currentLanguage hasPrefix:@"zh-Hant"]||[currentLanguage hasPrefix:@"zh-HK"])){//repair 151224
         self.font = [UIFont fontWithName:@"HelveticaNeueLTStd-MdCn" size:self.font.pointSize];
    }
//    self.clipsToBounds = NO;
    [self setFrame:self.frame];
}


- (void)setFrame:(CGRect)frame
{
    
    if( SYSTEM_VERSION_LESS_THAN(@"7.0") )
    {
        frame.origin.y += 2;
        frame.size.height += 4;
    }

    [super setFrame:frame];
}

@end

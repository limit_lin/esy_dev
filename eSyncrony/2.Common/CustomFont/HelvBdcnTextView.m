
#import "HelvBdcnTextView.h"

@implementation HelvBdcnTextView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        NSArray *languages = [NSLocale preferredLanguages];
        NSString *currentLanguage = [languages objectAtIndex:0];
        if (!([currentLanguage hasPrefix:@"zh-Hant"]||[currentLanguage hasPrefix:@"zh-HK"]))
            self.font = [UIFont fontWithName:@"helveticaneueltstd-bdcn" size:self.font.pointSize];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    NSArray *languages = [NSLocale preferredLanguages];
    NSString *currentLanguage = [languages objectAtIndex:0];
//    if (![currentLanguage hasPrefix:@"zh-Hant"])
    if (!([currentLanguage hasPrefix:@"zh-Hant"]||[currentLanguage hasPrefix:@"zh-HK"]))
        self.font = [UIFont fontWithName:@"helveticaneueltstd-bdcn" size:self.font.pointSize];
}

@end

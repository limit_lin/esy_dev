
#import "UINextButton.h"
#import "UIImage+Utils.h"
#import "Global.h"

@implementation UINextButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    UIEdgeInsets    contentInsets = UIEdgeInsetsMake(0, 20, 0, 40);

    UIImage *normalImage = [UIImage imageNamed:@"nextblue_button_n.png"];
    UIImage *highlightImage = [UIImage imageNamed:@"nextblue_button_p.png"];
    
    normalImage = [normalImage resizableImageWithCapInsets:contentInsets resizingMode:UIImageResizingModeStretch];
    
    highlightImage = [highlightImage resizableImageWithCapInsets:contentInsets resizingMode:UIImageResizingModeStretch];

    CGSize  size = normalImage.size;
    CGSize  stdSize = self.frame.size;
    
    size.width = size.height*stdSize.width/stdSize.height;

    normalImage = [normalImage renderAtSize:size];
    highlightImage = [highlightImage renderAtSize:size];

    [self setBackgroundImage:normalImage forState:UIControlStateNormal];
    [self setBackgroundImage:highlightImage forState:UIControlStateHighlighted];
    
    NSArray *languages = [NSLocale preferredLanguages];
    NSString *currentLanguage = [languages objectAtIndex:0];
    

    if (!([currentLanguage hasPrefix:@"zh-Hant"]||[currentLanguage hasPrefix:@"zh-HK"])){
        self.titleLabel.font = [UIFont fontWithName:@"HelveticaNeueLTStd-MdCn" size:self.titleLabel.font.pointSize];
    }
    
    if( SYSTEM_VERSION_LESS_THAN(@"7.0") )
        self.titleEdgeInsets = UIEdgeInsetsMake(6, 0, 0, 0);
    else
        self.titleEdgeInsets = UIEdgeInsetsMake(2, 0, 0, 0);
}

@end

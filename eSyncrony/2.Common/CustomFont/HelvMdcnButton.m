
#import "HelvMdcnButton.h"
#import "Global.h"

@implementation HelvMdcnButton

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    NSArray *languages = [NSLocale preferredLanguages];
    NSString *currentLanguage = [languages objectAtIndex:0];
    if (!([currentLanguage hasPrefix:@"zh-Hant"]||[currentLanguage hasPrefix:@"zh-HK"])) {
        self.titleLabel.font = [UIFont fontWithName:@"HelveticaNeueLTStd-MdCn" size:self.titleLabel.font.pointSize];
    }


    if( SYSTEM_VERSION_LESS_THAN(@"7.0") )
        self.titleEdgeInsets = UIEdgeInsetsMake(6, 0, 0, 0);
    else
        self.titleEdgeInsets = UIEdgeInsetsMake(2, 0, 0, 0);

}

@end

//
//  SuccessStoryCell.m
//  eSyncrony
//
//  Created by iosdeveloper on 14-6-19.
//  Copyright (c) 2014年 WonMH. All rights reserved.
//

#import "SuccessStoryCell.h"
#import <FacebookSDK/FacebookSDK.h>

#import "eSyncronyAppDelegate.h"
//https://developers.facebook.com/docs/reference/opengraph/action-type/og.likes

@implementation SuccessStoryCell

- (void)awakeFromNib
{
    // Initialization code
    self.lineView.layer.shadowOffset = CGSizeMake(1,2);
    self.lineView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.lineView.layer.shadowOpacity = 0.6;
//    self.btnLike.objectID = @"http://shareitexampleapp.parseapp.com/photo1/";
    
//    _btnLike.likeControlStyle = FBSDKLikeControlStyleBoxCount;
//    _btnLike.objectID = [NSString stringWithFormat:@"https://d1694b94lbm1p.cloudfront.net/images/testi/%@",self.likeImage];
//    _btnLike.objectID = [NSString stringWithFormat:@"https://www.esynchrony.com/success-story-facebook.php?c=%@",self.likeImage];
//    NSLog(@"%@",_btnLike.objectID);
//    _btnLike.foregroundColor = [UIColor redColor];
//    _btnLike.objectID = @"https://www.esynchrony.com/success-story-facebook.php?c=testithumb1";
//    [NSString stringWithFormat:@"https://www.esynchrony.com/success-story-facebook.php?c=%@",self.likeImage];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
//    _btnLike.objectID = [NSString stringWithFormat:@"https://www.esynchrony.com/success-story-facebook.php?c=%@",self.likeImage];
//    NSLog(@"%@",_btnLike.objectID);
    // Configure the view for the selected state
}

- (void)dealloc {
    [_lblName release];
    [_lineView release];
    [_storyImage release];
    [_shareImage release];
    [_shareDescription release];
    [super dealloc];
}

- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params[kv[0]] = val;
    }
    return params;
}

- (IBAction)didClickShare:(id)sender {
    // Check if the Facebook app is installed and we can present the share dialog
    FBLinkShareParams *params = [[FBLinkShareParams alloc] init];
    params.link = [NSURL URLWithString:@"http://www.esynchrony.com/success-story.php"];
    params.name = NSLocalizedString(@"Loving Couples from eSynchrony", @"FBShare") ;
    params.caption = @"www.esynchrony.com";
    params.picture = [NSURL URLWithString:[NSString stringWithFormat:@"https://d1694b94lbm1p.cloudfront.net/images/testi/%@",self.shareImage]];
    params.linkDescription = self.shareDescription;
   
    // If the Facebook app is installed and we can present the share dialog
    if ([FBDialogs canPresentShareDialogWithParams:params]) {
        
        // Present share dialog
        [FBDialogs presentShareDialogWithParams:params
                                    clientState:nil
                                        handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
                                          if(error) {
                                              // An error occurred, we need to handle the error
                                              // See: https://developers.facebook.com/docs/ios/errors
                                              NSLog(@"Error publishing story: %@", error.description);
                                          } else {
                                              // Success
                                              NSLog(@"result %@", results);
//                                              &&([results objectForKey:@"didComplete"]==1)
                                              if ([[results objectForKey:@"completionGesture"] isEqual:@"post"]) {
                                                  UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"eSynchrony"
                                                                                               message:NSLocalizedString(@"Shared successfully",)
                                                                                              delegate:nil
                                                                                     cancelButtonTitle:NSLocalizedString(@"OK",)
                                                                                     otherButtonTitles:nil];
                                                  [av show];
                                                  [av release];
                                              }else{
                                              }

                                          }
                                      }];
        
        // If the Facebook app is NOT installed and we can't present the share dialog
    } else {
        // FALLBACK: publish just a link using the Feed dialog
        
        // Put together the dialog parameters
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       NSLocalizedString(@"Loving Couples from eSynchrony",@"FBShare"), @"name",
                                       @"www.esynchrony.com", @"caption",
                                       self.shareDescription, @"description",
                                       @"http://www.esynchrony.com/success-story.php", @"link",
                                       [NSString stringWithFormat:@"https://d1694b94lbm1p.cloudfront.net/images/testi/%@",self.shareImage], @"picture",
                                       nil];
        
        // Show the feed dialog
        [FBWebDialogs presentFeedDialogModallyWithSession:nil
                                               parameters:params
                                                  handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                      if (error) {
                                                          // An error occurred, we need to handle the error
                                                          // See: https://developers.facebook.com/docs/ios/errors
                                                          NSLog(@"Error publishing story: %@", error.description);
                                                      } else {
                                                          if (result == FBWebDialogResultDialogNotCompleted) {
                                                              // User canceled.
                                                              NSLog(@"User cancelled.");
                                                          } else {
                                                              // Handle the publish feed callback
                                                              NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
                                                              
                                                              if (![urlParams valueForKey:@"post_id"]) {
                                                                  // User canceled.
                                                                  NSLog(@"User cancelled.");
                                                                  
                                                              } else {
                                                                  // User clicked the Share button
                                                                  NSString *result = [NSString stringWithFormat: @"Posted story, id: %@", [urlParams valueForKey:@"post_id"]];
                                                                  NSLog(@"result %@", result);
                                                                  UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"eSynchrony"
                                                                                                               message:NSLocalizedString(@"Shared successfully",)
                                                                                                              delegate:nil
                                                                                                     cancelButtonTitle:NSLocalizedString(@"OK",)
                                                                                                     otherButtonTitles:nil];
                                                                  [av show];
                                                                  [av release];
                                                              }
                                                          }
                                                      }
                                                  }];
    }
}

- (IBAction)didClickLike:(UIButton *)sender {
  
    // May return nil if a tracker has not already been initialized with a property
    // ID.
    id tracker = [[GAI sharedInstance] defaultTracker];
    
    NSString *targetUrl = @"http://www.esynchrony.com/success-story.php";
    
    [tracker send:[[GAIDictionaryBuilder createSocialWithNetwork:@"Facebook"           // Social network (required)
                                                          action:@"Like"             // Social action (required)
                                                          target:targetUrl] build]];  // Social target
    
//    
//    if (![FBSDKAccessToken currentAccessToken]) {
//        [self alertWithMessage:@"Log into FB and request publish_actions" title:@"Upload Failed"];
//        return;
//    }
//    NSDictionary *params = @{@"source":UIImagePNGRepresentation([UIImage imageNamed:[NSString stringWithFormat:@"https://d1694b94lbm1p.cloudfront.net/images/testi/%@",self.likeImage]])};
//    
//    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me/photos" parameters:params HTTPMethod:@"POST"] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
//        if (!error) {
//            [self alertWithMessage:[NSString stringWithFormat:@"Result: %@", result] title:@"Post Photo Success!"];
//        } else {
//            NSLog(@"Graph API Error: %@", [error description]);
//        }
//    }];
    

    
//    if ([FBSDKAccessToken currentAccessToken]){
//        NSDictionary *params = @{
//                                 @"message": @"This is a test message",
//                                 };
//        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"/me/og.likes" parameters:params HTTPMethod:@"POST"]startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
//            if (error)
//                NSLog(@"error:%@", error);
//            else{
//                UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"FBSDKGraphRequest" message:@"Text Liked successfully..!" delegate:self cancelButtonTitle:@"Done" otherButtonTitles:nil, nil];
//                [av show];
//            }
//        }];
//    }
    
#if 0
    // For more complex open graph stories, use `FBSDKShareAPI`
    // with `FBSDKShareOpenGraphContent`
    //create
//    NSDictionary *params = @{
//                                 @"object": [NSString stringWithFormat:@"https://d1694b94lbm1p.cloudfront.net/images/testi/%@",self.likeImage],//226075010839791-->//226737744171417
//                                 };
    
    
    NSDictionary *params = @{
                             @"object": [NSString stringWithFormat:@"https://www.esynchrony.com/success-story-facebook.php?c=%@",self.likeImage],//226075010839791-->//226737744171417
                             };

//    NSLog(@"%@",param1);
    /* make the API call */
    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                  initWithGraphPath:@"/me/og.likes"
                                  parameters:params
                                  HTTPMethod:@"POST"];
    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                          id result,
                                          NSError *error) {
        // Handle the result
        NSLog(@"%@",result);
    }];
#endif
#if 0
    //read
    
    // For more complex open graph stories, use `FBSDKShareAPI`
    // with `FBSDKShareOpenGraphContent`
    /* make the API call */
    FBSDKGraphRequest *request1 = [[FBSDKGraphRequest alloc]
                                  initWithGraphPath:@"/me/og.likes"
                                  parameters:params
                                  HTTPMethod:@"GET"];
    [request1 startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                          id result,
                                          NSError *error) {
        // Handle the result
    }];
#endif
#if 0
    //update
//    http://samples.ogp.me/226737744171417
    
//     http://samples.ogp.me/315597225118972
    // For more complex open graph stories, use `FBSDKShareAPI`
    // with `FBSDKShareOpenGraphContent`
    NSDictionary *params1 = @{
                             @"object": @"http://samples.ogp.me/226075010839791",
                             };
    /* make the API call */
    FBSDKGraphRequest *request2 = [[FBSDKGraphRequest alloc]
                                  initWithGraphPath:@"/id_from_create_call"
                                  parameters:params1
                                  HTTPMethod:@"POST"];
    [request2 startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                          id result,
                                          NSError *error) {
        // Handle the result
    }];
#endif
#if 0
    //delete
    // For more complex open graph stories, use `FBSDKShareAPI`
    // with `FBSDKShareOpenGraphContent`
    /* make the API call */
    FBSDKGraphRequest *request4 = [[FBSDKGraphRequest alloc]
                                  initWithGraphPath:@"/id_from_create_call"
                                  parameters:params
                                  HTTPMethod:@"DELETE"];
    [request4 startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                          id result,
                                          NSError *error) {
        // Handle the result
    }];
    
#endif

    
#if 0
    NSLog(@"click button like");
    // Check if the Facebook app is installed and we can present the like dialog
    FBLinkShareParams *params = [[FBLinkShareParams alloc] init];
    params.link = [NSURL URLWithString:@"http://www.esynchrony.com/success-story.php"];
    params.name = NSLocalizedString(@"Loving Couples from eSynchrony", @"FBLike") ;
    params.caption = @"www.esynchrony.com";
    params.picture = [NSURL URLWithString:[NSString stringWithFormat:@"https://d1694b94lbm1p.cloudfront.net/images/testi/%@",self.likeImage]];
    params.linkDescription = self.likeDescription;
    
    // If the Facebook app is installed and we can present the share dialog
    if ([FBDialogs canPresentShareDialogWithParams:params]) {
        
        // Present share dialog
        [FBDialogs presentShareDialogWithParams:params
                                    clientState:nil
                                        handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
                                            if(error) {
                                                // An error occurred, we need to handle the error
                                                // See: https://developers.facebook.com/docs/ios/errors
                                                NSLog(@"Error publishing story: %@", error.description);
                                            } else {
                                                // Success
                                                NSLog(@"result %@", results);
//                                                UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"eSynchrony"
//                                                                                             message:NSLocalizedString(@"Like successfully",)
//                                                                                            delegate:nil
//                                                                                   cancelButtonTitle:NSLocalizedString(@"OK",)
//                                                                                   otherButtonTitles:nil];
//                                                [av show];
//                                                [av release];
                                            }
                                        }];
        // If the Facebook app is NOT installed and we can't present the share dialog
        
    } else {
        // FALLBACK: publish just a link using the Feed dialog
        
        // Put together the dialog parameters
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       NSLocalizedString(@"Loving Couples from eSynchrony",@"FBLike"), @"name",
                                       @"www.esynchrony.com", @"caption",
                                       self.likeDescription, @"description",
                                       @"http://www.esynchrony.com/success-story.php", @"link",
                                       [NSString stringWithFormat:@"https://d1694b94lbm1p.cloudfront.net/images/testi/%@",self.likeImage], @"picture",
                                       nil];
        
        // Show the feed dialog
        [FBWebDialogs presentFeedDialogModallyWithSession:nil
                                               parameters:params
                                                  handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                      if (error) {
                                                          // An error occurred, we need to handle the error
                                                          // See: https://developers.facebook.com/docs/ios/errors
                                                          NSLog(@"Error publishing story: %@", error.description);
                                                      } else {
                                                          if (result == FBWebDialogResultDialogNotCompleted) {
                                                              // User canceled.
                                                              NSLog(@"User cancelled.");
                                                          } else {
                                                              // Handle the publish feed callback
                                                              NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
                                                              
                                                              if (![urlParams valueForKey:@"post_id"]) {
                                                                  // User canceled.
                                                                  NSLog(@"User cancelled.");
                                                                  
                                                              } else {
                                                                  // User clicked the Share button
                                                                  NSString *result = [NSString stringWithFormat: @"Posted story, id: %@", [urlParams valueForKey:@"post_id"]];
                                                                  NSLog(@"result %@", result);
//                                                                  UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"eSynchrony"
//                                                                                                               message:NSLocalizedString(@"Like successfully",)
//                                                                                                              delegate:nil
//                                                                                                     cancelButtonTitle:NSLocalizedString(@"OK",)
//                                                                                                     otherButtonTitles:nil];
//                                                                  [av show];
//                                                                  [av release];
                                                              }
                                                          }
                                                      }
                                                  }];
    }
    
#endif
}


@end

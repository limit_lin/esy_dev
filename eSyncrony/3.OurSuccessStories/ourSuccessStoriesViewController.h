
#import <UIKit/UIKit.h>

@interface ourSuccessStoriesViewController : GAITrackedViewController<UITableViewDelegate,UITableViewDataSource>
{
    IBOutlet UIScrollView   *scrlView;
    IBOutlet UITableView    *storyTableView;
    IBOutlet UIView         *subView;
    IBOutlet UILabel        *myStroyTv;
    IBOutlet UILabel        *introLabel;
    NSArray                 *arrName;
    NSArray                 *arrTitle;
    NSArray                 *arrStory;
    NSArray                 *arrImage;
    NSArray                 *arrShareImage;
    BOOL                    nibsRegistered;
    IBOutlet UIImageView    *storyImage;
   
}
@property BOOL isTable;
@property (retain, nonatomic)NSString *imagename;
@property (retain, nonatomic)NSString *strTitle;
@property (retain, nonatomic)NSString *strStory;
- (IBAction)didClickBack:(id)sender;

-(NSArray *)getArrayFromPlist:(NSString *)plistName;
- (void)initSetting;


@end

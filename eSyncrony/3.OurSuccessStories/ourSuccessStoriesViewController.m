
#import "ourSuccessStoriesViewController.h"
#import "SuccessStoryCell.h"
#import "eSyncronyAppDelegate.h"
#import "mainMenuViewController.h"
#import <FacebookSDK/FacebookSDK.h>

@interface ourSuccessStoriesViewController ()

@end

@implementation ourSuccessStoriesViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    self.screenName = @"OurSuccessStories";


//    [FBSettings enableBetaFeature:(FBBetaFeatures.LikeButton)];
//    [FBSettings enablePlatformCompatibility:NO];
//    FBLikeControl *like = [[FBLikeControl alloc] init];
//    like.objectID = @"http://shareitexampleapp.parseapp.com/photo1/";
//    like.likeControlStyle = FBLikeControlStyleStandard;
//    [self.view addSubview:like];
//    like.frame = CGRectMake(50, 200, 200, 50);
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    if (self.isTable) {
        
        arrName = [[NSArray alloc]initWithObjects:@"Alan & Erica",@"Roger & Sun May",@"Nicholas & Michelle",@"Feon & Chee Yong",@"Will & Win",@"Lin & Sand",@"Janice & Thomas",@"Peggy & Boon Peng",@"Joseph & Vivian",@"Terence & Esther",@"Dilys & Edmund",@"Isabella & Lewis",@"Sammi & Hanani",@"Suan & Erin",@"Jenny & Kib",@"Cindy & Ernie",@"Pam & Li", nil];
        
        arrImage = [[NSArray alloc]initWithObjects:@"Alan_Erica.jpg",@"Roger_SunMay.jpg",@"Nicholas_Michelle.jpg",@"Feon_CheeYoug.jpg",@"Will_Win.jpg",@"Lin_Sand.jpg",@"Janice_Thomas.jpg",@"Peggy_BoonPeng.jpg",@"Joseph_Vivian150804.jpg",@"TerenceEsther150804.jpg",@"DilysEdmund150804.jpg",@"Isabella_Lewis.jpg",@"Sammi_Hanani.jpg",@"Suan_Erin.jpg",@"Jenny_Kib.jpg",@"Cindy_Ernie.jpg",@"Pam_Li.jpg", nil];
        
        arrShareImage = [[NSArray alloc]initWithObjects:@"AlanAndErica.jpg",@"RogerAndSunMayphoto.jpg",@"nicholas3.jpg",@"FeonAndCheeYoug.jpg",@"WillAndWin.jpg",@"Lin_and_Sand.png",@"000Janice_and_Thomas.jpg",@"peggy_boon_peng.jpg",@"Joseph_Vivian150804.jpg",@"TerenceEsther150804.jpg",@"DilysEdmund150804.jpg",@"Isabella_Lewis.jpg",@"samhanini.jpg",@"testithumb1.jpg",@"testithumb2.jpg",@"testithumb11.jpg",@"testimonial17.jpg", nil];

        arrTitle = [[NSArray alloc]initWithArray:[self getArrayFromPlist:@"story_title"]];
        arrStory = [[NSArray alloc]initWithArray:[self getArrayFromPlist:@"story_content"]];
        
        storyTableView.hidden = NO;
        scrlView.hidden = YES;
        storyTableView.delegate = self;
        storyTableView.dataSource = self;
        [storyTableView reloadData];
        
    }else{
        
        [self initSetting];
    }
    
}
-(NSArray *)getArrayFromPlist:(NSString *)plistName {
    NSString *_strPath =[[NSBundle mainBundle] pathForResource:plistName ofType:@"plist"];
    NSDictionary *_dicCountry = [NSDictionary dictionaryWithContentsOfFile:_strPath];//leaks 100%
    return [_dicCountry objectForKey:@"Array"];
}

- (void)initSetting
{
    storyImage.image = [UIImage imageNamed:self.imagename];
    introLabel.text = self.strTitle;
    myStroyTv.text = self.strStory;
    [introLabel sizeToFit];
    [myStroyTv sizeToFit];
    
    CGRect rect  = myStroyTv.frame;
    rect.origin.y = introLabel.frame.origin.y+introLabel.frame.size.height+20;
    myStroyTv.frame = rect;
   
    subView.frame = CGRectMake(0, 0, 320, rect.origin.y + rect.size.height+10 );

    [scrlView addSubview:subView];
    scrlView.contentSize = subView.frame.size;
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
//    [FBSession openActiveSessionWithReadPermissions:@[@"public_profile"]
//                                       allowLoginUI:YES
//                                  completionHandler:
//     ^(FBSession *session, FBSessionState state, NSError *error) {
//         
//         
//     }];
}
- (IBAction)didClickBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}



#pragma mark --- Table Functions ---
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrName.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *StoryIdentifier = @"SuccessStoryCellIdentifier";
    
    SuccessStoryCell *cell ;
    
    cell = [tableView dequeueReusableCellWithIdentifier:StoryIdentifier];
    if (cell == nil)
    {
        UINib *nib = [UINib nibWithNibName:@"SuccessStoryCell" bundle:nil];
        [tableView registerNib:nib forCellReuseIdentifier:StoryIdentifier];
        cell = [tableView dequeueReusableCellWithIdentifier:StoryIdentifier];
    }
    
    cell.lblName.text = [arrName objectAtIndex:indexPath.row];
    cell.storyImage.image = [UIImage imageNamed:[arrImage objectAtIndex:indexPath.row]];
    
    cell.shareImage = [arrShareImage objectAtIndex:indexPath.row];
    cell.shareDescription = [arrTitle objectAtIndex:indexPath.row];
    
    //append like
    cell.likeImage = [arrShareImage objectAtIndex:indexPath.row];
    cell.likeDescription = [arrTitle objectAtIndex:indexPath.row];
    
//    cell.btnLike.objectID = @"http://shareitexampleapp.parseapp.com/photo1/";
//     cell.btnLike.objectID = @"https://www.facebook.com/FacebookDevelopers";

   cell.btnLike.objectID = [NSString stringWithFormat:@"https://d1694b94lbm1p.cloudfront.net/images/testi/%@",cell.likeImage];   
    cell.btnLike.foregroundColor = [UIColor redColor];
//    cell.btnLike.objectID = @"https://www.esynchrony.com/success-story-facebook.php?c=testithumb1";
    return cell;
}


 #pragma mark - Table view delegate
 
 // In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
 - (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
 {
     ourSuccessStoriesViewController *ourSucsStrView = [[[ourSuccessStoriesViewController alloc] initWithNibName:@"ourSuccessStoriesViewController" bundle:nil] autorelease];
     ourSucsStrView.imagename = [arrImage objectAtIndex:indexPath.row];
     ourSucsStrView.strTitle = [arrTitle objectAtIndex:indexPath.row];
     ourSucsStrView.strStory = [arrStory objectAtIndex:indexPath.row];
     if ([eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl==nil) {
         [self.navigationController pushViewController:ourSucsStrView animated:YES];
     }else{
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.navigationController pushViewController:ourSucsStrView animated:YES]; 
     }
     

 }

- (void)dealloc
{
    [scrlView release];
    [subView release];
    [myStroyTv release];
    [introLabel release];
    
    [storyTableView release];
    [arrName release];
    [arrStory release];
    [arrTitle release];
    [self.imagename release];
    [self.strStory release];
    [self.strTitle release];
    [storyImage release];
    [super dealloc];
}

@end

//
//  SuccessStoryCell.h
//  eSyncrony
//
//  Created by iosdeveloper on 14-6-19.
//  Copyright (c) 2014年 WonMH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FBLikeControl.h>

@interface SuccessStoryCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UILabel *lblName;
@property (retain, nonatomic) IBOutlet UIView *lineView;
@property (retain, nonatomic) IBOutlet UIImageView *storyImage;

@property (retain, nonatomic) IBOutlet FBLikeControl *btnLike;

@property (retain, nonatomic) NSString *shareImage;
@property (retain, nonatomic) NSString *shareDescription;

@property (retain, nonatomic) NSString *likeImage;
@property (retain, nonatomic) NSString *likeDescription;

- (IBAction)didClickShare:(id)sender;
- (IBAction)didClickLike:(UIButton *)sender;



@end

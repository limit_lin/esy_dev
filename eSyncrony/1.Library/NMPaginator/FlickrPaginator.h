//
//  FlickrPaginator.h
//  NMPaginator
//
//  Created by Nicolas Mondollot on 08/04/12.
//  Copyright (c) 2012 Nicolas Mondollot. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NMPaginator.h"

@interface FlickrPaginator : NMPaginator
{
    
}
@property (nonatomic, strong) NSString *login_dt;
@property (nonatomic, strong) NSString *tmp_dt;
@property (nonatomic, strong) NSString *paying;
@property (nonatomic, strong) NSString *verified;

@property (nonatomic, assign) NSInteger numTotal;

@property (nonatomic, strong) NSMutableArray *arrMatchesItems;

@end

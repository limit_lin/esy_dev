//
//  FlickrPaginator.m
//  NMPaginator
//
//  Created by Nicolas Mondollot on 08/04/12.
//  Copyright (c) 2012 Nicolas Mondollot. All rights reserved.
//

#import "FlickrPaginator.h"
#import "UtilComm.h"
#import "eSyncronyAppDelegate.h"
@implementation FlickrPaginator

@synthesize login_dt;
@synthesize tmp_dt;
@synthesize paying;
@synthesize verified;
@synthesize arrMatchesItems;
@synthesize numTotal;

# pragma - fetch flickr photos
- (void)fetchResultsWithPage:(NSInteger)page pageSize:(NSInteger)pageSize withType:(int)type
{
    // NSLog(@"pagepagepage===%d",page);
    
    // do request on async thread
    dispatch_queue_t fetchQ = dispatch_queue_create("Flickr fetcher", NULL);
    dispatch_async(fetchQ, ^{
        NSDictionary* result = [[NSDictionary alloc]init];
        if (type == 1) {
            result = [UtilComm loadMatchesInfoWithType:@"1" withPage:page];
        }else
            result = [UtilComm loadMatchesFavoritedInfo:page];
        
        // go back to main thread before adding results
        dispatch_sync(dispatch_get_main_queue(), ^{
            
            if( result == nil )
            {
                numTotal = 0;
                return;
            }
            
            id response = [result objectForKey:@"response"];
            arrMatchesItems = [[NSMutableArray alloc] initWithCapacity:0];
            
            /////////////////response = "Zero Matches Found"////////////
            
            if ([response  isKindOfClass:[NSString class]]&&[response isEqualToString:@"Zero Matches Found"]) {
                if (page==1) {
                    login_dt = nil;
                    tmp_dt = nil;
                    paying = nil;
                    verified = nil;
                    numTotal = 0;
                }
                [self receivedResults:nil total:0 withType:type];
                return;
            }
            
            ///////////////////////////////////////////////////////////
            
            if( [response isKindOfClass:[NSDictionary class]] )
            {
                
                if (page==1) {
                    login_dt = [response objectForKey:@"login_dt"];
                    tmp_dt = [response objectForKey:@"tmp_dt"];
                    paying = [response objectForKey:@"paying"];
                    verified = [response objectForKey:@"verified"];
                    [eSyncronyAppDelegate sharedInstance].paying = paying;
                    [[eSyncronyAppDelegate sharedInstance] saveLoginInfo];
                }
                
                NSArray *arrItems = [response objectForKey:@"matches"];
                
                if( [arrItems isKindOfClass:[NSArray class]] )
                {
                    for( int i = 0; i < [arrItems count]; i++ )
                    {
                        NSDictionary  *item = [arrItems objectAtIndex:i];
                        [arrMatchesItems addObject:item];
                    }
                }else{
                    if( [arrItems isKindOfClass:[NSDictionary class]] )
                    {
                        NSDictionary* item = (NSDictionary*)arrItems;
                        
                        NSMutableDictionary *mutItem = [NSMutableDictionary dictionaryWithDictionary:item];
                        
                        [arrMatchesItems addObject:mutItem];
                    }
                }
                
            }
            
            NSInteger total = [[response objectForKey:@"total"]integerValue];
            numTotal = total;
            if (![[eSyncronyAppDelegate sharedInstance].membership isEqualToString:@"Premium"]) {
                if(total>20){
                    total = 20;
                }
            }
            if( [arrMatchesItems count] > 0 )
            {
                [self receivedResults:arrMatchesItems total:total withType:type];
            }
        });
    });
    //    dispatch_release(fetchQ);
}

- (void)fetchResultsWithPages:(NSInteger)page pageSize:(NSInteger)pageSize withType:(int)type//back append
{
    // NSLog(@"pagepagepage===%d",page);
    
    // do request on async thread
    dispatch_queue_t fetchQ = dispatch_queue_create("Flickr fetcher", NULL);
    dispatch_async(fetchQ, ^{
        
        NSDictionary* result = [[NSDictionary alloc]init];
        if (type == 1) {
            result = [UtilComm loadMatchesInfoWithType:@"1" withPage:page];
        }else
            result = [UtilComm loadMatchesFavoritedInfo:page];
        
        // go back to main thread before adding results
        dispatch_sync(dispatch_get_main_queue(), ^{
            
            if( result == nil )
            {
                numTotal = 0;
                return;
            }
            
            id response = [result objectForKey:@"response"];
            arrMatchesItems = [[NSMutableArray alloc] initWithCapacity:0];
            
            /////////////////response = "Zero Matches Found"////////////
            
            if ([response  isKindOfClass:[NSString class]]&&[response isEqualToString:@"Zero Matches Found"]) {
                if (page==1) {
                    login_dt = nil;
                    tmp_dt = nil;
                    paying = nil;
                    verified = nil;
                    numTotal = 0;                    
                }
                [self receivedResult:nil total:0 withType:type];
                return;
            }
            
            ///////////////////////////////////////////////////////////
            
            if( [response isKindOfClass:[NSDictionary class]] )
            {
                
                if (page==1) {
                    login_dt = [response objectForKey:@"login_dt"];
                    tmp_dt = [response objectForKey:@"tmp_dt"];
                    paying = [response objectForKey:@"paying"];
                    verified = [response objectForKey:@"verified"];
                    [eSyncronyAppDelegate sharedInstance].paying = paying;
                    [[eSyncronyAppDelegate sharedInstance] saveLoginInfo];
                }
                
                NSArray *arrItems = [response objectForKey:@"matches"];
                
                if( [arrItems isKindOfClass:[NSArray class]] )
                {
                    for( int i = 0; i < [arrItems count]; i++ )
                    {
                        NSDictionary  *item = [arrItems objectAtIndex:i];
                        [arrMatchesItems addObject:item];
                    }
                }else{
                    if( [arrItems isKindOfClass:[NSDictionary class]] )
                    {
                        NSDictionary* item = (NSDictionary*)arrItems;
                        
                        NSMutableDictionary *mutItem = [NSMutableDictionary dictionaryWithDictionary:item];
                        
                        [arrMatchesItems addObject:mutItem];
                    }
                }
                
            }
            
            NSInteger total = [[response objectForKey:@"total"]integerValue];
            numTotal = total;
            if (![[eSyncronyAppDelegate sharedInstance].membership isEqualToString:@"Premium"]) {
                if(total>20){
                    total = 20;
                }
            }
            if( [arrMatchesItems count] > 0 )
            {
                [self receivedResult:arrMatchesItems total:total withType:type];
            }
        });
    });
    //    dispatch_release(fetchQ);
}

@end

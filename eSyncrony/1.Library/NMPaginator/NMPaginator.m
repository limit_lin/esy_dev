//
//  NMPaginator.m
//
//  Created by Nicolas Mondollot on 07/04/12.
//

#import "NMPaginator.h"

@interface NMPaginator() {
}

// protected properties
@property (assign, readwrite) NSInteger pageSize;
@property (assign, readwrite) NSInteger page;
@property (assign, readwrite) NSInteger total;
@property (nonatomic, strong, readwrite) NSMutableArray *results;
@property (assign, readwrite) RequestStatus requestStatus;

@property (assign, readwrite) NSInteger favorPage;
@property (assign, readwrite) NSInteger favorTotal;
@end

@implementation NMPaginator
@synthesize delegate;
@synthesize page=_page, total=_total, results=_results, requestStatus=_requestStatus, pageSize=_pageSize;

@synthesize favorPage=_favorPage,favorTotal = _favorTotal;

- (id)initWithPageSize:(NSInteger)pageSize delegate:(id<NMPaginatorDelegate>)paginatorDelegate
{
    if(self = [super init])
    {
        [self setDefaultValues];
        self.pageSize = pageSize;
        self.delegate = paginatorDelegate;
    }
    
    return self;
}

- (void)setDefaultValues
{
    self.total = 0;
    self.page = 0;
    
    self.favorTotal = 0;
    self.favorPage = 0;
    
    self.results = [NSMutableArray array];
    self.requestStatus = RequestStatusNone;
}

- (void)reset
{
    [self setDefaultValues];
    
    // send message to delegate
    if([self.delegate respondsToSelector:@selector(paginatorDidReset:)])
        [self.delegate paginatorDidReset:self];
}

- (BOOL)reachedLastPage:(int)type
{
    // NSLog(@"last last last");
    if(self.requestStatus == RequestStatusNone) return NO; // if we haven't made a request, we can't know for sure
    
    if (type == 1) {
        NSInteger totalPages = ceil((float)self.total/(float)self.pageSize); // total number of pages
        
        if (totalPages >= 10) {
            
            return self.page >=10;//Matching data restriction
        }
        else
            return self.page >= totalPages;
    }else
    {
        NSInteger totalPages = ceil((float)self.favorTotal/(float)self.pageSize); // total number of pages
        
        if (totalPages >= 10) {
            
            return self.favorPage >=10;//Matching data restriction
        }
        else
            return self.favorPage >= totalPages;
    }
}

-(BOOL)reachedFirstPage:(int)type
{
    //NSLog(@"first first first");
    if(self.requestStatus == RequestStatusNone) return NO; // if we haven't made a request, we can't know for sure
    NSInteger firstPages = 1;
    if (type == 1) {
        return self.page <= firstPages;
    }else
        return self.favorPage <= firstPages;
}

# pragma - fetch results

- (void)fetchFirstPage:(int)type
{
    // reset paginator
    [self reset];
    [self fetchNextPage:type];
    
}

- (void)fetchNextPage:(int)type
{
    //  NSLog(@"nextnext==== %d page",self.page);
    // don't do anything if there's already a request in progress
    if(self.requestStatus == RequestStatusInProgress)
        return;
    if(![self reachedLastPage:type]) {
        
        self.requestStatus = RequestStatusInProgress;
        if(type == 1)
            [self fetchResultsWithPage:self.page+1 pageSize:self.pageSize withType:type];
        else
            [self fetchResultsWithPage:self.favorPage+1 pageSize:self.pageSize withType:type];
        
    }
}

-(void)fetchBackPage:(int)type
{
    // NSLog(@"backback=== %d page",self.page);
    if(self.requestStatus == RequestStatusInProgress)
        return;
    if(self.page!=1) {
        self.requestStatus = RequestStatusInProgress;
        
        if(type == 1)
            [self fetchResultsWithPages:self.page-1 pageSize:self.pageSize withType:type];
        else
            [self fetchResultsWithPages:self.favorPage-1 pageSize:self.pageSize withType:type];
    }
}
#pragma mark - Sublclass methods
- (void)fetchResultsWithPage:(NSInteger)page pageSize:(NSInteger)pageSize withType:(int)type{
    // override this in subclass
}
- (void)fetchResultsWithPages:(NSInteger)page pageSize:(NSInteger)pageSize withType:(int)type
{
    
}
#pragma mark received results

// call these from subclass when you receive the results

- (void)receivedResults:(NSArray *)results total:(NSInteger)total withType:(int)type
{
    //    if (self.results !=nil) {
    //        [self.results removeAllObjects];
    //    }
    [self.results removeAllObjects];//remove all objects
    [self.results addObjectsFromArray:results];
    if (type == 1) {
        self.page++;
        self.favorPage = self.favorPage;
        self.total = total;
    }else{
        self.favorPage++;
        self.page = self.page;
        self.favorTotal = total;
    }
    //    NSLog(@"self.page = %ld self.favorPage = %ld",self.page,self.favorPage);
    
    self.requestStatus = RequestStatusDone;
    
    [self.delegate paginator:self didReceiveResults:results];
}

- (void)receivedResult:(NSArray *)results total:(NSInteger)total withType:(int)type//append
{
    [self.results removeAllObjects];//remove all objects
    [self.results addObjectsFromArray:results];
    if (type == 1) {
        self.page--;
        self.favorPage = self.favorPage;
        self.total = total;
    }else{
        self.favorPage--;
        self.page = self.page;
        self.favorTotal = total;
    }
    //    NSLog(@"self.page = %ld self.favorPage = %ld",self.page,self.favorPage);
    
    self.requestStatus = RequestStatusDone;
    
    [self.delegate paginator:self didReceiveResults:results];
}

- (void)failed
{
    self.requestStatus = RequestStatusDone;
    
    if([self.delegate respondsToSelector:@selector(paginatorDidFailToRespond:)])
        [self.delegate paginatorDidFailToRespond:self];
}

@end

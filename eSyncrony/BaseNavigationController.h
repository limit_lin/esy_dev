//
//  BaseNavigationController.h
//  eSyncrony
//
//  Created by iosdeveloper on 14-9-30.
//  Copyright (c) 2014年 WonMH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseNavigationController : UINavigationController

@end

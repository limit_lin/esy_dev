

#import "Global.h"
#import "UtilComm.h"
#import "DSActivityView.h"
#import "eSyncronyAppDelegate.h"
#import "CultureStepsViewController.h"
#import "eSyncronyViewController.h"

//http://stackoverflow.com/questions/3910244/getting-current-device-language-in-ios

@interface CultureStepsViewController ()

@end

@implementation CultureStepsViewController

int     _culture_arrSelIndexs[10];

@synthesize parentView;
@synthesize step;

int _cultureTitleColorRange[10] = { 15, 15, 21, 11, 24, 26, 29, 29, 47, 57 };
int _cultureTitleZhHantColorRange[10] = { 15, 15, 15, 15, 11, 16, 20, 9, 13, 22 };

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"CultureStepsView";
    if(arrStringForQuestion == nil)
    {
        arrStringForQuestion = [[NSMutableArray alloc] init];
        [arrStringForQuestion addObject:NSLocalizedString(@"Have you stayed overseas in a faraway country for at least 2 years before?",@"CultureSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"Have you stayed overseas in a neighbouring country for at least 3 years before?",@"CultureSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"Have you travelled to more than 5 countries to date?",@"CultureSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"Do you have more than 5 friends who are from another culture?",@"CultureSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"Do you consider yourself westernised?",@"CultureSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"Do you consider yourself a traditional Asian?",@"CultureSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"Do you consider yourself more comfortable with someone who is more localised?",@"CultureSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"Do you consider yourself well travelled?",@"CultureSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"Do you consider yourself well exposed to other cultures?",@"CultureSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"Would you date someone who is from a completely different culture?",@"CultureSteps")];
    }

    if( self.step == 1 )
    {
        [btnBack setHidden:YES];

        for( int i = 0; i < 10; i++ )
            _culture_arrSelIndexs[i] = -1;
    }

    [self setContentsToStep];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    subView1.frame = scrView.bounds;
    subView2.frame = scrView.bounds;
    scrView.contentSize = subView1.bounds.size;
}

- (IBAction)didClickBtnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)checkValidChoice
{
    int     nSel = _culture_arrSelIndexs[step-1];
    
    if( nSel >= 0 && nSel < 5 )
        return TRUE;
    
    [ErrorProc alertMessage:NSLocalizedString(@"Please select your choice",) withTitle:NSLocalizedString(@"Field Required",)];
    
    return FALSE;
}

- (void)saveStepInfoProc
{
//    NSString*   accNo = [eSyncronyAppDelegate sharedInstance].strAccNo;
//    NSString*   tokenKey = [eSyncronyAppDelegate sharedInstance].strTokenKey;
    
    //--------------------------------------------------------------//
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];

    int     nValues[10];
    int     pTable[2][5] = { {10, 8, 5, 3, 0}, {0, 3, 5, 8, 10} };

    for( int i = 0; i < 10; i++ )
    {
        if( i == 5 || i == 6 )
            nValues[i] = pTable[1][_culture_arrSelIndexs[i]];
        else
            nValues[i] = pTable[0][_culture_arrSelIndexs[i]];
    }

    for( int i = 0; i < 10; i++ )
    {
        NSString*   keyValue = [NSString stringWithFormat:@"Q%d", i+1];

        [params setObject:[NSNumber numberWithInt:nValues[i]] forKey:keyValue];
    }

    NSDictionary*    result = [UtilComm saveCultureInfo:params];
    [DSBezelActivityView removeViewAnimated:NO];

    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Saving Failure"];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( response == nil )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Saving Failure",)];
        return;
    }
    
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [ErrorProc alertSetCultureError:response];
    }
    else
    {
        [self moveNextWindow];
    }
}

-(void)moveNextWindow
{
    //[self.navigationController popToRootViewControllerAnimated:NO];
    [parentView enterDisplayComplete:6];
}

- (void)saveStepInfoAndMoveNext
{
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Saving...",)];
    [self performSelector:@selector(saveStepInfoProc) withObject:nil afterDelay:0.01];
}

- (IBAction)didClickBtnNext:(id)sender
{
    if( [self checkValidChoice] == FALSE )
        return;
    
    if( step == 10 )
    {
        [self saveStepInfoAndMoveNext];
        return;
    }

    if( self.nextView == nil )
        self.nextView = [[CultureStepsViewController alloc] initWithNibName:@"CultureStepsViewController" bundle:nil];
    
    self.nextView.parentView = self.parentView;
    self.nextView.step = self.step + 1;
    
    [self.navigationController pushViewController:self.nextView animated:YES];
}

-(void)cultureSteps_actionNext
{
    if( [self checkValidChoice] == FALSE )
        return;
    
    if( step == 10 )
    {
        [self saveStepInfoAndMoveNext];
        return;
    }
    
    if( self.nextView == nil )
        self.nextView = [[CultureStepsViewController alloc] initWithNibName:@"CultureStepsViewController" bundle:nil];
    
    self.nextView.parentView = self.parentView;
    self.nextView.step = self.step + 1;
    
    [self.navigationController pushViewController:self.nextView animated:YES];
}

- (void)selectChosenLevel
{
    int     nSel = _culture_arrSelIndexs[step-1];
    
    if( step <= 4 )
    {
        UIButton*   btnLevels[] = { btnLevel1, btnLevel2, btnLevel3, btnLevel4, btnLevel5 };
    
        for( int i = 0; i < 5; i++ )
            [btnLevels[i] setSelected:NO];
    
        if( nSel >= 0 && nSel < 5 )
            [btnLevels[nSel] setSelected:YES];
    }
    else
    {
        UIButton*   btnLevels[] = { btnLevel6, btnLevel7, btnLevel8, btnLevel9, btnLevel10 };
        
        for( int i = 0; i < 5; i++ )
            [btnLevels[i] setSelected:NO];
        
        if( nSel >= 0 && nSel < 5 )
            [btnLevels[nSel] setSelected:YES];
    }
}

- (void) setContentsToStep
{
//    NSLog(@"%d",step);
    
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    //NSLog(@"%@",language);
    

    UIView* tmpView = nil;
    
    if( step > 4 )
        tmpView = subView2;
    else
        tmpView = subView1;

    [scrView addSubview:tmpView];
    scrView.contentSize = tmpView.bounds.size;

    NSString*   title = [arrStringForQuestion objectAtIndex:step-1];
    
    NSMutableAttributedString* string = [[NSMutableAttributedString alloc]initWithString:title];
    
    
    
//    if ([language isEqualToString:@"zh-Hant"]) {//chinese
    if ([language hasPrefix:@"zh-Hant"]||[language hasPrefix:@"zh-HK"]){//151016
        int     nLen = _cultureTitleZhHantColorRange[step-1];
        
        NSRange range1, range2, range3;
        
        range1 = NSMakeRange(0, nLen);
        range2 = NSMakeRange(nLen, [title length]-nLen-1);
        range3 = NSMakeRange([title length]-1, 1);
        
        UIColor*    color1 = LEVEL1_COLOR;
        UIColor*    color2 = LINE_TEXT_COLOR;

        [string addAttribute:NSForegroundColorAttributeName value:color1 range:range1];
        [string addAttribute:NSForegroundColorAttributeName value:color2 range:range2];
        [string addAttribute:NSForegroundColorAttributeName value:color1 range:range3];
        
    }
    else//en
    {
        int     nLen = _cultureTitleColorRange[step-1];
        
        NSRange range1, range2, range3;
        
        range1 = NSMakeRange(0, nLen);
        range2 = NSMakeRange(nLen, [title length]-nLen-1);
        range3 = NSMakeRange([title length]-1, 1);
        
        UIColor*    color1 = LEVEL1_COLOR;
        UIColor*    color2 = LINE_TEXT_COLOR;
        
        [string addAttribute:NSForegroundColorAttributeName value:color1 range:range1];
        [string addAttribute:NSForegroundColorAttributeName value:color2 range:range2];
        [string addAttribute:NSForegroundColorAttributeName value:color1 range:range3];
    }
 

    if( step <= 4 )
        [lbQuestionType1 setAttributedText:string];
    else
        [lbQuestionType2 setAttributedText:string];

    [self selectChosenLevel];
}

- (void)selectLevel:(int)level
{
    _culture_arrSelIndexs[step-1] = level;
    
    [self selectChosenLevel];
}

- (IBAction)didClickLevel1:(id)sender
{
    [self selectLevel:0];
    [self cultureSteps_actionNext];
}

- (IBAction)didClickLevel2:(id)sender
{
    [self selectLevel:1];
    [self cultureSteps_actionNext];
}

- (IBAction)didClickLevel3:(id)sender
{
    [self selectLevel:2];
    [self cultureSteps_actionNext];
}

- (IBAction)didClickLevel4:(id)sender
{
    [self selectLevel:3];
    [self cultureSteps_actionNext];
}

- (IBAction)didClickLevel5:(id)sender
{
    [self selectLevel:4];
    [self cultureSteps_actionNext];
}

-(void)dealloc
{
    [scrView release];
    [subView1 release];
    [subView2 release];
    
    [lbQuestionType1 release];
    [lbQuestionType2 release];
    
    [btnLevel1 release];
    [btnLevel2 release];
    [btnLevel3 release];
    [btnLevel4 release];
    [btnLevel5 release];
    [btnLevel6 release];
    [btnLevel7 release];
    [btnLevel8 release];
    [btnLevel9 release];
    [btnLevel10 release];
    
    [btnBack release];
    [arrStringForQuestion release];

    [self.nextView release];
    
    [super dealloc];
}

@end

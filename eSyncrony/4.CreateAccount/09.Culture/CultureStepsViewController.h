
#import <UIKit/UIKit.h>

@class eSyncronyViewController;

@interface CultureStepsViewController : GAITrackedViewController
{
    IBOutlet UIScrollView*      scrView;
    IBOutlet UIView*            subView1;
    IBOutlet UIView*            subView2;
    
    IBOutlet UILabel*           lbQuestionType1;
    IBOutlet UILabel*           lbQuestionType2;

    IBOutlet UIButton*          btnBack;
    
    IBOutlet UIButton*          btnLevel1;
    IBOutlet UIButton*          btnLevel2;
    IBOutlet UIButton*          btnLevel3;
    IBOutlet UIButton*          btnLevel4;
    IBOutlet UIButton*          btnLevel5;
    IBOutlet UIButton*          btnLevel6;
    IBOutlet UIButton*          btnLevel7;
    IBOutlet UIButton*          btnLevel8;
    IBOutlet UIButton*          btnLevel9;
    IBOutlet UIButton*          btnLevel10;

    NSMutableArray*             arrStringForQuestion;
}

@property (nonatomic, assign) int step;

@property (nonatomic, assign) CultureStepsViewController* nextView;
@property (nonatomic, assign) eSyncronyViewController* parentView;

- (IBAction)didClickBtnBack:(id)sender;
- (IBAction)didClickBtnNext:(id)sender;

- (IBAction)didClickLevel1:(id)sender;
- (IBAction)didClickLevel2:(id)sender;
- (IBAction)didClickLevel3:(id)sender;
- (IBAction)didClickLevel4:(id)sender;
- (IBAction)didClickLevel5:(id)sender;

@end

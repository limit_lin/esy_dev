
#import <UIKit/UIKit.h>
@class eSyncronyViewController;

@interface AppearanceStepsViewController : GAITrackedViewController<UITableViewDataSource,UITableViewDelegate>
{
    // mainView
    IBOutlet UIScrollView*      scrView;
    IBOutlet UIButton*          btnBack;
    IBOutlet UIButton*          appearanceBtnNext;//append
    
    IBOutlet UIButton*          btnLevel1;
    IBOutlet UIButton*          btnLevel2;
    IBOutlet UIButton*          btnLevel3;
    IBOutlet UIButton*          btnLevel4;
    IBOutlet UIButton*          btnLevel5;

    //viewType1
    IBOutlet UILabel*           lbQuestionViewType1Left;
    IBOutlet UILabel*           lbQuestionViewType1Right;
    
    //viewType2
    IBOutlet UILabel*           lbQuestionViewType2Left;
    IBOutlet UILabel*           lbQuestionViewType2Right;
    IBOutlet UIPickerView*      _pickerView;
    
    //viewType3
    IBOutlet UILabel*           lbQuestionViewType3Left;
    IBOutlet UILabel*           lbQuestionViewType3Right;
    IBOutlet UITableView*       tableView;
    
    //view to display
    IBOutlet UIView*            subviewCont1;
    IBOutlet UIView*            subviewCont2;
    IBOutlet UIView*            subviewCont3;
    
    NSMutableArray*             arrSubViewCont;
    NSMutableArray*             arrLbViewLeft;
    NSMutableArray*             arrLbViewRight;
    
    NSMutableArray*             arrQuestionLeft;
    NSMutableArray*             arrQuestionRight;
    NSMutableArray*             arrIndexForView;
    
    NSArray*                    arrQuestion;
    
    BOOL    bInit;
}

@property(nonatomic, assign) int step;
@property(nonatomic, assign) AppearanceStepsViewController* nextView;
@property(nonatomic,assign) eSyncronyViewController* parentView;

- (IBAction)didClickBtnBack:(id)sender;
- (IBAction)didClickBtnNext:(id)sender;

- (IBAction)didClickLevel1:(id)sender;
- (IBAction)didClickLevel2:(id)sender;
- (IBAction)didClickLevel3:(id)sender;
- (IBAction)didClickLevel4:(id)sender;
- (IBAction)didClickLevel5:(id)sender;

- (void)initializeTableContent:(NSString*)str;

@end


#import "Global.h"
#import "UtilComm.h"
#import "eSyncronyAppDelegate.h"
#import "AppearanceStepsViewController.h"
#import "RegisterTableViewCell.h"
#import "eSyncronyViewController.h"
#import "DSActivityView.h"
#import "HelvBdcnLabel.h"

@interface AppearanceStepsViewController ()

@end

@implementation AppearanceStepsViewController

int     _appearance_arrSelIndexs[14];

@synthesize step;
@synthesize parentView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"AppearanceStepsView";
    scrView.contentSize = subviewCont1.bounds.size;

    if( arrQuestionLeft == nil )
    {
        arrQuestionLeft = [[NSMutableArray alloc] init];
        [arrQuestionLeft addObject:NSLocalizedString(@"I am well",@"AppearanceSteps")];
        [arrQuestionLeft addObject:NSLocalizedString(@"I am",@"AppearanceSteps")];
        [arrQuestionLeft addObject:NSLocalizedString(@"I am",@"AppearanceSteps")];
        [arrQuestionLeft addObject:NSLocalizedString(@"I dress",@"AppearanceSteps")];
        [arrQuestionLeft addObject:NSLocalizedString(@"I am",@"AppearanceSteps")];
        [arrQuestionLeft addObject:NSLocalizedString(@"I have",@"AppearanceSteps")];
        [arrQuestionLeft addObject:NSLocalizedString(@"I am",@"AppearanceSteps")];
        [arrQuestionLeft addObject:NSLocalizedString(@"I look",@"AppearanceSteps")];
        [arrQuestionLeft addObject:NSLocalizedString(@"I am",@"AppearanceSteps")];
        [arrQuestionLeft addObject:NSLocalizedString(@"I am",@"AppearanceSteps")];
        [arrQuestionLeft addObject:NSLocalizedString(@"My",@"AppearanceSteps")];
        [arrQuestionLeft addObject:NSLocalizedString(@"My",@"AppearanceSteps")];
        [arrQuestionLeft addObject:NSLocalizedString(@"My",@"AppearanceSteps")];
        [arrQuestionLeft addObject:NSLocalizedString(@"My",@"AppearanceSteps")];
    }
    
    if( arrQuestionRight == nil )
    {
        arrQuestionRight = [[NSMutableArray alloc] init];
        [arrQuestionRight addObject:NSLocalizedString(@"groomed",@"AppearanceSteps")];
        [arrQuestionRight addObject:NSLocalizedString(@"attractive",@"AppearanceSteps")];
        [arrQuestionRight addObject:NSLocalizedString(@"athletic",@"AppearanceSteps")];
        [arrQuestionRight addObject:NSLocalizedString(@"stylishly",@"AppearanceSteps")];
        [arrQuestionRight addObject:NSLocalizedString(@"overweight",@"AppearanceSteps")];
        [arrQuestionRight addObject:NSLocalizedString(@"pretty/handsome face",@"AppearanceSteps")];
        [arrQuestionRight addObject:NSLocalizedString(@"skinny",@"AppearanceSteps")];
        [arrQuestionRight addObject:NSLocalizedString(@"ordinary",@"AppearanceSteps")];
        [arrQuestionRight addObject:NSLocalizedString(@"healthy looking",@"AppearanceSteps")];
        [arrQuestionRight addObject:NSLocalizedString(@"sexy",@"AppearanceSteps")];
        [arrQuestionRight addObject:NSLocalizedString(@"height",@"AppearanceSteps")];
        [arrQuestionRight addObject:NSLocalizedString(@"weight",@"AppearanceSteps")];
        [arrQuestionRight addObject:NSLocalizedString(@"body type",@"AppearanceSteps")];
        [arrQuestionRight addObject:NSLocalizedString(@"hair",@"AppearanceSteps")];
    }
    
    if( arrIndexForView == nil )
    {
        arrIndexForView = [[NSMutableArray alloc] init];
        [arrIndexForView addObject:@"1"];
        [arrIndexForView addObject:@"1"];
        [arrIndexForView addObject:@"1"];
        [arrIndexForView addObject:@"1"];
        [arrIndexForView addObject:@"1"];
        [arrIndexForView addObject:@"1"];
        [arrIndexForView addObject:@"1"];
        [arrIndexForView addObject:@"1"];
        [arrIndexForView addObject:@"1"];
        [arrIndexForView addObject:@"1"];
        [arrIndexForView addObject:@"2"];
        [arrIndexForView addObject:@"2"];
        [arrIndexForView addObject:@"3"];
        [arrIndexForView addObject:@"3"];
    }

    if( arrSubViewCont == nil )
    {
        arrSubViewCont = [[NSMutableArray alloc] init];
        [arrSubViewCont addObject:subviewCont1];
        [arrSubViewCont addObject:subviewCont2];
        [arrSubViewCont addObject:subviewCont3];
    }

    if( arrLbViewLeft == nil )
    {
        arrLbViewLeft = [[NSMutableArray alloc] init];
        [arrLbViewLeft addObject:lbQuestionViewType1Left];
        [arrLbViewLeft addObject:lbQuestionViewType2Left];
        [arrLbViewLeft addObject:lbQuestionViewType3Left];
    }
    
    if( arrLbViewRight == nil )
    {
        arrLbViewRight = [[NSMutableArray alloc] init];
        [arrLbViewRight addObject:lbQuestionViewType1Right];
        [arrLbViewRight addObject:lbQuestionViewType2Right];
        [arrLbViewRight addObject:lbQuestionViewType3Right];
    }
    
    if( self.step == 1 )
    {
        [btnBack setHidden:YES];
        
        for( int i = 0; i < 14; i++ )
            _appearance_arrSelIndexs[i] = -1;
    
        if( [eSyncronyAppDelegate sharedInstance].gender == 0 )
        {
            _appearance_arrSelIndexs[10] = 170;
            _appearance_arrSelIndexs[11] = 80;
        }
        else
        {
            _appearance_arrSelIndexs[10] = 160;
            _appearance_arrSelIndexs[11] = 50;
        }
    }
    
    bInit = FALSE;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    subviewCont1.frame = scrView.bounds;
    subviewCont2.frame = scrView.bounds;
    subviewCont3.frame = scrView.bounds;

    if( bInit == FALSE )
    {
        [self setContentsToStep];
        bInit = TRUE;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)didClickBtnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)saveAppearanceProc
{
    
    int gender = [eSyncronyAppDelegate sharedInstance].gender;
    //--------------------------------------------------------------//
    int     nQValues[10];
    int     nQTable[5][5] = { {0, 3, 5, 8, 10}, {10, 8, 5, 3, 0}, {0, 1, 3, 4, 5}, {1, 2, 3, 4, 5}, {5, 4, 3, 2, 1} };
    
    for( int i = 0; i < 10; i++ )
    {
        if( i < 4||i==5||i==9 ){
            nQValues[i] = nQTable[0][_appearance_arrSelIndexs[i]];
        }else if(i==4){
            nQValues[i] = nQTable[1][_appearance_arrSelIndexs[i]];
        }else if(i==6){
            if (gender==0) {
                nQValues[i] = nQTable[4][_appearance_arrSelIndexs[i]];
            }else{
                nQValues[i] = nQTable[3][_appearance_arrSelIndexs[i]];
            }
            
        }else if(i==8||i==7){
            nQValues[i] = nQTable[2][_appearance_arrSelIndexs[i]];
        }
    }
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    
    [params setObject:[NSNumber numberWithInt:nQValues[0]] forKey:@"Q1"];
    [params setObject:[NSNumber numberWithInt:nQValues[1]] forKey:@"Q2"];
    [params setObject:[NSNumber numberWithInt:nQValues[2]] forKey:@"Q3"];
    [params setObject:[NSNumber numberWithInt:nQValues[3]] forKey:@"Q4"];
    [params setObject:[NSNumber numberWithInt:nQValues[4]] forKey:@"Q5"];
    [params setObject:[NSNumber numberWithInt:nQValues[5]] forKey:@"Q6"];
    [params setObject:[NSNumber numberWithInt:nQValues[6]] forKey:@"Q7"];
    [params setObject:[NSNumber numberWithInt:nQValues[7]] forKey:@"Q8"];
    [params setObject:[NSNumber numberWithInt:nQValues[8]] forKey:@"Q9"];
    [params setObject:[NSNumber numberWithInt:nQValues[9]] forKey:@"Q10"];
    [params setObject:[NSNumber numberWithInt:_appearance_arrSelIndexs[10]] forKey:@"height"];
    [params setObject:[NSNumber numberWithInt:_appearance_arrSelIndexs[11]] forKey:@"weight"];
    [params setObject:[NSNumber numberWithInt:_appearance_arrSelIndexs[12]] forKey:@"body"];
    [params setObject:[NSNumber numberWithInt:_appearance_arrSelIndexs[13]] forKey:@"hair"];

    NSDictionary*    result = [UtilComm savePhysicalInfo:params];
    [DSBezelActivityView removeViewAnimated:NO];

    if( result == nil )
    {
//        [ErrorProc alertToCheckInternetStateTitle:@"Saving Failure"];
        return;
    }

    id response = [result objectForKey:@"response"];
    if( response == nil )
    {
        [ErrorProc alertMessage:@"Unknown Error" withTitle:@"Saving Failure"];
        return;
    }
    
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [ErrorProc alertSetPhysicalError:response];
    }
    else
    {
        [self moveNextWindow];
    }
}

-(void)moveNextWindow
{
   // [self.navigationController popToRootViewControllerAnimated:NO];
    [parentView enterInterestView];
}

- (void)saveAppearanceAndMoveNext
{
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Saving...",)];
    [self performSelector:@selector(saveAppearanceProc) withObject:nil afterDelay:0.01];
}

- (BOOL)checkValidChoice
{
    int     nSel = _appearance_arrSelIndexs[step-1];

    if( nSel != -1 )
        return TRUE;

    [ErrorProc alertMessage:NSLocalizedString(@"Please select your choice",) withTitle:NSLocalizedString(@"Field Required",)];
    
    return FALSE;
}

- (IBAction)didClickBtnNext:(id)sender
{
    if( [self checkValidChoice] == FALSE )
        return;
    
    if( step == 14 )
    {
        [self saveAppearanceAndMoveNext];
        return;
    }
    
    if( self.nextView == nil )
        self.nextView = [[AppearanceStepsViewController alloc] initWithNibName:@"AppearanceStepsViewController" bundle:nil];
    
    self.nextView.parentView = self.parentView;
    self.nextView.step = self.step+1;
    
    [self.navigationController pushViewController:self.nextView animated:YES];
}

-(void)appearance_actionNext
{
//append
    if( [self checkValidChoice] == FALSE )
        return;
    
    if( step == 14 )
    {
        [self saveAppearanceAndMoveNext];
        return;
    }
    
    if( self.nextView == nil )
        self.nextView = [[AppearanceStepsViewController alloc] initWithNibName:@"AppearanceStepsViewController" bundle:nil];
    
    self.nextView.parentView = self.parentView;
    self.nextView.step = self.step+1;
    
    [self.navigationController pushViewController:self.nextView animated:YES];
    
    
}

- (void)selectChosenLevel
{
    int     nSel = _appearance_arrSelIndexs[step-1];
    
    if( step <= 10 )
    {
        UIButton*   btnLevels[] = { btnLevel1, btnLevel2, btnLevel3, btnLevel4, btnLevel5 };
        
        for( int i = 0; i < 5; i++ )
            [btnLevels[i] setSelected:NO];

        if( nSel >= 0 && nSel < 5 )
            [btnLevels[nSel] setSelected:YES];
    }
    else if( step == 11 )
    {
        [_pickerView selectRow:200-_appearance_arrSelIndexs[10] inComponent:0 animated:NO];
    }
    else if( step == 12 )
    {
        [_pickerView selectRow:120-_appearance_arrSelIndexs[11] inComponent:0 animated:NO];
    }
    else if( step >= 13 )
    {
        if( _appearance_arrSelIndexs[step-1] == -1 )
            return;
        
        [tableView selectRowAtIndexPath:[NSIndexPath indexPathForItem:_appearance_arrSelIndexs[step-1] inSection:0]
                               animated:NO
                         scrollPosition:UITableViewScrollPositionNone];
    }
}

- (void)setContentsToStep
{
    int index = [[arrIndexForView objectAtIndex:step-1] intValue];

    UIView* tmpView = [arrSubViewCont objectAtIndex:index-1];
    [scrView addSubview:tmpView];
    scrView.contentSize = tmpView.bounds.size;

    UILabel* tmpLb = [arrLbViewLeft objectAtIndex:index-1];
    tmpLb.text = [arrQuestionLeft objectAtIndex:step-1];

    tmpLb = [arrLbViewRight objectAtIndex:index-1];
    tmpLb.text = [arrQuestionRight objectAtIndex:step-1];

    if( step == 11 || step == 12 )
    {
        appearanceBtnNext.hidden = NO;//append
        
        [_pickerView reloadAllComponents];
    }
    else if( step == 13 )
    {
        [self initializeTableContent:@"bodytype"];
        [tableView reloadData];
        [self resizeTableView];
    }
    else if( step == 14 )
    {
        [self initializeTableContent:@"hairstyle"];
        [tableView reloadData];
        [self resizeTableView];
    }
    
    //--------------------------------------//
    [self selectChosenLevel];
}

- (void)initializeTableContent:(NSString*)str
{
    arrQuestion = [[NSMutableArray alloc] initWithArray:[Global loadQuestionArray:str]];
}

- (void)selectLevel:(int)level
{
    _appearance_arrSelIndexs[step-1] = level;
    
    UIButton*   btnLevels[] = { btnLevel1, btnLevel2, btnLevel3, btnLevel4, btnLevel5 };
    
    for( int i = 0; i < 5; i++ )
        [btnLevels[i] setSelected:NO];

    if( level >= 0 && level < 5 )
        [btnLevels[level] setSelected:YES];
}

- (IBAction)didClickLevel1:(id)sender
{
    [self selectLevel:0];
    //append
    [self appearance_actionNext];
    
}

- (IBAction)didClickLevel2:(id)sender
{
    [self selectLevel:1];
    //append
    [self appearance_actionNext];
}

- (IBAction)didClickLevel3:(id)sender
{
    [self selectLevel:2];
    //append
    [self appearance_actionNext];
}

- (IBAction)didClickLevel4:(id)sender
{
    [self selectLevel:3];
    //append
    [self appearance_actionNext];
}

- (IBAction)didClickLevel5:(id)sender
{
    [self selectLevel:4];
    //append
    [self appearance_actionNext];
}

- (void)resizeTableView
{
    int maxCount = ([UIScreen mainScreen].bounds.size.height - 144)/42;
    
    if( [arrQuestion count] < maxCount )
    {
        [tableView setFrame:CGRectMake(tableView.frame.origin.x, tableView.frame.origin.y, tableView.frame.size.width, 42 * [arrQuestion count])];
    }
    else
    {
        [tableView setFrame:CGRectMake(tableView.frame.origin.x, tableView.frame.origin.y, tableView.frame.size.width, 42 * maxCount )];
    }
}
 
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
 
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrQuestion count];
}
 
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 42;
}
 
 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RegisterTableViewCell *cell;
    NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"RegisterTableViewCell" owner:nil options:nil];
    cell = [arr objectAtIndex:0];
    NSInteger row = [indexPath row];
    //NSDictionary *_storeObj =[arrFriendList objectAtIndex:row];
    NSString * str =  [arrQuestion objectAtIndex:row];
    cell.lbContent.text = str;
    
    return cell;
}
 
 - (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)newIndexPath
{
    if( step >= 13 || step <= 14 )
        _appearance_arrSelIndexs[step-1] = (int)newIndexPath.row;
    
    
    //append
    [self appearance_actionNext];
}

//===============================================================================//

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if( step == 11 )
        return 200-140+1;
    else
        return 120-40+1;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return 240;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 42;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    HelvBdcnLabel*    label = [[HelvBdcnLabel alloc] initWithFrame:CGRectMake(0,0,90,44)];

    if( step == 11 )
        label.text = [NSString stringWithFormat:@"%d Cm", (int)(200-row)];
    else
        label.text = [NSString stringWithFormat:@"%d Kg", (int)(120-row)];

    label.textAlignment = NSTextAlignmentCenter;
    label.backgroundColor = [UIColor clearColor];
    
    return label;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if( step == 11 )
        _appearance_arrSelIndexs[10] = (int)(200-row);
    else if( step == 12 )
        _appearance_arrSelIndexs[11] = (int)(120-row);
}

//=============================================================================================//

- (void)dealloc
{
    [scrView release];
    [btnBack release];
  
    [lbQuestionViewType1Left release];
    [lbQuestionViewType1Right release];
  
    [lbQuestionViewType2Left release];
    [lbQuestionViewType2Right release];
  
    [lbQuestionViewType3Left release];
    [lbQuestionViewType3Right release];
    [tableView release];
  
    [subviewCont1 release];
    [subviewCont2 release];
    [subviewCont3 release];
    
    [btnLevel1 release];
    [btnLevel2 release];
    [btnLevel3 release];
    [btnLevel4 release];
    [btnLevel5 release];
  
    [arrSubViewCont release];
    [arrLbViewLeft release];
    [arrLbViewRight release];
  
    [arrQuestionLeft release];
    [arrQuestionRight release];
    [arrIndexForView release];
    [arrQuestion release];
    
    [self.nextView release];
    
    [appearanceBtnNext release];
    [super dealloc];
}

@end


#import "Global.h"
#import "UtilComm.h"
#import "DSActivityView.h"
#import "eSyncronyAppDelegate.h"
#import "OpennessStepsViewController.h"
#import "eSyncronyViewController.h"

@interface OpennessStepsViewController ()

@end

@implementation OpennessStepsViewController

@synthesize parentView;
@synthesize step;

int     _openness_arrSelIndexs[10];
int     _opennessTitleColorRange[10] = { 14, 13, 19, 0, 0, 0, 0, 0, 0, 0 };//en

int     _opennessTitleZhHantColorRange[10] = { 6, 5, 12, 0, 0, 0, 0, 0, 0, 0 };//zh_hant

- (void)viewDidLoad
{
    [super viewDidLoad];//151125
    
    self.screenName = @"OpennessStepsView";
    [scrView addSubview:subView1];
    
    scrView.contentSize = subView1.bounds.size;
    
    if( arrStringForQuestion == nil )
    {
        arrStringForQuestion = [[NSMutableArray alloc] init];
        [arrStringForQuestion addObject:NSLocalizedString(@"I have a vivid imagination.",@"OpennessSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"I have a rich vocabulary.",@"OpennessSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"I enjoy hearing new ideas.",@"OpennessSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"I tend to vote for conservative political candidates.",@"OpennessSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"I enjoy wild flights of fantasy.",@"OpennessSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"I get excited by new ideas.",@"OpennessSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"I do not enjoy going to museums.",@"OpennessSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"I believe in the importance of art.",@"OpennessSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"I am not interested in theoretical or philosophical discussions.",@"OpennessSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"I enjoy thinking about things.",@"OpennessSteps")];
    }
    
    if( self.step == 1 )
    {
        [btnBack setHidden:YES];
    
        for( int i = 0; i < 10; i++ )
            _openness_arrSelIndexs[i] = -1;
    }

    [self setContentsToStep];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    subView1.frame = scrView.bounds;
    scrView.contentSize = subView1.bounds.size;
}

- (IBAction)didClickBtnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)checkValidChoice
{
    int     nSel = _openness_arrSelIndexs[step-1];
    
    if( nSel >= 0 && nSel < 5 )
        return TRUE;
    
    [ErrorProc alertMessage:NSLocalizedString(@"Please select your choice",) withTitle:NSLocalizedString(@"Field Required",)];
    
    return FALSE;
}

- (void)saveStepInfoProc
{
//    NSString*   accNo = [eSyncronyAppDelegate sharedInstance].strAccNo;
//    NSString*   tokenKey = [eSyncronyAppDelegate sharedInstance].strTokenKey;
    
    //--------------------------------------------------------------//
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    
    int     nValues[10];
    int     pTable[2][5] = { {10, 8, 5, 3, 0}, {0, 3, 5, 8, 10} };
    
    for( int i = 0; i < 10; i++ )
    {
        if( i == 3 || i == 6 || i == 8 )
            nValues[i] = pTable[1][_openness_arrSelIndexs[i]];
        else
            nValues[i] = pTable[0][_openness_arrSelIndexs[i]];
    }
    
    for( int i = 0; i < 10; i++ )
    {
        NSString*   keyValue = [NSString stringWithFormat:@"Q%d", i+1];
        
        [params setObject:[NSNumber numberWithInt:nValues[i]] forKey:keyValue];
    }
    
    NSDictionary*    result = [UtilComm saveOpennessInfo:params];
    [DSBezelActivityView removeViewAnimated:NO];
    
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Saving Failure"];
        return;
    }

    id response = [result objectForKey:@"response"];
    if( response == nil )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Saving Failure",)];
        return;
    }
    
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [ErrorProc alertSetOpennessError:response];
    }
    else
    {
        [self moveNextWindow];
    }
}

-(void)moveNextWindow
{
   // [self.navigationController popToRootViewControllerAnimated:NO];
    [parentView enterAgreeableStepsView];
}

- (void)saveStepInfoAndMoveNext
{
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Saving...",)];
    [self performSelector:@selector(saveStepInfoProc) withObject:nil afterDelay:0.01];
}

- (IBAction)didClickBtnNext:(id)sender
{
    if( [self checkValidChoice] == FALSE )
        return;
    
    if( step == 10 )
    {
        [self saveStepInfoAndMoveNext];
        return;
    }
    
    if( self.nextView == nil )
        self.nextView = [[OpennessStepsViewController alloc] initWithNibName:@"OpennessStepsViewController" bundle:nil];
    
    self.nextView.parentView = self.parentView;
    self.nextView.step = self.step+1;

    [self.navigationController pushViewController:self.nextView animated:YES];
}

-(void)opennessSteps_actionNext
{
    if( [self checkValidChoice] == FALSE )
        return;
    
    if( step == 10 )
    {
        [self saveStepInfoAndMoveNext];
        return;
    }
    
    if( self.nextView == nil )
        self.nextView = [[OpennessStepsViewController alloc] initWithNibName:@"OpennessStepsViewController" bundle:nil];
    
    self.nextView.parentView = self.parentView;
    self.nextView.step = self.step+1;
    
    [self.navigationController pushViewController:self.nextView animated:YES];
}


- (void)selectChosenLevel
{
    int     nSel = _openness_arrSelIndexs[step-1];
    
    UIButton*   btnLevels[] = { btnLevel1, btnLevel2, btnLevel3, btnLevel4, btnLevel5 };
    
    for( int i = 0; i < 5; i++ )
        [btnLevels[i] setSelected:NO];
    
    if( nSel >= 0 && nSel < 5 )
        [btnLevels[4-nSel] setSelected:YES];
//        [btnLevels[nSel] setSelected:YES];
        
}

- (void)setContentsToStep
{
    //NSLog(@"%d",step);
    
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    int     nLen = 0;

    NSString*   title = [arrStringForQuestion objectAtIndex:step-1];
    
    NSMutableAttributedString* string = [[NSMutableAttributedString alloc]initWithString:title];
 
//judge NSLocalized start
    if ([language hasPrefix:@"zh-Hant"]||[language hasPrefix:@"zh-HK"]) {
        
        nLen = _opennessTitleZhHantColorRange[step-1];
        
    }
    else//en,Id
    {
        nLen = _opennessTitleColorRange[step-1];
        
    }
//judge end
    if( nLen == 0 )
    {
        NSRange     range1 = NSMakeRange(0, [title length]);
        UIColor*    color1 = OPENESS_TITLE_COLOR;

        [string addAttribute:NSForegroundColorAttributeName value:color1 range:range1];
    }
    else
    {
        NSRange range1, range2, range3;
    
        range1 = NSMakeRange(0, nLen);
        range2 = NSMakeRange(nLen, [title length]-nLen-1);
        range3 = NSMakeRange([title length]-1, 1);
        
        UIColor*    color1 = LEVEL1_COLOR;
        UIColor*    color2 = LINE_TEXT_COLOR;
    
        [string addAttribute:NSForegroundColorAttributeName value:color1 range:range1];
        [string addAttribute:NSForegroundColorAttributeName value:color2 range:range2];
        [string addAttribute:NSForegroundColorAttributeName value:color1 range:range3];
    }
    
    [lbQuestion setAttributedText:string];

    [self selectChosenLevel];
}

- (void)selectLevel:(int)level
{
    _openness_arrSelIndexs[step-1] = level;
    
    [self selectChosenLevel];
}

- (IBAction)didClickLevel1:(id)sender
{
//    [self selectLevel:0];
    [self selectLevel:4];
    [self opennessSteps_actionNext];
}

- (IBAction)didClickLevel2:(id)sender
{
//    [self selectLevel:1];
    [self selectLevel:3];
    [self opennessSteps_actionNext];
}

- (IBAction)didClickLevel3:(id)sender
{
    [self selectLevel:2];
    [self opennessSteps_actionNext];
}

- (IBAction)didClickLevel4:(id)sender
{
//    [self selectLevel:3];
    [self selectLevel:1];
    [self opennessSteps_actionNext];
}

- (IBAction)didClickLevel5:(id)sender
{
//    [self selectLevel:4];
    [self selectLevel:0];
    [self opennessSteps_actionNext];
}

-(void)dealloc
{
    [scrView release];
    [subView1 release];
    
    [lbQuestion release];
    [btnBack release];
    [arrStringForQuestion release];

    [btnLevel1 release];
    [btnLevel2 release];
    [btnLevel3 release];
    [btnLevel4 release];
    [btnLevel5 release];
    
    [self.nextView release];
    
    [super dealloc];
}

@end


#import <UIKit/UIKit.h>

@class eSyncronyViewController;

@interface BasicStepViewsController : GAITrackedViewController<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UIScrollView*      scrlView;
    IBOutlet UIView*            subViewSteps;

    IBOutlet UIView*            subViewTitle;
    IBOutlet UITableView*       tableView;
    
    IBOutlet UILabel*           labFirst;
    IBOutlet UILabel*           labSecond;
    IBOutlet UILabel*           labLast;
    
   
    IBOutlet UIButton*          clickNext;//append 0804
    
    NSMutableArray*             arrFirstLabTitle;
    NSMutableArray*             arrSecondLabTitle;
    NSMutableArray*             arrLastLabTitle;
    
    NSArray*                    arrQuestion;
    NSMutableArray*             arrNameOfInfoList;
}

@property(nonatomic, assign)    int nStep;
@property(nonatomic, assign)    int cityid;
@property(nonatomic, assign)    BasicStepViewsController* nextView;
@property(nonatomic, assign)    NSMutableArray*     arrSelectedRowIdx;

@property(nonatomic, assign)    eSyncronyViewController* parentView;
@property(nonatomic, assign)    NSMutableDictionary *dicLifeStyleData;


- (IBAction)didClickBack:(id)sender;
- (IBAction)didClickNext:(id)sender;

- (void)resizeTableView;

@end

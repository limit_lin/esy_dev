//
//  WelcomeViewController.h
//  eSyncrony
//
//  Created by iosdeveloper on 14-5-29.
//  Copyright (c) 2014年 WonMH. All rights reserved.
//

#import <UIKit/UIKit.h>
@class eSyncronyViewController;
@interface WelcomeViewController : GAITrackedViewController
@property(nonatomic, assign)    eSyncronyViewController* parentView;
@property (retain, nonatomic) IBOutlet UILabel *lblWelcome;
- (IBAction)didGoBasics:(id)sender;

@end

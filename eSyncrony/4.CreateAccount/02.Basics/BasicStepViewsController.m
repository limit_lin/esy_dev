
#import "Global.h"
#import "BasicStepViewsController.h"
#import "eSyncronyViewController.h"
#import "RegisterTableViewCell2.h"

#import "DSActivityView.h"
#import "eSyncronyAppDelegate.h"
#import "NRICVerifyViewController.h"

@interface BasicStepViewsController ()
{
    
    IBOutlet UIButton *checkSelectedBtn;
    IBOutlet UILabel *checkSelectLab;
    NSMutableArray* arrState;
    
}
- (IBAction)onClickcheckBtn:(UIButton *)sender;

@end

@implementation BasicStepViewsController

@synthesize parentView,dicLifeStyleData,cityid;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.screenName = @"BasicStepsView";
    cityid =[eSyncronyAppDelegate sharedInstance].countryId+1;
    if( arrFirstLabTitle == nil )
    {
        arrFirstLabTitle = [[NSMutableArray alloc] init];
        [arrFirstLabTitle addObject:NSLocalizedString(@"What is your current ",@"Basic Step")];
        [arrFirstLabTitle addObject:NSLocalizedString(@"What is your ",@"Basic Step")];
        [arrFirstLabTitle addObject:NSLocalizedString(@"What is your ",@"Basic Step")];
        [arrFirstLabTitle addObject:NSLocalizedString(@"what's the ",@"Basic Step")];
        [arrFirstLabTitle addObject:NSLocalizedString(@"What ",@"Basic Step")];
        [arrFirstLabTitle addObject:NSLocalizedString(@"What ",@"Basic Step")];
        [arrFirstLabTitle addObject:NSLocalizedString(@"What is your highest ",@"Basic Step")];
        if (cityid == 2) {
            [arrFirstLabTitle addObject:NSLocalizedString(@"What is",@"Basic Step")];
        }
        
    }
    
    if( arrSecondLabTitle == nil )
    {
        arrSecondLabTitle = [[NSMutableArray alloc] init];
        [arrSecondLabTitle addObject:NSLocalizedString(@"marital status",@"Basic Step")];
        [arrSecondLabTitle addObject:NSLocalizedString(@"ethnicity",@"Basic Step")];
        [arrSecondLabTitle addObject:NSLocalizedString(@"nationality",@"Basic Step")];
        [arrSecondLabTitle addObject:NSLocalizedString(@"state",@"Basic Step")];
        [arrSecondLabTitle addObject:NSLocalizedString(@"Language",@"Basic Step")];
        [arrSecondLabTitle addObject:NSLocalizedString(@"Other Language(s)",@"Basic Step")];
        [arrSecondLabTitle addObject:NSLocalizedString(@"education level",@"Basic Step")];
        if (cityid == 2) {
            [arrSecondLabTitle addObject:NSLocalizedString(@"state",@"Basic Step")];
        }
        
    }
    
    if( arrLastLabTitle == nil )
    {
        arrLastLabTitle = [[NSMutableArray alloc] init];
        [arrLastLabTitle addObject:NSLocalizedString(@"?",@"Basic Step")];
        [arrLastLabTitle addObject:NSLocalizedString(@"?",@"Basic Step")];
        [arrLastLabTitle addObject:NSLocalizedString(@"?",@"Basic Step")];
        [arrLastLabTitle addObject:NSLocalizedString(@" you staying now?",@"Basic Step")];
        [arrLastLabTitle addObject:NSLocalizedString(@" do you speak?",@"Basic Step")];
        [arrLastLabTitle addObject:NSLocalizedString(@" do you speak?",@"Basic Step")];
        [arrLastLabTitle addObject:NSLocalizedString(@"?",@"Basic Step")];
        if (cityid == 2) {
            [arrLastLabTitle addObject:NSLocalizedString(@"you are willing to date?",@"Basic Step")];
        } 
    }
    
    if( arrNameOfInfoList == nil )
    {
        arrNameOfInfoList = [[NSMutableArray alloc] init];
        [arrNameOfInfoList addObject:[NSString stringWithFormat:@"marital_status"]];
        [arrNameOfInfoList addObject:[NSString stringWithFormat:@"ethnicity"]];
//        int cityid =[eSyncronyAppDelegate sharedInstance].countryId+1;
        switch (cityid) {
            case 1:{
                [arrNameOfInfoList addObject:[NSString stringWithFormat:@"nationalityA"]];
                break;
            }
            case 2:{
                [arrNameOfInfoList addObject:[NSString stringWithFormat:@"nationalityB"]];
                break;
            }
            case 3:{
                [arrNameOfInfoList addObject:[NSString stringWithFormat:@"nationalityC"]];
                break;
            }
            case 8:
            {
                [arrNameOfInfoList addObject:[NSString stringWithFormat:@"nationalityD"]];
                break;
            }
            default:
                [arrNameOfInfoList addObject:[NSString stringWithFormat:@"nationalityA"]];
                break;
        }
        
        [arrNameOfInfoList addObject:[NSString stringWithFormat:@"state"]];
        [arrNameOfInfoList addObject:[NSString stringWithFormat:@"secondary_language"]];
        [arrNameOfInfoList addObject:[NSString stringWithFormat:@"secondary_language"]];
        [arrNameOfInfoList addObject:[NSString stringWithFormat:@"education"]];
        if (cityid == 2) {
             [arrNameOfInfoList addObject:[NSString stringWithFormat:@"state_malaysia"]];
        }
    }
    
    if( self.nStep == 2 )
    {
        self.arrSelectedRowIdx = nil;
        self.arrSelectedRowIdx = [[NSMutableArray alloc] initWithCapacity:0];
        for( int i = 0; i < 5; i++ )
        {
            [self.arrSelectedRowIdx addObject:[NSIndexPath indexPathForItem:-1 inSection:0]];
        }

        [self.arrSelectedRowIdx addObject:[NSMutableArray array]];
        [self.arrSelectedRowIdx addObject:[NSIndexPath indexPathForItem:-1 inSection:0]];
        [self.arrSelectedRowIdx addObject:[NSMutableArray array]];
//        NSLog(@"%@",dicLifeStyleData);
    }
    
    [scrlView addSubview:subViewSteps];
    scrlView.contentSize = subViewSteps.frame.size;
    
    [self setContentsToStep];
}

- (IBAction)didClickBack:(id)sender
{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)resizeTableView
{
    int maxCount = ([UIScreen mainScreen].bounds.size.height - 142)/42;
    
    if( [arrQuestion count] < maxCount )
    {
        [tableView setFrame:CGRectMake(tableView.frame.origin.x, tableView.frame.origin.y, tableView.frame.size.width, 42 * [arrQuestion count])];
    }
    else
    {
        [tableView setFrame:CGRectMake(tableView.frame.origin.x, tableView.frame.origin.y, tableView.frame.size.width, 42 * maxCount )];
    }
}

- (void)setContentsToStep
{
    if( self.nStep == 7 || self.nStep == 9)//append state_id
        tableView.allowsMultipleSelection = YES;
    else
        tableView.allowsMultipleSelection = NO;

    NSString* strFirst = [arrFirstLabTitle objectAtIndex:self.nStep - 2];
    NSString* strSecond = [arrSecondLabTitle objectAtIndex:self.nStep - 2];
    NSString* strLast = [arrLastLabTitle objectAtIndex:self.nStep - 2];

    float start_pos = 0.0f;

    labFirst.text = strFirst;
    labSecond.text = strSecond;
    labLast.text = strLast;

    [labFirst sizeToFit];
    [labSecond sizeToFit];
    [labLast sizeToFit];

    float total_len = [labFirst bounds].size.width + [labSecond bounds].size.width + [labLast bounds].size.width;
    
    start_pos = (320 - total_len) / 2;
    
    labFirst.frame = CGRectMake(start_pos, labFirst.frame.origin.y, labFirst.frame.size.width, labFirst.frame.size.height);
    labSecond.frame = CGRectMake(labFirst.frame.size.width + start_pos+1, labFirst.frame.origin.y, labSecond.frame.size.width, labSecond.frame.size.height);
    labLast.frame = CGRectMake(labFirst.frame.size.width + labSecond.frame.size.width + start_pos+1, labLast.frame.origin.y, labLast.frame.size.width, labLast.frame.size.height);

    NSString*       strPlist = [arrNameOfInfoList objectAtIndex:self.nStep-2];
//    int cityid =[eSyncronyAppDelegate sharedInstance].countryId+1;
    if ([strPlist isEqualToString:@"state"]) {
       
        switch (cityid) {
            case 1:{
//                arrQuestion = [[NSArray alloc]initWithObjects: nil];
                arrQuestion = [[NSArray alloc]initWithObjects: @[],nil];
                break;
            }
            case 2:{
                arrQuestion = [[NSArray alloc] initWithArray:[Global loadQuestionArray:@"state_malaysia"]];
                break;
            }
            case 3:{
                arrQuestion = [[NSArray alloc] initWithArray:[Global loadQuestionArray:@"state_hk"]];
                break;
            }
            case 8:
            {
                arrQuestion = [[NSArray alloc]initWithObjects: @[],nil];
                break;
            }
            case 203:
            {
                arrQuestion = [[NSArray alloc]initWithObjects: @[],nil];
                break;
            }
            default:
                arrQuestion = [[NSArray alloc]initWithObjects: @[],nil];
                break;
        }
        
    }else if ([strPlist isEqualToString:@"state_malaysia"]) {
        checkSelectedBtn.hidden = NO;
        checkSelectLab.hidden   = NO;
        [checkSelectedBtn setImage:[UIImage imageNamed:@"checkBox_n.PNG"] forState:UIControlStateNormal];
        [checkSelectedBtn setImage:[UIImage imageNamed:@"checkBox_d.PNG"] forState:UIControlStateSelected];
        
//        arrState  = [[NSMutableArray alloc]initWithObjects:nil];
        arrState  = [[NSMutableArray alloc]initWithObjects:@[],nil];
        arrState = [@[] mutableCopy];
        
        arrQuestion = [[NSMutableArray alloc] initWithArray:[Global loadQuestionArray:strPlist]];
    }
    else{
        arrQuestion = [[NSMutableArray alloc] initWithArray:[Global loadQuestionArray:strPlist]];
    }
    
    [tableView reloadData];

    if (cityid == 2) {
        if( self.nStep == 7 || self.nStep == 9)
        {
            NSMutableArray* arrTmp = [self.arrSelectedRowIdx objectAtIndex:self.nStep - 2];
            
            for( int i = 0; i < arrTmp.count; i++ )
            {
                NSIndexPath* indPath = [arrTmp objectAtIndex:i];
                if ( indPath.row >= 0 ) {
                    [tableView selectRowAtIndexPath:indPath animated:NO scrollPosition:UITableViewScrollPositionNone];
                }
            }
        }
        else
        {
            NSIndexPath* indPath = [self.arrSelectedRowIdx objectAtIndex:self.nStep - 2];
            if( indPath.row >= 0 )
                [tableView selectRowAtIndexPath:indPath animated:NO scrollPosition:UITableViewScrollPositionNone];
        }
    }else{
        if( self.nStep == 7)
        {
            NSMutableArray* arrTmp = [self.arrSelectedRowIdx objectAtIndex:self.nStep - 2];
            
            for( int i = 0; i < arrTmp.count; i++ )
            {
                NSIndexPath* indPath = [arrTmp objectAtIndex:i];
                if ( indPath.row >= 0 ) {
                    [tableView selectRowAtIndexPath:indPath animated:NO scrollPosition:UITableViewScrollPositionNone];
                }
            }
        }
        else
        {
            NSIndexPath* indPath = [self.arrSelectedRowIdx objectAtIndex:self.nStep - 2];
            if( indPath.row >= 0 )
                [tableView selectRowAtIndexPath:indPath animated:NO scrollPosition:UITableViewScrollPositionNone];
        }
    }
   [self resizeTableView];
}

- (void)saveBasicInfoProc
{
   // BasicViewController*    basicView = [BasicViewController sharedView];
   // NSString*   gender = [basicView getGenderType];
   // NSString*   dob = [basicView getDOB];
    
    int     nSelIndex[6];
    
    for( int i = 0; i < 6; i++ )
    {
        NSIndexPath* indPath = nil;
        
        if( i == 5 )
            indPath =  [self.arrSelectedRowIdx objectAtIndex:6];
        else
            indPath =  [self.arrSelectedRowIdx objectAtIndex:i];
        
        nSelIndex[i] = (int)indPath.row;
    }

    NSArray*        _arrayNationM;
//    int cityid =[eSyncronyAppDelegate sharedInstance].countryId+1;
    switch (cityid) {
        case 1:{
            _arrayNationM = [Global loadQuestionArray:@"nationality1"];
            break;
        }
        case 2:{
            _arrayNationM = [Global loadQuestionArray:@"nationality2"];
            break;
        }
        case 3:{
            _arrayNationM = [Global loadQuestionArray:@"nationality3"];
            break;
        }
        case 8:{
            _arrayNationM = [Global loadQuestionArray:@"nationality4"];
            break;
        }
        default:
            _arrayNationM = [Global loadQuestionArray:@"nationality1"];
            break;
    }
    
    nSelIndex[2] = [[_arrayNationM objectAtIndex:nSelIndex[2]] intValue];

    NSArray*        arrState_id;

    switch (cityid) {
        case 1:{
            arrState_id = [[NSArray alloc]initWithObjects: @"0",nil];
            break;
        }
        case 2:{
            arrState_id = [[NSArray alloc]initWithObjects: @"1",@"4",@"9",@"12",nil];
            break;
        }
        case 3:{
            arrState_id = [[NSArray alloc]initWithObjects: @"15",@"16",@"17",nil];
            break;
        }
        case 8://append indonesia
        {
            arrState_id = [[NSArray alloc]initWithObjects: @"0",nil];
            break;
        }
        default:
            arrState_id = [[NSArray alloc]initWithObjects: @"0",nil];
            break;
    }
    if (nSelIndex[3]>=0) {
        nSelIndex[3] = [[arrState_id objectAtIndex:nSelIndex[3]] intValue];
    }else{
        nSelIndex[3] = 0;
    }
    
    // slanguage
    NSMutableArray* arrTmp = [self.arrSelectedRowIdx objectAtIndex:5];
    NSString*       slanguage = nil;

    for( int i = 0; i < arrTmp.count; i++ )
    {
        NSIndexPath* indPath = [arrTmp objectAtIndex:i];
        
        if( slanguage == nil )
            slanguage = [NSString stringWithFormat:@"%d", (int)indPath.row];
        else
            slanguage = [slanguage stringByAppendingFormat:@",%d", (int)indPath.row];
    }

    //append state_id
    NSMutableArray *arrStateTmp = [self.arrSelectedRowIdx objectAtIndex:7];
    NSLog(@"arrStateTmp == %@",arrStateTmp);
    NSString* state_id = nil;
    for (int i = 0; i < arrStateTmp.count; i++) {
        NSIndexPath *indexpath = [arrStateTmp objectAtIndex:i];
        if (state_id == nil) {
             state_id = [NSString stringWithFormat:@"%d",[[arrState_id objectAtIndex:(int)indexpath.row] intValue]];
//            state_id = [NSString stringWithFormat:@"%d",(int)indPath.row];
        }else{
            state_id = [state_id stringByAppendingFormat:@"|%d",[[arrState_id objectAtIndex:(int)indexpath.row] intValue]];
//            state_id = [state_id stringByAppendingFormat:@"|%d",(int)indPath.row];
        }
    }
    NSLog(@"state_id == %@",state_id);
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];

    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    
    [params setObject:[NSNumber numberWithInt:nSelIndex[0]] forKey:@"mstatus"];
    [params setObject:[NSNumber numberWithInt:nSelIndex[1]] forKey:@"ethnicity"];
    [params setObject:[NSNumber numberWithInt:nSelIndex[2]] forKey:@"nationality"];
    [params setObject:[NSNumber numberWithInt:nSelIndex[3]] forKey:@"state_id"];
    [params setObject:[NSNumber numberWithInt:nSelIndex[4]] forKey:@"planguage"];
    [params setObject:slanguage forKey:@"slanguage"];
    [params setObject:[NSNumber numberWithInt:nSelIndex[5]] forKey:@"education"];
    if (state_id.length == 0) {
        state_id = @"";
    }
    [params setObject:state_id forKey:@"date_place"];
    
    NSLog(@"parms====%@",params);
    
    //NSLog(@"%@",dicLifeStyleData);
    
    [params setObject:[dicLifeStyleData objectForKey:@"religion"] forKey:@"religion"];
    [params setObject:[dicLifeStyleData objectForKey:@"jobtitle"] forKey:@"jobtitle"];
    [params setObject:[dicLifeStyleData objectForKey:@"income"] forKey:@"income"];
    [params setObject:[dicLifeStyleData objectForKey:@"travel"] forKey:@"travel"];
    [params setObject:[dicLifeStyleData objectForKey:@"exercise"] forKey:@"exercise"];
    [params setObject:[dicLifeStyleData objectForKey:@"children_num"] forKey:@"children_num"];
    [params setObject:[dicLifeStyleData objectForKey:@"want_children"] forKey:@"want_children"];
    [params setObject:[dicLifeStyleData objectForKey:@"drinking"] forKey:@"drinking"];
    [params setObject:[dicLifeStyleData objectForKey:@"smoking"] forKey:@"smoking"];
    
//    NSLog(@"parms basic && lifestyle == %@",params);
    [[eSyncronyAppDelegate sharedInstance] saveLoginInfo];
    //append 0113 start quiz repair
    [params setObject:@"setBasicALifestyle" forKey:@"cmd"];

    NSDictionary*    result = [UtilComm saveLifeStyleInfo:params];
    
    //[DSBezelActivityView removeViewAnimated:NO];
    
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Saving Failure"];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( response == nil )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Saving Failure",)];
        return;
    }
    
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [ErrorProc alertSetLifeStyleError:response];
    }
    
    if( [response isEqualToString:@"OK"] )
    {
        [self.navigationController popViewControllerAnimated:NO];
        [self pLifeStyle_moveNextWindow];
        
    }else{
        [ErrorProc alertSetLifeStyleError:response];
        [DSBezelActivityView removeViewAnimated:NO];
    }
}
-(void)pLifeStyle_moveNextWindow
{
//     [self.navigationController popToRootViewControllerAnimated:NO];
    [DSBezelActivityView removeViewAnimated:NO];
    [parentView enterDisplayComplete:1];
}

- (void)saveBasicInfoAndGoToLifeStyle
{
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Saving...",)];
    [self performSelector:@selector(saveBasicInfoProc) withObject:nil afterDelay:0.01];
}

-(void)judgeClickNextSave{
    
    if (cityid == 2) {
        if (self.nStep == 9) {
            [self saveBasicInfoAndGoToLifeStyle];//test
            return;
        }else
        {
            //        int cityid =[eSyncronyAppDelegate sharedInstance].countryId+1;
            if( self.nextView == nil )
                self.nextView = [[BasicStepViewsController alloc] initWithNibName:@"BasicStepViewsController" bundle:nil];
            if (self.nStep == 4) {
                if ((cityid==2 )||(cityid==3)) {//append MY, HK
                    self.nextView.nStep = self.nStep+1;
                }else
                    self.nextView.nStep = self.nStep+2;
            }else
                self.nextView.nStep = self.nStep+1;
            
            self.nextView.parentView = self.parentView;
            
            self.nextView.arrSelectedRowIdx = self.arrSelectedRowIdx;
            self.nextView.dicLifeStyleData = self.dicLifeStyleData;
            
            [self.navigationController pushViewController:self.nextView animated:YES];
//        [self setContentsToStep];
        }
    }else{
        
        if( self.nStep == 8 )
        {
            [self saveBasicInfoAndGoToLifeStyle];//test
            return;
        }
        else
        {
            //        int cityid =[eSyncronyAppDelegate sharedInstance].countryId+1;
            if( self.nextView == nil )
                self.nextView = [[BasicStepViewsController alloc] initWithNibName:@"BasicStepViewsController" bundle:nil];
            if (self.nStep == 4) {
                if ((cityid==2 )||(cityid==3)) {//append MY, HK
                    self.nextView.nStep = self.nStep+1;
                }else
                    self.nextView.nStep = self.nStep+2;
            }else
                self.nextView.nStep = self.nStep+1;
            
            self.nextView.parentView = self.parentView;
            
            self.nextView.arrSelectedRowIdx = self.arrSelectedRowIdx;
            self.nextView.dicLifeStyleData = self.dicLifeStyleData;
            
            [self.navigationController pushViewController:self.nextView animated:YES];
//        [self setContentsToStep];
        }
    }

}

- (IBAction)didClickNext:(id)sender
{
    id arrTmp = [self.arrSelectedRowIdx objectAtIndex:self.nStep-2];
    if(self.nStep != 9){
        
        if( arrTmp == nil || (self.nStep == 7 && [(NSMutableArray*)arrTmp count] == 0) || (self.nStep!=7 && [(NSIndexPath*)arrTmp row] < 0) )
        {
            if (self.nStep == 6) {
                UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Field Required",) message:[NSString stringWithFormat: NSLocalizedString(@"Please select at least one Languages",@"Basic Step")] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",) otherButtonTitles:nil];
                [av show];
                [av release];
            }else
            {
                UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Field Required",) message:[NSString stringWithFormat: NSLocalizedString(@"Please select your %@",),[arrSecondLabTitle objectAtIndex:self.nStep - 2]] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",) otherButtonTitles:nil];
                [av show];
                [av release];
            }
            return;
        }
    }

    [self judgeClickNextSave];
}

-(void)basicStep_actionNext
{
    //append didclickNext
    if (self.nStep == 7 || self.nStep == 9) {//append state_id
        
        clickNext.hidden = NO;
        return;
    }
    if ((self.nStep !=7)&&(self.nStep != 9)) {
        
        id arrTmp = [self.arrSelectedRowIdx objectAtIndex:self.nStep-2];
        
        if( arrTmp == nil || (self.nStep == 7 && [(NSMutableArray*)arrTmp count] == 0) || (self.nStep!=7 && [(NSIndexPath*)arrTmp row] < 0) )
        {
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Field Required",) message:[NSString stringWithFormat: NSLocalizedString(@"Please select your %@",),[arrSecondLabTitle objectAtIndex:self.nStep - 2]] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",) otherButtonTitles:nil];
            [av show];
            [av release];
            return;
        }
        [self judgeClickNextSave];
    }
}

#pragma tableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [arrQuestion count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 42;
}

- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        RegisterTableViewCell2 *cell;
        
        NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"RegisterTableViewCell2" owner:nil options:nil];
        cell = [arr objectAtIndex:0];
        
        NSInteger row = [indexPath row];

        //NSDictionary *_storeObj =[arrFriendList objectAtIndex:row];
        NSString * str =  [arrQuestion objectAtIndex:row];
        cell.lbContent.text = str;

        return cell;
}

- (void)tableView:(UITableView *)tableV didDeselectRowAtIndexPath:(NSIndexPath *)newIndexPath
{

    int row = (int)[newIndexPath row];
    [tableV selectRowAtIndexPath:newIndexPath animated:NO scrollPosition:UITableViewScrollPositionNone];

    if (self.nStep == 9) {
        checkSelectedBtn.selected = NO;
        if ([self.arrSelectedRowIdx objectAtIndex:self.nStep-2]!= nil) {
            for (NSIndexPath*_ip in arrState) {
                if (newIndexPath.row ==_ip.row) {//fix
                    [tableV deselectRowAtIndexPath:_ip animated:NO];
                    [arrState removeObject:_ip];
                    break;
                }
            }

            [self.arrSelectedRowIdx replaceObjectAtIndex:self.nStep-2 withObject:arrState];
        }
        
    }

    if( self.nStep == 7 )//|| self.nStep == 9
    {
        if( [self.arrSelectedRowIdx objectAtIndex:self.nStep-2] != nil )
        {
            NSMutableArray* arr = [self.arrSelectedRowIdx objectAtIndex:self.nStep-2];
            for ( NSIndexPath* _ip  in arr )
            {
                if (row == _ip.row)
                {
                    [tableV deselectRowAtIndexPath:_ip animated:NO];
                    [arr removeObject:_ip];
                    break;
                }
            }
            [self.arrSelectedRowIdx replaceObjectAtIndex:self.nStep-2  withObject:arr];
        }
    }
}

- (void)tableView:(UITableView *)tableV didSelectRowAtIndexPath:(NSIndexPath *)newIndexPath
{
    int row = (int)[newIndexPath row];
    
    if (self.nStep == 9) {
        
        if ([self.arrSelectedRowIdx objectAtIndex:self.nStep-2] != nil) {
            arrState = [self.arrSelectedRowIdx objectAtIndex:self.nStep-2];
            [arrState addObject:newIndexPath];
            [self.arrSelectedRowIdx replaceObjectAtIndex:self.nStep-2 withObject:arrState];
            if (arrState.count == arrQuestion.count) {
                checkSelectedBtn.selected = YES;
                
            }
        }
        else{
            arrState = [NSMutableArray arrayWithObjects:newIndexPath, nil];
            [self.arrSelectedRowIdx replaceObjectAtIndex:self.nStep-2 withObject:arrState];
        }
    }else{
    
        if (self.nStep == 7){// || self.nStep == 9
            [tableV selectRowAtIndexPath:newIndexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
            if( [self.arrSelectedRowIdx objectAtIndex:self.nStep-2] != nil )
            {
                NSMutableArray* arr = [self.arrSelectedRowIdx objectAtIndex:self.nStep-2];
                [arr addObject:newIndexPath];
                [self.arrSelectedRowIdx replaceObjectAtIndex:self.nStep-2  withObject:arr];
            }
            else
            {
                NSMutableArray* arr = [NSMutableArray arrayWithObjects:newIndexPath, nil];
                [self.arrSelectedRowIdx replaceObjectAtIndex:self.nStep-2  withObject:arr];
            }
        }else{
            
            if( [self.arrSelectedRowIdx objectAtIndex:self.nStep-2] != nil )
            {
                NSIndexPath* ip = [self.arrSelectedRowIdx objectAtIndex:self.nStep - 2];
                [tableV deselectRowAtIndexPath:ip animated:NO];
                
                if( row == ip.row )
                {
                    [self.arrSelectedRowIdx replaceObjectAtIndex:self.nStep-2  withObject:[NSIndexPath indexPathForItem:-1 inSection:0]];
                    return;
                }
                else
                    [self.arrSelectedRowIdx replaceObjectAtIndex:self.nStep-2  withObject:newIndexPath];
            }
            else
            {
                [self.arrSelectedRowIdx replaceObjectAtIndex:self.nStep-2  withObject:newIndexPath];
            }
            
            [tableV selectRowAtIndexPath:newIndexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
        }
    }
    [self basicStep_actionNext];
  
}
- (IBAction)onClickcheckBtn:(UIButton *)sender {
    sender.selected = !sender.selected;
    clickNext.hidden = NO;
    if (sender.selected) {
        NSLog(@"Select ALL");
        for (int row = 0; row < arrQuestion.count; row++) {
            NSIndexPath *indexpath = [NSIndexPath indexPathForRow:row inSection:0];
            [tableView selectRowAtIndexPath:indexpath animated:NO scrollPosition:UITableViewScrollPositionNone];
            RegisterTableViewCell2 *cell = [tableView cellForRowAtIndexPath:indexpath];
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            NSLog(@"line is %d",(int)indexpath.row);
            cell.selected = YES;
            if ([self.arrSelectedRowIdx objectAtIndex:self.nStep - 2]!= nil) {
                arrState = [self.arrSelectedRowIdx objectAtIndex:self.nStep-2];
                [arrState addObject:indexpath];
                [self.arrSelectedRowIdx replaceObjectAtIndex:self.nStep-2 withObject:arrState];
            }else{
                arrState = [NSMutableArray arrayWithObjects:indexpath, nil];
                [self.arrSelectedRowIdx replaceObjectAtIndex:self.nStep-2  withObject:arrState];
            }
        }
    }else
    {
        NSLog(@"Cancel select all");
        for (int row = 0; row < arrQuestion.count; row++) {
            NSIndexPath *indexpath = [NSIndexPath indexPathForRow:row inSection:0];
            [tableView selectRowAtIndexPath:indexpath animated:NO scrollPosition:UITableViewScrollPositionNone];
            RegisterTableViewCell2 *cell = [tableView cellForRowAtIndexPath:indexpath];
            cell.accessoryType = UITableViewCellAccessoryNone;
//            cell.selected = NO;
            if ([self.arrSelectedRowIdx objectAtIndex:self.nStep - 2]!= nil) {
                [tableView deselectRowAtIndexPath:indexpath animated:NO];
                [arrState removeObject:indexpath];
            }
            [self.arrSelectedRowIdx replaceObjectAtIndex:self.nStep-2 withObject:arrState];
        }
    }
}
-(void)dealloc
{
    [arrNameOfInfoList release];
    [arrQuestion release];

    [scrlView release];
    [subViewSteps release];
    [subViewTitle release];
    [tableView release];
    
    [labFirst release];
    [labSecond release];
    [labLast release];
    
    [arrFirstLabTitle release];
    [arrSecondLabTitle release];
    [arrLastLabTitle release];
    
    [self.nextView release];
    
    if( self.nStep == 2 )
        [self.arrSelectedRowIdx release];
    
    [clickNext release];
    [checkSelectedBtn release];
    [checkSelectLab release];
    [super dealloc];
}

@end

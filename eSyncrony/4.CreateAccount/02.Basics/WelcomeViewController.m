//
//  WelcomeViewController.m
//  eSyncrony
//
//  Created by iosdeveloper on 14-5-29.
//  Copyright (c) 2014年 WonMH. All rights reserved.
//

#import "WelcomeViewController.h"
#import "eSyncronyViewController.h"
#import "eSyncronyAppDelegate.h"
@interface WelcomeViewController ()

@end

@implementation WelcomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    self.screenName = @"WelcomeView";
    self.lblWelcome.text = [NSString stringWithFormat:NSLocalizedString(@"Hi %@! Nice to meet you.",@"lblWelcome"),[eSyncronyAppDelegate sharedInstance].strName];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_lblWelcome release];
    [super dealloc];
}
- (IBAction)didGoBasics:(id)sender {
    //[self.parentView enterBasicView];
     [self.parentView enter20ReasonsView];
}
@end

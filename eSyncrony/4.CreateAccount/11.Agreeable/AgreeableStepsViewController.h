
#import <UIKit/UIKit.h>
#import "eSyncronyViewController.h"

@interface AgreeableStepsViewController : GAITrackedViewController
{
    IBOutlet UIScrollView*      scrView;
    IBOutlet UIView*            subView1;
    
    IBOutlet UILabel*           lbQuestion;
    
    IBOutlet UIButton*          btnBack;
    
    IBOutlet UIButton*          btnLevel1;
    IBOutlet UIButton*          btnLevel2;
    IBOutlet UIButton*          btnLevel3;
    IBOutlet UIButton*          btnLevel4;
    IBOutlet UIButton*          btnLevel5;
    
    NSMutableArray*             arrStringForQuestion;
}

@property (nonatomic, assign) int step;
@property (nonatomic, assign) AgreeableStepsViewController* nextView;

@property (nonatomic, assign) eSyncronyViewController* parentView;


- (IBAction)didClickBtnBack:(id)sender;
- (IBAction)didClickBtnNext:(id)sender;

- (IBAction)didClickLevel1:(id)sender;
- (IBAction)didClickLevel2:(id)sender;
- (IBAction)didClickLevel3:(id)sender;
- (IBAction)didClickLevel4:(id)sender;
- (IBAction)didClickLevel5:(id)sender;

- (void)setContentsToStep;

@end

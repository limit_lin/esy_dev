
#import "Global.h"
#import "UtilComm.h"
#import "DSActivityView.h"
#import "eSyncronyAppDelegate.h"
#import "AgreeableStepsViewController.h"
#import "eSyncronyViewController.h"

@interface AgreeableStepsViewController ()

@end

@implementation AgreeableStepsViewController

@synthesize parentView;
@synthesize step;

int     _agreeable_arrSelIndexs[10];

- (void)viewDidLoad
{
    [super viewDidLoad];//151125
    self.screenName = @"AgreeablenessStepsView";
    
    [scrView addSubview:subView1];
    
    scrView.contentSize = subView1.bounds.size;

    if( arrStringForQuestion == nil )
    {
        arrStringForQuestion = [[NSMutableArray alloc] init];
        [arrStringForQuestion addObject:NSLocalizedString(@"I treat all people equally and with respect.",@"AgreeablenessSteps")];
//        [arrStringForQuestion addObject:NSLocalizedString(@"I am being very blunt and direct with your words.",@"AgreeablenessSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"I am blunt and direct with my words.", @"AgreeablenessSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"I believe that others have good intentions.",@"AgreeablenessSteps")];
//        [arrStringForQuestion addObject:NSLocalizedString(@"I hold a grudge.",@"AgreeablenessSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"I hold grudges.",@"AgreeablenessSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"I sympathize with others' feelings.",@"AgreeablenessSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"I contradict others.",@"AgreeablenessSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"I accept people as they are.",@"AgreeablenessSteps")];
//        [arrStringForQuestion addObject:NSLocalizedString(@"I make demands on others.",@"AgreeablenessSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"I am demanding.",@"AgreeablenessSteps")];
//        [arrStringForQuestion addObject:NSLocalizedString(@"I am easy to satisfy.",@"AgreeablenessSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"I am easily satisfied.",@"AgreeablenessSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"I suspect hidden motives in others.",@"AgreeablenessSteps")];
    }
    
    if( self.step == 1 )
    {
        [btnBack setHidden:YES];
    
        for( int i = 0; i < 10; i++ )
            _agreeable_arrSelIndexs[i] = -1;
    }
    
    [self setContentsToStep];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    subView1.frame = scrView.bounds;
    scrView.contentSize = subView1.bounds.size;
}

- (IBAction)didClickBtnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)checkValidChoice
{
    int     nSel = _agreeable_arrSelIndexs[step-1];
    
    if( nSel >= 0 && nSel < 5 )
        return TRUE;
    
    [ErrorProc alertMessage:NSLocalizedString(@"Please select your choice",) withTitle:NSLocalizedString(@"Field Required",)];
    
    return FALSE;
}

- (void)saveAgreeableStepInfoProc
{
//    NSString*   accNo = [eSyncronyAppDelegate sharedInstance].strAccNo;
//    NSString*   tokenKey = [eSyncronyAppDelegate sharedInstance].strTokenKey;
    
    //--------------------------------------------------------------//
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    
    int     nValues[10];
    int     pTable[2][5] = { {10, 8, 5, 3, 0}, {0, 3, 5, 8, 10} };
    
    for( int i = 0; i < 10; i++ )
    {
        if( i %2 == 0 )
            nValues[i] = pTable[0][_agreeable_arrSelIndexs[i]];
        else
            nValues[i] = pTable[1][_agreeable_arrSelIndexs[i]];
    }
    
    for( int i = 0; i < 10; i++ )
    {
        NSString*   keyValue = [NSString stringWithFormat:@"Q%d", i+1];
        
        [params setObject:[NSNumber numberWithInt:nValues[i]] forKey:keyValue];
    }
    
    NSDictionary*    result = [UtilComm saveAgreeableInfo:params];
    [DSBezelActivityView removeViewAnimated:NO];
    
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Saving Failure"];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( response == nil )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Saving Failure",)];
        return;
    }
    
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [ErrorProc alertSetAgreeablenessError:response];
    }
    else
    {
        [self moveNextWindow];
    }
}

-(void)moveNextWindow
{
   // [self.navigationController popToRootViewControllerAnimated:NO];
    [parentView enterDisplayComplete:7];
}

- (void)saveStepInfoAndMoveNext
{
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Saving...",)];
    [self performSelector:@selector(saveAgreeableStepInfoProc) withObject:nil afterDelay:0.01];
}

- (IBAction)didClickBtnNext:(id)sender
{
    if( [self checkValidChoice] == FALSE )
        return;
    
    if( step == 10 )
    {
        [self saveStepInfoAndMoveNext];
        return;
    }
    
    if( self.nextView == nil )
        self.nextView = [[AgreeableStepsViewController alloc] initWithNibName:@"AgreeableStepsViewController" bundle:nil];
    
    self.nextView.parentView = self.parentView;
    self.nextView.step = self.step+1;

    [self.navigationController pushViewController:self.nextView animated:YES];
}

-(void)agreeableSteps_actionNext
{
    if( [self checkValidChoice] == FALSE )
        return;
    
    if( step == 10 )
    {
        [self saveStepInfoAndMoveNext];
        return;
    }
    
    if( self.nextView == nil )
        self.nextView = [[AgreeableStepsViewController alloc] initWithNibName:@"AgreeableStepsViewController" bundle:nil];
    
    self.nextView.parentView = self.parentView;
    self.nextView.step = self.step+1;
    
    [self.navigationController pushViewController:self.nextView animated:YES];
}

- (void)selectChosenLevel
{
    int     nSel = _agreeable_arrSelIndexs[step-1];
    
    UIButton*   btnLevels[] = { btnLevel1, btnLevel2, btnLevel3, btnLevel4, btnLevel5 };
    
    for( int i = 0; i < 5; i++ )
        [btnLevels[i] setSelected:NO];
    
    if( nSel >= 0 && nSel < 5 )
        [btnLevels[4-nSel] setSelected:YES];//nSel-->4-nSel
}

-(void) setContentsToStep
{
    lbQuestion.text = [arrStringForQuestion objectAtIndex:step-1];
    
    [self selectChosenLevel];
}

- (void)selectLevel:(int)level
{
    _agreeable_arrSelIndexs[step-1] = level;
    
    [self selectChosenLevel];
}

- (IBAction)didClickLevel1:(id)sender
{
//    [self selectLevel:0];
    [self selectLevel:4];
    [self agreeableSteps_actionNext];
}

- (IBAction)didClickLevel2:(id)sender
{
//    [self selectLevel:1];
    [self selectLevel:3];
    [self agreeableSteps_actionNext];
}

- (IBAction)didClickLevel3:(id)sender
{
    [self selectLevel:2];
    [self agreeableSteps_actionNext];
}

- (IBAction)didClickLevel4:(id)sender
{
//    [self selectLevel:3];
    [self selectLevel:1];
    [self agreeableSteps_actionNext];
}

- (IBAction)didClickLevel5:(id)sender
{
//    [self selectLevel:4];
    [self selectLevel:0];
    [self agreeableSteps_actionNext];
}

-(void)dealloc
{
    [scrView release];
    [subView1 release];
    
    [lbQuestion release];
    [btnBack release];
    [arrStringForQuestion release];
    
    [btnLevel1 release];
    [btnLevel2 release];
    [btnLevel3 release];
    [btnLevel4 release];
    [btnLevel5 release];
    
    [self.nextView release];
    
    [super dealloc];
}

@end

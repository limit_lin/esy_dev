
#import <UIKit/UIKit.h>
#import "InputBaseViewController.h"

@class eSyncronyViewController;
@interface MyStoryStepsViewController : InputBaseViewController<UITextViewDelegate>
{
    //mainView
    IBOutlet UIScrollView*      scrView;
    IBOutlet UIButton*          btnBack;

    //subView
    IBOutlet UIView*            subView1;
    IBOutlet UIView*            subView2;
    IBOutlet UILabel*           txtQuestionTitle1;
    IBOutlet UILabel*           txtQuestionTitle2;
    
    //contents
    IBOutlet UITextView*        txtContent1_1;
    IBOutlet UITextView*        txtContent1_2;
    IBOutlet UITextView*        txtContent1_3;
    
    IBOutlet UITextView*        txtContent2;
    
    NSMutableArray*             arrStringForQuestion;
    NSMutableArray*             arrSubViews;

    NSRange     colorRange[3];
}

@property (nonatomic, assign) int step;
@property (nonatomic, assign) NSMutableArray*             arrStoryContents;
@property (nonatomic, assign) MyStoryStepsViewController* nextView;
@property (nonatomic, assign) eSyncronyViewController* parentView;

- (IBAction)didClickBtnBack:(id)sender;
- (IBAction)didClickBtnNext:(id)sender;

@end


#import "Global.h"
#import "UtilComm.h"
#import "DSActivityView.h"
#import "eSyncronyAppDelegate.h"
#import "MyStoryStepsViewController.h"
#import "eSyncronyViewController.h"

@interface MyStoryStepsViewController ()

@end

@implementation MyStoryStepsViewController

@synthesize parentView;
@synthesize step;
@synthesize arrStoryContents;

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.screenName = @"MyStoryStepsView";
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    if( arrStringForQuestion == nil)
    {
        arrStringForQuestion = [[NSMutableArray alloc] init];
        [arrStringForQuestion addObject:NSLocalizedString(@"What are the 3 things you are most thankful for",@"MyStorySteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"Who is the most influential Person in your life",@"MyStorySteps")];
//        [arrStringForQuestion addObject:NSLocalizedString(@"What are the 3 things your friends know you as",@"MyStorySteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"What are 3 words that your friends use to describe you",@"MyStorySteps")];
        
        if ([language hasPrefix:@"zh-Hant"]||[language hasPrefix:@"zh-HK"]) {//chinese zh-Hant
            
            colorRange[0] = NSMakeRange(0, 12);
            colorRange[1] = NSMakeRange(0, 13);
            colorRange[2] = NSMakeRange(0, 19);

        }
        else if ([language hasPrefix:@"id"])
        {
            colorRange[0] = NSMakeRange(4, 1);
            colorRange[1] = NSMakeRange(24, 11);
            colorRange[2] = NSMakeRange(4, 1);
        }
        else
        {
            colorRange[0] = NSMakeRange(13, 1);
            colorRange[1] = NSMakeRange(16, 11);
            colorRange[2] = NSMakeRange(9, 1);
        }
        
    }

    if( arrSubViews == nil )
    {
        arrSubViews = [[NSMutableArray alloc] init];
        [arrSubViews addObject:subView1];
        [arrSubViews addObject:subView2];
        [arrSubViews addObject:subView1];
    }
    
    if( self.step == 1 )
    {
        arrStoryContents = [[NSMutableArray alloc] initWithCapacity:0];
        for( int i = 0; i < 3; i++ )
        {
            NSArray*    storyItem = [NSArray arrayWithObjects:@"", @"", @"", nil];
            [arrStoryContents addObject:storyItem];
        }
        
        [btnBack setHidden:YES];
    }
    else
        [btnBack setHidden:NO];

    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnTableView:)];
    tapRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapRecognizer];

    [self setContentsToStep];
}

- (void)tapOnTableView:(UITapGestureRecognizer *)gesture
{
    [self.view endEditing:YES];
    [self restoreViewPosition];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    subView1.frame = scrView.bounds;
    subView2.frame = scrView.bounds;
}

- (BOOL)checkValidInput
{
    if( step%2 == 1 )
    {
        NSString *str1, *str2, *str3;
        
        str1 = txtContent1_1.text;
        str2 = txtContent1_2.text;
        str3 = txtContent1_3.text;
        
        if( [str1 isEqualToString:@""] || [str2 isEqualToString:@""] || [str3 isEqualToString:@""] )
            return NO;
        
        NSArray*    storyItem = [NSArray arrayWithObjects:str1, str2, str3, nil];
        [arrStoryContents setObject:storyItem atIndexedSubscript:step-1];
    }
    else
    {
        if( [txtContent2.text isEqualToString:@""] )
            return NO;

        NSArray*    storyItem = [NSArray arrayWithObjects:txtContent2.text, nil];
        [arrStoryContents setObject:storyItem atIndexedSubscript:step-1];
    }
    
    return YES;
}

- (void)saveStepInfo
{
    if( step%2 == 1 )
    {
        NSString *str1, *str2, *str3;
        
        str1 = txtContent1_1.text;
        str2 = txtContent1_2.text;
        str3 = txtContent1_3.text;

        
        NSArray*    storyItem = [NSArray arrayWithObjects:str1, str2, str3, nil];
        [arrStoryContents setObject:storyItem atIndexedSubscript:step-1];
    }
    else
    {
        NSArray*    storyItem = [NSArray arrayWithObjects:txtContent2.text, nil];
        [arrStoryContents setObject:storyItem atIndexedSubscript:step-1];
    }
}

- (IBAction)didClickBtnBack:(id)sender
{
    [self saveStepInfo];

    [self.navigationController popViewControllerAnimated:YES];
}

- (void)saveStoryInfoProc
{

    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    
    NSString    *story[3];
    
    for( int i = 0; i < 3; i++ )
    {
        story[i] = @"";

        NSArray*    storyItem = [arrStoryContents objectAtIndex:i];
        
        if( i%2 == 0 )
        {
            story[i] = [story[i] stringByAppendingFormat:@"%@,%@,%@",[storyItem objectAtIndex:0], [storyItem objectAtIndex:1], [storyItem objectAtIndex:2]];
        }
        else
        {
            story[i] = [storyItem objectAtIndex:0];
        }
    }
    
    [params setObject:story[0] forKey:@"story1"];
    [params setObject:story[1] forKey:@"story2"];
    [params setObject:story[2] forKey:@"story3"];
    
    [params setObject:@"setMyStory" forKey:@"cmd"];

    NSDictionary*    result = [UtilComm saveStoryInfo:params];
    [DSBezelActivityView removeViewAnimated:NO];
    
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Saving Failure"];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( response == nil )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Saving Failure",)];
        return;
    }

    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [ErrorProc alertSetMyStoryError:response];
    }
    else
    {
        [self moveNextWindow];
    }
}

-(void)moveNextWindow
{
   // [self.navigationController popToRootViewControllerAnimated:NO];
    [parentView enterThreeStepsView];//repair 0811 logic tweaks
//    [parentView enterDisplayComplete:10];
}

- (void)saveStepInfoAndMoveNext
{
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Saving...",)];
    [self performSelector:@selector(saveStoryInfoProc) withObject:nil afterDelay:0.01];
}


- (IBAction)didClickBtnNext:(id)sender
{
    if( [self checkValidInput] == NO )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Please enter your story",@"MyStorySteps") withTitle:NSLocalizedString(@"Field Required",)];
        return;
    }
    
    if( step == 3 )
    {
        [self saveStepInfoAndMoveNext];
        return;
    }

    if( self.nextView == nil )
        self.nextView = [[MyStoryStepsViewController alloc] initWithNibName:@"MyStoryStepsViewController" bundle:nil];
    
    self.nextView.parentView = self.parentView;
    self.nextView.step = self.step+1;
    self.nextView.arrStoryContents = self.arrStoryContents;

    [self.navigationController pushViewController:self.nextView animated:YES];
}

- (void)initTitle
{
    NSString* title = [arrStringForQuestion objectAtIndex:(step - 1)];
    
    NSMutableAttributedString* string = [[NSMutableAttributedString alloc]initWithString:title];

    UIColor*    color = TITLE_REG_COLOR;

    [string addAttribute:NSForegroundColorAttributeName value:color range:colorRange[step-1]];

    if( step%2 == 1 )
        [txtQuestionTitle1 setAttributedText:string];
    else
        [txtQuestionTitle2 setAttributedText:string];
}

- (void)restoreStoryContents
{
    NSArray*    storyItem = [arrStoryContents objectAtIndex:step-1];
    
    if( step%2 == 1 )
    {
        txtContent1_1.text = [storyItem objectAtIndex:0];
        txtContent1_2.text = [storyItem objectAtIndex:1];
        txtContent1_3.text = [storyItem objectAtIndex:2];
    }
    else
        txtContent2.text = [storyItem objectAtIndex:0];
}

- (void)setContentsToStep
{
    // removes before views
    UIView* tmpView = [arrSubViews objectAtIndex:step-1];
    [scrView addSubview:tmpView];
    scrView.contentSize = tmpView.frame.size;

    [self initTitle];
    [self restoreStoryContents];
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    CGRect rect = [textView convertRect:textView.bounds toView:self.view];
    
    float   offset;
    
    offset = self.view.frame.size.height - (rect.size.height + 226);
    offset = offset - rect.origin.y;
    
    if( offset > 0 )
        return;
    
    rect = self.view.frame;
    rect.origin.y = offset;
    
    self.view.frame = rect;
}
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        if( textView.tag == 1001 )
            [txtContent1_2 becomeFirstResponder];
        else if(textView.tag == 1002)
            [txtContent1_3 becomeFirstResponder];
        else if(textView.tag == 1003)
        {
            [textView resignFirstResponder];
            [self restoreViewPosition];
        }
        else
        {
            return YES;
        }
        return NO;
    }
    return YES;
}
-(void)dealloc
{
    [scrView release];
    [btnBack release];

    [subView1 release];
    [subView2 release];
    [txtQuestionTitle1 release];
    [txtQuestionTitle2 release];

    [txtContent1_1 release];
    [txtContent1_2 release];
    [txtContent1_3 release];
    
    [txtContent2 release];

    [arrStringForQuestion release];
    [arrSubViews release];
    
    if( self.step == 1 )
        [arrStoryContents release];

    [self.nextView release];

    [super dealloc];
}

@end

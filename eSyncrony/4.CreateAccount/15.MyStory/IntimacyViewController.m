
#import "Global.h"
#import "UtilComm.h"
#import "DSActivityView.h"
#import "eSyncronyAppDelegate.h"
#import "IntimacyViewController.h"
#import "eSyncronyViewController.h"

@interface IntimacyViewController ()

@end

@implementation IntimacyViewController

int     _relation_arrSelIndexs[10];

@synthesize parentView;
@synthesize step;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.screenName = @"IntimacyStepsView";
    if( arrStringForQuestion == nil )
    {
        arrStringForQuestion = [[NSMutableArray alloc] init];
        [arrStringForQuestion addObject:[NSString stringWithFormat:NSLocalizedString(@"Kissing on the first date is fine.",@"IntimacySteps")]];
        [arrStringForQuestion addObject:[NSString stringWithFormat:NSLocalizedString(@"Our Sexual Compatibility is crucial in our relationship.",@"IntimacySteps")]];
        [arrStringForQuestion addObject:[NSString stringWithFormat:NSLocalizedString(@"Sex within the first 3 date is acceptable.",@"IntimacySteps")]];
        [arrStringForQuestion addObject:[NSString stringWithFormat:NSLocalizedString(@"If I am attached but not yet married, it is ok to go on dates with another person?",@"IntimacySteps")]];
        [arrStringForQuestion addObject:[NSString stringWithFormat:NSLocalizedString(@"I am adventurous and like to experiment.",@"IntimacySteps")]];
        [arrStringForQuestion addObject:[NSString stringWithFormat:NSLocalizedString(@"People would describe me a sensual person.",@"IntimacySteps")]];
        [arrStringForQuestion addObject:[NSString stringWithFormat:NSLocalizedString(@"I prefer my partner to be sexually experienced.",@"IntimacySteps")]];
        [arrStringForQuestion addObject:[NSString stringWithFormat:NSLocalizedString(@"I enjoy being physically close to my partner.",@"IntimacySteps")]];
        [arrStringForQuestion addObject:[NSString stringWithFormat:NSLocalizedString(@"Flirting with the opposite gender is cheating when attached/married.",@"IntimacySteps")]];
        [arrStringForQuestion addObject:[NSString stringWithFormat:NSLocalizedString(@"It is normal to talk and engage in my fantasies with my partner.",@"IntimacySteps")]];
    }
    
    [scrView addSubview:subView];
    scrView.contentSize = subView.frame.size;
    
    if( self.step == 1 )
    {
        [btnBack setHidden:YES];
        
        for( int i = 0; i < 10; i++ )
            _relation_arrSelIndexs[i] = -1;
    }
    
    [self setContentsToStep];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    subView.frame = scrView.bounds;
    scrView.contentSize = subView.frame.size;
}

- (IBAction)didClickBtnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)saveIntimacyProc
{

    //--------------------------------------------------------------//
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    
    int     nQValues[10];
    int     nQTable[2][5] = { {0, 3, 5, 8, 10}, {10, 8, 5, 3, 0} };
    
    for( int i = 0; i < 10; i++ )
    {
        nQValues[i] = nQTable[1][_relation_arrSelIndexs[i]];
    }
    
    [params setObject:[NSNumber numberWithInt:nQValues[0]] forKey:@"Q1"];
    [params setObject:[NSNumber numberWithInt:nQValues[1]] forKey:@"Q2"];
    [params setObject:[NSNumber numberWithInt:nQValues[2]] forKey:@"Q3"];
    [params setObject:[NSNumber numberWithInt:nQValues[3]] forKey:@"Q4"];
    [params setObject:[NSNumber numberWithInt:nQValues[4]] forKey:@"Q5"];
    [params setObject:[NSNumber numberWithInt:nQValues[5]] forKey:@"Q6"];
    [params setObject:[NSNumber numberWithInt:nQValues[6]] forKey:@"Q7"];
    [params setObject:[NSNumber numberWithInt:nQValues[7]] forKey:@"Q8"];
    [params setObject:[NSNumber numberWithInt:nQValues[8]] forKey:@"Q9"];
    [params setObject:[NSNumber numberWithInt:nQValues[9]] forKey:@"Q10"];
    
    NSDictionary*    result = [UtilComm saveIntimacyInfo:params];
    [DSBezelActivityView removeViewAnimated:NO];
    
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Saving Failure"];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( response == nil )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Saving Failure",)];
        return;
    }
    
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [ErrorProc alertSetReadinessError:response];
    }
    else
    {
        
        [self moveNextWindow];
    }
}

//remove 0113 start quiz
-(void)moveNextWindow
{
   // [self.navigationController popToRootViewControllerAnimated:NO];
    
//    [parentView enterStoryStepsView];
//    [parentView enterNricVerifyStepWithValues:nil];//start quiz 0113
    [parentView enterNricVerifyStep];
}

- (void)saveIntimacyAndMoveNext
{
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Saving...",)];
    [self performSelector:@selector(saveIntimacyProc) withObject:nil afterDelay:0.01];
}

- (BOOL)checkValidChoice
{
    int     nSel = _relation_arrSelIndexs[step-1];
    
    if( nSel >= 0 && nSel < 5 )
        return TRUE;
    
    [ErrorProc alertMessage:NSLocalizedString(@"Please select your choice",) withTitle:NSLocalizedString(@"Field Required",)];
    
    return FALSE;
}

- (IBAction)didClickBtnNext:(id)sender
{
    if( [self checkValidChoice] == FALSE )
        return;
    
    if( step == 10 )
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Congratulation",@"IntimacySteps") message:NSLocalizedString(@"Thank you for completing the personality quiz. You are now one step closer to finding your perfect match! Congratulations!\n\nOur dating consultants will be calling you shortly to offer you a free phone consultation. It takes around 5-10 minutes to offer you valuable personal dating advice in your dating journey, better understand your expectations, as well as answer any of your questions.\n\nHere’s a few topics they might touch on:\n1. Your Background\n2. Your Dating Criteria\n3. Personal Dating Tips\n4. Relationship Advice\n\nAs we are experiencing high volume of sign ups, our consultants will be calling you within 1 to 3 days. Kindly bear with us! Thank you and Happy Matching!",@"IntimacySteps") delegate:self cancelButtonTitle:NSLocalizedString(@"OK",) otherButtonTitles:nil, nil];
        alert.tag = 2000;
        [alert show];
        [alert release];
        
        return;
    }
    
    if( self.nextView == nil )
        self.nextView = [[IntimacyViewController alloc] initWithNibName:@"IntimacyViewController" bundle:nil];
    
    self.nextView.parentView = self.parentView;
    self.nextView.step = self.step+1;
    
    [self.navigationController pushViewController:self.nextView animated:YES];
}

-(void)intimacy_actionNext
{
    
    if( [self checkValidChoice] == FALSE )
        return;
    
    if( step == 10 )
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Congratulation",@"IntimacySteps") message:NSLocalizedString(@"Thank you for completing the personality quiz. You are now one step closer to finding your perfect match! Congratulations!\n\nOur dating consultants will be calling you shortly to offer you a free phone consultation. It takes around 5-10 minutes to offer you valuable personal dating advice in your dating journey, better understand your expectations, as well as answer any of your questions.\n\nHere’s a few topics they might touch on:\n1. Your Background\n2. Your Dating Criteria\n3. Personal Dating Tips\n4. Relationship Advice\n\nAs we are experiencing high volume of sign ups, our consultants will be calling you within 1 to 3 days. Kindly bear with us! Thank you and Happy Matching!",@"IntimacySteps") delegate:self cancelButtonTitle:NSLocalizedString(@"OK",) otherButtonTitles:nil, nil];
        alert.tag = 2000;
        [alert show];
        [alert release];
        
        return;
    }
    
    if( self.nextView == nil )
        self.nextView = [[IntimacyViewController alloc] initWithNibName:@"IntimacyViewController" bundle:nil];
    
    self.nextView.parentView = self.parentView;
    self.nextView.step = self.step+1;
    
    [self.navigationController pushViewController:self.nextView animated:YES];
    
}

- (void)selectChosenLevel
{
    int     nSel = _relation_arrSelIndexs[step-1];
    
    UIButton*   btnLevels[] = { btnLevel1, btnLevel2, btnLevel3, btnLevel4, btnLevel5 };
    
    for( int i = 0; i < 5; i++ )
        [btnLevels[i] setSelected:NO];
    
    if( nSel >= 0 && nSel < 5 )
        [btnLevels[nSel] setSelected:YES];
}

-(void) setContentsToStep
{
    txtQuestion.text = [ arrStringForQuestion objectAtIndex:step-1];
    txtQuestion.font = [UIFont fontWithName:@"HelveticaNeueLTStd-MdCn" size:14];
    
    [self selectChosenLevel];
}

- (void)selectLevel:(int)level
{
    _relation_arrSelIndexs[step-1] = level;
    
    UIButton*   btnLevels[] = { btnLevel1, btnLevel2, btnLevel3, btnLevel4, btnLevel5 };
    
    for( int i = 0; i < 5; i++ )
        [btnLevels[i] setSelected:NO];
    
    if( level >= 0 && level < 5 )
        [btnLevels[level] setSelected:YES];
}

- (IBAction)didClickLevel1:(id)sender
{
    [self selectLevel:0];
    [self intimacy_actionNext];
}

- (IBAction)didClickLevel2:(id)sender
{
    [self selectLevel:1];
    [self intimacy_actionNext];
}

- (IBAction)didClickLevel3:(id)sender
{
    [self selectLevel:2];
    [self intimacy_actionNext];
}

- (IBAction)didClickLevel4:(id)sender
{
    [self selectLevel:3];
    [self intimacy_actionNext];
}

- (IBAction)didClickLevel5:(id)sender
{
    [self selectLevel:4];
    [self intimacy_actionNext];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==2000) {
        [self saveIntimacyAndMoveNext];
    }
}

- (void)dealloc
{
    [btnLevel1 release];
    [btnLevel2 release];
    [btnLevel3 release];
    [btnLevel4 release];
    [btnLevel5 release];
    
    [scrView release];
    [subView release];
    
    [txtQuestion release];
    [btnBack release];
    
    [self.nextView release];
    
    [super dealloc];
}

@end

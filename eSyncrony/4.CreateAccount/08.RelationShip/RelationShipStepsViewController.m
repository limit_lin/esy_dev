
#import "Global.h"
#import "UtilComm.h"
#import "DSActivityView.h"
#import "eSyncronyAppDelegate.h"
#import "RelationShipStepsViewController.h"
#import "eSyncronyViewController.h"

@interface RelationShipStepsViewController ()

@end

@implementation RelationShipStepsViewController

int     _relation_arrSelIndexs[10];

@synthesize parentView;
@synthesize step;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"RelationShipStepsView";
    if( arrStringForQuestion == nil )
    {
        arrStringForQuestion = [[NSMutableArray alloc] init];
        [arrStringForQuestion addObject:NSLocalizedString(@"Do you feel skeptical when you look at your engaged friends/family members?",@"RelationShipSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"Do the words 'wedding' and 'marriage' make you picture a stressful planning process and then a monotonous lifetime with the same person?",@"RelationShipSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"Can you spend at least 75% of your time with your partner?",@"RelationShipSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"Can you imagine yourself settling down with someone in 2 years time?",@"RelationShipSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"I believe being in serious relationship means I have to sacrifice my own needs for the relationship.",@"RelationShipSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"I have a close-knit family and I enjoy spending time with my family members.",@"RelationShipSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"Having children is important for an adult to live a fulfilled life.",@"RelationShipSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"Abortion is unacceptable and I would not undergo or allow my partner to undergo abortion.",@"RelationShipSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"A working mother can establish just as warm and secure a relationship as a mother who does not work.",@"RelationShipSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"Cohabitation is a good way to get to know and understand each other better before committing to marriage.",@"RelationShipSteps")];
    }
    
    [scrView addSubview:subView];
    scrView.contentSize = subView.frame.size;
    
    if( self.step == 1 )
    {
        [btnBack setHidden:YES];
    
        for( int i = 0; i < 10; i++ )
            _relation_arrSelIndexs[i] = -1;
    }
    
    [self setContentsToStep];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    subView.frame = scrView.bounds;
    scrView.contentSize = subView.frame.size;
}

- (IBAction)didClickBtnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)saveRelationShipProc
{
//    NSString*   accNo = [eSyncronyAppDelegate sharedInstance].strAccNo;
//    NSString*   tokenKey = [eSyncronyAppDelegate sharedInstance].strTokenKey;
    
    //--------------------------------------------------------------//
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    
    int     nQValues[10];
    int     nQTable[2][5] = { {0, 3, 5, 8, 10}, {10, 8, 5, 3, 0} };
    
    for( int i = 0; i < 10; i++ )
    {
        if( i < 2 ){
            nQValues[i] = nQTable[0][_relation_arrSelIndexs[i]];
        }else if(i>=2||i<=8){
            nQValues[i] = nQTable[1][_relation_arrSelIndexs[i]];
        }else{
            nQValues[i] = nQTable[0][_relation_arrSelIndexs[i]];
        }
    }

    [params setObject:[NSNumber numberWithInt:nQValues[0]] forKey:@"Q1"];
    [params setObject:[NSNumber numberWithInt:nQValues[1]] forKey:@"Q2"];
    [params setObject:[NSNumber numberWithInt:nQValues[2]] forKey:@"Q3"];
    [params setObject:[NSNumber numberWithInt:nQValues[3]] forKey:@"Q4"];
    [params setObject:[NSNumber numberWithInt:nQValues[4]] forKey:@"Q5"];
    [params setObject:[NSNumber numberWithInt:nQValues[5]] forKey:@"Q6"];
    [params setObject:[NSNumber numberWithInt:nQValues[6]] forKey:@"Q7"];
    [params setObject:[NSNumber numberWithInt:nQValues[7]] forKey:@"Q8"];
    [params setObject:[NSNumber numberWithInt:nQValues[8]] forKey:@"Q9"];
    [params setObject:[NSNumber numberWithInt:nQValues[9]] forKey:@"Q10"];

    NSDictionary*    result = [UtilComm saveReadinessInfo:params];
    [DSBezelActivityView removeViewAnimated:NO];
    
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Saving Failure"];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( response == nil )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Saving Failure",)];
        return;
    }
    
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [ErrorProc alertSetReadinessError:response];
    }
    else
    {
        [self moveNextWindow];
    }
}

-(void)moveNextWindow
{
    //[self.navigationController popToRootViewControllerAnimated:NO];
    [parentView enterDisplayComplete:5];
}

- (void)saveRelationShipAndMoveNext
{
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Saving...",)];
    [self performSelector:@selector(saveRelationShipProc) withObject:nil afterDelay:0.01];
}

- (BOOL)checkValidChoice
{
    int     nSel = _relation_arrSelIndexs[step-1];
    
    if( nSel >= 0 && nSel < 5 )
        return TRUE;
    
    [ErrorProc alertMessage:NSLocalizedString(@"Please select your choice",) withTitle:NSLocalizedString(@"Field Required",)];
    
    return FALSE;
}

- (IBAction)didClickBtnNext:(id)sender
{
    if( [self checkValidChoice] == FALSE )
        return;
    
    if( step == 10 )
    {
        [self saveRelationShipAndMoveNext];
        return;
    }
    
    if( self.nextView == nil )
        self.nextView = [[RelationShipStepsViewController alloc] initWithNibName:@"RelationShipStepsViewController" bundle:nil];
    
    self.nextView.parentView = self.parentView;
    self.nextView.step = self.step+1;
    
    [self.navigationController pushViewController:self.nextView animated:YES];
}

-(void)relationshipSteps_actionNext
{
    if( [self checkValidChoice] == FALSE )
        return;
    
    if( step == 10 )
    {
        [self saveRelationShipAndMoveNext];
        return;
    }
    
    if( self.nextView == nil )
        self.nextView = [[RelationShipStepsViewController alloc] initWithNibName:@"RelationShipStepsViewController" bundle:nil];
    
    self.nextView.parentView = self.parentView;
    self.nextView.step = self.step+1;
    
    [self.navigationController pushViewController:self.nextView animated:YES];
}

- (void)selectChosenLevel
{
    int     nSel = _relation_arrSelIndexs[step-1];
    
    UIButton*   btnLevels[] = { btnLevel1, btnLevel2, btnLevel3, btnLevel4, btnLevel5 };
    
    for( int i = 0; i < 5; i++ )
        [btnLevels[i] setSelected:NO];
    
    if( nSel >= 0 && nSel < 5 )
        [btnLevels[nSel] setSelected:YES];
}

-(void) setContentsToStep
{
    txtQuestion.text = [ arrStringForQuestion objectAtIndex:step-1];
    txtQuestion.font = [UIFont fontWithName:@"HelveticaNeueLTStd-MdCn" size:14];

    [self selectChosenLevel];
}

- (void)selectLevel:(int)level
{
    _relation_arrSelIndexs[step-1] = level;
    
    UIButton*   btnLevels[] = { btnLevel1, btnLevel2, btnLevel3, btnLevel4, btnLevel5 };
    
    for( int i = 0; i < 5; i++ )
        [btnLevels[i] setSelected:NO];
    
    if( level >= 0 && level < 5 )
        [btnLevels[level] setSelected:YES];
}

- (IBAction)didClickLevel1:(id)sender
{
    [self selectLevel:0];
    [self relationshipSteps_actionNext];
}

- (IBAction)didClickLevel2:(id)sender
{
    [self selectLevel:1];
    [self relationshipSteps_actionNext];
}

- (IBAction)didClickLevel3:(id)sender
{
    [self selectLevel:2];
    [self relationshipSteps_actionNext];
}

- (IBAction)didClickLevel4:(id)sender
{
    [self selectLevel:3];
    [self relationshipSteps_actionNext];
}

- (IBAction)didClickLevel5:(id)sender
{
    [self selectLevel:4];
    [self relationshipSteps_actionNext];
}

- (void)dealloc
{
    [btnLevel1 release];
    [btnLevel2 release];
    [btnLevel3 release];
    [btnLevel4 release];
    [btnLevel5 release];

    [scrView release];
    [subView release];

    [txtQuestion release];
    [btnBack release];
    
    [self.nextView release];

    [super dealloc];
}

@end


#import <UIKit/UIKit.h>

@class eSyncronyViewController;

@interface RelationShipStepsViewController : GAITrackedViewController
{
    IBOutlet UIScrollView*      scrView;
    IBOutlet UIView*            subView;
    
    IBOutlet UITextView *       txtQuestion;
    IBOutlet UIButton*          btnBack;
    
    IBOutlet UIButton*          btnLevel1;
    IBOutlet UIButton*          btnLevel2;
    IBOutlet UIButton*          btnLevel3;
    IBOutlet UIButton*          btnLevel4;
    IBOutlet UIButton*          btnLevel5;
    
    NSMutableArray*             arrStringForQuestion;
}

@property (nonatomic, assign) int step;
@property (nonatomic, assign) RelationShipStepsViewController* nextView;
@property (nonatomic, assign) eSyncronyViewController* parentView;

- (IBAction)didClickBtnBack:(id)sender;
- (IBAction)didClickBtnNext:(id)sender;

- (IBAction)didClickLevel1:(id)sender;
- (IBAction)didClickLevel2:(id)sender;
- (IBAction)didClickLevel3:(id)sender;
- (IBAction)didClickLevel4:(id)sender;
- (IBAction)didClickLevel5:(id)sender;

@end



#import <UIKit/UIKit.h>
#import "eSyncronyViewController.h"

@interface MatchCriteriaStepsViewController : GAITrackedViewController<UIAlertViewDelegate>
{
    IBOutlet UIScrollView*      scrView;
    IBOutlet UIView*            viewCont;
    
    IBOutlet UIView*            viewQuestionTitle;
    IBOutlet UIView*            viewLevelTitle;
    
    IBOutlet UIView*            viewQuestionTitle1;
    IBOutlet UIView*            viewQuestionTitle2;
    IBOutlet UIView*            viewQuestionTitle3;

    IBOutlet UIView*            viewLevelTitle1;
    IBOutlet UIView*            viewLevelTitle2;
    IBOutlet UIView*            viewLevelTitle3;
    
    IBOutlet UIButton*          btnLevel1;
    IBOutlet UIButton*          btnLevel2;
    IBOutlet UIButton*          btnLevel3;
    IBOutlet UIButton*          btnLevel4;
    IBOutlet UIButton*          btnLevel5;
    
    IBOutlet UIButton*          btnBack;
    
    IBOutlet UILabel*           lbQuestionKind1;
    IBOutlet UILabel*           lbQuestionKind2;
    IBOutlet UILabel*           lbQuestionKind3;
    
    NSMutableArray*             arrQuestionTitleViews;
    NSMutableArray*             arrLevelTitleViews;
    
    NSMutableArray*             arrIndexForView;
    NSMutableArray*             arrStringForQuestion;
    NSMutableArray*             arrLbViewForQuestion;
    
    NSMutableArray*             arrWarnMessage;
    int judgeNum;
    
}

@property(nonatomic, assign) int step;
@property(nonatomic, assign) int year;//append 1126
@property(nonatomic, assign) MatchCriteriaStepsViewController* nextView;
@property(nonatomic,assign) eSyncronyViewController* parentView;

@property (nonatomic, retain)	NSArray			*colorSchemes;
@property (nonatomic, retain)	NSDictionary	*contents;
@property (nonatomic, retain)	id				currentPopTipViewTarget;
@property (nonatomic, retain)	NSMutableArray	*visiblePopTipViews;
@property (retain, nonatomic) IBOutlet UILabel *lblLv1;
@property (retain, nonatomic) IBOutlet UILabel *lblLv2;
@property (retain, nonatomic) IBOutlet UILabel *lblLv3;
@property (retain, nonatomic) IBOutlet UILabel *lblLv4;
@property (retain, nonatomic) IBOutlet UILabel *lblLv5;



- (IBAction)didClickBtnBack:(id)sender;
- (IBAction)didClickBtnNext:(id)sender;

- (IBAction)didClickLevel1:(id)sender;
- (IBAction)didClickLevel2:(id)sender;
- (IBAction)didClickLevel3:(id)sender;
- (IBAction)didClickLevel4:(id)sender;
- (IBAction)didClickLevel5:(id)sender;
- (IBAction)didClickGestureMiss:(id)sender;

@end

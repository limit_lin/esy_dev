#import "Global.h"
#import "UtilComm.h"
#import "MatchCriteriaStepsViewController.h"
#import "eSyncronyAppDelegate.h"
#import "DSActivityView.h"
#import "mainMenuViewController.h"

@interface MatchCriteriaStepsViewController ()

@end

@implementation MatchCriteriaStepsViewController

int     _criteria_arrSelIndexs[12];

@synthesize parentView;

@synthesize step;
@synthesize year;
//@synthesize popUpView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.screenName = @"MyCriteriaStepsView";
    self.visiblePopTipViews = [NSMutableArray array];

    [scrView addSubview:viewCont];
    
    scrView.contentSize = viewCont.bounds.size;
    
    if(arrStringForQuestion == nil)
    {
        arrStringForQuestion = [[NSMutableArray alloc] init];
        [arrStringForQuestion addObject:NSLocalizedString(@"age",@"MatchCriteriaSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"height",@"MatchCriteriaSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"distance",@"MatchCriteriaSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"religion",@"MatchCriteriaSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"ethnicity",@"MatchCriteriaSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"income",@"MatchCriteriaSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"educational level",@"MatchCriteriaSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"having children",@"MatchCriteriaSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"divorced, widowed or legally separated?",@"MatchCriteriaSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"smokes?",@"MatchCriteriaSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"drinks?",@"MatchCriteriaSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"exercise habits",@"MatchCriteriaSteps")];
    }

    if( arrIndexForView == nil )
    {
        arrIndexForView = [[NSMutableArray alloc] init];
        [arrIndexForView addObject:@"1"];
        [arrIndexForView addObject:@"1"];
        [arrIndexForView addObject:@"1"];
        [arrIndexForView addObject:@"1"];
        [arrIndexForView addObject:@"1"];
        [arrIndexForView addObject:@"1"];
        [arrIndexForView addObject:@"1"];
        [arrIndexForView addObject:@"1"];
        [arrIndexForView addObject:@"2"];
        [arrIndexForView addObject:@"3"];
        [arrIndexForView addObject:@"3"];
        [arrIndexForView addObject:@"1"];
    }
    
    if( arrQuestionTitleViews == nil )
    {
        arrQuestionTitleViews = [[NSMutableArray alloc] init];
        [arrQuestionTitleViews addObject:viewQuestionTitle1];
        [arrQuestionTitleViews addObject:viewQuestionTitle2];
        [arrQuestionTitleViews addObject:viewQuestionTitle3];
    }
    
    if( arrLevelTitleViews == nil )
    {
        arrLevelTitleViews = [[NSMutableArray alloc] init];
        [arrLevelTitleViews addObject:viewLevelTitle1];
        [arrLevelTitleViews addObject:viewLevelTitle2];
        [arrLevelTitleViews addObject:viewLevelTitle3];
    }
    
    if( arrLbViewForQuestion == nil )
    {
        arrLbViewForQuestion = [[NSMutableArray alloc] init];
        [arrLbViewForQuestion addObject:lbQuestionKind1];
        [arrLbViewForQuestion addObject:lbQuestionKind2];
        [arrLbViewForQuestion addObject:lbQuestionKind3];
    }
    
    if( self.step == 1 )
    {
        [btnBack setHidden:YES];
        year = [eSyncronyAppDelegate sharedInstance].years;
        if (year == 0) {
            year = [self p_years];
        }
        
        for( int i = 0; i < 12; i++ )
            _criteria_arrSelIndexs[i] = -1;
    }
    else
        [btnBack setHidden:NO];
    
    [self initWarningMessages];
    [self setContentsToStep];
    
}
-(int)p_years
{
    
    NSDictionary* result = [UtilComm viewAccount];
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Error"];
        [eSyncronyAppDelegate sharedInstance].years = 21;
        [[eSyncronyAppDelegate sharedInstance] saveLoginInfo];
        return 21;
    }
    //        [DSBezelActivityView removeViewAnimated:NO];
    id response = [result objectForKey:@"response"];
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [self dismissViewControllerAnimated:NO completion:nil];
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
        [eSyncronyAppDelegate sharedInstance].years = 21;
        [[eSyncronyAppDelegate sharedInstance] saveLoginInfo];
        return 21;
    }
    
    NSDictionary *ListArr = response;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setLocale:[[NSLocale alloc]initWithLocaleIdentifier:@"en_us"]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    NSDate *dobDate = [[NSDate alloc] init];
    dobDate = [dateFormatter dateFromString:[ListArr objectForKey:@"dob"]];
    [dateFormatter release];
    UIDatePicker *datePiker=[[UIDatePicker alloc]init];
    if (dobDate == nil) {
        [eSyncronyAppDelegate sharedInstance].years = 21;
        [[eSyncronyAppDelegate sharedInstance] saveLoginInfo];
        return 21;
    }else
        datePiker.date = dobDate;
    
    
    NSCalendar          *cal = datePiker.calendar;
    NSDateComponents    *dateComp = [cal components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:datePiker.date];
    NSInteger           year1 = dateComp.year;
    NSInteger           month = dateComp.month;
    
    NSDateComponents*   dateComp2 = [cal components:NSCalendarUnitYear|NSCalendarUnitMonth fromDate:[NSDate date]];
    NSInteger           year2 = dateComp2.year;
    NSInteger           month2 = dateComp2.month;
    
    NSInteger           nYears;
    
    if( month2 >= month )
        nYears = year2 - year1;
    else
        nYears = year2 - year1 - 1;
    
    [eSyncronyAppDelegate sharedInstance].years = (int)nYears;
    [[eSyncronyAppDelegate sharedInstance] saveLoginInfo];
    return (int)nYears;
    
}
- (void)initWarningMessages
{
    arrWarnMessage = [[NSMutableArray alloc] initWithCapacity:0];
//    [arrWarnMessage addObject:NSLocalizedString(@"Restricting your matches' age  will result in less matches for you.",@"MatchCriteriaSteps")];//repair
//    NSLog(@"contryId == %d",[eSyncronyAppDelegate sharedInstance].countryId);//1 is Malaysia
    year = [eSyncronyAppDelegate sharedInstance].years;
//    NSLog(@"year = %d",year);
    
//   append gender judge and age --> male:0 female:1
    if ([eSyncronyAppDelegate sharedInstance].gender == 1) {//female

//        [self  RestrictingMatch:[eSyncronyAppDelegate sharedInstance].gender andYears:year];
        if (year >= 41) {
            
            [arrWarnMessage removeAllObjects];
            
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),(year -10),(year + 25)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),(year -3),(year + 15)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),(year -2),(year + 10)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),(year -20),(year + 5)]];
            
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),(year -15),(year + 2)]];
        }else if (year >= 36 && year <41) {
            
            [arrWarnMessage removeAllObjects];
            
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),(year -10),(year + 25)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),(year -3),(year + 15)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),(year -2),(year + 10)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),21,(year + 5)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),(year -15),(year + 2)]];
        }else if (year >= 31 && year < 36) {
            
            [arrWarnMessage removeAllObjects];
            
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),(year -10),(year + 25)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),(year -3),(year + 15)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),(year -2),(year + 10)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),21,(year + 5)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),21,(year + 2)]];
        }else if (year >= 24 && year <31) {
            [arrWarnMessage removeAllObjects];
            
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),21,(year + 25)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),(year -3),(year + 15)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),(year -2),(year + 10)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),21,(year + 5)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),21,(year + 2)]];
        }else if (year >= 23 && year < 24) {
            [arrWarnMessage removeAllObjects];
            
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),21,(year + 25)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),21,(year + 15)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),(year -2),(year + 10)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),21,(year + 5)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),21,(year + 2)]];
        }else
        {
            [arrWarnMessage removeAllObjects];
            
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),21,(year + 25)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),21,(year + 15)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),21,(year + 10)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),21,(year + 5)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),21,(year + 2)]];
        }
 
    }else//male
    {
        if (year >= 46) {
            [arrWarnMessage removeAllObjects];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),(year -5),(year + 18)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),(year -3),(year + 13)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),(year -25),(year + 10)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),(year -15),(year + 3)]];
            
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),(year -13),(year + 2)]];
        }else if (year >= 36 && year <46) {
            [arrWarnMessage removeAllObjects];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),(year -5),(year + 18)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),(year -3),(year + 13)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),21,(year + 10)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),(year -15),(year + 3)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),(year -13),(year + 2)]];
        }else if (year >= 34 && year < 36) {
           [arrWarnMessage removeAllObjects];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),(year -5),(year + 18)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),(year -3),(year + 13)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),21,(year + 10)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),21,(year + 3)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),(year -13),(year + 2)]];
        }else if (year >= 26 && year <34) {
            [arrWarnMessage removeAllObjects];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),(year -5),(year + 18)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),(year -3),(year + 13)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),21,(year + 10)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),21,(year + 3)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),21,(year + 2)]];
        }else if (year >= 24 && year < 26) {
            [arrWarnMessage removeAllObjects];
            
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),21,(year + 18)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),(year -3),(year + 13)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),21,(year + 10)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),21,(year + 3)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),21,(year + 2)]];
        }else
        {
            [arrWarnMessage removeAllObjects];
            
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),21,(year + 18)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),21,(year + 13)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),21,(year + 10)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),21,(year + 3)]];
            [arrWarnMessage addObject:[NSString stringWithFormat:NSLocalizedString(@"You will get matches between the ages of %d and %d", @"MatchCriteriaSteps"),21,(year + 2)]];
        }
    }
   //height
    [arrWarnMessage addObject:NSLocalizedString(@"This option may result in less matches for you.",@"MatchCriteriaSteps")];
    //distance
    [arrWarnMessage addObject:@""];
    //religion
    [arrWarnMessage addObject:NSLocalizedString(@"You will only have matches with the same religion as you",@"MatchCriteriaSteps")];
    //ethnicity
    [arrWarnMessage addObject:NSLocalizedString(@"You will have matches belonging to your race only",@"MatchCriteriaSteps")];
    //income
    [arrWarnMessage addObject:NSLocalizedString(@"Choosing a more stringent income criteria may result in less matches for you.",@"MatchCriteriaSteps")];
    //educational level
    [arrWarnMessage addObject:NSLocalizedString(@"Restricting your matches' education level will result in less matches for you",@"MatchCriteriaSteps")];
    //having children
    [arrWarnMessage addObject:@""];
//    dating divorced
    [arrWarnMessage addObject:@""];
    //smokes
    [arrWarnMessage addObject:NSLocalizedString(@"We will match you with members who do NOT smoke.",@"MatchCriteriaSteps")];
    //drinks
    [arrWarnMessage addObject:NSLocalizedString(@"We will match you with members who do NOT drink.",@"MatchCriteriaSteps")];
    //exercising habit
    [arrWarnMessage addObject:@""];

}

- (IBAction)didClickBtnBack:(id)sender
{
//    NSLog(@"backstep == %d",step);
    if( self.step == 2 )
    {
//        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//        year = [[defaults objectForKey:@"years"] intValue];
        year = [eSyncronyAppDelegate sharedInstance].years;
        if (year == 0) {
            year = [self p_years];
        }
        
        for( int i = 0; i < 12; i++ )
            _criteria_arrSelIndexs[i] = -1;
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)saveCriteriaProc
{
        //--------------------------------------------------------------//
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    
    [params setObject:[NSNumber numberWithInt:_criteria_arrSelIndexs[0]+1] forKey:@"age"];
    [params setObject:[NSNumber numberWithInt:_criteria_arrSelIndexs[1]+1] forKey:@"height"];
    [params setObject:[NSNumber numberWithInt:_criteria_arrSelIndexs[2]+1] forKey:@"distance"];
    [params setObject:[NSNumber numberWithInt:_criteria_arrSelIndexs[3]+1] forKey:@"religion"];
    [params setObject:[NSNumber numberWithInt:_criteria_arrSelIndexs[4]+1] forKey:@"ethnicity"];
    [params setObject:[NSNumber numberWithInt:_criteria_arrSelIndexs[5]+1] forKey:@"income"];
    [params setObject:[NSNumber numberWithInt:_criteria_arrSelIndexs[6]+1] forKey:@"education"];
    [params setObject:[NSNumber numberWithInt:_criteria_arrSelIndexs[7]+1] forKey:@"children"];
    [params setObject:[NSNumber numberWithInt:_criteria_arrSelIndexs[8]+1] forKey:@"mstatus"];
    [params setObject:[NSNumber numberWithInt:_criteria_arrSelIndexs[9]+1] forKey:@"smoke"];
    [params setObject:[NSNumber numberWithInt:_criteria_arrSelIndexs[10]+1] forKey:@"drink"];
    [params setObject:[NSNumber numberWithInt:_criteria_arrSelIndexs[11]+1] forKey:@"exercise"];

    NSDictionary*    result = [UtilComm saveCriteriaInfo:params];
    [DSBezelActivityView removeViewAnimated:NO];

    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Saving Failure"];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( response == nil )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Saving Failure",)];
        return;
    }
    
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [ErrorProc alertSetMatchingCriteriaError:response];
    }
    else
    {
        [self moveNextWindow];
    }
}

-(void)moveNextWindow
{
   // [self.navigationController popToRootViewControllerAnimated:NO];
    
    [parentView enterDisplayComplete:2];
}

- (void)saveCriteriaAndMoveNext
{
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Saving...",)];
    [self performSelector:@selector(saveCriteriaProc) withObject:nil afterDelay:0.01];
}

- (BOOL)checkValidChoice
{
    int     nSel = _criteria_arrSelIndexs[step-1];
    
    if( nSel >= 0 && nSel < 5 )
        return TRUE;
    
    [ErrorProc alertMessage:NSLocalizedString(@"Please select your choice",) withTitle:NSLocalizedString(@"Field Required",)];

    return FALSE;
}

- (IBAction)didClickBtnNext:(id)sender
{
    if( [self checkValidChoice] == FALSE )
        return;

    if( step == 12 )
    {
        [self saveCriteriaAndMoveNext];
        return;
    }
    
    if( self.nextView == nil )
        self.nextView = [[MatchCriteriaStepsViewController alloc] initWithNibName:@"MatchCriteriaStepsViewController" bundle:nil];
    
    self.nextView.step = self.step+1;
    
    self.nextView.parentView = self.parentView;
    
    [self.navigationController pushViewController:self.nextView animated:YES];
}
//append 0805
-(void)Matchcriteria_actionNext
{
    //append
    if( [self checkValidChoice] == FALSE )
        return;
    
    if( step == 12 )
    {
        [self saveCriteriaAndMoveNext];
        return;
    }
    
    if( self.nextView == nil )
        self.nextView = [[MatchCriteriaStepsViewController alloc] initWithNibName:@"MatchCriteriaStepsViewController" bundle:nil];
    
    self.nextView.step = self.step+1;
    
    self.nextView.parentView = self.parentView;
    
    [self.navigationController pushViewController:self.nextView animated:YES];
}

- (void)selectChosenLevel
{
    int     nSel = _criteria_arrSelIndexs[step-1];

    UIButton*   btnLevels[] = { btnLevel1, btnLevel2, btnLevel3, btnLevel4, btnLevel5 };
    
    for( int i = 0; i < 5; i++ )
        [btnLevels[i] setSelected:NO];

    if( nSel >= 0 && nSel < 5 )
        [btnLevels[nSel] setSelected:YES];
}

- (void)setContentsToStep
{
    //---------- add new views -------------//
    int index = [[arrIndexForView objectAtIndex:step-1] intValue];

    [viewQuestionTitle addSubview:[arrQuestionTitleViews objectAtIndex:index-1]];
    [viewLevelTitle addSubview:[arrLevelTitleViews objectAtIndex:index-1]];

    UILabel* lbQuestion = [arrLbViewForQuestion objectAtIndex:index-1];
    
    UIColor*    color = LINE_TEXT_COLOR;//RGBColor(116, 153, 5);

    NSString    *keyword = [arrStringForQuestion objectAtIndex:step-1];
    NSString    *title = nil;
    
    if( index == 1 )
        title = [NSString stringWithFormat:NSLocalizedString(@"How important is your\nmatch's %@ to you?",@"MatchCriteriaSteps"), keyword];//151102
    else if( index == 2 )
        title = [NSString stringWithFormat:NSLocalizedString(@"How open are you dating\n%@",@"MatchCriteriaSteps"), keyword];
    else
        title = [NSString stringWithFormat:NSLocalizedString(@"Are you open dating\nsomeone who %@",@"MatchCriteriaSteps"), keyword];

    NSMutableAttributedString* string = [[NSMutableAttributedString alloc]initWithString:title];
    NSRange range = [title rangeOfString:keyword];

    [string addAttribute:NSForegroundColorAttributeName value:color range:range];
    [lbQuestion setAttributedText:string];
    
    //--------------------------------------//
    [self selectChosenLevel];
}

- (void)selectLevel:(int)level sender:(id)sender
{
    _criteria_arrSelIndexs[step-1] = level;
    
    UIButton*   btnLevels[] = { btnLevel1, btnLevel2, btnLevel3, btnLevel4, btnLevel5 };
    
    for( int i = 0; i < 5; i++ )
        [btnLevels[i] setSelected:NO];
    
    if( level >= 0 && level < 5 )
        [btnLevels[level] setSelected:YES];
    //这里原本有一个逻辑含有判断根据国家来看弹出框，因为测试中不是malaysia会多一个distance测试　dutyAug 0813_备份中有修改的程序
    switch (level) {
        case 0:
        {
            switch (step) {
                case 1:
                {
                    NSString*   warnMessage = [arrWarnMessage objectAtIndex:0];
                    UIAlertView *alter = [[UIAlertView alloc]initWithTitle:@"eSynchrony" message:warnMessage delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
                    alter.tag = 40001;
                    [alter show];
                    [alter release];
                    
                }
                    break;
                case 10:
                {
                    NSString*   warnMessage = [arrWarnMessage objectAtIndex:step+3];
                    UIAlertView *alter = [[UIAlertView alloc]initWithTitle:@"eSynchrony" message:warnMessage delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
                    alter.tag = 40001;
                    [alter show];
                    [alter release];
                }
                    break;
                case 11:
                {
                    NSString*   warnMessage = [arrWarnMessage objectAtIndex:step+3];
                    UIAlertView *alter = [[UIAlertView alloc]initWithTitle:@"eSynchrony" message:warnMessage delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
                    alter.tag = 40001;
                    [alter show];
                    [alter release];
                }
                    break;
                default:
                    [self Matchcriteria_actionNext];
                    break;
            }
        }
            break;
        case 1:
        {
            switch (step) {
                case 1:
                {
                    NSString*   warnMessage = [arrWarnMessage objectAtIndex:1];
                    UIAlertView *alter = [[UIAlertView alloc]initWithTitle:@"eSynchrony" message:warnMessage delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
                    alter.tag = 40001;
                    [alter show];
                    [alter release];
                    
                }
                    break;
                default:
                    [self Matchcriteria_actionNext];
                    break;
            }
            
        }
            break;
        case 2:
        {
            switch (step) {
                case 1:
                {
                    NSString*   warnMessage = [arrWarnMessage objectAtIndex:2];
                    UIAlertView *alter = [[UIAlertView alloc]initWithTitle:@"eSynchrony" message:warnMessage delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
                    alter.tag = 40001;
                    [alter show];
                    [alter release];
                    
                }
                    break;
                default:
                    [self Matchcriteria_actionNext];
                    break;
            }
        }
            break;
        case 3:{
            
            if (step == 3 || (step >= 8 && step <=12)) {
                [self Matchcriteria_actionNext];
                return;
            }
            
            switch (step) {
                case 1:
                {
                    NSString*   warnMessage = [arrWarnMessage objectAtIndex:3];
                    UIAlertView *alter = [[UIAlertView alloc]initWithTitle:@"eSynchrony" message:warnMessage delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
                    alter.tag = 40001;
                    [alter show];
                    [alter release];
                    
                }
                    break;
                default:
                {
                    NSString*   warnMessage = [arrWarnMessage objectAtIndex:step+3];
                    UIAlertView *alter = [[UIAlertView alloc]initWithTitle:@"eSynchrony" message:warnMessage delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
                    alter.tag = 40001;
                    [alter show];
                    [alter release];
                }
                    break;
            }
        }
            break;
        case 4:{
            
            if (step == 3 || (step >= 8 && step <=12)) {
                [self Matchcriteria_actionNext];
                return;
            }
            
            switch (step) {
                case 1:
                {
                    NSString*   warnMessage = [arrWarnMessage objectAtIndex:4];
                    UIAlertView *alter = [[UIAlertView alloc]initWithTitle:@"eSynchrony" message:warnMessage delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
                    alter.tag = 40001;
                    [alter show];
                    [alter release];
                    
                }
                    break;
                default:
                {
                    NSString*   warnMessage = [arrWarnMessage objectAtIndex:step+3];
                    UIAlertView *alter = [[UIAlertView alloc]initWithTitle:@"eSynchrony" message:warnMessage delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
                    alter.tag = 40001;
                    [alter show];
                    [alter release];
                    
                }
                    break;
            }
        }
            break;
        default:
            [self Matchcriteria_actionNext];
            break;
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == 1) {
        [self Matchcriteria_actionNext];
    }
    else{
        return;
    }
}

- (IBAction)didClickLevel1:(id)sender
{
    [self selectLevel:0 sender:sender];
}

- (IBAction)didClickLevel2:(id)sender
{
    [self selectLevel:1 sender:sender];
}

- (IBAction)didClickLevel3:(id)sender
{
    [self selectLevel:2 sender:sender];
}

- (IBAction)didClickLevel4:(id)sender
{
    [self selectLevel:3 sender:sender];
}

- (IBAction)didClickLevel5:(id)sender
{
    [self selectLevel:4 sender:sender];
}

- (IBAction)didClickGestureMiss:(id)sender {
}

- (void)dealloc
{
    [scrView release];
    [viewCont release];
   
    [viewQuestionTitle release];
    [viewLevelTitle release];
   
    [viewQuestionTitle1 release];
    [viewQuestionTitle2 release];
    [viewQuestionTitle3 release];
   
    [viewLevelTitle1 release];
    [viewLevelTitle2 release];
    [viewLevelTitle3 release];
   
    [btnLevel1 release];
    [btnLevel2 release];
    [btnLevel3 release];
    [btnLevel4 release];
    [btnLevel5 release];
   
    [btnBack release];

    [lbQuestionKind1 release];
    [lbQuestionKind2 release];
    [lbQuestionKind3 release];

    [arrQuestionTitleViews release];
    [arrLevelTitleViews release];

    [arrIndexForView release];
    [arrStringForQuestion release];
    [arrLbViewForQuestion release];
    
    [arrWarnMessage release];
    [self.nextView release];
    [_currentPopTipViewTarget release];
	[_visiblePopTipViews release];
    [_lblLv1 release];
    [_lblLv2 release];
    [_lblLv3 release];
    [_lblLv4 release];
    [_lblLv5 release];
    [super dealloc];
}
@end

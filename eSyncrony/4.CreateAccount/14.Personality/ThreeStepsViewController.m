//
//  ThreeStepsViewController.m
//  eSyncrony
//
//  Created by iosdeveloper on 14-6-4.
//  Copyright (c) 2014年 WonMH. All rights reserved.
//

#import "ThreeStepsViewController.h"
#import "eSyncronyAppDelegate.h"
#import "eSyncronyViewController.h"
@interface ThreeStepsViewController ()

@end

@implementation ThreeStepsViewController
@synthesize slide;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //给图层添加背景图片
//    backgroundView.layer.contents = (id)[UIImage imageNamed:@"bg.png"];
    //将边框设计为圆角
    backgroundView.layer.cornerRadius = 8;
    backgroundView.layer.masksToBounds = YES;
    //为边框添加一个有色边框
    backgroundView.layer.borderWidth = 1;
//    backgroundView.layer.borderColor = [[UIColor colorWithRed:229 green:229 blue:229 alpha:1] CGColor];
    backgroundView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    switch (self.pagenum) {
        case 0:{
            self.currentView = self.viewPage1;
            lblTitleContent.text =NSLocalizedString(@"Our matching system is based on many years of actual matchmaking experiences.",@"PersonalitySteps");
            self.lblTitle.text = NSLocalizedString(@"How do we match?",@"PersonalitySteps");
            break;
        }
            
        case 1:{
            self.currentView = self.viewPage2;
            lblTitleContent.text =NSLocalizedString(@"Once there are mutual matches, we will arrange the face-to-face date!",@"PersonalitySteps");
            self.lblTitle.text = NSLocalizedString(@"How can you go on date?",@"PersonalitySteps");
            break;
        }
            
        default:{
            self.currentView = self.viewPage3;
           lblTitleContent.text =NSLocalizedString(@"Feedback is essential for us to refine and improve on your future proposed matches.",@"PersonalitySteps");
            self.lblTitle.text = NSLocalizedString(@"Feedback time!",@"PersonalitySteps");
            break;
        }
    }
    
    self.currentView.frame = CGRectMake(0, 0, self.currentView.frame.size.width, self.currentView.frame.size.height);
    [self.viewScroll setContentSize:CGSizeMake(self.currentView.frame.size.width, self.currentView.frame.size.height)];
    [self.viewScroll addSubview:self.currentView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_lblTitle release];
    [_viewPage1 release];
    [_viewPage1 release];
    [_viewPage3 release];
    [_viewScroll release];
    [lblTitleContent release];
    [backgroundView release];
    [super dealloc];
}
//-(void)setScrollView{
//    [UIView animateWithDuration:0.4 //速度0.4秒
//                     animations:^{//修改坐标
//                         self.viewScroll.contentOffset = CGPointMake(self.pagenum*320,self.viewScroll.contentOffset.y);
//                         self.viewScroll.contentOffset = CGPointMake(self.pagenum*320,0);
//                         [self.viewScroll setContentSize:CGSizeMake(320*3, self.currentView.frame.size.height)];
//                         
//                     }];
//}
- (IBAction)goNextWindow:(id)sender {
    
    [self.navigationController popToRootViewControllerAnimated:NO];
//    [self.slide.parentView enterIntimacyStepsView];
    [self.slide.parentView enterDisplayComplete:10];

}

- (IBAction)goPage2:(id)sender {
    [self.slide nextViewController];

}

- (IBAction)goPage3:(id)sender {
    [self.slide nextViewController];
}

- (IBAction)clickNext:(id)sender {
    if (self.pagenum<=1) {
        [self.slide nextViewController];
    }else{
        [self.navigationController popToRootViewControllerAnimated:NO];
//        [self.slide.parentView enterIntimacyStepsView];
        [self.slide.parentView enterDisplayComplete:10];
    }
}


@end

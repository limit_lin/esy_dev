
#import "Global.h"
#import "UtilComm.h"
#import "DSActivityView.h"
#import "eSyncronyAppDelegate.h"
#import "PersonalityStepsViewController.h"
#import "eSyncronyViewController.h"
#import "RegisterTableViewCell.h"

#define     MOST_LIKE_VALUE         1
#define     LEAST_LIKE_VALUE        -1

@interface PersonalityStepsViewController ()

@end

@implementation PersonalityStepsViewController

@synthesize parentView;
@synthesize round;
@synthesize step;

int     _personality_pSelectValue[16];

- (void)viewDidLoad
{
    [super viewDidLoad];//151125
    
    self.screenName = @"PersonalityStepsView";
    if( arrQuestionTitle == nil )
    {
        arrQuestionTitle = [[NSMutableArray alloc] initWithCapacity:0];
        [arrQuestionTitle addObject:NSLocalizedString(@"Select ONE from each section that describes you the MOST",@"PersonalitySteps")];
        [arrQuestionTitle addObject:NSLocalizedString(@"Select ONE from each section that describes you the LEAST",@"PersonalitySteps")];
    }
    
    [scrView addSubview:viewCont];
    
    if( self.step == 1 )
    {
        for( int i = 0 ; i < 16; i ++ )
        {
            _personality_pSelectValue[i] = -1;
        }
        
        [btnBack setHidden:YES];
    }

    [self InitVariables];
}

- (void)InitVariables
{
    if( arrNameOfInfoList != nil )
    {
        [arrNameOfInfoList removeAllObjects];
        [arrNameOfInfoList release];
        arrNameOfInfoList = nil;
    }

    arrNameOfInfoList  = [[NSMutableArray alloc] init];
    int tmpcount = 0;

    for( int i = 0 ; i < 16; i ++ )
    {
        if( i % 2 == 0 )
            tmpcount++;

        NSString* strTmp = [NSString stringWithFormat:@"personality_choice_list_q%d", tmpcount + round * 8];
        [arrNameOfInfoList addObject:strTmp];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    viewCont.frame = scrView.bounds;
    scrView.contentSize = viewCont.frame.size;

    [self setContentsToStep];
}

- (IBAction)didClickBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)resizeTableView
{
    if( [arrQuestion count] < 7 )
    {
        [tableView setFrame:CGRectMake(tableView.frame.origin.x, tableView.frame.origin.y, tableView.frame.size.width, 42 * [arrQuestion count])];
    }
    else
        [tableView setFrame:CGRectMake(tableView.frame.origin.x, tableView.frame.origin.y, tableView.frame.size.width, 42 * 7 )];
}

- (void)initTitle
{
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    NSString* title = [arrQuestionTitle objectAtIndex:(step - 1) % 2];
    
    NSMutableAttributedString* string = [[NSMutableAttributedString alloc]initWithString:title];
    
    NSRange range, range1, range2;

    if ([language hasPrefix:@"zh-Hant"]||[language hasPrefix:@"zh-HK"]) {//chinese zh-Hant
        
    range = NSMakeRange(0,16);
    range1 = NSMakeRange(7, 2);
    range2 = NSMakeRange(10, 3);
    }
    else//en
    {
        range = NSMakeRange(0, [title length]);
        range1 = NSMakeRange(7, 3);
        range2 = NSMakeRange(52, [title length]-52);
        
    }

    UIColor*    color1 = TITLE_TEXT_COLLOR;
    UIColor*    color2 = TITLE_REG_COLOR;

    [string addAttribute:NSForegroundColorAttributeName value:color1 range:range];
    [string addAttribute:NSForegroundColorAttributeName value:color2 range:range1];
    [string addAttribute:NSForegroundColorAttributeName value:color2 range:range2];
    //[string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeueLTStd-BdCn" size:14] range:range1];
    //[string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeueLTStd-BdCn" size:14] range:range2];
    
    [txtQuestion setAttributedText:string];
}

- (void)selectChosenItem
{
    if( _personality_pSelectValue[step-1] == -1 )
        return;
    
    NSIndexPath*    indexPath = [NSIndexPath indexPathForItem:_personality_pSelectValue[step-1] inSection:0];

    [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
}

- (void)setContentsToStep
{
    NSString*   strPlist = [arrNameOfInfoList objectAtIndex:step - 1];
    arrQuestion = [[NSMutableArray alloc] initWithArray:[Global loadQuestionArray:strPlist]];

    [tableView reloadData];
    [self resizeTableView];
    [self selectChosenItem];

    [self initTitle];
    if( step == 1 )
        [btnBack setHidden:YES];
}

- (BOOL)checkValidChoice
{

    if( _personality_pSelectValue[step-1] == -1 )
    {
//        [ErrorProc alertMessage:NSLocalizedString(@"Please select your choice",) withTitle:NSLocalizedString(@"Field Required",)];
        return FALSE;
    }
    
    if( (step-1)%2 == 1 && _personality_pSelectValue[step-1] == _personality_pSelectValue[step-2] )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Please select another item",) withTitle:NSLocalizedString(@"Field Required",)];
        return FALSE;
    }

    return TRUE;
}

//=====================================================================================//

- (void)calcTICS
{
    // T, I, S, C
    int     pTblTISC[24][4] = { {2, 1, 0, 3}, {1, 2, 3, 0}, {1, 0, 2, 3}, {3, 2, 1, 0}, // round 1
                                {1, 0, 2, 3}, {0, 2, 1, 3}, {3, 1, 0, 2}, {3, 0, 1, 2}, // round 1
                                {0, 2, 3, 1}, {1, 3, 2, 0}, {2, 1, 0, 3}, {3, 2, 1, 0}, // round 2
                                {0, 2, 1, 3}, {3, 1, 2, 0}, {2, 0, 1, 3}, {0, 2, 3, 1}, // round 2
                                {1, 3, 2, 0}, {0, 1, 3, 2}, {0, 2, 1, 3}, {0, 2, 1, 3}, // round 3
                                {1, 2, 0, 3}, {2, 1, 0, 3}, {0, 1, 2, 3}, {2, 1, 0, 3}, // round 3
    };

    int     i, k;

    nD = nI = nS = nC = 0;
    
    for( i = 0; i < 8; i++ )
    {
        for( k = 0; k < 4; k++ )
            pOrderValue[i][k] = 0;
    }

    for( int i = 0; i < 16; i++ )
    {
        int     index = i/2;
        int     nSel = _personality_pSelectValue[i];
        
        if( i%2 == 0 )  // most like
        {
            pOrderValue[index][nSel] = MOST_LIKE_VALUE;
            
            if( nSel == pTblTISC[index][0] )
                nD++;
            else if( nSel == pTblTISC[index][1] )
                nI++;
            else if( nSel == pTblTISC[index][2] )
                nS++;
            else if( nSel == pTblTISC[index][3] )
                nC++;
        }
        else // least like
        {
            pOrderValue[index][nSel] = LEAST_LIKE_VALUE;
            
            if( k == pTblTISC[index][0] )
                nD--;
            else if( k == pTblTISC[index][1] )
                nI--;
            else if( k == pTblTISC[index][2] )
                nS--;
            else if( k == pTblTISC[index][3] )
                nC--;
        }
    }
}

- (void)savePersonalityProc
{
    
    
    [self calcTICS];
    //--------------------------------------------------------------//
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    
    [params setObject:[NSNumber numberWithInt:round+1] forKey:@"batch_num"];
    
    for( int i = 0; i < 8; i++ )
    {
        NSString*   qValue = [NSString stringWithFormat:@"%d,%d,%d,%d", pOrderValue[i][0], pOrderValue[i][1], pOrderValue[i][2], pOrderValue[i][3]];
        
        NSString*   keyValue = [NSString stringWithFormat:@"qValues%d", i+1];
        
        [params setObject:qValue forKey:keyValue];
    }
    
    [params setObject:[NSNumber numberWithInt:nD] forKey:@"total_D"];
    [params setObject:[NSNumber numberWithInt:nI] forKey:@"total_I"];
    [params setObject:[NSNumber numberWithInt:nS] forKey:@"total_S"];
    [params setObject:[NSNumber numberWithInt:nC] forKey:@"total_C"];
    
    NSDictionary*    result = [UtilComm savePersonality:params];
    [DSBezelActivityView removeViewAnimated:NO];
    
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Saving Failure"];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( response == nil )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Saving Failure",)];
        return;
    }
    if( [response isEqualToString:@"OK"] )
    {
        [self moveNextWindow];
        return;
    }else{
        if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
        {
            if( [response isEqualToString:@"ERROR 99"] )
                [self moveNextWindow];
            else
                [ErrorProc alertSetDISCError:response];
        }
        else
        {
            [self moveNextWindow];
        }
    }
}

-(void)moveNextWindow
{
    
    if( round == 2 )
    {
       // [self.navigationController popToRootViewControllerAnimated:NO];
//        [parentView enterThreeStepsView];//repair 0811　logic tweaks
        [parentView enterIntimacyStepsView];
        return;
    }
    else
    {
        if( self.nextView == nil )
            self.nextView = [[PersonalityStepsViewController alloc] initWithNibName:@"PersonalityStepsViewController" bundle:nil];

        self.nextView.parentView = self.parentView;
        self.nextView.round = self.round+1;
        self.nextView.step = 1;

        [self.navigationController pushViewController:self.nextView animated:YES];
    }
}

- (void)savePersonalityAndMoveNext
{
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Saving...",)];
    [self performSelector:@selector(savePersonalityProc) withObject:nil afterDelay:0.01];
}

- (IBAction)didClickNext:(id)sender
{
    if( [self checkValidChoice] == FALSE )
        return;

    if( step == 16 )
    {
        [self savePersonalityAndMoveNext];
        return;
    }

    if( self.nextView == nil )
        self.nextView = [[PersonalityStepsViewController alloc] initWithNibName:@"PersonalityStepsViewController" bundle:nil];

    self.nextView.parentView = self.parentView;
    self.nextView.round = self.round;
    self.nextView.step = self.step+1;
    
    [self.navigationController pushViewController:self.nextView animated:YES];
}

-(void)personality_actionNext
{
    if( [self checkValidChoice] == FALSE )
        return;
    
    if( step == 16 )
    {
        [self savePersonalityAndMoveNext];
        return;
    }
    
    if( self.nextView == nil )
        self.nextView = [[PersonalityStepsViewController alloc] initWithNibName:@"PersonalityStepsViewController" bundle:nil];
    
    self.nextView.parentView = self.parentView;
    self.nextView.round = self.round;
    self.nextView.step = self.step+1;
    
    [self.navigationController pushViewController:self.nextView animated:YES];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [arrQuestion count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 42;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RegisterTableViewCell *cell;
    
    NSArray     *arr = [[NSBundle mainBundle] loadNibNamed:@"RegisterTableViewCell" owner:nil options:nil];
    cell = [arr objectAtIndex:0];

    NSInteger   row = [indexPath row];
    NSString*   str =  [arrQuestion objectAtIndex:row];

    cell.lbContent.text = str;
    
    if( (step-1)%2 == 1 && _personality_pSelectValue[step-2] == indexPath.row )
    {
        cell.lbContent.enabled = NO;
    }

    return cell;
}

- (void)refreshCellItemsOrderValue
{
    
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tV didSelectRowAtIndexPath:(NSIndexPath *)newIndexPath
{
    _personality_pSelectValue[step-1] = (int)newIndexPath.row;
    
    if( (step-1)%2 == 1 && _personality_pSelectValue[step-1] == _personality_pSelectValue[step-2] )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Please select another item",) withTitle:NSLocalizedString(@"Field Required",)];
        _personality_pSelectValue[step-1] = -1;
        
        [tV deselectRowAtIndexPath:newIndexPath animated:NO];
    }
    
    [self personality_actionNext];
}

- (void)dealloc
{
    [scrView release];
    [viewCont release];

    [tableView release];
    [txtQuestion release];
    [btnBack release];

    [arrQuestionTitle release];
    [arrQuestion release];
    [arrNameOfInfoList release];

    [self.nextView release];

    [super dealloc];
}

@end

//
//  ThreeStepsViewController.h
//  eSyncrony
//
//  Created by iosdeveloper on 14-6-4.
//  Copyright (c) 2014年 WonMH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DVSlideViewController.h"
@interface ThreeStepsViewController : UIViewController
{
    
    IBOutlet UIView *backgroundView;
    IBOutlet UILabel *lblTitleContent;
}
@property (nonatomic, assign) DVSlideViewController* slide;
@property (nonatomic, assign) UIView* currentView;
@property int pagenum;
@property (retain, nonatomic) IBOutlet UILabel *lblTitle;
@property (retain, nonatomic) IBOutlet UIView *viewPage1;
@property (retain, nonatomic) IBOutlet UIView *viewPage2;
@property (retain, nonatomic) IBOutlet UIView *viewPage3;
@property (retain, nonatomic) IBOutlet UIScrollView *viewScroll;
- (IBAction)goNextWindow:(id)sender;
- (IBAction)goPage2:(id)sender;
- (IBAction)goPage3:(id)sender;
- (IBAction)clickNext:(id)sender;

@end

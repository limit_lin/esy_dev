
#import <UIKit/UIKit.h>
@class eSyncronyViewController;

@interface PersonalityStepsViewController : GAITrackedViewController
{
    //mainWnd
    IBOutlet UIScrollView*      scrView;
    IBOutlet UIView*            viewCont;
    
    //subView
    IBOutlet UITableView*       tableView;
    IBOutlet UILabel*           txtQuestion;
    IBOutlet UIButton*          btnBack;

    int     pOrderValue[8][4];
    int     nD, nI, nS, nC;

    //local variables
    NSMutableArray*             arrQuestionTitle;
    NSArray*                    arrQuestion;
    NSMutableArray*             arrNameOfInfoList;
}

@property (nonatomic, assign) int round;
@property (nonatomic, assign) int step;

@property (nonatomic, assign) PersonalityStepsViewController* nextView;
@property (nonatomic, assign) eSyncronyViewController* parentView;

- (IBAction)didClickBack:(id)sender;
- (IBAction)didClickNext:(id)sender;

- (void)setContentsToStep;
- (void)resizeTableView;

@end

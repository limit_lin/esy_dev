

#import "Global.h"
#import "UtilComm.h"
#import "DSActivityView.h"
#import "eSyncronyAppDelegate.h"
#import "MoneyStepsViewController.h"
#import "eSyncronyViewController.h"

@interface MoneyStepsViewController ()

@end

@implementation MoneyStepsViewController

@synthesize parentView;
@synthesize step;

int     _money_arrSelIndexs[15];

- (void)viewDidLoad
{
   [super viewDidLoad];
    
    
    self.screenName = @"MoneyHabitsStepsView";
    [scrView addSubview:subView1];
    
    scrView.contentSize = subView1.bounds.size;
    
    if(arrStringForQuestion == nil)
    {
        arrStringForQuestion = [[NSMutableArray alloc] init];
        [arrStringForQuestion addObject:NSLocalizedString(@"I am very conservative with my investments and need as much details as possible before investing.",@"MoneyHabitsSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"I tend to go with my gut feeling, my investments tend to be on the risky side.",@"MoneyHabitsSteps")];
//        [arrStringForQuestion addObject:NSLocalizedString(@"I tend to always look for the best deal when spending and free is best.",@"MoneyHabitsSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"I tend to look for best deals when spending money.",@"MoneyHabitsSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"The bank's savings rate is good enough for me and keeping cash is the best way to save.",@"MoneyHabitsSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"If I see a golden business opportunity, I should pursue the chance even if the family income suffers.",@"MoneyHabitsSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"The man is supposed to bring home the bacon and look after bulk of the family's finances.",@"MoneyHabitsSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"It's a new world where women should help out as much in the family's finances.",@"MoneyHabitsSteps")];
//        [arrStringForQuestion addObject:NSLocalizedString(@"The control of the family's finances should be based simply on a pro-rated manner on who earns more.",@"MoneyHabitsSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"The amount of control over family finances should be proportionately based on one's salary.", @"MoneyHabitsSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"Whoever is better at managing the finances should take more role in it regardless of gender.",@"MoneyHabitsSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"The husband should earn more or at least equal to the wife for there to be proper respect in the relationship.",@"MoneyHabitsSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"I believe in joining my finances with my lifetime partner as much as possible.",@"MoneyHabitsSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"I will look after my own finances, my lifetime partner looks after his/her.",@"MoneyHabitsSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"I will discuss most financial matters with my lifetime partner as it affects them as well.",@"MoneyHabitsSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"My partner and I will have equal access to the family finances.",@"MoneyHabitsSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"It's my money and I have the right to do what I want with it.",@"MoneyHabitsSteps")];
    }

    if( self.step == 1 )
    {
        [btnBack setHidden:YES];
    
        for( int i = 0; i < 15; i++ )
            _money_arrSelIndexs[i] = -1;
    }
    
    [self setContentsToStep];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    subView1.frame = scrView.bounds;
    scrView.contentSize = subView1.bounds.size;
}

- (IBAction)didClickBtnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)checkValidChoice
{
    int     nSel = _money_arrSelIndexs[step-1];
    
    if( nSel >= 0 && nSel < 5 )
        return TRUE;
    
    [ErrorProc alertMessage:NSLocalizedString(@"Please select your choice",) withTitle:NSLocalizedString(@"Field Required",)];
    
    return FALSE;
}

- (void)saveMoneyStepInfoProc
{
//    NSString*   accNo = [eSyncronyAppDelegate sharedInstance].strAccNo;
//    NSString*   tokenKey = [eSyncronyAppDelegate sharedInstance].strTokenKey;
    
    //--------------------------------------------------------------//
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    
    int     nValues[15];
    int     pTable[2][5] = { {10, 8, 5, 3, 0}, {0, 3, 5, 8, 10} };
    
    for( int i = 0; i < 15; i++ )
    {
        if( i == 0 || i == 6 || i == 8 || i == 11 || i == 14 )
            nValues[i] = pTable[0][_money_arrSelIndexs[i]];
        else
            nValues[i] = pTable[1][_money_arrSelIndexs[i]];
    }
    
    for( int i = 0; i < 15; i++ )
    {
        NSString*   keyValue = [NSString stringWithFormat:@"Q%d", i+1];
        
        [params setObject:[NSNumber numberWithInt:nValues[i]] forKey:keyValue];
    }
    
    NSDictionary*    result = [UtilComm saveMoneyInfo:params];
    [DSBezelActivityView removeViewAnimated:NO];
    
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Saving Failure"];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( response == nil )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Saving Failure",)];
        return;
    }
    
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [ErrorProc alertSetMoneyError:response];
    }
    else
    {
        [self moveNextWindow];
    }
}

-(void)moveNextWindow
{
   // [self.navigationController popToRootViewControllerAnimated:NO];
    [parentView enterDisplayComplete:8];
}

- (void)saveStepInfoAndMoveNext
{
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Saving...",)];
    [self performSelector:@selector(saveMoneyStepInfoProc) withObject:nil afterDelay:0.01];
}

- (IBAction)didClickBtnNext:(id)sender
{
    if( [self checkValidChoice] == FALSE )
        return;
    
    if( step == 15 )
    {
        [self saveStepInfoAndMoveNext];
        return;
    }
    
    if( self.nextView == nil )
        self.nextView = [[MoneyStepsViewController alloc] initWithNibName:@"MoneyStepsViewController" bundle:nil];
    
    self.nextView.parentView = self.parentView;
    self.nextView.step = self.step+1;
    
    [self.navigationController pushViewController:self.nextView animated:YES];
}

-(void)money_actionNext
{
    if( [self checkValidChoice] == FALSE )
        return;
    
    if( step == 15 )
    {
        [self saveStepInfoAndMoveNext];
        return;
    }
    
    if( self.nextView == nil )
        self.nextView = [[MoneyStepsViewController alloc] initWithNibName:@"MoneyStepsViewController" bundle:nil];
    
    self.nextView.parentView = self.parentView;
    self.nextView.step = self.step+1;
    
    [self.navigationController pushViewController:self.nextView animated:YES];
}

- (void)selectChosenLevel
{
    int     nSel = _money_arrSelIndexs[step-1];
    
    UIButton*   btnLevels[] = { btnLevel1, btnLevel2, btnLevel3, btnLevel4, btnLevel5 };
    
    for( int i = 0; i < 5; i++ )
        [btnLevels[i] setSelected:NO];
    
    if( nSel >= 0 && nSel < 5 )
        [btnLevels[4-nSel] setSelected:YES];//nSel-->4-nSel
}

-(void) setContentsToStep
{
    lbQuestion.text = [arrStringForQuestion objectAtIndex:step-1];
    
    [self selectChosenLevel];
}

- (void)selectLevel:(int)level
{
    _money_arrSelIndexs[step-1] = level;
    
    [self selectChosenLevel];
}

- (IBAction)didClickLevel1:(id)sender
{
    [self selectLevel:4];//0-->4
    [self money_actionNext];
}

- (IBAction)didClickLevel2:(id)sender
{
    [self selectLevel:3];//1-->3
    [self money_actionNext];
}

- (IBAction)didClickLevel3:(id)sender
{
    [self selectLevel:2];
    [self money_actionNext];
}

- (IBAction)didClickLevel4:(id)sender
{
    [self selectLevel:1];//3-->1
    [self money_actionNext];
}

- (IBAction)didClickLevel5:(id)sender
{
    [self selectLevel:0];//4-->0
    [self money_actionNext];
}

-(void)dealloc
{
    [scrView release];
    [subView1 release];
    
    [lbQuestion release];
    [btnBack release];
    [arrStringForQuestion release];
    
    [btnLevel1 release];
    [btnLevel2 release];
    [btnLevel3 release];
    [btnLevel4 release];
    [btnLevel5 release];
    
    [self.nextView release];
    
    [super dealloc];
}

@end

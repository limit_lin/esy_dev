
#import <Parse/Parse.h>
//#import <iAd/iAd.h>
#import "Global.h"
#import "UtilComm.h"
#import "DSActivityView.h"
#import "eSyncronyAppDelegate.h"
#import "CreateAccountMainViewController.h"
#import "eSyncronyViewController.h"

#import "SMSVerifyViewController.h"
#import "CoreTelephony/CTCarrier.h"
#import "CoreTelephony/CTTelephonyNetworkInfo.h"
//http://54.169.45.51/api/user/index.php/account/myActivationCode?mobile=63694568
//https://www.esynchrony.com/lucytest.php?mail=ll@sina.com
@implementation CreateAccountMainViewController
{
    
//    IBOutlet UILabel *pEmailLab;
//    IBOutlet UIImageView *pStarImage;
    IBOutlet UILabel *pAgreeLab;
    IBOutlet UILabel *pTermsLab;
    IBOutlet UIButton *pTermsBtn;
    IBOutlet UILabel *pBornLab;
    
}
@synthesize dicRegisterInfo;
@synthesize dicHearusform;

//textField.autocorrectionType = UITextAutocorrectionTypeNo;
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if( textField.tag == 101 )
        [pwTxtFld becomeFirstResponder];
    else if(textField.tag == 102)
        [pwConfirmTxtFld becomeFirstResponder];
    else if(textField.tag == 103)
    {
        [textField resignFirstResponder];
        [self restoreViewPosition];
    }
    else
    {
        [textField resignFirstResponder];
        [self restoreViewPosition];
    }
    
    return YES;
}
//- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
//    
//    CGFloat offset = self.view.frame.size.height - (textField.frame.origin.y + textField.frame.size.height + 216 +50);
//    if (offset <= 0) {
//        [UIView animateWithDuration:0.3 animations:^{
//            CGRect frame = self.view.frame;
//            frame.origin.y  = offset;
//            self.view.frame = frame;
//        }];
//    }
//    return YES;
//}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGRect rect = [textField convertRect:textField.bounds toView:self.view];
    
    float   offset;
    
    offset = self.view.frame.size.height - (rect.size.height + 216 + 50);
    offset = offset - rect.origin.y;
    
    if( offset > 0 )
        return;
    
    rect = self.view.frame;
    rect.origin.y = offset;
    
    self.view.frame = rect;
}


- (NSString*)getDOB
{
    return [Global getYearMonthDateFrom:self.birthDate];
}

-(void)pCreateAccountMainViewControllerLayout
{
    NSRange  colorRange;
    
    if([[eSyncronyAppDelegate sharedInstance].language hasPrefix:@"zh-Hant"]||[[eSyncronyAppDelegate sharedInstance].language hasPrefix:@"zh-HK"]){
        colorRange=[pBornLab.text rangeOfString:@"出生"];
        
    }else if ([[eSyncronyAppDelegate sharedInstance].language hasPrefix:@"id"]){
        colorRange=[pBornLab.text rangeOfString:@"dilahirkan"];
    }else
    {
        colorRange=[pBornLab.text rangeOfString:@"born"];
    }
    NSMutableAttributedString *str = [[[NSMutableAttributedString alloc]initWithString:pBornLab.text]autorelease];
    [str addAttribute:NSForegroundColorAttributeName value:RGBColor(176, 65, 99) range:colorRange];
    pBornLab.attributedText = str;
    
//    CGSize pEmailLabSize = [pEmailLab.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:pEmailLab.font,NSFontAttributeName, nil]];
    CGSize pAgreeLabSize = [pAgreeLab.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:pAgreeLab.font,NSFontAttributeName, nil]];
    CGSize pTermsLabSize = [pTermsLab.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:pTermsLab.font,NSFontAttributeName, nil]];
//    CGSize pTermsBtnSize = [pTermsBtn.titleLabel.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:pTermsBtn.titleLabel.font,NSFontAttributeName, nil]];
    
//    pStarImage.frame = CGRectMake(pEmailLabSize.width + 2, 25, 8, 8);
    pAgreeLab.frame = CGRectMake(46, 290, pAgreeLabSize.width, 44);
    pTermsBtn.frame = CGRectMake(pAgreeLabSize.width+47,290,pTermsLabSize.width,44);
    pTermsLab.frame = CGRectMake(pAgreeLabSize.width+47,290,pTermsLabSize.width, 44);
   
    return;

}

//-(NSDate*)dateFromString:(NSString *)fromString
//{
//    NSDateFormatter *dateFormatter =[[NSDateFormatter alloc]init];
//    [dateFormatter setDateFormat:@"yyyy-mm-dd"];
//    NSDate *destDate = [dateFormatter dateFromString:fromString];
//    [dateFormatter release];
//    return destDate;
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"CreateAccountMainView";
    
    [self pCreateAccountMainViewControllerLayout];//append
    
    _isFaceBookLogin = NO;
    
    nameTxtFld.delegate = self;
    txtSMSCode.delegate = self;
    
    [btnMale setSelected:NO];
    [btnFemale setSelected:NO];

    if( self.regionArray == nil )
    {
        self.regionArray = [NSMutableArray arrayWithCapacity:0];
        [self.regionArray addObject:NSLocalizedString(@"Singapore",@"region array")];
        [self.regionArray addObject:NSLocalizedString(@"Malaysia",@"region array")];
        [self.regionArray addObject:NSLocalizedString(@"Hong Kong",@"region array")];
        [self.regionArray addObject:NSLocalizedString(@"Indonesia",@"region array")];//append Indonesia
        [self.regionArray addObject:NSLocalizedString(@"Thailand",@"region array")];//append Thailand city code = 203
    }
    
//    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnTableView:)];//leaks
    UITapGestureRecognizer *tapRecognizer = [[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnTableView:)]autorelease];
    tapRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapRecognizer];
    
    if( self.nStep == 1 )
    {
        if (self.Reg_CountryId<0||self.Reg_CountryId>4) {//3-->4  多了一个泰国
            self.Reg_CountryId = 0;
        }
        self.selectedIdx = self.Reg_CountryId;
        
//        if (self.Reg_CountryId<0||self.Reg_CountryId>2) {
//            self.Reg_CountryId = 0;
//        }
//        self.selectedIdx = self.Reg_CountryId;
        
        if (self.Reg_facebookId == nil) {
            self.Reg_facebookId = @"";
        }
        
        if (self.Reg_Email == nil) {
            self.Reg_Email = [NSString stringWithFormat:@""];
        }
        
        if (self.Reg_Name==nil) {
            self.Reg_Name = [NSString stringWithFormat:@""];
        }
        
        if (self.Reg_Gender==nil) {
            self.Reg_Gender = [NSString stringWithFormat:@""];
        }
        
        if (self.firstname==nil) {
            self.firstname = [NSString stringWithFormat:@""];
        }
        if (self.lastname==nil) {
            self.lastname = [NSString stringWithFormat:@""];
        }
        
        self.Reg_Password = [NSString stringWithFormat:@""];
        self.Reg_PasswordConfirm = [NSString stringWithFormat:@""];
        
        if (self.Reg_facebookId.length>0) {
            fbBtn.hidden = YES;
            fbLbl.hidden = YES;
            fbLbl2.hidden = YES;
            fbImage1.hidden = YES;
            fbImage2.hidden = YES;
            fbImage3.hidden = YES;
            fbImage4.hidden = YES;
        }
        
        self.isSingleChecked = NO;
        self.isTermChecked = NO;

        liveinLabFld.text = [self.regionArray objectAtIndex:self.selectedIdx];
        nameTxtFld.text = self.Reg_Name;
        
        if ([self.Reg_Gender isEqualToString:@"M"]) {
            [self didClickMaleBtn:nil];
        }else if ([self.Reg_Gender isEqualToString:@"F"]){
            [self didClickFemaleBtn:nil];
        }
        
        lbMySex.text = NSLocalizedString(@"I am a",@"lbMySex");

        [scrlView addSubview:subViewStep1];
        scrlView.contentSize = subViewStep1.frame.size;
        
    }
    else if(self.nStep ==2)
    {
        if (self.Reg_facebookId.length>0) {
//             self.birthDate = [eSyncronyAppDelegate sharedInstance].birthDay;
            
//            self.birthDate = [Global convertDateFromString:self.birthday];
        }
        
//        if ([eSyncronyAppDelegate sharedInstance].strFaceBookID.length>0) {
//            
//            self.birthDate = [eSyncronyAppDelegate sharedInstance].birthDay;
//        }
//        else{//append
//            self.birthDate = nil;
//        }
        [self setDefaultBirthday];
        [self setDateRange];
        
        _prevView = self;
        [scrlView addSubview:subViewStep3];
        scrlView.contentSize = subViewStep3.frame.size;
    }
    else if(self.nStep == 3)
    {
        [scrlView addSubview:subViewStep2];
        scrlView.contentSize = subViewStep2.frame.size;
        
//        //append
//        if (self.Reg_CountryId==7) {
//            self.Reg_CountryId = 3;
//        }
//       // NSLog(@"county id2==%d",self.Reg_CountryId);
        
        NSString* str = [self.regionArray objectAtIndex:self.Reg_CountryId];
        lbDeclare.text = [NSString stringWithFormat:NSLocalizedString(@"I declare I am legally single and am a resident of %@",@"lbDeclare"),str];
        [self refreshContent];
        
    }else if (self.nStep == 4){
        
        [scrlView addSubview:subViewStep4];
        scrlView.contentSize = subViewStep4.frame.size;
        
    }else if (self.nStep == 5){
        
        [scrlView addSubview:subViewStep5];
        scrlView.contentSize = subViewStep5.frame.size;
        
        [self startTime];
    }
    else
    {
    }
}
#if 0
-(void)getAppAdAttribution{
    // Check for iOS 8 attribution implementation
    if ([[ADClient sharedClient ]respondsToSelector:@selector(lookupAdConversionDetails:)]) {
        NSLog(@"iOS 8 call exists");
        
        [[ADClient sharedClient] lookupAdConversionDetails: ^(NSDate *appPurchaseDate, NSDate *iAdImpressionDate) {
            // True if we were installed from an iAd campaign
            BOOL iAdOriginatedInstallation = (iAdImpressionDate != nil);
            NSLog(@"iAdOriginatedInstallation = %d",iAdOriginatedInstallation);
        }];
    }
    // Check for iOS 7.1 implementation
    else if ([[ADClient sharedClient]respondsToSelector:@selector(determineAppInstallationAttributionWithCompletionHandler:)])
    {
        NSLog(@"iOS 7.1 call exists");
        
        [[ADClient sharedClient] determineAppInstallationAttributionWithCompletionHandler:^(BOOL
                                                                                            appInstallationWasAttributedToiAd) {
            // True if we were installed from an iAd campaign
            BOOL iAdOriginatedInstallation = (appInstallationWasAttributedToiAd == YES);
            NSLog(@"iAdOriginatedInstallation = %d",iAdOriginatedInstallation);
        }];
    }
}
#endif  

-(void)requestHearusfrom
{
    NSArray *arr=[[NSArray alloc ]init];
    NSMutableArray *arr1=[[NSMutableArray alloc]init];
    NSDictionary *result=[UtilComm requestHearusfrom];
    
    id response = [result objectForKey:@"response"];

    
//    NSLog(@"%@",response);
    dicHearusform = [NSMutableDictionary dictionary];
    
    if( [response isKindOfClass:[NSDictionary class]] )
    {
        id item=[response objectForKey:@"item"];
        if([item isKindOfClass:[NSArray class]])
        {
            int num=(int)[item count];
            
            for (int i=0;i<num;i++) {
                NSDictionary *dic = [item objectAtIndex:i];
//                NSString *hearusfrom=[dic objectForKey:@"hearusfrom"];//repair 151210
                NSString *hearusfrom= [NSString stringWithFormat:@"%@",NSLocalizedString([dic objectForKey:@"hearusfrom"], @"hearsfrom")];
                
                NSString *value=[dic objectForKey:@"value"];
                
                [dicHearusform setObject:hearusfrom forKey:value];

                NSArray *hear=[NSArray alloc];
//                NSArray *values=[NSArray alloc];
                hear = [hearusfrom componentsSeparatedByString:NSLocalizedString(@"", nil)];//字符串转换成数组
//                values = [value componentsSeparatedByString:NSLocalizedString(@"", nil)];
                
                arr=[NSArray arrayWithArray:hear];
                [arr1 addObjectsFromArray:arr];
                
                //NSLog(@"%@ %@",arr,arr1);
            }
            self.HearArray=[arr1 mutableCopy];
//            NSLog(@"self.HearArray===%@",self.HearArray);
            //            NSLog(@"%@",dics);
        }
    }
    [arr release];
    NSLog(@"dicHearusfrom = %@",dicHearusform);
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)tapOnTableView:(UITapGestureRecognizer *)gesture
{
    [self.view endEditing:YES];
    [self restoreViewPosition];
}

- (IBAction)didClickBackBtn:(id)sender
{
    if (self.nStep != 5){
        for( UIView* v in self.view.subviews )
        {
            if( [v isKindOfClass:[UIWebView class]])
            {
                [v removeFromSuperview];
                return;
            }
        }
        if( self.nStep == 3 )
        {
            self.prevView.Reg_Email = emailTxtFld.text;
            self.prevView.Reg_Password = pwTxtFld.text;
            self.prevView.Reg_PasswordConfirm = pwConfirmTxtFld.text;
            
            lbHearusfrom.text=[self.HearArray objectAtIndex:self.selectedIdx];
            
            self.prevView.isSingleChecked = self.isSingleChecked;
            self.prevView.isTermChecked = self.isTermChecked;
        }
//        if(self.nStep == 2){
//            
//            self.prevView.birthDate = [eSyncronyAppDelegate sharedInstance].birthDay;
////            self.birthDate = [eSyncronyAppDelegate sharedInstance].birthDay;
//        }
            [self.navigationController popViewControllerAnimated:YES];
        
        if ((self.nStep == 1) && (_isFaceBookLogin == YES)) {
            
            _isFaceBookLogin = NO;
            eSyncronyViewController *esynchronyView = [[[eSyncronyViewController alloc] initWithNibName:@"eSyncronyViewController" bundle:nil] autorelease];
            [DSBezelActivityView removeViewAnimated:NO];
            [self.navigationController pushViewController:esynchronyView animated:YES];
        }
            
        
    }else{
        
        [self gotoVerifyPhoneNum];
    }
}

- (IBAction)didClickMaleBtn:(id)sender
{
    [self.view endEditing:YES];

    [btnMale setSelected:YES];
    [btnFemale setSelected:NO];
//  lbMySex.text = @"I am Man";
    self.Reg_Gender = @"M";
}

- (IBAction)didClickFemaleBtn:(id)sender
{
    [self.view endEditing:YES];
    
    [btnMale setSelected:NO];
    [btnFemale setSelected:YES];
//    lbMySex.text = @"I am Woman";
    self.Reg_Gender = @"F";
}

- (IBAction)didClickContinueBtn:(id)sender
{
    self.Reg_Name = nameTxtFld.text;
    self.Reg_CountryId = self.selectedIdx;
//    NSLog(@"county id3==%d\n",self.Reg_CountryId);
//    
//    //self.Reg_CountryId = self.selectedIdx;
//    //append
//    if (self.selectedIdx==3) {
//        self.Reg_CountryId = 7;//7+1=8
//    }
//    else
//    {
//        self.Reg_CountryId = self.selectedIdx;
//    }
//    NSLog(@"county id4==%d\n",self.Reg_CountryId);
//    //append end
    
    //NSLog(@"Reg_name===%@  nameTxtFld.text==%@ ",self.Reg_Name,nameTxtFld);
    
    NSString* tmpStr1 = nil, *tmpStr2 = nil;
    
    int tmpStr3=[self convertToInt:self.Reg_Name];
    
   // NSLog(@"%d",tmpStr3);
    
    BOOL f = NO;

//    if( [self.Reg_Name length] == 0 )
//    {
//        tmpStr1 = NSLocalizedString(@"Field Required",);
//        tmpStr2 = NSLocalizedString(@"Please enter user name.",@"Create Account");
//        f = YES;
//    }
//    
//    else if( [self.Reg_Name length] < 4 )
//    {
//        tmpStr1 = NSLocalizedString(@"Invalid length",);
//        tmpStr2 = NSLocalizedString(@"User name length needs to be at least 4 characters.",@"Create Account");
//        f = YES;
//    }
    if( tmpStr3 == 0 )
    {
        tmpStr1 = NSLocalizedString(@"Field Required",);
        tmpStr2 = NSLocalizedString(@"Please enter user name.",@"Create Account");
        f = YES;
    }
    
    else if( tmpStr3 < 4 )
    {
        tmpStr1 = NSLocalizedString(@"Invalid length",);
        tmpStr2 = NSLocalizedString(@"User name length needs to be at least 4 characters.",@"Create Account");
        f = YES;
    }
    else if( [self.Reg_Gender isEqualToString:@""] )
    {
        tmpStr1 = NSLocalizedString(@"Field Required",);
        tmpStr2 = NSLocalizedString(@"Please select your gender.",@"Create Account");
        f = YES;
    }
    else if( self.Reg_CountryId == -1 )
    {
        tmpStr1 = NSLocalizedString(@"Field Required",);
        tmpStr2 = NSLocalizedString(@"Please enter where you live.",@"Create Account");
        f = YES;
    }
    
    if( f == YES )
    {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:tmpStr1 message:tmpStr2 delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",) otherButtonTitles:nil];
        [av show];
        [av release];
        return;
    }

//    [subViewStep1 removeFromSuperview];
//    [scrlView addSubview:subViewStep3];
//    scrlView.contentSize = subViewStep3.frame.size;
    CreateAccountMainViewController *createAccountView = [[[CreateAccountMainViewController alloc] initWithNibName:@"CreateAccountMainViewController" bundle:nil] autorelease];

    createAccountView.nStep = 2;
    createAccountView.parentView = self.parentView;
    createAccountView.prevView = self;
    
    createAccountView.Reg_facebookId = self.Reg_facebookId;
    
    createAccountView.Reg_Email = self.Reg_Email;
    createAccountView.Reg_Password = self.Reg_Password;
    createAccountView.Reg_PasswordConfirm = self.Reg_PasswordConfirm;
    
    createAccountView.isSingleChecked = self.isSingleChecked;
    createAccountView.isTermChecked = self.isTermChecked;
    
    createAccountView.Reg_Name = self.Reg_Name;
    createAccountView.firstname = self.firstname;
    createAccountView.lastname = self.lastname;
    createAccountView.Reg_Gender = self.Reg_Gender;
    createAccountView.Reg_CountryId = self.Reg_CountryId;

    createAccountView.birthday = self.birthday;
    
    [self.navigationController pushViewController:createAccountView animated:YES];
}

-(int)convertToInt:(NSString*)strtemp {//判断注册时候的字节数

    int strlength = 0;
    char* p = (char*)[strtemp cStringUsingEncoding:NSUnicodeStringEncoding];
    for (int i=0; i<[strtemp lengthOfBytesUsingEncoding:NSUnicodeStringEncoding];i++) {
        if (*p) {
            p++;
            strlength++;
        }
        else {
            p++;
        }
    }
    return strlength;
    
}
/*
- (NSMutableArray*)validateEmailWithString:(NSString*)emails
{
    NSMutableArray *validEmails = [[NSMutableArray alloc] init];
    NSArray *emailArray = [emails componentsSeparatedByString:@","];
    for (NSString *email in emailArray)
    {
        NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
        if ([emailTest evaluateWithObject:email])
            [validEmails addObject:email];
    }
    return [validEmails autorelease];
}
*/
- (BOOL)isValidEmailAddress:(NSString *)checkString
{
    BOOL stricterFilter = YES;
    //NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-].+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *stricterFilterString = @"[\\._%+-=*/?,;:'{}].+@([_%+-=*/?,;:']+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    NSRange rng1 = [checkString rangeOfString:@"@"];
    NSRange rng2 = [checkString rangeOfString:@".com"];
    BOOL bFlag1 = [emailTest evaluateWithObject:checkString];
    BOOL bFlag2 = rng1.location == NSNotFound;
    BOOL bFlag3 = rng2.location == checkString.length-4;

    return bFlag1 || bFlag2 || !bFlag3;
}

- (IBAction)didClickCreateAccount:(id)sender
{
    NSString*   strMsgLeft = nil, *strMsgRight = nil;
    NSString*   strTmp1 = nil, *strTmp2 = nil , *strTmp3 = nil,*strTmp4=nil;
    BOOL        f = NO;

    strTmp1 = emailTxtFld.text;
    strTmp2 = pwTxtFld.text;
    strTmp3 = pwConfirmTxtFld.text;
    strTmp4 = lbHearusfrom.text;
    
    if ([strTmp4 length] == 0) {//append
        
        strMsgLeft = NSLocalizedString(@"Field Required",);
        strMsgRight = NSLocalizedString(@"Please click 'Hear us from' to choose",@"Create Account");
        [lbHearusfrom becomeFirstResponder];
        f = YES;
    }
    else if( [strTmp1 length] == 0 )
    {
        strMsgLeft = NSLocalizedString(@"Field Required",);
        strMsgRight = NSLocalizedString(@"Please enter your email address",@"Create Account");
        [emailTxtFld becomeFirstResponder];
        f = YES;
    }else if( [self isValidEmailAddress:strTmp1] )
    {
        strMsgLeft = NSLocalizedString(@"Invalid Format",);
        strMsgRight = NSLocalizedString(@"Please enter a valid email address.",@"Create Account");
        [emailTxtFld becomeFirstResponder];
        f = YES;
    }else if( [strTmp2 length] == 0)
    {
        strMsgLeft = NSLocalizedString(@"Field Required",);
        strMsgRight = NSLocalizedString(@"Please enter password.",@"Create Account");
        [pwTxtFld becomeFirstResponder];
        f = YES;
    }else if ( [ strTmp2 length ] < 6 )
    {
        strMsgLeft = NSLocalizedString(@"Invalid length",);
        strMsgRight = NSLocalizedString(@"Password length needs to be at least 6 characters.",@"Create Account");
        [pwTxtFld becomeFirstResponder];
        f = YES;
    }else if( [strTmp3 length] == 0)
    {
        strMsgLeft = NSLocalizedString(@"Field Required",);
        strMsgRight = NSLocalizedString(@"Please enter your password again",@"Create Account");
        [pwConfirmTxtFld becomeFirstResponder];
        f = YES;
    }
    else if ( [strTmp2 isEqualToString:strTmp3] == NO)
    {
        strMsgLeft = NSLocalizedString(@"Password mismatch",@"Create Account");
        strMsgRight = NSLocalizedString(@"Password and confirm password do not match.",@"Create Account");
        [pwConfirmTxtFld becomeFirstResponder];
        f = YES;
    }
    else if( self.isTermChecked == NO )
    {
        strMsgLeft = NSLocalizedString(@"Terms and Conditions",@"Create Account");
        strMsgRight = NSLocalizedString(@"You must agree with terms and conditions.",@"Create Account");
        f = YES;
    }
    else if( self.isSingleChecked == NO )
    {
        strMsgLeft = @"eSynchrony";
        
        NSString* str = [self.regionArray objectAtIndex:self.Reg_CountryId];
        strMsgRight = [NSString stringWithFormat:NSLocalizedString(@"You must declare you are legally single and a resident of %@",@"Create Account"),str];
        str = nil;
        f = YES;
    }

    if( f == YES )
    {
        [ErrorProc alertMessage:strMsgRight withTitle:strMsgLeft];
        return;
    }

   //register.....
   //[DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Registering...",@"Create Account")];
    [self performSelector:@selector(registerProcess) withObject:nil afterDelay:0.01];
    
}

- (IBAction)didClickContinueNext:(id)sender {

    [subViewStep3 removeFromSuperview];
    
    if (self.HearArray == nil) {
        
        [self requestHearusfrom];
        
    }
    
    [scrlView addSubview:subViewStep2];
    scrlView.contentSize = subViewStep2.frame.size;
    
    //append
    if (self.Reg_CountryId==7) {
        self.Reg_CountryId = 3;
    }
    if (self.Reg_CountryId==202) {//append
        self.Reg_CountryId = 4;
    }
    
    
    //NSLog(@"county id8==%d\n",self.Reg_CountryId);
    
    NSString* str = [self.regionArray objectAtIndex:self.Reg_CountryId];
    lbDeclare.text = [NSString stringWithFormat:NSLocalizedString(@"I declare I am legally single and am a resident of %@",@"lbDeclare"),str];
    [self refreshContent];
}

- (void)setDateRange
{
    NSCalendar          *cal = pickerDate.calendar;
//    NSDateComponents    *dateComp= [cal components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:[NSDate date]];
    NSDateComponents *dateComp = [cal components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:[NSDate date]];
    
    dateComp.year -= 21;
    pickerDate.maximumDate = [cal dateFromComponents:dateComp];
    
    dateComp.year = 1940;
    dateComp.month = 1;
    dateComp.day = 1;
    
    pickerDate.minimumDate = [cal dateFromComponents:dateComp];
}

- (IBAction)changeDate:(UIDatePicker *)sender {
    
    [self birthday:sender];
    
}
- (void)setDefaultBirthday
{
    if(self.birthDate != nil )
    {
        pickerDate.date = self.birthDate;
        [self birthday:pickerDate];
        return;
    }
    
    NSCalendar          *cal = pickerDate.calendar;
//    NSDateComponents    *dateComp= [cal components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:[NSDate date]];
    NSDateComponents *dateComp = [cal components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:[NSDate date]];
    
    dateComp.year -= 21;
    self.birthDate = [cal dateFromComponents:dateComp];
    pickerDate.date = self.birthDate;
    [self birthday:pickerDate];
}

- (void)birthday:(UIDatePicker*)picker
{
    NSCalendar          *cal = picker.calendar;
    NSDateComponents    *dateComp = [cal components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:picker.date];
    NSInteger           year = dateComp.year;
    NSInteger           month = dateComp.month;
    NSInteger           day = dateComp.day;
    
   // NSArray *arr = [Global loadQuestionArray:@"month_list"];
    
    //lbBirthday.text = [NSString stringWithFormat:NSLocalizedString(@"I was born on %@ %d, %d",@"lbBirthday"), [arr objectAtIndex:month-1], (int)day, (int)year];
    lbBirthday.text = [NSString stringWithFormat:NSLocalizedString(@"I was born on %d %d, %d",@"lbBirthday"), (int)month, (int)day, (int)year];
    
    NSDateComponents*   dateComp2 = [cal components:NSCalendarUnitYear|NSCalendarUnitMonth fromDate:[NSDate date]];
    NSInteger           year2 = dateComp2.year;
    NSInteger           month2 = dateComp2.month;
    
    NSInteger           nYears;
    
    if( month2 >= month )
        nYears = year2 - year;
    else
        nYears = year2 - year - 1;
    
    lbYearsOld.text = [NSString stringWithFormat:NSLocalizedString(@"%d years old",@"lbYearsOld"), (int)nYears];
   //append years for criteria popup window
    [eSyncronyAppDelegate sharedInstance].years =(int)nYears;
//    NSLog(@"creat an account nyears = %d",[eSyncronyAppDelegate sharedInstance].years);
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setObject:[NSNumber numberWithInt:(int)nYears] forKey:@"years"];
    [[eSyncronyAppDelegate sharedInstance] saveLoginInfo];
    
    self.birthDate = [cal dateFromComponents:dateComp];
    
    mDOB_year   = (int)year;
    mDOB_month  = (int)month;
    mDOB_day    = (int)day;
}

- (void)registerProcess
{
    
    int country;
    self.Reg_Email = emailTxtFld.text;
    self.Reg_Password = pwTxtFld.text;

    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];

    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    NSLog(@"county id9==%@\n",[NSNumber numberWithInt:self.Reg_CountryId+1]);
    if (self.Reg_CountryId+1 ==4) {
       country = 8;
    }else if (self.Reg_CountryId+1 == 5)
    {
        country = 203;
    }else
        country = self.Reg_CountryId+1;
    
    [params setObject:self.Reg_Name forKey:@"nname"];
    [params setObject:self.firstname forKey:@"fname"];
    [params setObject:self.lastname forKey:@"lname"];
    [params setObject:self.Reg_Email forKey:@"email"];
    [params setObject:self.Reg_Password forKey:@"password"];
    //[params setObject:[NSNumber numberWithInt:self.Reg_CountryId+1] forKey:@"cty_id"];
    [params setObject:[NSNumber numberWithInt:country] forKey:@"cty_id"];
    
    [params setObject:self.Reg_Gender forKey:@"gender"];
    [params setObject:self.Reg_facebookId forKey:@"facebook_id"];
    
//    [params setObject:[NSNumber numberWithInt:[eSyncronyAppDelegate sharedInstance].years] forKey:@"years"];
     if (self.hear == nil) {
        self.hear = @"18";
    }else
        [params setObject:self.hear forKey:@"hearusfrom"];
    
    if (self.birthDate != nil) {
        NSCalendar          *cal = pickerDate.calendar;
        NSDateComponents    *dateComp = [cal components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:self.birthDate];
        NSInteger           year = dateComp.year;
        NSInteger           month = dateComp.month;
        NSInteger           day = dateComp.day;
        
        [params setObject:[NSString stringWithFormat:@"%ld-%ld-%ld",(long)year,(long)month,(long)day] forKey:@"birthday"];
//        [params setObject:[NSString stringWithFormat:@"%d-%d-%d",year,month,day] forKey:@"birthday"];
        [params setObject:self.birthDate forKey:@"birthdate"];
    }

    [self performSelector:@selector(didLogin:) withObject:params afterDelay:0.1];
    
    return;
}


- (void)didLogin:(NSMutableDictionary *)value
{
//    [self.navigationController popViewControllerAnimated:NO];
//    [self.navigationController popToRootViewControllerAnimated:NO];

//    [[eSyncronyAppDelegate sharedInstance].viewController procCheckUserStep];
    
//    SMSVerifyViewController *smsVerifyStepView = [[[SMSVerifyViewController alloc] initWithNibName:@"SMSVerifyViewController" bundle:nil] autorelease];
//    smsVerifyStepView.dicRegisterInfo = value;
//    smsVerifyStepView.parentView = self.parentView;
//    smsVerifyStepView.step = 1;
//    
//    [self.navigationController pushViewController:smsVerifyStepView animated:YES];
    
    
    CreateAccountMainViewController *createAccountView = [[[CreateAccountMainViewController alloc] initWithNibName:@"CreateAccountMainViewController" bundle:nil] autorelease];
    
    createAccountView.nStep = 4;
    createAccountView.parentView = self.parentView;
    createAccountView.prevView = self;
    createAccountView.dicRegisterInfo = value;
    createAccountView.Reg_phoneNumber = self.Reg_phoneNumber;
    
    [DSBezelActivityView removeViewAnimated:NO];
    [self.navigationController pushViewController:createAccountView animated:YES];
}

- (void)DataSelectView:(DataSelectViewController *)dataSelectView didSelectItem:(int)itemIndex withTag:(int)tag
{
    self.selectedIdx = itemIndex;
    [self initSetting];

    [dataSelectView release];
}


- (void)HearSelectView:(HearSelectViewController*)dataSelectView didSelectItem:(int)itemIndex withTag:(int)tag
{
    self.selectedIdx = itemIndex;
    [self initSetting1];
    
    [dataSelectView release];
}

- (IBAction)didClickSelectRegionBox:(id)sender
{
    [self.view endEditing:YES];
    
    DataSelectViewController*   selectRegionView = [DataSelectViewController createWithDataList:self.regionArray selectIndex:self.selectedIdx withTag:0];

    selectRegionView.delegate = self;
    [self.view addSubview:selectRegionView.view];
    selectRegionView.view.frame = self.view.bounds;
}
- (IBAction)didClickHearusfrom:(id)sender {
    
    [self.view endEditing:YES];
    
    HearSelectViewController*   selecthearView = [HearSelectViewController createWithHearList:self.HearArray selectIndex:self.selectedIdx withTag:0];
    
    selecthearView.delegate = self;
    [self.view addSubview:selecthearView.view];
    selecthearView.view.frame = self.view.bounds;
    
}
- (IBAction)didClickFbLogin:(id)sender
{
    _isFaceBookLogin = YES;
    
    [[eSyncronyAppDelegate sharedInstance]logoutFB];
    NSArray *permissionsArray = @[@"email",@"user_birthday", @"user_location",@"user_education_history"];

    [_activityIndicator startAnimating]; // Show loading indicator until login is finished
    
    _isFaceBookLogin = YES;
    /*
    // Login PFUser using facebook
    [PFFacebookUtils logInWithPermissions:permissionsArray block:^(PFUser *user, NSError *error) {
        [_activityIndicator stopAnimating]; // Hide loading indicator
        if( user ) {
            [self registerWithFaceBook];
        }
    }];*/

    [FBSession openActiveSessionWithReadPermissions:permissionsArray
                                       allowLoginUI:YES
                                  completionHandler:
     ^(FBSession *session,
       FBSessionState state, NSError *error) {
         if( state == FBSessionStateOpen ){
             [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Registering...",@"Create Account")];
             [self registerWithFaceBook];
         }else{
             NSLog(@"RegisterWithFaceBook Something went wrong");
             NSString *alertText = nil;
             NSString *alertTitle = nil;
             // If the error requires people using an app to make an action outside of the app in order to recover
             
             NSDictionary *errorInformation = [[[error.userInfo objectForKey:@"com.facebook.sdk:ParsedJSONResponseKey"] objectForKey:@"body"] objectForKey:@"error"];
             
             if ([FBErrorUtility shouldNotifyUserForError:error] == YES){
                 alertTitle = NSLocalizedString(@"Something went wrong",@"FbLogin");
                 alertText = [FBErrorUtility userMessageForError:error];
                 
             } else {
                 
                 // If the user cancelled login, do nothing
                 if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
                     alertTitle = NSLocalizedString(@"Info",);
                     alertText = NSLocalizedString(@"User cancelled login.",@"FbLogin");
                     
                     // Handle session closures that happen outside of the app
                 } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession){
                     alertTitle = NSLocalizedString(@"Session Error",);
                     alertText = NSLocalizedString(@"Your current session is no longer valid. Please log in again.",@"FbLogin");
                     
                     
                     // Here we will handle all other errors with a generic error message.
                     // We recommend you check our Handling Errors guide for more information
                     // https://developers.facebook.com/docs/ios/errors/
                 } else {
                     //Get more error information from the error
                     
                     // Show the user an error message
                     alertTitle = NSLocalizedString(@"Please retry.",@"FbLogin");
                     alertText = [NSString stringWithFormat:NSLocalizedString(@" If the problem persists contact us and mention this error code: %@",@"FbLogin"), [errorInformation objectForKey:@"message"]];
                     
                 }
                 
             }
             if ([errorInformation objectForKey:@"message"]) {
                 UIAlertView *av = [[UIAlertView alloc] initWithTitle:alertTitle
                                                              message:alertText
                                                             delegate:nil
                                                    cancelButtonTitle:NSLocalizedString(@"OK",)
                                                    otherButtonTitles:nil, nil];
                 [av show];
                 [av release];
                 
             }
             [[eSyncronyAppDelegate sharedInstance]logoutFB];
         }
     }];
}

- (void)registerWithFaceBook
{
    // Send request to Facebook
    FBRequest *request = [FBRequest requestForMe];
    [request startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        // handle response
        if( !error ) {
            // Parse the data received
            NSDictionary *userData = (NSDictionary *)result;
            NSLog(@"Facebook userData:%@",userData);
            self.Reg_facebookId = userData[@"id"];
            
            self.Reg_Email = userData[@"email"];
            self.Reg_Email = [self.Reg_Email stringByReplacingOccurrencesOfString:@"%%40" withString:@"@"];
            
            self.birthday =userData[@"birthday"];
            
            self.Reg_Name = userData[@"first_name"];
            self.firstname = userData[@"first_name"];
            self.lastname = userData[@"last_name"];
            
            self.Reg_Gender = userData[@"gender"];
            if( [self.Reg_Gender isEqualToString:@"male"] )
                self.Reg_Gender = @"M";
            else
                self.Reg_Gender = @"F";

            self.location = [[userData objectForKey:@"location"]objectForKey:@"name"];
            if ([self.location rangeOfString:@"Málai"].location != NSNotFound){
                self.Reg_CountryId = 1;
            }else if([self.location rangeOfString:@"Hong Kong"].location != NSNotFound){
                self.Reg_CountryId = 2;
            }else if([self.location rangeOfString:@"Indonesia"].location != NSNotFound){//append Indonesia
                
                self.Reg_CountryId = 7;//8-1
            }else if([self.location rangeOfString:@"Thailand"].location != NSNotFound){//append Thailand
                
                self.Reg_CountryId = 202;//203-1
            }else{
                self.Reg_CountryId = 0;
            }
            NSLog(@"county id10==%d",self.Reg_CountryId);
            
            if (userData[@"email"]==nil) {
                [DSBezelActivityView removeViewAnimated:NO];
                UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Authentication Failure",)
                                                             message:NSLocalizedString(@"Your Email has not been verified by Facebook.",@"FbLogin")
                                                            delegate:nil
                                                   cancelButtonTitle:NSLocalizedString(@"OK",)
                                                   otherButtonTitles:nil, nil];
                [av show];
                [av release];
            }else{
                [self performSelector:@selector(procLoginWithFB) withObject:nil afterDelay:0.01];
            }
            
        }else{
            [DSBezelActivityView removeViewAnimated:NO];
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Sorry",)
                                                         message:NSLocalizedString(@"Facebook login Failed,please try again later.",@"FbLogin")
                                                        delegate:nil
                                               cancelButtonTitle:NSLocalizedString(@"OK",)
                                               otherButtonTitles:nil, nil];
            [av show];
            [av release];
            NSLog(@"error:%@",error);
            
        }
    }];
}

- (void)procLoginWithFB
{
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    [params setObject:self.Reg_facebookId forKey:@"facebook_id"];
    [params setObject:self.Reg_Email forKey:@"email"];
    
    NSDictionary*    result = [UtilComm loginWithFaceBook:params];
    [DSBezelActivityView removeViewAnimated:NO];
    
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Authentication Failure"];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( response == nil )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Saving Failure",)];
        return;
    }
    
    if( [response isKindOfClass:[NSString class]]  )
    {
        if ([response isEqualToString:@"Not Registered"]) {

            [self viewDidLoad];
        }
    }
    else
    {
        [eSyncronyAppDelegate sharedInstance].strFaceBookID     = self.Reg_facebookId;
        [eSyncronyAppDelegate sharedInstance].strLoginEmail     = self.Reg_Email;
        [eSyncronyAppDelegate sharedInstance].strLoginPassword  = @"";
//        [eSyncronyAppDelegate sharedInstance].strName = [response objectForKey:@"nname"];
        [eSyncronyAppDelegate sharedInstance].strName  = self.Reg_Name;
        [eSyncronyAppDelegate sharedInstance].membership = [response objectForKey:@"membership"];
        [eSyncronyAppDelegate sharedInstance].strAccNo = [response objectForKey:@"acc_no"];
        [eSyncronyAppDelegate sharedInstance].strTokenKey = [response objectForKey:@"token_key"];
        [eSyncronyAppDelegate sharedInstance].countryId = [[response objectForKey:@"cty_id"]intValue]-1;
//        [eSyncronyAppDelegate sharedInstance].countryId = self.Reg_CountryId;
    
        [eSyncronyAppDelegate sharedInstance].birthDay=[response objectForKey:@"birthDay"];
        
        [eSyncronyAppDelegate sharedInstance].bIsLoggedIn = YES;

        self.Reg_Gender = [response objectForKey:@"gender"];
        if( [self.Reg_Gender isEqualToString:@"M"] ){
            [eSyncronyAppDelegate sharedInstance].gender = 0;
        }
        else{
            [eSyncronyAppDelegate sharedInstance].gender = 1;
        }
        
        [[eSyncronyAppDelegate sharedInstance] saveLoginInfo];
        
        //append
        [params setObject:self.Reg_Name forKey:@"nname"];
        [params setObject:@"" forKey:@"password"];
        [params setObject:[NSNumber numberWithInt:self.Reg_CountryId+1] forKey:@"cty_id"];
        [params setObject:self.Reg_Gender forKey:@"gender"];
        [params setObject:@"49" forKey:@"hearusfrom"];
        [params setObject:self.firstname forKey:@"fname"];
        [params setObject:self.lastname forKey:@"lname"];
        [params setObject:self.birthday forKey:@"birthday"];
    
        [[eSyncronyAppDelegate sharedInstance].viewController showBusyDialogWithTitle:NSLocalizedString(@"Loading...",)];
        [self performSelector:@selector(didLogin:) withObject:params afterDelay:0.1];
        
//        [self didLogin:nil];
    }
}


- (IBAction)didClickTermsAndCondition:(id)sender
{
    //CGSize size = scrlView.contentSize;
    //UIWebView *web = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    UIWebView *web = [[UIWebView alloc] initWithFrame:scrlView.frame];
    web.delegate = self;
    web.scalesPageToFit = YES;
    //[scrlView addSubview:web];
    [self.view addSubview:web];

    NSURL       *_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",WEBSERVICE_TERMS]];

    NSURLRequest* _urlRequest = [NSURLRequest requestWithURL:_url];
    [web loadRequest:_urlRequest];

    [DSBezelActivityView newActivityViewForView:web withLabel:NSLocalizedString(@"Loading...",)];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [DSBezelActivityView removeViewAnimated:NO];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [DSBezelActivityView removeViewAnimated:NO];
    UIAlertView* av = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"Loading failed!",) delegate:nil cancelButtonTitle:NSLocalizedString(@"Close",) otherButtonTitles:nil, nil];
    [av show];
    [av release];
}

- (IBAction)didClickAgreetoAll:(id)sender
{
    [chkbtnAgreetoAll setSelected:!chkbtnAgreetoAll.selected ];
    self.isTermChecked = !self.isTermChecked ;
}

- (IBAction)didClickDeclare:(id)sender
{
    [chkbtnDeclare setSelected:!chkbtnDeclare.selected];
    self.isSingleChecked = !self.isSingleChecked;
}

- (IBAction)didClickPhoneNumber:(id)sender {
    
   // [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Registering...",@"Create Account")];
    self.Reg_phoneNumber = txtPhoneNumber.text;
    int countryId = [[dicRegisterInfo objectForKey:@"cty_id"] intValue] - 1;
    
    NSString *countryname;
    NSString *MOBILE;
    //0: Singapore, 1: Malaysia, 2: HONG KONG
    
//    NSLog(@"didClickPhoneNumber countryId====%d",countryId);

    switch (countryId) {
        case 0:{
            MOBILE = @"^([89])\\d{7}$";
            countryname = @"Singapore";
            break;
        }
        case 1:{
            MOBILE = @"^(01)\\d{8,9}$";
            countryname = @"Malaysia";
            break;
        }
        case 2:{
            MOBILE = @"^([5689])\\d{7}$";
            countryname = @"HONG KONG";
            break;
        }
        case 7:{
            MOBILE = @"^(08)\\d{7,10}$";//11-->9~12
            countryname = @"Indonesia";
            break;
        }
        case 202:{
            MOBILE = @"^([0])\\d{9}$";
            countryname = @"Thailand";
            break;
        }
        default:{
            MOBILE = @"^([5689])\\d{7}$";
            countryname = @"HONG KONG";
            break;
        }
    }
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    if (![regextestmobile evaluateWithObject:self.Reg_phoneNumber]) {
        [DSBezelActivityView removeViewAnimated:NO];
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Info",) message:[NSString stringWithFormat:NSLocalizedString(@"Please fill in the phone number with the correct format for %@ !",@"SMSVerify"),[NSString stringWithFormat:@"%@",NSLocalizedString(countryname, @"region array")]] delegate:self cancelButtonTitle:NSLocalizedString(@"OK",) otherButtonTitles:nil, nil];
        
        [alert show];
        [alert release];
        return;
    }
    
    [self performSelector:@selector(smsCreatAccountReg) withObject:nil afterDelay:0.01];
    
}

-(void)smsCreatAccountReg
{
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    [params setObject:self.Reg_phoneNumber forKey:@"handphone"];
    int country=[[dicRegisterInfo objectForKey:@"cty_id"] intValue];
    
    if (country == 4) {
        country = 8;
    }
    if (country == 5) {
        country = 203;
    }
    NSLog(@"smsCreatAccountReg countryId = %d",country);

    if (dicRegisterInfo != nil) {
        [params setObject:[dicRegisterInfo objectForKey:@"nname"] forKey:@"nname"];
        [params setObject:[dicRegisterInfo objectForKey:@"email"] forKey:@"email"];
        [params setObject:[dicRegisterInfo objectForKey:@"password"] forKey:@"password"];
        
        //[params setObject:[dicRegisterInfo objectForKey:@"cty_id"] forKey:@"cty_id"];
        [params setObject:[NSNumber numberWithInt:country] forKey:@"cty_id"];
        
        [params setObject:[dicRegisterInfo objectForKey:@"gender"] forKey:@"gender"];
        
        [params setObject:[dicRegisterInfo objectForKey:@"fname"] forKey:@"fname"];
        [params setObject:[dicRegisterInfo objectForKey:@"lname"] forKey:@"lname"];
        [params setObject:[dicRegisterInfo objectForKey:@"facebook_id"] forKey:@"facebook_id"];
        
        [params setObject:[dicRegisterInfo objectForKey:@"hearusfrom"] forKey:@"hearusfrom"];
        
        [params setObject:[dicRegisterInfo objectForKey:@"birthday"] forKey:@"dob"];
        
        [params setObject:@"register" forKey:@"cmd"];
    }
    
    NSDictionary*    result = [UtilComm registerWithParams:params];
    [DSBezelActivityView removeViewAnimated:NO];
    
    if( result == nil )
    {
        [ErrorProc alertToCheckInternetStateTitle:@"SMS Verify repeat"];
        
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( [response isKindOfClass:[NSDictionary class]] )
    {
        if( response == nil )
        {
            [ErrorProc alertMessage:NSLocalizedString(@"Error",) withTitle:NSLocalizedString(@"Register Failure",@"Create Account")];
            return;
        }
        if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
        {
            [ErrorProc alertRegisterError:response];
        }
        else
        {
            [eSyncronyAppDelegate sharedInstance].strFaceBookID     = [dicRegisterInfo objectForKey:@"facebook_id"];
            [eSyncronyAppDelegate sharedInstance].strLoginEmail     = [dicRegisterInfo objectForKey:@"email"];
            [eSyncronyAppDelegate sharedInstance].strLoginPassword  = [dicRegisterInfo objectForKey:@"password"];
            [eSyncronyAppDelegate sharedInstance].strName  = [dicRegisterInfo objectForKey:@"nname"];
            [eSyncronyAppDelegate sharedInstance].strAccNo = [response objectForKey:@"acc_no"];
            [eSyncronyAppDelegate sharedInstance].strTokenKey = [response objectForKey:@"token_key"];
            [eSyncronyAppDelegate sharedInstance].countryId = [[dicRegisterInfo objectForKey:@"cty_id"] intValue]-1;
            [eSyncronyAppDelegate sharedInstance].membership = [response objectForKey:@"membership"];
            if( [self.Reg_Gender isEqualToString:@"M"] )
                [eSyncronyAppDelegate sharedInstance].gender = 0;
            else
                [eSyncronyAppDelegate sharedInstance].gender = 1;
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"MM/dd/yyyy"];
            [eSyncronyAppDelegate sharedInstance].birthDay = [dateFormatter dateFromString:[dicRegisterInfo objectForKey:@"birthday"]];
            [dateFormatter release];
            
            [eSyncronyAppDelegate sharedInstance].bIsLoggedIn = YES;
            [[eSyncronyAppDelegate sharedInstance] saveLoginInfo];
            
//            if (country == 203){//appending for Thai cancel OTP
//                [_parentView enterSMSVerifyStep];
//            }
//            else{
                [self performSelector:@selector(sendSMSbySelfReg) withObject:nil afterDelay:0.1];
//            }
            
        }
    }
    else
    {
        if([response isEqualToString:@"ERROR 15"])
        {
            [ErrorProc alertMessage:NSLocalizedString(@"Please enter 9 to 12 digits only for contact number.",) withTitle:NSLocalizedString(@"eSynchrony",)];
            return;
        }
        else if([response isEqualToString:@"ERROR 10"])
        {
            [ErrorProc alertMessage:NSLocalizedString(@"Please enter a valid Mobile Number.",) withTitle:NSLocalizedString(@"eSynchrony",)];
            return;
        }
    
        else if([response isEqualToString:@"ERROR 13"])
        {
            
            [ErrorProc alertMessage:NSLocalizedString(@"Wrong City_id",) withTitle:NSLocalizedString(@"eSynchrony",)];
            return;
        }
        else if([response isEqualToString:@"ERROR 14"])
        {
            
            [ErrorProc alertMessage:NSLocalizedString(@"PhoneNumber Already Exist",) withTitle:NSLocalizedString(@"eSynchrony",)];
            return;
        }
        else if([response isEqualToString:@"ERROR 0"])
        {
            [ErrorProc alertMessage:NSLocalizedString(@"User Already Exist or Passwd error",) withTitle:NSLocalizedString(@"eSynchrony",)];
            return;
            
        }
        else if([response isEqualToString:@"ERROR 8"])
        {
            [ErrorProc alertMessage:NSLocalizedString(@"Email Already Exist",) withTitle:NSLocalizedString(@"eSynchrony",)];
            return;
            
        }
        else if([response isEqualToString:@"ERROR 16"])
        {
            
            [ErrorProc alertMessage:NSLocalizedString(@"Wrong gender detected",) withTitle:NSLocalizedString(@"eSynchrony",)];
            return;
        }
        else
            [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Error",)];
        return;
        
    }
    
}

-(void)sendSMSbySelfReg{
    
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
   // [self performSelector:@selector(smsVerifyProcReg) withObject:nil afterDelay:0.01];
    [self performSelector:@selector(sendSMSbySelfREG) withObject:nil afterDelay:0.01];
    
}

-(void)sendSMSbySelfREG{
    
    [self performSelector:@selector(smsVerifyProcReg) withObject:nil afterDelay:0.01];
    return;
    
    //舍弃本机发短信
    MFMessageComposeViewController *picker = [[[MFMessageComposeViewController alloc] init]autorelease];
    BOOL canSendSMS = [MFMessageComposeViewController canSendText];
    CTTelephonyNetworkInfo *info = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *carrier = [info subscriberCellularProvider];
    NSString *code = [carrier mobileNetworkCode];
    
    NSLog(@"can send SMS [%d]\nmobileNetworkCode:%@",canSendSMS,code);
    
    if (canSendSMS&&code.length>0) {
        [info release];
        [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
        
        NSString*   cty_id = [NSString stringWithFormat:@"%d", [eSyncronyAppDelegate sharedInstance].countryId+1];
       
        NSLog(@"sendSMSbySelfREG cityID = %@",cty_id);
        
        NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
        
        [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
        [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
        
        [params setObject:self.Reg_phoneNumber forKey:@"handphone"];
        [params setObject:cty_id forKey:@"cty_id"];
        //[params setObject:[NSNumber numberWithBool:YES] forKey:@"sendServerSMS"];
        [params setObject:@"no" forKey:@"sendServerSMS"];
        
        NSDictionary*    result = [UtilComm smsVerify:params];
        [DSBezelActivityView removeViewAnimated:NO];
        
        if( result == nil )
        {
            //[ErrorProc alertToCheckInternetStateTitle:@"SMS Verify Failture"];
            return;
        }
        
        id response = [result objectForKey:@"response"];
        
        if( [response isKindOfClass:[NSDictionary class]] && [response objectForKey:@"vcode"] != nil )
        {
            self.Reg_vcode = [response objectForKey:@"vcode"];
            picker.messageComposeDelegate = self;
            
            picker.navigationBar.tintColor = [UIColor blackColor];
            
            picker.body = [NSString stringWithFormat:NSLocalizedString(@"Your eSynchrony.com Activation code:%@",@"SMSVerify"),self.Reg_vcode];
            
            picker.recipients = [NSArray arrayWithObject:self.Reg_phoneNumber];
            
            [self presentViewController:picker animated:YES completion:nil];
            
            
        }
        else if( [response isKindOfClass:[NSString class]] )
        {
            if( [response hasPrefix:@"ERROR"] )
                [ErrorProc alertDefaultError:response withTitle:NSLocalizedString(@"SMS Verify Failture",@"SMSVerify")];
            else
                [ErrorProc alertMessage:response withTitle:NSLocalizedString(@"eSynchrony",)];
        }
        else
        {
            [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Error",)];
        }
        
    }else{
        NSLog(@"本机无法发送，改由服务器发送短信");
        [info release];
        [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
        [self performSelector:@selector(smsVerifyProcReg) withObject:nil afterDelay:0.01];
    }
}

//MFMessageComposeViewControllerDelegate
-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    
    //Notifies users about errors associated with the interface
    NSString *error = nil;
    switch (result) {
            
        case MessageComposeResultCancelled:
            error = NSLocalizedString(@"Message cancelled",@"SMSVerify");
            break;
            
        case MessageComposeResultSent:
            break;

        case MessageComposeResultFailed:
            error = NSLocalizedString(@"Message failed",@"SMSVerify");
            break;
            
        default:
            break;
            
    }
    if (error!=nil) {
        NSLog(@"本机发送短信失败，改由服务器发送短信");
        //        [self.navigationController popToViewController:self animated:YES];
        [self performSelector:@selector(smsVerifyProcReg) withObject:nil afterDelay:0.01];
    }else{
        
        [controller dismissViewControllerAnimated:YES completion:nil];
        
        CreateAccountMainViewController *createAccountView = [[[CreateAccountMainViewController alloc] initWithNibName:@"CreateAccountMainViewController" bundle:nil] autorelease];
        
        createAccountView.nStep = 5;
        createAccountView.parentView = self.parentView;
        createAccountView.prevView = self;
        createAccountView.Reg_vcode = self.Reg_vcode;
        createAccountView.Reg_phoneNumber = self.Reg_phoneNumber;
        [self.navigationController pushViewController:createAccountView animated:YES];
    }    
}
- (void)smsVerifyProcReg
{
    
    //--------------------------------------------------------------//
    NSString*   cty_id = [NSString stringWithFormat:@"%d", [eSyncronyAppDelegate sharedInstance].countryId+1];
    
    NSLog(@"cty_id cty_id====%@", cty_id);
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];

    [params setObject:self.Reg_phoneNumber forKey:@"handphone"];
    [params setObject:cty_id forKey:@"cty_id"];
//    [NSNumber numberWithBool:YES]-->@"no"
//    [params setObject:@"no" forKey:@"sendServerSMS"];//for test
    
    NSDictionary*    result = [UtilComm smsVerify:params];
    
    [DSBezelActivityView removeViewAnimated:NO];
    
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"SMS Verify Failture"];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    
    if( [response isKindOfClass:[NSDictionary class]] && [response objectForKey:@"vcode"] != nil )
    {
        //NSLog(@"%d",self.nStep);//
        if (self.nStep==4) {//
            
            self.Reg_vcode = [response objectForKey:@"vcode"];
            
            CreateAccountMainViewController *createAccountView = [[[CreateAccountMainViewController alloc] initWithNibName:@"CreateAccountMainViewController" bundle:nil] autorelease];
            
            createAccountView.nStep = 5;
            createAccountView.parentView = self.parentView;
            createAccountView.prevView = self;
            createAccountView.Reg_vcode = self.Reg_vcode;
            createAccountView.Reg_phoneNumber = self.Reg_phoneNumber;
            [self.navigationController pushViewController:createAccountView animated:YES];
            
            [DSBezelActivityView removeViewAnimated:NO];
        }
        else{
            
            [self startTime];
            
        }
    }
    else if( [response isKindOfClass:[NSString class]] )
    {
        if( [response hasPrefix:@"ERROR"] )
            [ErrorProc alertDefaultError:response withTitle:NSLocalizedString(@"SMS Verify Failture",@"SMSVerify")];
        else
            [ErrorProc alertMessage:response withTitle:NSLocalizedString(@"eSynchrony",)];
    }
    else
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Error",)];
    }
    
}

- (IBAction)didClickVerify:(id)sender {
    
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(smsVerifyCodeProcReg) withObject:nil afterDelay:0.01];
}

- (void)smsVerifyCodeProcReg
{
    
//    NSString*   cty_id = [NSString stringWithFormat:@"%d", [eSyncronyAppDelegate sharedInstance].countryId+1];
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
//    [params setObject:cty_id forKey:@"city_id"];//append 151208
    [params setObject:self.Reg_phoneNumber forKey:@"handphone"];
    [params setObject:txtSMSCode.text forKey:@"smscode"];
    
    NSDictionary*    result = [UtilComm smsVerifyCode:params];
    [DSBezelActivityView removeViewAnimated:NO];
    
    if( result == nil )
    {
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( [response isKindOfClass:[NSString class]] )
    {
        if( [response isEqualToString:@"true"] )
        {
            [self moveNext];
        }
        else if( [response hasPrefix:@"ERROR"] )
            [ErrorProc alertDefaultError:response withTitle:NSLocalizedString(@"SMS Verify Code Failture",@"SMSVerify")];
        else
            [ErrorProc alertMessage:NSLocalizedString(@"The activation code is not verified, please try again.",@"SMSVerify") withTitle:NSLocalizedString(@"Verification",@"SMSVerify")];
    }
    else
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Error",)];
    }

}

- (void)moveNext
{
    [_parentView enterWelcomeView];
}
-(void)startTime{
    
    __block int timeout=10; //倒计时时间
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer, ^{
        if(timeout<=0){ //倒计时结束，关闭
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                [self.btnResend setTitle:NSLocalizedString(@"RESEND",@"SMSVerify") forState:UIControlStateNormal];
                self.btnResend.userInteractionEnabled = YES;
            });
        }else{
            //            int minutes = timeout / 60;
            int seconds = timeout % 60;
            NSString *strTime = [NSString stringWithFormat:@"%.2d", seconds];
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                //                NSLog(@"____%@",strTime);
                [self.btnResend setTitle:[NSString stringWithFormat:NSLocalizedString(@"RESEND(%@)",@"SMSVerify"),strTime] forState:UIControlStateNormal];
                self.btnResend.userInteractionEnabled = NO;
                
            });
            timeout--;
        }
    });
    dispatch_resume(_timer);
    
}
- (IBAction)didClickResend:(id)sender {
    
    [self sendSMSbySelfReg];
    
}

- (IBAction)didClickBackVerify:(id)sender {
    
//    [self.navigationController popToRootViewControllerAnimated:NO];
//    [[eSyncronyAppDelegate sharedInstance].viewController procCheckUserStep];
    
    [self performSelector:@selector(gotoVerifyPhoneNum) withObject:nil afterDelay:0.01];
}

-(void)gotoVerifyPhoneNum
{
    SMSVerifyViewController *smsVerifyStepView = [[[SMSVerifyViewController alloc] initWithNibName:@"SMSVerifyViewController" bundle:nil] autorelease];
    smsVerifyStepView.parentView = self.parentView;
    smsVerifyStepView.step = 1;
    
    [self.navigationController pushViewController:smsVerifyStepView animated:YES];
}

- (void)initSetting
{
    liveinLabFld.text = [self.regionArray objectAtIndex:self.selectedIdx];
}

- (void)initSetting1
{
    lbHearusfrom.text = [self.HearArray objectAtIndex:self.selectedIdx];
    NSLog(@"%@",lbHearusfrom.text);
    
    NSDictionary *result=[UtilComm requestHearusfrom];
    id response = [result objectForKey:@"response"];
    
    //    NSLog(@"%@",response);
    
    //    dicHearusform = [NSMutableDictionary dictionaryWithContentsOfURL:[NSURL URLWithString:url]];
    dicHearusform = [NSMutableDictionary dictionary];
    
    if( [response isKindOfClass:[NSDictionary class]] )
    {
        id item=[response objectForKey:@"item"];
        if([item isKindOfClass:[NSArray class]])
        {
            int num=(int)[item count];
            
            for (int i=0;i<num;i++) {
                NSDictionary *dic = [item objectAtIndex:i];
//                NSString *hearusfrom=[dic objectForKey:@"hearusfrom"];//repair 151210
                NSString *hearusfrom= [NSString stringWithFormat:@"%@",NSLocalizedString([dic objectForKey:@"hearusfrom"], @"hearsfrom")];
                NSString *value=[dic objectForKey:@"value"];
                [dicHearusform setObject:hearusfrom forKey:value];
            }
        }
    }
    
    NSLog(@"dicHearusfrom = %@",dicHearusform);
//    self.HEARArray=[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"Bing Search",@"21",@"Blog",@"15",@"Brochure/Flyer",@"47",@"Email",@"22",@"Facebook",@"18",@"Google",@"19",@"Groupon",@"26",@"Instagram",@"51",@"LA Group",@"24",@"Magazine",@"7",@"Mobile Ads",@"50",@"New World Media",@"43",@"Newspaper",@"6",@"Online Ad",@"4",@"Radio",@"8",@"Television",@"9",@"Word of Mouth",@"5",@"Yahoo",@"20",@"Facebook Ad",@"1",@"Google Ad",@"2",@"Youtube Ad",@"44",@"Yahoo Ad",@"3",nil];
    
//    NSEnumerator *keys1 = [self.HEARArray keyEnumerator];
//   // NSEnumerator *objs1 = [self.HEARArray objectEnumerator];
//    for (NSObject *keys2 in keys1) {
//        NSLog(@"obj: %@", keys2);
//        //通过KEY找到value
//        NSObject *object = [self.HEARArray objectForKey:keys2];
//        NSLog(@"object==%@",object);
//        if ([object isEqual:lbHearusfrom.text ]) {
//            self.hear = keys2;
//            NSLog(@"self.hearValue=%@",self.hear);
//            return;
//        }   
//    }
    NSEnumerator *keys1 = [dicHearusform keyEnumerator];
    // NSEnumerator *objs1 = [self.HEARArray objectEnumerator];
    for (NSObject *keys2 in keys1) {
        NSLog(@"obj: %@", keys2);
        //通过KEY找到value
//        NSObject *object = [dicHearusform objectForKey:keys2];
        NSObject *object = [NSString stringWithFormat:@"%@",NSLocalizedString([dicHearusform objectForKey:keys2], @"hearusfrom")];//151210
        NSLog(@"object==%@",object);
        if ([object isEqual:lbHearusfrom.text ]) {
            self.hear =(NSString *)keys2;
//            self.hear =keys2;
//            lbHearusfrom.text = [NSString stringWithFormat:@"%@",NSLocalizedString(lbHearusfrom.text, @"hearusfrom")];
            NSLog(@"self.hearValue=%@",self.hear);
            return;
        }
    }
    if (self.hear == nil) {
//        lbHearusfrom.text = @"Facebook";
        self.hear = @"18";
    }
    
    
    
}

- (void)refreshContent
{
    [chkbtnAgreetoAll setSelected:self.isTermChecked];
    [chkbtnDeclare setSelected:self.isSingleChecked];
    
    //lbHearusfrom.text=[self.HearArray objectAtIndex:self.selectedIdx];//append
    emailTxtFld.text = self.Reg_Email;
    pwTxtFld.text = self.Reg_Password;
    pwConfirmTxtFld.text = self.Reg_PasswordConfirm;
}

-(void)dealloc
{
    [nameTxtFld release];
    [liveinLabFld release];
    
    [subViewStep1 release];
    [subViewStep2 release];
    
    [scrlView release];
    
    [emailTxtFld release];
    [pwTxtFld release];
    [pwConfirmTxtFld release];
    
    [chkbtnAgreetoAll release];
    [chkbtnDeclare release];
    
    [btnMale release];
    [btnFemale release];

    [self.regionArray release];
    
    [self.Reg_Name release];
    [self.Reg_Gender release];
    [self.Reg_Email release];
    [self.Reg_Password release];
    [self.Reg_facebookId release];
    [self.firstname release];
    [self.lastname release];
    [self.location release];
    [self.birthday release];

    [fbBtn release];
    [fbLbl release];
    [fbImage1 release];
    [fbImage2 release];
    [fbImage3 release];
    [fbImage4 release];
    [fbLbl2 release];
    [subViewStep3 release];
    [lbBirthday release];
    [lbYearsOld release];
    [pickerDate release];
    [subViewStep4 release];
    [subViewStep5 release];
    [txtPhoneNumber release];
    [txtSMSCode release];
    
    [_btnResend release];
//    [lbHearusfrom release];
    [pAgreeLab release];
    [pTermsLab release];
    [pTermsBtn release];
    [pBornLab release];
    
//    [dicHearusform release];
    
//    [pStarImage release];
//    [pEmailLab release];
    [super dealloc];
}

@end
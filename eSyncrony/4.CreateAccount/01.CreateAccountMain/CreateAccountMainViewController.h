
#import <UIKit/UIKit.h>
#import "DataSelectViewController.h"
#import "HearSelectViewController.h"
#import "InputBaseViewController.h"
#import "SMSVerifyViewController.h"

#import <MessageUI/MFMessageComposeViewController.h>

@class eSyncronyViewController;

#define APP_ID @"226737744171417"

@interface CreateAccountMainViewController :InputBaseViewController <UITextFieldDelegate, UIWebViewDelegate, DataSelectViewDelegate,MFMessageComposeViewControllerDelegate,HearSelectViewDelegate>
{
    IBOutlet UIScrollView*      scrlView;
    IBOutlet UIView*            subViewStep1;
    IBOutlet UIView*            subViewStep2;
    IBOutlet UIView*            subViewStep3;
    
    IBOutlet UIView *subViewStep4;
    IBOutlet UIView *subViewStep5;
    
    IBOutlet UITextField *txtPhoneNumber;
    IBOutlet UITextField *txtSMSCode;

    IBOutlet UITextField*       nameTxtFld;
    IBOutlet UILabel*           liveinLabFld;
    IBOutlet UITextField*       emailTxtFld;
    IBOutlet UITextField*       pwTxtFld;
    IBOutlet UITextField*       pwConfirmTxtFld;
    
    IBOutlet UIButton*          chkbtnAgreetoAll;
    IBOutlet UIButton*          chkbtnDeclare;
    
    IBOutlet UIButton*          btnMale;
    IBOutlet UIButton*          btnFemale;
    
    IBOutlet UILabel*           lbMySex;
    IBOutlet UILabel*           lbDeclare;

    IBOutlet UITextField *lbHearusfrom;
    
    IBOutlet UIButton *fbBtn;
    IBOutlet UILabel *fbLbl;
    IBOutlet UIImageView *fbImage1;
    IBOutlet UIImageView *fbImage2;
    IBOutlet UIImageView *fbImage3;
    IBOutlet UIImageView *fbImage4;
    IBOutlet UILabel *fbLbl2;
    
    
    IBOutlet UILabel *lbBirthday;
    IBOutlet UILabel *lbYearsOld;
    IBOutlet UIDatePicker *pickerDate;
    
    //save variables
    int     mDOB_year;
    int     mDOB_month;
    int     mDOB_day;
    
}

@property (nonatomic, assign)   int nStep;
/////////////hear us from///////////////
//@property (nonatomic, assign)   int hearValue;
@property (nonatomic, strong) NSString  *hear;

@property (nonatomic, assign)   int Reg_CountryId; //0: Singapore, 1: Malaysia, 2: HONG KONG, 7:Indonesia

@property (nonatomic, assign)   eSyncronyViewController* parentView;

@property (nonatomic, assign)   CreateAccountMainViewController* prevView;

//local variables
@property (nonatomic, assign)   BOOL    isSingleChecked;
@property (nonatomic, assign)   BOOL    isTermChecked;
@property (nonatomic, assign)   BOOL    isFaceBookLogin;

@property (nonatomic, assign)   int             selectedIdx;
@property (nonatomic, strong)   NSMutableArray* regionArray;

@property (nonatomic, strong)   NSMutableArray* HearArray;
@property (nonatomic, strong)   NSMutableDictionary* HEARArray;
@property (nonatomic, strong)   NSMutableDictionary* dicHearusform;
@property (nonatomic, strong)   NSString*       Reg_Name;
@property (nonatomic, strong)   NSString*       Reg_Gender;
@property (nonatomic, strong)   NSString*       Reg_Email;

@property (nonatomic, strong)   NSString*       Reg_Password;
@property (nonatomic, strong)   NSString*       Reg_PasswordConfirm;

@property (nonatomic, strong)   NSString*       Reg_facebookId;
@property (nonatomic, strong)   NSString*       Reg_phoneNumber;
@property (nonatomic, strong)   NSString*       Reg_vcode;

@property (retain, nonatomic) IBOutlet UIButton *btnResend;


@property (nonatomic, strong) NSString  *firstname;
@property (nonatomic, strong) NSString  *lastname;
@property (nonatomic, strong) NSString  *birthday;
@property (nonatomic, strong) NSString  *location;

@property (nonatomic, strong) NSMutableDictionary *dicRegisterInfo;

@property (nonatomic, strong)SMSVerifyViewController* SMSVerifyViewController;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *activityIndicator;

@property(nonatomic, strong)    NSDate *birthDate;
- (NSString*)getDOB;



- (IBAction)didClickBackBtn:(id)sender;
- (IBAction)didClickMaleBtn:(id)sender;
- (IBAction)didClickFemaleBtn:(id)sender;

- (IBAction)didClickContinueBtn:(id)sender;

- (IBAction)didClickFbLogin:(id)sender;
- (IBAction)didClickTermsAndCondition:(id)sender;
- (IBAction)didClickAgreetoAll:(id)sender;
- (IBAction)didClickDeclare:(id)sender;
- (IBAction)didClickSelectRegionBox:(id)sender;

- (IBAction)didClickCreateAccount:(id)sender;
- (IBAction)didClickContinueNext:(id)sender;

- (IBAction)didClickHearusfrom:(id)sender;
- (IBAction)changeDate:(UIDatePicker *)sender;
//- (IBAction)btnNext:(UIButton *)sender;

- (IBAction)didClickPhoneNumber:(id)sender;
- (IBAction)didClickVerify:(id)sender;
- (IBAction)didClickResend:(id)sender;
- (IBAction)didClickBackVerify:(id)sender;


- (void)initSetting;
//- (void)didLogin;
- (void)procLoginWithFB;
- (void)smsVerifyCodeProcReg;


-(void)startTime;


@end

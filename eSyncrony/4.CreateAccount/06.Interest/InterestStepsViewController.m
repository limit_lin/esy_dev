
#import "Global.h"
#import "UtilComm.h"
#import "InterestStepsViewController.h"
#import "eSyncronyViewController.h"
#import "RegisterTableViewCell.h"
#import "eSyncronyAppDelegate.h"
#import "DSActivityView.h"

@interface InterestStepsViewController ()

@end

@implementation InterestStepsViewController
{
    NSString *_language;
}

int     _interest_arrSelIndexs2[2];

@synthesize arrSelIndexs1;
@synthesize parentView;
@synthesize step;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"InterestStepsView";
    
  _language = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    if(arrStringForQuestion == nil)
    {
        arrStringForQuestion = [[NSMutableArray alloc] init];
        [arrStringForQuestion addObject:NSLocalizedString(@"Food that I like",@"InterestSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"My hobbies are ",@"InterestSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"Sports that I like to do",@"InterestSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"Movies I like to watch",@"InterestSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"Music that I listen to",@"InterestSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"Type of holiday I like to go",@"InterestSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"How important is it for your partner and you to have similar interest and hobbies?",@"InterestSteps")];
        [arrStringForQuestion addObject:NSLocalizedString(@"How important is it for your partner and you to have similar interest and hobbies for pets?",@"InterestSteps")];
    }
    if(arrIndexForView == nil)
    {
        arrIndexForView = [[NSMutableArray alloc] init];
        [arrIndexForView addObject:@"1"];
        [arrIndexForView addObject:@"1"];
        [arrIndexForView addObject:@"1"];
        [arrIndexForView addObject:@"1"];
        [arrIndexForView addObject:@"1"];
        [arrIndexForView addObject:@"1"];
        [arrIndexForView addObject:@"2"];
        [arrIndexForView addObject:@"2"];
    }
    
    if ([_language hasPrefix:@"zh-Hant"]||[_language hasPrefix:@"zh-HK"]){//repair 151224
        
    if( arrNameOfInfoList == nil )
    {
        arrNameOfInfoList = [[NSMutableArray alloc] init];
        [arrNameOfInfoList addObject:[NSString stringWithFormat:@"fav_food1"]];
        [arrNameOfInfoList addObject:[NSString stringWithFormat:@"fav_hobby1"]];
        [arrNameOfInfoList addObject:[NSString stringWithFormat:@"fav_sport1"]];
        [arrNameOfInfoList addObject:[NSString stringWithFormat:@"fav_movie1"]];
        [arrNameOfInfoList addObject:[NSString stringWithFormat:@"fav_music1"]];
        [arrNameOfInfoList addObject:[NSString stringWithFormat:@"fav_holiday1"]];
        [arrNameOfInfoList addObject:[NSString stringWithFormat:@""]];
        [arrNameOfInfoList addObject:[NSString stringWithFormat:@""]];
    }
     }
    else
    {
        if( arrNameOfInfoList == nil )
        {
            arrNameOfInfoList = [[NSMutableArray alloc] init];
            [arrNameOfInfoList addObject:[NSString stringWithFormat:@"fav_food"]];
            [arrNameOfInfoList addObject:[NSString stringWithFormat:@"fav_hobby"]];
            [arrNameOfInfoList addObject:[NSString stringWithFormat:@"fav_sport"]];
            [arrNameOfInfoList addObject:[NSString stringWithFormat:@"fav_movie"]];
            [arrNameOfInfoList addObject:[NSString stringWithFormat:@"fav_music"]];
            [arrNameOfInfoList addObject:[NSString stringWithFormat:@"fav_holiday"]];
            [arrNameOfInfoList addObject:[NSString stringWithFormat:@""]];
            [arrNameOfInfoList addObject:[NSString stringWithFormat:@""]];
        }
        
    }

    if(arrSubViewCont == nil)
    {
        arrSubViewCont = [[NSMutableArray alloc] init];
        [arrSubViewCont addObject:subView1];
        [arrSubViewCont addObject:subView2];
    }
    
    if( self.step == 1 )
    {
        [btnBack setHidden:YES];

        self.arrSelIndexs1 = [[NSMutableArray alloc] initWithCapacity:0];
        for( int i = 0; i < 6; i++ )
        {
            NSMutableArray* selItems = [NSMutableArray arrayWithCapacity:0];
            [self.arrSelIndexs1 addObject:selItems];
        }
        
        _interest_arrSelIndexs2[0] = -1;
        _interest_arrSelIndexs2[1] = -1;
    }
    else
    {
        [btnBack setHidden:NO];
    }
    
    bInit = FALSE;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    subView1.frame = scrView.bounds;
    subView2.frame = scrView.bounds;
    
    if( bInit == FALSE )
    {
        [self setContentsToStep];
        bInit = TRUE;
    }
}

- (IBAction)didClickBtnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (int)getRealIndexFromIndex:(int)index listDis:(NSArray*)listDis listOrg:(NSArray*)listOrg listNum:(NSArray*)listNum
{
    NSString*       strDisItem = [listDis objectAtIndex:index];
    int             nSel = 0;
     
    for( int k = 0; k < [listOrg count]; k++ )
    {
        NSString*   strOrgItem = [listOrg objectAtIndex:k];
        if( [strDisItem isEqualToString:strOrgItem] )
        {
            nSel = k;
            break;
        }
    }

    if( nSel < [listNum count] )
        nSel = [[listNum objectAtIndex:nSel] intValue];

    return nSel;
}

- (NSArray*)getRealIndexesList
{
    NSMutableArray* realIndexesList = [NSMutableArray arrayWithCapacity:0];
    
    _language = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    if ([_language hasPrefix:@"zh-Hant"]||[_language hasPrefix:@"zh-HK"]){//repair 151224
        for( int i = 0; i < 6; i++ )
        {
            NSArray*    indPathList = [self.arrSelIndexs1 objectAtIndex:i];
            NSString*   indexString = @"";
            
            for( int k = 0; k < [indPathList count]; k++ )
            {
                NSIndexPath*    indPath = [indPathList objectAtIndex:k];
                    
                indexString = [indexString stringByAppendingFormat:@"%ld|",indPath.row];
            }
            
            [realIndexesList addObject:indexString];
            
            //NSLog(@"%@",realIndexesList);
        }
        
    }
    else//en
    {
        for( int i = 0; i < 6; i++ )
        {
            NSString    *strPlist = [arrNameOfInfoList objectAtIndex:i];
            NSString    *strPlist1 = [NSString stringWithFormat:@"%@1", strPlist];
            NSString    *strPlistNum = [NSString stringWithFormat:@"%@_num", strPlist];
            
            NSArray*    listDis = [Global loadQuestionArray:strPlist];
            NSArray*    listOrg = [Global loadQuestionArray:strPlist1];
            NSArray*    listNum = [Global loadQuestionArray:strPlistNum];
            
            NSArray*    indPathList = [self.arrSelIndexs1 objectAtIndex:i];
            NSString*   indexString = @"";
            
            for( int k = 0; k < [indPathList count]; k++ )
            {
                NSIndexPath*    indPath = [indPathList objectAtIndex:k];
                
                int index = [self getRealIndexFromIndex:(int)indPath.row listDis:listDis listOrg:listOrg listNum:listNum];
                
                indexString = [indexString stringByAppendingFormat:@"%d|", index];
                
                
            }
            
            [realIndexesList addObject:indexString];
        }
    }
    return realIndexesList;
}

- (void)saveInterestProc
{
    
    NSArray*    indexList = [self getRealIndexesList];

    //--------------------------------------------------------------//
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];

    [params setObject:[indexList objectAtIndex:0] forKey:@"food"];
    [params setObject:[indexList objectAtIndex:1] forKey:@"hobby"];
    [params setObject:[indexList objectAtIndex:2] forKey:@"sport"];
    [params setObject:[indexList objectAtIndex:3] forKey:@"film"];
    [params setObject:[indexList objectAtIndex:4] forKey:@"music"];
    [params setObject:[indexList objectAtIndex:5] forKey:@"holiday"];
    
    [params setObject:[NSNumber numberWithInt:_interest_arrSelIndexs2[0]] forKey:@"impt_hobby"];
    [params setObject:[NSNumber numberWithInt:_interest_arrSelIndexs2[1]] forKey:@"impt_pet"];
    
   // NSLog(@"params=======%@\n",params);
    

    NSDictionary*    result = [UtilComm saveInterestInfo:params];
    [DSBezelActivityView removeViewAnimated:NO];

    if( result == nil )
    {
//        //[ErrorProc alertToCheckInternetStateTitle:@"Saving Failure"];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( response == nil )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Saving Failure",)];
        return;
    }
//    NSDictionary *error = [[result objectForKey:@"response"]objectForKey:@"error"];
//    if( error != nil )
//    {
//        [ErrorProc alertMessage:@"Error" withTitle:@"Saving Failure"];
//        return;
//    }
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [ErrorProc alertError:response withTitle:NSLocalizedString(@"setInterestHobbies Failure",)];
    }
    else
    {
        [self moveNextWindow];
    }
}

-(void)moveNextWindow
{
  //  [self.navigationController popToRootViewControllerAnimated:NO];
    [parentView enterDisplayComplete:3];
}

- (void)saveInterestAndMoveNext
{
    //[self moveNextWindow];
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Saving...",)];
    [self performSelector:@selector(saveInterestProc) withObject:nil afterDelay:0.01];
}

- (BOOL)checkValidChoice
{
    
    if( step <= 6 )
    {
        NSArray*    selItems = [arrSelIndexs1 objectAtIndex:step-1];
        if( [selItems count] >= 3 )
            return TRUE;
        
        [ErrorProc alertMessage:NSLocalizedString(@"Please select 3 or more choices.",@"InterestSteps") withTitle:NSLocalizedString(@"Field Required",)];
    }
    else //if( step >= 7 )
    {
        int     nSel = _interest_arrSelIndexs2[step-7];
        if( nSel != -1 )
            return TRUE;

        [ErrorProc alertMessage:NSLocalizedString(@"Please select your choice",) withTitle:NSLocalizedString(@"Field Required",)];
    }

    return FALSE;
}

- (IBAction)didClickBtnNext:(id)sender
{
    if( [self checkValidChoice] == FALSE )
        return;
    
    if (step >= 7) {
        
        [interestNext setHidden:YES];
    }
    
    if( step == 8 )
    {
        [self saveInterestAndMoveNext];
        return;
    }
    
    if( self.nextView == nil )
        self.nextView = [[InterestStepsViewController alloc] initWithNibName:@"InterestStepsViewController" bundle:nil];

    self.nextView.parentView = self.parentView;
    self.nextView.step = self.step+1;
    self.nextView.arrSelIndexs1 = self.arrSelIndexs1;

    [self.navigationController pushViewController:self.nextView animated:YES];
    
}

-(void)interestSteps_actionNext
{
    if( [self checkValidChoice] == FALSE )
        return;
    
    if( step == 8 )
    {
        [self saveInterestAndMoveNext];
        return;
    }
        if( self.nextView == nil )
            self.nextView = [[InterestStepsViewController alloc] initWithNibName:@"InterestStepsViewController" bundle:nil];
        
        self.nextView.parentView = self.parentView;
        self.nextView.step = self.step+1;
        self.nextView.arrSelIndexs1 = self.arrSelIndexs1;
        
        [self.navigationController pushViewController:self.nextView animated:YES];
}

- (void)selectChosenLevel
{
    if( step <= 6 )
    {
        NSArray*    selItems = [arrSelIndexs1 objectAtIndex:step-1];

        for( int k = 0; k < [selItems count]; k++ )
        {
            NSIndexPath* selIndex = [selItems objectAtIndex:k];
            
            NSInteger     nSel = selIndex.row;

            [tableView selectRowAtIndexPath:[NSIndexPath indexPathForItem:nSel inSection:0]
                                   animated:NO
                             scrollPosition:UITableViewScrollPositionBottom];
        }
    }
    else// if( step >= 7 )
    {
        int         nSel = _interest_arrSelIndexs2[step-7];
        UIButton*   btnLevels[] = { btnLevel1, btnLevel2, btnLevel3, btnLevel4, btnLevel5 };
        
        for( int i = 0; i < 5; i++ )
            [btnLevels[i] setSelected:NO];
        
        if( nSel >= 0 && nSel < 5 )
            [btnLevels[nSel] setSelected:YES];
    }
}

- (void)setContentsToStep
{
    if (step >= 7) {//append
        
        [interestNext setHidden:YES];
    }
    
    int index = [[arrIndexForView objectAtIndex:step-1] intValue];
    UIView* tmpView = [arrSubViewCont objectAtIndex:index-1];
    [scrView addSubview:tmpView];
    scrView.contentSize = tmpView.bounds.size;

    if( index == 1 )
    {
        lbQuestionType1.text = [arrStringForQuestion objectAtIndex:step-1];
    }
    else if( index == 2 )
    {
        NSString*   titleHead = NSLocalizedString(@"How important is it for your partner and you to have similar ",@"InterestSteps");
        NSString*   strTitle = [arrStringForQuestion objectAtIndex:step-1];
        NSMutableAttributedString* string = [[NSMutableAttributedString alloc]initWithString:strTitle];

        NSRange range1, range2, range3, range;
        
        range1 = NSMakeRange(0, [titleHead length]);
        range2 = NSMakeRange(range1.length, [strTitle length]-range1.length-1);
        range3 = NSMakeRange(range2.location+range2.length, 1);
        range = NSMakeRange(0, [strTitle length]);
        
        UIColor*    color1 = LEVEL1_COLOR;
        UIColor*    color2 = LINE_TEXT_COLOR;
        
        [string addAttribute:NSForegroundColorAttributeName value:color1 range:range1];
        [string addAttribute:NSForegroundColorAttributeName value:color2 range:range2];
        [string addAttribute:NSForegroundColorAttributeName value:color1 range:range3];
        
        [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeueLTStd-MdCn" size:16] range:range];

        //txtQuestionType2.text = 0;
        [txtQuestionType2 setAttributedText:string];
        txtQuestionType2.textAlignment = NSTextAlignmentCenter;
    }

    [self initializeTableContent];
    [tableView reloadData];
    [self resizeTableView];
    
    tableView.contentOffset = CGPointZero;
    
    //--------------------------------------//
    [self selectChosenLevel];
}

- (void)initializeTableContent
{
    NSString*   strPlist = [arrNameOfInfoList objectAtIndex:step - 1];
    arrQuestion = [[NSMutableArray alloc] initWithArray:[Global loadQuestionArray:strPlist]];
}

- (void)selectLevel:(int)level
{
    _interest_arrSelIndexs2[step-7] = level+1;

    UIButton*   btnLevels[] = { btnLevel1, btnLevel2, btnLevel3, btnLevel4, btnLevel5 };
    
    for( int i = 0; i < 5; i++ )
        [btnLevels[i] setSelected:NO];
    
    if( level >= 0 && level < 5 )
        [btnLevels[level] setSelected:YES];
}

- (IBAction)didClickLevel1:(id)sender
{
    [self selectLevel:0];
  //append
    [self interestSteps_actionNext];
}

- (IBAction)didClickLevel2:(id)sender
{
    [self selectLevel:1];
    //append
    [self interestSteps_actionNext];
}

- (IBAction)didClickLevel3:(id)sender
{
    [self selectLevel:2];
    //append
    [self interestSteps_actionNext];
}

- (IBAction)didClickLevel4:(id)sender
{
    [self selectLevel:3];
    //append
    [self interestSteps_actionNext];
}

- (IBAction)didClickLevel5:(id)sender
{
    [self selectLevel:4];
    //append
    [self interestSteps_actionNext];
}

- (void)resizeTableView
{
    int maxCount = ([UIScreen mainScreen].bounds.size.height - 144)/42;
    
    if( [arrQuestion count] < maxCount )
    {
        [tableView setFrame:CGRectMake(tableView.frame.origin.x, tableView.frame.origin.y, tableView.frame.size.width, 42 * [arrQuestion count])];
    }
    else
    {
        [tableView setFrame:CGRectMake(tableView.frame.origin.x, tableView.frame.origin.y, tableView.frame.size.width, 42 * maxCount )];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrQuestion count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 42;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RegisterTableViewCell *cell;
    NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"RegisterTableViewCell" owner:nil options:nil];
    cell = [arr objectAtIndex:0];
    NSInteger row = [indexPath row];
    //NSDictionary *_storeObj =[arrFriendList objectAtIndex:row];
    NSString * str =  [arrQuestion objectAtIndex:row];
    cell.lbContent.text = str;
    return cell;
    
}

- (void)tableView:(UITableView *)curTableView didSelectRowAtIndexPath:(NSIndexPath *)newIndexPath
{
    NSArray*    arrIndPaths = tableView.indexPathsForSelectedRows;
    
    if( arrIndPaths == nil )
        [arrSelIndexs1 setObject:[NSMutableArray arrayWithCapacity:0] atIndexedSubscript:step-1];
    else
        [arrSelIndexs1 setObject:arrIndPaths atIndexedSubscript:step-1];
    //NSLog(@"chosen arrIndPaths ===%@\n",arrIndPaths);
}

- (void)tableView:(UITableView *)curTableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray*    arrIndPaths = tableView.indexPathsForSelectedRows;
    
    if( arrIndPaths == nil )
        [arrSelIndexs1 setObject:[NSMutableArray arrayWithCapacity:0] atIndexedSubscript:step-1];
    else
        [arrSelIndexs1 setObject:arrIndPaths atIndexedSubscript:step-1];
}

- (void)dealloc
{
    [scrView release];
    [subView1 release];
    [subView2 release];
   
    [tableView release];
    [lbQuestionType1 release];
    [txtQuestionType2 release];
   
    [btnBack release];

    [btnLevel1 release];
    [btnLevel2 release];
    [btnLevel3 release];
    [btnLevel4 release];
    [btnLevel5 release];

    [arrIndexForView release];
    [arrStringForQuestion release];
    
    [arrQuestion release];
    [arrSubViewCont release];
    [arrNameOfInfoList release];
    
    [self.nextView release];

    if( self.step == 1 )
        [arrSelIndexs1 release];
   
    [interestNext release];
    [super dealloc];
}
@end


#import <UIKit/UIKit.h>
@class eSyncronyViewController;

@interface InterestStepsViewController : GAITrackedViewController<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UIScrollView*      scrView;
    IBOutlet UIView*            subView1;
    IBOutlet UIView*            subView2;
    
    IBOutlet UITableView*       tableView;
    IBOutlet UILabel*           lbQuestionType1;
    IBOutlet UITextView*        txtQuestionType2;
    
    IBOutlet UIButton*          btnBack;
    
    IBOutlet UIButton*          btnLevel1;
    IBOutlet UIButton*          btnLevel2;
    IBOutlet UIButton*          btnLevel3;
    IBOutlet UIButton*          btnLevel4;
    IBOutlet UIButton*          btnLevel5;
    
    NSMutableArray*             arrIndexForView;
    NSMutableArray*             arrStringForQuestion;
    
    NSMutableArray*             arrQuestion;
    NSMutableArray*             arrSubViewCont;
    NSMutableArray*             arrNameOfInfoList;
    
    BOOL    bInit;
    
    IBOutlet UIButton *interestNext;
}

@property(nonatomic, assign) int    step;
@property(nonatomic, assign) NSMutableArray*     arrSelIndexs1;
@property(nonatomic, assign) InterestStepsViewController* nextView;

@property(nonatomic, assign) eSyncronyViewController* parentView;

- (IBAction)didClickBtnBack:(id)sender;
- (IBAction)didClickBtnNext:(id)sender;

- (IBAction)didClickLevel1:(id)sender;
- (IBAction)didClickLevel2:(id)sender;
- (IBAction)didClickLevel3:(id)sender;
- (IBAction)didClickLevel4:(id)sender;
- (IBAction)didClickLevel5:(id)sender;

- (void)initializeTableContent;
@end

//
//  ReasonsTableViewCell.h
//  eSyncrony
//
//  Created by iosdeveloper on 14-5-30.
//  Copyright (c) 2014年 WonMH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReasonsTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imageReason;
@property (strong, nonatomic) IBOutlet UIButton *btnProceed;
@property (retain, nonatomic) IBOutlet UIView *viewBackground;
@property (retain, nonatomic) IBOutlet UILabel *lblReason;
@property (retain, nonatomic) IBOutlet UILabel *lblReasonContent;


@end

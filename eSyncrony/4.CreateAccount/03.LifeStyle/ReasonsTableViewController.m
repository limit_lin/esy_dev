//
//  ReasonsTableViewController.m
//  eSyncrony
//
//  Created by iosdeveloper on 14-5-30.
//  Copyright (c) 2014年 WonMH. All rights reserved.
//

#import "ReasonsTableViewController.h"
#import "ReasonsTableViewCell.h"
#import "eSyncronyAppDelegate.h"
#import "eSyncronyViewController.h"
#import "Global.h"
@interface ReasonsTableViewController ()
{
    IBOutlet UIButton *pNextBtn;
}
@end

@implementation ReasonsTableViewController
@synthesize parentView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //append
        CGRect tableViewRect = CGRectMake(0.0, 0.0, 50.0, 320.0);
        self.tableviewReasons= [[UITableView alloc] initWithFrame:tableViewRect style:UITableViewStylePlain];
//        self.tableviewReasons.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
       self.tableviewReasons.center = CGPointMake(self.tableviewReasons.frame.size.width / 2, self.view.frame.size.height / 2);
        
//        NSLog(@"%f,%f,%f,%f",self.tableviewReasons.frame.origin.x,self.tableviewReasons.frame.origin.y,self.tableviewReasons.frame.size.width,self.tableviewReasons.frame.size.height);
        
        self.tableviewReasons.delegate = self;
        self.tableviewReasons.dataSource = self;
        
        //tableview逆时针旋转90度。
        self.tableviewReasons.transform = CGAffineTransformMakeRotation(-M_PI / 2);
        // scrollbar 不显示
        self.tableviewReasons.showsVerticalScrollIndicator = YES;
        //append end
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.screenName = @"20ReasonsView";
    arrReasons = [[NSArray alloc] initWithArray:[Global loadQuestionArray:@"20reason_list"]];
    
    self.tableviewReasons.backgroundColor = [UIColor whiteColor];
    
//    if ([[eSyncronyAppDelegate sharedInstance].language hasPrefix:@"id"]) {
//        
//        CGSize pNextBtnSize = [pNextBtn.titleLabel.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:pNextBtn.titleLabel.font ,NSFontAttributeName, nil]];
//        pNextBtn.frame = CGRectMake( 320 - pNextBtnSize.width, 26, pNextBtnSize.width, 32);
//    }

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)goLifeStyleView{
    
    
   // [self.navigationController popToRootViewControllerAnimated:NO];
    
    [parentView enterLifeStyleView];
}
#pragma mark - Table view data source

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//
//    // Return the number of sections.
////    return 1;
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
        return 20;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *ReasonsCellIdentifier = @"ReasonsCellIdentifier";

    if (!nibsRegistered) {
        UINib *nib = [UINib nibWithNibName:@"ReasonsTableViewCell" bundle:nil];
        [tableView registerNib:nib forCellReuseIdentifier:ReasonsCellIdentifier];
        nibsRegistered = YES;
    }
    ReasonsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ReasonsCellIdentifier forIndexPath:indexPath];

    cell.imageReason.image = [UIImage imageNamed:[NSString stringWithFormat:@"choose_reason%ld.png",(indexPath.row+1)]];
    cell.lblReason.text = [NSString stringWithFormat:NSLocalizedString(@"Reason #%ld", @"ReasonsTableView"),(indexPath.row+1)];
    cell.lblReasonContent.text = [arrReasons objectAtIndex:indexPath.row];
    
    if (indexPath.row == 19) {
        cell.btnProceed.hidden = YES;
        [cell.btnProceed addTarget:self action:@selector(goLifeStyleView) forControlEvents:UIControlEventTouchUpInside];
    }else{
        cell.btnProceed.hidden = YES;
    }
    
    //cell顺时针旋转90度
    cell.contentView.transform = CGAffineTransformMakeRotation(M_PI/2);

    return cell;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat sectionHeaderHeight = 30;
    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
//        scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
//        scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    }
}
/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here, for example:
    // Create the next view controller.
  //  <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:<#@"Nib name"#> bundle:nil];
    
    // Pass the selected object to the new view controller.
    
    // Push the view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
}
*/

- (void)dealloc {
    [_tableviewReasons release];
    [pNextBtn release];
    [super dealloc];
}
- (IBAction)clickNext:(id)sender {
    [self goLifeStyleView];
}
@end

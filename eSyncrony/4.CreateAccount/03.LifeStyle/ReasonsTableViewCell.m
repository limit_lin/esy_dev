//
//  ReasonsTableViewCell.m
//  eSyncrony
//
//  Created by iosdeveloper on 14-5-30.
//  Copyright (c) 2014年 WonMH. All rights reserved.
//

#import "ReasonsTableViewCell.h"

@implementation ReasonsTableViewCell

- (void)awakeFromNib
{
    // Initialization code
    self.viewBackground.layer.cornerRadius = 10.0;
    self.viewBackground.layer.shadowOffset = CGSizeMake(3,3);
    self.viewBackground.layer.shadowColor = [UIColor blackColor].CGColor;
    self.viewBackground.layer.shadowOpacity = 0.6;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_imageReason release];
    [_viewBackground release];
    [_btnProceed release];
    [_lblReason release];
    [_lblReasonContent release];
    [super dealloc];
}

@end

//
//  ReasonsTableViewController.h
//  eSyncrony
//
//  Created by iosdeveloper on 14-5-30.
//  Copyright (c) 2014年 WonMH. All rights reserved.
//

#import <UIKit/UIKit.h>
@class eSyncronyViewController;
@interface ReasonsTableViewController : GAITrackedViewController<UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate>
{
    BOOL nibsRegistered;
    NSArray *arrReasons;
}
@property(nonatomic, assign) eSyncronyViewController* parentView;
@property (strong, nonatomic) IBOutlet UITableView *tableviewReasons;
- (IBAction)clickNext:(id)sender;
@end

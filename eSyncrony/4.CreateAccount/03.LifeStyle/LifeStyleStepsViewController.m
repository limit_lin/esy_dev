
#import "Global.h"
#import "UtilComm.h"
#import "LifeStyleStepsViewController.h"
#import "eSyncronyViewController.h"
#import "RegisterTableViewCell2.h"
#import "eSyncronyAppDelegate.h"
#import "DSActivityView.h"
#import "BasicStepViewsController.h"

@interface LifeStyleStepsViewController ()

@end

@implementation LifeStyleStepsViewController

@synthesize step;
@synthesize arrSelectedRowIdx;

@synthesize parentView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    self.screenName = @"LifeStyleStepsView";
    if( arrNameOfInfoList == nil )
    {
        arrNameOfInfoList = [[NSMutableArray alloc] init];
        [arrNameOfInfoList addObject:@"religion"];
        [arrNameOfInfoList addObject:@"profession"];
        [arrNameOfInfoList addObject:@"income"];
        [arrNameOfInfoList addObject:@"travel_exercise"];
        [arrNameOfInfoList addObject:@"travel_exercise"];
        [arrNameOfInfoList addObject:@"child"];
        [arrNameOfInfoList addObject:@"child_want"];
        [arrNameOfInfoList addObject:@"drinking_smoking"];
        [arrNameOfInfoList addObject:@"smoking_drinking"];
    }
    
    if ([[eSyncronyAppDelegate sharedInstance].language hasPrefix:@"id"]) {
        if( arrSecondLabTitle == nil )
        {
            arrSecondLabTitle = [[NSMutableArray alloc] init];
            [arrSecondLabTitle addObject:NSLocalizedString(@"My ",@"LifeStyleSteps")];
            [arrSecondLabTitle addObject:NSLocalizedString(@"My ",@"LifeStyleSteps")];
            [arrSecondLabTitle addObject:NSLocalizedString(@"My Income ",@"LifeStyleSteps")];
            [arrSecondLabTitle addObject:NSLocalizedString(@"travel overseas",@"LifeStyleSteps")];
            [arrSecondLabTitle addObject:NSLocalizedString(@"Exercise",@"LifeStyleSteps")];
            [arrSecondLabTitle addObject:NSLocalizedString(@"Children",@"LifeStyleSteps")];
            [arrSecondLabTitle addObject:NSLocalizedString(@"Children",@"LifeStyleSteps")];
            [arrSecondLabTitle addObject:@""];
            [arrSecondLabTitle addObject:@""];
        }
        if( arrFirstLabTitle == nil ){
            arrFirstLabTitle = [[NSMutableArray alloc] init];
            [arrFirstLabTitle addObject:NSLocalizedString(@"Religion",@"LifeStyleSteps")];
            [arrFirstLabTitle addObject:NSLocalizedString(@"Profession",@"LifeStyleSteps")];
            [arrFirstLabTitle addObject:NSLocalizedString(@"is",@"LifeStyleSteps")];
            [arrFirstLabTitle addObject:NSLocalizedString(@"I ",@"LifeStyleSteps")];
            [arrFirstLabTitle addObject:NSLocalizedString(@"I ",@"LifeStyleSteps")];
            [arrFirstLabTitle addObject:NSLocalizedString(@"I have ",@"LifeStyleSteps")];
            [arrFirstLabTitle addObject:NSLocalizedString(@"I want ",@"LifeStyleSteps")];
            [arrFirstLabTitle addObject:NSLocalizedString(@"I drink...",@"LifeStyleSteps")];
            [arrFirstLabTitle addObject:NSLocalizedString(@"I smoke...",@"LifeStyleSteps")];
            }
            
    }else{
    
        if( arrSecondLabTitle == nil )
        {
                arrSecondLabTitle = [[NSMutableArray alloc] init];
                [arrSecondLabTitle addObject:NSLocalizedString(@"Religion",@"LifeStyleSteps")];
                [arrSecondLabTitle addObject:NSLocalizedString(@"Profession",@"LifeStyleSteps")];
                [arrSecondLabTitle addObject:NSLocalizedString(@"My Income ",@"LifeStyleSteps")];
                [arrSecondLabTitle addObject:NSLocalizedString(@"travel overseas",@"LifeStyleSteps")];
                [arrSecondLabTitle addObject:NSLocalizedString(@"Exercise",@"LifeStyleSteps")];
                [arrSecondLabTitle addObject:NSLocalizedString(@"Children",@"LifeStyleSteps")];
                [arrSecondLabTitle addObject:NSLocalizedString(@"Children",@"LifeStyleSteps")];
                [arrSecondLabTitle addObject:@""];
                [arrSecondLabTitle addObject:@""];
        }
        if( arrFirstLabTitle == nil )
        {
                arrFirstLabTitle = [[NSMutableArray alloc] init];
                [arrFirstLabTitle addObject:NSLocalizedString(@"My ",@"LifeStyleSteps")];
                [arrFirstLabTitle addObject:NSLocalizedString(@"My ",@"LifeStyleSteps")];
                [arrFirstLabTitle addObject:NSLocalizedString(@"is",@"LifeStyleSteps")];
                [arrFirstLabTitle addObject:NSLocalizedString(@"I ",@"LifeStyleSteps")];
                [arrFirstLabTitle addObject:NSLocalizedString(@"I ",@"LifeStyleSteps")];
                [arrFirstLabTitle addObject:NSLocalizedString(@"I have ",@"LifeStyleSteps")];
                [arrFirstLabTitle addObject:NSLocalizedString(@"I want ",@"LifeStyleSteps")];
                [arrFirstLabTitle addObject:NSLocalizedString(@"I drink...",@"LifeStyleSteps")];
                [arrFirstLabTitle addObject:NSLocalizedString(@"I smoke...",@"LifeStyleSteps")];
        }
    }
    
    if( arrSecondLabTitle == nil )
    {
        if ([[eSyncronyAppDelegate sharedInstance].language hasPrefix:@"id"]) {
            
        }else{
            arrSecondLabTitle = [[NSMutableArray alloc] init];
            [arrSecondLabTitle addObject:NSLocalizedString(@"Religion",@"LifeStyleSteps")];
            [arrSecondLabTitle addObject:NSLocalizedString(@"Profession",@"LifeStyleSteps")];
            [arrSecondLabTitle addObject:NSLocalizedString(@"My Income ",@"LifeStyleSteps")];
            [arrSecondLabTitle addObject:NSLocalizedString(@"travel overseas",@"LifeStyleSteps")];
            [arrSecondLabTitle addObject:NSLocalizedString(@"Exercise",@"LifeStyleSteps")];
            [arrSecondLabTitle addObject:NSLocalizedString(@"Children",@"LifeStyleSteps")];
            [arrSecondLabTitle addObject:NSLocalizedString(@"Children",@"LifeStyleSteps")];
            [arrSecondLabTitle addObject:@""];
            [arrSecondLabTitle addObject:@""];
        }

    }

    if( self.step == 0 )
    {
        arrSelectedRowIdx = nil;
        arrSelectedRowIdx = [[NSMutableArray alloc] initWithCapacity:0];
        for( int i = 0; i < 9; i++ )
        {
            [arrSelectedRowIdx addObject:[NSIndexPath indexPathForItem:-1 inSection:0]];
        }
        
        [btnBack setHidden:YES];
    }
    else
        [btnBack setHidden:NO];
    
    [scrlView addSubview:subViewSteps];
    scrlView.contentSize = subViewSteps.frame.size;

    [self setContentsToStep];
}

- (IBAction)didClickBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)resizeTableView
{
    int maxCount = ([UIScreen mainScreen].bounds.size.height - 144)/42;
    
    if( [arrQuestion count] < maxCount )
    {
        [tableView setFrame:CGRectMake(tableView.frame.origin.x, tableView.frame.origin.y, tableView.frame.size.width, 42 * [arrQuestion count])];
    }
    else
    {
        [tableView setFrame:CGRectMake(tableView.frame.origin.x, tableView.frame.origin.y, tableView.frame.size.width, 42 * maxCount )];
    }
}

- (void)setContentsToStep
{
    NSString* strFirst = [arrFirstLabTitle objectAtIndex:step];
    NSString* strSecond = [arrSecondLabTitle objectAtIndex:step];
    
    float start_pos = 0.0f;
    
    labFirst.text = strFirst;
    labSecond.text = strSecond;
    
    [labFirst sizeToFit];
    [labSecond sizeToFit];
    
    float total_len = [labFirst bounds].size.width + [labSecond bounds].size.width;
    
    start_pos = (320 - total_len) / 2;
    if( step == 2 )
    {
        labSecond.frame = CGRectMake(start_pos, labSecond.frame.origin.y, labSecond.frame.size.width, labSecond.frame.size.height);
        labFirst.frame = CGRectMake(labSecond.frame.size.width + start_pos + 1, labFirst.frame.origin.y, labFirst.frame.size.width, labFirst.frame.size.height);
    }
    else
    {
        labFirst.frame = CGRectMake(start_pos, labFirst.frame.origin.y, labFirst.frame.size.width, labFirst.frame.size.height);
        labSecond.frame = CGRectMake(labFirst.frame.size.width + start_pos + 1, labFirst.frame.origin.y, labSecond.frame.size.width, labSecond.frame.size.height);
    }
    
    NSString*   strPlist = [arrNameOfInfoList objectAtIndex:step];
    if( step == 2 )
    {
        //0: Singapore, 1: Malaysia, 2: HONG KONG
        int nCtID = [eSyncronyAppDelegate sharedInstance].countryId;
        NSLog(@"lifeStyle == %d\n",nCtID);
        
        if (nCtID == 0)
            strPlist = [strPlist stringByAppendingString:@"_sg"];
        else if (nCtID == 1)
            strPlist = [strPlist stringByAppendingString:@"_my"];
        else if (nCtID == 2)
            strPlist = [strPlist stringByAppendingString:@"_hk"];
        else if (nCtID == 7)
            strPlist = [strPlist stringByAppendingString:@"_id"];
        else if (nCtID == 202)
            strPlist = [strPlist stringByAppendingString:@"_th"];
        else
            strPlist = [strPlist stringByAppendingString:@"_hk"];
        
    }
    arrQuestion = [[NSMutableArray alloc] initWithArray:[Global loadQuestionArray:strPlist]];

    [tableView reloadData];
    
    tableView.contentOffset = CGPointZero;
    
    {
        NSIndexPath* indPath = [arrSelectedRowIdx objectAtIndex:step];
        if ( indPath.row >= 0 )
            [tableView selectRowAtIndexPath:indPath animated:NO scrollPosition:UITableViewScrollPositionMiddle];
    }
    
    [self resizeTableView];
}

- (void)saveLifeStyleProc
{
    int     nSelIndex[9];
    
    for( int i = 0; i < 9; i++ )
    {
        NSIndexPath* indPath = nil;
        
        indPath =  [arrSelectedRowIdx objectAtIndex:i];
        
        nSelIndex[i] = (int)indPath.row;
    }
    
    //------------------ Get Profession Match Index ------------------//

    NSArray*        arrayProfession = [Global loadQuestionArray:@"profession"];
    NSArray*        arrayProfessionM = [Global loadQuestionArray:@"profession1"];
    NSString*       strProf = [arrayProfession objectAtIndex:nSelIndex[1]];

    for( int k = 0; k < [arrayProfessionM count]; k++ )
    {
        NSString*   strItem = [arrayProfessionM objectAtIndex:k];
        if( [strItem isEqualToString:strProf] )
        {
            if( k >= 25 )
                nSelIndex[1] = k+1;
            else
                nSelIndex[1] = k;

            break;
        }
    }

    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    [params setObject:[NSNumber numberWithInt:nSelIndex[0]] forKey:@"religion"];
    [params setObject:[NSNumber numberWithInt:nSelIndex[1]] forKey:@"jobtitle"];
    [params setObject:[NSNumber numberWithInt:nSelIndex[2]] forKey:@"income"];
    [params setObject:[NSNumber numberWithInt:nSelIndex[3]] forKey:@"travel"];
    [params setObject:[NSNumber numberWithInt:nSelIndex[4]] forKey:@"exercise"];
    [params setObject:[NSNumber numberWithInt:nSelIndex[5]] forKey:@"children_num"];
    [params setObject:[NSNumber numberWithInt:nSelIndex[6]] forKey:@"want_children"];
    [params setObject:[NSNumber numberWithInt:nSelIndex[7]] forKey:@"drinking"];
    [params setObject:[NSNumber numberWithInt:nSelIndex[8]] forKey:@"smoking"];
    
    [self moveNextWindow:params];
}

-(void)moveNextWindow:(NSMutableDictionary *)values
{
    //[self.navigationController popToRootViewControllerAnimated:NO];
   // [parentView enterDisplayComplete:1];
    
//    [DSBezelActivityView removeViewAnimated:NO];
    [parentView enterBasicStepWithLifeStyle:1 Values:values];
}

- (void)saveLifeStyleAndMoveNext
{
//    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Saving...",)];
    [self performSelector:@selector(saveLifeStyleProc) withObject:nil afterDelay:0.01];
}

- (IBAction)didClickNext:(id)sender
{
    id arrTmp = [arrSelectedRowIdx objectAtIndex:step];
    
    if( arrTmp==nil || [(NSIndexPath*)arrTmp row] < 0)
    {
        NSString *strMessage = NSLocalizedString(@"Please select your choice",);
        NSString *strName = [arrNameOfInfoList objectAtIndex:step];
        if ([strName isEqualToString:@"religion"]||[strName isEqualToString:@"profession"]||[strName isEqualToString:@"income"]) {
            strMessage = [NSString stringWithFormat: NSLocalizedString(@"Please select your %@.",),strName];//need to be translated
        }
        UIAlertView *av = [[UIAlertView alloc] initWithTitle: NSLocalizedString(@"Field Required",) message:strMessage delegate:nil cancelButtonTitle: NSLocalizedString(@"OK",) otherButtonTitles:nil];
        [av show];
        [av release];
        
        return;
    }

    if( step == 8 )
    {
        [self saveLifeStyleAndMoveNext];
    }
    else
    {
        if( self.nextView == nil )
            self.nextView = [[LifeStyleStepsViewController alloc] initWithNibName:@"LifeStyleStepsViewController" bundle:nil];
        
        self.nextView.parentView = self.parentView;
        self.nextView.step = self.step+1;

        self.nextView.arrSelectedRowIdx = self.arrSelectedRowIdx;

        [self.navigationController pushViewController:self.nextView animated:YES];
    }
}

-(void)lifeStyleSteps_actionNext
{
    
    //append
    id arrTmp = [arrSelectedRowIdx objectAtIndex:step];
    
    if( arrTmp==nil || [(NSIndexPath*)arrTmp row] < 0)
    {
        NSString *strMessage = NSLocalizedString(@"Please select your choice",);
        NSString *strName = [arrNameOfInfoList objectAtIndex:step];
        if ([strName isEqualToString:@"religion"]||[strName isEqualToString:@"profession"]||[strName isEqualToString:@"income"]) {
            strMessage = [NSString stringWithFormat: NSLocalizedString(@"Please select your %@.",),strName];//need to be translated
        }
        UIAlertView *av = [[UIAlertView alloc] initWithTitle: NSLocalizedString(@"Field Required",) message:strMessage delegate:nil cancelButtonTitle: NSLocalizedString(@"OK",) otherButtonTitles:nil];
        [av show];
        [av release];
        
        return;
    }
    
    if( step == 8 )
    {
        [self saveLifeStyleAndMoveNext];
    }
    else
    {
        if( self.nextView == nil )
            self.nextView = [[LifeStyleStepsViewController alloc] initWithNibName:@"LifeStyleStepsViewController" bundle:nil];
        
        self.nextView.parentView = self.parentView;
        self.nextView.step = self.step+1;
        
        self.nextView.arrSelectedRowIdx = self.arrSelectedRowIdx;
        
        [self.navigationController pushViewController:self.nextView animated:YES];
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [arrQuestion count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 42;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RegisterTableViewCell2 *cell;
    NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"RegisterTableViewCell2" owner:nil options:nil];
    cell = [arr objectAtIndex:0];
    NSInteger row = [indexPath row];
    //NSDictionary *_storeObj =[arrFriendList objectAtIndex:row];
    NSString * str =  [arrQuestion objectAtIndex:row];
    cell.lbContent.text = str;
    
    return cell;
}

- (void)tableView:(UITableView *)tableV didSelectRowAtIndexPath:(NSIndexPath *)newIndexPath
{
    int row = (int)[newIndexPath row];
    
    {
        if( [arrSelectedRowIdx objectAtIndex:step] != nil )
        {
            NSIndexPath* ip = [arrSelectedRowIdx objectAtIndex:step];
            [tableV deselectRowAtIndexPath:ip animated:NO];
            
            if ( row == ip.row )
            {
                [arrSelectedRowIdx replaceObjectAtIndex:step  withObject:[NSIndexPath indexPathForItem:-1 inSection:0]];
                return;
            }
            else
                [arrSelectedRowIdx replaceObjectAtIndex:step  withObject:newIndexPath];
        }
        else
        {
            [arrSelectedRowIdx replaceObjectAtIndex:step  withObject:newIndexPath];
        }
        [tableV selectRowAtIndexPath:newIndexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
    }
    
    [self lifeStyleSteps_actionNext];

}

-(void)dealloc
{
    [arrNameOfInfoList release];
    [arrQuestion release];
    
    [scrlView release];
    [subViewSteps release];
    [subViewTitle release];
    [tableView release];
    
    [btnBack release];
    [btnNext release];
    
    if( self.step == 0 )
        [self.arrSelectedRowIdx release];
    
    [self.nextView release];

    [super dealloc];
}

@end


#import <UIKit/UIKit.h>

@class eSyncronyViewController;

@interface LifeStyleStepsViewController : GAITrackedViewController<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UIScrollView*      scrlView;
    IBOutlet UIView*            subViewSteps;
    
    IBOutlet UIView*            subViewTitle;
    IBOutlet UITableView*       tableView;
    
    IBOutlet UIButton*          btnBack;
    IBOutlet UIButton*          btnNext;

    NSArray*                    arrQuestion;
    NSMutableArray*             arrNameOfInfoList;
    
    IBOutlet UILabel*           labFirst;
    IBOutlet UILabel*           labSecond;
    NSMutableArray*             arrFirstLabTitle;
    NSMutableArray*             arrSecondLabTitle;
}

@property(nonatomic, assign)    int     step;
@property(nonatomic, assign)    NSMutableArray*     arrSelectedRowIdx;

@property(nonatomic, assign)    LifeStyleStepsViewController    *nextView;

@property(nonatomic, assign)    eSyncronyViewController* parentView;

- (IBAction)didClickBack:(id)sender;
- (IBAction)didClickNext:(id)sender;

- (void)resizeTableView;

@end


#import "Global.h"
#import "DisplayCompleteProgrsesViewController.h"
#import "eSyncronyViewController.h"
#import "eSyncronyAppDelegate.h"
@interface DisplayCompleteProgrsesViewController ()

@end

@implementation DisplayCompleteProgrsesViewController

@synthesize step,parentView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"DisplayCompleteProgressView";
    [srcView addSubview:viewCont];
    srcView.contentSize = viewCont.frame.size;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];//151125
    [self InitVariables];
    [self InitUIContent];

    srcView.contentSize = viewCont.frame.size;
    [srcView setContentOffset:CGPointMake(0, 0) animated:NO];
}

- (IBAction)didClickProceed:(id)sender
{
#if 0
    self.step ++;
    [self InitVariables];
    [self InitUIContent];
    
    srcView.contentSize = viewCont.frame.size;
    [srcView setContentOffset:CGPointMake(0, 0) animated:NO];
    return;
#endif
    
   //[self.navigationController popViewControllerAnimated:NO];
    
    switch( step )
    {
        case 1:
            [parentView enterCriteriaView];
            break;
        case 2:
            [parentView enterAppearanceView];
            break;
        case 3:
            [parentView enterMyValueView];
            break;
        case 4:
            [parentView enterRelationStepsView];
            break;
        case 5:
            [parentView enterCultureStepsView];
            break;
        case 6:
            [parentView enterOpennessStepsView];
            break;
        case 7:
            [parentView enterMoneyStepsView];
            break;
        case 8:
            [parentView enterAdmirationStepsView];
            break;
        case 9:
            [parentView enterPersonalityStepsView];
            break;
        case 10:
//            [parentView enterMainWindow];
            [parentView enterDocCertStep];//151027
            //[parentView enterVerifyStep];
            break;
        default:
            break;
    }
        
}

- (void)InitUIContent
{
    //float total_len = imgPrgFill.bounds.size.width + imgPrgSliderBg.bounds.size.width;
    float total_len = imgPrgSliderBg.frame.origin.x + imgPrgSliderBg.frame.size.width - imgPrgFill.frame.origin.x - imgPrgThumb.bounds.size.width/2;
    float total_move_len = total_len - imgPrgThumb.bounds.size.width/2;

    float start_x = imgPrgFill.frame.origin.x + imgPrgThumb.bounds.size.width/2;
    float xVar = 0, wid;

    //[imgPrgFill setFrame:CGRectMake(start_x, imgPrgFill.frame.origin.y, total_len/10.0f*step, imgPrgFill.frame.size.height) ];
    
    //imgPrgFill.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    //imgPrgFill.autoresizesSubviews = YES;

    //xVar = total_len/10.0f*step + imgPrgThumb.bounds.size.width/2;
    xVar = total_move_len/10.0f*step + imgPrgThumb.bounds.size.width/2;
    wid = MAX( 0, total_len - xVar );
    [imgPrgSliderBg setFrame:CGRectMake(start_x + xVar, imgPrgSliderBg.frame.origin.y, wid, imgPrgSliderBg.frame.size.height)];

    xVar = total_move_len/10.0f*step;
    [imgPrgThumb setFrame:CGRectMake(start_x + xVar, imgPrgThumb.frame.origin.y, imgPrgThumb.frame.size.width, imgPrgThumb.frame.size.height)];
    
    //[imgPrgThumb setFrame:CGRectMake(start_x + total_len/10.0f*step-imgPrgThumb.frame.size.width, imgPrgThumb.frame.origin.y, imgPrgThumb.frame.size.width, imgPrgThumb.frame.size.height)];

    switch (step) {
        case 1:
            self.lblTitle.text = NSLocalizedString(@"My Criteria",);
            imgPrgStarFill1.image = [UIImage imageNamed:@"heart3.png"];
            imgPrgStarFill2.image = [UIImage imageNamed:@"heart2.png"];
            imgPrgStarFill3.image = [UIImage imageNamed:@"heart2.png"];
            imgPrgStarFill4.image = [UIImage imageNamed:@"heart2.png"];
            imgPrgStarFill5.image = [UIImage imageNamed:@"heart2.png"];
            break;
        case 2:
            self.lblTitle.text = NSLocalizedString(@"Our Success Stories",);
            imgPrgStarFill1.image = [UIImage imageNamed:@"heart3.png"];
            imgPrgStarFill2.image = [UIImage imageNamed:@"heart2.png"];
            imgPrgStarFill3.image = [UIImage imageNamed:@"heart2.png"];
            imgPrgStarFill4.image = [UIImage imageNamed:@"heart2.png"];
            imgPrgStarFill5.image = [UIImage imageNamed:@"heart2.png"];
            break;
        case 3:
            self.lblTitle.text = NSLocalizedString(@"Our Success Stories",);
            imgPrgStarFill1.image = [UIImage imageNamed:@"heart1.png"];
            imgPrgStarFill2.image = [UIImage imageNamed:@"heart2.png"];
            imgPrgStarFill3.image = [UIImage imageNamed:@"heart2.png"];
            imgPrgStarFill4.image = [UIImage imageNamed:@"heart2.png"];
            imgPrgStarFill5.image = [UIImage imageNamed:@"heart2.png"];
            break;
        case 4:
            self.lblTitle.text = NSLocalizedString(@"Our Success Stories",);
            imgPrgStarFill1.image = [UIImage imageNamed:@"heart1.png"];
            imgPrgStarFill2.image = [UIImage imageNamed:@"heart3.png"];
            imgPrgStarFill3.image = [UIImage imageNamed:@"heart2.png"];
            imgPrgStarFill4.image = [UIImage imageNamed:@"heart2.png"];
            imgPrgStarFill5.image = [UIImage imageNamed:@"heart2.png"];
            break;
        case 5:
            self.lblTitle.text = NSLocalizedString(@"Our Success Stories",);
            imgPrgStarFill1.image = [UIImage imageNamed:@"heart1.png"];
            imgPrgStarFill2.image = [UIImage imageNamed:@"heart1.png"];
            imgPrgStarFill3.image = [UIImage imageNamed:@"heart2.png"];
            imgPrgStarFill4.image = [UIImage imageNamed:@"heart2.png"];
            imgPrgStarFill5.image = [UIImage imageNamed:@"heart2.png"];
            break;
        case 6:
            self.lblTitle.text = NSLocalizedString(@"Our Success Stories",);
            imgPrgStarFill1.image = [UIImage imageNamed:@"heart1.png"];
            imgPrgStarFill2.image = [UIImage imageNamed:@"heart1.png"];
            imgPrgStarFill3.image = [UIImage imageNamed:@"heart3.png"];
            imgPrgStarFill4.image = [UIImage imageNamed:@"heart2.png"];
            imgPrgStarFill5.image = [UIImage imageNamed:@"heart2.png"];
            break;
        case 7:
            self.lblTitle.text = NSLocalizedString(@"Our Success Stories",);
            imgPrgStarFill1.image = [UIImage imageNamed:@"heart1.png"];
            imgPrgStarFill2.image = [UIImage imageNamed:@"heart1.png"];
            imgPrgStarFill3.image = [UIImage imageNamed:@"heart1.png"];
            imgPrgStarFill4.image = [UIImage imageNamed:@"heart2.png"];
            imgPrgStarFill5.image = [UIImage imageNamed:@"heart2.png"];
            break;
        case 8:
            self.lblTitle.text = NSLocalizedString(@"Our Success Stories",);
            imgPrgStarFill1.image = [UIImage imageNamed:@"heart1.png"];
            imgPrgStarFill2.image = [UIImage imageNamed:@"heart1.png"];
            imgPrgStarFill3.image = [UIImage imageNamed:@"heart1.png"];
            imgPrgStarFill4.image = [UIImage imageNamed:@"heart3.png"];
            imgPrgStarFill5.image = [UIImage imageNamed:@"heart2.png"];
            break;
        case 9:
            self.lblTitle.text = NSLocalizedString(@"My Personality",);
            imgPrgStarFill1.image = [UIImage imageNamed:@"heart1.png"];
            imgPrgStarFill2.image = [UIImage imageNamed:@"heart1.png"];
            imgPrgStarFill3.image = [UIImage imageNamed:@"heart1.png"];
            imgPrgStarFill4.image = [UIImage imageNamed:@"heart1.png"];
            imgPrgStarFill5.image = [UIImage imageNamed:@"heart2.png"];
            break;
        case 10:
            self.lblTitle.text = NSLocalizedString(@"Our Success Stories",);
            imgPrgStarFill1.image = [UIImage imageNamed:@"heart1.png"];
            imgPrgStarFill2.image = [UIImage imageNamed:@"heart1.png"];
            imgPrgStarFill3.image = [UIImage imageNamed:@"heart1.png"];
            imgPrgStarFill4.image = [UIImage imageNamed:@"heart1.png"];
            imgPrgStarFill5.image = [UIImage imageNamed:@"heart1.png"];
            break;
            
        default:
            break;
    }

    if( step == 9 )
    {
        strChpaterImg = [NSString stringWithFormat:@"chapter_%d.png", step * 10 + 5];
        lbPercent.text = [NSString stringWithFormat:@"%d%%", 80];
    }
    else if( step == 10 )
    {
        strChpaterImg = [NSString stringWithFormat:@"chapter_%d.png", step * 10];
        lbPercent.text = [NSString stringWithFormat:@"%d%%", 99];
    }
    else if( step == 1 )
    {
        strChpaterImg = [NSString stringWithFormat:@"chapter_%d.png", step * 10];
        lbPercent.text = [NSString stringWithFormat:@"%d%%", 10];
    }
    else if( step == 2 )
    {
        strChpaterImg = [NSString stringWithFormat:@"chapter_%d.png", step * 10];
        lbPercent.text = [NSString stringWithFormat:@"%d%%", 15];
    }
    else
    {
        strChpaterImg = [NSString stringWithFormat:@"chapter_%d.png", step * 10];
        lbPercent.text = [NSString stringWithFormat:@"%d%%",(step-1)*10];
        
    }
    
    [lbPercent sizeToFit];
    CGRect rect = lbPercent.frame;
    rect.size.height = 21;
    lbPercent.frame = rect;
    
    CGRect rect1 = lpPercentComeplete.frame;
    rect1.origin.x = rect.origin.x + rect.size.width + 5;
    lpPercentComeplete.frame = rect1;

    imgSuccessStory.image = [UIImage imageNamed:strChpaterImg];
    
    txtStory.text = strCompleteStory;
    txtStory1.text = strCompleteStory1;

    rect = txtStory.frame;
    CGSize size = CGSizeMake(280.f, MAXFLOAT);
    CGSize actualsize = [strCompleteStory sizeWithFont:txtStory.font constrainedToSize:size lineBreakMode:NSLineBreakByTruncatingTail];
    
    rect.size = actualsize;
    txtStory.frame = rect;
    
    rect.origin.y = rect.origin.y+rect.size.height+15;
    actualsize = [strCompleteStory1 sizeWithFont:txtStory1.font constrainedToSize:size lineBreakMode:NSLineBreakByTruncatingTail];
    rect.size = actualsize;
    txtStory1.frame = rect;
    
    //-----------------------//
    rect = btnProcess.frame;
    rect.origin.y = txtStory1.frame.origin.y + txtStory1.frame.size.height;
    
    btnProcess.frame = rect;

    if( viewCont.frame.size.height < rect.origin.y + rect.size.height + 20 )
    {
        CGRect  frame;
        
        frame.origin = CGPointZero;
        frame.size.width = viewCont.frame.size.width;
        frame.size.height = rect.origin.y + rect.size.height + 20;
        
        viewCont.frame = frame;
    }
}

- (void)InitVariables
{
    NSArray*    arryMainContent = [Global loadQuestionArray:@"completion_text_list"];
    NSArray*    arryMainContent1 = [Global loadQuestionArray:@"completion_text_list1"];
    if (step==1) {
        
        if ([eSyncronyAppDelegate sharedInstance].gender==0) {
            strCompleteStory = NSLocalizedString(@"\"Tell us who are you looking for in a relationship.\n\nRemember to keep an open mind in order not to miss the chance to know more potential ladies.\"",@"CompleteStory1");
        }else{
            strCompleteStory = NSLocalizedString(@"\"Tell us who are you looking for in a relationship.\n\nRemember to keep an open mind in order not to miss the chance to know more potential guys.\"",@"CompleteStory1");
        }
        strCompleteStory1 = @"";
        return;
    }
//    else if(step ==3) {
//        strCompleteStory = @"\"We understand this is a long process, but we seek your understanding and cooperation as it is our mission to find compatible matches for you.\n\nIf you are unable to finish the quiz now, you can come back later. Your answers will be automatically saved so that you can resume later.\"";
//        strCompleteStory1 = @"Violet Lim\nCEO of Lunch Actually Group";
//        return;
//    }else if(step ==4) {
//        strCompleteStory = @"\"Thanks to eSynchrony, we have found each other!\n\nUpon our first meeting, we immediately clicked and our lunch dates extended to many more dates. We finally tied the kont last 9th September 2013.\n\nTo all who are still searching for your other half, do not give up!\"";
//        strCompleteStory1 = @"Roger & Sun May";
//        return;
//    }else if(step ==9) {
//        strCompleteStory = @"\"You are almost done! A lot of people do not make it this far. You are already a step ahead!\n\nNext, tell us more about your personality.\"";
//        strCompleteStory1 = [arryMainContent1 objectAtIndex:step-1];
//        return;
//    }
    
    
    strCompleteStory = [arryMainContent objectAtIndex:step-1];
    
    
    strCompleteStory1 = [arryMainContent1 objectAtIndex:step-1];
}

- (void)dealloc
{
    [srcView release];
    [viewCont release];
    
    [imgPrgFill release];
    [imgPrgSliderBg release];
    [imgPrgThumb release];
    
    [imgPrgStarFill1 release];
    [imgPrgStarFill2 release];
    [imgPrgStarFill3 release];
    [imgPrgStarFill4 release];
    [imgPrgStarFill5 release];
    
    [imgSuccessStory release];
    
    [lbPercent release];
    [lpPercentComeplete release];
    
    [txtStory release];
    [txtStory1 release];
    
    [btnProcess release];
    
    [_lblStep1 release];
    [_lblTitle release];
    [super dealloc];
}

@end

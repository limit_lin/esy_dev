
#import <UIKit/UIKit.h>
@class eSyncronyViewController;

@interface DisplayCompleteProgrsesViewController : GAITrackedViewController
{
    // Interface objects
    IBOutlet UIScrollView*      srcView;
    IBOutlet UIView*            viewCont;
    
    IBOutlet UIImageView*       imgPrgFill;
    IBOutlet UIImageView*       imgPrgSliderBg;
    IBOutlet UIImageView*       imgPrgThumb;
    
    IBOutlet UIImageView*       imgPrgStarFill1;
    IBOutlet UIImageView*       imgPrgStarFill2;
    IBOutlet UIImageView*       imgPrgStarFill3;
    IBOutlet UIImageView*       imgPrgStarFill4;
    IBOutlet UIImageView*       imgPrgStarFill5;

    IBOutlet UIImageView*       imgSuccessStory;
    
    IBOutlet UILabel*           lbPercent;
    IBOutlet UILabel*           lpPercentComeplete;
    
    IBOutlet UILabel*        txtStory;
    IBOutlet UILabel*        txtStory1;
    
    IBOutlet UIButton*          btnProcess;

    //local variables
    NSString*                   strChpaterImg;
    NSString*                   strCompleteStory;
    NSString*                   strCompleteStory1;
}

@property(nonatomic,readwrite)int step;
@property(nonatomic, strong)eSyncronyViewController* parentView;
@property (strong, nonatomic) IBOutlet UILabel *lblStep1;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;

- (void)InitUIContent;
- (void)InitVariables;

- (IBAction)didClickProceed:(id)sender;

@end


#import <UIKit/UIKit.h>

@class  eSyncronyViewController;

@interface MyValueStepsViewController : GAITrackedViewController<UITableViewDataSource,UITableViewDelegate>
{
    //main Wnd Views;
    IBOutlet UIScrollView*      scrView;
    IBOutlet UIView*            subview;
    IBOutlet UIButton*          btnBack;
    
    //sub views
    IBOutlet UITableView*       tableView;
    IBOutlet UILabel*           lblTitle;
    
    //local variables
    NSArray*                    arrQuestion;
    NSMutableArray*             arrNameOfInfoList;

    int     nT, nI, nS, nC;
}

@property (nonatomic, assign) int step;
@property (nonatomic, assign) NSMutableArray*             arrSelOrders;
@property (nonatomic, assign) MyValueStepsViewController* nextView;
@property (nonatomic, assign) eSyncronyViewController* parentView;

- (IBAction)didClickBack:(id)sender;
- (IBAction)didClickNext:(id)sender;

- (void)setContentsToStep;
- (void)resizeTableView;

@end

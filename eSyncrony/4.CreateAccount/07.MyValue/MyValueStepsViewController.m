
#import "Global.h"
#import "UtilComm.h"
#import "DSActivityView.h"
#import "eSyncronyAppDelegate.h"
#import "MyValueStepsViewController.h"
#import "RegisterMyValueTableViewCell.h"
#import "eSyncronyViewController.h"

#define     MOST_LIKE_VALUE         2
#define     LEAST_LIKE_VALUE        1

@interface MyValueStepsViewController ()

@end

int     _myvalue_pOrderValue[10][4];

@implementation MyValueStepsViewController

@synthesize parentView;
@synthesize step;

@synthesize arrSelOrders;

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    self.screenName = @"MyValueStepsView";
//    [tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];//UITableViewStylePlain UITableView 如何不显示分割线
    if( arrNameOfInfoList == nil )
    {
        arrNameOfInfoList = [[NSMutableArray alloc] init];
        [arrNameOfInfoList addObject:[NSString stringWithFormat:@"myvalue_choice_list_q1"]];
        [arrNameOfInfoList addObject:[NSString stringWithFormat:@"myvalue_choice_list_q2"]];
        [arrNameOfInfoList addObject:[NSString stringWithFormat:@"myvalue_choice_list_q3"]];
        [arrNameOfInfoList addObject:[NSString stringWithFormat:@"myvalue_choice_list_q4"]];
        [arrNameOfInfoList addObject:[NSString stringWithFormat:@"myvalue_choice_list_q5"]];
        [arrNameOfInfoList addObject:[NSString stringWithFormat:@"myvalue_choice_list_q6"]];
        [arrNameOfInfoList addObject:[NSString stringWithFormat:@"myvalue_choice_list_q7"]];
        [arrNameOfInfoList addObject:[NSString stringWithFormat:@"myvalue_choice_list_q8"]];
        [arrNameOfInfoList addObject:[NSString stringWithFormat:@"myvalue_choice_list_q9"]];
        [arrNameOfInfoList addObject:[NSString stringWithFormat:@"myvalue_choice_list_q10"]];
    }
    
    if( self.step == 1 )
    {
        [btnBack setHidden:YES];
        [self InitVariables];
    }
    else
        [btnBack setHidden:NO];

    [scrView addSubview:subview];

    [self initTitle];
    [self setContentsToStep];
}

- (void)initTitle
{
    NSString *title = NSLocalizedString(@"Please rank the words below that fits you the most to the least",@"MyValueSteps");
    
    NSMutableAttributedString* string = [[NSMutableAttributedString alloc]initWithString:title];
    
    NSRange range;
    
    range = NSMakeRange([title length]-17, 17);
    
    UIColor*    color1 = TITLE_TEXT_COLLOR;
    
    [string addAttribute:NSForegroundColorAttributeName value:color1 range:range];
    [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeueLTStd-BdCn" size:15] range:range];
    
    //txtQuestionType2.text = 0;
    [lblTitle setAttributedText:string];
    return;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    subview.frame = scrView.bounds;
    scrView.contentSize = subview.frame.size;

    [self resizeTableView];
}

- (void)InitVariables
{
    arrSelOrders = [[NSMutableArray alloc] initWithCapacity:0];
}

- (void)resizeTableView
{
    int maxCount = ([UIScreen mainScreen].bounds.size.height - 144)/42;
    
    if( [arrQuestion count] < maxCount )
    {
        [tableView setFrame:CGRectMake(tableView.frame.origin.x, tableView.frame.origin.y, tableView.frame.size.width, 42 * [arrQuestion count])];
    }
    else
    {
        [tableView setFrame:CGRectMake(tableView.frame.origin.x, tableView.frame.origin.y, tableView.frame.size.width, 42 * maxCount )];
    }
}

- (void)setContentsToStep
{
    NSString* strPlist = [arrNameOfInfoList objectAtIndex:step - 1];
    arrQuestion = [[NSMutableArray alloc] initWithArray:[Global loadQuestionArray:strPlist]];
    
    if( [arrSelOrders count] < step )
    {
        NSMutableArray* selOrders = [[NSMutableArray alloc] initWithCapacity:0];

        for( int k = 0; k < [arrQuestion count]; k++ )
            [selOrders addObject:[NSNumber numberWithInt:-1]];
        
        [arrSelOrders addObject:selOrders];
    }

    [tableView reloadData];
    [self resizeTableView];
    
    [self refreshCellItemsOrderValue:YES];
}

//=====================================================================================//

- (void)calcTICS
{
    // T, I, S, C
    int     pTblTISC[10][4] = { {0, 1, 3, 2}, {2, 3, 1, 0}, {1, 2, 0, 3},
                                {3, 1, 0, 2}, {1, 0, 3, 2}, {3, 0, 2, 1},
                                {0, 1, 3, 2}, {2, 1, 3, 0}, {0, 3, 1, 2}, {3, 2, 0, 1} };
    
    
    nT = nI = nS = nC = 0;

    for( int i = 0; i < 10; i++ )
    {
        NSMutableArray*    selOrders = [arrSelOrders objectAtIndex:i];
        
        for( int k = 0; k < 4; k++ )
        {
            int nOrder = [[selOrders objectAtIndex:k] intValue];
            
            if( nOrder == 0 )
            {
                _myvalue_pOrderValue[i][k] = MOST_LIKE_VALUE;
                
                if( k == pTblTISC[i][0] )
                    nT++;
                else if( k == pTblTISC[i][1] )
                    nI++;
                else if( k == pTblTISC[i][2] )
                    nS++;
                else if( k == pTblTISC[i][3] )
                    nC++;
            }
            else if( nOrder == 3 )
            {
                _myvalue_pOrderValue[i][k] = LEAST_LIKE_VALUE;
                
                if( k == pTblTISC[i][0] )
                    nT--;
                else if( k == pTblTISC[i][1] )
                    nI--;
                else if( k == pTblTISC[i][2] )
                    nS--;
                else if( k == pTblTISC[i][3] )
                    nC--;
            }
            else
                _myvalue_pOrderValue[i][k] = 0;
        }
    }
}

- (void)saveMyValueProc
{
//    NSString*   accNo = [eSyncronyAppDelegate sharedInstance].strAccNo;
//    NSString*   tokenKey = [eSyncronyAppDelegate sharedInstance].strTokenKey;

    [self calcTICS];
    //--------------------------------------------------------------//
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    
    for( int i = 0; i < 10; i++ )
    {
        NSString*   qValue = [NSString stringWithFormat:@"%d,%d,%d,%d", _myvalue_pOrderValue[i][0], _myvalue_pOrderValue[i][1], _myvalue_pOrderValue[i][2], _myvalue_pOrderValue[i][3]];
        
        NSString*   keyValue = [NSString stringWithFormat:@"qValues%d", i+1];
        
        [params setObject:qValue forKey:keyValue];
    }
    
    [params setObject:[NSNumber numberWithInt:nT] forKey:@"total_T"];
    [params setObject:[NSNumber numberWithInt:nI] forKey:@"total_I"];
    [params setObject:[NSNumber numberWithInt:nS] forKey:@"total_S"];
    [params setObject:[NSNumber numberWithInt:nC] forKey:@"total_C"];

    NSDictionary*    result = [UtilComm saveMyValue:params];
    [DSBezelActivityView removeViewAnimated:NO];

    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Saving Failure"];
        return;
    }

    id response = [result objectForKey:@"response"];
    if( response == nil )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Saving Failure",)];
        return;
    }
    
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        if( [response isEqualToString:@"ERROR 99"] )
            [self moveNextWindow];
        else
            [ErrorProc alertSetTICSError:response];
    }
    else
    {
        [self moveNextWindow];
       
    }
}

-(void)moveNextWindow
{
    //[self.navigationController popToRootViewControllerAnimated:NO];
//    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Saving...",)];
    [parentView enterDisplayComplete:4];
}

- (void)saveMyValueAndMoveNext
{
    //[self moveNextWindow];
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Saving...",)];
    [self performSelector:@selector(saveMyValueProc) withObject:nil afterDelay:0.01];
}

//=========================================================================================//

- (IBAction)didClickNext:(id)sender
{
    NSArray*    selIndexes = [tableView indexPathsForSelectedRows];
    NSInteger   nSelCount = 0;
    
    if( selIndexes != nil )
        nSelCount = [selIndexes count];

    if( nSelCount < [arrQuestion count] )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Please select all items",@"MyValueSteps") withTitle:NSLocalizedString(@"Field Required",)];
        return;
    }

    if( step == 10 )
    {
        [self saveMyValueAndMoveNext];
        return;
    }
    
    if( self.nextView == nil )
        self.nextView = [[MyValueStepsViewController alloc] initWithNibName:@"MyValueStepsViewController" bundle:nil];
   
    self.nextView.parentView = self.parentView;
    self.nextView.step = self.step+1;
    self.nextView.arrSelOrders = self.arrSelOrders;
    
    [self.navigationController pushViewController:self.nextView animated:YES];
}

-(void)valueStps_actionNext
{
    
    NSArray*    selIndexes = [tableView indexPathsForSelectedRows];
    NSInteger   nSelCount = 0;
    
    if( selIndexes != nil )
        nSelCount = [selIndexes count];
    
    if( nSelCount < [arrQuestion count] )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Please select all items",@"MyValueSteps") withTitle:NSLocalizedString(@"Field Required",)];
        return;
    }
    
    if( step == 10 )
    {
        [self saveMyValueAndMoveNext];
        return;
    }
    
    if( self.nextView == nil )
        self.nextView = [[MyValueStepsViewController alloc] initWithNibName:@"MyValueStepsViewController" bundle:nil];
    
    self.nextView.parentView = self.parentView;
    self.nextView.step = self.step+1;
    self.nextView.arrSelOrders = self.arrSelOrders;
    
    [self.navigationController pushViewController:self.nextView animated:YES];
    
}


- (IBAction) didClickBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [arrQuestion count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 42;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RegisterMyValueTableViewCell *cell;
    
    NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"RegisterMyValueTableViewCell" owner:nil options:nil];
    cell = [arr objectAtIndex:0];

    NSInteger row = [indexPath row];
    NSString* str =  [arrQuestion objectAtIndex:row];
    
    cell.lbContent.text = str;
    cell.lbNumber.text = @"";
    
    return cell;
}

- (void)refreshCellItemsOrderValue:(BOOL)bSel
{
    RegisterMyValueTableViewCell *cell;

    NSMutableArray*    selOrders = [arrSelOrders objectAtIndex:step-1];
    
    for( int k = 0; k < [arrQuestion count]; k++ )
    {
        int order = [[selOrders objectAtIndex:k] intValue];
        
        NSIndexPath* indPath = [NSIndexPath indexPathForItem:k inSection:0];
        if( bSel == TRUE && order >= 0 )
            [tableView selectRowAtIndexPath:indPath animated:NO scrollPosition:UITableViewScrollPositionNone];

        cell = (RegisterMyValueTableViewCell*)[tableView cellForRowAtIndexPath:indPath];
        [cell setNumber:order];
    }
}

- (void)tableView:(UITableView *)tV didSelectRowAtIndexPath:(NSIndexPath *)newIndexPath
{
    NSArray*    selIndexs = tV.indexPathsForSelectedRows;
    NSInteger   nSel = 0;
    NSInteger   index = newIndexPath.row;
    
    if( selIndexs != nil )
        nSel = [selIndexs count];
    
    NSMutableArray*    selOrders = [arrSelOrders objectAtIndex:step-1];
    
    [selOrders setObject:[NSNumber numberWithInteger:nSel-1] atIndexedSubscript:index];

    [self refreshCellItemsOrderValue:NO];
    
    
    if (nSel == 4) {
        //append
        [self valueStps_actionNext];
    }
}

- (void)tableView:(UITableView *)tV didDeselectRowAtIndexPath:(NSIndexPath *)newIndexPath
{
    NSMutableArray*    selOrders = [arrSelOrders objectAtIndex:step-1];
    
    NSInteger         index = newIndexPath.row;
    NSInteger         nSel = [[selOrders objectAtIndex:index] integerValue];
    
    for( int k = 0; k < [arrQuestion count]; k++ )
    {
        int order = [[selOrders objectAtIndex:k] intValue];
        
        if( nSel >= order )
            continue;
        
        [selOrders setObject:[NSNumber numberWithInt:order-1] atIndexedSubscript:k];
    }
    
    [selOrders setObject:[NSNumber numberWithInt:-1] atIndexedSubscript:index];

    [self refreshCellItemsOrderValue:NO];
}

-(void)dealloc
{
    if( self.step == 1 )
    {
        arrSelOrders = nil;
        [arrSelOrders release];
    }
    [arrNameOfInfoList release];
    [arrQuestion release];
    
    [scrView release];
    [tableView release];
    [btnBack release];
    
    [lblTitle release];
    self.nextView = nil;


    [super dealloc];
}

@end

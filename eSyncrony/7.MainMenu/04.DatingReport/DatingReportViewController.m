
#import "DatingReportViewController.h"
#import "Global.h"
#import "DSActivityView.h"
#import "eSyncronyAppDelegate.h"
#import "mainMenuViewController.h"

@implementation DatingReportViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }

    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"DatingReportView";
    [scrlView addSubview:contentsView];
    scrlView.contentSize = contentsView.frame.size;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self refreshContent];
}

- (void)refreshContent
{
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(esyncDatingReport_loadInfos) withObject:nil afterDelay:0.1];
}

- (void)esyncDatingReport_loadInfos
{
    NSDictionary* result = [UtilComm viewDatesReport];
    [DSBezelActivityView removeViewAnimated:NO];

    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Error"];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
        return;
    }
    
    NSDictionary    *dicResponse = (NSDictionary*)response;
    NSArray         *items = [dicResponse objectForKey:@"item"];
    if( items == nil || ![items isKindOfClass:[NSArray class]] )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"ERROR",)];
        return;
    }
    
    if( [items count] < 9 )
        return;
    
    // total
    totalNumLabel.text = [items objectAtIndex:0];

    // went out
    wentOutDateNumLabel.text = [items objectAtIndex:1];

    // average score
    averageScoreLabel.text = NSLocalizedString([items objectAtIndex:2],@"DatingReportView");
    
    avScoreAdviceLabel.text = NSLocalizedString([items objectAtIndex:3],@"DatingReportView");
    firstImpressLabel.text = NSLocalizedString([items objectAtIndex:4],@"DatingReportView");
    duringDateLabel.text = NSLocalizedString([items objectAtIndex:5],@"DatingReportView");
    positiveFirstLabel.text = NSLocalizedString([items objectAtIndex:6],@"DatingReportView");
    impressionLabel.text = NSLocalizedString([items objectAtIndex:7],@"DatingReportView");
    considerOnSecondLabel.text = NSLocalizedString([items objectAtIndex:8],@"DatingReportView");
}

- (void)dealloc
{
    [scrlView release];
    [contentsView release];
    
    [totalNumLabel release];
    [wentOutDateNumLabel release];
    [averageScoreLabel release];
    [avScoreAdviceLabel release];
    [firstImpressLabel release];
    [duringDateLabel release];
    [positiveFirstLabel release];
    [impressionLabel release];
    [considerOnSecondLabel release];
    
    [super dealloc];
}

@end






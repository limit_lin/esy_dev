
#import <UIKit/UIKit.h>

@interface DatingReportViewController : GAITrackedViewController
{
    IBOutlet UIScrollView   *scrlView;
    IBOutlet UIView         *contentsView;
    
    IBOutlet UILabel        *totalNumLabel;
    IBOutlet UILabel        *wentOutDateNumLabel;
    IBOutlet UILabel        *averageScoreLabel;
    IBOutlet UILabel        *avScoreAdviceLabel;
    IBOutlet UILabel        *firstImpressLabel;
    IBOutlet UILabel        *duringDateLabel;
    IBOutlet UILabel        *positiveFirstLabel;
    IBOutlet UILabel        *impressionLabel;
    IBOutlet UILabel        *considerOnSecondLabel;
}

@end

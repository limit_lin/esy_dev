//
//  MenuItemCell

#import "MenuItemCell.h"
#import "mainMenuViewController.h"
#import "global.h"

@implementation MenuItemCell

@synthesize itemTitleLabel, icnImgView, mainMenuView, idx;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)dealloc
{
    [itemTitleLabel release];
    [icnImgView release];
    
    [super dealloc];
}

-(IBAction)didClickCell:(id)sender
{
   [mainMenuView procItemClickEventWithID:idx];
}

@end


#import <UIKit/UIKit.h>

@class mainMenuViewController;

@interface MenuProfileCell : UITableViewCell{
    
    IBOutlet UIButton *btnAccount;
    IBOutlet UIImageView *imgAccout;
    IBOutlet UILabel *lblAccout;
    IBOutlet UIButton *btnReport;
    IBOutlet UIImageView *imgReport;
    IBOutlet UILabel *lblReport;
}

@property(strong,nonatomic) IBOutlet UILabel *numLabel;
@property(strong,nonatomic) mainMenuViewController *mainMenuView;

-(IBAction)didClickBaseProfile:(id)sender;
-(IBAction)didClickProfile:(id)sender;
-(IBAction)didClickReport:(id)sender;
-(IBAction)didClickAccount:(id)sender;
- (void)refreshContent;
@end

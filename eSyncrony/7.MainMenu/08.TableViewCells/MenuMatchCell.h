
#import <UIKit/UIKit.h>

@class mainMenuViewController;

@interface MenuMatchCell : UITableViewCell
{
    IBOutlet UILabel*       lblMatchNum;
    IBOutlet UIImageView*   viewMatchNumBG;
}

@property (nonatomic, assign) mainMenuViewController *mainMenuView;

- (void)refreshContent;

- (IBAction)didClickMyMatches:(id)sender;
- (IBAction)didClickProposed:(id)sender;
- (IBAction)didClickPending:(id)sender;
- (IBAction)didClickApproved:(id)sender;
- (IBAction)didClickCancelled:(id)sender;
- (IBAction)didClickReport:(id)sender;
- (IBAction)didClickFavorited:(id)sender;

@end


#import "Global.h"
#import "MenuMatchCell.h"
#import "mainMenuViewController.h"
#import "eSyncronyAppDelegate.h"

@implementation MenuMatchCell

@synthesize mainMenuView;

- (void)refreshContent
{    
    int     nMatchNum = [Global sharedGlobal].nProposedMatches;
    if (nMatchNum>20){
         [lblMatchNum setText:@"20+"];
    }else{
         [lblMatchNum setText:[NSString stringWithFormat:@"%d", nMatchNum]];
    }
    lblMatchNum.hidden = (nMatchNum == 0);
    viewMatchNumBG.hidden = (nMatchNum == 0);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (IBAction)didClickMyMatches:(id)sender
{
    [mainMenuView procItemClickEventWithID:MENU_MY_MATCHES];
}

- (IBAction)didClickProposed:(id)sender
{
    [mainMenuView procItemClickEventWithID:MYMATCHES_PROPOSED];
}

- (IBAction)didClickPending:(id)sender
{
    [mainMenuView procItemClickEventWithID:MYMATCHES_PENDING];
}

- (IBAction)didClickApproved:(id)sender
{
    [mainMenuView procItemClickEventWithID:MYMATCHES_APPROVED];
}

- (IBAction)didClickCancelled:(id)sender
{
    [mainMenuView procItemClickEventWithID:MYMATCHES_CANCELLED];
}

- (IBAction)didClickReport:(id)sender
{
    [mainMenuView procItemClickEventWithID:MYMATCHES_REPORT];
}

- (IBAction)didClickFavorited:(id)sender {
    [mainMenuView procItemClickEventWithID:MYMATCHES_FAVORITED];
}

-(void)dealloc
{
    [lblMatchNum release];
    [viewMatchNumBG release];

    [super dealloc];
}


@end


#import "MenuDatesCell.h"
#import "mainMenuViewController.h"
#import "global.h"

@implementation MenuDatesCell

- (void)refreshContent
{
    int     nDatesNum = [Global sharedGlobal].nDates;
    
    [lblDatesNum setText:[NSString stringWithFormat:@"%d", nDatesNum]];
    
    lblDatesNum.hidden = (nDatesNum == 0);
    viewDatesNumBG.hidden = (nDatesNum == 0);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(IBAction)didClickBaseDates:(id)sender
{
    [self.mainMenuView procItemClickEventWithID:MENU_DATES];
}

-(IBAction)didClickNewDates:(id)sender
{
    [self.mainMenuView procItemClickEventWithID:DATES_NEW_DATES];
}

-(IBAction)didClickPastDates:(id)sender
{
    [self.mainMenuView procItemClickEventWithID:DATES_PAST_DATES];
}

- (IBAction)didClickFeedBack:(id)sender {
    [self.mainMenuView procItemClickEventWithID:DATE_FEEDBACK];
}

-(void)dealloc
{
    [lblDatesNum release];
    [viewDatesNumBG release];
    
    [super dealloc];
}


@end

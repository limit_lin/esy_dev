
#import <UIKit/UIKit.h>

@interface NoMatchCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *noMatchLabel;

@end

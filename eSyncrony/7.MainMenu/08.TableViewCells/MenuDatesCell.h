
#import <UIKit/UIKit.h>

@class mainMenuViewController;

@interface MenuDatesCell : UITableViewCell
{
    IBOutlet UILabel*       lblDatesNum;
    IBOutlet UIImageView*   viewDatesNumBG;
}

@property(strong,nonatomic) mainMenuViewController *mainMenuView;

- (void)refreshContent;

-(IBAction)didClickBaseDates:(id)sender;
-(IBAction)didClickNewDates:(id)sender;
-(IBAction)didClickPastDates:(id)sender;
- (IBAction)didClickFeedBack:(id)sender;

@end

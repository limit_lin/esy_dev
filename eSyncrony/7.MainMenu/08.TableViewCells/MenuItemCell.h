
#import <UIKit/UIKit.h>

@class mainMenuViewController;

@interface MenuItemCell : UITableViewCell

@property(strong,nonatomic) IBOutlet UILabel *itemTitleLabel;
@property(strong,nonatomic) IBOutlet UIImageView *icnImgView;
@property(strong,nonatomic) mainMenuViewController *mainMenuView;
@property(nonatomic) NSInteger idx;

-(IBAction)didClickCell:(id)sender;


@end

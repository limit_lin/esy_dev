//
//  MenuProfileCell.m

#import "MenuProfileCell.h"
#import "mainMenuViewController.h"
#import "global.h"
#import "eSyncronyAppDelegate.h"
@implementation MenuProfileCell

@synthesize numLabel, mainMenuView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)refreshContent
{
    if (![[eSyncronyAppDelegate sharedInstance].membership isEqualToString:@"Premium"]){
        btnReport.hidden = YES;
        imgReport.hidden = YES;
        lblReport.hidden = YES;
        btnAccount.hidden = NO;
        imgAccout.hidden = NO;
        lblAccout.hidden = NO;
    }
}
-(IBAction)didClickBaseProfile:(id)sender
{
    [mainMenuView procItemClickEventWithID:MENU_MYPROFILE];
}

-(IBAction)didClickProfile:(id)sender
{
    [mainMenuView procItemClickEventWithID:PROFILE_PROFILE];
}

-(IBAction)didClickReport:(id)sender
{
    [mainMenuView procItemClickEventWithID:PROFILE_REPORT];
}

-(IBAction)didClickAccount:(id)sender
{
    [mainMenuView procItemClickEventWithID:PROFILE_ACCOUNT];
}

-(void)dealloc
{
    [numLabel release];
    [mainMenuView release];
    
    [btnAccount release];
    [imgAccout release];
    [lblAccout release];
    [btnReport release];
    [lblReport release];
    [imgReport release];
    [super dealloc];
}


@end


#import "PhotoExchangeCell.h"
#import "PhotoExchangeViewController.h"
#import "MatchProfileViewController.h"
#import "PhotoGalleryViewController.h"

#import "DSActivityView.h"
#import "Global.h"
#import "UtilComm.h"
#import "ImageUtil.h"

#import "eSyncronyAppDelegate.h"
#import "mainMenuViewController.h"

@interface PhotoExchangeViewController ()

@end

@implementation PhotoExchangeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"PhotoExchangeView";
    arrMatchesItems = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self refreshContent];
}

- (void)refreshContent
{
    lblNoMatches.hidden = YES;
    
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(esyncPhotoExchange_loadInfos) withObject:nil afterDelay:0.1];
}

- (void)esyncPhotoExchange_loadInfos
{
    [DSBezelActivityView removeViewAnimated:NO];

   // NSDictionary* result = [UtilComm viewPhotoRequest:nil];
    NSDictionary* result = [UtilComm viewPhotoRequestNew:nil];
    NSLog(@"%@",result);
    
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Error"];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
        return;
    }
    
    [arrMatchesItems release];
    arrMatchesItems = [[NSMutableArray alloc] initWithCapacity:0];
    
    NSDictionary*   responseDic = (NSDictionary*)response;
    
    if( [responseDic isKindOfClass:[NSDictionary class]] )
    {
        NSArray *arrItems = [responseDic objectForKey:@"item"];
        
        if( [arrItems isKindOfClass:[NSDictionary class]] )
        {
            NSDictionary* item = (NSDictionary*)arrItems;
            NSMutableDictionary *mutItem = [NSMutableDictionary dictionaryWithDictionary:item];
            
            [arrMatchesItems addObject:mutItem];
        }
        else if( [arrItems isKindOfClass:[NSArray class]] )
        {
            for( int i = 0; i < [arrItems count]; i++ )
            {
                NSDictionary        *item = [arrItems objectAtIndex:i];
                NSMutableDictionary *mutItem = [NSMutableDictionary dictionaryWithDictionary:item];
                
                [arrMatchesItems addObject:mutItem];
            }
        }
    }
    
    [listTblView reloadData];

    if( [arrMatchesItems count] == 0 )
    {
        lblNoMatches.hidden = NO;
        listTblView.scrollEnabled = NO;
    }
    else
    {
        lblNoMatches.hidden = YES;
        listTblView.scrollEnabled = YES;
    }
}

#pragma mark --- Table Functions ---
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if( arrMatchesItems == nil )
        return 0;
    
    return [arrMatchesItems count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 140;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary     *dicMatchesInfo = [arrMatchesItems objectAtIndex:indexPath.row];
    PhotoExchangeCell       *cell;
    
//    NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"PhotoExchangeCell" owner:nil options:nil];
//    cell = [arr objectAtIndex:0];
    
    static NSString *CellIdentifier = @"ExchangeIdentifier";
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
       /* UINib *nib = [UINib nibWithNibName:@"PhotoExchangeCell" bundle:nil];
        [tableView registerNib:nib forCellReuseIdentifier:CellIdentifier];
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        */
        cell=[[[NSBundle mainBundle]loadNibNamed:@"PhotoExchangeCell" owner:self options:nil]objectAtIndex:0];
    }

    cell.delegate = self;
    [cell setMatchesInfo:dicMatchesInfo withIndex:indexPath.row];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary         *dicMatchesInfo = [arrMatchesItems objectAtIndex:indexPath.row];
    MatchProfileViewController  *matchInfoViewCtrl = [MatchProfileViewController createWithMatchInfo:dicMatchesInfo];

    matchInfoViewCtrl.m_bTopButton = NO;
    matchInfoViewCtrl.m_bDismissSelf = YES;

    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.navigationController pushViewController:matchInfoViewCtrl animated:YES];

    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

//===============================================================================================================//
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if( _bUpload == YES )
    {
        if( buttonIndex == 0 )
        {
            PhotoGalleryViewController *galleryViewController = [[[PhotoGalleryViewController alloc] initWithNibName:@"PhotoGalleryViewController" bundle:nil] autorelease];
            
            galleryViewController.strMatchAccNo = @"";
            galleryViewController.strUserName = [eSyncronyAppDelegate sharedInstance].strName;
            
            [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.navigationController pushViewController: galleryViewController animated:NO ];
        }
    }
    else
    {
        [self refreshContent];
    }
}

- (void)refreshContentAfterAlertMsg:(NSString*)strMsg
{
    if( ![strMsg isKindOfClass:[NSString  class]] )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Error",)];
        return;
    }
    
    if( [strMsg hasPrefix:@"ERROR"] )
    {
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:strMsg];
        return;
    }
    
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"eSynchrony"
                                                 message:strMsg
                                                delegate:self
                                       cancelButtonTitle:NSLocalizedString(@"Ok",)
                                       otherButtonTitles:nil];
    [av show];
    [av release];
}

- (void)onNoUploadedPhotoProc
{
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"eSynchrony"
                                                 message:NSLocalizedString(@"Your photo has not been uploaded or is still pending for approval.",@"PhotoExchangeView")
                                                delegate:self
                                       cancelButtonTitle:NSLocalizedString(@"Ok",)
                                       otherButtonTitles:NSLocalizedString(@"Cancel",), nil];
    [av show];
    [av release];
}

- (void)requestApprovePhoto:(NSDictionary*)dicMatchInfo
{
    [DSBezelActivityView removeViewAnimated:NO];
    
    NSString    *macc_no = [dicMatchInfo objectForKey:@"acc_no"];
    if( macc_no == nil )
        macc_no = [dicMatchInfo objectForKey:@"Acc_no"];
    if( macc_no == nil )
        return;
    
    NSDictionary    *result = [UtilComm exchangePhotoProcess:macc_no];
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"eSynchrony"];
        return;
    }
    
    NSString    *response = [result objectForKey:@"response"];
    [self refreshContentAfterAlertMsg:response];
}

- (void)requestDeclinePhoto:(NSDictionary*)dicMatchInfo
{
    [DSBezelActivityView removeViewAnimated:NO];
    
    NSString    *macc_no = [dicMatchInfo objectForKey:@"acc_no"];
    if( macc_no == nil )
        macc_no = [dicMatchInfo objectForKey:@"Acc_no"];
    if( macc_no == nil )
        return;
    
    NSDictionary    *result = [UtilComm rejectPhotoExchange:macc_no];
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"eSynchrony"];
        return;
    }
    
    NSString    *response = [result objectForKey:@"response"];
    [self refreshContentAfterAlertMsg:response];
}

- (void)requestSendRemindPhoto:(NSDictionary*)dicMatchInfo
{
    [DSBezelActivityView removeViewAnimated:NO];
    
    NSString    *macc_no = [dicMatchInfo objectForKey:@"acc_no"];
    if( macc_no == nil )
        macc_no = [dicMatchInfo objectForKey:@"Acc_no"];
    if( macc_no == nil )
        return;

    NSDictionary    *result = [UtilComm sendPhotoReminder:macc_no];
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"eSynchrony"];
        return;
    }
    
    NSString    *response = [result objectForKey:@"response"];
    [self refreshContentAfterAlertMsg:response];
}

- (void)didClickApproveWithUpload:(BOOL)bUpload atIndex:(NSInteger)index
{
    _bUpload = bUpload;

    if( bUpload == YES )
    {
        [self onNoUploadedPhotoProc];
        return;
    }
    
    NSDictionary*   dicMatchInfo = [arrMatchesItems objectAtIndex:index];
    
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(requestApprovePhoto:) withObject:dicMatchInfo afterDelay:0.1];
}

- (void)didClickDeclineWithUpload:(BOOL)bUpload atIndex:(NSInteger)index
{
    _bUpload = bUpload;
    
    if( bUpload == YES )
    {
        [self onNoUploadedPhotoProc];
        return;
    }
    
    NSDictionary*   dicMatchInfo = [arrMatchesItems objectAtIndex:index];
    
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(requestDeclinePhoto:) withObject:dicMatchInfo afterDelay:0.1];
}

- (void)didClickRemindWithUpload:(BOOL)bUpload atIndex:(NSInteger)index
{
    _bUpload = bUpload;
    
    if( bUpload == YES )
    {
        [self onNoUploadedPhotoProc];
        return;
    }
    
    NSDictionary*   dicMatchInfo = [arrMatchesItems objectAtIndex:index];
    
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(requestSendRemindPhoto:) withObject:dicMatchInfo afterDelay:0.1];
}

- (void)dealloc
{
    [listTblView release];
    [lblNoMatches release];
    
    [arrMatchesItems release];
    
    [super dealloc];
}

@end


#import "PhotoExchangeCell.h"

#import "Global.h"
#import "UtilComm.h"
#import "DSActivityView.h"
#import "ImageUtil.h"
#import "UIImageView+WebCache.h"
#import "mainMenuViewController.h"
#import "eSyncronyAppDelegate.h"

#import "MatchProfileViewController.h"

@implementation PhotoExchangeCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}


- (void)loadDefaultImage{
    if( [eSyncronyAppDelegate sharedInstance].gender == 1 ){
        photoImgView.image = [UIImage imageNamed:@"male1.png"];
    }else {
        photoImgView.image = [UIImage imageNamed:@"female1.png"];
    }
}

- (BOOL)downloadMatchUserPhoto
{
    NSString        *macc_no = [_dicMatchesInfo objectForKey:@"acc_no"];
    if( macc_no == nil )
        macc_no = [_dicMatchesInfo objectForKey:@"Acc_no"];
    if( macc_no == nil )
        return FALSE;
    
    NSString    *view_photo = [_dicMatchesInfo objectForKey:@"view_photo"];
 
    if( [view_photo isEqualToString:@"photo view not allowed."] )
        return FALSE;
    
    NSString *filename = [_dicMatchesInfo objectForKey:@"filename"];
    NSString*   imageURL = [NSString stringWithFormat:@"%@%@", WEBSERVICE_PHOTO_BASEURI, filename];
    NSString*   imagePath = [ImageUtil loadImagePathFromURL:imageURL];
    
    [self performSelectorOnMainThread:@selector(PhotoExchangeCell_onDownLoadImage:) withObject:imagePath waitUntilDone:YES];
    
    return TRUE;
}

- (void)PhotoExchangeCell_onDownLoadImage:(NSString*)imagePath
{
    UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
    if( image != nil )
    {
        photoFrameView.hidden = NO;
        photoImgView.image = image;
        [_dicMatchesInfo setObject:imagePath forKey:@"localphoto_path"];
    }
    else
        [_dicMatchesInfo setObject:@"nophoto" forKey:@"localphoto_path"];
}

- (void)setMatchPhoto
{
    
    NSString *strImage = nil;
    if( ![[_dicMatchesInfo objectForKey:@"view_photo"] isEqualToString:@"photo view not allowed."] ){
        strImage = [NSString stringWithFormat:@"%@%@", WEBSERVICE_PHOTO_BASEURI, [_dicMatchesInfo objectForKey:@"filename"]];
    }
    
    NSString *strPlaceholder;
    if( [eSyncronyAppDelegate sharedInstance].gender == 1 ){
        strPlaceholder = @"male1.png";
    }else {
        strPlaceholder = @"female1.png";
    }
    
    [photoImgView sd_setImageWithURL:[NSURL URLWithString:strImage] placeholderImage:[UIImage imageNamed:strPlaceholder] ];
    
//    if( [eSyncronyAppDelegate sharedInstance].gender == 1 )
//        photoImgView.image = [UIImage imageNamed:@"male1.png"];
//    
//    NSString    *imagePath = [_dicMatchesInfo objectForKey:@"localphoto_path"];
//    
//    if( imagePath != nil )
//    {
//        UIImage     *image = [UIImage imageWithContentsOfFile:imagePath];
//        if( image != nil )
//            photoImgView.image = image;
//    }
//    else
//    {
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,
//                                                 (unsigned long)NULL), ^(void) {
//            [_dicMatchesInfo retain];
//            if( [self downloadMatchUserPhoto] == FALSE )
//            {
//                [_dicMatchesInfo setObject:@"nophoto" forKey:@"localphoto_path"];
//            }
//            [_dicMatchesInfo release];
//        });
//    }
}

- (NSString*)getEthnicityName:(int)nEthnicity
{
    NSArray *arrayNames = [Global loadQuestionArray:@"ethnicity"];
    if( nEthnicity < [arrayNames count] )
        return [arrayNames objectAtIndex:nEthnicity];
    
    return @"";
}

- (void)setMatchesInfo:(NSMutableDictionary*)dicMatchesInfo withIndex:(NSInteger)index
{
    _nIndex = index;
    _dicMatchesInfo = dicMatchesInfo;
    
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
    
    photoBGView.layer.cornerRadius = 5;
    photoImgView.layer.cornerRadius = 45;
    photoImgView.clipsToBounds = YES;
    photoFrameView.hidden = YES;
    
    rightView.layer.cornerRadius = 5;
    
    lblName.text = [dicMatchesInfo objectForKey:@"nname"];
    //    [nameLabel sizeToFit];
    CGSize lblNameSize = [lblName.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:lblName.font,NSFontAttributeName, nil]];
    lblName.frame = CGRectMake(120, 14, lblNameSize.width, lblNameSize.height);
    
    CGFloat xPos = lblName.frame.origin.x + lblName.frame.size.width;
    CGFloat yPos = lblName.frame.origin.y;
    
    lblAge.text = [NSString stringWithFormat:@", %@", [dicMatchesInfo objectForKey:@"age"]];
    CGRect frame = lblAge.frame;
    frame.origin.x = xPos;
    frame.origin.y = yPos;
    
    lblAge.frame = frame;
    [lblAge sizeToFit];

    int nEthnicity = [[dicMatchesInfo objectForKey:@"Ethnicity"] intValue];
    lblNation.text = [self getEthnicityName:nEthnicity];
    [lblNation sizeToFit];
    
    NSString    *strDate = [dicMatchesInfo objectForKey:@"m_replydt"];
    if( strDate == nil || [strDate isEqualToString:@""] )
    {
        lblDate.hidden = YES;
        
        CGRect frame = lblStatusTitle.frame;
        frame.origin.y -= 15;
        lblStatusTitle.frame = frame;
        
        frame = lblStatusContent.frame;
        frame.origin.y -= 15;
        lblStatusContent.frame = frame;
    }
    else
    {
        NSDate* date = [Global convertStringToDate:strDate withFormat:@"yyyy-MM-dd HH:mm:ss"];
        lblDate.text = [Global getDateTime:date withFormat:@"dd MMM yyyy"];
    }

    [lblDate sizeToFit];

    //[lblStatusContent sizeToFit];
    [lblStatusTitle sizeToFit];
    
    [self checkAndSetStatusContent];
    [self setMatchPhoto];
}

- (void)checkAndSetStatusContent
{
    NSArray     *arrayAnon = [_dicMatchesInfo objectForKey:@"anon"];
    NSString    *strStatus1 = [arrayAnon objectAtIndex:5];
    NSArray     *arrStatus = [strStatus1 componentsSeparatedByString:@"|"];
    NSString    *strStatus = [arrStatus objectAtIndex:1];
    lblStatusContent.font = [UIFont fontWithName:lblStatusContent.font.fontName size:13];
//    lblStatusContent.text = strStatus;
    lblStatusContent.text = [NSString stringWithFormat:@"%@",NSLocalizedString(strStatus, @"PhotoExchangeCell")];
    
    bUpload = NO;
    
    NSRange range = [strStatus rangeOfString:@"rejected"];
//    NSLog(@"strStatusstrStatusstrStatus====%@",strStatus);
    
    if([strStatus isEqualToString:@"You have rejected this match. Want to change to Approve?"] )
    {
        NSMutableAttributedString* string = [[NSMutableAttributedString alloc]initWithString:strStatus];
        
        [string addAttribute:NSForegroundColorAttributeName value:TITLE_REG_COLOR range:range];
        lblStatusContent.font = [UIFont fontWithName:lblStatusContent.font.fontName size:11];
        [lblStatusContent setAttributedText:string];
        
        viewBtnDecline.hidden = YES;
        viewBtnRemind.hidden = YES;
        viewBtnApprove.hidden = NO;
    }
    else if( [strStatus isEqualToString:@"Approve Decline"] )
    {
        lblStatusTitle.hidden=  YES;
        lblStatusContent.hidden = YES;
        viewBtnRemind.hidden = YES;
        viewBtnApprove.hidden = NO;
        viewBtnDecline.hidden = NO;
    }
    else if( [strStatus isEqualToString:@"Send Reminder"] )
    {
        lblStatusTitle.hidden=  YES;
        lblStatusContent.hidden = YES;
        viewBtnApprove.hidden = YES;
        viewBtnDecline.hidden = YES;
        viewBtnRemind.hidden = NO;
    }
    else if( [strStatus hasPrefix:@"Approve Decline - Upload"] )
    {
        bUpload = YES;
        
        lblStatusTitle.hidden=  YES;
        lblStatusContent.hidden = YES;
        viewBtnDecline.hidden = NO;
        viewBtnRemind.hidden = YES;
        viewBtnApprove.hidden = NO;
    }
    else if( [strStatus hasPrefix:@"Change to Approve - Upload"] )
    {
        bUpload = YES;
    
//       strStatus = @"You have rejected this match. Want to change to Approve?";
        strStatus = NSLocalizedString(@"You have rejected this match. Want to change to Approve?",@"PhotoExchangeCell");
        range = [strStatus rangeOfString:@"rejected"];
//        range = [strStatus rangeOfString:[NSString stringWithFormat:@"%@",NSLocalizedString(@"rejected", @"PhotoExchangeCell")]];
        
        NSMutableAttributedString* string = [[NSMutableAttributedString alloc]initWithString:strStatus];
        [string addAttribute:NSForegroundColorAttributeName value:TITLE_REG_COLOR range:range];
        lblStatusContent.font = [UIFont fontWithName:lblStatusContent.font.fontName size:11];
        [lblStatusContent setAttributedText:string];
        viewBtnApprove.hidden = NO;
        viewBtnDecline.hidden = YES;
        viewBtnRemind.hidden = YES;
    }
    else if( [strStatus hasPrefix:@"Your match would like to view your photo. Approve Decline"])
    {
//         lblStatusContent.text = NSLocalizedString(@"Your match would like to view your profile photo.",@"PhotoExchangeCell");
        lblStatusContent.text = NSLocalizedString(@"Your match would like to view your photo.",@"PhotoExchangeCell");//repair photo exchange
   
        viewBtnDecline.hidden = NO;
        viewBtnRemind.hidden = YES;
        viewBtnApprove.hidden = NO;
        
    }
    else if ([strStatus hasPrefix:@"Match has exchanged photo with you."])//append
    {
//        lblStatusContent.text = NSLocalizedString(@"You decides not to exchange for now.",@"PhotoExchangeCell");
        lblStatusContent.text = NSLocalizedString(@"Match has exchanged photo with you.",@"PhotoExchangeCell");
        viewBtnApprove.hidden = YES;
        viewBtnDecline.hidden = YES;
        viewBtnRemind.hidden = YES;
    }
    else if( [strStatus hasPrefix:@"Waiting for match to upload photos"] )
    {
        lblStatusContent.text = NSLocalizedString(@"Waiting for match to upload photos.",@"PhotoExchangeCell");
        viewBtnDecline.hidden = YES;
        viewBtnRemind.hidden = YES;
        viewBtnApprove.hidden = YES;
    }
    else if( [strStatus hasPrefix:@"You decides not to exchange for now."] )//append
    {
        lblStatusContent.text = NSLocalizedString(@"You decides not to exchange for now.",@"PhotoExchangeCell");
        viewBtnDecline.hidden = YES;
        viewBtnRemind.hidden = YES;
        viewBtnApprove.hidden = YES;
    }
    else
    {
        viewBtnDecline.hidden = YES;
        viewBtnRemind.hidden = YES;
        viewBtnApprove.hidden = YES;
    }
    
    //lblStatusContent.textAlignment = NSTextAlignmentCenter;

    lblStatusContent.numberOfLines = 0;
    CGRect rect = lblStatusContent.frame;
    CGSize size = CGSizeMake(130.f, 100);
    CGSize actualsize = [lblStatusContent.text sizeWithFont:lblStatusContent.font constrainedToSize:size lineBreakMode:NSLineBreakByTruncatingTail];
    if (actualsize.height>50) {
        lblStatusContent.adjustsFontSizeToFitWidth =YES;
        rect.origin.y = rect.origin.y +10;
    }
    actualsize.height = actualsize.height+5;
    rect.size = actualsize;
    lblStatusContent.frame = rect;
    
    
//    [lblStatusContent sizeToFit];
}

- (IBAction)didClickApprove:(id)sender
{
    [self.delegate didClickApproveWithUpload:bUpload atIndex:_nIndex];
}

- (IBAction)didClickRemind:(id)sender
{
    [self.delegate didClickRemindWithUpload:bUpload atIndex:_nIndex];
}

- (IBAction)didClickDecline:(id)sender
{
    [self.delegate didClickDeclineWithUpload:bUpload atIndex:_nIndex];
}

-(void)dealloc
{
    [photoBGView release];
    [photoImgView release];
    [photoFrameView release];
    [rightView release];
    
    [lblName release];
    [lblAge release];
    [lblNation release];
    [lblDate release];
    
    [lblStatusTitle release];
    [lblStatusContent release];
    
    [viewBtnApprove release];
    [viewBtnRemind release];
    [viewBtnDecline release];
    
    [super dealloc];
}

@end

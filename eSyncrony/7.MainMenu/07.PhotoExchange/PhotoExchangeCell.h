
#import <UIKit/UIKit.h>

@protocol PhotoExchangeCellDelegate <NSObject>

@optional

- (void)didClickApproveWithUpload:(BOOL)bUpload atIndex:(NSInteger)index;

- (void)didClickDeclineWithUpload:(BOOL)bUpload atIndex:(NSInteger)index;

- (void)didClickRemindWithUpload:(BOOL)bUpload atIndex:(NSInteger)index;

@end

@interface PhotoExchangeCell : UITableViewCell
{
    IBOutlet UIImageView    *photoBGView;
    IBOutlet UIImageView    *photoImgView;
    IBOutlet UIImageView    *photoFrameView;
    
    IBOutlet UIImageView    *rightView;
    
    IBOutlet UILabel        *lblName;
    IBOutlet UILabel        *lblAge;
    IBOutlet UILabel        *lblNation;
    IBOutlet UILabel        *lblDate;
    IBOutlet UILabel        *lblStatusTitle;
    IBOutlet UILabel        *lblStatusContent;
    
    IBOutlet UIView         *viewBtnApprove;
    IBOutlet UIView         *viewBtnDecline;
    IBOutlet UIView         *viewBtnRemind;

    NSMutableDictionary*   _dicMatchesInfo;

    BOOL        bUpload;
    NSInteger   _nIndex;
}

@property (nonatomic, assign) id<PhotoExchangeCellDelegate> delegate;

- (void)setMatchesInfo:(NSMutableDictionary*)dicMatchesInfo withIndex:(NSInteger)index;

- (IBAction)didClickApprove:(id)sender;

- (IBAction)didClickRemind:(id)sender;

- (IBAction)didClickDecline:(id)sender;

@end


#import <UIKit/UIKit.h>
#import "PhotoExchangeCell.h"

@interface PhotoExchangeViewController : GAITrackedViewController <PhotoExchangeCellDelegate>
{
    IBOutlet UITableView    *listTblView;
    IBOutlet UILabel        *lblNoMatches;
    
    NSMutableArray          *arrMatchesItems;

    BOOL    _bUpload;
}

@end

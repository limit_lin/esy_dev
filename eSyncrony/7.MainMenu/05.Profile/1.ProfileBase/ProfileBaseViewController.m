
#import "DSActivityView.h"
#import "Global.h"
#import "UtilComm.h"
#import "ImageUtil.h"

#import "eSyncronyAppDelegate.h"
#import "mainMenuViewController.h"

#import "UIProfilePhotoItemView.h"
#import "ProfileBaseViewController.h"

#define SUB_TITLE_VIEW_HEIGHT       32
#define SUB_TITLE_VIEW_BG_COLOR     RGBColor(146, 199, 220)

#define CLASS_TITLE_VIEW_TEXT_COLOR RGBColor(80, 80, 80)

@implementation ProfileBaseViewController

- (void)initContentViews
{
    for( UIView* view in scrlView.subviews )
        [view removeFromSuperview];

    [scrlView addSubview:contentsView];
    scrlView.contentSize = contentsView.frame.size;
    _fLastYPos = contentsView.frame.size.height;
}

- (void)addSubTitleView:(NSString*)strTitle//personality,lifestyle...
{
    CGSize  size = scrlView.frame.size;
    
    UIView  *bgView = [[[UIView alloc] initWithFrame:CGRectMake(0, _fLastYPos, size.width, SUB_TITLE_VIEW_HEIGHT)] autorelease];
    bgView.backgroundColor = SUB_TITLE_VIEW_BG_COLOR;
    [scrlView addSubview:bgView];

    UIView  *bgView1 = [[[UIView alloc] initWithFrame:CGRectMake(3, _fLastYPos+3, size.width-6, SUB_TITLE_VIEW_HEIGHT-6)] autorelease];
    bgView1.backgroundColor = [UIColor whiteColor];
    [scrlView addSubview:bgView1];
    
    UILabel *lblTitle;
    
    if( SYSTEM_VERSION_LESS_THAN(@"7.0") )
        lblTitle = [[[UILabel alloc] initWithFrame:CGRectMake(3, _fLastYPos+9, size.width-6, 20)] autorelease];
    else
        lblTitle = [[[UILabel alloc] initWithFrame:CGRectMake(3, _fLastYPos+7, size.width-6, 20)] autorelease];
    
    if ([strTitle isEqualToString:NSLocalizedString(@"COMMON INTEREST",)]||[strTitle isEqualToString:NSLocalizedString(@"COMMON INTERESTS",)]) {
        strTitle = [NSString stringWithFormat:@"%d %@",comInterstNum,strTitle];
        NSMutableAttributedString *attributedString = [[[NSMutableAttributedString alloc] initWithString:strTitle] autorelease];
        //为所有文本设置字体
        [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeueLTStd-MdCn" size:15] range:NSMakeRange(0, [attributedString length])];
        [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:176/255.0f green:65/255.0f blue:99/255.0f alpha:1.0f]  range:NSMakeRange(0, [attributedString length])];
//        NSRange rang = [[attributedString string] rangeOfString:NSLocalizedString(@"COMMON INTEREST",) options:NSCaseInsensitiveSearch];
        NSRange rang = [[attributedString string] rangeOfString:NSLocalizedString(strTitle,) options:NSCaseInsensitiveSearch];
        
        [attributedString addAttribute:NSForegroundColorAttributeName value:TITLE_TEXT_COLLOR range:rang];
        lblTitle.attributedText = attributedString;
    }else{
        lblTitle.font = [UIFont fontWithName:@"HelveticaNeueLTStd-MdCn" size:15];
        lblTitle.textColor = TITLE_TEXT_COLLOR;
        lblTitle.text = strTitle;
    }
    lblTitle.backgroundColor = [UIColor clearColor];
    lblTitle.textAlignment = NSTextAlignmentCenter;
    [scrlView addSubview:lblTitle];

    _fLastYPos += SUB_TITLE_VIEW_HEIGHT;
}

- (void)addClassTitleView:(NSString*)strTitle//music,sport....
{
    CGSize  size = scrlView.frame.size;

    UILabel *lblTitle = [[[UILabel alloc] initWithFrame:CGRectMake(10, _fLastYPos+8, size.width-20, 20)] autorelease];
    lblTitle.font = [UIFont fontWithName:@"HelveticaNeueLTStd-MdCn" size:15];
    lblTitle.backgroundColor = [UIColor clearColor];
    lblTitle.textColor = CLASS_TITLE_VIEW_TEXT_COLOR;
    
    lblTitle.text = strTitle;
    [scrlView addSubview:lblTitle];
    
    UIView  *splitView = [[[UIView alloc] initWithFrame:CGRectMake(3, _fLastYPos+30, size.width-6, 1)] autorelease];
    splitView.backgroundColor = [UIColor lightGrayColor];
    [scrlView addSubview:splitView];

    _fLastYPos += 32;
}

- (void)addContentToRight:(NSString*)strContent
{
    CGSize  size = scrlView.frame.size;
    
    UILabel *lblContent = [[[UILabel alloc] initWithFrame:CGRectMake(10, _fLastYPos+10, size.width-20, 15)] autorelease];
    lblContent.font = [UIFont fontWithName:@"HelveticaNeueLTStd-MdCn" size:15];
    lblContent.backgroundColor = [UIColor clearColor];
    lblContent.textColor = [UIColor greenColor];//TITLE_REG_COLOR;
    lblContent.text = strContent;
    lblContent.textAlignment = NSTextAlignmentRight;
    lblContent.numberOfLines = 100;
    [scrlView addSubview:lblContent];
    
    CGSize maxSize = CGSizeMake( size.width-20, 1000 );
    CGSize contentSize = [strContent sizeWithFont:lblContent.font constrainedToSize:maxSize];
    contentSize.width += 10;
    lblContent.frame = CGRectMake(size.width-10-contentSize.width, _fLastYPos+10, contentSize.width, contentSize.height );
    
    _fLastYPos += 18 + contentSize.height;
}

- (void)addOneItemTitle:(NSString*)strTitle content:(NSString*)strContent//strTitle==gender,birthday...
{
    
    CGSize  size = scrlView.frame.size;

    UILabel *lblTitle = [[[UILabel alloc] initWithFrame:CGRectMake(10, _fLastYPos+10, size.width-20,20)] autorelease];
    
    lblTitle.backgroundColor = [UIColor clearColor];
    lblTitle.textColor = CLASS_TITLE_VIEW_TEXT_COLOR;
    lblTitle.text = strTitle;
    CGSize plabelSize = [lblTitle.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:lblTitle.font,NSFontAttributeName, nil]];
//    CGSize plabelSize = [lblTitle boundingRectWithSize:CGSizeMake(size.width-20, 20)];
    lblTitle.frame = CGRectMake(10, _fLastYPos+10, plabelSize.width,plabelSize.height);
    
    [scrlView addSubview:lblTitle];
    [lblTitle sizeToFit];
//    [lblTitle setAdjustsFontSizeToFitWidth:YES];

    UILabel *lblContent = [[[UILabel alloc] initWithFrame:CGRectMake(10, _fLastYPos+10, size.width-20, 20)] autorelease];

    lblContent.font = [UIFont fontWithName:@"HelveticaNeueLTStd-MdCn" size:15];
    lblContent.backgroundColor = [UIColor clearColor];
    lblContent.textAlignment = NSTextAlignmentRight;
    lblContent.numberOfLines = 0;
    lblContent.text = strContent;
    
    if (lblContent.text.length == 0) {
        lblContent.textColor = TITLE_TEXT_COLLOR;
    }else{
        subRange =[lblContent.text rangeOfString:NSLocalizedString(@"(Verified)",@"ProfileView")];
        if (subRange.location == NSNotFound) {
            lblContent.textColor = TITLE_TEXT_COLLOR;
        }else
        {
            //NSMutableString *string=[lblContent.text mutableCopy];
            
            int length= (int)[lblContent.text length];
            //NSLog(@"%d %@",length,lblContent.text);
            
            NSMutableAttributedString *str=[[NSMutableAttributedString alloc]initWithString:lblContent.text];
            
            //lblContent.textColor = TITLE_TEXT_COLLOR;
            //        NSMutableString *mutableContent=[[NSMutableString alloc]initWithCharacters:strContent length:length];
            //        NSLog(@"%@",mutableContent);
            
            [str addAttribute:NSForegroundColorAttributeName value:TITLE_TEXT_COLLOR range:NSMakeRange(0, (length-subRange.length))];
            // [str addAttribute:NSForegroundColorAttributeName value:[UIColor greenColor] range:NSMakeRange(subRange.location, 10)];
            [str addAttribute:NSForegroundColorAttributeName value:RGBColor(82,159,170) range:NSMakeRange(subRange.location, subRange.length)];
            
            //lblContent.textColor=[UIColor greenColor];
            lblContent.attributedText = str;
            //NSLog(@"%@",lblContent.attributedText);
        }
    }
    [scrlView addSubview:lblContent];
    
    float   maxWidth = size.width-30-lblTitle.frame.size.width;
    
    lblContent.frame = CGRectMake(0, 0, maxWidth, 20 );
//    [lblContent sizeToFit];
    
    CGSize contentSize = lblContent.frame.size;
    lblContent.frame = CGRectMake(size.width-10-contentSize.width, _fLastYPos+10, contentSize.width, contentSize.height );
     
     _fLastYPos += 12 + MAX( contentSize.height, 20 );
    
    UIView  *splitView = [[[UIView alloc] initWithFrame:CGRectMake(3, _fLastYPos-1, size.width-6, 1)] autorelease];
    splitView.backgroundColor = [UIColor lightGrayColor];
    [scrlView addSubview:splitView];
}

- (void)showCommonInterest:(NSDictionary*)dicCommonInterest
{
    _fLastYPos = contentsView.frame.size.height;

    if( ![dicCommonInterest isKindOfClass:[NSDictionary class]] )
    {
        //No Common Interest
//        [self addContentToRight:@"No Common Interest"];
        return;
    }
    
    NSString*   strFood = [dicCommonInterest objectForKey:@"food"];
    NSString*   strHobby = [dicCommonInterest objectForKey:@"hobby"];
    NSString*   strFilm = [dicCommonInterest objectForKey:@"film"];
    NSString*   strMusic = [dicCommonInterest objectForKey:@"music"];
    NSString*   strTravel = [dicCommonInterest objectForKey:@"travel"];
    NSString*   strSport = [dicCommonInterest objectForKey:@"sport"];
    if ([strFood isEqualToString:@""]&&[strHobby isEqualToString:@""]&&[strFilm isEqualToString:@""]&&[strMusic isEqualToString:@""]&&[strTravel isEqualToString:@""]&&[strSport isEqualToString:@""]) {
        return;
    }
    comInterstNum = 0;
    NSArray *arrComInterest = [NSArray arrayWithObjects:strFood,strHobby,strFilm,strMusic,strTravel,strSport, nil];
    
//    NSLog(@"%@",arrComInterest);
    
    for (NSString *str in arrComInterest) {
        NSArray*    items = [str componentsSeparatedByString:@","];
        if( !(items == 0 || [str isEqualToString:@""]) ){

            comInterstNum += items.count;
            
        }
    }
    if (comInterstNum <= 1) {
        [self addSubTitleView:NSLocalizedString(@"COMMON INTEREST",@"ProfileBaseView")];
    }else
        [self addSubTitleView:NSLocalizedString(@"COMMON INTERESTS",@"ProfileBaseView")];
    
    if( ![strFood isEqualToString:@""] )
        [self addOneInterestInfo:strFood type:@"Food" ofTitle:NSLocalizedString(@"Favourite foods",@"ProfileBaseView")];
    
    if( ![strHobby isEqualToString:@""] )
        [self addOneInterestInfo:strHobby type:@"Hobby" ofTitle:NSLocalizedString(@"Hobbies",@"ProfileBaseView")];
    
    if( ![strFilm isEqualToString:@""] )
        [self addOneInterestInfo:strFilm type:@"Film" ofTitle:NSLocalizedString(@"Films",@"ProfileBaseView")];
    
    if( ![strMusic isEqualToString:@""] )
        [self addOneInterestInfo:strMusic type:@"Music" ofTitle:NSLocalizedString(@"Music",@"ProfileBaseView")];
    
    if( ![strTravel isEqualToString:@""] )
        [self addOneInterestInfo:strTravel type:@"Travel" ofTitle:NSLocalizedString(@"Holiday Destination",@"ProfileBaseView")];
    
    if( ![strSport isEqualToString:@""] )
        [self addOneInterestInfo:strSport type:@"sport" ofTitle:NSLocalizedString(@"Sport",@"ProfileBaseView")];
}

- (void)addOneInterestInfo:(NSString*)txtInfo type:(NSString*)type ofTitle:(NSString*)strTitle
{
    NSArray*    items = [txtInfo componentsSeparatedByString:@","];
    if( items == 0 || [txtInfo isEqualToString:@""] )
        return;
    
    BOOL bEmpty = YES;
    
    for( int i = 0; i < [items count]; i++ )
    {
        NSString*   name = [items objectAtIndex:i];
        if( [name isEqualToString:@""] )
            continue;

        bEmpty = NO;
        break;
    }
    
    if( bEmpty == YES )
        return;

    [self addClassTitleView:strTitle];

    CGSize  size = scrlView.frame.size;
    UIView  *infoView = [[[UIView alloc] initWithFrame:CGRectMake(0, _fLastYPos, size.width, 40)] autorelease];
    CGRect  frame = infoView.frame;

    frame.origin.y = _fLastYPos;

    float   fGap = (infoView.frame.size.width - PROFILE_PHOTO_ITEM_WIDTH*3)/4;
    int     nRealCount = 0;
    
    //======================================================//
    
    for( int i = 0; i < [items count]; i++ )
    {
        
        
//        NSString*   name = [items objectAtIndex:i];
//        NSLog(@"txtInfo===%@\ntype====%@\nstrTitle=====%@",txtInfo,type,strTitle);
#if 1
        //international start
        NSString *name = nil;
        NSString *names = nil;//zh_hant name
        
        //        NSLog(@"%@",[eSyncronyAppDelegate sharedInstance].language);
        
        if ([[eSyncronyAppDelegate sharedInstance].language hasPrefix:@"zh-Hant"]||[[eSyncronyAppDelegate sharedInstance].language hasPrefix:@"zh-HK"]) {
            
            NSString *_strPath =[[NSBundle mainBundle] pathForResource:@"ethnicity_zhHant" ofType:@"plist"];
//            NSString *_strPath =[[NSBundle mainBundle] pathForResource:@"ethnicity_zhHant" ofType:@"xml"];
            NSDictionary *_dicCountry = [NSDictionary dictionaryWithContentsOfFile:_strPath];
            NSDictionary *p_ethnicity=[_dicCountry objectForKey:type];
            
            //name = NSLocalizedString([items objectAtIndex:i],);
            names = [p_ethnicity objectForKey:[items objectAtIndex:i]];
            name = [items objectAtIndex:i];
            
        }
        else
        {
            name = [items objectAtIndex:i];
            
        }
        //international end
#endif
        //NSLog(@"name == %@",name);
        
        if( [name isEqualToString:@""] )
            continue;

//        UIProfilePhotoItemView* itemView = [UIProfilePhotoItemView createWithType:type name:name];

#if 1
        UIProfilePhotoItemView* itemView;
        if ([[eSyncronyAppDelegate sharedInstance].language hasPrefix:@"zh-Hant"]||[[eSyncronyAppDelegate sharedInstance].language hasPrefix:@"zh-HK"]) {//151204
            
            itemView = [UIProfilePhotoItemView createWithType:type name:name andZhhantName:names];
        }else//en
        {
            itemView = [UIProfilePhotoItemView createWithType:type name:name];
        }
#endif
        
        CGRect  rect = itemView.frame;
            
        rect.origin.x = fGap + (nRealCount%3)*(PROFILE_PHOTO_ITEM_WIDTH+fGap);
        rect.origin.y = fGap + (nRealCount/3)*(PROFILE_PHOTO_ITEM_HEIGHT+fGap);
        itemView.frame = rect;
            
        [infoView addSubview:itemView];

        nRealCount++;
    }
        
    int nLines = (nRealCount+2)/3;
    frame.size.height = fGap + nLines*(PROFILE_PHOTO_ITEM_HEIGHT+fGap);

    _fLastYPos += frame.size.height;
    
    infoView.frame = frame;
    [scrlView addSubview:infoView];
}

- (void)showInterestInfo:(NSArray*)arrayInterestInfo//interests
{
    if( arrayInterestInfo == nil )
        return;
    
    if( ![arrayInterestInfo isKindOfClass:[NSArray class]] )
        return;

    if( arrayInterestInfo.count==0 )
        return;
    
    [self addSubTitleView:NSLocalizedString(@"INTEREST & HOBBIES",@"ProfileBaseView")];
    
    [self addOneInterestInfo:[arrayInterestInfo objectAtIndex:0] type:@"Food" ofTitle:NSLocalizedString(@"Favourite foods",@"ProfileBaseView")];
    [self addOneInterestInfo:[arrayInterestInfo objectAtIndex:1] type:@"Hobby" ofTitle:NSLocalizedString(@"Hobbies",@"ProfileBaseView")];
    [self addOneInterestInfo:[arrayInterestInfo objectAtIndex:2] type:@"Film" ofTitle:NSLocalizedString(@"Films",@"ProfileBaseView")];
    [self addOneInterestInfo:[arrayInterestInfo objectAtIndex:3] type:@"Music" ofTitle:NSLocalizedString(@"Music",@"ProfileBaseView")];
    [self addOneInterestInfo:[arrayInterestInfo objectAtIndex:4] type:@"Travel" ofTitle:NSLocalizedString(@"Holiday Destination",@"ProfileBaseView")];
    [self addOneInterestInfo:[arrayInterestInfo objectAtIndex:5] type:@"sport" ofTitle:NSLocalizedString(@"Sport",@"ProfileBaseView")];
    [self addOneInterestInfo:[arrayInterestInfo objectAtIndex:6] type:@"OwnPets" ofTitle:NSLocalizedString(@"Pet",@"ProfileBaseView")];
    
}

- (void)addOneStoryItemTitle:(NSString*)strTitle content:(NSString*)strContent//story
{
    CGSize  size = scrlView.frame.size;
    
    UILabel *lblTitle = [[[UILabel alloc] initWithFrame:CGRectMake(10, _fLastYPos+10, size.width-20, 15)] autorelease];
    
    if ([[eSyncronyAppDelegate sharedInstance].language hasPrefix:@"zh-Hant"]||[[eSyncronyAppDelegate sharedInstance].language hasPrefix:@"zh-HK"]) {
//        lblTitle.font = [UIFont fontWithName:@"CALIBRI" size:15];
    }
    else
        lblTitle.font = [UIFont fontWithName:@"HelveticaNeueLTStd-MdCn" size:15];

    lblTitle.backgroundColor = [UIColor clearColor];
    
    lblTitle.textColor = CLASS_TITLE_VIEW_TEXT_COLOR;
    lblTitle.text = strTitle;
    lblTitle.numberOfLines = 0;//append for lbltitle
    [scrlView addSubview:lblTitle];
    [lblTitle sizeToFit];
    
    _fLastYPos += 10 + lblTitle.frame.size.height;
    
    UILabel *lblContent = [[[UILabel alloc] initWithFrame:CGRectMake(10, _fLastYPos+10, size.width-20, 15)] autorelease];
    if ([[eSyncronyAppDelegate sharedInstance].language hasPrefix:@"zh-Hant"]||[[eSyncronyAppDelegate sharedInstance].language hasPrefix:@"zh-HK"]) {
        lblContent.font = [UIFont fontWithName:@"CALIBRI" size:15];
    }
    else
        lblContent.font = [UIFont fontWithName:@"HelveticaNeueLTStd-MdCn" size:15];

    lblContent.backgroundColor = [UIColor clearColor];
    lblContent.textColor = TITLE_TEXT_COLLOR;
    if (![strTitle isEqualToString:NSLocalizedString(@"The most influential person in my life is:",@"ProfileBaseView")]) {
        strContent = [ strContent stringByReplacingOccurrencesOfString:@"," withString:@", "];
    }
    
    lblContent.text = strContent;
    lblContent.numberOfLines = 100;
    [scrlView addSubview:lblContent];

//    CGSize maxSize = CGSizeMake( size.width-30, 1000 );
//    CGSize contentSize = [strContent sizeWithFont:lblContent.font constrainedToSize:maxSize];
    CGRect rect = lblContent.frame;
    CGSize size2 = CGSizeMake(size.width-20, 1000);
    CGSize actualsize = [lblContent.text sizeWithFont:lblContent.font constrainedToSize:size2 lineBreakMode:NSLineBreakByTruncatingTail];
    
    rect.size = actualsize;
    rect.size.height = rect.size.height+4;
    lblContent.frame = rect;
    lblContent.frame = CGRectMake(10, _fLastYPos+10, rect.size.width, rect.size.height );
    
    _fLastYPos += 18 + rect.size.height;
    
    UIView  *splitView = [[[UIView alloc] initWithFrame:CGRectMake(3, _fLastYPos-1, size.width-6, 1)] autorelease];
    splitView.backgroundColor = [UIColor lightGrayColor];
    [scrlView addSubview:splitView];
}

- (void)showStoryInfo:(NSDictionary*)dicStoryInfo
{
    if( dicStoryInfo == nil )
        return;

    [self addSubTitleView:NSLocalizedString(@"MY STORY",@"ProfileBaseView")];
    
    if( ![dicStoryInfo isKindOfClass:[NSDictionary class]] )
    {
        [self addOneStoryItemTitle:NSLocalizedString(@"I am thankful for:",@"ProfileBaseView") content:@""];
        [self addOneStoryItemTitle:NSLocalizedString(@"The most influential person in my life is:",@"ProfileBaseView") content:@""];
        [self addOneStoryItemTitle:NSLocalizedString(@"My friends describes me as:",@"ProfileBaseView") content:@""];
        //append VIP story,title need changed and transaltion
        [self addOneStoryItemTitle:NSLocalizedString(@"For me,a successful relationship is:",@"ProfileBaseView") content:@""];
        [self addOneStoryItemTitle:NSLocalizedString(@"It's important i think make a relationship works is:",@"ProfileBaseView") content:@""];
        [self addOneStoryItemTitle:NSLocalizedString(@"My ideal relationship is:",@"ProfileBaseView") content:@""];
        
        return;
    }

    [self addOneStoryItemTitle:NSLocalizedString(@"I am thankful for:",@"ProfileBaseView") content:[dicStoryInfo objectForKey:@"story1"]];
    [self addOneStoryItemTitle:NSLocalizedString(@"The most influential person in my life is:",@"ProfileBaseView") content:[dicStoryInfo objectForKey:@"story2"]];
    [self addOneStoryItemTitle:NSLocalizedString(@"My friends describes me as:",@"ProfileBaseView") content:[dicStoryInfo objectForKey:@"story3"]];
    //append VIP story
    [self addOneStoryItemTitle:NSLocalizedString(@"For me,a successful relationship is:",@"ProfileBaseView") content:[dicStoryInfo objectForKey:@"story4"]];
    [self addOneStoryItemTitle:NSLocalizedString(@"It's important i think make a relationship works is:",@"ProfileBaseView") content:[dicStoryInfo objectForKey:@"story5"]];
    [self addOneStoryItemTitle:NSLocalizedString(@"My ideal relationship is:",@"ProfileBaseView") content:[dicStoryInfo objectForKey:@"story6"]];
    
}

- (void)dealloc
{
    [scrlView release];
    [contentsView release];
    
    [super dealloc];
}

@end


#import "Global.h"
#import "UtilComm.h"
#import "ImageUtil.h"
#import "HelvMdCnLabel.h"
#import "UIProfilePhotoItemView.h"
#import "UIImageView+WebCache.h"
//test
@implementation UIProfilePhotoItemView

@synthesize imageView;
+ (id)createWithType:(NSString*)type name:(NSString*)name
{
    UIProfilePhotoItemView* view = [[[UIProfilePhotoItemView alloc] initWithFrame:CGRectMake(0, 0, PROFILE_PHOTO_ITEM_WIDTH, PROFILE_PHOTO_ITEM_HEIGHT)] autorelease];
    
    [view setType:type name:name];
    
    return view;
}

//==============  append zh_hant  ====0713=============//
+ (id)createWithType:(NSString*)type name:(NSString*)name andZhhantName:(NSString*)names
{
    UIProfilePhotoItemView* view = [[[UIProfilePhotoItemView alloc] initWithFrame:CGRectMake(0, 0, PROFILE_PHOTO_ITEM_WIDTH, PROFILE_PHOTO_ITEM_HEIGHT)] autorelease];
    
    [view setType:type name:name andZhhantName:names];
    
    return view;
}

- (void)setType:(NSString*)type name:(NSString*)name andZhhantName:(NSString*)names
{
    imageView = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, PROFILE_PHOTO_ITEM_WIDTH, PROFILE_PHOTO_ITEM_WIDTH)] autorelease];
    imageView.backgroundColor = [UIColor clearColor];
    [self addSubview:imageView];
//    imageView.image = [UIImage imageNamed:@"no_image.png"];
    
    HelvMdCnLabel*  label = [[[HelvMdCnLabel alloc] initWithFrame:CGRectMake(0, (PROFILE_PHOTO_ITEM_WIDTH+2), PROFILE_PHOTO_ITEM_WIDTH, 28)] autorelease];
    [self addSubview:label];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:@"HelveticaNeueLTStd-BdCn" size:13];
    label.textColor = TITLE_TEXT_COLLOR;
    label.text = names;
    label.numberOfLines = 2;
    label.backgroundColor = [UIColor clearColor];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,
                                             (unsigned long)NULL), ^(void) {
        [self downloadProfilePhotoType:type named:name];
    });
}
//================================================//

- (void)setType:(NSString*)type name:(NSString*)name
{
    imageView = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, PROFILE_PHOTO_ITEM_WIDTH, PROFILE_PHOTO_ITEM_WIDTH)] autorelease];
    [self addSubview:imageView];
//    imageView.image = [UIImage imageNamed:@"no_image.png"];
    
    HelvMdCnLabel*  label = [[[HelvMdCnLabel alloc] initWithFrame:CGRectMake(0, (PROFILE_PHOTO_ITEM_WIDTH+2), PROFILE_PHOTO_ITEM_WIDTH, 28)] autorelease];
    [self addSubview:label];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:@"HelveticaNeueLTStd-BdCn" size:13];
    label.textColor = TITLE_TEXT_COLLOR;
    label.text = name;
    label.numberOfLines = 2;
    label.backgroundColor = [UIColor clearColor];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,
                                             (unsigned long)NULL), ^(void) {
        [self downloadProfilePhotoType:type named:name];
    });
    
  
}
- (void)downloadProfilePhotoType:(NSString*)type named:(NSString*)name
{
    NSString*   fileName = [name stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    NSString*   imageURL = [NSString stringWithFormat:@"%@%@/%@.jpg", WEBSERVICE_IMAGE_BASEURL, type, fileName];

//        NSString*   imagePath = [ImageUtil loadImagePathFromURL:imageURL];
    [self performSelectorOnMainThread:@selector(ProfilePhotoItem_onDownLoadImage:) withObject:imageURL waitUntilDone:NO];
}

- (void)ProfilePhotoItem_onDownLoadImage:(NSString*)imagePath
{

    NSURL *fileURL= [NSURL URLWithString:imagePath];
//    NSLog(@"fileURL == %@",fileURL);
    [imageView sd_setImageWithURL:fileURL placeholderImage:[UIImage imageNamed:@"no_image"]];
    
//    UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
//    if( image != nil )
//        imageView.image = image;
    

}

@end

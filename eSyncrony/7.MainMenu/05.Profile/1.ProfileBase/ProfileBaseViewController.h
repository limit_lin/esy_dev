
#import <UIKit/UIKit.h>
#import "HttpBaseViewController.h"

@interface ProfileBaseViewController : HttpBaseViewController
{
    IBOutlet    UIScrollView   *scrlView;
    IBOutlet    UIView         *contentsView;
    
    BOOL    bGotoUpgrade;
    float   _fLastYPos;
    int comInterstNum;
    NSRange subRange;
}

- (void)initContentViews;

- (void)addSubTitleView:(NSString*)strTitle;

- (void)addClassTitleView:(NSString*)strTitle;

- (void)addContentToRight:(NSString*)strContent;

- (void)addOneItemTitle:(NSString*)strTitle content:(NSString*)strContent;

- (void)showCommonInterest:(NSDictionary*)dicCommonInterest;

- (void)addOneInterestInfo:(NSString*)txtInfo type:(NSString*)type ofTitle:(NSString*)strTitle;

- (void)showInterestInfo:(NSArray*)arrayInterestInfo;

- (void)addOneStoryItemTitle:(NSString*)strTitle content:(NSString*)strContent;

- (void)showStoryInfo:(NSDictionary*)dicStoryInfo;

@end

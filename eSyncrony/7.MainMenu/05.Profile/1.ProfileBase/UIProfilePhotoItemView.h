
#import <UIKit/UIKit.h>

#define PROFILE_PHOTO_ITEM_WIDTH        80
#define PROFILE_PHOTO_ITEM_HEIGHT       110

@interface UIProfilePhotoItemView : UIView
{
    
}

@property(nonatomic,strong)UIImageView     *imageView;
+ (id)createWithType:(NSString*)type name:(NSString*)name;
+ (id)createWithType:(NSString*)type name:(NSString*)name andZhhantName:(NSString*)names;//append zh_hant 0713

@end

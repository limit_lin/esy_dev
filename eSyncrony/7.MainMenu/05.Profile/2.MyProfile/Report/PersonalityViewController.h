
#import <UIKit/UIKit.h>

@interface PersonalityViewController : GAITrackedViewController
{
    IBOutlet UIScrollView   *scrlView;
    IBOutlet UIView         *contentsView;

    IBOutlet UILabel        *lblTitle;
    IBOutlet UILabel        *nameLabel;
    IBOutlet UILabel        *discTxtView1;
    
    IBOutlet UIImageView    *ImgContrast;
    
    IBOutlet UIImageView    *imgDivider2;
    IBOutlet UILabel        *Title2;
    IBOutlet UILabel        *Descipt2;
    
    IBOutlet UIImageView    *imgDivider3;
    IBOutlet UILabel        *Title3;
    IBOutlet UILabel        *Descipt3;
    
    IBOutlet UIImageView    *imgDivider4;
    IBOutlet UILabel        *Title4;
    IBOutlet UILabel        *Descipt4;
    
    IBOutlet UIImageView    *imgDivider5;
    IBOutlet UILabel        *Title5;
    IBOutlet UILabel        *Descipt5;
}

@end

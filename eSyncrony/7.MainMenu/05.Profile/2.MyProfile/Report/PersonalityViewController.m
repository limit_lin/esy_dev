
#import "PersonalityViewController.h"

#import "DSActivityView.h"
#import "Global.h"
#import "UtilComm.h"
#import "ImageUtil.h"

#import "eSyncronyAppDelegate.h"
#import "mainMenuViewController.h"

@interface PersonalityViewController ()

@end

@implementation PersonalityViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"PersonalityReportView";
    [scrlView addSubview:contentsView];
    scrlView.contentSize = contentsView.frame.size;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    ImgContrast.layer.borderWidth = 4;
    ImgContrast.layer.borderColor = [UIColor whiteColor].CGColor;

    [self performSelector:@selector(ShowActivity) withObject:nil afterDelay:0.1];
}

- (void)ShowActivity
{
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(Personality_loadReport) withObject:nil afterDelay:0.1];
}
- (void)Personality_loadReport
{
    NSDictionary* result = [UtilComm personalityTypeReport:nil];
    [DSBezelActivityView removeView];
    
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Error"];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
        return;
    }
    
    NSArray *dicArray = [response objectForKey:@"item"];
    
    nameLabel.text = dicArray[0];
    nameLabel.text = nameLabel.text.uppercaseString;
    
    // Description
//    NSString *str1 = dicArray[1];
//    discTxtView1.text = [NSString stringWithFormat:@"%@\n",str1];
    
#if 1
    NSString *str1 = dicArray[1];
    NSString *str2 = dicArray[2];
    NSArray *arr = [str2 componentsSeparatedByString:@"\t"];
    discTxtView1.text = [NSString stringWithFormat:@"%@\n\n%@",str1,arr[0]];
#endif
    
    CGSize maximumLabelSize = CGSizeMake(270, FLT_MAX);
    CGSize expectedLabelSize = [discTxtView1.text sizeWithFont:discTxtView1.font constrainedToSize:maximumLabelSize lineBreakMode:discTxtView1.lineBreakMode];
    CGRect newFrame = discTxtView1.frame;
    newFrame.size.height = expectedLabelSize.height + 10;
    discTxtView1.frame = newFrame;
    
    // Readiness
    NSDictionary *Dict2 = dicArray[3];
    NSArray *Array2 = [Dict2 objectForKey:@"anon"];
    Descipt2.text = Array2[1];
    
    CGSize expectedLabelSize2 = [Array2[1] sizeWithFont:Descipt2.font constrainedToSize:maximumLabelSize lineBreakMode:Descipt2.lineBreakMode];
    CGRect newFrame2 = Descipt2.frame;
    newFrame2.size.height = expectedLabelSize2.height + 10;
    Descipt2.frame = newFrame2;
    
    imgDivider2.frame = CGRectMake(imgDivider2.frame.origin.x, discTxtView1.frame.origin.y + + discTxtView1.frame.size.height + 7, imgDivider2.frame.size.width, imgDivider2.frame.size.height);
    Title2.frame = CGRectMake(Title2.frame.origin.x, imgDivider2.frame.origin.y + imgDivider2.frame.size.height + 7, Title2.frame.size.width, Title2.frame.size.height);
    Descipt2.frame = CGRectMake(Descipt2.frame.origin.x, Title2.frame.origin.y + Title2.frame.size.height + 5, Descipt2.frame.size.width, Descipt2.frame.size.height);
    
    // Culture
    NSDictionary *Dict3 = dicArray[4];
    NSArray *Array3 = [Dict3 objectForKey:@"anon"];
    Descipt3.text = Array3[1];
    
    CGSize expectedLabelSize3 = [Array3[1] sizeWithFont:Descipt3.font constrainedToSize:maximumLabelSize lineBreakMode:Descipt3.lineBreakMode];
    CGRect newFrame3 = Descipt3.frame;
    newFrame3.size.height = expectedLabelSize3.height + 10;
    Descipt3.frame = newFrame3;
    
    imgDivider3.frame = CGRectMake(imgDivider3.frame.origin.x, Descipt2.frame.origin.y + Descipt2.frame.size.height + 7, imgDivider3.frame.size.width, imgDivider3.frame.size.height);
    Title3.frame = CGRectMake(Title3.frame.origin.x, imgDivider3.frame.origin.y + imgDivider3.frame.size.height + 7, Title3.frame.size.width, Title3.frame.size.height);
    Descipt3.frame = CGRectMake(Descipt3.frame.origin.x, Title3.frame.origin.y + Title3.frame.size.height + 5, Descipt3.frame.size.width, Descipt3.frame.size.height);
    
    // Money
    NSDictionary *Dict4 = dicArray[5];
    NSArray *Array4 = [Dict4 objectForKey:@"anon"];
    Descipt4.text = Array4[1];
    
    CGSize expectedLabelSize4 = [Array4[1] sizeWithFont:Descipt3.font constrainedToSize:maximumLabelSize lineBreakMode:Descipt4.lineBreakMode];
    CGRect newFrame4 = Descipt4.frame;
    newFrame4.size.height = expectedLabelSize4.height + 10;
    Descipt4.frame = newFrame4;
    
    imgDivider4.frame = CGRectMake(imgDivider4.frame.origin.x, Descipt3.frame.origin.y + Descipt3.frame.size.height + 7, imgDivider4.frame.size.width, imgDivider4.frame.size.height);
    Title4.frame = CGRectMake(Title4.frame.origin.x, imgDivider4.frame.origin.y + imgDivider4.frame.size.height + 7, Title4.frame.size.width, Title4.frame.size.height);
    Descipt4.frame = CGRectMake(Descipt4.frame.origin.x, Title4.frame.origin.y + Title4.frame.size.height + 5, Descipt4.frame.size.width, Descipt4.frame.size.height);
    
    // Openness
    NSDictionary *Dict5 = dicArray[6];
    NSArray *Array5 = [Dict5 objectForKey:@"anon"];
    Descipt5.text = Array5[1];
    
    CGSize expectedLabelSize5 = [Array5[1] sizeWithFont:Descipt4.font constrainedToSize:maximumLabelSize lineBreakMode:Descipt5.lineBreakMode];
    CGRect newFrame5 = Descipt5.frame;
    newFrame5.size.height = expectedLabelSize5.height + 10;
    Descipt5.frame = newFrame5;
    
    imgDivider5.frame = CGRectMake(imgDivider5.frame.origin.x, Descipt4.frame.origin.y + Descipt4.frame.size.height + 7, imgDivider5.frame.size.width, imgDivider5.frame.size.height);
    Title5.frame = CGRectMake(Title5.frame.origin.x, imgDivider5.frame.origin.y + imgDivider5.frame.size.height + 7, Title5.frame.size.width, Title5.frame.size.height);
    Descipt5.frame = CGRectMake(Descipt5.frame.origin.x, Title5.frame.origin.y + Title5.frame.size.height + 5, Descipt5.frame.size.width, Descipt5.frame.size.height);
    
    contentsView.frame = CGRectMake(contentsView.frame.origin.x, contentsView.frame.origin.y, contentsView.frame.size.width, Descipt5.frame.origin.y + Descipt5.frame.size.height + 10 );
    
    scrlView.contentSize = contentsView.frame.size;
}
//- (void)Personality_loadReport
//{
//    NSDictionary* result = [UtilComm personalityTypeReport:nil];
//    [DSBezelActivityView removeView];
//    
//    if( result == nil )
//    {
//        //[ErrorProc alertToCheckInternetStateTitle:@"Error"];
//        return;
//    }
//    
//    id response = [result objectForKey:@"response"];
//    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
//    {
//        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
//        return;
//    }
//    
//    NSArray *dicArray = [response objectForKey:@"item"];
//    
//    nameLabel.text = NSLocalizedString(dicArray[0],@"MatchPersonalityView");
//    nameLabel.text = nameLabel.text.uppercaseString;
//    
//    // Description
//    NSString *str1 = dicArray[1];
//    discTxtView1.text = [NSString stringWithFormat:@"%@\n",NSLocalizedString(str1,@"MatchPersonalityView")];
//    
//    #if 0
//    NSString *str1 = dicArray[1];
//    NSString *str2 = dicArray[2];
//    NSArray *arr = [str2 componentsSeparatedByString:@"\t"];
//    discTxtView1.text = [NSString stringWithFormat:@"%@\n\n%@",NSLocalizedString(str1,@"MatchPersonalityView"),NSLocalizedString(arr[0],@"MatchPersonalityView")];
//    #endif
//    
//    CGSize maximumLabelSize = CGSizeMake(270, FLT_MAX);
//    CGSize expectedLabelSize = [discTxtView1.text sizeWithFont:discTxtView1.font constrainedToSize:maximumLabelSize lineBreakMode:discTxtView1.lineBreakMode];
//    CGRect newFrame = discTxtView1.frame;
//    newFrame.size.height = expectedLabelSize.height + 10;
//    discTxtView1.frame = newFrame;
//
//    // Readiness
//    NSDictionary *Dict2 = dicArray[3];
//    NSArray *Array2 = [Dict2 objectForKey:@"anon"];
//    Descipt2.text = NSLocalizedString(Array2[1],@"MatchPersonalityView");
//    
//    CGSize expectedLabelSize2 = [Array2[1] sizeWithFont:Descipt2.font constrainedToSize:maximumLabelSize lineBreakMode:Descipt2.lineBreakMode];
//    CGRect newFrame2 = Descipt2.frame;
//    newFrame2.size.height = expectedLabelSize2.height + 10;
//    Descipt2.frame = newFrame2;
//    
//    imgDivider2.frame = CGRectMake(imgDivider2.frame.origin.x, discTxtView1.frame.origin.y + + discTxtView1.frame.size.height + 7, imgDivider2.frame.size.width, imgDivider2.frame.size.height);
//    Title2.frame = CGRectMake(Title2.frame.origin.x, imgDivider2.frame.origin.y + imgDivider2.frame.size.height + 7, Title2.frame.size.width, Title2.frame.size.height);
//    Descipt2.frame = CGRectMake(Descipt2.frame.origin.x, Title2.frame.origin.y + Title2.frame.size.height + 5, Descipt2.frame.size.width, Descipt2.frame.size.height);
//
//    // Culture
//    NSDictionary *Dict3 = dicArray[4];
//    NSArray *Array3 = [Dict3 objectForKey:@"anon"];
//    Descipt3.text = NSLocalizedString(Array3[1],@"MatchPersonalityView");
//    
//    CGSize expectedLabelSize3 = [Array3[1] sizeWithFont:Descipt3.font constrainedToSize:maximumLabelSize lineBreakMode:Descipt3.lineBreakMode];
//    CGRect newFrame3 = Descipt3.frame;
//    newFrame3.size.height = expectedLabelSize3.height + 10;
//    Descipt3.frame = newFrame3;
//    
//    imgDivider3.frame = CGRectMake(imgDivider3.frame.origin.x, Descipt2.frame.origin.y + Descipt2.frame.size.height + 7, imgDivider3.frame.size.width, imgDivider3.frame.size.height);
//    Title3.frame = CGRectMake(Title3.frame.origin.x, imgDivider3.frame.origin.y + imgDivider3.frame.size.height + 7, Title3.frame.size.width, Title3.frame.size.height);
//    Descipt3.frame = CGRectMake(Descipt3.frame.origin.x, Title3.frame.origin.y + Title3.frame.size.height + 5, Descipt3.frame.size.width, Descipt3.frame.size.height);
//   
//    // Money
//    NSDictionary *Dict4 = dicArray[5];
//    NSArray *Array4 = [Dict4 objectForKey:@"anon"];
//    Descipt4.text = NSLocalizedString(Array4[1],@"MatchPersonalityView");
//    
//    CGSize expectedLabelSize4 = [Array4[1] sizeWithFont:Descipt3.font constrainedToSize:maximumLabelSize lineBreakMode:Descipt4.lineBreakMode];
//    CGRect newFrame4 = Descipt4.frame;
//    newFrame4.size.height = expectedLabelSize4.height + 10;
//    Descipt4.frame = newFrame4;
//    
//    imgDivider4.frame = CGRectMake(imgDivider4.frame.origin.x, Descipt3.frame.origin.y + Descipt3.frame.size.height + 7, imgDivider4.frame.size.width, imgDivider4.frame.size.height);
//    Title4.frame = CGRectMake(Title4.frame.origin.x, imgDivider4.frame.origin.y + imgDivider4.frame.size.height + 7, Title4.frame.size.width, Title4.frame.size.height);
//    Descipt4.frame = CGRectMake(Descipt4.frame.origin.x, Title4.frame.origin.y + Title4.frame.size.height + 5, Descipt4.frame.size.width, Descipt4.frame.size.height);
//    
//    // Openness
//    NSDictionary *Dict5 = dicArray[6];
//    NSArray *Array5 = [Dict5 objectForKey:@"anon"];
//    Descipt5.text = NSLocalizedString(Array5[1],@"MatchPersonalityView");
//    
//    CGSize expectedLabelSize5 = [Array5[1] sizeWithFont:Descipt4.font constrainedToSize:maximumLabelSize lineBreakMode:Descipt5.lineBreakMode];
//    CGRect newFrame5 = Descipt5.frame;
//    newFrame5.size.height = expectedLabelSize5.height + 10;
//    Descipt5.frame = newFrame5;
//    
//    imgDivider5.frame = CGRectMake(imgDivider5.frame.origin.x, Descipt4.frame.origin.y + Descipt4.frame.size.height + 7, imgDivider5.frame.size.width, imgDivider5.frame.size.height);
//    Title5.frame = CGRectMake(Title5.frame.origin.x, imgDivider5.frame.origin.y + imgDivider5.frame.size.height + 7, Title5.frame.size.width, Title5.frame.size.height);
//    Descipt5.frame = CGRectMake(Descipt5.frame.origin.x, Title5.frame.origin.y + Title5.frame.size.height + 5, Descipt5.frame.size.width, Descipt5.frame.size.height);
//    
//    contentsView.frame = CGRectMake(contentsView.frame.origin.x, contentsView.frame.origin.y, contentsView.frame.size.width, Descipt5.frame.origin.y + Descipt5.frame.size.height + 10 );
//    
//    scrlView.contentSize = contentsView.frame.size;
//}

- (void)dealloc
{
    [scrlView release];
    [contentsView release];
    
    [nameLabel release];
    [discTxtView1 release];
    
    [imgDivider2 release];
    [Title2 release];
    [Descipt2 release];
    
    [imgDivider3 release];
    [Title3 release];
    [Descipt3 release];
    
    [imgDivider4 release];
    [Title4 release];
    [Descipt4 release];
    
    [imgDivider5 release];
    [Title5 release];
    [Descipt5 release];
    
    [super dealloc];
}

@end


#import <UIKit/UIKit.h>
#import "DataSelectViewController.h"

@class ourSuccessStoriesViewController;

@interface MyAccountViewController : GAITrackedViewController <UITextFieldDelegate, DataSelectViewDelegate>
{
    IBOutlet UIScrollView   *scrlView;
    IBOutlet UIView         *contentsView;
    
    IBOutlet UIView         *viewDateNums;
    IBOutlet UIView         *viewNotice;
    
    IBOutlet UILabel        *leftDatesLabel;

    IBOutlet UILabel        *forthLabel;
    IBOutlet UILabel        *backLabel;
    
    IBOutlet UIDatePicker   *datePiker;
    IBOutlet UITextField    *accountNumTF;
    IBOutlet UITextField    *diplayNameTF;
    IBOutlet UITextField    *phoneNumTF;
    IBOutlet UITextField    *newPwTF;
    IBOutlet UITextField    *repeatPwTF;
    
    // Other Controllers
    IBOutlet UILabel        *fromyour;
    IBOutlet UILabel        *packages;
    BOOL                    bIsFirst;
    
    NSArray*    arrHeaderChars;
    NSArray*    arrFooterChars;
    
    int     nSelHeader, nSelFooter;
}

@property (nonatomic, assign) BOOL bPsdChangeMode;
@property (nonatomic, assign) int   countryId;
@property (retain, nonatomic) IBOutlet UIImageView *imageNric;
@property (retain, nonatomic) IBOutlet UILabel *lblNric;
@property (retain, nonatomic) IBOutlet UILabel *lblNricContent;

@property (strong, nonatomic) IBOutlet UIImageView *imageText1;
@property (strong, nonatomic) IBOutlet UIButton *btnHeader;
@property (strong, nonatomic) IBOutlet UIButton *btnFooter;
@property (strong, nonatomic) IBOutlet UIImageView *imageText2;
@property (strong, nonatomic) IBOutlet UIImageView *imageText3;
@property (strong, nonatomic) IBOutlet UIImageView *imageText4;
@property (strong, nonatomic) IBOutlet UITextField *txtNumber1;
@property (strong, nonatomic) IBOutlet UITextField *txtNumber2;
@property (strong, nonatomic) IBOutlet UITextField *txtNumber3;
@property (strong, nonatomic) IBOutlet UILabel *lbl1;
@property (strong, nonatomic) IBOutlet UILabel *lbl2;

- (IBAction)didClickSubscriptionPlansBtn:(id)sender;
- (IBAction)didClickSuccessStroyBtn:(id)sender;
- (IBAction)didClickForthLabelBtn:(id)sender;
- (IBAction)didClickBackLabelBtn:(id)sender;
- (IBAction)didClickUpdateBtn:(id)sender;
- (IBAction)didClickSaveBtn:(id)sender;

- (void)initSetting;

@end



#import "MyAccountViewController.h"
#import "ourSuccessStoriesViewController.h"

#import "DSActivityView.h"
#import "Global.h"
#import "UtilComm.h"

#import "mainMenuViewController.h"
#import "eSyncronyAppDelegate.h"

@interface MyAccountViewController ()

@end

@implementation MyAccountViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"MyAccountView";
    accountNumTF.delegate = self;
    diplayNameTF.delegate = self;
    phoneNumTF.delegate = self;
    newPwTF.delegate = self;
    repeatPwTF.delegate = self;
    
    [scrlView addSubview:contentsView];
    scrlView.contentSize = contentsView.frame.size;

    UITapGestureRecognizer *singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [contentsView addGestureRecognizer:singleFingerTap];
    
    self.countryId = [eSyncronyAppDelegate sharedInstance].countryId;//repair -1

    NSLog(@"self.countryId == %d",self.countryId);
    switch (self.countryId) {
        case 1:{

            accountNumTF.hidden = YES;
            self.imageText1.hidden = YES;


            forthLabel.hidden = YES;
            backLabel.hidden = YES;

            self.btnHeader.hidden = YES;
            self.btnFooter.hidden = YES;
            self.imageText2.hidden = NO;
            self.imageText3.hidden = NO;
            self.imageText4.hidden = NO;
            self.txtNumber1.hidden = NO;
            self.txtNumber2.hidden = NO;
            self.txtNumber3.hidden = NO;
            self.lbl1.hidden = NO;
            self.lbl2.hidden = NO;
            break;
        }
        case 2:{//hk

            arrHeaderChars = [[NSArray alloc] initWithObjects:@"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z", nil];
            arrFooterChars = [[NSArray alloc] initWithObjects:@"0", @"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"A", nil];
            break;
        }
        case 7://append indonesia
        {
            
            accountNumTF.hidden = NO;
            self.imageText1.hidden = NO;
           // self.imageNric.hidden = NO;
           // self.lblNricContent.hidden = NO;
 
            forthLabel.hidden = YES;
            backLabel.hidden = YES;
            
            self.btnHeader.hidden = YES;
            self.btnFooter.hidden = YES;
            self.imageText2.hidden = YES;
            self.imageText3.hidden = YES;
            self.imageText4.hidden = YES;
            self.txtNumber1.hidden = YES;
            self.txtNumber2.hidden = YES;
            self.txtNumber3.hidden = YES;
            self.lbl1.hidden = YES;
            self.lbl2.hidden = YES;
            break;
            
        }
        case 202://append thailand 0825
        {
            
            accountNumTF.hidden = NO;
            self.imageText1.hidden = NO;
            // self.imageNric.hidden = NO;
            // self.lblNricContent.hidden = NO;
            
            forthLabel.hidden = YES;
            backLabel.hidden = YES;
            
            self.btnHeader.hidden = YES;
            self.btnFooter.hidden = YES;
            self.imageText2.hidden = YES;
            self.imageText3.hidden = YES;
            self.imageText4.hidden = YES;
            self.txtNumber1.hidden = YES;
            self.txtNumber2.hidden = YES;
            self.txtNumber3.hidden = YES;
            self.lbl1.hidden = YES;
            self.lbl2.hidden = YES;
            break;
            
        }
        default:{
            
//            accountNumTF.hidden = NO;
//            self.imageText1.hidden = NO;
//            
//            
//            forthLabel.hidden = NO;
//            backLabel.hidden = NO;
//
//            self.btnHeader.hidden = NO;
//            self.btnFooter.hidden = NO;
//            self.imageText2.hidden = NO;
//            self.imageText3.hidden = NO;
//            self.imageText4.hidden = NO;
//            self.txtNumber1.hidden = NO;
//            self.txtNumber2.hidden = NO;
//            self.txtNumber3.hidden = NO;
//            self.lbl1.hidden = NO;
//            self.lbl2.hidden = NO;
            
            arrHeaderChars = [[NSArray alloc] initWithObjects:@"S", @"T", @"F", @"G", nil];
            arrFooterChars = [[NSArray alloc] initWithObjects:@"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z", nil];
            break;
        }
            
    }
    
    nSelHeader = 0;
    nSelFooter = 0;
    
    [self setDateRange];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    phoneNumTF.enabled = NO;
//    phoneNumTF.enabled = YES;//repair
    
    if( bIsFirst == NO )
    {
        bIsFirst = YES;
        
        
        viewNotice.hidden = YES;
        viewDateNums.hidden = YES;
        
        [self performSelector:@selector(ShowActivity) withObject:nil afterDelay:0.1];

    }
    
    if( self.bPsdChangeMode == YES )
    {
        CGPoint point;
        point.x = 0;
        point.y = scrlView.contentSize.height - scrlView.frame.size.height;
        scrlView.contentOffset = point;
    }
    
    if( [eSyncronyAppDelegate sharedInstance].nIDVerify != 0 )
    {
        accountNumTF.enabled = NO;
        datePiker.enabled = NO;
    }
}

- (void)setDateRange
{
    NSCalendar          *cal = datePiker.calendar;
    //    NSDateComponents    *dateComp= [cal components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:[NSDate date]];
    NSDateComponents *dateComp = [cal components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:[NSDate date]];
    
    dateComp.year -= 21;
    datePiker.maximumDate = [cal dateFromComponents:dateComp];
    
    dateComp.year = 1940;
    dateComp.month = 1;
    dateComp.day = 1;
    
    datePiker.minimumDate = [cal dateFromComponents:dateComp];
}

- (void)ShowActivity
{
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(loadNumberDates) withObject:nil afterDelay:0.1];
}

- (void)loadNumberDates
{
    
    NSDictionary* result = [UtilComm retrieveNumberDatesAndPackage];
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Error"];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [self dismissViewControllerAnimated:NO completion:nil];
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
        return;
    }
    
    NSArray *ListArr = [response objectForKey:@"item"];
    int date = [ListArr[0]intValue];

    if( date == 0 )
    {
        viewDateNums.hidden = YES;
        viewNotice.hidden = NO;
    }
    else if(date>0&&date<10)
    {
        viewDateNums.hidden = NO;
        viewNotice.hidden = YES;
        leftDatesLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Your unlimited date remaining\nand will expire on %@",@"MyAccountView"),ListArr[2]];
        
    }else if(date>10){
        viewDateNums.hidden = NO;
        viewNotice.hidden = YES;
        leftDatesLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Your unlimited date will expire on %@",@"MyAccountView"),ListArr[2]];
    }

    [self loadAccountInfo];
}

- (void)loadAccountInfo
{
    NSDictionary* result = [UtilComm viewAccount];
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Error"];
        return;
    }
    [DSBezelActivityView removeViewAnimated:NO];
    id response = [result objectForKey:@"response"];
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [self dismissViewControllerAnimated:NO completion:nil];
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
        return;
    }
    
    NSDictionary *ListArr = response;

    diplayNameTF.text = [ListArr objectForKey:@"nname"];
    phoneNumTF.text = [ListArr objectForKey:@"handphone"];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    NSDate *dobDate = [[NSDate alloc] init];
    dobDate = [dateFormatter dateFromString:[ListArr objectForKey:@"dob"]];
    [dateFormatter release];
    datePiker.date = dobDate;
    
    [eSyncronyAppDelegate sharedInstance].strNric = [ListArr objectForKey:@"nric"];
    
    if( [[ListArr objectForKey:@"icverify"] isEqualToString:@"Y"] ){
        [eSyncronyAppDelegate sharedInstance].nIDVerify = 1;
    }
    else{
        [eSyncronyAppDelegate sharedInstance].nIDVerify = 0;
    }
    [[eSyncronyAppDelegate sharedInstance]saveLoginInfo];
    [self RestoreNric];
}

- (void)restoreViewPosition
{
    
    [accountNumTF resignFirstResponder];
    [diplayNameTF resignFirstResponder];
    [phoneNumTF resignFirstResponder];
    [newPwTF resignFirstResponder];
    [repeatPwTF resignFirstResponder];
    
    scrlView.contentSize = contentsView.frame.size;
    
    CGPoint point = scrlView.contentOffset;
    
    if( point.y > scrlView.contentSize.height - scrlView.frame.size.height )
    {
        point.y = scrlView.contentSize.height - scrlView.frame.size.height;
        scrlView.contentOffset = point;
    }
}

- (void)tapOnTableView:(UITapGestureRecognizer *)gesture
{
    [self restoreViewPosition];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGRect rect = [textField convertRect:textField.bounds toView:scrlView];
    CGSize size = contentsView.frame.size;
    
    size.height += 250;
    scrlView.contentSize = size;

    rect.size.height += 250;
    
    float   offset = (rect.origin.y + rect.size.height) - scrlView.frame.size.height;
    if( offset > scrlView.contentOffset.y )
    {
        CGPoint point = scrlView.contentOffset;
        point.y = offset;
        scrlView.contentOffset = point;
    }
    //[scrlView scrollRectToVisible:rect animated:NO];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    if( textField.tag == 1004 )
        [repeatPwTF becomeFirstResponder];
//    if (textField.tag == 1003) {
//        [phoneNumTF becomeFirstResponder];
//    }
    else
        [self restoreViewPosition];

    return YES;
}

- (IBAction)didClickSubscriptionPlansBtn:(id)sender
{
    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl procUpgrade];
}

- (IBAction)didClickSuccessStroyBtn:(id)sender
{
    ourSuccessStoriesViewController *ourSucsStrView = [[[ourSuccessStoriesViewController alloc] initWithNibName:@"ourSuccessStoriesViewController" bundle:nil] autorelease];
    ourSucsStrView.isTable = YES;
    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.navigationController pushViewController:ourSucsStrView animated:YES];
}

- (void)RestoreNric
{
    
   NSLog(@"RestoreNric == %d\n",self.countryId);
    
    NSString *strTotal = [eSyncronyAppDelegate sharedInstance].strNric;
    if ([eSyncronyAppDelegate sharedInstance].nIDVerify > 0) {
        datePiker.userInteractionEnabled = NO;
        
        self.imageNric.hidden = NO;
        self.lblNric.hidden = NO;
        self.lblNricContent.hidden = NO;
        
        accountNumTF.hidden = YES;
        self.imageText1.hidden = YES;
        forthLabel.hidden = YES;
        backLabel.hidden = YES;
        self.btnHeader.hidden = YES;
        self.btnFooter.hidden = YES;
        self.imageText2.hidden = YES;
        self.imageText3.hidden = YES;
        self.imageText4.hidden = YES;
        self.txtNumber1.hidden = YES;
        self.txtNumber2.hidden = YES;
        self.txtNumber3.hidden = YES;
        self.lbl1.hidden = YES;
        self.lbl2.hidden = YES;
        
        self.lblNricContent.text = strTotal;
    }else{
        if( strTotal == nil || [strTotal length] < 4 )
        {
            nSelHeader = 0;
            nSelFooter = 0;
            
            forthLabel.text = [arrHeaderChars objectAtIndex:nSelHeader];
            backLabel.text = [arrFooterChars objectAtIndex:nSelFooter];
            
            accountNumTF.text = @"";
            return;
        }
        NSString *strForth;
        NSString *strBack;
        NSString *strNric;
        switch (self.countryId) {
            case 0:{
                strForth = [strTotal substringWithRange:NSMakeRange(0, 1)];
                strBack = [strTotal substringWithRange:NSMakeRange(strTotal.length-1, 1)];
                strNric = [strTotal substringWithRange:NSMakeRange(1, strTotal.length-2)];
                break;
            }
            case 1:{
                
                strForth = [strTotal substringWithRange:NSMakeRange(0, 6)];
                strBack = [strTotal substringWithRange:NSMakeRange(strTotal.length-4, 4)];
                strNric = [strTotal substringWithRange:NSMakeRange(6, 2)];
                break;
            }
            case 7://append Indonesia
            {
                strForth = nil;
                strBack  = nil;
                strNric = [strTotal substringWithRange:NSMakeRange(0, strTotal.length)];
                //NSLog(@"%@",strNric);
                break;
                
            }
            case 202://append 0825
            {
                strForth = nil;
                strBack  = nil;
                strNric = [strTotal substringWithRange:NSMakeRange(0, strTotal.length)];
                NSLog(@"%@",strNric);
                break;
                
            }

            default:{
                strForth = [strTotal substringWithRange:NSMakeRange(0, 1)];
                strBack = [strTotal substringWithRange:NSMakeRange(strTotal.length-2, 1)];
                strNric = [strTotal substringWithRange:NSMakeRange(1, strTotal.length-4)];
                break;
            }
        }
        if (self.countryId!=1) {
            if (self.countryId ==7) {
                
                accountNumTF.text = strNric;
            }
            if (self.countryId == 202) {
                 accountNumTF.text = strNric;
            }
            else
            {
            
                nSelHeader = [self getIndexOfString:strForth inArray:arrHeaderChars];
                nSelFooter = [self getIndexOfString:strBack inArray:arrFooterChars];
                
                forthLabel.text = [arrHeaderChars objectAtIndex:nSelHeader];
                backLabel.text = [arrFooterChars objectAtIndex:nSelFooter];
                
                accountNumTF.text = strNric;
            }

        }else{
            self.txtNumber1.text = strForth;
            self.txtNumber2.text = strNric;
            self.txtNumber3.text = strBack;
        }
    }
 
}

- (void)initSetting
{
    diplayNameTF.text = [eSyncronyAppDelegate sharedInstance].strName;
    
    [self RestoreNric];
}

- (void)DataSelectView:(DataSelectViewController *)dataSelectView didSelectItem:(int)itemIndex withTag:(int)tag
{
    if (self.countryId!=1) {
        
        if( tag == 0 )
        {
            
            nSelHeader = itemIndex;
            forthLabel.text = [arrHeaderChars objectAtIndex:nSelHeader];
        }
        else
        {
            
            nSelFooter = itemIndex;
            backLabel.text = [arrFooterChars objectAtIndex:nSelFooter];
        }
        
        [dataSelectView release];
    }
    
}

- (IBAction)didClickForthLabelBtn:(id)sender
{
    [self restoreViewPosition];
    if( [eSyncronyAppDelegate sharedInstance].nIDVerify != 0 )
        return;
    
    DataSelectViewController*   dataSelView = [DataSelectViewController createWithDataList:arrHeaderChars selectIndex:nSelHeader withTag:0];
    
    dataSelView.delegate = self;
    [self.view addSubview:dataSelView.view];
    dataSelView.view.frame = self.view.bounds;
}

- (IBAction)didClickBackLabelBtn:(id)sender
{
    [self restoreViewPosition];
    if( [eSyncronyAppDelegate sharedInstance].nIDVerify != 0 )
        return;
    
    DataSelectViewController*   dataSelView = [DataSelectViewController createWithDataList:arrFooterChars selectIndex:nSelFooter withTag:1];
    
    dataSelView.delegate = self;
    [self.view addSubview:dataSelView.view];
    dataSelView.view.frame = self.view.bounds;
}

- (IBAction)didClickUpdateBtn:(id)sender
{
    
    [self restoreViewPosition];
    
    NSString *strName = diplayNameTF.text;
    NSString *strHandphone = phoneNumTF.text;
    NSString *strNric = accountNumTF.text;
    if ([eSyncronyAppDelegate sharedInstance].nIDVerify == 0){
        if (self.countryId == 1) {
            if ([self.txtNumber1.text isEqualToString:@""]||[self.txtNumber2.text isEqualToString:@""]||[self.txtNumber3.text isEqualToString:@""]) {
                [ErrorProc alertMessage:NSLocalizedString(@"ID Number is required.",@"MyAccountView") withTitle:NSLocalizedString(@"Field Required",)];
                return;
            }
        }else {
            if( [strNric isEqualToString:@""] )
            {
                [ErrorProc alertMessage:NSLocalizedString(@"Nric is required.",@"MyAccountView") withTitle:NSLocalizedString(@"Field Required",)];
                return;
            }
        }
    }
    
    
    
    if( [strName isEqualToString:@""] )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Name is required.",@"MyAccountView") withTitle:NSLocalizedString(@"Field Required",)];
        return;
    }
    
    if( [strHandphone isEqualToString:@""] )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"MobileNo is required.",@"MyAccountView") withTitle:NSLocalizedString(@"Field Required",)];
        return;
    }
    int countryId = [eSyncronyAppDelegate sharedInstance].countryId;
    NSLog(@"didClickUpdateBtn countryId == %d",countryId);
    
    NSString *countryname;
    NSString *MOBILE;
    //0: Singapore, 1: Malaysia, 2: HONG KONG,7: INDONESIA
    
    switch (countryId) {
        case 0:{
            MOBILE = @"^([89])\\d{7}$";
            countryname = @"Singapore";
            break;
        }
        case 1:{
            MOBILE = @"^(01)\\d{8}$";
            countryname = @"Malaysia";
            break;
        }
        case 7:{
            MOBILE = @"^(08)\\d{7,10}$";//11-->9~12
            countryname = @"Indonesia";
            break;
        }
        case 202:{
            MOBILE = @"^([0])\\d{9}$";
            countryname = @"Thailand";
            break;
        }
        default:{
            MOBILE = @"^([5689])\\d{7}$";
            countryname = @"HONG KONG";
            break;
        }
    }
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    if (![regextestmobile evaluateWithObject:strHandphone]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Info",) message:[NSString stringWithFormat:NSLocalizedString(@"Please fill in the phone number with the correct format for %@ !",@"MyAccountView"),countryname] delegate:self cancelButtonTitle:NSLocalizedString(@"OK",) otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        return;
    }
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(updateNric) withObject:nil afterDelay:0.1];
}

- (void)updateNric
{
    
    if ([eSyncronyAppDelegate sharedInstance].nIDVerify > 0){
        [self UploadingAccount];

        return;
    }
    NSString *strForth;
    NSString *strNric;
    NSString *strBack;
    if (self.countryId == 1) {
        strForth = self.txtNumber1.text;
        strNric = self.txtNumber2.text;
        strBack = self.txtNumber3.text;
    }else if (self.countryId == 7) {

        strForth = nil;
        strNric = accountNumTF.text;
        strBack = nil;
    }else if (self.countryId == 202) {//append 0825
        
        strForth = nil;
        strNric = accountNumTF.text;
        strBack = nil;
    }else{
        strForth = forthLabel.text;
        strNric = accountNumTF.text;
        strBack = backLabel.text;
    }
    
    NSString *strTotal = [NSString stringWithFormat:@"%@%@%@", strForth, strNric, strBack];
    if (self.countryId == 2) {//repair nric S839925(A)
       strTotal = [NSString stringWithFormat:@"%@%@(%@)", strForth, strNric, strBack];
    }
    
    if( [strTotal isEqualToString:[eSyncronyAppDelegate sharedInstance].strNric] )
    {
        [self UploadingAccount];

        return;
    }
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    
    if (self.countryId == 7) {
        [params setObject:strNric forKey:@"nric"];
    }else if (self.countryId == 202) {//0825
        [params setObject:strNric forKey:@"nric"];
    }
    else
    {
        if (strForth == nil) {
            strForth = @"";
        }
        if (strNric == nil) {
            strNric = @"";
        }
        if (strBack == nil) {
            strBack = @"";
        }
        [params setObject:strForth forKey:@"nricp"];
        [params setObject:strNric forKey:@"nric"];
        [params setObject:strBack forKey:@"nrics"];
    }

    NSDictionary*    result = [UtilComm nricVerify:params];
    
//    NSDictionary*    result = [UtilComm nricVerifyNew:params];
    [DSBezelActivityView removeViewAnimated:NO];
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"NRIC Verify Failure"];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    
    if( [response isKindOfClass:[NSString class]] )
    {
        if( [response isEqualToString:@"true"] )
            [self UploadingAccount];
        else
        {
            [ErrorProc alertMessage:response withTitle:@"Nric Verify"];
            [self RestoreNric];
        }
    }
    else
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Error",)];
        [self RestoreNric];
    }
}

- (void)UploadingAccount
{
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSString *strName = diplayNameTF.text;
    NSString *strDob = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:datePiker.date]];
    NSString *strHandphone = phoneNumTF.text;
    
    NSDictionary* result = [UtilComm updateAccount:strName dob:strDob handphone:strHandphone];
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Error"];
        return;
    }
    [DSBezelActivityView removeViewAnimated:NO];
    id response = [result objectForKey:@"response"];
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [self dismissViewControllerAnimated:NO completion:nil];
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
        return;
    }
    else if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"true"] )
    {
        [eSyncronyAppDelegate sharedInstance].strName = strName;
//        [eSyncronyAppDelegate sharedInstance].strHandphone  = strHandphone;
        
        [[eSyncronyAppDelegate sharedInstance] saveLoginInfo];

        [ErrorProc alertMessage:NSLocalizedString(@"Account updated successfully.",@"MyAccountView") withTitle:NSLocalizedString(@"Update Account",@"MyAccountView")];
    }
    else
        [ErrorProc alertMessage:response withTitle:NSLocalizedString(@"eSynchrony",)];
}

- (IBAction)didClickSaveBtn:(id)sender
{
    [self restoreViewPosition];

    NSString    *newPwd = newPwTF.text;
    NSString    *newConfirm = repeatPwTF.text;
    
    if( newPwd.length < 6 )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"You must enter at least 6 characters.",@"MyAccountView") withTitle:NSLocalizedString(@"Error",)];
        return;
    }
    
    if( ![newConfirm isEqualToString:newPwd] )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Password Confirm does not match.",@"MyAccountView") withTitle:NSLocalizedString(@"Error",)];
        return;
    }
    
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Uploading...",)];
    [self performSelector:@selector(updatePwd) withObject:nil afterDelay:0.1];
}

- (void)updatePwd
{
    [DSBezelActivityView removeView];
    
    NSString        *newPwd = newPwTF.text;
    NSDictionary    *result = [UtilComm updatePassword:newPwd];
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Error"];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [self dismissViewControllerAnimated:NO completion:nil];
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
        return;
    }
    
    [eSyncronyAppDelegate sharedInstance].strLoginPassword = response;
    [[eSyncronyAppDelegate sharedInstance] saveLoginInfo];

//    UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ChangePassword Result",@"MyAccountView")
//                                                 message:[NSString stringWithFormat:NSLocalizedString(@"Update Password is %@",@"MyAccountView"), response]
//                                                delegate:nil
//                                       cancelButtonTitle:NSLocalizedString(@"OK",)
//                                       otherButtonTitles:nil];
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Change Password",@"MyAccountView")
                                                 message:[NSString stringWithFormat:NSLocalizedString(@"Your password has been updated",@"MyAccountView"), response]
                                                delegate:nil
                                       cancelButtonTitle:NSLocalizedString(@"OK",)
                                       otherButtonTitles:nil];
    
    [av show];
    [av release];
}

- (int)getIndexOfString:(NSString*)string inArray:(NSArray*)array
{
    for( int i = 0; i < [array count]; i++ )
    {
        if( [string isEqualToString:[array objectAtIndex:i]] )
            return i;
    }
    
    return 0;
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    [self restoreViewPosition];
}

- (void)dealloc
{
    [scrlView release];
    [contentsView release];
    [leftDatesLabel release];

    [forthLabel release];
    [backLabel release];
    [accountNumTF release];
    [diplayNameTF release];
    [datePiker release];
    [phoneNumTF release];
    [newPwTF release];
    [repeatPwTF release];
    
    [arrHeaderChars release];
    [arrFooterChars release];
    
    [_imageText1 release];
    [_btnHeader release];
    [_btnFooter release];
    [_imageText2 release];
    [_imageText3 release];
    [_imageText4 release];
    [_txtNumber1 release];
    [_txtNumber2 release];
    [_txtNumber3 release];
    [_lbl1 release];
    [_lbl2 release];
    
    [_imageNric release];
    [_lblNric release];
    [_lblNricContent release];
    [super dealloc];
}

@end

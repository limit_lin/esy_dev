
#import "EditProfileViewController.h"
#import "EditProfSettingViewController.h"
#import "ProfileViewController.h"
#import "mainMenuViewController.h"

#import "eSyncronyAppDelegate.h"
#import "DSActivityView.h"
#import "XMLParser.h"
#import "Global.h"


@interface EditProfileViewController ()

@end

@implementation EditProfileViewController

@synthesize ethnicityLabel;
@synthesize nationalitLabel;
@synthesize firstLangLabel;
@synthesize otherLangLabel;
@synthesize educationLabel;
@synthesize incomeLabel;
@synthesize professionLabel;
@synthesize travelLabel;
@synthesize exerciseLabel;
@synthesize haveChildLabel;
@synthesize wantChildLabel;
@synthesize smokingLabel;
@synthesize drinkingLabel;
@synthesize religionLabel;
@synthesize bodyLabel;
@synthesize hairLabel;
@synthesize foodsLabel;
@synthesize hobbiesLabel;
@synthesize watchLabel;
@synthesize listenLabel;
@synthesize holidayLabel;
@synthesize petLabel;
@synthesize sportsLabel;

/*
@synthesize handphoneStr;
@synthesize ethnicitySelStr;
@synthesize nationalitSelStr;
@synthesize firstLangSelStr;
@synthesize otherLangSelStr;
@synthesize educationSelStr;
@synthesize incomeSelStr;
@synthesize professionSelStr;
@synthesize travelSelStr;
@synthesize exerciseSelStr;
@synthesize haveChildSelStr;
@synthesize wantChildSelStr;
@synthesize smokingSelStr;
@synthesize drinkingSelStr;
@synthesize religionSelStr;
@synthesize bodySelStr;
@synthesize hairSelStr;
@synthesize foodsSelStr;
@synthesize hobbiesSelStr;
@synthesize watchSelStr;
@synthesize listenSelStr;
@synthesize holidaySelStr;
@synthesize petSelStr;
@synthesize sportsSelStr;
*/

@synthesize editItemArray, editItemType, parent;


+(NSString *)p_stringDeleteString:(NSString *)str
{
    NSMutableString *str1 = [NSMutableString stringWithString:str];
//     NSLog(@"str1 = %@",str1);    
    unichar c = [str1 characterAtIndex:0];
    NSRange range = NSMakeRange(0, 1);
    if (c == ',') {
        [str1 deleteCharactersInRange:range]; 
    }
    NSString *newstr = [NSString stringWithString:str1];
    return newstr;
}

+(NSString *)p_stringTranslation:(NSString *)needTranslationStr andPlistName:(NSString *)plistName inWhereLookFor:(NSDictionary *)p_dic//translate other language bylimit 0716
{
    NSArray*    items = [needTranslationStr componentsSeparatedByString:@","];
    if( items == 0 || [needTranslationStr isEqualToString:@""] )
        return needTranslationStr;
    if ([items count] == 1) {
        return [[p_dic objectForKey:plistName]objectForKey:needTranslationStr];
    }else
    {
        NSDictionary *p_ethnicity=[p_dic objectForKey:plistName];
        NSMutableString *p_content = [[NSMutableString alloc]init];
        NSString *p_toContent = [[NSString alloc]init];
        NSString *name = [[NSString alloc]init];
        for( int i = 0; i < [items count]; i++ )
        {
            NSString *str=[[NSString alloc]init];
            str = [[items objectAtIndex:i] stringByReplacingOccurrencesOfString:@" " withString:@""];
//            str = [[items objectAtIndex:i] stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:@""];
            name =[p_ethnicity objectForKey:str];
//            str = [items objectAtIndex:i];
//            NSLog(@"%@",str);
//            NSMutableString *str1 = [NSMutableString stringWithString:str];
//            unichar c = [str1 characterAtIndex:0];
//            NSRange range = NSMakeRange(0, 1);
//            if (c == ' ') { //此处可以是任何字符
//                [str1 deleteCharactersInRange:range];
//            }
//
//            name =[p_ethnicity objectForKey:str1];
//            NSLog(@"%@",name);
            
//            if( [name isEqualToString:@""]||[name isEqualToString:nil]||[name isEqualToString:NULL])
            if( name.length == 0)
                break;
            [p_content appendString:[NSString stringWithFormat:@",%@",name]];
            
        }
        unichar c = [p_content characterAtIndex:0];
        NSRange range = NSMakeRange(0, 1);
        if (c == ',') {
            [p_content deleteCharactersInRange:range];
        }
        p_toContent = [p_content copy];
//        NSLog(@"%@",p_toContent);
        return p_toContent;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"EditProfileView";
    [scrlView addSubview:contentsView];
    scrlView.contentSize = contentsView.frame.size;
    
    profileSettingViewCtrl = [[EditProfSettingViewController alloc] initWithNibName:@"EditProfSettingViewController" bundle:nil];
    
//   language = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    editItemArray = [[NSMutableArray alloc] init];
    itemArray = [[NSMutableArray alloc] init];

    heightTf.delegate = self;
    weightTf.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self loadProfileInfo];
    
    profileSettingViewCtrl.view.frame = self.view.bounds;
}

- (void)getItemArrayFromPlist:(NSString *)name
{
    if( [name isEqualToString:@"income"] )
    {
        int nCtID = [eSyncronyAppDelegate sharedInstance].countryId;
        
        NSLog(@"Edit profile nCtID == %d",nCtID);
        
        if( nCtID == 0 )
            name = [name stringByAppendingString:@"_sg"];
        else if( nCtID == 1 )
            name = [name stringByAppendingString:@"_my"];
        else if( nCtID == 2 )
            name = [name stringByAppendingString:@"_hk"];
        else if( nCtID == 7)
            name = [name stringByAppendingString:@"_id"];
        else if (nCtID == 202)
            name = [name stringByAppendingString:@"_th"];
        else
            name = [name stringByAppendingString:@"_hk"];
    }
    NSString *_strPath =[[NSBundle mainBundle] pathForResource:name ofType:@"plist"];
    NSDictionary *_dicCountry = [NSDictionary dictionaryWithContentsOfFile:_strPath];
    
    [itemArray removeAllObjects];
    
    [itemArray addObjectsFromArray:[_dicCountry objectForKey:@"Array"]];
}

- (void)getEditItemArrayType:(NSInteger)type
{
    BOOL        bSingleSelect = NO;
    NSString    *str = @"";
    
    switch (type) {
        case EDIT_PROFILE_ETHNICITY:
            bSingleSelect = YES;
            str = ethnicityLabel.text;
            break;
        case EDIT_PROFILE_NATIONALITY:
            bSingleSelect = YES;
            str = nationalitLabel.text;
            break;
        case EDIT_PROFILE_FIRST_LANG:
            bSingleSelect = YES;
            str = firstLangLabel.text;
            break;
        case EDIT_PROFILE_OTHER_LANG:
            str = otherLangLabel.text;
            break;
        case EDIT_PROFILE_EDUCATION:
            bSingleSelect = YES;
            str = educationLabel.text;
            break;
        case EDIT_PROFILE_INCOME:
            bSingleSelect = YES;
            str = incomeLabel.text;
            break;
        case EDIT_PROFILE_PROFESSION:
            bSingleSelect = YES;
            str = professionLabel.text;
            break;
        case EDIT_PROFILE_TRAVEL:
            bSingleSelect = YES;
            str = travelLabel.text;
            break;
        case EDIT_PROFILE_EXERCISE:
            bSingleSelect = YES;
            str = exerciseLabel.text;
            break;
        case EDIT_PROFILE_HAVE_CHILD:
            bSingleSelect = YES;
            str = haveChildLabel.text;
            break;
        case EDIT_PROFILE_WANT_CHILD:
            bSingleSelect = YES;
            str = wantChildLabel.text;
            break;
        case EDIT_PROFILE_SMOKING:
            bSingleSelect = YES;
            str = smokingLabel.text;
            break;
        case EDIT_PROFILE_DRINKING:
            bSingleSelect = YES;
            str = drinkingLabel.text;
            break;
        case EDIT_PROFILE_RELIGION:
            bSingleSelect = YES;
            str = religionLabel.text;
            break;
        case EDIT_PROFILE_BODY_TYPE:
            bSingleSelect = YES;
            str = bodyLabel.text;
            break;
        case EDIT_PROFILE_HAIR_STYLE:
            bSingleSelect = YES;
            str = hairLabel.text;
            break;
        case EDIT_PROFILE_FOODS:
            str = foodsLabel.text;
            break;
        case EDIT_PROFILE_HOBBIES:
            str = hobbiesLabel.text;
            break;
        case EDIT_PROFILE_WATCH:
            str = watchLabel.text;
            break;
        case EDIT_PROFILE_LISTEN:
            str = listenLabel.text;
            break;
        case EDIT_PROFILE_SPEND_HOLIDAY:
            str = holidayLabel.text;
            break;
        case EDIT_PROFILE_PET:
            str = petLabel.text;
            break;
        case EDIT_PROFILE_SPORTS:
            str = sportsLabel.text;
            break;
    }

    if( str && [str length] > 0 )
    {
        if( bSingleSelect == YES )
        {
            [editItemArray addObject:str];
            return;
        }
        
        NSArray *sarray_1 = [str componentsSeparatedByString:@", "];
        NSArray *sarray_2 = [str componentsSeparatedByString:@","];
        
        if( [sarray_1 count] >= [sarray_2 count] )
            [editItemArray addObjectsFromArray:sarray_1];
        else
            [editItemArray addObjectsFromArray:sarray_2];
    }
}

- (NSString *)getSelectedItemNumStringWithType:(NSInteger)type plistName:(NSString *)name
{
    NSString *_retStr = @"";

    [editItemArray removeAllObjects];

    [self getItemArrayFromPlist:name];
    
    if( !itemArray || [itemArray count] < 1 )
    {
        return _retStr;
    }
    
    [self getEditItemArrayType:type];
    
    if( !editItemArray || [editItemArray count] < 1 )
    {
        return _retStr;
    }
    
    BOOL    isFirst = YES, isOneSel = YES;
    
    for( NSString *substr in editItemArray )
    {
        for( int i = 0; i < [itemArray count]; i++ )
        {
            if( [substr isEqualToString:[itemArray objectAtIndex:i]] )
            {
                if( isFirst )
                {
                    _retStr = [NSString stringWithFormat:@"%d", i];
                    isFirst = NO;
                }
                else
                {
                    isOneSel = NO;
                    _retStr = [NSString stringWithFormat:@"%@|%d", _retStr, i];
                }
                break;
            }
        }
    }
    
    if( !isOneSel )
    {
        _retStr = [NSString stringWithFormat:@"%@|", _retStr];
    }

    return _retStr;
}

- (void)loadBasicProfileInfo:(NSDictionary*)dicBasicInfo
{
    if( dicBasicInfo == nil )
        return;
    
    if( ![dicBasicInfo isKindOfClass:[NSDictionary class]] )
        return;
    
    NSArray*    arrayBasic = [dicBasicInfo objectForKey:@"basic"];
   // NSLog(@"%@",arrayBasic);
    
    if( arrayBasic == nil || ![arrayBasic isKindOfClass:[NSArray class]] )
        return;

    //[self addOneItemTitle:@"Marital Status" content:[arrayBasic objectAtIndex:6]];
    self.handphoneStr = [arrayBasic objectAtIndex:4];
    
   // NSLog(@"self.handphoneStr==%@",self.handphoneStr);
    //international treatment
    if ([[eSyncronyAppDelegate sharedInstance].language hasPrefix:@"zh-Hant"]||[[eSyncronyAppDelegate sharedInstance].language hasPrefix:@"id"]||[[eSyncronyAppDelegate sharedInstance].language hasPrefix:@"zh-HK"]){//151029
        NSString *_strPath =[[NSBundle mainBundle] pathForResource:@"ethnicity_zhHant" ofType:@"plist"];
//        NSString *_strPath =[[NSBundle mainBundle] pathForResource:@"ethnicity_zhHant" ofType:@"xml"];
        NSDictionary *_dicCountry = [NSDictionary dictionaryWithContentsOfFile:_strPath];
       
        ethnicityLabel.text =[[_dicCountry objectForKey:@"ethnicity"]objectForKey:[arrayBasic objectAtIndex:7]];
        self.ethnicitySelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_ETHNICITY plistName:@"ethnicity"];
        
        nationalitLabel.text = [[_dicCountry objectForKey:@"nationality"]objectForKey:[arrayBasic objectAtIndex:8]];
//        NSString *str = [self getSelectedItemNumStringWithType:EDIT_PROFILE_NATIONALITY plistName:@"nationality"];
        NSString *str = [self getSelectedItemNumStringWithType:EDIT_PROFILE_NATIONALITY plistName:@"nationalityA"];
//        self.nationalitSelStr = [NSString stringWithFormat:@"%d", ([str intValue]+1)];
        self.nationalitSelStr = [NSString stringWithFormat:@"%d", [str intValue]];
        
        firstLangLabel.text = [[_dicCountry objectForKey:@"secondary_language"]objectForKey:[arrayBasic objectAtIndex:11]];
        self.firstLangSelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_FIRST_LANG plistName:@"secondary_language"];
        
        otherLangLabel.text = [EditProfileViewController p_stringTranslation:[arrayBasic objectAtIndex:10] andPlistName:@"secondary_language" inWhereLookFor:_dicCountry];
        self.otherLangSelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_OTHER_LANG plistName:@"secondary_language"];
        
        educationLabel.text = [[_dicCountry objectForKey:@"education"]objectForKey:[arrayBasic objectAtIndex:9]];
        self.educationSelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_EDUCATION plistName:@"education"];
        
//        [_strPath release];
//        [_dicCountry release];
    }
    else//en
    {
        ethnicityLabel.text = [arrayBasic objectAtIndex:7];
        self.ethnicitySelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_ETHNICITY plistName:@"ethnicity"];
        
        nationalitLabel.text = [arrayBasic objectAtIndex:8];
        NSString *str = [self getSelectedItemNumStringWithType:EDIT_PROFILE_NATIONALITY plistName:@"nationalityA"];
//        self.nationalitSelStr = [NSString stringWithFormat:@"%d", ([str intValue]+1)];
         self.nationalitSelStr = [NSString stringWithFormat:@"%d", [str intValue]];
        
        firstLangLabel.text = [arrayBasic objectAtIndex:11];
        self.firstLangSelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_FIRST_LANG plistName:@"secondary_language"];
        
        otherLangLabel.text = [arrayBasic objectAtIndex:10];
        self.otherLangSelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_OTHER_LANG plistName:@"secondary_language"];
        
        educationLabel.text = [arrayBasic objectAtIndex:9];
        self.educationSelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_EDUCATION plistName:@"education"];
 
    }

}

- (void)loadLifeStyleInfo:(NSArray*)arrayLifeStyleInfo
{
    if( arrayLifeStyleInfo == nil )
        return;
    
    if( ![arrayLifeStyleInfo isKindOfClass:[NSArray class]] )
        return;
    
    //[self addOneItemTitle:@"Industry" content:[arrayLifeStyleInfo objectAtIndex:2]];
    
     if ([[eSyncronyAppDelegate sharedInstance].language hasPrefix:@"zh-Hant"]||[[eSyncronyAppDelegate sharedInstance].language hasPrefix:@"zh-HK"]||[[eSyncronyAppDelegate sharedInstance].language hasPrefix:@"id"]){
         
        NSString *_strPath =[[NSBundle mainBundle] pathForResource:@"ethnicity_zhHant" ofType:@"plist"];
//         NSString *_strPath =[[NSBundle mainBundle] pathForResource:@"ethnicity_zhHant" ofType:@"xml"];
        NSDictionary *_dicCountry = [NSDictionary dictionaryWithContentsOfFile:_strPath];
        
        incomeLabel.text = [arrayLifeStyleInfo objectAtIndex:0];
        self.incomeSelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_INCOME plistName:@"income"];
        
         if ([[_dicCountry objectForKey:@"profession"] objectForKey:[arrayLifeStyleInfo objectAtIndex:1]]==nil) {
             
             professionLabel.text = [arrayLifeStyleInfo objectAtIndex:1];
             self.professionSelStr = @"32";
             
         }else
         {
            professionLabel.text = [[_dicCountry objectForKey:@"profession"] objectForKey:[arrayLifeStyleInfo objectAtIndex:1]];
            self.professionSelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_PROFESSION plistName:@"profession"];
         }
        travelLabel.text = [[_dicCountry objectForKey:@"travel_exercise"] objectForKey:[arrayLifeStyleInfo objectAtIndex:8]];
        self.travelSelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_TRAVEL plistName:@"travel_exercise"];
        
        exerciseLabel.text = [[_dicCountry objectForKey:@"travel_exercise"] objectForKey:[arrayLifeStyleInfo objectAtIndex:4]];
        self.exerciseSelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_EXERCISE plistName:@"travel_exercise"];
        
        haveChildLabel.text = [[_dicCountry objectForKey:@"children_num_value"] objectForKey:[arrayLifeStyleInfo objectAtIndex:7]];
        self.haveChildSelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_HAVE_CHILD plistName:@"child"];
        
        wantChildLabel.text = [[_dicCountry objectForKey:@"children_num_value"] objectForKey:[arrayLifeStyleInfo objectAtIndex:6]];
        self.wantChildSelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_WANT_CHILD plistName:@"child_want"];
        
        smokingLabel.text = [[_dicCountry objectForKey:@"smoking"] objectForKey:[arrayLifeStyleInfo objectAtIndex:3]];
        self.smokingSelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_SMOKING plistName:@"smoking_drinking"];
        
        drinkingLabel.text = [[_dicCountry objectForKey:@"drinking"] objectForKey:[arrayLifeStyleInfo objectAtIndex:5]];
        self.drinkingSelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_DRINKING plistName:@"drinking_smoking"];
        
        religionLabel.text = [[_dicCountry objectForKey:@"religion"] objectForKey:[arrayLifeStyleInfo objectAtIndex:9]];
        self.religionSelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_RELIGION plistName:@"religion"];
    
    }else{
        
        incomeLabel.text = [arrayLifeStyleInfo objectAtIndex:0];
        self.incomeSelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_INCOME plistName:@"income"];
        
        NSString *_strPath =[[NSBundle mainBundle] pathForResource:@"ethnicity_zhHant" ofType:@"plist"];
        NSDictionary *_dicCountry = [NSDictionary dictionaryWithContentsOfFile:_strPath];
        if ([[_dicCountry objectForKey:@"profession"] objectForKey:[arrayLifeStyleInfo objectAtIndex:1]]==nil) {
            
            professionLabel.text = [arrayLifeStyleInfo objectAtIndex:1];
            self.professionSelStr = @"32";
            
        }else
        {
            professionLabel.text = [arrayLifeStyleInfo objectAtIndex:1];
            self.professionSelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_PROFESSION plistName:@"profession"];
        }
//        [_dicCountry release];
//        [_strPath release];
        
        travelLabel.text = [arrayLifeStyleInfo objectAtIndex:8];
        self.travelSelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_TRAVEL plistName:@"travel_exercise"];
        
        exerciseLabel.text = [arrayLifeStyleInfo objectAtIndex:4];
        self.exerciseSelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_EXERCISE plistName:@"travel_exercise"];
        
        haveChildLabel.text = [arrayLifeStyleInfo objectAtIndex:7];
        self.haveChildSelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_HAVE_CHILD plistName:@"child"];
        
        wantChildLabel.text = [arrayLifeStyleInfo objectAtIndex:6];
        self.wantChildSelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_WANT_CHILD plistName:@"child_want"];
        
        smokingLabel.text = [arrayLifeStyleInfo objectAtIndex:3];
        self.smokingSelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_SMOKING plistName:@"smoking_drinking"];
        
        drinkingLabel.text = [arrayLifeStyleInfo objectAtIndex:5];
        self.drinkingSelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_DRINKING plistName:@"drinking_smoking"];
        
        religionLabel.text = [arrayLifeStyleInfo objectAtIndex:9];
        self.religionSelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_RELIGION plistName:@"religion"];
    }
}

- (void)loadAppearanceInfo:(NSDictionary*)dicAppearanceInfo
{
    if( dicAppearanceInfo == nil )
        return;
    
    if( ![dicAppearanceInfo isKindOfClass:[NSDictionary class]] )
        return;
    
    NSArray*    arrayApperance = [dicAppearanceInfo objectForKey:@"appearance"];
    if( arrayApperance == nil || ![arrayApperance isKindOfClass:[NSArray class]] )
        return;

    heightTf.text = [arrayApperance objectAtIndex:0];
    weightTf.text = [arrayApperance objectAtIndex:2];
    
  if ([[eSyncronyAppDelegate sharedInstance].language hasPrefix:@"zh-Hant"]||[[eSyncronyAppDelegate sharedInstance].language hasPrefix:@"zh-HK"]||[[eSyncronyAppDelegate sharedInstance].language hasPrefix:@"id"]){
        NSString *_strPath =[[NSBundle mainBundle] pathForResource:@"ethnicity_zhHant" ofType:@"plist"];
//      NSString *_strPath =[[NSBundle mainBundle] pathForResource:@"ethnicity_zhHant" ofType:@"xml"];
        NSDictionary *_dicCountry = [NSDictionary dictionaryWithContentsOfFile:_strPath];
        
        bodyLabel.text = [[_dicCountry objectForKey:@"bodytype"] objectForKey:[arrayApperance objectAtIndex:1]];
        self.bodySelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_BODY_TYPE plistName:@"bodytype"];
        
        hairLabel.text = [[_dicCountry objectForKey:@"hairstyle"] objectForKey:[arrayApperance objectAtIndex:3]];
        self.hairSelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_HAIR_STYLE plistName:@"hairstyle"];
        
    }else{
        
        bodyLabel.text = [arrayApperance objectAtIndex:1];
        self.bodySelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_BODY_TYPE plistName:@"bodytype"];
        
        hairLabel.text = [arrayApperance objectAtIndex:3];
        self.hairSelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_HAIR_STYLE plistName:@"hairstyle"];
    }
    

}
-(NSString *)p_interestStringTranslation:(NSString *)needTranslationStr andPlistName:(NSString *)plistName//translate other language bylimit 0716
{
    NSString *_strPath =[[NSBundle mainBundle] pathForResource:@"ethnicity_zhHant" ofType:@"plist"];
//    NSString *_strPath =[[NSBundle mainBundle] pathForResource:@"ethnicity_zhHant" ofType:@"xml"];
    NSDictionary *_dicCountry = [NSDictionary dictionaryWithContentsOfFile:_strPath];
    NSDictionary *p_ethnicity=[_dicCountry objectForKey:plistName];
    
    NSArray*    items = [needTranslationStr componentsSeparatedByString:@","];
    if( items == 0 || [needTranslationStr isEqualToString:@""] )
        return needTranslationStr;
    if ([items count] == 1) {
        
        return [p_ethnicity objectForKey:needTranslationStr];
        
    }else
    {
        BOOL p_judge = YES;
        
        NSMutableString *p_content = [[NSMutableString alloc]init];
        NSString *p_toContent = [[NSString alloc]init];
        NSString *name = [[NSString alloc]init];
        for( int i = 0; i < [items count]; i++ )
        {
//            NSLog(@"%@",[items objectAtIndex:i]);
            if (([[items objectAtIndex:i] isEqualToString:@""])||([items objectAtIndex:i] == nil)) {
                continue;
                
                p_judge = NO;
                break;
            }
//            NSLog(@"%@",name);
            if (p_judge == YES) {
                name =[p_ethnicity objectForKey:[items objectAtIndex:i]];
                if( name.length == 0)
                    break;
                [p_content appendString:[NSString stringWithFormat:@",%@",name]];
            }
        }
        unichar c = [p_content characterAtIndex:0];
        NSRange range = NSMakeRange(0, 1);
        if (c == ',') {
            [p_content deleteCharactersInRange:range];
        }
        p_toContent = [p_content copy];
        NSLog(@"%@",p_toContent);
        return p_toContent;
    }
}
- (void)loadInterestInfo:(NSArray*)arrayInterestInfo
{
    if( arrayInterestInfo == nil )
        return;
    
    if( ![arrayInterestInfo isKindOfClass:[NSArray class]] )
        return;
    
    if ([[eSyncronyAppDelegate sharedInstance].language hasPrefix:@"zh-Hant"]||[[eSyncronyAppDelegate sharedInstance].language hasPrefix:@"zh-HK"]||[[eSyncronyAppDelegate sharedInstance].language hasPrefix:@"id"]){
        foodsLabel.text = [arrayInterestInfo objectAtIndex:0];
        foodsLabel.text =[EditProfileViewController p_stringDeleteString:foodsLabel.text];
        foodsLabel.text = [self p_interestStringTranslation:foodsLabel.text andPlistName:@"Food"];
        self.foodsSelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_FOODS plistName:@"fav_food"];
        
        hobbiesLabel.text = [arrayInterestInfo objectAtIndex:1];
        hobbiesLabel.text = [EditProfileViewController p_stringDeleteString:hobbiesLabel.text];
        hobbiesLabel.text = [self p_interestStringTranslation:hobbiesLabel.text andPlistName:@"Hobby"];
        self.hobbiesSelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_HOBBIES plistName:@"fav_hobby"];
        
        watchLabel.text = [arrayInterestInfo objectAtIndex:2];
        watchLabel.text =[EditProfileViewController p_stringDeleteString:watchLabel.text];
        watchLabel.text = [self p_interestStringTranslation:watchLabel.text andPlistName:@"Film"];
        self.watchSelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_WATCH plistName:@"fav_movie"];
        
        listenLabel.text = [arrayInterestInfo objectAtIndex:3];
        listenLabel.text =[EditProfileViewController p_stringDeleteString:listenLabel.text];
        listenLabel.text = [self p_interestStringTranslation:listenLabel.text andPlistName:@"Music"];
        self.listenSelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_LISTEN plistName:@"fav_music"];
        
        holidayLabel.text = [arrayInterestInfo objectAtIndex:4];
        holidayLabel.text =[EditProfileViewController p_stringDeleteString:holidayLabel.text];
        holidayLabel.text = [self p_interestStringTranslation:holidayLabel.text andPlistName:@"Travel"];
        self.holidaySelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_SPEND_HOLIDAY plistName:@"fav_holiday"];
        
        petLabel.text = [arrayInterestInfo objectAtIndex:6];
        //petLabel.text =[EditProfileViewController p_stringDeleteString:petLabel.text];
        if ((petLabel.text ==nil)||([petLabel.text isEqualToString:@""])) {
            petLabel.text = @"No Pets";
        }
        petLabel.text = [self p_interestStringTranslation:petLabel.text andPlistName:@"OwnPets"];
        self.petSelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_PET plistName:@"fav_pet"];
        
        sportsLabel.text = [arrayInterestInfo objectAtIndex:5];//append for no interest
        if ((sportsLabel.text == nil)||[sportsLabel.text isEqualToString:@""]) {
            sportsLabel.text = [self p_interestStringTranslation:sportsLabel.text andPlistName:@"sport"];
            self.sportsSelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_SPORTS plistName:@"fav_sport"];
        }else
        {
            sportsLabel.text =[EditProfileViewController p_stringDeleteString:sportsLabel.text];
            sportsLabel.text = [self p_interestStringTranslation:sportsLabel.text andPlistName:@"sport"];
            self.sportsSelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_SPORTS plistName:@"fav_sport"];
        }
        
    }else{
        
        foodsLabel.text = [arrayInterestInfo objectAtIndex:0];
        foodsLabel.text =[EditProfileViewController p_stringDeleteString:foodsLabel.text];
        self.foodsSelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_FOODS plistName:@"fav_food"];
        
        hobbiesLabel.text = [arrayInterestInfo objectAtIndex:1];
        hobbiesLabel.text =[EditProfileViewController p_stringDeleteString:hobbiesLabel.text];
        self.hobbiesSelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_HOBBIES plistName:@"fav_hobby"];
        
        watchLabel.text = [arrayInterestInfo objectAtIndex:2];
        watchLabel.text =[EditProfileViewController p_stringDeleteString:watchLabel.text];
        self.watchSelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_WATCH plistName:@"fav_movie"];
        
        listenLabel.text = [arrayInterestInfo objectAtIndex:3];
        listenLabel.text =[EditProfileViewController p_stringDeleteString:listenLabel.text];
        self.listenSelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_LISTEN plistName:@"fav_music"];
        
        holidayLabel.text = [arrayInterestInfo objectAtIndex:4];
        holidayLabel.text =[EditProfileViewController p_stringDeleteString:holidayLabel.text];
        self.holidaySelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_SPEND_HOLIDAY plistName:@"fav_holiday"];
        
        petLabel.text = [arrayInterestInfo objectAtIndex:6];
        if ((petLabel.text ==nil)||([petLabel.text isEqualToString:@""])) {
            petLabel.text = @"No Pets";
        }
        //petLabel.text =[EditProfileViewController p_stringDeleteString:petLabel.text];
        self.petSelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_PET plistName:@"fav_pet"];
        
        sportsLabel.text = [arrayInterestInfo objectAtIndex:5];
        if ((sportsLabel.text == nil)||[sportsLabel.text isEqualToString:@""]) {
            self.sportsSelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_SPORTS plistName:@"fav_sport"];
        }else
        {
            sportsLabel.text =[EditProfileViewController p_stringDeleteString:sportsLabel.text];
            self.sportsSelStr = [self getSelectedItemNumStringWithType:EDIT_PROFILE_SPORTS plistName:@"fav_sport"];
        }
    }
}

- (void)loadProfileInfo
{
    [self loadBasicProfileInfo:[self.dicProfileInfo objectForKey:@"basic"]];
    [self loadLifeStyleInfo:[self.dicProfileInfo objectForKey:@"lifestyle"]];
    [self loadAppearanceInfo:[self.dicProfileInfo objectForKey:@"appearance"]];
    [self loadInterestInfo:[self.dicProfileInfo objectForKey:@"interest"]];
}

//========================================================================================================//
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if( textField.tag == 100 )
    {
        [weightTf becomeFirstResponder];
        return YES;
    }

    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if( textField.tag == 100 )
    {
        [scrlView scrollRectToVisible:CGRectMake(0, 940, 320, 460) animated:YES];
    }
    else
    {
        [scrlView scrollRectToVisible:CGRectMake(0, 970, 320, 460) animated:YES];
    }
}

- (void)loadItemArrayWithName:(NSString *)name
{
    [editItemArray removeAllObjects];

    [self getItemArrayFromPlist:name];
    if( !itemArray || [itemArray count] < 1 )
        return;

    [self getEditItemArrayType:editItemType];
}

- (void)receiveEditResultWithSelectedItemNumString:(NSString *)selNumStr
{
    NSString    *str;
    NSInteger   i, editItemCnt;

    editItemCnt = [editItemArray count];
    if( editItemCnt == 0 )
    {
        str = @"";
    }
    else
    {
        str = [editItemArray objectAtIndex:0];

        for( i = 1; i < editItemCnt; i++ ) {
            str = [NSString stringWithFormat:@"%@, %@", str, [editItemArray objectAtIndex:i]];
        }
    }

    switch( editItemType )
    {
        case EDIT_PROFILE_ETHNICITY:
            ethnicityLabel.text = str;
            self.ethnicitySelStr = selNumStr;
            break;
        case EDIT_PROFILE_NATIONALITY:
            nationalitLabel.text = str;
            self.nationalitSelStr = selNumStr;
            break;
        case EDIT_PROFILE_FIRST_LANG:
            firstLangLabel.text = str;
            self.firstLangSelStr = selNumStr;
            break;
        case EDIT_PROFILE_OTHER_LANG:
            otherLangLabel.text = str;
            self.otherLangSelStr = selNumStr;
            break;
        case EDIT_PROFILE_EDUCATION:
            educationLabel.text = str;
            self.educationSelStr = selNumStr;
            break;
        case EDIT_PROFILE_INCOME:
            incomeLabel.text = str;
            self.incomeSelStr = selNumStr;
            break;
        case EDIT_PROFILE_PROFESSION:
            professionLabel.text = str;
            self.professionSelStr = selNumStr;
            break;
        case EDIT_PROFILE_TRAVEL:
            travelLabel.text = str;
            self.travelSelStr = selNumStr;
            break;
        case EDIT_PROFILE_EXERCISE:
            exerciseLabel.text = str;
            self.exerciseSelStr = selNumStr;
            break;
        case EDIT_PROFILE_HAVE_CHILD:
            haveChildLabel.text = str;
            self.haveChildSelStr = selNumStr;
            break;
        case EDIT_PROFILE_WANT_CHILD:
            wantChildLabel.text = str;
            self.wantChildSelStr = selNumStr;
            break;
        case EDIT_PROFILE_SMOKING:
            smokingLabel.text = str;
            self.smokingSelStr = selNumStr;
            break;
        case EDIT_PROFILE_DRINKING:
            drinkingLabel.text = str;
            self.drinkingSelStr = selNumStr;
            break;
        case EDIT_PROFILE_RELIGION:
            religionLabel.text = str;
            self.religionSelStr = selNumStr;
            break;
        case EDIT_PROFILE_BODY_TYPE:
            bodyLabel.text = str;
            self.bodySelStr = selNumStr;
            break;
        case EDIT_PROFILE_HAIR_STYLE:
            hairLabel.text = str;
            self.hairSelStr = selNumStr;
            break;
        case EDIT_PROFILE_FOODS:
            foodsLabel.text = str;
            self.foodsSelStr = selNumStr;
            break;
        case EDIT_PROFILE_HOBBIES:
            hobbiesLabel.text = str;
            self.hobbiesSelStr = selNumStr;
            break;
        case EDIT_PROFILE_WATCH:
            watchLabel.text = str;
            self.watchSelStr = selNumStr;
            break;
        case EDIT_PROFILE_LISTEN:
            listenLabel.text = str;
            self.listenSelStr = selNumStr;
            break;
        case EDIT_PROFILE_SPEND_HOLIDAY:
            holidayLabel.text = str;
            self.holidaySelStr = selNumStr;
            break;
        case EDIT_PROFILE_PET:
            petLabel.text = str;
            self.petSelStr = selNumStr;
            break;
        case EDIT_PROFILE_SPORTS:
            sportsLabel.text = str;
            self.sportsSelStr = selNumStr;
            break;
    }
}   

- (IBAction)didClickBackBtn:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)alertFieldRequireMessage:(NSString*)strMsg
{
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Field Required",)
                                                 message:strMsg
                                                delegate:nil
                                       cancelButtonTitle:NSLocalizedString(@"OK",)
                                       otherButtonTitles:nil];
    [av show];
    [av release];

    return NO;
}

- (void)focusToView:(UIView*)focusedView
{
    CGRect frame = focusedView.frame;
    
    frame.origin.y -= 30;
    if( frame.origin.y < 0 )
        frame.origin.y = 0;
    
    frame.size.height += 60;

    [scrlView scrollRectToVisible:frame animated:YES];
}

- (BOOL)verifyFormData
{
    NSArray     *array;
    
//    array = [self.ethnicitySelStr componentsSeparatedByString:@"|"];
    if( [self.ethnicitySelStr isEqualToString:@""] )
    {
        [self focusToView:ethnicityBtn];
        return [self alertFieldRequireMessage:NSLocalizedString(@"Please select your ethnicity.",@"EditProfileView")];
    }

//    array = [self.nationalitSelStr componentsSeparatedByString:@"|"];
    if( [self.nationalitSelStr isEqualToString:@""] )
    {
        [self focusToView:nationalitBtn];
        return [self alertFieldRequireMessage:NSLocalizedString(@"Please select your nationality.",@"EditProfileView")];
    }

//    array = [self.firstLangSelStr componentsSeparatedByString:@"|"];
    if( [self.firstLangSelStr isEqualToString:@""] )
    {
        [self focusToView:firstLangBtn];
        return [self alertFieldRequireMessage:NSLocalizedString(@"Please select your language.",@"EditProfileView")];
    }
    
    array = [self.otherLangSelStr componentsSeparatedByString:@"|"];
    if( [self.otherLangSelStr isEqualToString:@""]||[array count] < 1  )
    {
        [self focusToView:otherLangBtn];
        return [self alertFieldRequireMessage:NSLocalizedString(@"Please select your secondary language.",@"EditProfileView")];
    }
    
//    array = [self.educationSelStr componentsSeparatedByString:@"|"];
    if( [self.educationSelStr isEqualToString:@""] )
    {
        [self focusToView:educationBtn];
        return [self alertFieldRequireMessage:NSLocalizedString(@"Please select your educational level.",@"EditProfileView")];
    }
    
//    array = [self.incomeSelStr componentsSeparatedByString:@"|"];
    if( [self.incomeSelStr isEqualToString:@""] )
    {
        [self focusToView:incomeBtn];
        return [self alertFieldRequireMessage:NSLocalizedString(@"Please select your income.",@"EditProfileView")];
    }
    
//    array = [self.professionSelStr componentsSeparatedByString:@"|"];
    if( [self.professionSelStr isEqualToString:@""] )
    {
        [self focusToView:professionBtn];
        return [self alertFieldRequireMessage:NSLocalizedString(@"Please select your career.",@"EditProfileView")];
    }
    
//    array = [self.travelSelStr componentsSeparatedByString:@"|"];
    if( [self.travelSelStr isEqualToString:@""] )
    {
        [self focusToView:travelBtn];
        return [self alertFieldRequireMessage:NSLocalizedString(@"Please select your travel habits.",@"EditProfileView")];
    }

//    array = [self.exerciseSelStr componentsSeparatedByString:@"|"];
    if( [self.exerciseSelStr isEqualToString:@""] )
    {
        [self focusToView:exerciseBtn];
        return [self alertFieldRequireMessage:NSLocalizedString(@"Please select your exercise habits.",@"EditProfileView")];
    }
    
//    array = [self.haveChildSelStr componentsSeparatedByString:@"|"];
    if( [self.haveChildSelStr isEqualToString:@""] )
    {
        [self focusToView:haveChildBtn];
        return [self alertFieldRequireMessage:NSLocalizedString(@"Please select whether you have child or not.",@"EditProfileView")];
    }
    
//    array = [self.wantChildSelStr componentsSeparatedByString:@"|"];
    if( [self.wantChildSelStr isEqualToString:@""] )
    {
        [self focusToView:wantChildBtn];
        return [self alertFieldRequireMessage:NSLocalizedString(@"Please select whether you want to have child or not.",@"EditProfileView")];
    }
    
//    array = [self.smokingSelStr componentsSeparatedByString:@"|"];
    if( [self.smokingSelStr isEqualToString:@""] )
    {
        [self focusToView:smokingBtn];
        return [self alertFieldRequireMessage:NSLocalizedString(@"Please select your smoking habits.",@"EditProfileView")];
    }

//    array = [self.drinkingSelStr componentsSeparatedByString:@"|"];
    if( [self.drinkingSelStr isEqualToString:@""] )
    {
        [self focusToView:drinkingBtn];
        return [self alertFieldRequireMessage:NSLocalizedString(@"Please select your drinking habits.",@"EditProfileView")];
    }

//    array = [self.religionSelStr componentsSeparatedByString:@"|"];
    if( [self.religionSelStr isEqualToString:@""] )
    {
        [self focusToView:religionBtn];
        return [self alertFieldRequireMessage:NSLocalizedString(@"Please select your religion.",@"EditProfileView")];
    }
    
//    array = [self.bodySelStr componentsSeparatedByString:@"|"];
    if( [self.bodySelStr isEqualToString:@""] )
    {
        [self focusToView:bodyBtn];
        return [self alertFieldRequireMessage:NSLocalizedString(@"Please select your body type.",@"EditProfileView")];
    }
    
//    array = [self.hairSelStr componentsSeparatedByString:@"|"];
    if( [self.hairSelStr isEqualToString:@""] )
    {
        [self focusToView:hairBtn];
        return [self alertFieldRequireMessage:NSLocalizedString(@"Please select your hair style.",@"EditProfileView")];
    }
    
    if( [weightTf.text floatValue] <= 0 )
    {
        [self focusToView:weightTf];
        return [self alertFieldRequireMessage:NSLocalizedString(@"Please enter your weight.",@"EditProfileView")];
    }
    
    if( [heightTf.text floatValue] <= 0 )
    {
        [self focusToView:heightTf];
        return [self alertFieldRequireMessage:NSLocalizedString(@"Please enter your height.",@"EditProfileView")];
    }
    
    array = [self.foodsSelStr componentsSeparatedByString:@"|"];
    if( [self.foodsSelStr isEqualToString:@""] )
    {
        [self focusToView:foodsBtn];
        return [self alertFieldRequireMessage:NSLocalizedString(@"Please select your favorite foods.",@"EditProfileView")];
    }
    
    if( [array count] < 4 )
    {
        [self focusToView:foodsBtn];
        return [self alertFieldRequireMessage:NSLocalizedString(@"Please Select more than 3 favourite foods.",@"EditProfileView")];
    }
   
    array = [self.hobbiesSelStr componentsSeparatedByString:@"|"];
    if( [self.hobbiesSelStr isEqualToString:@""] )
    {
        [self focusToView:hobbiesBtn];
        return [self alertFieldRequireMessage:NSLocalizedString(@"Please select your hobbies.",@"EditProfileView")];
    }
    
    if( [array count] < 4 )
    {
        [self focusToView:hobbiesBtn];
        return [self alertFieldRequireMessage:NSLocalizedString(@"Please Select more than 3 hobbies.",@"EditProfileView")];
    }
    
    array = [self.watchSelStr componentsSeparatedByString:@"|"];
    if( [self.watchSelStr isEqualToString:@""] )
    {
        [self focusToView:watchBtn];
        return [self alertFieldRequireMessage:NSLocalizedString(@"Please select your favorite movies.",@"EditProfileView")];
    }

    if( [array count] < 4 )
    {
        [self focusToView:watchBtn];
        return [self alertFieldRequireMessage:NSLocalizedString(@"Please Select more than 3 favorite movies.",@"EditProfileView")];
    }
    
    array = [self.listenSelStr componentsSeparatedByString:@"|"];
    if( [self.listenSelStr isEqualToString:@""] )
    {
        [self focusToView:listenBtn];
        return [self alertFieldRequireMessage:NSLocalizedString(@"Please select your favorite musics.",@"EditProfileView")];
    }
    
    if( [array count] < 4 )
    {
        [self focusToView:listenBtn];
        return [self alertFieldRequireMessage:NSLocalizedString(@"Please Select more than 3 favorite musics.",@"EditProfileView")];
    }
    
    array = [self.holidaySelStr componentsSeparatedByString:@"|"];
    if( [self.holidaySelStr isEqualToString:@""] )
    {
        [self focusToView:holidayBtn];
        return [self alertFieldRequireMessage:NSLocalizedString(@"Please select where you'd like to spend holidays.",@"EditProfileView")];
    }
    
    if( [array count] < 4 )
    {
        [self focusToView:holidayBtn];
        return [self alertFieldRequireMessage:NSLocalizedString(@"Please Select more than 3 holiday items.",@"EditProfileView")];
    }

    array = [self.sportsSelStr componentsSeparatedByString:@"|"];
    if( [self.sportsSelStr isEqualToString:@""] )
    {
        [self focusToView:sportsBtn];
        return [self alertFieldRequireMessage:NSLocalizedString(@"Please select your favorite sports.",@"EditProfileView")];
    }

    if( [array count] < 4 )
    {
        [self focusToView:sportsBtn];
        return [self alertFieldRequireMessage:NSLocalizedString(@"Please Select more than 3 favorite sports.",@"EditProfileView")];
    }
    
    array = [self.petSelStr componentsSeparatedByString:@"|"];
    if( [self.petSelStr isEqualToString:@""] || [array count] < 1 )
    {
        [self focusToView:petBtn];
        return [self alertFieldRequireMessage:NSLocalizedString(@"Please select your favorite pet.",@"EditProfileView")];
    }

    return YES;
}

//==================================================================================================================//

- (void)checkSavingResult:(NSDictionary*)result andGoToNext:(int)step
{
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Saving Failure"];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( response == nil )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Saving Failure",)];
        return;
    }
    
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
    }
    else
    {
        [self saveProfileWithStep:step];
    }
}

- (void)updateBasic
{
    [DSBezelActivityView removeViewAnimated:NO];

    NSArray     *arrNationM = [Global loadQuestionArray:@"nationality1"];
    int         nNation = [self.nationalitSelStr intValue];
    nNation = [[arrNationM objectAtIndex:nNation] intValue];
//    NSLog(@"nNation = %d",nNation);
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strName forKey:@"nname"];
    
    [params setObject:self.handphoneStr forKey:@"handphone"];
    
    [params setObject:self.ethnicitySelStr forKey:@"ethnicity"];
    [params setObject:[NSNumber numberWithInt:nNation] forKey:@"nationality"];
    [params setObject:self.firstLangSelStr forKey:@"planguage"];
    
    NSString    *secondLangs = [self.otherLangSelStr stringByReplacingOccurrencesOfString:@"|" withString:@","];
    if( [[secondLangs substringFromIndex:secondLangs.length-1] isEqualToString:@","] )
        secondLangs = [secondLangs substringToIndex:secondLangs.length-1];

    [params setObject:secondLangs forKey:@"slanguage"];
    
    [params setObject:self.educationSelStr forKey:@"education"];
    
    NSDictionary*    result = [UtilComm updateBasicInfo:params];
    
    [self checkSavingResult:result andGoToNext:2];
}

- (void)updateLifestyle
{
    [DSBezelActivityView removeViewAnimated:NO];
    
    //------------------ Get Profession Match Index ------------------//
    
    int             nProfession = [self.professionSelStr intValue];
    
//    NSLog(@"nProfeesion1 == %d",nProfession);
    
    if (nProfession != 32) {
    
        NSArray*        arrayProfession = [Global loadQuestionArray:@"profession"];
        NSArray*        arrayProfessionM = [Global loadQuestionArray:@"profession1"];
        NSString*       strProf = [arrayProfession objectAtIndex:nProfession];
        
        for( int k = 0; k < [arrayProfessionM count]; k++ )
        {
            NSString*   strItem = [arrayProfessionM objectAtIndex:k];
            if( [strItem isEqualToString:strProf] )
            {
                if( k >= 25 )
                    nProfession = k+1;
                else
                    nProfession = k;
                break;
            }
        }

    }
//    NSLog(@"nProfeesion2 == %d",nProfession);
    
    //--------------------------------------------------------------//
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    
    [params setObject:self.religionSelStr forKey:@"religion"];
    [params setObject:[NSNumber numberWithInt:nProfession] forKey:@"career"];
    
    [params setObject:self.incomeSelStr forKey:@"income"];
    
    [params setObject:self.travelSelStr forKey:@"travel"];
    [params setObject:self.exerciseSelStr forKey:@"exercise"];
    [params setObject:self.haveChildSelStr forKey:@"children_num"];
    [params setObject:self.wantChildSelStr forKey:@"want_children"];
    
    [params setObject:self.drinkingSelStr forKey:@"drinking"];//1.2.8feedback mistake fixed
    [params setObject:self.smokingSelStr forKey:@"smoking"];
    
    NSDictionary*    result = [UtilComm updateLifestyle:params];
    [self checkSavingResult:result andGoToNext:3];
}

- (void)updateAppearance
{
    [DSBezelActivityView removeViewAnimated:NO];
    
//    NSString*   accNo = [eSyncronyAppDelegate sharedInstance].strAccNo;
//    NSString*   tokenKey = [eSyncronyAppDelegate sharedInstance].strTokenKey;
    
    //--------------------------------------------------------------//

    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];

    [params setObject:heightTf.text forKey:@"height"];
    [params setObject:weightTf.text forKey:@"weight"];
    [params setObject:self.bodySelStr forKey:@"body"];
    [params setObject:self.hairSelStr forKey:@"hair"];
    
    NSDictionary*    result = [UtilComm updateAppearance:params];
    [self checkSavingResult:result andGoToNext:4];
}

- (int)getRealIndexFromIndex:(int)index listDis:(NSArray*)listDis listOrg:(NSArray*)listOrg listNum:(NSArray*)listNum
{
    NSString*       strDisItem = [listDis objectAtIndex:index];
    int             nSel = 0;
    
    for( int k = 0; k < [listOrg count]; k++ )
    {
        NSString*   strOrgItem = [listOrg objectAtIndex:k];
        if( [strDisItem isEqualToString:strOrgItem] )
        {
            nSel = k;
            break;
        }
    }
    
    if( nSel < [listNum count] )
        nSel = [[listNum objectAtIndex:nSel] intValue];
    
    return nSel;
}

- (NSArray*)getRealIndexesList//0630
{
    
    NSMutableArray  *realIndexesList = [NSMutableArray arrayWithCapacity:0];
    NSMutableArray  *arrNameOfInfoList = [NSMutableArray arrayWithCapacity:0];
    NSMutableArray  *arrSelIndexes = [NSMutableArray arrayWithCapacity:0];
    
   NSString *_language = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    if ([_language hasPrefix:@"zh-Hant"]||[_language hasPrefix:@"zh-HK"]||[_language hasPrefix:@"id"]){
        [arrNameOfInfoList addObject:[NSString stringWithFormat:@"fav_food1"]];
        [arrNameOfInfoList addObject:[NSString stringWithFormat:@"fav_hobby1"]];
        [arrNameOfInfoList addObject:[NSString stringWithFormat:@"fav_sport1"]];
        [arrNameOfInfoList addObject:[NSString stringWithFormat:@"fav_movie1"]];
        [arrNameOfInfoList addObject:[NSString stringWithFormat:@"fav_music1"]];
        [arrNameOfInfoList addObject:[NSString stringWithFormat:@"fav_holiday1"]];
    }
    else//en
    {
        [arrNameOfInfoList addObject:[NSString stringWithFormat:@"fav_food"]];
        [arrNameOfInfoList addObject:[NSString stringWithFormat:@"fav_hobby"]];
        [arrNameOfInfoList addObject:[NSString stringWithFormat:@"fav_sport"]];
        [arrNameOfInfoList addObject:[NSString stringWithFormat:@"fav_movie"]];
        [arrNameOfInfoList addObject:[NSString stringWithFormat:@"fav_music"]];
        [arrNameOfInfoList addObject:[NSString stringWithFormat:@"fav_holiday"]];
   }

    [arrSelIndexes addObject:self.foodsSelStr];
    [arrSelIndexes addObject:self.hobbiesSelStr];
    [arrSelIndexes addObject:self.sportsSelStr];
    [arrSelIndexes addObject:self.watchSelStr];
    [arrSelIndexes addObject:self.listenSelStr];
    [arrSelIndexes addObject:self.holidaySelStr];
    
    if ([_language hasPrefix:@"zh-Hant"]||[_language hasPrefix:@"zh-HK"]||[_language hasPrefix:@"id"]){
        for( int i = 0; i < 6; i++ )
        {
            
            NSString    *stringIndexes = [arrSelIndexes objectAtIndex:i];
            NSString    *indexString = @"";
            
            stringIndexes = [stringIndexes substringToIndex:[stringIndexes length]-1];
            
            NSArray*    arrIndexs = [stringIndexes componentsSeparatedByString:@"|"];
            
            for( int k = 0; k < [arrIndexs count]; k++ )
            {
                int nOldIndex = [[arrIndexs objectAtIndex:k] intValue];
//                NSLog(@"%d",nOldIndex);
                
                indexString = [indexString stringByAppendingFormat:@"%d|",nOldIndex ];
                
//                NSLog(@"%d %@\n",nOldIndex,indexString);
            }
            
            [realIndexesList addObject:indexString];
            
//            NSLog(@"%@",realIndexesList);
        }
    }
else//en
{
    for( int i = 0; i < 6; i++ )
    {
        NSString    *strPlist = [arrNameOfInfoList objectAtIndex:i];
        NSString    *strPlist1 = [NSString stringWithFormat:@"%@1", strPlist];
        NSString    *strPlistNum = [NSString stringWithFormat:@"%@_num", strPlist];

        
        NSArray     *listDis = [Global loadQuestionArray:strPlist];
        NSArray     *listOrg = [Global loadQuestionArray:strPlist1];
        NSArray     *listNum = [Global loadQuestionArray:strPlistNum];
        
//        NSLog(@"%@ %@ %@",listDis,listOrg,listNum);

        NSString    *stringIndexes = [arrSelIndexes objectAtIndex:i];
        NSString    *indexString = @"";

        stringIndexes = [stringIndexes substringToIndex:[stringIndexes length]-1];
        
        NSArray*    arrIndexs = [stringIndexes componentsSeparatedByString:@"|"];

        for( int k = 0; k < [arrIndexs count]; k++ )
        {
            int nOldIndex = [[arrIndexs objectAtIndex:k] intValue];
//            NSLog(@"%d",nOldIndex);
            
            int index = [self getRealIndexFromIndex:nOldIndex listDis:listDis listOrg:listOrg listNum:listNum];
//            NSLog(@"%d",index);
            
            indexString = [indexString stringByAppendingFormat:@"%d|", index];
            
//            NSLog(@"%d %d %@\n",nOldIndex,index,indexString);
        }

        [realIndexesList addObject:indexString];
        
//        NSLog(@"%@",realIndexesList);
    }
}
    return realIndexesList;
}

- (void)updateInterest
{
    [DSBezelActivityView removeViewAnimated:NO];
    
    NSArray*    indexList = [self getRealIndexesList];
    
    //--------------------------------------------------------------//
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    
    [params setObject:[indexList objectAtIndex:0] forKey:@"food"];
    [params setObject:[indexList objectAtIndex:1] forKey:@"hobby"];
    [params setObject:[indexList objectAtIndex:2] forKey:@"sport"];
    [params setObject:[indexList objectAtIndex:3] forKey:@"film"];
    [params setObject:[indexList objectAtIndex:4] forKey:@"music"];
    [params setObject:[indexList objectAtIndex:5] forKey:@"holiday"];
    
    NSArray     *arrPets = [Global loadQuestionArray:@"fav_pet1"];
    
    int         nPet = [self.petSelStr intValue];
    nPet = [[arrPets objectAtIndex:nPet] intValue];
    
   [params setObject:[NSNumber numberWithInt:nPet] forKey:@"pets1"];

    
    NSDictionary*    result = [UtilComm updateInterest:params];
    [self checkSavingResult:result andGoToNext:5];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.parent refreshContent];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)saveProfileWithStep:(NSInteger)step
{
    if( step == 1 )
    {
        if( ![self verifyFormData] )
            return;

        [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Saving basic information...",@"EditProfileView")];
        [self performSelector:@selector(updateBasic) withObject:nil afterDelay:0.01];
    }
    else if( step == 2 )
    {
        [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Saving lifestyle information...",@"EditProfileView")];
        [self performSelector:@selector(updateLifestyle) withObject:nil afterDelay:0.01];
    }
    else if( step == 3 )
    {
        [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Saving appearance information...",@"EditProfileView")];
        [self performSelector:@selector(updateAppearance) withObject:nil afterDelay:0.01];
    }
    else if( step == 4 )
    {
        [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Saving interest information...",@"EditProfileView")];
        [self performSelector:@selector(updateInterest) withObject:nil afterDelay:0.01];
    }
    else
    {
        [DSBezelActivityView removeViewAnimated:YES];
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"eSynchrony"
                                                     message:NSLocalizedString(@"Your profile has been saved successfully.",@"EditProfileView")
                                                    delegate:self
                                           cancelButtonTitle:NSLocalizedString(@"OK",)
                                           otherButtonTitles:nil];
        [av show];
        [av release];
    }
}

- (IBAction)didClickSaveBtn:(id)sender
{
     [self saveProfileWithStep:1];
}

- (void)addSettingView:(UIButton *)button
{
    [profileSettingViewCtrl initSetting];
    
    [button setHighlighted:NO];
    
    [self.view performSelector:@selector(addSubview:) withObject:profileSettingViewCtrl.view afterDelay:0.1];
}

- (IBAction)didClickEthnicityBtn:(id)sender
{
    self.editItemType = EDIT_PROFILE_ETHNICITY;
    [self loadItemArrayWithName:@"ethnicity"];    
    
    profileSettingViewCtrl.itemArray = itemArray;
    profileSettingViewCtrl.parent = self;
    profileSettingViewCtrl.titleString = NSLocalizedString(@"Ethnicity",@"EditProfileView");
    profileSettingViewCtrl.isAllowMultySel = NO;
    
    [ethnicityBtn setHighlighted:YES];
    
    [self performSelector:@selector(addSettingView:) withObject:ethnicityBtn afterDelay:0.1];
}

- (IBAction)didClickNationalitBtn:(id)sender
{
    self.editItemType = EDIT_PROFILE_NATIONALITY;
    [self loadItemArrayWithName:@"nationalityA"];//nationality-->nationalityA
    
    profileSettingViewCtrl.itemArray = itemArray;
    profileSettingViewCtrl.parent = self;
    profileSettingViewCtrl.titleString = NSLocalizedString(@"Nationality",@"EditProfileView");
    profileSettingViewCtrl.isAllowMultySel = NO;
    
    [nationalitBtn setHighlighted:YES];
    
    [self performSelector:@selector(addSettingView:) withObject:nationalitBtn afterDelay:0.1];
}

- (IBAction)didClickFirstLangBtn:(id)sender
{
    self.editItemType = EDIT_PROFILE_FIRST_LANG;
    [self loadItemArrayWithName:@"secondary_language"];
    
    profileSettingViewCtrl.itemArray = itemArray;
    profileSettingViewCtrl.parent = self;
    profileSettingViewCtrl.titleString = NSLocalizedString(@"I can speak in",@"EditProfileView");
    profileSettingViewCtrl.isAllowMultySel = NO;
    
    [firstLangBtn setHighlighted:YES];
    
    [self performSelector:@selector(addSettingView:) withObject:firstLangBtn afterDelay:0.1];
}

- (IBAction)didClickOtherLangBtn:(id)sender
{
    self.editItemType = EDIT_PROFILE_OTHER_LANG;
    [self loadItemArrayWithName:@"secondary_language"];
    
    profileSettingViewCtrl.itemArray = itemArray;
    profileSettingViewCtrl.parent = self;
    profileSettingViewCtrl.titleString = NSLocalizedString(@"Other language I speak",@"EditProfileView");
    profileSettingViewCtrl.isAllowMultySel = YES;
    
    [otherLangBtn setHighlighted:YES];

    [self performSelector:@selector(addSettingView:) withObject:otherLangBtn afterDelay:0.1];
}

- (IBAction)didClickEducationBtn:(id)sender
{
    self.editItemType = EDIT_PROFILE_EDUCATION;
    [self loadItemArrayWithName:@"education"];
    
    profileSettingViewCtrl.itemArray = itemArray;
    profileSettingViewCtrl.parent = self;
    profileSettingViewCtrl.titleString = NSLocalizedString(@"Education",@"EditProfileView");
    profileSettingViewCtrl.isAllowMultySel = NO;

    [educationBtn setHighlighted:YES];
    
    [self performSelector:@selector(addSettingView:) withObject:educationBtn afterDelay:0.1];
}

- (IBAction)didClickIncomeBtn:(id)sender
{
    self.editItemType = EDIT_PROFILE_INCOME;
    [self loadItemArrayWithName:@"income"];
    
    profileSettingViewCtrl.itemArray = itemArray;
    profileSettingViewCtrl.parent = self;
    profileSettingViewCtrl.titleString = NSLocalizedString(@"My income",@"EditProfileView");
    profileSettingViewCtrl.isAllowMultySel = NO;
    
    [incomeBtn setHighlighted:YES];
    
    [self performSelector:@selector(addSettingView:) withObject:incomeBtn afterDelay:0.1];
}

- (IBAction)didClickProfessionBtn:(id)sender
{
    self.editItemType = EDIT_PROFILE_PROFESSION;
    [self loadItemArrayWithName:@"profession"];
    
    profileSettingViewCtrl.itemArray = itemArray;
    profileSettingViewCtrl.parent = self;
    profileSettingViewCtrl.titleString = NSLocalizedString(@"Profession",@"EditProfileView");
    profileSettingViewCtrl.isAllowMultySel = NO;
    
    [professionBtn setHighlighted:YES];
    
    [self performSelector:@selector(addSettingView:) withObject:professionBtn afterDelay:0.1];
}

- (IBAction)didClickTravelBtn:(id)sender
{
    self.editItemType = EDIT_PROFILE_TRAVEL;
    [self loadItemArrayWithName:@"travel_exercise"];
    
    profileSettingViewCtrl.itemArray = itemArray;
    profileSettingViewCtrl.parent = self;
    profileSettingViewCtrl.titleString = NSLocalizedString(@"I Travel",@"EditProfileView");
    profileSettingViewCtrl.isAllowMultySel = NO;
    
    [travelBtn setHighlighted:YES];
    
    [self performSelector:@selector(addSettingView:) withObject:travelBtn afterDelay:0.1];
}

- (IBAction)didClickExerciseBtn:(id)sender
{
    self.editItemType = EDIT_PROFILE_EXERCISE;
    [self loadItemArrayWithName:@"travel_exercise"];
    
    profileSettingViewCtrl.itemArray = itemArray;
    profileSettingViewCtrl.parent = self;
    profileSettingViewCtrl.titleString = NSLocalizedString(@"I Exercise",@"EditProfileView");
    profileSettingViewCtrl.isAllowMultySel = NO;
    
    [exerciseBtn setHighlighted:YES];
    
    [self performSelector:@selector(addSettingView:) withObject:exerciseBtn afterDelay:0.1];
}

- (IBAction)didClickHaveChildBtn:(id)sender
{
    self.editItemType = EDIT_PROFILE_HAVE_CHILD;
    [self loadItemArrayWithName:@"child"];
    
    profileSettingViewCtrl.itemArray = itemArray;
    profileSettingViewCtrl.parent = self;
    profileSettingViewCtrl.titleString = NSLocalizedString(@"I have children",@"EditProfileView");
    profileSettingViewCtrl.isAllowMultySel = NO;
    
    [haveChildBtn setHighlighted:YES];
    
    [self performSelector:@selector(addSettingView:) withObject:haveChildBtn afterDelay:0.1];
}

- (IBAction)didClickWantChildBtn:(id)sender
{
    self.editItemType = EDIT_PROFILE_WANT_CHILD;
    [self loadItemArrayWithName:@"child_want"];

    profileSettingViewCtrl.itemArray = itemArray;
    profileSettingViewCtrl.parent = self;
    profileSettingViewCtrl.titleString = NSLocalizedString(@"I want children",@"EditProfileView");
    profileSettingViewCtrl.isAllowMultySel = NO;
    
    [wantChildBtn setHighlighted:YES];
    
    [self performSelector:@selector(addSettingView:) withObject:wantChildBtn afterDelay:0.1];
}

- (IBAction)didClickSmokingBtn:(id)sender
{
    self.editItemType = EDIT_PROFILE_SMOKING;
    [self loadItemArrayWithName:@"smoking_drinking"];
    
    profileSettingViewCtrl.itemArray = itemArray;
    profileSettingViewCtrl.parent = self;
    profileSettingViewCtrl.titleString = NSLocalizedString(@"Smoking",@"EditProfileView");
    profileSettingViewCtrl.isAllowMultySel = NO;
    
    [smokingBtn setHighlighted:YES];
    
    [self performSelector:@selector(addSettingView:) withObject:smokingBtn afterDelay:0.1];
}

- (IBAction)didClickDrinkingBtn:(id)sender
{
    self.editItemType = EDIT_PROFILE_DRINKING;
    [self loadItemArrayWithName:@"drinking_smoking"];
    
    profileSettingViewCtrl.itemArray = itemArray;
    profileSettingViewCtrl.parent = self;
    profileSettingViewCtrl.titleString = NSLocalizedString(@"Drinking",@"EditProfileView");
    profileSettingViewCtrl.isAllowMultySel = NO;
   
    [drinkingBtn setHighlighted:YES];
    
    [self performSelector:@selector(addSettingView:) withObject:drinkingBtn afterDelay:0.1];
}

- (IBAction)didClickReligionBtn:(id)sender
{
    self.editItemType = EDIT_PROFILE_RELIGION;
    [self loadItemArrayWithName:@"religion"];
    
    profileSettingViewCtrl.itemArray = itemArray;
    profileSettingViewCtrl.parent = self;
    profileSettingViewCtrl.titleString = NSLocalizedString(@"Religion",@"EditProfileView");
    profileSettingViewCtrl.isAllowMultySel = NO;
   
    [religionBtn setHighlighted:YES];
    
    [self performSelector:@selector(addSettingView:) withObject:religionBtn afterDelay:0.1];
}

- (IBAction)didClickBodyBtn:(id)sender
{
    self.editItemType = EDIT_PROFILE_BODY_TYPE;
    [self loadItemArrayWithName:@"bodytype"];
    
    profileSettingViewCtrl.itemArray = itemArray;
    profileSettingViewCtrl.parent = self;
    profileSettingViewCtrl.titleString = NSLocalizedString(@"Body type",@"EditProfileView");
    profileSettingViewCtrl.isAllowMultySel = NO;
    
    [bodyBtn setHighlighted:YES];
    
    [self performSelector:@selector(addSettingView:) withObject:bodyBtn afterDelay:0.1];
}

- (IBAction)didClickHairBtn:(id)sender
{
    self.editItemType = EDIT_PROFILE_HAIR_STYLE;
    [self loadItemArrayWithName:@"hairstyle"];
    
    profileSettingViewCtrl.itemArray = itemArray;
    profileSettingViewCtrl.parent = self;
    profileSettingViewCtrl.titleString = NSLocalizedString(@"Hair style",@"EditProfileView");
    profileSettingViewCtrl.isAllowMultySel = NO;
    
    [hairBtn setHighlighted:YES];
    
    [self performSelector:@selector(addSettingView:) withObject:hairBtn afterDelay:0.1];
}

- (IBAction)didClickFoodsBtn:(id)sender
{
    self.editItemType = EDIT_PROFILE_FOODS;

    
    NSString *_language = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    if ([_language hasPrefix:@"zh-Hant"]||[_language hasPrefix:@"zh-HK"]||[_language hasPrefix:@"id"]){
    
         [self loadItemArrayWithName:@"fav_food1"];
     }
    else
        [self loadItemArrayWithName:@"fav_food"];
    
    profileSettingViewCtrl.itemArray = itemArray;
    profileSettingViewCtrl.parent = self;
    profileSettingViewCtrl.titleString = NSLocalizedString(@"My favorite foods are",@"EditProfileView");
    profileSettingViewCtrl.isAllowMultySel = YES;
    
    [foodsBtn setHighlighted:YES];
    
    [self performSelector:@selector(addSettingView:) withObject:foodsBtn afterDelay:0.1];
}

- (IBAction)didClickHobbiesBtn:(id)sender
{
    self.editItemType = EDIT_PROFILE_HOBBIES;
    
     NSString *_language = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    if ([_language hasPrefix:@"zh-Hant"]||[_language hasPrefix:@"zh-HK"]||[_language hasPrefix:@"id"]){
    
        [self loadItemArrayWithName:@"fav_hobby1"];
    }
    else
    [self loadItemArrayWithName:@"fav_hobby"];
    
    profileSettingViewCtrl.itemArray = itemArray;
    profileSettingViewCtrl.parent = self;
    profileSettingViewCtrl.titleString = NSLocalizedString(@"My hobbies are",@"EditProfileView");
    profileSettingViewCtrl.isAllowMultySel = YES;
    
    [hobbiesBtn setHighlighted:YES];
    
    [self performSelector:@selector(addSettingView:) withObject:hobbiesBtn afterDelay:0.1];
}

- (IBAction)didClickWatchBtn:(id)sender
{
    self.editItemType = EDIT_PROFILE_WATCH;
    
     NSString *_language = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    if ([_language hasPrefix:@"zh-Hant"]||[_language hasPrefix:@"zh-HK"]||[_language hasPrefix:@"id"]){
    
        [self loadItemArrayWithName:@"fav_movie1"];
    }
    else
        [self loadItemArrayWithName:@"fav_movie"];
    
    profileSettingViewCtrl.itemArray = itemArray;
    profileSettingViewCtrl.parent = self;
    profileSettingViewCtrl.titleString = NSLocalizedString(@"I like to watch",@"EditProfileView");
    profileSettingViewCtrl.isAllowMultySel = YES;
    
    [watchBtn setHighlighted:YES];
    
    [self performSelector:@selector(addSettingView:) withObject:watchBtn afterDelay:0.1];
}

- (IBAction)didClickListenBtn:(id)sender
{
    self.editItemType = EDIT_PROFILE_LISTEN;
    
     NSString *_language = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    if ([_language hasPrefix:@"zh-Hant"]||[_language hasPrefix:@"zh-HK"]||[_language hasPrefix:@"id"]){
    
        [self loadItemArrayWithName:@"fav_music1"];
    }
    else
        [self loadItemArrayWithName:@"fav_music"];
    
    profileSettingViewCtrl.itemArray = itemArray;
    profileSettingViewCtrl.parent = self;
    profileSettingViewCtrl.titleString = NSLocalizedString(@"I like to listen to",@"EditProfileView");
    profileSettingViewCtrl.isAllowMultySel = YES;
   
    [listenBtn setHighlighted:YES];
    
    [self performSelector:@selector(addSettingView:) withObject:listenBtn afterDelay:0.1];
}

- (IBAction)didClickHolidayBtn:(id)sender
{
    self.editItemType = EDIT_PROFILE_SPEND_HOLIDAY;
    
    NSString *_language = [[NSLocale preferredLanguages] objectAtIndex:0];

    if ([_language hasPrefix:@"zh-Hant"]||[_language hasPrefix:@"zh-HK"]||[_language hasPrefix:@"id"]){
    
        [self loadItemArrayWithName:@"fav_holiday1"];
    }
    else
        [self loadItemArrayWithName:@"fav_holiday"];
    
    profileSettingViewCtrl.itemArray = itemArray;
    profileSettingViewCtrl.parent = self;
    profileSettingViewCtrl.titleString = NSLocalizedString(@"I like to spend my holidays",@"EditProfileView");
    profileSettingViewCtrl.isAllowMultySel = YES;
   
    [holidayBtn setHighlighted:YES];
    
    [self performSelector:@selector(addSettingView:) withObject:holidayBtn afterDelay:0.1];
}

- (IBAction)didClickPetBtn:(id)sender
{
    self.editItemType = EDIT_PROFILE_PET;
    [self loadItemArrayWithName:@"fav_pet"];
    
    profileSettingViewCtrl.itemArray = itemArray;
    profileSettingViewCtrl.parent = self;
    profileSettingViewCtrl.titleString = NSLocalizedString(@"My favorite pet is",@"EditProfileView");
    profileSettingViewCtrl.isAllowMultySel = NO;
  
    [petBtn setHighlighted:YES];
    
    [self performSelector:@selector(addSettingView:) withObject:petBtn afterDelay:0.1];
}

- (IBAction)didClickSportsBtn:(id)sender
{
    self.editItemType = EDIT_PROFILE_SPORTS;
    
     NSString *_language = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    if ([_language hasPrefix:@"zh-Hant"]||[_language hasPrefix:@"zh-HK"]||[_language hasPrefix:@"id"]){
    
        [self loadItemArrayWithName:@"fav_sport1"];
    }
    else
        [self loadItemArrayWithName:@"fav_sport"];
    
    profileSettingViewCtrl.itemArray = itemArray;
    profileSettingViewCtrl.parent = self;
    profileSettingViewCtrl.titleString = NSLocalizedString(@"My favorite sports are",@"EditProfileView");
    profileSettingViewCtrl.isAllowMultySel = YES;

    [sportsBtn setHighlighted:YES];

    [self performSelector:@selector(addSettingView:) withObject:sportsBtn afterDelay:0.1];
}

- (void)dealloc
{
    [scrlView release];
    [contentsView release];

    [ethnicityLabel release];
    [nationalitLabel release];
    [firstLangLabel release];
    [otherLangLabel release];
    [educationLabel release];
    [incomeLabel release];
    [professionLabel release];
    [travelLabel release];
    [exerciseLabel release];
    [haveChildLabel release];
    [wantChildLabel release];
    [smokingLabel release];
    [drinkingLabel release];
    [religionLabel release];
    [bodyLabel release];
    [hairLabel release];
    [foodsLabel release];
    [hobbiesLabel release];
    [watchLabel release];
    [listenLabel release];
    [holidayLabel release];
    [petLabel release];
    [sportsLabel release];
    [heightTf release];
    [weightTf release];
    
    [ethnicityBtn release];
    [nationalitBtn release];
    [firstLangBtn release];
    [otherLangBtn release];
    [educationBtn release];
    [incomeBtn release];
    [professionBtn release];
    [travelBtn release];
    [exerciseBtn release];
    [haveChildBtn release];
    [wantChildBtn release];
    [smokingBtn release];
    [drinkingBtn release];
    [religionBtn release];
    [bodyBtn release];
    [hairBtn release];
    [foodsBtn release];
    [hobbiesBtn release];
    [watchBtn release];
    [listenBtn release];
    [holidayBtn release];
    [petBtn release];
    [sportsBtn release];
    
    [self.ethnicitySelStr release];
    [self.nationalitSelStr release];
    [self.firstLangSelStr release];
    [self.otherLangSelStr release];
    [self.educationSelStr release];
    [self.incomeSelStr release];
    [self.professionSelStr release];
    [self.travelSelStr release];
    [self.exerciseSelStr release];
    [self.haveChildSelStr release];
    [self.wantChildSelStr release];
    [self.smokingSelStr release];
    [self.drinkingSelStr release];
    [self.religionSelStr release];
    [self.bodySelStr release];
    [self.hairSelStr release];
    [self.foodsSelStr release];
    [self.hobbiesSelStr release];
    [self.watchSelStr release];
    [self.listenSelStr release];
    [self.holidaySelStr release];
    [self.petSelStr release];
    [self.sportsSelStr release];

    [profileSettingViewCtrl release];
    [itemArray release];
    
    [editItemArray release];
    
    [parent release];
    
    [self.dicProfileInfo release];
    
    [super dealloc];
}

@end
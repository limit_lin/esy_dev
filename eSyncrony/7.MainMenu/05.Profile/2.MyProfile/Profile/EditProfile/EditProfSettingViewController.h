

#import <UIKit/UIKit.h>

@class EditProfileViewController;
@class eSynchronyFilterViewController;

@interface EditProfSettingViewController : GAITrackedViewController
{
    IBOutlet UIView         *selView;
    IBOutlet UIImageView    *deviderView;
    IBOutlet UIButton       *okBtn;
    IBOutlet UILabel        *okLabel;
    IBOutlet UILabel        *titleLabel;
}

@property (nonatomic, strong) NSMutableArray            *itemArray;
@property (nonatomic, strong) EditProfileViewController *parent;
@property (nonatomic, strong) eSynchronyFilterViewController *filterParent;

@property (nonatomic, strong) NSString                  *titleString;
@property (nonatomic, strong) IBOutlet UITableView      *tblView;
@property (nonatomic, assign) BOOL                      isAllowMultySel;
@property (nonatomic, strong) NSString                  *selNumStr;

- (IBAction)didClickOKBtn:(id)sender;

- (void)initSetting;

@end

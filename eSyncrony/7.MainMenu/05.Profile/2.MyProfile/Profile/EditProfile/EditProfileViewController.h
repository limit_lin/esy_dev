
#import <UIKit/UIKit.h>

@class EditProfSettingViewController;
@class ProfileViewController;

@interface EditProfileViewController : GAITrackedViewController <UITextFieldDelegate>
{
    IBOutlet UIScrollView   *scrlView;
    IBOutlet UIView         *contentsView;

    IBOutlet UITextField    *heightTf;
    IBOutlet UITextField    *weightTf;

    IBOutlet UIButton       *ethnicityBtn;
    IBOutlet UIButton       *nationalitBtn;
    IBOutlet UIButton       *firstLangBtn;
    IBOutlet UIButton       *otherLangBtn;
    IBOutlet UIButton       *educationBtn;
    IBOutlet UIButton       *incomeBtn;
    IBOutlet UIButton       *professionBtn;
    IBOutlet UIButton       *travelBtn;
    IBOutlet UIButton       *exerciseBtn;
    IBOutlet UIButton       *haveChildBtn;
    IBOutlet UIButton       *wantChildBtn;
    IBOutlet UIButton       *smokingBtn;
    IBOutlet UIButton       *drinkingBtn;
    IBOutlet UIButton       *religionBtn;
    IBOutlet UIButton       *bodyBtn;
    IBOutlet UIButton       *hairBtn;
    IBOutlet UIButton       *foodsBtn;
    IBOutlet UIButton       *hobbiesBtn;
    IBOutlet UIButton       *watchBtn;
    IBOutlet UIButton       *listenBtn;
    IBOutlet UIButton       *holidayBtn;
    IBOutlet UIButton       *petBtn;
    IBOutlet UIButton       *sportsBtn;

    EditProfSettingViewController   *profileSettingViewCtrl;
    NSMutableArray                  *itemArray;
    
    //NSString               *language;//append
}

@property (nonatomic, strong) NSDictionary      *dicProfileInfo;

@property (nonatomic, strong) IBOutlet UILabel        *ethnicityLabel;
@property (nonatomic, strong) IBOutlet UILabel        *nationalitLabel;
@property (nonatomic, strong) IBOutlet UILabel        *firstLangLabel;
@property (nonatomic, strong) IBOutlet UILabel        *otherLangLabel;
@property (nonatomic, strong) IBOutlet UILabel        *educationLabel;
@property (nonatomic, strong) IBOutlet UILabel        *incomeLabel;
@property (nonatomic, strong) IBOutlet UILabel        *professionLabel;
@property (nonatomic, strong) IBOutlet UILabel        *travelLabel;
@property (nonatomic, strong) IBOutlet UILabel        *exerciseLabel;
@property (nonatomic, strong) IBOutlet UILabel        *haveChildLabel;
@property (nonatomic, strong) IBOutlet UILabel        *wantChildLabel;
@property (nonatomic, strong) IBOutlet UILabel        *smokingLabel;
@property (nonatomic, strong) IBOutlet UILabel        *drinkingLabel;
@property (nonatomic, strong) IBOutlet UILabel        *religionLabel;
@property (nonatomic, strong) IBOutlet UILabel        *bodyLabel;
@property (nonatomic, strong) IBOutlet UILabel        *hairLabel;
@property (nonatomic, strong) IBOutlet UILabel        *foodsLabel;
@property (nonatomic, strong) IBOutlet UILabel        *hobbiesLabel;
@property (nonatomic, strong) IBOutlet UILabel        *watchLabel;
@property (nonatomic, strong) IBOutlet UILabel        *listenLabel;
@property (nonatomic, strong) IBOutlet UILabel        *holidayLabel;
@property (nonatomic, strong) IBOutlet UILabel        *petLabel;
@property (nonatomic, strong) IBOutlet UILabel        *sportsLabel;

@property (nonatomic, strong) NSString          *handphoneStr;
@property (nonatomic, strong) NSString          *ethnicitySelStr;
@property (nonatomic, strong) NSString          *nationalitSelStr;
@property (nonatomic, strong) NSString          *firstLangSelStr;
@property (nonatomic, strong) NSString          *otherLangSelStr;
@property (nonatomic, strong) NSString          *educationSelStr;
@property (nonatomic, strong) NSString          *incomeSelStr;
@property (nonatomic, strong) NSString          *professionSelStr;
@property (nonatomic, strong) NSString          *travelSelStr;
@property (nonatomic, strong) NSString          *exerciseSelStr;
@property (nonatomic, strong) NSString          *haveChildSelStr;
@property (nonatomic, strong) NSString          *wantChildSelStr;
@property (nonatomic, strong) NSString          *smokingSelStr;
@property (nonatomic, strong) NSString          *drinkingSelStr;
@property (nonatomic, strong) NSString          *religionSelStr;
@property (nonatomic, strong) NSString          *bodySelStr;
@property (nonatomic, strong) NSString          *hairSelStr;
@property (nonatomic, strong) NSString          *foodsSelStr;
@property (nonatomic, strong) NSString          *hobbiesSelStr;
@property (nonatomic, strong) NSString          *watchSelStr;
@property (nonatomic, strong) NSString          *listenSelStr;
@property (nonatomic, strong) NSString          *holidaySelStr;
@property (nonatomic, strong) NSString          *petSelStr;
@property (nonatomic, strong) NSString          *sportsSelStr;

@property (nonatomic, strong) NSMutableArray    *editItemArray;
@property (nonatomic, assign) NSInteger         editItemType;

@property (nonatomic, strong) ProfileViewController *parent;

- (IBAction)didClickBackBtn:(id)sender;
- (IBAction)didClickSaveBtn:(id)sender;

- (IBAction)didClickEthnicityBtn:(id)sender;
- (IBAction)didClickNationalitBtn:(id)sender;
- (IBAction)didClickFirstLangBtn:(id)sender;
- (IBAction)didClickOtherLangBtn:(id)sender;
- (IBAction)didClickEducationBtn:(id)sender;
- (IBAction)didClickIncomeBtn:(id)sender;
- (IBAction)didClickProfessionBtn:(id)sender;
- (IBAction)didClickTravelBtn:(id)sender;
- (IBAction)didClickExerciseBtn:(id)sender;
- (IBAction)didClickHaveChildBtn:(id)sender;
- (IBAction)didClickWantChildBtn:(id)sender;
- (IBAction)didClickSmokingBtn:(id)sender;
- (IBAction)didClickDrinkingBtn:(id)sender;
- (IBAction)didClickReligionBtn:(id)sender;
- (IBAction)didClickBodyBtn:(id)sender;
- (IBAction)didClickHairBtn:(id)sender;
- (IBAction)didClickFoodsBtn:(id)sender;
- (IBAction)didClickHobbiesBtn:(id)sender;
- (IBAction)didClickWatchBtn:(id)sender;
- (IBAction)didClickListenBtn:(id)sender;
- (IBAction)didClickHolidayBtn:(id)sender;
- (IBAction)didClickPetBtn:(id)sender;
- (IBAction)didClickSportsBtn:(id)sender;

- (void)loadProfileInfo;
- (void)loadItemArrayWithName:(NSString *)name;
- (void)receiveEditResultWithSelectedItemNumString:(NSString *)selNumStr;
- (void)saveProfileWithStep:(NSInteger)step;
- (void)updateBasic;

+(NSString *)p_stringDeleteString:(NSString *)str;

@end

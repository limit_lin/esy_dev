
#import <UIKit/UIKit.h>
#import "ProfileBaseViewController.h"

@interface ProfileViewController : ProfileBaseViewController
{    
    IBOutlet UIImageView    *profileImage;


    BOOL    m_bIsInit;
    BOOL    isDocVerified;
    float   _fLastY;
    NSString *industry;
    NSString *photoPermission;
    NSDictionary            *_dicProfileInfo;
}
@property(retain,nonatomic)NSString *edu_verify;
@property(retain,nonatomic)NSString *id_verify;
@property(retain,nonatomic)NSString *income_verify;
@property(retain,nonatomic)NSString *work_verify;
@property(retain,nonatomic)NSString *marriage_verify;

@property(retain,nonatomic)NSString *edu_status;
@property(retain,nonatomic)NSString *id_status;
@property(retain,nonatomic)NSString *income_status;
@property(retain,nonatomic)NSString *work_status;
@property(retain,nonatomic)NSString *marriage_status;

- (IBAction)didClickEditProfile:(id)sender;

- (IBAction)didClickChangePw:(id)sender;

- (IBAction)didClickUploadDoc:(id)sender;

- (IBAction)didClickMatchSetting:(id)sender;

- (IBAction)didClickPhotoImage:(id)sender;

- (void)refreshContent;

@end


#import <UIKit/UIKit.h>

@class MatchSettingViewController;

@interface MSSelectViewController : GAITrackedViewController
{
    IBOutlet UITableView    *tbleView;
}

@property (nonatomic, assign) NSMutableArray    *itemArray;
@property (nonatomic, assign) MatchSettingViewController *parent;


@end

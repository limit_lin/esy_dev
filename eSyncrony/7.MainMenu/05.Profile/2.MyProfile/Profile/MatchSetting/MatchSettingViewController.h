
#import <UIKit/UIKit.h>

@class MSSelectViewController;

@interface MatchSettingViewController : GAITrackedViewController
{
    IBOutlet UIScrollView *scrlView;
    IBOutlet UIView *contentsView;
    
    IBOutlet UIButton       *ageBtn;
    IBOutlet UIButton       *heightBtn;
    IBOutlet UIButton       *distanceBtn;
    IBOutlet UIButton       *religionBtn;
    IBOutlet UIButton       *ethnicityBtn;
    IBOutlet UIButton       *incomeBtn;
    IBOutlet UIButton       *educationBtn;
    IBOutlet UIButton       *childrenBtn;
    IBOutlet UIButton       *marialBtn;
    IBOutlet UIButton       *smokeBtn;
    IBOutlet UIButton       *drinkBtn;
    IBOutlet UIButton       *exerciseBtn;
    
    MSSelectViewController  *msSelViewCtrl;
    
    NSMutableArray          *itemArray1;
    NSMutableArray          *itemArray2;
    NSMutableArray          *arrWarnMessage;
}

@property (nonatomic, strong) IBOutlet UILabel        *ageLabel;
@property (nonatomic, strong) IBOutlet UILabel        *heightLabel;
@property (nonatomic, strong) IBOutlet UILabel        *distanceLabel;
@property (nonatomic, strong) IBOutlet UILabel        *religionLabel;
@property (nonatomic, strong) IBOutlet UILabel        *ethnicityLabel;
@property (nonatomic, strong) IBOutlet UILabel        *incomeLabel;
@property (nonatomic, strong) IBOutlet UILabel        *educationLabel;
@property (nonatomic, strong) IBOutlet UILabel        *childrenLabel;
@property (nonatomic, strong) IBOutlet UILabel        *marialLabel;
@property (nonatomic, strong) IBOutlet UILabel        *smokeLabel;
@property (nonatomic, strong) IBOutlet UILabel        *drinkLabel;
@property (nonatomic, strong) IBOutlet UILabel        *exerciseLabel;

@property (nonatomic)  NSInteger editItemType;
@property (nonatomic)  NSInteger selIdx;

- (IBAction)didClickBackBtn:(id)sender;
- (IBAction)didClickSaveBtn:(id)sender;

- (IBAction)didClickAgeBtn:(id)sender;
- (IBAction)didClickHeightBtn:(id)sender;
- (IBAction)didClickDistanceBtn:(id)sender;
- (IBAction)didClickReligionBtn:(id)sender;
- (IBAction)didClickEthnicityBtn:(id)sender;
- (IBAction)didClickIncomeBtn:(id)sender;
- (IBAction)didClickEducationBtn:(id)sender;
- (IBAction)didClickChildrenBtn:(id)sender;
- (IBAction)didClickMarialBtn:(id)sender;
- (IBAction)didClickSmokeBtn:(id)sender;
- (IBAction)didClickDrinkBtn:(id)sender;
- (IBAction)didClickExerciseBtn:(id)sender;

- (void)receiveEditResult;

@end


#import "MSSelectViewController.h"
#import "MatchSettingViewController.h"
#import "RadioBoxCell.h"

@interface MSSelectViewController ()

@end

@implementation MSSelectViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"MSSelectView";
    // IOS7
    //CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    //tbleView.frame = CGRectMake(tbleView.frame.origin.x, tbleView.frame.origin.y + (screenHeight - 480) / 2, tbleView.frame.size.width, tbleView.frame.size.height);
    
    tbleView.layer.cornerRadius = 5;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [tbleView reloadData];
}

#pragma mark --- Table Functions ---
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if( self.itemArray == nil )
        return 0;
    
	return [self.itemArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RadioBoxCell    *cell;
    NSArray         *arr = [[NSBundle mainBundle] loadNibNamed:@"RadioBoxCell" owner:nil options:nil];
    cell = [arr objectAtIndex:0];
    
    if( self.itemArray != nil )
        cell.reasonLabel.text = [self.itemArray objectAtIndex:indexPath.row];
    else
        cell.reasonLabel.text = @"";
    
    UIImage *img;
    if( self.parent.selIdx == indexPath.row )
    {
        img = [UIImage imageNamed:@"btnRadio_p"];
        [cell.radioImgView initWithImage:img];
    }
    else
    {
        img = [UIImage imageNamed:@"btnRadio_n"];
        [cell.radioImgView initWithImage:img];
    }

    return cell;
}

- (void)procTblSelectCell
{
    [self.view removeFromSuperview];
    
    [self.parent receiveEditResult];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView reloadData];
 
    self.parent.selIdx = indexPath.row;
    
    [self performSelector:@selector(procTblSelectCell) withObject:nil afterDelay:0.2];
}

- (void)dealloc
{
    [tbleView release];

    [super dealloc];
}

@end

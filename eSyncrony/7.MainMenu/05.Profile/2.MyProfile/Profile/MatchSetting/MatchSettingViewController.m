
#import "MatchSettingViewController.h"
#import "MSSelectViewController.h"

#import "DSActivityView.h"
#import "Global.h"
#import "UtilComm.h"
#import "ImageUtil.h"

#import "eSyncronyAppDelegate.h"
#import "mainMenuViewController.h"

@interface MatchSettingViewController ()

@end

@implementation MatchSettingViewController

@synthesize ageLabel;
@synthesize heightLabel;
@synthesize distanceLabel;
@synthesize religionLabel;
@synthesize ethnicityLabel;
@synthesize incomeLabel;
@synthesize educationLabel;
@synthesize childrenLabel;
@synthesize marialLabel;
@synthesize smokeLabel;
@synthesize drinkLabel;
@synthesize exerciseLabel;

@synthesize editItemType, selIdx;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"MatchSettingView";
    [scrlView addSubview:contentsView];
    scrlView.contentSize = contentsView.frame.size;

    NSString        *_strPath =[[NSBundle mainBundle] pathForResource:@"criteria_list" ofType:@"plist"];
    NSDictionary    *_dic = [NSDictionary dictionaryWithContentsOfFile:_strPath];
    itemArray1 = [[NSMutableArray alloc] initWithArray:[_dic objectForKey:@"Array"]];
    
    itemArray2 = [[NSMutableArray alloc] initWithCapacity:0];
    [itemArray2 addObject:NSLocalizedString(@"Not Open",@" MatchSettingView")];
    [itemArray2 addObject:NSLocalizedString(@"Slightly Open",@" MatchSettingView")];
    [itemArray2 addObject:NSLocalizedString(@"Moderately Open",@" MatchSettingView")];
    [itemArray2 addObject:NSLocalizedString(@"Very Open",@" MatchSettingView")];
    [itemArray2 addObject:NSLocalizedString(@"Extremely Open",@" MatchSettingView")];

    arrWarnMessage = [[NSMutableArray alloc] initWithCapacity:0];
//    [arrWarnMessage addObject:NSLocalizedString(@"Restricting your matches' age will result in less matches for you.",@" MatchSettingView")];
//    [arrWarnMessage addObject:NSLocalizedString(@"This option may result in less matches for you.",@" MatchSettingView")];
//    [arrWarnMessage addObject:@""];
//    [arrWarnMessage addObject:NSLocalizedString(@"You will only have matches with the same religion as you",@" MatchSettingView")];
//    [arrWarnMessage addObject:NSLocalizedString(@"You will have matches belonging to your race only",@" MatchSettingView")];
//    [arrWarnMessage addObject:NSLocalizedString(@"Choosing a more stringent income criteria may result in less matches for you.",@" MatchSettingView")];
//    [arrWarnMessage addObject:NSLocalizedString(@"Restricting your matches' education level will result in less matches for you",@" MatchSettingView")];
//    [arrWarnMessage addObject:@""];
//    [arrWarnMessage addObject:@""];
//    [arrWarnMessage addObject:NSLocalizedString(@"We will match you with members who do NOT smoke.",@" MatchSettingView")];
//    [arrWarnMessage addObject:NSLocalizedString(@"We will match you with members who do NOT drink.",@" MatchSettingView")];
//    [arrWarnMessage addObject:@""];
    //age
    [arrWarnMessage addObject:NSLocalizedString(@"You may consider relaxing the ‘age’ criteria to ‘not important’ or ‘slightly important’ in order to get more matches.",@"MatchSettingView")];
    //height
    [arrWarnMessage addObject:NSLocalizedString(@"You may consider relaxing the ‘height’ criteria to ‘not important’ or ‘slightly important’ in order to get more matches.",@"MatchSettingView")];
    //distance
    [arrWarnMessage addObject:NSLocalizedString(@"You may consider relaxing the ‘distance’ criteria to ‘not important’ or ‘slightly important’ in order to get more matches.",@"MatchSettingView")];
    //religion
    [arrWarnMessage addObject:NSLocalizedString(@"You may consider relaxing the ‘religion’ criteria to ‘not important’ or ‘slightly important’ in order to get more matches.",@"MatchSettingView")];
    //ethnicity
    [arrWarnMessage addObject:NSLocalizedString(@"You may consider relaxing the ‘ethnicity’ criteria to ‘not important’ or ‘slightly important’ in order to get more matches.",@"MatchSettingView")];
    //income
    [arrWarnMessage addObject:NSLocalizedString(@"You may consider relaxing the ‘income’ criteria to ‘not important’ or ‘slightly important’ in order to get more matches.",@"MatchSettingView")];
    //education
    [arrWarnMessage addObject:NSLocalizedString(@"You may consider relaxing the ‘education level’ criteria to ‘not important’ or ‘slightly important’ in order to get more matches.",@"MatchSettingView")];
    //having children
    [arrWarnMessage addObject:NSLocalizedString(@"You may consider relaxing the ‘having children’ criteria to ‘not important’ or ‘slightly important’ in order to get more matches.",@"MatchSettingView")];
    //marial openness
   [arrWarnMessage addObject:NSLocalizedString(@"You may consider relaxing the ‘marial openness’ criteria to ‘not important’ or ‘slightly important’ in order to get more matches.",@"MatchSettingView")];
    
    //smoking
    [arrWarnMessage addObject:NSLocalizedString(@"You may consider relaxing the ‘smoking’ criteria to ‘extremely open’ and ‘very open’ in order to get more matches.",@"MatchSettingView")];
    
    //drinking
    [arrWarnMessage addObject:NSLocalizedString(@"You may consider relaxing the ‘drinking’ criteria to ‘extremely open’ and ‘very open’ in order to get more matches.",@"MatchSettingView")];
    
    //exercise habit
    [arrWarnMessage addObject:NSLocalizedString(@"You may consider relaxing the ‘exercise habit’ criteria to ‘not important’ or ‘slightly important’ in order to get more matches.",@"MatchSettingView")];

    msSelViewCtrl = [[MSSelectViewController alloc] initWithNibName:@"MSSelectViewController" bundle:nil];
    msSelViewCtrl.view.frame = self.view.bounds;
    msSelViewCtrl.parent = self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];//151125
    
    ageLabel.text = @"";
    heightLabel.text = @"";
    distanceLabel.text = @"";
    religionLabel.text = @"";
    ethnicityLabel.text = @"";
    incomeLabel.text = @"";
    educationLabel.text = @"";
    childrenLabel.text = @"";
    marialLabel.text = @"";
    smokeLabel.text = @"";
    drinkLabel.text = @"";
    exerciseLabel.text = @"";

    [self performSelector:@selector(ShowActivity) withObject:nil afterDelay:0.1];
}

- (void)ShowActivity
{
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(esyncMatchSetting_loadInfos) withObject:nil afterDelay:0.1];
}

- (void)esyncMatchSetting_loadInfos
{
    [DSBezelActivityView removeViewAnimated:NO];
    NSDictionary* result = [UtilComm viewMatchingCriteria];
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Error"];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( [response isKindOfClass:[NSString class]] && ([response hasPrefix:@"ERROR"]||[response hasPrefix:@"ERROR"]))
    {
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
        return;
    }
    
    NSDictionary *ListArr = response;
    
    int age = MAX(1, [[ListArr objectForKey:@"age"] intValue]);
    int children = MAX(1, [[ListArr objectForKey:@"children"] intValue]);
    int distance = MAX(1, [[ListArr objectForKey:@"distance"] intValue]);
    int drink = MAX(1, [[ListArr objectForKey:@"drink"] intValue]);
    int education = MAX(1, [[ListArr objectForKey:@"education"] intValue]);
    int ethnicity = MAX(1, [[ListArr objectForKey:@"ethnicity"] intValue]);
    int exercise = MAX(1, [[ListArr objectForKey:@"exercise"] intValue]);
    int height = MAX(1, [[ListArr objectForKey:@"height"] intValue]);
    int income = MAX(1, [[ListArr objectForKey:@"income"] intValue]);
    int mstatus = MAX(1, [[ListArr objectForKey:@"mstatus"] intValue]);
    int religion = MAX(1, [[ListArr objectForKey:@"religion"] intValue]);
    int smoke = MAX(1, [[ListArr objectForKey:@"smoke"] intValue]);
    
    ageLabel.text = [itemArray1 objectAtIndex:age-1];
    heightLabel.text = [itemArray1 objectAtIndex:height-1];
    distanceLabel.text = [itemArray1 objectAtIndex:distance-1];
    religionLabel.text = [itemArray1 objectAtIndex:religion-1];
    ethnicityLabel.text = [itemArray1 objectAtIndex:ethnicity-1];
    incomeLabel.text = [itemArray1 objectAtIndex:income-1];
    educationLabel.text = [itemArray1 objectAtIndex:education-1];
    childrenLabel.text = [itemArray1 objectAtIndex:children-1];
    marialLabel.text = [itemArray1 objectAtIndex:mstatus-1];
    smokeLabel.text = [itemArray2 objectAtIndex:smoke-1];
    drinkLabel.text = [itemArray2 objectAtIndex:drink-1];
    exerciseLabel.text = [itemArray1 objectAtIndex:exercise-1];
}

- (void)alertWarnMessage:(int)nMsgIndx
{
    NSString *strMsg = [arrWarnMessage objectAtIndex:nMsgIndx];
    if( [strMsg isEqualToString:@""] )
        return;
    
    if( nMsgIndx == 9 || nMsgIndx == 10 )
    {
//        if( self.selIdx != 0 )
    if( self.selIdx > 1 )
            return;
    }
    else
    {
        if( self.selIdx < 3 )
            return;
    }
    
    [ErrorProc alertMessage:strMsg withTitle:@"eSynchrony"];
}

- (void)receiveEditResult
{
    NSString *str = @"";
    
    if( editItemType == MATCH_SETTING_SMOKE || editItemType == MATCH_SETTING_DRINK )
        str = [itemArray2 objectAtIndex:self.selIdx];
    else
        str = [itemArray1 objectAtIndex:self.selIdx];

    switch( editItemType )
    {
        case MATCH_SETTING_AGE:
            ageLabel.text = str;
            [self alertWarnMessage:0];
            break;
        case MATCH_SETTING_HEIGHT:
            heightLabel.text = str;
            [self alertWarnMessage:1];
            break;
        case MATCH_SETTING_DISTANCE:
            distanceLabel.text = str;
            [self alertWarnMessage:2];
            break;
        case MATCH_SETTING_RELIGION:
            religionLabel.text = str;
            [self alertWarnMessage:3];
            break;
        case MATCH_SETTING_ETHNICITY:
            ethnicityLabel.text = str;
            [self alertWarnMessage:4];
            break;
        case MATCH_SETTING_INCOME:
            incomeLabel.text = str;
            [self alertWarnMessage:5];
            break;
        case MATCH_SETTING_EDUCATION:
            educationLabel.text = str;
            [self alertWarnMessage:6];
            break;
        case MATCH_SETTING_CHIDREN:
            childrenLabel.text = str;
            [self alertWarnMessage:7];
            break;
        case MATCH_SETTING_MARIAL:
            marialLabel.text = str;
            [self alertWarnMessage:8];
            break;
        case MATCH_SETTING_SMOKE:
            smokeLabel.text = str;
            [self alertWarnMessage:9];
            break;
        case MATCH_SETTING_DRINK:
            drinkLabel.text = str;
            [self alertWarnMessage:10];
            break;
        case MATCH_SETTING_EXERCISE:
            exerciseLabel.text = str;
            [self alertWarnMessage:11];
            break;
    }
}

- (int)getIndexOfString:(NSString*)string
{
    int     i;
    
    for( i = 0; i < [itemArray1 count]; i++ )
    {
        if( [string isEqualToString:[itemArray1 objectAtIndex:i]] )
            return i+1;
    }
    
    for( i = 0; i < [itemArray2 count]; i++ )
    {
        if( [string isEqualToString:[itemArray2 objectAtIndex:i]] )
            return i+1;
    }
    
    return 1;
}

- (void)setSelectedItemIndexInCaseOfString:(NSString *)string
{
    selIdx = [self getIndexOfString:string]-1;
}

- (IBAction)didClickBackBtn:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)didClickSaveBtn:(id)sender
{
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Uploading...",)];
    [self performSelector:@selector(uploadInfos) withObject:nil afterDelay:0.1];
}

- (void)uploadInfos
{
    [DSBezelActivityView removeView];

    int age         = [self getIndexOfString:ageLabel.text];
    int children    = [self getIndexOfString:childrenLabel.text];
    int distance    = [self getIndexOfString:distanceLabel.text];
    int drink       = [self getIndexOfString:drinkLabel.text];
    int education   = [self getIndexOfString:educationLabel.text];
    int ethnicity   = [self getIndexOfString:ethnicityLabel.text];
    int exercise    = [self getIndexOfString:exerciseLabel.text];
    int height      = [self getIndexOfString:heightLabel.text];
    int income      = [self getIndexOfString:incomeLabel.text];
    int mstatus     = [self getIndexOfString:marialLabel.text];
    int religion    = [self getIndexOfString:religionLabel.text];
    int smoke       = [self getIndexOfString:smokeLabel.text];
    
    NSDictionary* result = [UtilComm updateMatchingCriteria:age
                                                     height:height
                                                   distance:distance
                                                   religion:religion
                                                  ethnicity:ethnicity
                                                     income:income
                                                  education:education
                                                   children:children
                                                    mstatus:mstatus
                                                      smoke:smoke
                                                      drink:drink
                                                  excercise:exercise];
    
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Error"];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
        return;
    }
    
    if ([response isEqualToString:@"OK"])
    {
        //@"Update Match Criteria"
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"eSynchrony"
                                                     message:NSLocalizedString(@"Match Settings updated succesfully.",@" MatchSettingView")
                                                    delegate:self
                                           cancelButtonTitle:NSLocalizedString(@"OK",)
                                           otherButtonTitles:nil];
        [av show];
        [av release];
    }
    else
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"ERROR",)];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)addSettingView:(UIButton *)button
{
    [button setHighlighted:NO];
    
    [self.view performSelector:@selector(addSubview:) withObject:msSelViewCtrl.view afterDelay:0.1];
}

- (IBAction)didClickAgeBtn:(id)sender
{
    self.editItemType = MATCH_SETTING_AGE;
    
    [self setSelectedItemIndexInCaseOfString:ageLabel.text];
    msSelViewCtrl.itemArray = itemArray1;
    
    [ageBtn setHighlighted:YES];
    
    [self performSelector:@selector(addSettingView:) withObject:ageBtn afterDelay:0.1];
}

- (IBAction)didClickHeightBtn:(id)sender
{
    self.editItemType = MATCH_SETTING_HEIGHT;
    [self setSelectedItemIndexInCaseOfString:heightLabel.text];
    msSelViewCtrl.itemArray = itemArray1;
    
    [heightBtn setHighlighted:YES];
    
    [self performSelector:@selector(addSettingView:) withObject:heightBtn afterDelay:0.1];
}

- (IBAction)didClickDistanceBtn:(id)sender
{
    self.editItemType = MATCH_SETTING_DISTANCE;
    [self setSelectedItemIndexInCaseOfString:distanceLabel.text];
    msSelViewCtrl.itemArray = itemArray1;
    
    [distanceBtn setHighlighted:YES];
    
    [self performSelector:@selector(addSettingView:) withObject:distanceBtn afterDelay:0.1];
}

- (IBAction)didClickReligionBtn:(id)sender
{
    self.editItemType = MATCH_SETTING_RELIGION;
    [self setSelectedItemIndexInCaseOfString:religionLabel.text];
    msSelViewCtrl.itemArray = itemArray1;
    
    [religionBtn setHighlighted:YES];
    
    [self performSelector:@selector(addSettingView:) withObject:religionBtn afterDelay:0.1];
}

- (IBAction)didClickEthnicityBtn:(id)sender
{
    self.editItemType = MATCH_SETTING_ETHNICITY;
    [self setSelectedItemIndexInCaseOfString:ethnicityLabel.text];
    msSelViewCtrl.itemArray = itemArray1;
    
    [ethnicityBtn setHighlighted:YES];
    
    [self performSelector:@selector(addSettingView:) withObject:ethnicityBtn afterDelay:0.1];
}

- (IBAction)didClickIncomeBtn:(id)sender
{
    self.editItemType = MATCH_SETTING_INCOME;
    [self setSelectedItemIndexInCaseOfString:incomeLabel.text];
    msSelViewCtrl.itemArray = itemArray1;
    
    [incomeBtn setHighlighted:YES];
    
    [self performSelector:@selector(addSettingView:) withObject:incomeBtn afterDelay:0.1];
}

- (IBAction)didClickEducationBtn:(id)sender
{
    self.editItemType = MATCH_SETTING_EDUCATION;
    [self setSelectedItemIndexInCaseOfString:educationLabel.text];
    msSelViewCtrl.itemArray = itemArray1;
    
    [educationBtn setHighlighted:YES];
    
    [self performSelector:@selector(addSettingView:) withObject:educationBtn afterDelay:0.1];
}

- (IBAction)didClickChildrenBtn:(id)sender
{
    self.editItemType = MATCH_SETTING_CHIDREN;
    [self setSelectedItemIndexInCaseOfString:childrenLabel.text];
    msSelViewCtrl.itemArray = itemArray1;
    
    [childrenBtn setHighlighted:YES];
    
    [self performSelector:@selector(addSettingView:) withObject:childrenBtn afterDelay:0.1];
}

- (IBAction)didClickMarialBtn:(id)sender
{
    self.editItemType = MATCH_SETTING_MARIAL;
    [self setSelectedItemIndexInCaseOfString:marialLabel.text];
    msSelViewCtrl.itemArray = itemArray1;
    
    [marialBtn setHighlighted:YES];
    
    [self performSelector:@selector(addSettingView:) withObject:marialBtn afterDelay:0.1];
}

- (IBAction)didClickSmokeBtn:(id)sender
{
    self.editItemType = MATCH_SETTING_SMOKE;
    [self setSelectedItemIndexInCaseOfString:smokeLabel.text];
    msSelViewCtrl.itemArray = itemArray2;
    
    [smokeBtn setHighlighted:YES];
    
    [self performSelector:@selector(addSettingView:) withObject:smokeBtn afterDelay:0.1];
}

- (IBAction)didClickDrinkBtn:(id)sender
{
    self.editItemType = MATCH_SETTING_DRINK;
    [self setSelectedItemIndexInCaseOfString:drinkLabel.text];
    msSelViewCtrl.itemArray = itemArray2;
    
    [drinkBtn setHighlighted:YES];
    
    [self performSelector:@selector(addSettingView:) withObject:drinkBtn afterDelay:0.1];
}

- (IBAction)didClickExerciseBtn:(id)sender
{
    self.editItemType = MATCH_SETTING_EXERCISE;
    
    [self setSelectedItemIndexInCaseOfString:exerciseLabel.text];
    msSelViewCtrl.itemArray = itemArray1;

    [exerciseBtn setHighlighted:YES];

    [self performSelector:@selector(addSettingView:) withObject:exerciseBtn afterDelay:0.1];
}

- (void)dealloc
{
    [scrlView release];
    [contentsView release];
    
    [ageBtn release];
    [heightBtn release];
    [distanceBtn release];
    [religionBtn release];
    [ethnicityBtn release];
    [incomeBtn release];
    [educationBtn release];
    [childrenBtn release];
    [marialBtn release];
    [smokeBtn release];
    [drinkBtn release];
    [exerciseBtn release];
    
    [ageLabel release];
    [heightLabel release];
    [distanceLabel release];
    [religionLabel release];
    [ethnicityLabel release];
    [incomeLabel release];
    [educationLabel release];
    [childrenLabel release];
    [marialLabel release];
    [smokeLabel release];
    [drinkLabel release];
    [exerciseLabel release];
    
    [itemArray1 release];
    [itemArray2 release];
    [arrWarnMessage release];
    
    [msSelViewCtrl release];
    
    [super dealloc];
}

@end

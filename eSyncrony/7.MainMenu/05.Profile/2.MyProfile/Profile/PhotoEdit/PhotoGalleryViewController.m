
#import "PhotoGalleryViewController.h"
#import "AddPhotoViewController.h"
#import "PhotoEditViewController.h"

#import "DSActivityView.h"
#import "Global.h"
#import "UtilComm.h"
#import "ImageUtil.h"

#import "eSyncronyAppDelegate.h"
#import "mainMenuViewController.h"

@interface PhotoGalleryViewController ()
{
    UIActivityIndicatorView* activityIndicator;
}

@end

@implementation PhotoGalleryViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"PhotoGalleryView";
    activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.frame = selImageView.bounds;

    bInitView = FALSE;
    
    if( self.strMatchAccNo == nil )
        self.strMatchAccNo = @"";
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([self.photoPermission isEqualToString:@"Y"]) {
        [btnPermission setTitle:NSLocalizedString(@"Set as Private Photo",@"PhotoGalleryView") forState:UIControlStateNormal];
        [btnPermission setTitle:NSLocalizedString(@"Set as Private Photo",@"PhotoGalleryView") forState:UIControlStateSelected];
    }else{
        [btnPermission setTitle:NSLocalizedString(@"Set as Public Photo",@"PhotoGalleryView") forState:UIControlStateNormal];
        [btnPermission setTitle:NSLocalizedString(@"Set as Public Photo",@"PhotoGalleryView") forState:UIControlStateSelected];
    }
    
    if( bInitView == TRUE )
        return;
    
    if( ![self.strMatchAccNo isEqualToString:@""] )
    {
        btnAdd.hidden = YES;
        btnEdit.hidden = YES;
        btnDelete.hidden = YES;
        btnSetPrimary.hidden = YES;
        btnPermission.hidden = YES;
    }

    [self refreshContent];
}

- (void)refreshContent
{
    for( UIView* view in galleryScrlView.subviews )
        [view removeFromSuperview];

    //[DSBezelActivityView newActivityViewForView:self.view withLabel:@"Loading..."];
    [self showBusyDialogWithTitle:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(loadAllPhotosProc) withObject:nil afterDelay:0.1];

    bInitView = TRUE;
}

- (void)loadAllPhotosProc
{
    //[DSBezelActivityView removeViewAnimated:NO];
    [self hideBusyDialog];

    NSDictionary* result = [UtilComm retrieveAllPhotos:self.strMatchAccNo];
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Error"];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        if( [response isEqualToString:@"ERROR 0"] ||
           [response isEqualToString:@"ERROR 1"] ||
           [response isEqualToString:@"ERROR 2"] ||
           [response isEqualToString:@"ERROR 3"] )
            [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
        else
            [self emptyGallery];    
        return;
    }
    
    NSDictionary    *dicResponse = (NSDictionary*)response;
    if( dicResponse == nil || ![dicResponse isKindOfClass:[NSDictionary class]] )
    {
        //[ErrorProc alertMessage:@"Unknown Error" withTitle:@"ERROR"];
        [self emptyGallery];
        return;
    }
    [arrayPhotosInfo release];
    arrayPhotosInfo = [[NSMutableArray alloc] initWithCapacity:0];
    
    id items = [dicResponse objectForKey:@"item"];
    if( ![items isKindOfClass:[NSArray class]] )
    {
        //[ErrorProc alertMessage:@"Unknown Error" withTitle:@"ERROR"];
        NSDictionary    *dicItem = (NSDictionary*)items;
        [arrayPhotosInfo addObject:dicItem];
//        [self emptyGallery];
//        return;
    }else{
        for( int i = 0; i < [items count]; i++ )
        {
            NSDictionary            *item = [items objectAtIndex:i];
            NSMutableDictionary     *mutItem = [NSMutableDictionary dictionaryWithDictionary:item];
            [arrayPhotosInfo addObject:mutItem];
        }
    }

    [self initGallery];
}

- (void)emptyGallery
{
    [contentView addSubview:emptyView];
    userNameLabel.text = self.strUserName;
}

- (void)initGallery
{
    if( [arrayPhotosInfo count] == 0 )
    {
        [contentView addSubview:emptyView];
        userNameLabel.text = self.strUserName;
        return;
    }
    
    galleryScrlView.delegator = self;

    [contentView addSubview:galleryView];
    contentView.contentSize = galleryView.frame.size;
    for( int i = 0; i < [arrayPhotosInfo count]; i++ )
    //for( int i = 0; i < 1; i++ )
    {
        UIImageView *itemView = [[UIImageView alloc] initWithFrame:CGRectMake(i*80, 5, 80, 80)];
        itemView.image = [UIImage imageNamed:@"no_image.png"];
        [galleryScrlView addSubview:itemView];

        NSMutableDictionary     *item = [arrayPhotosInfo objectAtIndex:i];
        [item setObject:itemView forKey:@"itemview"];

        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,
                                             (unsigned long)NULL), ^(void) {
            [self downloadGalleryPhotoAtIndex:i];
        });
    }
    
    galleryScrlView.contentSize = CGSizeMake( 80*[arrayPhotosInfo count], 90 );
    
    [self selectGalleryAtIndex:0];
}

- (void)downloadGalleryPhotoAtIndex:(int)index
{
    NSMutableDictionary     *item = [arrayPhotosInfo objectAtIndex:index];
//  UIImageView *itemView = [item objectForKey:@"itemview"];

    NSString    *fileName = [item objectForKey:@"filename"];
    NSString    *ImageURL = WEBSERVICE_PHOTO_BASEURI;
    
    ImageURL = [ImageURL stringByAppendingString:fileName];
    
    NSString    *imgFilePath = [ImageUtil loadImagePathFromURL:ImageURL];
    UIImage     *image = [UIImage imageWithContentsOfFile:imgFilePath];
    if( image == nil )
        return;
    
#if 0
    itemView.image = image;
    
    if( nSelectIndex == index )
        selImageView.image = image;
#else
    [item setObject:imgFilePath forKey:@"localpath"];
    
    [self performSelectorOnMainThread:@selector(PhotoGallery_onDownLoadImage:) withObject:[NSNumber numberWithInt:index] waitUntilDone:NO];
#endif
}

- (void)PhotoGallery_onDownLoadImage:(NSNumber*)indxNumber
{
    int     index = [indxNumber intValue];
    
    NSDictionary    *item = [arrayPhotosInfo objectAtIndex:index];
    NSString        *imagePath = [item objectForKey:@"localpath"];
    UIImage         *image = [UIImage imageWithContentsOfFile:imagePath];
    if( image == nil )
        return;

    UIImageView *itemView = [item objectForKey:@"itemview"];
    itemView.image = image;
    if( nSelectIndex == index )
    {
        selImageView.image = image;
        
        [activityIndicator stopAnimating];
        [activityIndicator removeFromSuperview];
    }
}

- (void)selectGalleryAtIndex:(int)index
{
    if( index >= [arrayPhotosInfo count] )
        return;

    nSelectIndex = index;
    
    NSMutableDictionary     *item = [arrayPhotosInfo objectAtIndex:index];
    UIImageView *itemView = [item objectForKey:@"itemview"];

    selImageView.image = itemView.image;
    
    lblImageName.text = [item objectForKey:@"photo_name"];
    lblImageDescription.text = [item objectForKey:@"description"];
    
//    if ([lblImageName.text isEqualToString:nil]) {
    if (lblImageName.text.length == 0) {
    
        lblImageName.hidden=YES;
        
    }
//    if ([lblImageName.text isEqualToString:nil]) {
    if (lblImageName.text.length == 0) {
        lblImageDescription.hidden=YES;
    }
    
    if (lblImageDescription.hidden&&lblImageName.hidden) {
        
        selImageView.frame=CGRectMake(5, 5, 290, 246);
        
    }

    if( [item objectForKey:@"localpath"] == nil )
    {
        [activityIndicator startAnimating];
        [selImageView addSubview:activityIndicator];
    }
    else
    {
        [activityIndicator stopAnimating];
        [activityIndicator removeFromSuperview];
    }
}

- (void)refreshCurrentImage
{
    [self selectGalleryAtIndex:nSelectIndex];
}

- (IBAction)didClickBackBtn:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)didClickAddBtn:(id)sender
{
    AddPhotoViewController *addPhotoViewCtrl = [[[AddPhotoViewController alloc] initWithNibName:@"AddPhotoViewController" bundle:nil] autorelease];
    
    addPhotoViewCtrl.prevViewController = self;

    [self.navigationController pushViewController:addPhotoViewCtrl animated:YES];
}

- (IBAction)didClickEditBtn:(id)sender
{
    PhotoEditViewController *editPhotoViewCtrl = [[[PhotoEditViewController alloc] initWithNibName:@"PhotoEditViewController" bundle:nil] autorelease];
    
    NSMutableDictionary     *item = [arrayPhotosInfo objectAtIndex:nSelectIndex];
    
    editPhotoViewCtrl.dicPhotoInfo = item;
    editPhotoViewCtrl.prevViewController = self;
    
    [self.navigationController pushViewController:editPhotoViewCtrl animated:YES];
}

- (IBAction)didClickDeleteBtn:(id)sender
{
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"eSynchrony"
                                                 message:NSLocalizedString(@"Do you want to delete?",@"PhotoGalleryView")
                                                delegate:self
                                       cancelButtonTitle:NSLocalizedString(@"Yes",)
                                       otherButtonTitles:NSLocalizedString(@"No",), nil];
    [av show];
    [av release];
}



- (void)removePhotoProc
{
    [self hideBusyDialog];
    
    NSMutableDictionary     *item = [arrayPhotosInfo objectAtIndex:nSelectIndex];
    NSString *photo_id = [item objectForKey:@"photo_id"];
    
    NSDictionary* result = [UtilComm removeThisPhoto:photo_id];
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Error"];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
        return;
    }
    
    [self refreshContent];
}

//================================================================================================//
//================================================================================================//

- (void)setPrimaryPhotoProc
{

    NSMutableDictionary     *item = [arrayPhotosInfo objectAtIndex:nSelectIndex];
    NSString *photo_id = [item objectForKey:@"photo_id"];
    
    NSDictionary* result = [UtilComm setThisToPrimary:photo_id];
    [self hideBusyDialog];
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Error"];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
        return;
    }
    
    [ErrorProc alertMessage:NSLocalizedString(@"Your profile has been submitted and pending for approval.",@"PhotoGalleryView") withTitle:@"eSynchrony"];
}

- (IBAction)didClickSetAsPrimaryPhotoBtn:(id)sender
{
    [self showBusyDialogWithTitle:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(setPrimaryPhotoProc) withObject:nil afterDelay:0.1];
}

-(void)changePermission{
    NSString *status = @"N";
    if ([self.photoPermission isEqualToString:@"N"]) {
        status = @"Y";
    }
    NSDictionary* result = [UtilComm changePermission:status];
    
    [self hideBusyDialog];
    
    if( result == nil )
    {
        return;
    }
    NSString *response = [result objectForKey:@"response"];
    if( [response isEqualToString:@"Not Allowed"] ){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"eSynchrony" message:NSLocalizedString(@"To make your photos private, subscribe to our premium packages now!",@"PhotoGalleryView") delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",) otherButtonTitles:NSLocalizedString(@"OK",), nil];
        alert.tag = 2001;
        [alert show];
        [alert release];
        return;
    }
    
    
    if ([response isEqualToString:@"OK"]) {
        if ([self.photoPermission isEqualToString:@"N"]) {
            self.photoPermission = @"Y";
            [btnPermission setTitle:NSLocalizedString(@"Set as Private Photo",@"PhotoGalleryView") forState:UIControlStateNormal];
            [btnPermission setTitle:NSLocalizedString(@"Set as Private Photo",@"PhotoGalleryView") forState:UIControlStateSelected];
        }else{
            self.photoPermission = @"N";
            [btnPermission setTitle:NSLocalizedString(@"Set as Public Photo",@"PhotoGalleryView") forState:UIControlStateNormal];
            [btnPermission setTitle:NSLocalizedString(@"Set as Public Photo",@"PhotoGalleryView") forState:UIControlStateSelected];
        }
    }
}

- (void)displayUpgrade
{
    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl procUpgrade];
}

- (IBAction)didClickChangePermission:(id)sender {
    
    if (![[eSyncronyAppDelegate sharedInstance].membership isEqualToString:@"Premium"]) {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"eSynchrony" message:NSLocalizedString(@"To make your photos private, subscribe to our premium packages now!",@"PhotoGalleryView") delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",) otherButtonTitles:NSLocalizedString(@"OK",), nil];
        alert.tag = 2001;
        [alert show];
        [alert release];
        
    }else{
        
        [self showBusyDialogWithTitle:NSLocalizedString(@"Loading...",)];
        [self performSelector:@selector(changePermission) withObject:nil afterDelay:0.1];
    }
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==2001) {
        if (buttonIndex==1) {
            [self.navigationController popViewControllerAnimated:NO];
            [self.navigationController popViewControllerAnimated:NO];
            [self performSelector:@selector(displayUpgrade) withObject:nil afterDelay:0.1];
        }
        return;
    }
    if( buttonIndex == 0 )
    {
        [self showBusyDialogWithTitle:NSLocalizedString(@"Loading...",)];
        [self performSelector:@selector(removePhotoProc) withObject:nil afterDelay:0.1];
    }
}
- (void)dealloc
{
    [contentView release];
    [emptyView release];
    [galleryView release];
    
    [userNameLabel release];
    [selImageView release];
    [galleryScrlView release];
    
    [self.strUserName release];
    [self.strMatchAccNo release];
    [self.photoPermission release];
    [btnPermission release];
    [super dealloc];
}

@end

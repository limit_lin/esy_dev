
#import "InputBaseViewController.h"
#import "PhotoGalleryViewController.h"

@interface PhotoEditViewController : InputBaseViewController
{
    IBOutlet UIImageView    *imgView;

    IBOutlet UITextField    *txtPhotoName;
    IBOutlet UITextView     *txtViewDescription;
    
    IBOutlet UIImageView    *bgDescription;
}

@property (nonatomic, strong) NSMutableDictionary  *dicPhotoInfo;
@property (nonatomic, assign) PhotoGalleryViewController*  prevViewController;

- (IBAction)onClickBack:(id)sender;

- (IBAction)onClickUpdate:(id)sender;

@end

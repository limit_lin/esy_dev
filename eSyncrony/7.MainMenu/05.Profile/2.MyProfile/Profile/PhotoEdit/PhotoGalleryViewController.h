
#import <UIKit/UIKit.h>
#import "GalleryScrollView.h"
#import "HttpBaseViewController.h"

@interface PhotoGalleryViewController : HttpBaseViewController
{
    IBOutlet UIScrollView       *contentView;
    
    IBOutlet UIView             *galleryView;
    IBOutlet UIImageView        *selImageView;
    IBOutlet GalleryScrollView  *galleryScrlView;
    IBOutlet UILabel            *lblImageName;
    IBOutlet UILabel            *lblImageDescription;

    IBOutlet UIView             *emptyView;
    IBOutlet UILabel            *userNameLabel;
    
    IBOutlet UIButton           *btnAdd;
    IBOutlet UIButton           *btnEdit;
    IBOutlet UIButton           *btnDelete;
    IBOutlet UIButton           *btnSetPrimary;
    IBOutlet UIButton           *btnPermission;
    
    NSMutableArray          *arrayPhotosInfo;
    int                     nSelectIndex;
    
    BOOL                    bInitView;
}

@property (nonatomic, strong) NSString  *strUserName;
@property (nonatomic, strong) NSString  *strMatchAccNo;
@property (nonatomic, strong) NSString  *photoPermission;

- (void)refreshContent;

- (IBAction)didClickBackBtn:(id)sender;

- (IBAction)didClickAddBtn:(id)sender;

- (IBAction)didClickEditBtn:(id)sender;

- (IBAction)didClickDeleteBtn:(id)sender;

- (IBAction)didClickSetAsPrimaryPhotoBtn:(id)sender;

- (IBAction)didClickChangePermission:(id)sender;



- (void)selectGalleryAtIndex:(int)index;

- (void)refreshCurrentImage;

@end

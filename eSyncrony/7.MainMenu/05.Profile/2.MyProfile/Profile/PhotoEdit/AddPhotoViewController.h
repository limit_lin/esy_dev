

#import <UIKit/UIKit.h>
#import "PhotoGalleryViewController.h"

@interface AddPhotoViewController : GAITrackedViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate >
{
    IBOutlet UIImageView    *imgViewPhoto;
    IBOutlet UILabel *labelGender;
    
    UIImagePickerController *_imagePickerController;
    UIImage                 *_photoImage;
    BOOL                    _bAskMSG;
}

@property (nonatomic, assign) PhotoGalleryViewController*  prevViewController;

- (IBAction)onClickCamera:(id)sender;
- (IBAction)onClickAlbum:(id)sender;

- (IBAction)onClickBack:(id)sender;

@end

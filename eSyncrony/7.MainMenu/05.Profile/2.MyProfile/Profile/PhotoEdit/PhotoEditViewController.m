
#import "PhotoEditViewController.h"

#import "UIImage+Utils.h"
#import "DSActivityView.h"
#import "Global.h"
#import "UtilComm.h"
#import "ImageUtil.h"

#import "eSyncronyAppDelegate.h"
#import "mainMenuViewController.h"

@interface PhotoEditViewController ()

@end

@implementation PhotoEditViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"PhotoEditView";
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnTableView:)];
    tapRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapRecognizer];
    
    txtPhotoName.text = [self.dicPhotoInfo objectForKey:@"photo_name"];
    txtViewDescription.text = [self.dicPhotoInfo objectForKey:@"description"];
    
    NSString    *localPath = [self.dicPhotoInfo objectForKey:@"localpath"];
    if( localPath != nil )
        imgView.image = [UIImage imageWithContentsOfFile:localPath];
    
    if( imgView.image == nil )
        imgView.image = [UIImage imageNamed:@"no_image.png"];
    
    //txtViewDescription.layer.borderWidth = 1;
    //txtViewDescription.layer.cornerRadius = 5;
    //txtViewDescription.layer.borderColor = [UIColor lightGrayColor].CGColor;
    //txtViewDescription.backgroundColor = [UIColor whiteColor];
    txtViewDescription.backgroundColor = [UIColor clearColor];
    
    //txtPhotoName.layer.borderWidth = 1;
    //txtPhotoName.layer.cornerRadius = 5;
    //txtPhotoName.layer.borderColor = [UIColor lightGrayColor].CGColor;
    //txtPhotoName.backgroundColor = [UIColor whiteColor];
    txtPhotoName.backgroundColor = [UIColor clearColor];
    //bg_text.png
    
    UIEdgeInsets    contentInsets = UIEdgeInsetsMake(5, 5, 5, 5);
    
    UIImage     *bgImage = [UIImage imageNamed:@"bg_text.png"];
    bgImage = [bgImage resizableImageWithCapInsets:contentInsets resizingMode:UIImageResizingModeStretch];
    bgImage = [bgImage renderAtSize:bgDescription.frame.size];
    
    bgDescription.image = bgImage;
}

- (void)tapOnTableView:(UITapGestureRecognizer *)gesture
{
    [self.view endEditing:YES];
    [self restoreViewPosition];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [self restoreViewPosition];

    return NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGRect rect = [textField convertRect:textField.bounds toView:self.view];
    
    float   offset;
    
    offset = self.view.frame.size.height - (rect.size.height + 240);
    offset = offset - rect.origin.y;
    
    if( offset > 0 )
        return;
    
    rect = self.view.frame;
    rect.origin.y = offset;
    
    self.view.frame = rect;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    CGRect rect = [textView convertRect:textView.bounds toView:self.view];
    
    float   offset;
    
    offset = self.view.frame.size.height - (rect.size.height + 226);
    offset = offset - rect.origin.y;
    
    if( offset > 0 )
        return;
    
    rect = self.view.frame;
    rect.origin.y = offset;
    
    self.view.frame = rect;
}

- (IBAction)onClickUpdate:(id)sender
{
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(updatePhotoProc) withObject:nil afterDelay:0.1];
}

- (void)updatePhotoProc
{
    [DSBezelActivityView removeViewAnimated:NO];

    NSString *photo_id = [self.dicPhotoInfo objectForKey:@"photo_id"];
    NSString *filename = [self.dicPhotoInfo objectForKey:@"filename"];
    NSString *visible = [self.dicPhotoInfo objectForKey:@"visible"];
    NSString *rating = [self.dicPhotoInfo objectForKey:@"rating"];
    
    NSDictionary* result = [UtilComm updatePhotoID:photo_id
                                          fileName:filename
                                         photoName:txtPhotoName.text
                                       description:txtViewDescription.text
                                           visible:visible
                                            rating:rating];
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Error"];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
        return;
    }
    
    [self.dicPhotoInfo setObject:txtPhotoName.text forKey:@"photo_name"];
    [self.dicPhotoInfo setObject:txtViewDescription.text forKey:@"description"];

    [ErrorProc alertMessage:NSLocalizedString(@"Photo details updated successfully.",@"PhotoEditView") withTitle:@"eSynchrony"];
}

- (IBAction)onClickBack:(id)sender
{
    [self.prevViewController refreshCurrentImage];

    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dealloc
{
    [imgView release];
    
    [txtPhotoName release];
    [txtViewDescription release];
    [bgDescription release];
    
    [self.dicPhotoInfo release];
    
    [super dealloc];
}

@end

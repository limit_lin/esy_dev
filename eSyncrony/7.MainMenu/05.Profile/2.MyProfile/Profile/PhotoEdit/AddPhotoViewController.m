
#import "AddPhotoViewController.h"

#import "DSActivityView.h"
#import "Global.h"
#import "UtilComm.h"
#import "ImageUtil.h"

#import "eSyncronyAppDelegate.h"
#import "mainMenuViewController.h"

@interface AddPhotoViewController ()

@end

@implementation AddPhotoViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"AddPhotoView";
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    

    if( [eSyncronyAppDelegate sharedInstance].gender == 1 )
    {
        [imgViewPhoto setImage:[UIImage imageNamed:@"female1.png"]];
        labelGender.text = NSLocalizedString(@"It is better to provide photos where you are well-dressed(e.g. during special events). This will increase the chances of approval as compared to those who are casually dressed by 3 times.", @"PhotoUploadView");
        
    }
    
//    else//male
//    {
//        [imgViewPhoto setImage:[UIImage imageNamed:@"male1.png"]];
//        labelGender.text = NSLocalizedString(@"It is better to provide photos where you're nicely dressed. You may look into uploading photos that were taken at a formal event like a wedding reception. This will help to increase your chances of approval and have 3 times more success rate as compared to those who are dressed casually.", @"PhotoUploadView");
//    }
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,
                                             (unsigned long)NULL), ^(void) {
        [self loadMyPhoto];
    });
}

- (void)loadMyPhoto
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    if (![[defaults objectForKey:@"userPhotoName"] isEqualToString:@"nophoto"]) {
        if([defaults objectForKey:@"userPhotoName"]!=nil){
            NSString*   imageURL = [NSString stringWithFormat:@"%@%@", WEBSERVICE_PHOTO_BASEURI, [defaults objectForKey:@"userPhotoName"]];
            NSString*   imagePath = [ImageUtil loadImagePathFromURL:imageURL];
            UIImage     *image = [UIImage imageWithContentsOfFile:imagePath];
            if( image == nil )
            {
                if( [eSyncronyAppDelegate sharedInstance].gender == 1 )
                    [imgViewPhoto setImage:[UIImage imageNamed:@"female1.png"]];
                return;
            }
            
            [imgViewPhoto setImage:image];
            imgViewPhoto.clipsToBounds = YES;
            imgViewPhoto.layer.cornerRadius = 55;
            return ;
        }
    }
    NSDictionary*   result = [UtilComm retrievePrimaryPhotoFilename];
    NSString*       response = [result objectForKey:@"response"];
    if( result == nil || response == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Authentication Failure"];
        return;
    }
    
    if( [response isEqualToString:@"no photo uploaded yet"]||[response isEqualToString:@"pending for approval"])
    {
        return;
    }

    NSString    *ImageURL = WEBSERVICE_PHOTO_BASEURI;
    ImageURL = [ImageURL stringByAppendingString:response];
    NSString    *imgFilePath = [ImageUtil loadImagePathFromURL:ImageURL];
    
    [self performSelectorOnMainThread:@selector(AddPhotoView_onDownLoadImage:) withObject:imgFilePath waitUntilDone:NO];
}

- (void)AddPhotoView_onDownLoadImage:(NSString*)imgFilePath
{
    UIImage         *image = [UIImage imageWithContentsOfFile:imgFilePath];
    if( image == nil )
        return;
    
    [imgViewPhoto setImage:image];
    imgViewPhoto.clipsToBounds = YES;
    imgViewPhoto.layer.cornerRadius = 55;

}

- (IBAction)onClickCamera:(id)sender
{
    if( _imagePickerController == nil )
    {
        if( [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] == FALSE )
        {
            [ErrorProc alertMessage:NSLocalizedString(@"Camera is not available",@"AddPhotoView") withTitle:NSLocalizedString(@"Error",)];
            
            return;
        }
        _imagePickerController = [[UIImagePickerController alloc] init];

    }
    _imagePickerController.delegate = self;
//#ifdef PHOTO_EDIT_ALLOW_MODE
    _imagePickerController.allowsEditing = YES;
//#endif
    _imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:_imagePickerController animated:YES completion:nil];

    
}

- (IBAction)onClickAlbum:(id)sender
{
    if( _imagePickerController == nil )
    {
        _imagePickerController = [[UIImagePickerController alloc] init];
        _imagePickerController.delegate = self;
#ifdef PHOTO_EDIT_ALLOW_MODE
        _imagePickerController.allowsEditing = YES;
#endif
    }
    
    _imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:_imagePickerController animated:YES completion:nil];
}

- (void)verifyPhotoProc
{
    //--------------------------------------------------------------//
    NSDictionary*    result = [UtilComm uploadNormalPhoto:_photoImage];
    [_photoImage release];
    _photoImage = nil;
    [DSBezelActivityView removeViewAnimated:NO];
    
    if( result == nil )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"We are unable to process your request at this moment. Please try again later.",@"AddPhotoView") withTitle:NSLocalizedString(@"Sorry",)];
        return;
    }
    
    id photo = [result objectForKey:@"photo"];
    id details = [photo objectForKey:@"details"];
    id errormsg = [details objectForKey:@"errormsg"];
    if( [errormsg isKindOfClass:[NSString class]] )
    {
        if( [errormsg isEqualToString:@"'SUCCESS'"] )
        {
            //[self moveNext];
            _bAskMSG = NO;
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"esync"
                                                         message:NSLocalizedString(@"Photo uploaded successfully and it is pending for approval!",@"AddPhotoView")
                                                        delegate:self
                                               cancelButtonTitle:NSLocalizedString(@"OK",)
                                               otherButtonTitles:nil];
            [av show];
            [av release];
        }
        else
            [ErrorProc alertMessage:errormsg withTitle:NSLocalizedString(@"Error",)];
    }
    else
    {
        [ErrorProc alertMessage:NSLocalizedString(@"We are unable to process your request at this moment. Please try again later.",@"AddPhotoView") withTitle:NSLocalizedString(@"Sorry",)];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if( _bAskMSG == YES )
    {
        if( buttonIndex == 1 )
        {
            [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Uploading...",)];
            [self performSelector:@selector(verifyPhotoProc) withObject:nil afterDelay:0.01];
        }
        else
        {
            [_photoImage release];
            _photoImage = nil;
        }
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
        [self.prevViewController refreshContent];
        
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker
        didFinishPickingImage:(UIImage *)image
                  editingInfo:(NSDictionary *)editingInfo
{
    [picker dismissViewControllerAnimated:NO completion:nil];
    
    _photoImage = [image retain];
    
    _bAskMSG = YES;
    
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Photo Upload"
                                                 message:NSLocalizedString(@"Confirm to upload this photo?",@"AddPhotoView")
                                                delegate:self
                                       cancelButtonTitle:NSLocalizedString(@"Cancel",)
                                       otherButtonTitles:NSLocalizedString(@"OK",), nil];
    [av show];
    [av release];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)onClickBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dealloc
{
    [imgViewPhoto release];
    [_imagePickerController release];
    
    [labelGender release];
    [super dealloc];
}

@end

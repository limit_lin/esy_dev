
#import "GalleryScrollView.h"
#import "PhotoGalleryViewController.h"

@interface GalleryScrollView ()

@end

@implementation GalleryScrollView

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = (UITouch *)[touches anyObject];
    CGPoint point = [touch locationInView:self];
    
    int index = point.x /80;
    
    PhotoGalleryViewController* viewController = (PhotoGalleryViewController*)self.delegator;

    [viewController selectGalleryAtIndex:index];
}

@end


#import "ProfileViewController.h"
#import "PhotoGalleryViewController.h"

#import "EditProfileViewController.h"
#import "MatchSettingViewController.h"
#import "DocCertViewController.h"
#import "ImageUtil.h"

#import "DSActivityView.h"
#import "Global.h"
#import "UtilComm.h"

#import "mainMenuViewController.h"
#import "eSyncronyAppDelegate.h"

@interface ProfileViewController ()


@end

@implementation ProfileViewController
@synthesize edu_verify;
@synthesize id_verify;
@synthesize work_verify;
@synthesize income_verify;
@synthesize marriage_verify;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"MyProfileView";
    
    isDocVerified = FALSE;
    [self showDefaultPhoto];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    if( m_bIsInit == NO )
    {
        [self refreshContent];
    }
}

- (void)refreshContent
{
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",@"")];
    [self performSelector:@selector(p_loadProfileInfo) withObject:nil afterDelay:0.01];
}

- (NSString *)getMatchStringOfPlist:(NSString *)plistName num:(NSString *)numString
{
    BOOL        isFirst = YES;
    NSString    *str = @"";
    
    NSString        *_strPath =[[NSBundle mainBundle] pathForResource:plistName ofType:@"plist"];
    NSDictionary    *_dicCountry = [NSDictionary dictionaryWithContentsOfFile:_strPath];
    
    NSMutableArray  *pArray = [[NSMutableArray alloc] init];
    [pArray addObjectsFromArray:[_dicCountry objectForKey:@"Array"]];
    
    NSArray *numArray = [numString componentsSeparatedByString:@"|"];
    
    for( NSString *substr in numArray )
    {
        if( isFirst )
            str = [pArray objectAtIndex:[substr integerValue]];
        else
            str = [NSString stringWithFormat:@"%@, %@",str, [pArray objectAtIndex:[substr integerValue]]];
        
        isFirst = NO;
    }
    
    return str;
}

- (void)showDefaultPhoto
{
    if( [eSyncronyAppDelegate sharedInstance].gender == 1 )
        [profileImage setImage:[UIImage imageNamed:@"female1.png"]];

}

- (void)loadMyPhoto
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    if (![[defaults objectForKey:@"userPhotoName"] isEqualToString:@"nophoto"]) {
        if([defaults objectForKey:@"userPhotoName"]!=nil){
            NSString*   imageURL = [NSString stringWithFormat:@"%@%@", WEBSERVICE_PHOTO_BASEURI, [defaults objectForKey:@"userPhotoName"]];
            NSString*   imagePath = [ImageUtil loadImagePathFromURL:imageURL];
            UIImage     *image = [UIImage imageWithContentsOfFile:imagePath];
            if( image == nil )
            {
                [self showDefaultPhoto];
                return;
            }
            
            [profileImage setImage:image];
            profileImage.clipsToBounds = YES;
            profileImage.layer.cornerRadius = 55;
            return ;
        }
    }
    NSDictionary*   result = [UtilComm retrievePrimaryPhotoFilename];
    NSString*       response = [result objectForKey:@"response"];
    
    if( result == nil || response == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"ERROR"];
        return;
    }
    
    if( [response isEqualToString:@"ERROR 0"] ||
       [response isEqualToString:@"ERROR 1"] ||
       [response isEqualToString:@"ERROR 2"] ||
       [response isEqualToString:@"ERROR 3"] )
    {
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
        return;
    }
    else if( [response isEqualToString:@"no photo uploaded yet"]||[response isEqualToString:@"pending for approval"])
    {
        [self showDefaultPhoto];
        return;
    }
    [defaults setObject:response forKey:@"userPhotoName"];
    NSString    *ImageURL = WEBSERVICE_PHOTO_BASEURI;
    ImageURL = [ImageURL stringByAppendingString:response];
    NSString    *imgFilePath = [ImageUtil loadImagePathFromURL:ImageURL];
    UIImage     *image = [UIImage imageWithContentsOfFile:imgFilePath];
    if( image == nil )
    {
        [self showDefaultPhoto];
        return;
    }
    
    [profileImage setImage:image];
    profileImage.clipsToBounds = YES;
    profileImage.layer.cornerRadius = 55;

}
- (void)p_loadProfileInfo
{
    [DSBezelActivityView removeViewAnimated:NO];

    NSDictionary* result = [UtilComm getProfileWithType:@"basic|lifestyle|appearance|interest|story"];
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"ERROR"];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    
   // NSLog(@"response====%@",response);
 
    
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
        return;
    }
    
    NSDictionary    *dicResponse = (NSDictionary*)response;
    if (dicResponse != nil ) {
        
        
        NSDictionary* verify_status=[response objectForKey:@"verify_status"];
        
       // NSLog(@"verify_status===%@",verify_status);
        
        edu_verify=[verify_status objectForKey:@"EDU_VERIFY"];
        id_verify=[verify_status objectForKey:@"ID_VERIFY"];
        income_verify=[verify_status objectForKey:@"INCOME_VERIFY"];
        work_verify=[verify_status objectForKey:@"WORK_VERIFY"];
        marriage_verify = [verify_status objectForKey:@"MVERIFY"];
        
//        NSLog(@"%@\n%@\n%@\n%@\n%@",edu_verify,id_verify,income_verify,work_verify,marriage_verify);
    }
    if( dicResponse == nil || ![dicResponse isKindOfClass:[NSDictionary class]] )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"ERROR",)];
        return;
    }
    
    [_dicProfileInfo release];
    _dicProfileInfo = [dicResponse retain];
    
    m_bIsInit = YES;

    [self showProfileInfo:dicResponse];
    [self loadMyPhoto];
}

- (void)showBasicInfo:(NSDictionary*)dicBasicInfo
{
    if( dicBasicInfo == nil )
        return;
    
    if( ![dicBasicInfo isKindOfClass:[NSDictionary class]] )
        return;
    
    photoPermission = [dicBasicInfo objectForKey:@"public_photo"];
    
    NSArray*    arrayBasic = [dicBasicInfo objectForKey:@"basic"];
    if( arrayBasic == nil || ![arrayBasic isKindOfClass:[NSArray class]] )
        return;
    
    [self addSubTitleView:NSLocalizedString(@"PERSONAL BACKGROUND",@"ProfileView")];

    NSString    *strGender = [dicBasicInfo objectForKey:@"gender"];
    if( [strGender isEqualToString:@"M"] )
    {
        [self addOneItemTitle:NSLocalizedString(@"Gender",) content:NSLocalizedString(@"Male",)];
        [eSyncronyAppDelegate sharedInstance].gender = 0;
    }
    else
    {
        [self addOneItemTitle:NSLocalizedString(@"Gender",) content:NSLocalizedString(@"Female",)];
        [eSyncronyAppDelegate sharedInstance].gender = 1;
    }
    [[eSyncronyAppDelegate sharedInstance] saveLoginInfo];
    
    NSString    *strDOB = [dicBasicInfo objectForKey:@"dob"];
    NSDate      *date = [Global convertStringToDate:strDOB withFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLog(@"date:%@",date);
    [self addOneItemTitle:NSLocalizedString(@"Birthday",@"ProfileView") content:[Global getDateTime:date withFormat:@"dd MMM yyyy"]];
  
    //=================================//

    NSString *_language = [[NSLocale preferredLanguages] objectAtIndex:0];

    if ([_language hasPrefix:@"zh-Hant"]||[_language hasPrefix:@"zh-HK"]||[_language hasPrefix:@"id"]) {//zh-Hant
        
        NSString *_strPath =[[NSBundle mainBundle] pathForResource:@"ethnicity_zhHant" ofType:@"plist"];
//        NSString *_strPath =[[NSBundle mainBundle] pathForResource:@"ethnicity_zhHant" ofType:@"xml"];
        NSDictionary *_dicCountry = [NSDictionary dictionaryWithContentsOfFile:_strPath];
        NSDictionary *p_ethnicity=[_dicCountry objectForKey:@"ethnicity"];
 
        [self addOneItemTitle:NSLocalizedString(@"Ethinicity",@"ProfileView") content:[p_ethnicity objectForKey:[arrayBasic objectAtIndex:7]]];
        
 
        p_ethnicity=[_dicCountry objectForKey:@"nationality"];
        
        if ([id_verify isEqualToString:@"1"]) {
            
            _id_status=NSLocalizedString(@"(Verified)",@"ProfileView");
            
            [self addOneItemTitle:NSLocalizedString(@"Nationality",@"ProfileView") content:[NSString stringWithFormat:@"%@　%@",[p_ethnicity objectForKey:[arrayBasic objectAtIndex:8]],_id_status]];
            
        }
        else
        {
            [self addOneItemTitle:NSLocalizedString(@"Nationality",@"ProfileView") content:[p_ethnicity objectForKey:[arrayBasic objectAtIndex:8]]];
        }
        //append marriage status
        if ([marriage_verify isEqualToString:@"Y"]||[marriage_verify isEqualToString:@"y"]) {
            
            _marriage_status = NSLocalizedString(@"(Verified)",@"ProfileView");
            
            [self addOneItemTitle:NSLocalizedString(@"Marital Status",@"ProfileView") content:[NSString stringWithFormat:@"%@　%@",[[_dicCountry objectForKey:@"marital_status"]objectForKey:[arrayBasic objectAtIndex:6]],_marriage_status]];
            
        }
        else
        {
            [self addOneItemTitle:NSLocalizedString(@"Marital Status",@"ProfileView") content:[[_dicCountry objectForKey:@"marital_status"]objectForKey:[arrayBasic objectAtIndex:6]]];
        }
        [self addOneItemTitle:NSLocalizedString(@"I can speak in",@"ProfileView") content:[[_dicCountry objectForKey:@"secondary_language"]objectForKey:[arrayBasic objectAtIndex:11]]];
        
//     [self addOneItemTitle:NSLocalizedString(@"Other language",@"ProfileView") content:[[_dicCountry objectForKey:@"secondary_language"]objectForKey:[arrayBasic objectAtIndex:10]]];

//    [self addOneItemTitle:NSLocalizedString(@"Other language",@"ProfileView") content:[arrayBasic objectAtIndex:10]];
        
        //===================translate other language=================//
#if 1
        NSArray*    items = [[arrayBasic objectAtIndex:10] componentsSeparatedByString:@","];
        if( items == 0 || [[arrayBasic objectAtIndex:10] isEqualToString:@""] )
            [self addOneItemTitle:NSLocalizedString(@"Other language",@"MatchProfileView") content:[arrayBasic objectAtIndex:10]];
        
        if ([items count] == 1) {
            [self addOneItemTitle:NSLocalizedString(@"Other language",@"MatchProfileView") content:[[_dicCountry objectForKey:@"secondary_language"]objectForKey:[arrayBasic objectAtIndex:10]]];
        }else
        {
            p_ethnicity=[_dicCountry objectForKey:@"secondary_language"];
            
            NSMutableString *p_content = [[NSMutableString alloc]init];
            NSString *p_toContent = [[NSString alloc]init];
            NSString *name = [[NSString alloc]init];
            for( int i = 0; i < [items count]; i++ )
            {
                NSString *str=[[NSString alloc]init];
                str = [[items objectAtIndex:i] stringByReplacingOccurrencesOfString:@" " withString:@""];
                name =[p_ethnicity objectForKey:str];
                
//                if( [name isEqualToString:@""]||[name isEqualToString:nil]||[name isEqualToString:NULL])
//                if( [name isEqualToString:@""]||(name == nil)||(name == NULL))
                if (name.length == 0)
                    break;
                [p_content appendString:[NSString stringWithFormat:@",%@",name]];
                //                NSLog(@"%@",p_content);
                
            }
            unichar c = [p_content characterAtIndex:0];
            NSRange range = NSMakeRange(0, 1);
            if (c == ',') { //此处可以是任何字符
                [p_content deleteCharactersInRange:range];
            }
            p_toContent = [p_content copy];
            
            [self addOneItemTitle:NSLocalizedString(@"Other language",@"MatchProfileView") content:p_toContent];
        }
#endif
        //=================================================//
        
        if ([edu_verify isEqualToString:@"1"]) {
            
            _edu_status=NSLocalizedString(@"(Verified)",@"ProfileView");
            
            [self addOneItemTitle:NSLocalizedString(@"Education",@"ProfileView") content:[NSString stringWithFormat:@"%@　%@",[[_dicCountry objectForKey:@"education"]objectForKey:[arrayBasic objectAtIndex:9]],_edu_status]];
            
        }
        else
        {
            [self addOneItemTitle:NSLocalizedString(@"Education",@"ProfileView") content:[[_dicCountry objectForKey:@"education"]objectForKey:[arrayBasic objectAtIndex:9]]];
        }
   
    }
    else//en
    {
        
        [self addOneItemTitle:NSLocalizedString(@"Ethinicity",@"ProfileView") content:[arrayBasic objectAtIndex:7]];
        
        if ([id_verify isEqualToString:@"1"]) {
            
            _id_status=NSLocalizedString(@"(Verified)",@"ProfileView");
            
            [self addOneItemTitle:NSLocalizedString(@"Nationality",@"ProfileView") content:[NSString stringWithFormat:@"%@　%@",[arrayBasic objectAtIndex:8],_id_status]];
            
        }
        else
        {
            [self addOneItemTitle:NSLocalizedString(@"Nationality",@"ProfileView") content:[arrayBasic objectAtIndex:8]];
        }
        
        //append marriage status
        if ([marriage_verify isEqualToString:@"Y"]||[marriage_verify isEqualToString:@"y"]) {
            
            _marriage_status = NSLocalizedString(@"(Verified)",@"ProfileView");
            [self addOneItemTitle:NSLocalizedString(@"Marital Status",@"ProfileView") content:[NSString stringWithFormat:@"%@　%@",[arrayBasic objectAtIndex:6],_marriage_status]];
            
        }
        else
        {
            [self addOneItemTitle:NSLocalizedString(@"Marital Status",@"ProfileView") content:[arrayBasic objectAtIndex:6]];
        }

        [self addOneItemTitle:NSLocalizedString(@"I can speak in",@"ProfileView") content:[arrayBasic objectAtIndex:11]];
        
        [self addOneItemTitle:NSLocalizedString(@"Other language",@"ProfileView") content:[arrayBasic objectAtIndex:10]];
        
        if ([edu_verify isEqualToString:@"1"]) {
            
            _edu_status= NSLocalizedString(@"(Verified)",@"ProfileView");
            
            [self addOneItemTitle:NSLocalizedString(@"Education",@"ProfileView") content:[NSString stringWithFormat:@"%@　%@",[arrayBasic objectAtIndex:9],_edu_status]];
            
        }
        else
        {
            [self addOneItemTitle:NSLocalizedString(@"Education",@"ProfileView") content:[arrayBasic objectAtIndex:9]];
        }
        
    }
}

- (void)showLifeStyleInfo:(NSArray*)arrayLifeStyleInfo//repair income,job
{
    if( arrayLifeStyleInfo == nil )
        return;
    
    if( ![arrayLifeStyleInfo isKindOfClass:[NSArray class]] )
        return;
    
    [self addSubTitleView:NSLocalizedString(@"LIFESTYLE",@"ProfileView")];
    
    NSString *_language = [[NSLocale preferredLanguages] objectAtIndex:0];
    
     if ([_language hasPrefix:@"zh-Hant"]||[_language hasPrefix:@"zh-HK"]||[_language hasPrefix:@"id"]) {
         
         NSString *_strPath =[[NSBundle mainBundle] pathForResource:@"ethnicity_zhHant" ofType:@"plist"];
//         NSString *_strPath =[[NSBundle mainBundle] pathForResource:@"ethnicity_zhHant" ofType:@"xml"];
         NSDictionary *_dicCountry = [NSDictionary dictionaryWithContentsOfFile:_strPath];
        // NSDictionary *p_ethnicity=[_dicCountry objectForKey:@"ethnicity"];
         
         if ([income_verify isEqualToString:@"1"]) {
             
             _income_status=NSLocalizedString(@"(Verified)",@"ProfileView");
             [self addOneItemTitle:NSLocalizedString(@"My income",@"ProfileView") content:[NSString stringWithFormat:@
                                                                                           "%@　%@",[arrayLifeStyleInfo objectAtIndex:0],_income_status]];
             
         }
         else
         {
             [self addOneItemTitle:NSLocalizedString(@"My income",@"ProfileView") content:[arrayLifeStyleInfo objectAtIndex:0]];
         }
         if ([ work_verify isEqualToString:@"1"]) {
             
             _work_status = NSLocalizedString(@"(Verified)",@"ProfileView");
             if ([[_dicCountry objectForKey:@"profession"] objectForKey:[arrayLifeStyleInfo objectAtIndex:1]] == nil) {
                 [self addOneItemTitle:NSLocalizedString(@"My job",@"ProfileView") content:[NSString stringWithFormat:@"%@　%@",[arrayLifeStyleInfo objectAtIndex:1],_work_status]];
             }else
                 [self addOneItemTitle:NSLocalizedString(@"My job",@"ProfileView") content:[NSString stringWithFormat:@"%@　%@",[[_dicCountry objectForKey:@"profession"] objectForKey:[arrayLifeStyleInfo objectAtIndex:1]],_work_status]];
             
         }
         else
         {
             if ([[_dicCountry objectForKey:@"profession"] objectForKey:[arrayLifeStyleInfo objectAtIndex:1]] == nil) {
                 [self addOneItemTitle:NSLocalizedString(@"My job",@"ProfileView") content:[arrayLifeStyleInfo objectAtIndex:1]];
             }else
                 [self addOneItemTitle:NSLocalizedString(@"My job",@"ProfileView") content:[[_dicCountry objectForKey:@"profession"] objectForKey:[arrayLifeStyleInfo objectAtIndex:1]]];
         }
         [self addOneItemTitle:NSLocalizedString(@"Industry",@"ProfileView") content:NSLocalizedString(industry,)];

         [self addOneItemTitle:NSLocalizedString(@"Smoking",@"ProfileView") content:[[_dicCountry objectForKey:@"smoking"] objectForKey:[arrayLifeStyleInfo objectAtIndex:3]]];
         [self addOneItemTitle:NSLocalizedString(@"Drinking",@"ProfileView") content:[[_dicCountry objectForKey:@"drinking"] objectForKey:[arrayLifeStyleInfo objectAtIndex:5]]];
         [self addOneItemTitle:NSLocalizedString(@"I Exercise",@"ProfileView") content:[[_dicCountry objectForKey:@"travel_exercise"] objectForKey:[arrayLifeStyleInfo objectAtIndex:4]]];
         [self addOneItemTitle:NSLocalizedString(@"I Travel",@"ProfileView") content:[[_dicCountry objectForKey:@"travel_exercise"] objectForKey:[arrayLifeStyleInfo objectAtIndex:8]]];
         [self addOneItemTitle:NSLocalizedString(@"Has Children",@"ProfileView") content:[[_dicCountry objectForKey:@"children_num_value"] objectForKey:[arrayLifeStyleInfo objectAtIndex:7]]];
         [self addOneItemTitle:NSLocalizedString(@"Wants Children",@"ProfileView") content:[[_dicCountry objectForKey:@"children_num_value"] objectForKey:[arrayLifeStyleInfo objectAtIndex:6]]];
         [self addOneItemTitle:NSLocalizedString(@"Religion",@"ProfileView") content:[[_dicCountry objectForKey:@"religion"] objectForKey:[arrayLifeStyleInfo objectAtIndex:9]]];
         
         
     }
    else//en
    {
        if ([income_verify isEqualToString:@"1"]) {
            
            _income_status = NSLocalizedString(@"(Verified)",@"ProfileView");
            [self addOneItemTitle:NSLocalizedString(@"My income",@"ProfileView") content:[NSString stringWithFormat:@
                                                                                          "%@　%@",[arrayLifeStyleInfo objectAtIndex:0],_income_status]];
            
        }
        else
        {
            [self addOneItemTitle:NSLocalizedString(@"My income",@"ProfileView") content:[arrayLifeStyleInfo objectAtIndex:0]];
        }
        if ([ work_verify isEqualToString:@"1"]) {
            
            _work_status = NSLocalizedString(@"(Verified)",@"ProfileView");
            
            [self addOneItemTitle:NSLocalizedString(@"My job",@"ProfileView") content:[NSString stringWithFormat:@"%@　%@",[arrayLifeStyleInfo objectAtIndex:1],_work_status]];
            
        }
        else
        {
            [self addOneItemTitle:NSLocalizedString(@"My job",@"ProfileView") content:[arrayLifeStyleInfo objectAtIndex:1]];
        }
        [self addOneItemTitle:NSLocalizedString(@"Industry",@"ProfileView") content:industry];
        
        [self addOneItemTitle:NSLocalizedString(@"Smoking",@"ProfileView") content:[arrayLifeStyleInfo objectAtIndex:3]];
        [self addOneItemTitle:NSLocalizedString(@"Drinking",@"ProfileView") content:[arrayLifeStyleInfo objectAtIndex:5]];
        [self addOneItemTitle:NSLocalizedString(@"I Exercise",@"ProfileView") content:[arrayLifeStyleInfo objectAtIndex:4]];
        [self addOneItemTitle:NSLocalizedString(@"I Travel",@"ProfileView") content:[arrayLifeStyleInfo objectAtIndex:8]];
        [self addOneItemTitle:NSLocalizedString(@"Has Children",@"ProfileView") content:[arrayLifeStyleInfo objectAtIndex:7]];
        [self addOneItemTitle:NSLocalizedString(@"Wants Children",@"ProfileView") content:[arrayLifeStyleInfo objectAtIndex:6]];
        [self addOneItemTitle:NSLocalizedString(@"Religion",@"ProfileView") content:[arrayLifeStyleInfo objectAtIndex:9]];
    }
    
}

- (void)showAppearanceInfo:(NSDictionary*)dicAppearanceInfo//apperance
{
    if( dicAppearanceInfo == nil )
        return;
    
    if( ![dicAppearanceInfo isKindOfClass:[NSDictionary class]] )
        return;
    
    NSArray*    arrayApperance = [dicAppearanceInfo objectForKey:@"appearance"];
    if( arrayApperance == nil || ![arrayApperance isKindOfClass:[NSArray class]] )
        return;
    
    [self addSubTitleView:NSLocalizedString(@"MY APPEARANCE",@"ProfileView")];
    
    [self addOneItemTitle:NSLocalizedString(@"Height",@"ProfileView") content:[NSString stringWithFormat:@"%@cm",[arrayApperance objectAtIndex:0]]];
    [self addOneItemTitle:NSLocalizedString(@"Weight",@"ProfileView") content:[NSString stringWithFormat:@"%@kg",[arrayApperance objectAtIndex:2]]];
    
    NSString *_language = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    if ([_language hasPrefix:@"zh-Hant"]||[_language hasPrefix:@"zh-HK"]||[_language hasPrefix:@"id"]) {
        
        NSString *_strPath =[[NSBundle mainBundle] pathForResource:@"ethnicity_zhHant" ofType:@"plist"];
//        NSString *_strPath =[[NSBundle mainBundle] pathForResource:@"ethnicity_zhHant" ofType:@"xml"];
        NSDictionary *_dicCountry = [NSDictionary dictionaryWithContentsOfFile:_strPath];
        NSDictionary *p_ethnicity=[_dicCountry objectForKey:@"bodytype"];
        
        [self addOneItemTitle:NSLocalizedString(@"Body type",@"ProfileView") content:[p_ethnicity objectForKey:[arrayApperance objectAtIndex:1]]];
        [self addOneItemTitle:NSLocalizedString(@"Hair Style",@"ProfileView") content:[[_dicCountry objectForKey:@"hairstyle"]objectForKey:[arrayApperance objectAtIndex:3]]];
    }
    else//en
    {
        [self addOneItemTitle:NSLocalizedString(@"Body type",@"ProfileView") content:[arrayApperance objectAtIndex:1]];
        [self addOneItemTitle:NSLocalizedString(@"Hair Style",@"ProfileView") content:[arrayApperance objectAtIndex:3]];
    }

}

- (void)showProfileInfo:(NSDictionary*)dicProfileInfo
{
    [self initContentViews];

    [self showBasicInfo:[dicProfileInfo objectForKey:@"basic"]];
    industry = [[dicProfileInfo objectForKey:@"basic"]objectForKey:@"industry"];
    [self showLifeStyleInfo:[dicProfileInfo objectForKey:@"lifestyle"]];
    [self showAppearanceInfo:[dicProfileInfo objectForKey:@"appearance"]];
    
    [self showInterestInfo:[dicProfileInfo objectForKey:@"interest"]];
    
    [self showStoryInfo:[dicProfileInfo objectForKey:@"story"]];
    
    scrlView.contentSize = CGSizeMake(scrlView.frame.size.width, _fLastYPos);
}

- (IBAction)didClickEditProfile:(id)sender
{
    EditProfileViewController *editProfileViewCtrl = [[EditProfileViewController alloc] initWithNibName:@"EditProfileViewController" bundle:nil];

    editProfileViewCtrl.parent = self;
    editProfileViewCtrl.dicProfileInfo =  _dicProfileInfo;

    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.navigationController pushViewController:editProfileViewCtrl animated:YES];
}

- (IBAction)didClickChangePw:(id)sender
{
    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl procMyAccount:YES];
}

- (void)showDocVerifyStep:(NSDictionary*)docCertInfo
{
    [Global loadBasicProfileInfoAndSaveToClient];
    
    DocCertViewController* docCertView = [[[DocCertViewController alloc] initWithNibName:@"DocCertViewController" bundle:nil] autorelease];

    docCertView.parentView = nil;
    docCertView.docCertInfo = docCertInfo;

    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.navigationController pushViewController:docCertView animated:YES];
}

- (void)enterDocCertStep
{
    [DSBezelActivityView removeViewAnimated:NO];

    if( isDocVerified == TRUE )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"You have completed upload your documents.\nThank you!",@"ProfileView") withTitle:@"eSynchrony"];
        return;
    }
    
    NSDictionary* result = [UtilComm getDocCertVerifyStatus];
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Authentication Failure"];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
        return;
    }
    
    NSString*   eduVerify = [response objectForKey:@"EDU_VERIFY"];
    NSString*   idVerify = [response objectForKey:@"ID_VERIFY"];
    NSString*   incomeVerify = [response objectForKey:@"INCOME_VERIFY"];
    NSString*   workVerify = [response objectForKey:@"WORK_VERIFY"];
    
    if( [idVerify isEqualToString:@"0"] )
        [eSyncronyAppDelegate sharedInstance].nIDVerify = 0;
    else
        [eSyncronyAppDelegate sharedInstance].nIDVerify = 1;
    [[eSyncronyAppDelegate sharedInstance] saveLoginInfo];
    
    // if there is exist item which not verified
    if( [eduVerify isEqualToString:@"0"] || [idVerify isEqualToString:@"0"] ||
       [incomeVerify isEqualToString:@"0"] || [workVerify isEqualToString:@"0"] )
    {
        isDocVerified = FALSE;
        [self showDocVerifyStep:response];
        return;
    }
    else
    {
        isDocVerified = TRUE;
        [ErrorProc alertMessage:NSLocalizedString(@"You have completed upload your documents.",@"ProfileView") withTitle:@"eSynchrony"];
    }
}

- (IBAction)didClickUploadDoc:(id)sender
{
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(enterDocCertStep) withObject:nil afterDelay:0.01];
}

- (IBAction)didClickMatchSetting:(id)sender
{
    MatchSettingViewController *matchSettingViewCtrl = [[[MatchSettingViewController alloc] initWithNibName:@"MatchSettingViewController" bundle:nil] autorelease];
    
    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.navigationController pushViewController:matchSettingViewCtrl animated:YES];
}

- (IBAction)didClickPhotoImage:(id)sender
{
    PhotoGalleryViewController *galleryViewController = [[[PhotoGalleryViewController alloc] initWithNibName:@"PhotoGalleryViewController" bundle:nil] autorelease];
    
    galleryViewController.strMatchAccNo = @"";
    galleryViewController.strUserName = [eSyncronyAppDelegate sharedInstance].strName;
    galleryViewController.photoPermission = photoPermission;
    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.navigationController pushViewController:galleryViewController animated:YES];
}

- (void)dealloc
{
    [profileImage release];

    [industry release];
    [photoPermission release];
    [_dicProfileInfo release];
    
    [super dealloc];
}

@end


#import "MatchProfileViewController.h"
#import "MatchPersonalityViewController.h"

#import "DeclineViewController.h"
#import "AskPhotoViewController.h"

#import "DSActivityView.h"
#import "Global.h"
#import "UtilComm.h"
#import "ImageUtil.h"

#import "eSyncronyAppDelegate.h"
#import "mainMenuViewController.h"

#import "UIProfilePhotoItemView.h"
#import "DetailMathProfileViewController.h"

#import "QAViewController.h"
#import "PhotoGalleryViewController.h"

#import "UIImageView+WebCache.h"

@implementation MatchProfileViewController

@synthesize myMatchesProposedCell;
@synthesize toName;

+ (MatchProfileViewController*)createWithMatchInfo:(NSMutableDictionary*)dicMatchInfo
{
    MatchProfileViewController*   viewController = [[[MatchProfileViewController alloc] initWithNibName:@"MatchProfileViewController" bundle:nil] autorelease];
    
    viewController.dicMatchInfo = dicMatchInfo;
    
    return viewController;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"MatchProfileView";
    [self initUI];
    
    toName = [_dicMatchInfo objectForKey:@"nname"];
    
    [self showBusyDialogWithTitle:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(LoadAllInfo) withObject:nil afterDelay:0.01];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.isAlreadyDecline) {
        btnTop1.hidden = YES;
    }
    NSString    *view_photo = [self.dicMatchInfo objectForKey:@"view_photo"];
    
    if( ![view_photo isEqualToString:@"photo view not allowed."] ){
        [btnAskPhoto setTitle:NSLocalizedString(@"View Photo",) forState:UIControlStateNormal];
        [btnAskPhoto setTitle:NSLocalizedString(@"View Photo",) forState:UIControlStateSelected];
        self.isPhotoAllowed = YES;
    }
}

- (void)initUI
{
    userNamelabel.text = [self.dicMatchInfo objectForKey:@"nname"];
  
    if( !self.m_bTopButton )
    {
        btnTop1.hidden = YES;
        btnTop2.hidden = YES;
        btnTop3.hidden = YES;
    }
//    if (!self.m_bDismissSelf) {
//        btnTop1.hidden = NO;
//        btnTop2.hidden = YES;
//        btnTop3.hidden = NO;
//    }
}

- (void)LoadAllInfo
{
    [self hideBusyDialog];

    [self initContentViews];

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,
                                             (unsigned long)NULL), ^(void) {
        [self downloadUserPhotos];
    });
    
    if( [self LoadCompatibility] == FALSE )
        return;

    if( [self LoadCommonInterestInfo] == FALSE )
        return;

    if( [self LoadViewMatchProfile] == FALSE )
        return;
}

- (BOOL)LoadCommonInterestInfo
{
    NSString*   macc_no = [self.dicMatchInfo objectForKey:@"acc_no"];
    if( macc_no == nil )
        macc_no = [self.dicMatchInfo objectForKey:@"Acc_no"];
    if( macc_no == nil )
        return NO;
    
    NSString        *strKey = [NSString stringWithFormat:@"%@_common_interest", macc_no];
    NSDictionary    *responseDic = [[Global sharedGlobal] getCacheInfoForKey:strKey];
    if( responseDic != nil )
    {
        [self showCommonInterest:responseDic];
        return YES;
    }
    
    NSDictionary *result = [UtilComm getCommonInterest:macc_no];
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Error"];
        return NO;
    }
    
    id response = [result objectForKey:@"response"];
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
        return NO;
    }

    responseDic = (NSDictionary*)response;
    
    [[Global sharedGlobal] setCacheInfo:responseDic forKey:strKey];
    [self showCommonInterest:responseDic];

    return YES;
}

- (BOOL)LoadViewMatchProfile
{
    NSString*   macc_no = [self.dicMatchInfo objectForKey:@"acc_no"];
    if( macc_no == nil )
        macc_no = [self.dicMatchInfo objectForKey:@"Acc_no"];
    if( macc_no == nil )
        return NO;
    
    NSString        *strKey = [NSString stringWithFormat:@"%@_match_profile", macc_no];
    NSDictionary    *responseDic = [[Global sharedGlobal] getCacheInfoForKey:strKey];
    if( responseDic != nil )
    {
        [self showProfileInfo:responseDic];
        return YES;
    }

    NSDictionary *result = [UtilComm viewMatchProfile:macc_no Type:@"basic|lifestyle|appearance|interest|story"];
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Error"];
        return NO;
    }
    
    id response = [result objectForKey:@"response"];
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
        return NO;
    }
    
    if( ![response isKindOfClass:[NSDictionary class]] )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"ERROR",)];
        return NO;
    }
    
    responseDic = (NSDictionary*)response;
    if (responseDic != nil ) {
        
        
        NSDictionary* verify_status=[response objectForKey:@"verify_status"];
       // NSLog(@"verify_status===%@",verify_status);
        edu_verify=[verify_status objectForKey:@"EDU_VERIFY"];
        id_verify=[verify_status objectForKey:@"ID_VERIFY"];
        income_verify=[verify_status objectForKey:@"INCOME_VERIFY"];
        work_verify=[verify_status objectForKey:@"WORK_VERIFY"];
        marriage_verify = [verify_status objectForKey:@"MVERIFY"];
        
//        NSLog(@"%@\n%@\n%@\n%@\n%@",edu_verify,id_verify,income_verify,work_verify,marriage_verify);
        
//               [self checkStatus];
        
    }
    [[Global sharedGlobal] setCacheInfo:responseDic forKey:strKey];
    [self showProfileInfo:responseDic];
    
    return YES;
}

- (BOOL)LoadCompatibility
{
    NSString*   macc_no = [self.dicMatchInfo objectForKey:@"acc_no"];
    if( macc_no == nil )
        macc_no = [self.dicMatchInfo objectForKey:@"Acc_no"];
    if( macc_no == nil )
        return NO;
    
    NSString        *strKey = [NSString stringWithFormat:@"%@_comp_preview", macc_no];
    NSDictionary    *responseDic = [[Global sharedGlobal] getCacheInfoForKey:strKey];
    if( [responseDic objectForKey:@"distance"] != nil )
    {
        [self showHeaderInfo:responseDic];
        return YES;
    }

    NSDictionary *result = [UtilComm compatibilityPreview:macc_no];
    
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Error"];
        return NO;
    }
    
    id response = [result objectForKey:@"response"];
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
        return NO;
    }
    
    if( ![response isKindOfClass:[NSDictionary class]] )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"ERROR",)];
        return NO;
    }
    
    [[Global sharedGlobal] setCacheInfo:response forKey:strKey];
    [self showHeaderInfo:response];
    
    return YES;
}

- (void)downloadUserPhotos
{
    NSString*   macc_no = [self.dicMatchInfo objectForKey:@"acc_no"];
    if( macc_no == nil )
        macc_no = [self.dicMatchInfo objectForKey:@"Acc_no"];
    if( macc_no == nil )
        return;
    
    NSString    *matchPhotoPath = [self retrieveUserPhotoPath:macc_no];
    if( matchPhotoPath != nil )
        [self performSelectorOnMainThread:@selector(onDownLoadMatchImage:) withObject:matchPhotoPath waitUntilDone:YES];
    
    NSString    *userPhotoPath = [self retrieveUserPhotoPath:[eSyncronyAppDelegate sharedInstance].strAccNo];
    if( userPhotoPath != nil )
        [self performSelectorOnMainThread:@selector(onDownLoadUserImage:) withObject:userPhotoPath waitUntilDone:YES];
}

- (NSString*)retrieveUserPhotoPath:(NSString*)acc_no
{
    if ([acc_no isEqualToString:[eSyncronyAppDelegate sharedInstance].strAccNo]) {
        acc_no = @"";
        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
        if ([[defaults objectForKey:@"userPhotoName"] isEqualToString:@"nophoto"]) {
            
        }else if([defaults objectForKey:@"userPhotoName"]!=nil){
            NSString*   imageURL = [NSString stringWithFormat:@"%@%@", WEBSERVICE_PHOTO_BASEURI, [defaults objectForKey:@"userPhotoName"]];
            NSString*   imagePath = [ImageUtil loadImagePathFromURL:imageURL];
            return imagePath;
        }
        NSDictionary    *result = [UtilComm retrieveMatchesPhotoFilename:acc_no];
        if( result == nil )
            return nil;
        
        NSString    *response = [result objectForKey:@"response"];
        if( ![response isKindOfClass:[NSString  class]] )
        {
            [defaults setObject:@"nophoto" forKey:@"userPhotoName"];
            return nil;
        }
        
        if( [response hasPrefix:@"ERROR"] )
        {
            [defaults setObject:@"nophoto" forKey:@"userPhotoName"];
            return nil;
        }
        
        if( [response isEqualToString:@"photo view not allowed."] ){
            [defaults setObject:@"nophoto" forKey:@"userPhotoName"];
            return nil;
        }
        [defaults setObject:response forKey:@"userPhotoName"];
        NSString*   imageURL = [NSString stringWithFormat:@"%@%@", WEBSERVICE_PHOTO_BASEURI, response];
        NSString*   imagePath = [ImageUtil loadImagePathFromURL:imageURL];
        return imagePath;
    
    }else{
        NSString    *view_photo = [self.dicMatchInfo objectForKey:@"view_photo"];
       
        if( [view_photo isEqualToString:@"photo view not allowed."] )
            return nil;

        NSString *filename = [self.dicMatchInfo objectForKey:@"filename"];
        NSString*   imageURL = [NSString stringWithFormat:@"%@%@", WEBSERVICE_PHOTO_BASEURI, filename];
        NSString*   imagePath = [ImageUtil loadImagePathFromURL:imageURL];
        return imagePath;
    }
}

- (void)onDownLoadMatchImage:(NSString*)imagePath
{
    UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
    if( image != nil )
    {
        imgViewMainPhoto.image = image;
        imgViewMainPhoto.layer.cornerRadius = 45;
        imgViewMainPhoto.clipsToBounds = YES;

        imgViewMatchPhoto.image = image;
        imgViewMatchPhoto.layer.cornerRadius = 25;
        imgViewMatchPhoto.clipsToBounds = YES;
    }
}

- (void)onDownLoadUserImage:(NSString*)imagePath
{
    UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
    if( image != nil )
    {
        imgViewUserPhoto.image = image;
        imgViewUserPhoto.layer.cornerRadius = 25;
        imgViewUserPhoto.clipsToBounds = YES;
    }
}

//==================================================================================================//
//==================================================================================================//
//==================================================================================================//

- (void)showHeaderInfo:(NSDictionary*)dicCompatibleInfo
{
    userNamelabel.text = [self.dicMatchInfo objectForKey:@"nname"];

    lblName.text = [NSString stringWithFormat:@"%@,  %@", [self.dicMatchInfo objectForKey:@"nname"], [self.dicMatchInfo objectForKey:@"age"]];
    lblName1.text = [self.dicMatchInfo objectForKey:@"nname"];
//    NSLog(@"dicCompatibleInfo distance:%@",[self.dicMatchInfo objectForKey:@"distance"]);
    NSString    *numDistance = [self.dicMatchInfo objectForKey:@"distance"];
    if( numDistance == nil ){
        numDistance = [self.dicMatchInfo objectForKey:@"Distance"];
    }
    if( numDistance == nil ){
        numDistance = [dicCompatibleInfo objectForKey:@"Distance"];
    }
    if( numDistance == nil ){
        numDistance = [dicCompatibleInfo objectForKey:@"distance"];
    }
    int nShowValue, nDistance = [numDistance intValue];

    nShowValue = nDistance;

    if( nDistance >= 100 )
        nDistance = 100;
    else if( nDistance >= 90 )
        nDistance = 90;
    else if( nDistance >= 80 )
        nDistance = 80;
    else if( nDistance >= 70 )
        nDistance = 70;
    else if( nDistance >= 60 )
        nDistance = 60;
    else
        nDistance = 50;

    lblPercent.text = [NSString stringWithFormat:@"%d%%", nShowValue];
    imgIconLove.image = [UIImage imageNamed:[NSString stringWithFormat:@"icon_love_%d.png", nDistance]];
    
    lblUserDisc.text = NSLocalizedString([dicCompatibleInfo objectForKey:@"user_disc"],@"MatchProfileView");
    lblMatchDisc.text = NSLocalizedString([dicCompatibleInfo objectForKey:@"match_disc"],@"MatchProfileView");

    if( [eSyncronyAppDelegate sharedInstance].gender == 0 )
    {
        imgViewMainPhoto.image = [UIImage imageNamed:@"female1.png"];
        imgViewMatchPhoto.image = [UIImage imageNamed:@"female1.png"];
        imgViewUserPhoto.image = [UIImage imageNamed:@"male1.png"];
    }
    else
    {
        imgViewMainPhoto.image = [UIImage imageNamed:@"male1.png"];
        imgViewMatchPhoto.image = [UIImage imageNamed:@"male1.png"];
        imgViewUserPhoto.image = [UIImage imageNamed:@"female1.png"];
    }
    if ([[dicCompatibleInfo objectForKey:@"isfit"]isEqualToString:@"Y"]) {
        self.isFit = YES;
    }else{
        self.isFit = NO;
    }
}

- (void)showBasicInfo:(NSDictionary*)dicBasicInfo
{
    if( dicBasicInfo == nil )
        return;
    
    if( ![dicBasicInfo isKindOfClass:[NSDictionary class]] )
        return;
    
    NSArray*    arrayBasic = [dicBasicInfo objectForKey:@"basic"];
    if( arrayBasic == nil || ![arrayBasic isKindOfClass:[NSArray class]] )
        return;
    
    [self addSubTitleView:NSLocalizedString(@"PERSONAL BACKGROUND",@"MatchProfileView")];
    
    //append lanaguage judge
    
    NSString *_language= [[NSLocale preferredLanguages] objectAtIndex:0];
    
    if ([_language hasPrefix:@"zh-Hant"]||[_language hasPrefix:@"zh-HK"]){
    
        NSString *_strPath =[[NSBundle mainBundle] pathForResource:@"ethnicity_zhHant" ofType:@"plist"];
//        NSString *_strPath =[[NSBundle mainBundle] pathForResource:@"ethnicity_zhHant" ofType:@"xml"];
        NSDictionary *_dicCountry = [NSDictionary dictionaryWithContentsOfFile:_strPath];
        NSDictionary *p_ethnicity=[_dicCountry objectForKey:@"ethnicity"];
        
        [self addOneItemTitle:NSLocalizedString(@"Ethinicity",@"MatchProfileView") content:[p_ethnicity objectForKey:[arrayBasic objectAtIndex:7]]];
        
        p_ethnicity=[_dicCountry objectForKey:@"nationality"];
        
        if ([id_verify isEqualToString:@"1"]) {
            id_status = NSLocalizedString(@"(Verified)",@"ProfileView");
            
            [self addOneItemTitle:NSLocalizedString(@"Nationality",@"MatchProfileView") content:[NSString stringWithFormat:@"%@　%@",[p_ethnicity objectForKey:[arrayBasic objectAtIndex:8]],id_status]];
            
        }else
        {
           [self addOneItemTitle:NSLocalizedString(@"Nationality",@"MatchProfileView") content:[p_ethnicity objectForKey:[arrayBasic objectAtIndex:8]]];
        }
        
//append marriage status
        
        if ([marriage_verify isEqualToString:@"Y"]||[marriage_verify isEqualToString:@"y"]) {
            id_status=NSLocalizedString(@"(Verified)",@"ProfileView");
            
            [self addOneItemTitle:NSLocalizedString(@"Marital Status",@"MatchProfileView") content:[NSString stringWithFormat:@"%@　%@",[[_dicCountry objectForKey:@"marital_status"] objectForKey:[arrayBasic objectAtIndex:6]],id_status]];
            
        }else
        {
            [self addOneItemTitle:NSLocalizedString(@"Marital Status",@"MatchProfileView") content:[[_dicCountry objectForKey:@"marital_status"]objectForKey:[arrayBasic objectAtIndex:6]]];
        }
        
       [self addOneItemTitle:NSLocalizedString(@"I can speak in",@"MatchProfileView") content:[[_dicCountry objectForKey:@"secondary_language"]objectForKey:[arrayBasic objectAtIndex:11]]];
        
        
//       [self addOneItemTitle:NSLocalizedString(@"Other language",@"MatchProfileView") content:[[_dicCountry objectForKey:@"secondary_language"]objectForKey:[arrayBasic objectAtIndex:10]]];
        
    //[self addOneItemTitle:NSLocalizedString(@"Other language",@"MatchProfileView") content:[arrayBasic objectAtIndex:10]];
        
        //====================translate other language=================//
#if 1
            NSArray*    items = [[arrayBasic objectAtIndex:10] componentsSeparatedByString:@","];
            if( items == 0 || [[arrayBasic objectAtIndex:10] isEqualToString:@""] )
                [self addOneItemTitle:NSLocalizedString(@"Other language",@"MatchProfileView") content:[arrayBasic objectAtIndex:10]];
        
        if ([items count] == 1) {
            [self addOneItemTitle:NSLocalizedString(@"Other language",@"MatchProfileView") content:[[_dicCountry objectForKey:@"secondary_language"]objectForKey:[arrayBasic objectAtIndex:10]]];
        }else
        {
            p_ethnicity=[_dicCountry objectForKey:@"secondary_language"];
            
            NSMutableString *p_content = [[NSMutableString alloc]init];
            NSString *p_toContent = [[NSString alloc]init];
            NSString *name = nil;
            for( int i = 0; i < [items count]; i++ )
            {
                NSString *str=[[NSString alloc]init];
                str = [[items objectAtIndex:i] stringByReplacingOccurrencesOfString:@" " withString:@""];
                name =[p_ethnicity objectForKey:str];
                
                if(name.length == 0)
                    break;
                [p_content appendString:[NSString stringWithFormat:@",%@",name]];
//                NSLog(@"%@",p_content);

            }
            unichar c = [p_content characterAtIndex:0];
            NSRange range = NSMakeRange(0, 1);
            if (c == ',') { //此处可以是任何字符
                [p_content deleteCharactersInRange:range];
            }
            p_toContent = [p_content copy];
           
            [self addOneItemTitle:NSLocalizedString(@"Other language",@"MatchProfileView") content:p_toContent];
        }
#endif
        //=================================================//
        
        
        if ([edu_verify isEqualToString:@"1"]) {
            edu_status = NSLocalizedString(@"(Verified)",@"ProfileView");
            
            [self addOneItemTitle:NSLocalizedString(@"Education",@"MatchProfileView") content:[NSString stringWithFormat:@"%@　%@",[[_dicCountry objectForKey:@"education"]objectForKey:[arrayBasic objectAtIndex:9]],edu_status]];
            
        }
        else
        {
            [self addOneItemTitle:NSLocalizedString(@"Education",@"MatchProfileView") content:[[_dicCountry objectForKey:@"education"]objectForKey:[arrayBasic objectAtIndex:9]]];
        }
        
        
        lblNation.text = [[_dicCountry objectForKey:@"nationality"] objectForKey:[arrayBasic objectAtIndex:8]];//repair 0709
        
    }
    else
    {
        [self addOneItemTitle:NSLocalizedString(@"Ethinicity",@"MatchProfileView") content:[arrayBasic objectAtIndex:7]];
        
        if ([id_verify isEqualToString:@"1"]) {
            id_status=NSLocalizedString(@"(Verified)",@"ProfileView");
            [self addOneItemTitle:NSLocalizedString(@"Nationality",@"MatchProfileView") content:[NSString stringWithFormat:@"%@　%@",[arrayBasic objectAtIndex:8],id_status]];
        }else
        {
            [self addOneItemTitle:NSLocalizedString(@"Nationality",@"MatchProfileView") content:[arrayBasic objectAtIndex:8]];
        }

        if ([marriage_verify isEqualToString:@"Y"]||[marriage_verify isEqualToString:@"y"]) {
            id_status=NSLocalizedString(@"(Verified)",@"ProfileView");
            
            [self addOneItemTitle:NSLocalizedString(@"Marital Status",@"MatchProfileView") content:[NSString stringWithFormat:@"%@　%@",[arrayBasic objectAtIndex:6],id_status]];
            
        }else
        {
            [self addOneItemTitle:NSLocalizedString(@"Marital Status",@"MatchProfileView") content:[arrayBasic objectAtIndex:6]];
        }
        
        [self addOneItemTitle:NSLocalizedString(@"I can speak in",@"MatchProfileView") content:[arrayBasic objectAtIndex:11]];
        [self addOneItemTitle:NSLocalizedString(@"Other language",@"MatchProfileView") content:[arrayBasic objectAtIndex:10]];
        
        
        if ([edu_verify isEqualToString:@"1"]) {
            edu_status = NSLocalizedString(@"(Verified)",@"ProfileView");
            [self addOneItemTitle:NSLocalizedString(@"Education",@"MatchProfileView") content:[NSString stringWithFormat:@"%@　%@",[arrayBasic objectAtIndex:9],edu_status]];
            
        }
        else
        {
            [self addOneItemTitle:NSLocalizedString(@"Education",@"MatchProfileView") content:[arrayBasic objectAtIndex:9]];
        }
        
        
          lblNation.text = [arrayBasic objectAtIndex:8];
    }
}

- (void)showLifeStyleInfo:(NSArray*)arrayLifeStyleInfo
{
    if( arrayLifeStyleInfo == nil )
        return;
    
    if( ![arrayLifeStyleInfo isKindOfClass:[NSArray class]] )
        return;
    
    [self addSubTitleView:NSLocalizedString(@"LIFESTYLE",@"MatchProfileView")];
    
    NSString *_language= [[NSLocale preferredLanguages] objectAtIndex:0];
    
    if ([_language hasPrefix:@"zh-Hant"]||[_language hasPrefix:@"zh-HK"]||[_language hasPrefix:@"id"]){
        
        NSString *_strPath =[[NSBundle mainBundle] pathForResource:@"ethnicity_zhHant" ofType:@"plist"];
//        NSString *_strPath =[[NSBundle mainBundle] pathForResource:@"ethnicity_zhHant" ofType:@"xml"];
        NSDictionary *_dicCountry = [NSDictionary dictionaryWithContentsOfFile:_strPath];
       // NSDictionary *p_ethnicity=[_dicCountry objectForKey:@"ethnicity"];
        
        if ([ work_verify isEqualToString:@"1"]) {
            
            work_status=NSLocalizedString(@"(Verified)",@"MatchProfileView");
            
            if ([[_dicCountry objectForKey:@"profession"] objectForKey:[arrayLifeStyleInfo objectAtIndex:1]]==nil) {
                [self addOneItemTitle:NSLocalizedString(@"My job",@"MatchProfileView") content:[NSString stringWithFormat:@"%@　%@",[arrayLifeStyleInfo objectAtIndex:1],work_status]];
            }else
                [self addOneItemTitle:NSLocalizedString(@"My job",@"MatchProfileView") content:[NSString stringWithFormat:@"%@　%@",[[_dicCountry objectForKey:@"profession"] objectForKey:[arrayLifeStyleInfo objectAtIndex:1]],work_status]];
            
        }
        else
        {
            if ([[_dicCountry objectForKey:@"profession"] objectForKey:[arrayLifeStyleInfo objectAtIndex:1]]==nil) {
                [self addOneItemTitle:NSLocalizedString(@"Job",@"MatchProfileView") content:[arrayLifeStyleInfo objectAtIndex:1]];
            }else
                [self addOneItemTitle:NSLocalizedString(@"Job",@"MatchProfileView") content:[[_dicCountry objectForKey:@"profession"] objectForKey:[arrayLifeStyleInfo objectAtIndex:1]]];
        }
        
        [self addOneItemTitle:NSLocalizedString(@"Smoking",@"MatchProfileView") content:[[_dicCountry objectForKey:@"smoking"] objectForKey:[arrayLifeStyleInfo objectAtIndex:3]]];
        [self addOneItemTitle:NSLocalizedString(@"Drinking",@"MatchProfileView") content:[[_dicCountry objectForKey:@"drinking"] objectForKey:[arrayLifeStyleInfo objectAtIndex:5]]];
        [self addOneItemTitle:NSLocalizedString(@"Exercise",@"MatchProfileView") content:[[_dicCountry objectForKey:@"travel_exercise"] objectForKey:[arrayLifeStyleInfo objectAtIndex:4]]];
        [self addOneItemTitle:NSLocalizedString(@"Has Children",@"MatchProfileView") content:[[_dicCountry objectForKey:@"children_num_value"] objectForKey:[arrayLifeStyleInfo objectAtIndex:7]]];
        [self addOneItemTitle:NSLocalizedString(@"Wants Children",@"MatchProfileView") content:[[_dicCountry objectForKey:@"children_num_value"] objectForKey:[arrayLifeStyleInfo objectAtIndex:6]]];
        [self addOneItemTitle:NSLocalizedString(@"Religion",@"MatchProfileView") content:[[_dicCountry objectForKey:@"religion"] objectForKey:[arrayLifeStyleInfo objectAtIndex:9]]];
        
        
        
    }
    else
    {
        if ([ work_verify isEqualToString:@"1"]) {
            work_status = NSLocalizedString(@"(Verified)",@"ProfileView");
            [self addOneItemTitle:NSLocalizedString(@"My job",@"MatchProfileView") content:[NSString stringWithFormat:@"%@　%@",[arrayLifeStyleInfo objectAtIndex:1],work_status]];
            
        }
        else
        {
            [self addOneItemTitle:NSLocalizedString(@"Job",@"MatchProfileView") content:[arrayLifeStyleInfo objectAtIndex:1]];
        }
        
        [self addOneItemTitle:NSLocalizedString(@"Smoking",@"MatchProfileView") content:[arrayLifeStyleInfo objectAtIndex:3]];
        [self addOneItemTitle:NSLocalizedString(@"Drinking",@"MatchProfileView") content:[arrayLifeStyleInfo objectAtIndex:5]];
        [self addOneItemTitle:NSLocalizedString(@"Exercise",@"MatchProfileView") content:[arrayLifeStyleInfo objectAtIndex:4]];
        [self addOneItemTitle:NSLocalizedString(@"Has Children",@"MatchProfileView") content:[arrayLifeStyleInfo objectAtIndex:7]];
        [self addOneItemTitle:NSLocalizedString(@"Wants Children",@"MatchProfileView") content:[arrayLifeStyleInfo objectAtIndex:6]];
        [self addOneItemTitle:NSLocalizedString(@"Religion",@"MatchProfileView") content:[arrayLifeStyleInfo objectAtIndex:9]];
    }
    

}

- (void)showAppearanceInfo:(NSDictionary*)dicAppearanceInfo
{
    if( dicAppearanceInfo == nil )
        return;
    
    if( ![dicAppearanceInfo isKindOfClass:[NSDictionary class]] )
        return;
    
    NSArray*    arrayApperance = [dicAppearanceInfo objectForKey:@"appearance"];
    if( arrayApperance == nil || ![arrayApperance isKindOfClass:[NSArray class]] )
        return;
    
    [self addSubTitleView:NSLocalizedString(@"MY APPEARANCE",@"MatchProfileView")];
    
    [self addOneItemTitle:NSLocalizedString(@"Height",@"MatchProfileView") content:[NSString stringWithFormat:@"%@cm",[arrayApperance objectAtIndex:0]]];
    
    NSString *_language = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    if ([_language hasPrefix:@"zh-Hant"]||[_language hasPrefix:@"zh-HK"]){
    
        NSString *_strPath =[[NSBundle mainBundle] pathForResource:@"ethnicity_zhHant" ofType:@"plist"];
//        NSString *_strPath =[[NSBundle mainBundle] pathForResource:@"ethnicity_zhHant" ofType:@"xml"];
        NSDictionary *_dicCountry = [NSDictionary dictionaryWithContentsOfFile:_strPath];
        NSDictionary *p_ethnicity=[_dicCountry objectForKey:@"bodytype"];
        
        [self addOneItemTitle:NSLocalizedString(@"Body type",@"MatchProfileView") content:[p_ethnicity objectForKey:[arrayApperance objectAtIndex:1]]];
        [self addOneItemTitle:NSLocalizedString(@"Hair Style",@"MatchProfileView") content:[[_dicCountry objectForKey:@"hairstyle"] objectForKey:[arrayApperance objectAtIndex:3]]];
    }
    else//en
    {
        [self addOneItemTitle:NSLocalizedString(@"Body type",@"MatchProfileView") content:[arrayApperance objectAtIndex:1]];
        [self addOneItemTitle:NSLocalizedString(@"Hair Style",@"MatchProfileView") content:[arrayApperance objectAtIndex:3]];
    }

}

- (void)addLastComment
{
    NSString    *strContent = nil;
    CGSize      size = scrlView.frame.size;
    
    if( [eSyncronyAppDelegate sharedInstance].gender == 0 )
        strContent = NSLocalizedString(@"*She may not fit into your strict criteria, but we think you two are very compatible.",@"MatchProfileView");
    else
        strContent = NSLocalizedString(@"*He may not fit into your strict criteria, but we think you two are very compatible.",@"MatchProfileView");

    UILabel *lblContent = [[[UILabel alloc] initWithFrame:CGRectMake(10, _fLastYPos+10, size.width-20, 15)] autorelease];
    if ([[eSyncronyAppDelegate sharedInstance].language hasPrefix:@"zh-Hant"]||[[eSyncronyAppDelegate sharedInstance].language hasPrefix:@"zh-HK"]) {
//        lblContent.font = [UIFont fontWithName:@"CALIBRI" size:15];
    }
    else
        lblContent.font = [UIFont fontWithName:@"HelveticaNeueLTStd-MdCn" size:15];
//    lblContent.font = [UIFont fontWithName:@"HelveticaNeueLTStd-MdCn" size:15];
    lblContent.backgroundColor = [UIColor clearColor];
    lblContent.textColor = TITLE_TEXT_COLLOR;
    lblContent.text = strContent;
    lblContent.numberOfLines = 100;
    [scrlView addSubview:lblContent];

    CGSize maxSize = CGSizeMake( size.width-30, 1000 );
    CGSize contentSize = [strContent sizeWithFont:lblContent.font constrainedToSize:maxSize];
    lblContent.frame = CGRectMake(10, _fLastYPos+10, size.width-20, contentSize.height );

    _fLastYPos += 18 + contentSize.height;
}

- (void)showProfileInfo:(NSDictionary*)dicProfileInfo
{

    [self showBasicInfo:[dicProfileInfo objectForKey:@"basic"]];
    [self showLifeStyleInfo:[dicProfileInfo objectForKey:@"lifestyle"]];
    [self showAppearanceInfo:[dicProfileInfo objectForKey:@"appearance"]];
    [self showInterestInfo:[dicProfileInfo objectForKey:@"interest"]];
    [self showStoryInfo:[dicProfileInfo objectForKey:@"story"]];
    if (self.isFit) {
        [self addLastComment];
    }

    scrlView.contentSize = CGSizeMake(scrlView.frame.size.width, _fLastYPos);
}

//======================================================================================================================//
//======================================================================================================================//
- (void)displayUpgrade
{
    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl procUpgrade];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==2000) {
        if (buttonIndex==1) {
            [self.navigationController popViewControllerAnimated:NO];
            [self performSelector:@selector(displayUpgrade) withObject:nil afterDelay:0.1];
        }
        return;
    }
    
    if (alertView.tag==2001) {
        
        if (buttonIndex==1) {
            MatchSettingViewController *matchSettingViewCtrl = [[[MatchSettingViewController alloc] initWithNibName:@"MatchSettingViewController" bundle:nil] autorelease];
            
            [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.navigationController pushViewController:matchSettingViewCtrl animated:YES];
        }
        return;
    }
    
    if( bGotoUpgrade == FALSE )
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:NO];
        [self performSelector:@selector(displayUpgrade) withObject:nil afterDelay:0.1];
    }
}

- (IBAction)didClickBackBtn:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)didClickQA:(id)sender
{
    
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(requestQAProc) withObject:nil afterDelay:0.1];
    
}

- (void)requestQAProc
{
    NSString    *macc_no = [self.dicMatchInfo objectForKey:@"acc_no"];
    if( macc_no == nil )
        macc_no = [self.dicMatchInfo objectForKey:@"Acc_no"];
    if( macc_no == nil )
        return;
    
    NSDictionary    *result = [UtilComm retrievePendingQuestion:macc_no];
    [DSBezelActivityView removeViewAnimated:NO];
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"eSynchrony"];
        return;
    }
    
    id    response = [result objectForKey:@"response"];
    
    if([response isKindOfClass:[NSString class]]&& [response hasPrefix:@"ERROR"] )
    {
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
        return;
    }
    
    if( [response isKindOfClass:[NSDictionary class]] )
    {
        int num = [[response objectForKey:@"num"]intValue];
//        num = 2;//for test
        if( num >=5 )
        {
            NSString    *strMsg = [NSString stringWithFormat:NSLocalizedString(@"Oops! Your questions have been sent to %@. We will notify you via email when %@ has replied",@"MatchProfileView"), userNamelabel.text, userNamelabel.text];
            
            [ErrorProc alertMessage:strMsg withTitle:@"eSynchrony"];
        }
        else
        {
            QAViewController* viewController = [[[QAViewController alloc] initWithNibName:@"QAViewController" bundle:nil] autorelease];
            viewController.dicMatchInfo = self.dicMatchInfo;
            viewController.mprofileViewController = self;
            viewController.selectedNum = num;
            [self.navigationController pushViewController:viewController animated:YES];
        }

    }

}

- (IBAction)didClickAskPhoto:(id)sender
{
    
//    self.isPhotoAllowed = NO;//for test 0818
    
        if (self.isPhotoAllowed) {
            
            PhotoGalleryViewController *galleryViewController = [[[PhotoGalleryViewController alloc] initWithNibName:@"PhotoGalleryViewController" bundle:nil] autorelease];
            NSString *maccno = [self.dicMatchInfo objectForKey:@"acc_no"];
            if (maccno == nil) {
                maccno = [self.dicMatchInfo objectForKey:@"Acc_no"];
            }
            galleryViewController.strMatchAccNo = maccno;
            galleryViewController.strUserName = [self.dicMatchInfo objectForKey:@"nname"];
            
            [self.navigationController pushViewController:galleryViewController animated:YES];
            
        }else{
            
            
//            if ([[eSyncronyAppDelegate sharedInstance].membership isEqualToString:@"Free"]) {
//                
//                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"eSynchrony" message:NSLocalizedString(@"To view your match's photos, subscribe to our premium packages now!",@"MatchProfileView") delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",) otherButtonTitles:NSLocalizedString(@"OK",), nil];
//                alert.tag = 2000;
//                [alert show];
//                [alert release];
//                
//            }else{
                AskPhotoViewController *askPhotoViewCtrl = [[[AskPhotoViewController alloc] initWithNibName:@"AskPhotoViewController" bundle:nil] autorelease];
                
                askPhotoViewCtrl.dicMatchInfo = self.dicMatchInfo;
                askPhotoViewCtrl.prevViewController = self;
                
                [self.navigationController pushViewController:askPhotoViewCtrl animated:YES];
//            }
        }
}

- (IBAction)didClickPersonality:(id)sender
{
    MatchPersonalityViewController* viewController = [[[MatchPersonalityViewController alloc] initWithNibName:@"MatchPersonalityViewController" bundle:nil] autorelease];
    
    viewController.dicMatchInfo = self.dicMatchInfo;

    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)didClickDecline:(id)sender
{
    
    NSLog(@"%d",[eSyncronyAppDelegate sharedInstance].declineTotal);
    
    if([eSyncronyAppDelegate sharedInstance].declineTotal > 5)
    {
        UIAlertView *alter=[[UIAlertView alloc]initWithTitle:@"eSynchrony" message:NSLocalizedString(@"You may want to review your matching criteria to let us recommend more relevant matches for you. Click here to review now.",@"MatchProfileView") delegate:self cancelButtonTitle:NSLocalizedString(@"Close",@"MatchProfileView") otherButtonTitles:NSLocalizedString(@"Here",@"MatchProfileView"), nil];
        
        alter.tag = 2001;
        
        [alter show];
        [alter release];
        
    }
        DeclineViewController   *declineViewCtrl = [[DeclineViewController alloc] initWithNibName:@"DeclineViewController" bundle:nil];
        
        declineViewCtrl.view.frame = self.view.bounds;
        declineViewCtrl.dicMatchInfo = self.dicMatchInfo;
        declineViewCtrl._delegator = self;
        declineViewCtrl.delegateListViewCtrl = self.delegateListViewCtrl;
        [self.view addSubview:declineViewCtrl.view];
    
}

- (IBAction)didClickSkip:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)didClickApprove:(id)sender
{
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(MatchProfile_requestApproveToSever) withObject:nil afterDelay:0.1];
}

- (void)MatchProfile_requestApproveToSever
{
    [DSBezelActivityView removeViewAnimated:NO];

    NSString    *macc_no = [self.dicMatchInfo objectForKey:@"acc_no"];
    if( macc_no == nil )
        macc_no = [self.dicMatchInfo objectForKey:@"Acc_no"];
    if( macc_no == nil )
        return;
    
    NSDictionary    *result = [UtilComm approveToMatch:macc_no];
    
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"eSynchrony"];
        return;
    }
    
    NSString    *response = [result objectForKey:@"response"];
    if( ![response isKindOfClass:[NSString  class]] )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Error",)];
        return;
    }
    
    if( [response hasPrefix:@"ERROR"] )
    {
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
        return;
    }
    if( [response isEqualToString:@"YOU GOT A MATCH"]||[response isEqualToString:@"You have a mutual match!!"])
    {
        bGotoUpgrade = FALSE;
      /*
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"eSynchrony"
                                                     message:NSLocalizedString(@"You have approved the match. It is now pending for match's approval. Thank you.",@"MatchProfileView")
                                                    delegate:self
                                           cancelButtonTitle:NSLocalizedString(@"OK",)
                                           otherButtonTitles:nil, nil];
       
       */
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"The page at https://www.esynchrony.com says:",@"MatchProfileView")
                                                     message:[NSString stringWithFormat:NSLocalizedString(@"Confirm to arrange date with %@",@"MatchProfileView"),userNamelabel.text]
                                                    delegate:self
                                           cancelButtonTitle:NSLocalizedString(@"OK",@"MatchProfileView")
                                           otherButtonTitles:NSLocalizedString(@"Cancel",@"MatchProfileView"), nil];
        [av show];
        [av release];
    }
    else
    {
        bGotoUpgrade = TRUE;
        
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"eSynchrony"
                                                     message:response
                                                    delegate:self
                                           cancelButtonTitle:NSLocalizedString(@"OK",)
                                           otherButtonTitles:nil, nil];
        [av show];
        [av release];
    }
}

- (IBAction)didClickMoreBtn:(id)sender
{
    DetailMathProfileViewController* viewController = [[[DetailMathProfileViewController alloc] initWithNibName:@"DetailMathProfileViewController" bundle:nil] autorelease];
    
    viewController.dicMatchInfo = self.dicMatchInfo;

    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)dealloc
{
    [self.dicMatchInfo release];

    [btnAskPhoto release];
    [super dealloc];
}

@end


#import "DeclineViewController.h"
#import "DeclineReasonViewController.h"

#import "DSActivityView.h"
#import "Global.h"
#import "UtilComm.h"
#import "eSyncronyAppDelegate.h"
#import "mainMenuViewController.h"
#import "MatchProfileViewController.h"
#import "MyMatchesProposedViewController.h"
#import "PendingViewController.h"
#import "ApprovedViewController.h"
@interface DeclineViewController ()

@end

@implementation DeclineViewController

@synthesize  selectedIdx, reasonArray;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"DeclineView";
    commentTextView.delegate = self;
    
    selectedIdx = 0;
    
    NSString *_strPath =[[NSBundle mainBundle] pathForResource:@"reason_list" ofType:@"plist"];
    
    NSDictionary *_dicCountry = [NSDictionary dictionaryWithContentsOfFile:_strPath];
    
    reasonArray = [[NSMutableArray alloc] initWithArray:[_dicCountry objectForKey:@"Array"]];
    
    contentView.layer.cornerRadius = 5;

    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnTableView:)];
    tapRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapRecognizer];
}

- (void)tapOnTableView:(UITapGestureRecognizer *)gesture
{
    [self.view endEditing:YES];
    [self restoreViewPosition];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    if ([self.delegateListViewCtrl isKindOfClass:[MyMatchesProposedViewController class]]) {
        NSLog(@"self.delegateListViewCtrl:%@",self.delegateListViewCtrl);
//    }
    [self initSetting];
}

- (void)initSetting
{
    reasoneLabel.text = [reasonArray objectAtIndex:selectedIdx];
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    self.view.center = CGPointMake(self.view.center.x, 60);
    
    CGRect rect = [textView convertRect:textView.bounds toView:self.view];
        
    float   offset;
        
    offset = self.view.frame.size.height - (rect.size.height + 240);
    offset = offset - rect.origin.y;
        
    if( offset > 0 )
        return YES;

    rect = self.view.frame;
    rect.origin.y = offset;
        
    self.view.frame = rect;
    
    return YES;
}

- (IBAction)didClickCanceBtn:(id)sender;
{
    //test start需要注释掉的
//    [eSyncronyAppDelegate sharedInstance].declineTotal++;
//    
//    NSLog(@"%d",[eSyncronyAppDelegate sharedInstance].declineTotal);
    //test end
    
    [commentTextView resignFirstResponder];
    [self.view removeFromSuperview];

    [self release];
}

- (IBAction)didclickReasonSelBtn:(id)sender
{
    [commentTextView resignFirstResponder];
    
    if( !declineReasonViewCtrl )
        declineReasonViewCtrl = [[DeclineReasonViewController alloc] initWithNibName:@"DeclineReasonViewController" bundle:nil];
    
    declineReasonViewCtrl.decineViewCtrl = self;
    declineReasonViewCtrl.itemArray = reasonArray;
    declineReasonViewCtrl.selectIdx = selectedIdx;

    [self.view addSubview:declineReasonViewCtrl.view];
}

- (IBAction)didClickApprove:(id)sender
{
    
}

- (IBAction)didClickOKBtn:(id)sender
{
    [commentTextView resignFirstResponder];
 
    [eSyncronyAppDelegate sharedInstance].declineTotal++;

//    NSLog(@"%d",[eSyncronyAppDelegate sharedInstance].declineTotal);
    
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(requestDeclineToSever) withObject:nil afterDelay:0.1];
}

- (void)requestDeclineToSever
{
    [DSBezelActivityView removeViewAnimated:NO];
    
    NSString    *macc_no = [self.dicMatchInfo objectForKey:@"acc_no"];
    if( macc_no == nil )
        macc_no = [self.dicMatchInfo objectForKey:@"Acc_no"];
    if( macc_no == nil )
        return;
    int reasonid = (int)(selectedIdx+1);
    if (reasonid == 7) {
        reasonid = 8;
    }else if(reasonid == 8){
        reasonid = 7;
    }
    NSDictionary    *result = [UtilComm declineToMatch:macc_no reasonId:reasonid comment:commentTextView.text];
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"eSynchrony"];
        return;
    }

    NSString    *response = [result objectForKey:@"response"];
    if( ![response isKindOfClass:[NSString  class]] )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Error",)];
        return;
    }
    
    if( [response hasPrefix:@"ERROR"] )
    {
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
        return;
    }
    
    if ([response hasPrefix:@"DECLINE Successful"]) {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"eSynchrony"
                                                     message:NSLocalizedString(@"You have declined the match successfully.",@"DeclineView")
                                                    delegate:self
                                           cancelButtonTitle:NSLocalizedString(@"OK",)
                                           otherButtonTitles:nil];
        av.tag = 2001;
        [av show];
        [av release];
        return;
    }
    if ([response hasPrefix:@"Oops! You have run out of dates"]) {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"eSynchrony"
                                                     message:response
                                                    delegate:self
                                           cancelButtonTitle:NSLocalizedString(@"OK",)
                                           otherButtonTitles:NSLocalizedString(@"Cancel",),nil];
        av.tag = 2002;
        [av show];
        [av release];
        return;
    }
    if ([response hasPrefix:@"User has to subscribe first"]) {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"eSynchrony"
                                                     message:response
                                                    delegate:self
                                           cancelButtonTitle:NSLocalizedString(@"OK",)
                                           otherButtonTitles:nil];
        av.tag = 2003;
        [av show];
        [av release];
        return;
    }
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.view removeFromSuperview];
    [self release];
    switch (alertView.tag) {
        case 2001:{
            MatchProfileViewController* viewController = (MatchProfileViewController*)self._delegator;
            [viewController.navigationController popViewControllerAnimated:YES];
            if ([self.delegateListViewCtrl isKindOfClass:[MyMatchesProposedViewController class]]) {
                MyMatchesProposedViewController* listViewController = (MyMatchesProposedViewController*)self.delegateListViewCtrl;
                [listViewController refreshContent];
                break;
            }
            if ([self.delegateListViewCtrl isKindOfClass:[PendingViewController class]]) {
                PendingViewController* listViewController = (PendingViewController*)self.delegateListViewCtrl;
                [listViewController refreshContent];
                break;
            }
            if ([self.delegateListViewCtrl isKindOfClass:[ApprovedViewController class]]) {
                ApprovedViewController* listViewController = (ApprovedViewController*)self.delegateListViewCtrl;
                [listViewController refreshContent];
                break;
            }
            break;
            
        }
        case 2002:{
            [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl procUpgrade];
            break;
        }
        default:
            break;
    }
    
    
    
}

- (void)dealloc
{
    [reasoneLabel release];
    [commentTextView release];
    [reasonArray release];
    
    [declineReasonViewCtrl release];
    
    [super dealloc];
}

@end

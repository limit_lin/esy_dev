
#import "DeclineReasonViewController.h"
#import "DeclineViewController.h"
#import "RadioBoxCell.h"

@interface DeclineReasonViewController ()

@end

@implementation DeclineReasonViewController

@synthesize decineViewCtrl, itemArray, selectIdx;

- (void)viewDidLoad
{
    [super viewDidLoad];
//    self.screenName = @"DeclineReasonView";
    tbleView.layer.cornerRadius = 5;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    selectIdx = decineViewCtrl.selectedIdx;
    [tbleView reloadData];
}

#pragma mark --- Table Functions ---
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [itemArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RadioBoxCell *cell;
    NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"RadioBoxCell" owner:nil options:nil];
    cell = [arr objectAtIndex:0];
    
    cell.reasonLabel.text = [itemArray objectAtIndex:indexPath.row];
    
    UIImage *img;
    if (selectIdx == indexPath.row) {
        img = [UIImage imageNamed:@"btnRadio_p"];
        [cell.radioImgView initWithImage:img];
    }
    else {
        img = [UIImage imageNamed:@"btnRadio_n"];
        [cell.radioImgView initWithImage:img];
    }
    
    return cell;
}

- (void)procTblSelectCell
{
    [self.view removeFromSuperview];
    
    [decineViewCtrl initSetting];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectIdx = indexPath.row;
    
    decineViewCtrl.selectedIdx = indexPath.row;
    
    [self performSelector:@selector(procTblSelectCell) withObject:nil afterDelay:0.2];
}

- (void)dealloc
{
    [tbleView release];
    [decineViewCtrl release];
    [itemArray release];
    
    [super dealloc];
}

@end

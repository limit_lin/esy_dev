
#import <UIKit/UIKit.h>
#import "InputBaseViewController.h"

@class DeclineReasonViewController;
@interface DeclineViewController : InputBaseViewController <UITextViewDelegate>
{
    IBOutlet UIView         *contentView;
    IBOutlet UILabel        *reasoneLabel;
    IBOutlet UITextView     *commentTextView;
    
    DeclineReasonViewController *declineReasonViewCtrl;
}

@property (nonatomic, assign) id _delegator;
@property (nonatomic, assign) id delegateListViewCtrl;
@property (nonatomic) NSInteger selectedIdx;
@property (nonatomic, strong) NSMutableArray *reasonArray;
@property (nonatomic, assign) NSMutableDictionary* dicMatchInfo;

- (IBAction)didClickCanceBtn:(id)sender;
- (IBAction)didClickOKBtn:(id)sender;
- (IBAction)didclickReasonSelBtn:(id)sender;

- (void)initSetting;

@end

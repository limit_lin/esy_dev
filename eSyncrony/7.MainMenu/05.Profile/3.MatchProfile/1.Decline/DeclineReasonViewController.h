

#import <UIKit/UIKit.h>

@class DeclineViewController;

@interface DeclineReasonViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UITableView    *tbleView;    
}

@property (nonatomic, strong) DeclineViewController *decineViewCtrl;
@property (nonatomic, strong) NSMutableArray *itemArray;
@property (nonatomic) NSInteger selectIdx;

@end

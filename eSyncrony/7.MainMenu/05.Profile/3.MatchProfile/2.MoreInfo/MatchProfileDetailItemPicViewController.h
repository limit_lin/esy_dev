
#import <UIKit/UIKit.h>

@interface MatchProfileDetailItemPicViewController : UIViewController
{
    IBOutlet    UILabel     *lblLeftTitle;
    IBOutlet    UILabel     *lblRightTitle;
    IBOutlet    UILabel     *lblMe;
    IBOutlet    UILabel     *lblMatch;
    
    IBOutlet    UIView      *viewMePos;
    IBOutlet    UIView      *viewMatchPos;
    
    IBOutlet    UIView      *viewLeftCircle;
    IBOutlet    UIView      *viewRightCircle;
    
    NSString    *_strMatchName;
    NSString    *_strLeftTitle;
    NSString    *_strRightTitle;
    int         _matchScore;
    int         _myScore;
}

+ (id)createWithMatchName:(NSString*)strMatchName
                leftTitle:(NSString*)strLeftTitle
               rightTitle:(NSString*)strRightTitle
               matchScore:(int)matchScore
                  myScore:(int)myScore;
- (void)setMatchName:(NSString*)strMatchName
           leftTitle:(NSString*)strLeftTitle
          rightTitle:(NSString*)strRightTitle
          matchScore:(int)matchScore
             myScore:(int)myScore;
@end


#import "DetailMathProfileViewController.h"

#import "MatchProfileViewController.h"
#import "mainMenuViewController.h"

#import "DSActivityView.h"
#import "Global.h"
#import "UtilComm.h"
#import "eSyncronyAppDelegate.h"
#import "MatchProfileDetailItemPicViewController.h"

@interface DetailMathProfileViewController ()

@end

@implementation DetailMathProfileViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"DetailMathProfileView";
    lblName.text = [self.dicMatchInfo objectForKey:@"nname"];
    
    NSNumber    *numDistance = [self.dicMatchInfo objectForKey:@"distance"];
    if( numDistance == nil )
        numDistance = [self.dicMatchInfo objectForKey:@"Distance"];

    int score = [numDistance intValue];
    NSString *strMatch1;
    NSString *strMatch2;
    if (score>=0&&score<=50) {
        strMatch1 = NSLocalizedString(@"This can be a case of opposite attracting.",@"DetailMathProfileView");
        strMatch2 = NSLocalizedString(@"Though you both have different views and values in many areas, thing brings about many perspectives and different views on many matters.",@"DetailMathProfileView");
    }else if(score>=50.1&&score<=60){
        strMatch1 = NSLocalizedString(@"There are many areas where both of you will disagree but fortunately there are certain core values that are in alignment.",@"DetailMathProfileView");
        strMatch2 = NSLocalizedString(@"Overall, it is important for both sides to appreciate and understand the differences which bring about different perspective and perhaps even keep things interesting.",@"DetailMathProfileView");
    }else if (score>=60.1&&score<=70){
        strMatch1 = NSLocalizedString(@"There is a fine balance of areas where both of you are similar and different.",@"DetailMathProfileView");
        strMatch2 = NSLocalizedString(@"This could brings about a healthy relationship where there are core areas of agreement but some areas of differences which could complement both sides in the relationship.",@"DetailMathProfileView");
    }else if (score>=70.1&&score<=80){
        strMatch1 = NSLocalizedString(@"Both of you have many areas of alignment in beliefs and values which makes both parties more amenable to each other thoughts and ideas.",@"DetailMathProfileView");
        strMatch2 = NSLocalizedString(@"There are some differences which would still require understanding and give and take.",@"DetailMathProfileView");
    }else{
        strMatch1 = NSLocalizedString(@"There is a close alignment in both of your values and beliefs system,so much so that getting along is almost a breeze.",@"DetailMathProfileView");
        strMatch2 = NSLocalizedString(@"Both sides must have a balanced view on each others' values to ensure different perspectives and opinions in the relationship.",@"DetailMathProfileView");
        
    }
    
    self.lblMatch1.text = strMatch1;
    self.lblMatch2.text = strMatch2;
    CGRect rect1 = self.lblMatch1.frame;
    CGRect rect2 = self.lblMatch2.frame;
    CGSize size = CGSizeMake(280.f, MAXFLOAT);
    CGSize actualsize1 = [strMatch1 sizeWithFont:self.lblMatch1.font constrainedToSize:size lineBreakMode:NSLineBreakByTruncatingTail];
    CGSize actualsize2 = [strMatch2 sizeWithFont:self.lblMatch2.font constrainedToSize:size lineBreakMode:NSLineBreakByTruncatingTail];
    rect1.size = actualsize1;
    rect2.size = actualsize2;
    self.lblMatch1.frame = rect1;
    self.lblMatch2.frame = CGRectMake(self.lblMatch2.frame.origin.x, self.lblMatch1.frame.origin.y+self.lblMatch1.frame.size.height+8,rect2.size.width, rect2.size.height);
   
    CGRect rect3 = self.imageHeart1.frame;
    CGRect rect4 = self.imageHeart2.frame;
    rect3.origin.y = self.lblMatch1.frame.origin.y-3;
    rect4.origin.y = self.lblMatch2.frame.origin.y-3;
    self.imageHeart1.frame = rect3;
    self.imageHeart2.frame = rect4;
    
    CGRect rect5 = self.viewLine.frame;
    rect5.origin.y = self.lblMatch2.frame.origin.y+self.lblMatch2.frame.size.height+8;
    self.viewLine.frame = rect5;
    
    CGRect rect6 = contentsView2.frame;
    rect6.origin.y = self.viewLine.frame.origin.y+20;
    contentsView2.frame = rect6;
    
    CGRect rect7 = contentsView.frame;
    rect7.size.height = contentsView2.frame.origin.y + contentsView2.frame.size.height;
    contentsView.frame = rect7;
    
    [scrlView addSubview:contentsView];
    scrlView.contentSize = contentsView.frame.size;

    [self showBusyDialogWithTitle:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(LoadAllInfo) withObject:nil afterDelay:0.01f];
}

- (void)LoadAllInfo
{
    [self hideBusyDialog];
    
    NSString    *macc_no = [self.dicMatchInfo objectForKey:@"acc_no"];
    if( macc_no == nil )
        macc_no = [self.dicMatchInfo objectForKey:@"Acc_no"];
    if( macc_no == nil )
        return;
    
    NSString        *strKey = [NSString stringWithFormat:@"%@_comp_detail", macc_no];
    NSDictionary    *responseDic = [[Global sharedGlobal] getCacheInfoForKey:strKey];
    if( responseDic != nil )
    {
        [self showDetailInfo:responseDic];
        return;
    }

    NSDictionary *result = [UtilComm compatibilityViewDetail:macc_no];
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Error"];
        return;
    }

    id response = [result objectForKey:@"response"];
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
        return;
    }
    
    responseDic = (NSDictionary*)response;
    if( ![responseDic isKindOfClass:[NSDictionary class]] )
    {
        [ErrorProc alertMessage:@"Unknown Error" withTitle:@"ERROR"];
        return;
    }
    
    [[Global sharedGlobal] setCacheInfo:responseDic forKey:strKey];
    [self showDetailInfo:responseDic];
}

- (void)showMoneyInfo:(NSDictionary*)dicInfo
{
    if( dicInfo == nil )
        return;
    
    int matchScore = [[dicInfo objectForKey:@"matchscore"] intValue];
    int myScore = [[dicInfo objectForKey:@"myscore"] intValue];

    
    detailController1 = [MatchProfileDetailItemPicViewController createWithMatchName:[self.dicMatchInfo objectForKey:@"nname"]
                                                                           leftTitle:NSLocalizedString(@"Conservative",@"DetailMathProfileView")
                                                                          rightTitle:NSLocalizedString(@"Risk Taker",@"DetailMathProfileView")
                                                                          matchScore:matchScore
                                                                             myScore:myScore];
    
    [viewPicture1 addSubview:detailController1.view];
}

- (void)showCultureInfo:(NSDictionary*)dicInfo
{
    if( dicInfo == nil )
        return;
    
    int matchScore = [[dicInfo objectForKey:@"matchscore"] intValue];
    int myScore = [[dicInfo objectForKey:@"myscore"] intValue];
    
    detailController2 = [MatchProfileDetailItemPicViewController createWithMatchName:lblName.text
                                                                           leftTitle:NSLocalizedString(@"Localised",@"DetailMathProfileView")
                                                                          rightTitle:NSLocalizedString(@"Wordly",@"DetailMathProfileView")
                                                                          matchScore:matchScore
                                                                             myScore:myScore];
    
    [viewPicture2 addSubview:detailController2.view];
}

- (void)showReadinessInfo:(NSDictionary*)dicInfo
{
    if( dicInfo == nil )
        return;
    
    int matchScore = [[dicInfo objectForKey:@"matchscore"] intValue];
    int myScore = [[dicInfo objectForKey:@"myscore"] intValue];
    
    detailController3 = [MatchProfileDetailItemPicViewController createWithMatchName:lblName.text
                                                                           leftTitle:NSLocalizedString(@"Exploring",@"DetailMathProfileView")
                                                                          rightTitle:NSLocalizedString(@"Settling",@"DetailMathProfileView")
                                                                          matchScore:matchScore
                                                                             myScore:myScore];
    
    [viewPicture3 addSubview:detailController3.view];
}

- (void)showOpenessInfo:(NSDictionary*)dicInfo
{
    if( dicInfo == nil )
        return;
    
    int matchScore = [[dicInfo objectForKey:@"matchscore"] intValue];
    int myScore = [[dicInfo objectForKey:@"myscore"] intValue];
    
    detailController4 = [MatchProfileDetailItemPicViewController createWithMatchName:lblName.text
                                                                           leftTitle:NSLocalizedString(@"Sensing",@"DetailMathProfileView")
                                                                          rightTitle:NSLocalizedString(@"Intuitive",@"DetailMathProfileView")
                                                                          matchScore:matchScore
                                                                             myScore:myScore];

    [viewPicture4 addSubview:detailController4.view];
}

- (void)showDetailInfo:(NSDictionary*)dicDetailInfo
{
    [self showMoneyInfo:[dicDetailInfo objectForKey:@"money"]];
    [self showCultureInfo:[dicDetailInfo objectForKey:@"culture"]];
    [self showReadinessInfo:[dicDetailInfo objectForKey:@"readiness"]];
    [self showOpenessInfo:[dicDetailInfo objectForKey:@"openness"]];
}

- (IBAction)onClickBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dealloc
{
    [lblName release];
    [scrlView release];
    [contentsView release];
    [contentsView2 release];
    [viewPicture1 release];
    [viewPicture2 release];
    [viewPicture3 release];
    [viewPicture4 release];

    [detailController1 release];
    [detailController2 release];
    [detailController3 release];
    [detailController4 release];
    
    [_lblMatch1 release];
    [_lblMatch2 release];
    [_imageHeart1 release];
    [_imageHeart2 release];
    [_viewLine release];
    [super dealloc];
}

@end

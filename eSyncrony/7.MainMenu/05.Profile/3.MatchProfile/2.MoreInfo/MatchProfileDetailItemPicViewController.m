
#import "MatchProfileDetailItemPicViewController.h"

@interface MatchProfileDetailItemPicViewController ()

@end

@implementation MatchProfileDetailItemPicViewController

+ (id)createWithMatchName:(NSString*)strMatchName
                leftTitle:(NSString*)strLeftTitle
               rightTitle:(NSString*)strRightTitle
               matchScore:(int)matchScore
                  myScore:(int)myScore
{
    MatchProfileDetailItemPicViewController* viewController = [[MatchProfileDetailItemPicViewController alloc] initWithNibName:@"MatchProfileDetailItemPicViewController" bundle:nil];
    
    [viewController setMatchName:strMatchName
                       leftTitle:strLeftTitle
                      rightTitle:strRightTitle
                      matchScore:matchScore
                         myScore:myScore];

    return viewController;
}

- (void)setMatchName:(NSString*)strMatchName
           leftTitle:(NSString*)strLeftTitle
          rightTitle:(NSString*)strRightTitle
          matchScore:(int)matchScore
             myScore:(int)myScore
{
    _strMatchName = [strMatchName retain];
    _strLeftTitle = [strLeftTitle retain];
    _strRightTitle = [strRightTitle retain];
    
    _matchScore = MIN( 100, MAX(0, matchScore) );
    _myScore = MIN( 100, MAX(0, myScore) );
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    viewLeftCircle.layer.cornerRadius = 2.5f;
    viewRightCircle.layer.cornerRadius = 2.5f;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    lblMatch.text = _strMatchName;
    lblLeftTitle.text = _strLeftTitle;
    lblRightTitle.text = _strRightTitle;
    
    CGPoint centerPos = lblMe.center;
    centerPos.x = 80 + 140*_myScore/100;
    lblMe.center = centerPos;
    
    centerPos = viewMePos.center;
    centerPos.x = 80 + 140*_myScore/100;
    viewMePos.center = centerPos;
    
    centerPos = lblMatch.center;
    centerPos.x = 80 + 140*_matchScore/100;
    lblMatch.center = centerPos;
    
    centerPos = viewMatchPos.center;
    centerPos.x = 80 + 140*_matchScore/100;
    viewMatchPos.center = centerPos;
}

- (void)dealloc
{
    [lblLeftTitle release];
    [lblRightTitle release];
    [lblMe release];
    [lblMatch release];
    [viewMePos release];
    [viewMatchPos release];
    [viewLeftCircle release];
    [viewRightCircle release];
    
    [_strMatchName release];
    [_strLeftTitle release];
    [_strRightTitle release];
    
    [super dealloc];
}

@end

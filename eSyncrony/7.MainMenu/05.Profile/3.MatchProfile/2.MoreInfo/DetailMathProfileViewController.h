

#import <UIKit/UIKit.h>
#import "HttpBaseViewController.h"
#import "MatchProfileDetailItemPicViewController.h"

@interface DetailMathProfileViewController : HttpBaseViewController
{
    IBOutlet UILabel        *lblName;

    IBOutlet UIScrollView   *scrlView;
    IBOutlet UIView         *contentsView;
    IBOutlet UIView         *contentsView2;
    
    IBOutlet UIView         *viewPicture1;
    IBOutlet UIView         *viewPicture2;
    IBOutlet UIView         *viewPicture3;
    IBOutlet UIView         *viewPicture4;
    
    MatchProfileDetailItemPicViewController*    detailController1;
    MatchProfileDetailItemPicViewController*    detailController2;
    MatchProfileDetailItemPicViewController*    detailController3;
    MatchProfileDetailItemPicViewController*    detailController4;
}
@property (strong, nonatomic) IBOutlet UILabel *lblMatch1;
@property (strong, nonatomic) IBOutlet UILabel *lblMatch2;
@property (strong, nonatomic) IBOutlet UIImageView *imageHeart1;
@property (strong, nonatomic) IBOutlet UIImageView *imageHeart2;
@property (strong, nonatomic) IBOutlet UIView *viewLine;

@property (nonatomic, assign) NSMutableDictionary* dicMatchInfo;

- (IBAction)onClickBack:(id)sender;

@end

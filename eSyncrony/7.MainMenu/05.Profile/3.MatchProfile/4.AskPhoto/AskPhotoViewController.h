
#import <UIKit/UIKit.h>
#import "MatchProfileViewController.h"

@interface AskPhotoViewController : GAITrackedViewController
{
    IBOutlet UIImageView    *imgViewPhoto;
    
    IBOutlet UIView         *viewFrameParent;
    
    IBOutlet UIView *viewFrameOK;
    IBOutlet UIView *viewFrameApproveDecline;
    IBOutlet UIView *viewFrameApprove;
    IBOutlet UIView *viewFrameRemind;
    IBOutlet UIView *viewNoRequest;
    IBOutlet UIView *viewFrameUpLoadPhoto;
    
    IBOutlet UIView         *viewFrameMSG;
    IBOutlet UILabel        *lblMessage;
    
    BOOL    bUpload;
}

@property (nonatomic, strong) NSMutableDictionary           *dicMatchInfo;
@property (nonatomic, assign) MatchProfileViewController    *prevViewController;



- (IBAction)didClickBackBtn:(id)sender;

- (IBAction)onClickApprove:(id)sender;

- (IBAction)onClickDecline:(id)sender;

- (IBAction)onClickGoToGallery:(id)sender;

- (IBAction)onClickSendRemind:(id)sender;

- (IBAction)didClickViewExchangeStatus:(id)sender;

- (IBAction)didClickUploadPhoto:(id)sender;
@end

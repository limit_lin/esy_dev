
#import "AskPhotoViewController.h"
#import "PhotoGalleryViewController.h"
#import "AddPhotoViewController.h"

#import "DSActivityView.h"
#import "Global.h"
#import "UtilComm.h"
#import "ImageUtil.h"

#import "eSyncronyAppDelegate.h"
#import "mainMenuViewController.h"

@implementation AskPhotoViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"AskPhotoView";
    [self refreshContent];
}

- (void)refreshContent
{
    for( UIView* view in viewFrameParent.subviews )
        [view removeFromSuperview];
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(requestAskPhotoProc) withObject:nil afterDelay:0.1];
}

- (void)requestAskPhotoProc
{
    [DSBezelActivityView removeViewAnimated:NO];
    
    NSString    *macc_no = [self.dicMatchInfo objectForKey:@"acc_no"];
    if( macc_no == nil )
        macc_no = [self.dicMatchInfo objectForKey:@"Acc_no"];
    if( macc_no == nil )
        return;
    
    NSDictionary    *result = [UtilComm viewPhotoSingle:macc_no];
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"eSynchrony"];
        return;
    }
    
    NSString    *response = [result objectForKey:@"response"];
    if( ![response isKindOfClass:[NSString  class]] )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Error",)];
        return;
    }
    
    if( [response hasPrefix:@"ERROR"] )
    {
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
        return;
    }
    
    bUpload = NO;
//    NSArray *responseArr = [responseAll componentsSeparatedByString:@"|"];
//    NSString *response = [responseArr objectAtIndex:1];
    
    if( [response isEqualToString:@"Click here to exchange each other's profile photo."] )
    {
        [viewFrameParent addSubview:viewNoRequest];
    }
    else if( [response isEqualToString:@"To reveal match's picture, upload your photo now."] )
    {
        [viewFrameParent addSubview:viewFrameUpLoadPhoto];
    }
    else if( [response isEqualToString:@"Match has exchanged photo with you."] )
    {
        [viewFrameParent addSubview:viewFrameOK];
    }
    else if( [response hasPrefix:@"Match photo is already public"] )
    {
        
        [viewFrameParent addSubview:viewFrameOK];
    }
    else if( [response hasPrefix:@"Approve Decline - Upload"] )
    {
        bUpload = YES;
        [viewFrameParent addSubview:viewFrameApproveDecline];
    }
    else if( [response hasPrefix:@"Approve Decline"] )
    {
        [viewFrameParent addSubview:viewFrameApproveDecline];
    }
    else if( [response hasPrefix:@"Send Reminder"] )
    {
        [viewFrameParent addSubview:viewFrameRemind];
    }
    else if( [response hasPrefix:@"You have rejected this match."] )
    {
        [viewFrameParent addSubview:viewFrameApprove];
    }
    else if( [response hasPrefix:@"Change to Approve - Upload"] )
    {
        bUpload = YES;
        [viewFrameParent addSubview:viewFrameApprove];
    }
    else
    {
        //Waiting for match reply
        //Match decides not to exchange for now
        [viewFrameParent addSubview:viewFrameMSG];
        lblMessage.text = response;
    }
//    else if ([response hasPrefix:@"View"])//for test
//    {
//        [viewFrameParent addSubview:viewFrameUpLoadPhoto];
//    }

}

- (IBAction)didClickBackBtn:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

//=============================================================================//

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if( bUpload == YES )
    {
        if( buttonIndex == 0 )
        {
            PhotoGalleryViewController *galleryViewController = [[[PhotoGalleryViewController alloc] initWithNibName:@"PhotoGalleryViewController" bundle:nil] autorelease];

            galleryViewController.strMatchAccNo = @"";
            galleryViewController.strUserName = [eSyncronyAppDelegate sharedInstance].strName;

            [self.navigationController pushViewController:galleryViewController animated:YES];
        }
    }
    else
    {
        [self refreshContent];
    }
}

- (void)refreshContentAfterAlertMsg:(NSString*)strMsg
{
    if( ![strMsg isKindOfClass:[NSString  class]] )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Error",)];
        return;
    }
    
    if( [strMsg hasPrefix:@"ERROR"] )
    {
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:strMsg];
        return;
    }
    
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"eSynchrony"
                                                 message:strMsg
                                                delegate:self
                                       cancelButtonTitle:NSLocalizedString(@"OK",)
                                       otherButtonTitles:nil];
    [av show];
    [av release];
}

- (void)onNoUploadedPhotoProc
{
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"eSynchrony"
                                                 message:NSLocalizedString(@"Your photo has not been uploaded or is still pending for approval.",@"AskPhotoView")
                                                delegate:self
                                       cancelButtonTitle:NSLocalizedString(@"OK",)
                                       otherButtonTitles:NSLocalizedString(@"Cancel",), nil];
    [av show];
    [av release];
}

- (IBAction)onClickApprove:(id)sender
{
    if( bUpload == YES )
    {
        [self onNoUploadedPhotoProc];
        return;
    }

    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(requestApprovePhoto) withObject:nil afterDelay:0.1];
}

- (void)requestApprovePhoto
{
    
    
    NSString    *macc_no = [self.dicMatchInfo objectForKey:@"acc_no"];
    if( macc_no == nil )
        macc_no = [self.dicMatchInfo objectForKey:@"Acc_no"];
    if( macc_no == nil )
        return;
    
    NSDictionary    *result = [UtilComm exchangePhotoProcess:macc_no];
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"eSynchrony"];
        return;
    }
    
    NSString    *response = [result objectForKey:@"response"];
    [DSBezelActivityView removeViewAnimated:NO];
    [self refreshContentAfterAlertMsg:response];
}

- (IBAction)onClickDecline:(id)sender
{
    if( bUpload == YES )
    {
        [self onNoUploadedPhotoProc];
        return;
    }

    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(requestDeclinePhoto) withObject:nil afterDelay:0.1];
}

- (void)requestDeclinePhoto
{
    [DSBezelActivityView removeViewAnimated:NO];
    
    NSString    *macc_no = [self.dicMatchInfo objectForKey:@"acc_no"];
    if( macc_no == nil )
        macc_no = [self.dicMatchInfo objectForKey:@"Acc_no"];
    if( macc_no == nil )
        return;
    
    NSDictionary    *result = [UtilComm rejectPhotoExchange:macc_no];
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"eSynchrony"];
        return;
    }
    
    NSString    *response = [result objectForKey:@"response"];
    [self refreshContentAfterAlertMsg:response];
}

- (IBAction)onClickGoToGallery:(id)sender
{
    NSString    *macc_no = [self.dicMatchInfo objectForKey:@"acc_no"];
    if( macc_no == nil )
        macc_no = [self.dicMatchInfo objectForKey:@"Acc_no"];
    if( macc_no == nil )
        return;

    PhotoGalleryViewController *galleryViewController = [[[PhotoGalleryViewController alloc] initWithNibName:@"PhotoGalleryViewController" bundle:nil] autorelease];
    
    galleryViewController.strMatchAccNo = macc_no;
    galleryViewController.strUserName = [self.dicMatchInfo objectForKey:@"nname"];

    [self.navigationController pushViewController:galleryViewController animated:YES];
}

- (IBAction)onClickSendRemind:(id)sender
{
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(requestSendRemindPhoto) withObject:nil afterDelay:0.1];
}

- (void)requestSendRemindPhoto
{
    [DSBezelActivityView removeViewAnimated:NO];
    
    NSString    *macc_no = [self.dicMatchInfo objectForKey:@"acc_no"];
    if( macc_no == nil )
        macc_no = [self.dicMatchInfo objectForKey:@"Acc_no"];
    if( macc_no == nil )
        return;
    
    NSDictionary    *result = [UtilComm sendPhotoReminder:macc_no];
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"eSynchrony"];
        return;
    }
    
    NSString    *response = [result objectForKey:@"response"];
    [self refreshContentAfterAlertMsg:response];
}

- (void)displayPhotoExchange
{
    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl procPhotoExchange];
}

- (IBAction)didClickViewExchangeStatus:(id)sender
{
    [self.navigationController popToViewController:[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl animated:NO];

    [self performSelector:@selector(displayPhotoExchange) withObject:nil afterDelay:0.1];
}

- (IBAction)didClickUploadPhoto:(id)sender {
    
//    PhotoGalleryViewController *galleryViewController = [[[PhotoGalleryViewController alloc] initWithNibName:@"PhotoGalleryViewController" bundle:nil] autorelease];
//    
//    galleryViewController.strMatchAccNo = @"";
//    galleryViewController.strUserName = [eSyncronyAppDelegate sharedInstance].strName;
//    
//    [self.navigationController pushViewController:galleryViewController animated:YES];
    
//  repair 0728
    AddPhotoViewController *addPhotoViewCtrl = [[[AddPhotoViewController alloc] initWithNibName:@"AddPhotoViewController" bundle:nil] autorelease];
    
    [self.navigationController pushViewController:addPhotoViewCtrl animated:YES];
    
    
    
}

- (void)dealloc
{
    [imgViewPhoto release];
    
    [self.dicMatchInfo release];

    [viewNoRequest release];
    [viewFrameUpLoadPhoto release];
    [super dealloc];
}

@end

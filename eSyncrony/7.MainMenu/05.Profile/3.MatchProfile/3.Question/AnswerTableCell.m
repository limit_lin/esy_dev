
#import "AnswerTableCell.h"
#import "Global.h"

@implementation AnswerTableCell

- (void)awakeFromNib
{
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

+ (float)calcHeightFromInfo:(NSDictionary*)dicQuestionInfo
{
    NSArray     *arrayInfos = [dicQuestionInfo objectForKey:@"anon"];
    NSString    *strQuestion = [arrayInfos objectAtIndex:1];
    UIFont      *font = [UIFont fontWithName:@"HelveticaNeueLTStd-BdCn" size:14.0f];
    
    CGSize  constrainedSize = CGSizeMake(280, 500);
    CGSize  size = [strQuestion sizeWithFont:font constrainedToSize:constrainedSize];

    return size.height + 62;
}

- (void)setQuestionInfo:(NSDictionary*)dicQuestionInfo forIndex:(NSInteger)rowIndex
{
    _dicQuestionInfo = dicQuestionInfo;
    
    _nRowIndex = rowIndex;

    lblNumber.text = [NSString stringWithFormat:@"%d.", (int)(rowIndex+1) ];
    [lblNumber sizeToFit];
    
    NSArray     *arrayInfos = [_dicQuestionInfo objectForKey:@"anon"];
    NSString    *strQuestion = [arrayInfos objectAtIndex:1];
    
    lblViewQuestion.text = strQuestion;
    lblViewQuestion.textColor = TITLE_TEXT_COLLOR;
    
    [lblViewQuestion sizeToFit];

    CGRect frame = lblViewQuestion.frame;
    CGRect rect1 = viewAnswer.frame;
    rect1.origin.y = frame.origin.y + frame.size.height + 5;
    viewAnswer.frame = rect1;
    
    frame = lblNumber.frame;
    frame.origin.x = 27 - frame.size.width;
    lblNumber.frame = frame;
}

- (void)setSelectAnswer:(NSInteger)nSelIndex
{
    NSString    *strAnswer;

    _nSelIndex = nSelIndex;
    
    NSArray*    arrayInfos = [_dicQuestionInfo objectForKey:@"anon"];
    
    if( _nSelIndex >= 1 && _nSelIndex <= 4 )
        strAnswer = [arrayInfos objectAtIndex:_nSelIndex+1];
    else
        strAnswer = NSLocalizedString(@"Please Select",@"AnswerTableCell");
    
    lblAnswer.text = strAnswer;
}

- (IBAction)onClickCombo:(id)sender
{
    [self.parentViewController onClickAnswerButton:_nRowIndex selectedIndex:_nSelIndex];
}

- (void)dealloc
{
    [lblNumber release];
    [lblViewQuestion release];
    [viewAnswer release];
    [lblAnswer release];

    [super dealloc];
}

@end

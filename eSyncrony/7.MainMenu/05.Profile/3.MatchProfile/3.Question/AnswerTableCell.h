
#import <UIKit/UIKit.h>
#import "AnswerViewController.h"

@interface AnswerTableCell : UITableViewCell
{
    IBOutlet    UILabel     *lblNumber;
    IBOutlet    UILabel     *lblViewQuestion;

    IBOutlet    UIView      *viewAnswer;
    IBOutlet    UILabel     *lblAnswer;

    NSInteger   _nRowIndex;
    NSInteger   _nSelIndex;
    
    NSDictionary    *_dicQuestionInfo;
}

@property (nonatomic, assign) AnswerViewController* parentViewController;
@property NSInteger answernum;
+ (float)calcHeightFromInfo:(NSDictionary*)dicQuestionInfo;

- (void)setQuestionInfo:(NSDictionary*)dicQuestionInfo forIndex:(NSInteger)rowIndex;
- (void)setSelectAnswer:(NSInteger)nSelIndex;

- (IBAction)onClickCombo:(id)sender;

@end

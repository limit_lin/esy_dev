
#import <UIKit/UIKit.h>
#import "QAViewController.h"

@interface QATableViewCell : UITableViewCell
{
    IBOutlet    UILabel     *lblNumber;
    IBOutlet    UILabel     *lblViewQuestion;
    IBOutlet    UIButton    *btnStar;
    
    IBOutlet    UIImageView *imgViewBG;
    
    NSInteger   _nRowIndex;
}

@property (nonatomic, assign) QAViewController* parentViewController;

- (void)setQuestionInfo:(NSDictionary*)dicQuestionInfo forIndex:(NSInteger)rowIndex;

- (IBAction)onClickStar:(id)sender;

@end

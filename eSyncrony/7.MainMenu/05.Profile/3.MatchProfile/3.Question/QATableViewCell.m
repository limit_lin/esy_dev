
#import "QATableViewCell.h"
#import "Global.h"

@implementation QATableViewCell

- (void)awakeFromNib
{
//    self.parentViewController = [[QAViewController alloc]init];//append
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    imgViewBG.highlighted = selected;
    
}
-(void)cellSelected{

    NSLog(@"self.parentViewController.kindsArrItems == %@",self.parentViewController.kindsArrItems);
//    NSMutableArray *arr=[[NSMutableArray alloc]init];
//    
//    arr = [self.parentViewController.kindsArrItems mutableCopy];
//    
//    for (int i = 0; i < arr.count;i++) {
//        if ([lblViewQuestion.text isEqualToString:[arr objectAtIndex:i]]) {
//            [self setSelected:YES animated:YES];
//        }
//    }
//    
}
- (void)setQuestionInfo:(NSDictionary*)dicQuestionInfo forIndex:(NSInteger)rowIndex
{
    _nRowIndex = rowIndex;
    
    lblNumber.text = [NSString stringWithFormat:@"%d.", (int)(rowIndex+1) ];
    [lblNumber sizeToFit];

    lblViewQuestion.text = [dicQuestionInfo objectForKey:@"question"];
    if (self.parentViewController.selectedNum > 0) {
        [self cellSelected];//append 151020
    }
    lblViewQuestion.textColor = TITLE_TEXT_COLLOR;
    
    [lblViewQuestion sizeToFit];

    CGRect frame = lblViewQuestion.frame;
    CGSize size = frame.size;
    frame.origin.y = (60 - size.height)/2+2;
    frame.size = size;
    lblViewQuestion.frame = frame;

    CGRect rect = lblNumber.frame;
    rect.origin.y = frame.origin.y;//+5
    lblNumber.frame = rect;

    BOOL bFav = [[dicQuestionInfo objectForKey:@"bFav"] boolValue];
    [btnStar setSelected:bFav];
    
    frame = lblNumber.frame;
    frame.origin.x = 28 - frame.size.width;
    lblNumber.frame = frame;
}

- (IBAction)onClickStar:(id)sender
{
    if( [btnStar isSelected] )
        [self.parentViewController removeQuestionFromFavourite:_nRowIndex];
    else
        [self.parentViewController addQuestionToFavourite:_nRowIndex];
}

- (void)dealloc
{
    [lblNumber release];
    [lblViewQuestion release];
    [btnStar release];
    [imgViewBG release];
    
    [super dealloc];
}

@end



#import <UIKit/UIKit.h>

@interface PastQuestionTableCell : UITableViewCell
{
    IBOutlet    UILabel     *lblNumber;
    IBOutlet    UILabel     *lblViewQuestion;
    
    IBOutlet    UILabel     *lblMyAnswer;
    IBOutlet    UILabel     *lblMatchAnswer;
    IBOutlet    UIView      *viewAnswer;
}

+ (float)calcHeightFromInfo:(NSDictionary*)dicQuestionInfo;

- (void)setQuestionInfo:(NSDictionary*)dicQuestionInfo matchName:(NSString*)strMatchName forIndex:(NSInteger)rowIndex;

@end


#import <UIKit/UIKit.h>
#import "DataSelectViewController.h"
#import "QAViewController.h"

@interface AnswerViewController : GAITrackedViewController <DataSelectViewDelegate,UIAlertViewDelegate>
{
    IBOutlet UIButton       *btnQuestions;
    IBOutlet UIButton       *btnPastQues;
    
    IBOutlet UIButton       *btnConfirm;
    
    IBOutlet UIScrollView   *scrView;
    IBOutlet UITableView    *tblView;
    
    IBOutlet UILabel        *lblAnswerTitle;
    
    NSMutableArray          *answersList;
    NSArray                 *pastQuestionsList;
}

@property (nonatomic, assign) BOOL  bPastQuestions;
@property (nonatomic, assign) BOOL  bAnswer;
@property (nonatomic, assign) NSMutableDictionary   *dicMatchInfo;
@property (nonatomic, strong) NSMutableArray        *arrayQuestionsList;
@property (nonatomic, strong) NSArray *arrAnswers;
@property (nonatomic, assign) QAViewController  *prevViewController;

- (IBAction)onClickBack:(id)sender;

- (IBAction)onClickConfirm:(id)sender;

- (IBAction)onClickQuestions:(id)sender;

- (IBAction)onClickPastQues:(id)sender;

- (void)onClickAnswerButton:(NSInteger)rowIndex selectedIndex:(NSInteger)nSelIndex;

@end

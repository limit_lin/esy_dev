
#import "AnswerViewController.h"

#import "AnswerTableCell.h"
#import "PastQuestionTableCell.h"

#import "DSActivityView.h"
#import "Global.h"
#import "UtilComm.h"
#import "mainMenuViewController.h"
#import "eSyncronyAppDelegate.h"

@implementation AnswerViewController
{
    NSMutableDictionary *questionInfo;
}

- (void)refreshButtonStatus
{
    if( self.bPastQuestions == NO )
    {
        [btnQuestions setBackgroundImage:[UIImage imageNamed:@"greenbtn_n.png"] forState:UIControlStateNormal];
        [btnQuestions setBackgroundImage:[UIImage imageNamed:@"greenbtn_p.png"] forState:UIControlStateHighlighted];
        [btnQuestions setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [btnPastQues setBackgroundImage:[UIImage imageNamed:@"whitebtn_n.png"] forState:UIControlStateNormal];
        [btnPastQues setBackgroundImage:[UIImage imageNamed:@"whitebtn_p.png"] forState:UIControlStateHighlighted];
        [btnPastQues setTitleColor:TITLE_TEXT_COLLOR forState:UIControlStateNormal];
        
        btnConfirm.hidden = NO;
    }
    else
    {
        [btnPastQues setBackgroundImage:[UIImage imageNamed:@"greenbtn_n.png"] forState:UIControlStateNormal];
        [btnPastQues setBackgroundImage:[UIImage imageNamed:@"greenbtn_p.png"] forState:UIControlStateHighlighted];
        [btnPastQues setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [btnQuestions setBackgroundImage:[UIImage imageNamed:@"whitebtn_n.png"] forState:UIControlStateNormal];
        [btnQuestions setBackgroundImage:[UIImage imageNamed:@"whitebtn_p.png"] forState:UIControlStateHighlighted];
        [btnQuestions setTitleColor:TITLE_TEXT_COLLOR forState:UIControlStateNormal];
        
        btnConfirm.hidden = YES;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self refreshButtonStatus];
    [self refreshContentSize];
    
    if( self.bPastQuestions == YES ){
        self.screenName = @"PastQuestionsView";
        [self refreshPastAnswersList];
    }else{
        self.screenName = @"AnswerView";
    }
}

- (void)refreshPastAnswersList
{
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(requestPastAnswers) withObject:nil afterDelay:0.1];
}

- (void)requestPastAnswers
{
    [DSBezelActivityView removeViewAnimated:NO];//no work
    
    NSString    *macc_no = [self.dicMatchInfo objectForKey:@"acc_no"];
    if( macc_no == nil )
        macc_no = [self.dicMatchInfo objectForKey:@"Acc_no"];
    if( macc_no == nil )
        return;
    
    NSDictionary    *result = [UtilComm viewAnswers:macc_no];
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"esync"];
        return;
    }
    
    NSString    *response = [result objectForKey:@"response"];
    
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
        return;
    }
    
    // there is no asked questions
    if( [response isKindOfClass:[NSString class]] && [response isEqualToString:@""] )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"No Past Questions Found.",@"AnswerView") withTitle:@"esync"];
    }
    // if there is asked questions from match
    else if( [response isKindOfClass:[NSDictionary class]] )
    {
        [DSBezelActivityView removeViewAnimated:NO];//append
        
        [pastQuestionsList release];

        NSDictionary    *responseDic = (NSDictionary*)response;
        pastQuestionsList = [[responseDic objectForKey:@"item"] retain];
        
        [tblView reloadData];
        [self refreshContentSize];
        
        [DSBezelActivityView removeViewAnimated:NO];//no work
        
    }
    else
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Error",)];
}

//======================================================================================================//
//======================================================================================================//
//======================================================================================================//

- (void)refreshContentSize
{
    float   height = 0;
    
    if( self.bPastQuestions == NO )
    {
        for( int i = 0; i < [self.arrayQuestionsList count]; i++ )
            height += [AnswerTableCell calcHeightFromInfo:[self.arrayQuestionsList objectAtIndex:i]];
    }
    else
    {
        for( int i = 0; i < [pastQuestionsList count]; i++ )
            height += [PastQuestionTableCell calcHeightFromInfo:[pastQuestionsList objectAtIndex:i]];
    }
    
    CGRect frame = tblView.frame;
    
    if( self.bPastQuestions == NO )
    {
        lblAnswerTitle.hidden = NO;
        frame.origin.y = 40;
    }
    else
    {
        lblAnswerTitle.hidden = YES;
        frame.origin.y = 0;
    }
    frame.size.height = height;
    
    tblView.frame = frame;

    scrView.contentSize = CGSizeMake(frame.size.width, frame.origin.y + frame.size.height);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"Answer.arrayQuestionsList.count:%lu",(unsigned long)self.arrayQuestionsList.count);
    if( self.bPastQuestions == NO )
    {
        if( self.arrayQuestionsList == nil )
            return 0;

        return [self.arrayQuestionsList count];
    }
    else
    {
        if( pastQuestionsList == nil )
            return 0;
        
        return [pastQuestionsList count];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( self.bPastQuestions == NO )
    {
        NSMutableDictionary     *dicQuestionInfo = [self.arrayQuestionsList objectAtIndex:indexPath.row];
        return [AnswerTableCell calcHeightFromInfo:dicQuestionInfo];
    }
    else
    {
        NSMutableDictionary     *dicQuestionInfo = [pastQuestionsList objectAtIndex:indexPath.row];
        return [PastQuestionTableCell calcHeightFromInfo:dicQuestionInfo];
    }
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( self.bPastQuestions == NO )
    {
        NSMutableDictionary     *dicQuestionInfo = [_arrayQuestionsList objectAtIndex:indexPath.row];
        AnswerTableCell         *cell;
    
        NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"AnswerTableCell" owner:nil options:nil];
        cell = [arr objectAtIndex:0];
        NSInteger answernum = [[self.arrAnswers objectAtIndex:indexPath.row]intValue];
        
        [cell setQuestionInfo:dicQuestionInfo forIndex:indexPath.row];
        cell.parentViewController = self;
        [cell setSelectAnswer:answernum];
        NSMutableDictionary *mutableQuestion = [dicQuestionInfo mutableCopy];
        
        if (answernum!=0) {//crash
            
//            [dicQuestionInfo setObject:[NSNumber numberWithInteger:answernum] forKey:@"selAnswerID"];
             [mutableQuestion setObject:[NSNumber numberWithInteger:answernum] forKey:@"selAnswerID"];
             dicQuestionInfo = [mutableQuestion mutableCopy];//append 151214
        }
        return cell;
    }
    else
    {
        NSMutableDictionary     *dicQuestionInfo = [pastQuestionsList objectAtIndex:indexPath.row];
        PastQuestionTableCell   *cell;
        
        NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"PastQuestionTableCell" owner:nil options:nil];
        cell = [arr objectAtIndex:0];
        
        [cell setQuestionInfo:dicQuestionInfo matchName:[self.dicMatchInfo objectForKey:@"nname"] forIndex:indexPath.row];

        return cell;
    }

    return nil;
}

//====================================================================================//
//====================================================================================//
//====================================================================================//

- (void)DataSelectView:(DataSelectViewController *)dataSelectView didSelectItem:(int)itemIndex withTag:(int)tag
{
    AnswerTableCell *cell = (AnswerTableCell*)[tblView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:tag inSection:0]];

    [cell setSelectAnswer:itemIndex];
    
    [dataSelectView release];
    
    questionInfo = [self.arrayQuestionsList objectAtIndex:tag];
    NSMutableDictionary *mutableQuestionInfo = [questionInfo mutableCopy];
//    [questionInfos setObject:[NSNumber numberWithInt:itemIndex] forKey:@"selAnswerID"];//crash
    [mutableQuestionInfo setObject:[NSNumber numberWithInt:itemIndex] forKey:@"selAnswerID"];
    
    questionInfo=[mutableQuestionInfo mutableCopy];

    [self.arrayQuestionsList replaceObjectAtIndex:tag withObject:questionInfo];//append 151214
    
}

- (void)makeAnswerList:(NSInteger)rowIndex
{
    [answersList release];
    answersList = nil;
    
    answersList = [[NSMutableArray alloc] initWithCapacity:0];
    [answersList addObject:NSLocalizedString(@"Please Select",@"AnswerView")];
    
    NSDictionary    *dic_questionInfo = [self.arrayQuestionsList objectAtIndex:rowIndex];
    NSArray         *arrayInfos = [dic_questionInfo objectForKey:@"anon"];
    
    for( int i = 1; i <= 4; i++ )
    {
        NSString*   answerItem = [arrayInfos objectAtIndex:i+1];
        if( answerItem == nil || [answerItem isEqualToString:@""] )
            continue;

        [answersList addObject:answerItem];
    }
}

- (void)onClickAnswerButton:(NSInteger)rowIndex selectedIndex:(NSInteger)nSelIndex
{
    [self makeAnswerList:rowIndex];

    DataSelectViewController*   answerListView = [DataSelectViewController createWithDataList:answersList selectIndex:(int)nSelIndex withTag:(int)rowIndex];
    
    answerListView.delegate = self;
    [self.view addSubview:answerListView.view];
    answerListView.view.frame = self.view.bounds;
}

//====================================================================================//
//====================================================================================//
//====================================================================================//

- (IBAction)onClickBack:(id)sender
{
    if( self.bAnswer == YES || self.bPastQuestions == YES )
        [self.navigationController popToViewController:self.prevViewController.mprofileViewController animated:YES];
    else
        [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)checkAnswers
{
    for( int i = 0; i < [self.arrayQuestionsList count]; i++ )
    {
        NSMutableDictionary *p_questionInfo = [self.arrayQuestionsList objectAtIndex:i];
        p_questionInfo = [self.arrayQuestionsList objectAtIndex:i];
        id numAnswer = [p_questionInfo objectForKey:@"selAnswerID"];
        //append
        if (numAnswer == nil) {
            questionInfo = [self.arrayQuestionsList objectAtIndex:i];
            numAnswer = [questionInfo objectForKey:@"selAnswerID"];
        }
        //end
        if( numAnswer == nil )
            return NO;
        
        if( [numAnswer intValue] == 0 )
            return NO;
    }
    
    return YES;
}

- (IBAction)onClickConfirm:(id)sender
{
    if( [self checkAnswers] == NO )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Please answer all selected questions.",@"AnswerView") withTitle:@"esync"];
        return;
    }
    
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    
    if( self.bAnswer == NO ){
        
        if ([[eSyncronyAppDelegate sharedInstance].membership isEqualToString:@"Free"]) {
            
            [DSBezelActivityView removeViewAnimated:NO];
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"esync" message:NSLocalizedString(@"To send questions to your matches, subscribe to our premium packages now!",@"AnswerView") delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",) otherButtonTitles:NSLocalizedString(@"OK",), nil];
            alert.tag = 2000;
            [alert show];
            [alert release];
            
        }else{
            [self performSelector:@selector(sendQuestionsProc) withObject:nil afterDelay:0.1];
        }
        
    }
    else{
        [self performSelector:@selector(sendAnswerProc) withObject:nil afterDelay:0.1];
    }
}

- (NSString*)getQuestionString
{
    NSString    *strQuestions = @"";
    int nAnswerID = 0;
    for( int i = 0; i < [self.arrayQuestionsList count]; i++ )
    {
        NSMutableDictionary *dicQuestion= [self.arrayQuestionsList objectAtIndex:i];
        NSArray     *arrayInfos = [dicQuestion objectForKey:@"anon"];
        int nQuestionID = [[arrayInfos objectAtIndex:0] intValue];
        nAnswerID = [[dicQuestion objectForKey:@"selAnswerID"] intValue];
        NSLog(@"nAnswerID == %d",nAnswerID);
        if( [strQuestions isEqualToString:@""] )
            strQuestions = [NSString stringWithFormat:@"%d,%d", nQuestionID, nAnswerID];
        else
            strQuestions = [NSString stringWithFormat:@"%@|%d,%d", strQuestions, nQuestionID,nAnswerID];
    }

    return strQuestions;
}

- (void)displayUpgrade
{
    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl procUpgrade];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag==2000) {
        if (buttonIndex==1) {
            [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.navigationController popViewControllerAnimated:NO];
            [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.navigationController popViewControllerAnimated:NO];
            [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.navigationController popViewControllerAnimated:NO];
            [self performSelector:@selector(displayUpgrade) withObject:nil afterDelay:0.1];
        }

    }
    
    if (alertView.tag == 2001) {
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.navigationController popViewControllerAnimated:NO];
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.navigationController popViewControllerAnimated:YES];
    }
}
- (void)sendQuestionsProc
{
    NSString    *macc_no = [self.dicMatchInfo objectForKey:@"acc_no"];
    if( macc_no == nil )
        macc_no = [self.dicMatchInfo objectForKey:@"Acc_no"];
    if( macc_no == nil ){
        [DSBezelActivityView removeViewAnimated:NO];
        return;
    }
    NSString    *strQuestions = [self getQuestionString];
    NSDictionary    *result = [UtilComm sendQuestions:strQuestions toMatch:macc_no];
    [DSBezelActivityView removeViewAnimated:NO];
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"esync"];
        return;
    }
    
    NSString    *response = [result objectForKey:@"response"];
    if( ![response isKindOfClass:[NSString  class]] )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Error",)];
        return;
    }
    
    if( [response hasPrefix:@"ERROR"] )
    {
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
        return;
    }
    
    if( ![response isEqualToString:@"true"] )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Error",)];
        return;
    }
    
    NSString    *match_name = [self.dicMatchInfo objectForKey:@"nname"];
    NSString    *strMsg = [NSString stringWithFormat:NSLocalizedString(@"Your questions have been sent to %@. We will notify you via email when %@ has replied",@"AnswerView"), match_name, match_name];

    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"esync"
                                                 message:strMsg
                                                delegate:self
                                       cancelButtonTitle:NSLocalizedString(@"OK",)
                                       otherButtonTitles:nil];
    av.tag = 2001;
    [av show];
    [av release];
}

- (BOOL)sendAnswer:(int)nAnswer toQuestion:(int)nQuestion toMatch:(NSString*)macc_no
{
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    NSDictionary    *result = [UtilComm sendAnswer:nAnswer toQuestions:nQuestion toMatch:macc_no];
    [DSBezelActivityView removeViewAnimated:NO];
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"esync"];
        return NO;
    }
    
    NSString    *response = [result objectForKey:@"response"];
    if( ![response isKindOfClass:[NSString  class]] )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Error",)];
        return NO;
    }
    
    if( [response hasPrefix:@"ERROR"] )
    {
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
        return NO;
    }
    
    if( ![response isEqualToString:@"true"] )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Error",)];
        return NO;
    }
    
    return YES;
}

- (void)sendAnswerProc
{

    NSString    *macc_no = [self.dicMatchInfo objectForKey:@"acc_no"];
    if( macc_no == nil )
        macc_no = [self.dicMatchInfo objectForKey:@"Acc_no"];
    if( macc_no == nil ){
        [DSBezelActivityView removeViewAnimated:NO];
        return;
    }
    for( int i = 0; i < [self.arrayQuestionsList count]; i++ )
    {
        NSMutableDictionary *dic_questionInfo = [self.arrayQuestionsList objectAtIndex:i];
        
        NSArray     *arrayInfos = [dic_questionInfo objectForKey:@"anon"];
        
        int nQuestionID = [[arrayInfos objectAtIndex:0] intValue];
        int nAnswerID = [[dic_questionInfo objectForKey:@"selAnswerID"] intValue];
        
        if( [self sendAnswer:nAnswerID toQuestion:nQuestionID toMatch:macc_no] == NO )
            return;
    }
    [DSBezelActivityView removeViewAnimated:NO];
    self.bPastQuestions = YES;
    [self refreshButtonStatus];
    [self refreshPastAnswersList];
}



- (IBAction)onClickQuestions:(id)sender
{
    if( self.bPastQuestions == NO )
        return;

    if( self.bAnswer == YES )
        [self.prevViewController refreshQuestionsList];

    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)onClickPastQues:(id)sender
{
    if( self.bPastQuestions == YES )
        return;
    
    self.bPastQuestions = YES;
    [self refreshButtonStatus];
    [self refreshPastAnswersList];
}

- (void)dealloc
{
    [btnQuestions release];
    [btnPastQues release];
    [scrView release];
    [tblView release];
    [lblAnswerTitle release];
    
    [answersList release];
    [pastQuestionsList release];
    [btnConfirm release];
    
    [self.arrayQuestionsList release];
    [self.arrAnswers release];
    [super dealloc];
}

@end

#import "QAViewController.h"
#import "DSActivityView.h"
#import "Global.h"
#import "UtilComm.h"
#import "mainMenuViewController.h"
#import "eSyncronyAppDelegate.h"

#import "QATableViewCell.h"
#import "AnswerViewController.h"

@interface QAViewController ()

@end

@implementation QAViewController
@synthesize dicSelectedQuestionInfo;
@synthesize kindsArrItems;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"Q&AView";
    _nShowCount = 0;
    kindsArrItems = [[NSMutableArray alloc]init];
    dicSelectedQuestionInfo =[[NSMutableDictionary alloc]init];
    
    lblAskTitle.text = [NSString stringWithFormat:NSLocalizedString(@"Ask %@ Some Questions",@"Q&AView"), [self.dicMatchInfo objectForKey:@"nname"]];
    
    viewFooter.hidden = YES;
    
    [self onClickHobby:btnHobby];//append
    
//    ios7下，默认UITableView的分隔线没有满屏，左边会空出大概10个像素，需要用下面的代码将其设置为满屏
    [tblView setSeparatorInset:UIEdgeInsetsZero];
//    [self refreshQuestionsList];//0821

}

- (void)refreshQuestionsList
{
    
    [_arrayQuestionsList release];
    
    _arrayQuestionsList = [[NSMutableArray alloc] initWithCapacity:0];
    _nShowCount = 0;
    
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(requestAskedQuestions) withObject:nil afterDelay:0.1];
}

- (void)requestAskedQuestions
{
    [DSBezelActivityView removeViewAnimated:NO];
    
    NSString    *macc_no = [self.dicMatchInfo objectForKey:@"acc_no"];
    if( macc_no == nil )
        macc_no = [self.dicMatchInfo objectForKey:@"Acc_no"];
    if( macc_no == nil )
        return;
    
//    [self loadQuestions:macc_no];
//    return;
//下列不执行！
    
    NSDictionary    *result = [UtilComm loadAskedQuestions:macc_no];
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"eSynchrony"];
        return;
    }
    
    NSString    *response = [result objectForKey:@"response"];
    
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
        return;
    }
    
    // there is no asked questions
    if( [response isKindOfClass:[NSString class]] && [response isEqualToString:@""] )
    {
        NSLog(@"!!!!there is no asked questions!!!!");
        [self loadQuestions:macc_no];
    }
    // if there is asked questions from match
    else if( [response isKindOfClass:[NSDictionary class]] )
    {
        NSLog(@"!!!!there is asked questions from match!!!!");
        [self addQuestionsList:response bFav:YES];
        
//        [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
        [self performSelector:@selector(showAnswerView) withObject:nil afterDelay:0.1];
    }
    else
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Error",)];
    
}

-(void)showAnswerView{
    [DSBezelActivityView removeViewAnimated:NO];//no work
    AnswerViewController* viewController = [[[AnswerViewController alloc] initWithNibName:@"AnswerViewController" bundle:nil] autorelease];
    
    viewController.arrayQuestionsList = _arrayQuestionsList;
    viewController.dicMatchInfo = self.dicMatchInfo;
    viewController.bAnswer = YES;
    viewController.bPastQuestions = NO;
    viewController.prevViewController = self;
    
    [self.navigationController pushViewController:viewController animated:NO];
}
- (BOOL)loadFavouriteQuestions:(NSString*)macc_no
{
//    https://www.esynchrony.com/api/user/index.php/QnA/viewFaveQuestions?token_key=6eb58ab6e1cea16f0e7b6956f9e81713&macc_no=95735&acc_no=1137
    
    NSDictionary    *result = [UtilComm loadFavouriteQuestions:macc_no andCategory:(int)tag];
//    NSDictionary    *result = [UtilComm loadFavouriteQuestions:macc_no];
    
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"eSynchrony"];
        return NO;
    }
    
    id    response = [result objectForKey:@"response"];

    return [self addQuestionsList:response bFav:YES];
}

- (BOOL)loadNormalQuestions:(NSString*)macc_no
{
//    https://www.esynchrony.com/api/user/index.php/QnA/viewQuestion?token_key=6eb58ab6e1cea16f0e7b6956f9e81713&macc_no=95735&acc_no=1137
    
    NSDictionary    *result = [UtilComm loadNormalQuestions:macc_no andCategory:(int)tag];
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"eSynchrony"];
        return NO;
    }

    id    response = [result objectForKey:@"response"];
    
    return [self addQuestionsList:response bFav:NO];
}

- (BOOL)addQuestionsList:(id)response bFav:(BOOL)bFav
{
    if( [response isKindOfClass:[NSString  class]] && [response hasPrefix:@"ERROR"] )
    {
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
        return NO;
    }

    if( ![response isKindOfClass:[NSDictionary  class]] )
        return YES;
    
    NSDictionary    *responseDic = (NSDictionary*)response;
//    NSArray *arrayQuestions = [[NSArray alloc]init];
//    arrayQuestions = [responseDic objectForKey:@"item"];
    NSArray *arrayQuestions = [responseDic objectForKey:@"item"];//151023
  
//    NSLog(@"boolean == %hhd count ==  %d",[arrayQuestions isKindOfClass:[NSArray class]],arrayQuestions.count);//boolean == 0 count ==  1
    if (arrayQuestions.count == 1) {
        if ([arrayQuestions isKindOfClass:[NSDictionary class]]) {//为了解决问题为一个api返回的一个bug bylimit1013
            [_arrayQuestionsList addObject:arrayQuestions];
            return YES;
        }
    }
    
    if( arrayQuestions == nil ||(![arrayQuestions isKindOfClass:[NSArray class]]))
    {
//        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Error",)];
        return NO;
    }

    for( int i = 0; i < [arrayQuestions count]; i++ )
    {
        NSDictionary            *item = [arrayQuestions objectAtIndex:i];
        NSMutableDictionary     *itemQues = [NSMutableDictionary dictionaryWithDictionary:item];
        
        [itemQues setObject:[NSNumber numberWithBool:bFav] forKey:@"bFav"];
        
        [_arrayQuestionsList addObject:itemQues];
    }
    return YES;
}

- (void)loadQuestions:(NSString*)macc_no
{
    if( [self loadFavouriteQuestions:macc_no] == NO ){
        NSLog(@"[loadFavouriteQuestions:macc_no] == NO");

    }
    
    if( [self loadNormalQuestions:macc_no] == NO ){
        NSLog(@"[loadNormalQuestions:macc_no] == NO");

    }
    
    _nShowCount = MIN( 10, [_arrayQuestionsList count] );
    
    [tblView reloadData];
    [self refreshContentSize];
}

- (void)refreshContentSize
{
    CGRect frame = tblView.frame;
    frame.size.height = 60*_nShowCount;
    tblView.frame = frame;

    CGRect rect = viewFooter.frame;
    rect.origin.y = frame.origin.y + frame.size.height;
    viewFooter.frame = rect;
    
    if( _nShowCount == [_arrayQuestionsList count] )
    {
        viewFooter.hidden = YES;
//        scrView.contentSize = CGSizeMake( rect.size.width, rect.origin.x);
         scrView.contentSize = CGSizeMake( rect.size.width,rect.origin.y + rect.size.height);//modify for Lacking of resolve when the number 10 can not slide a problemm 
    }
    else
    {
        viewFooter.hidden = NO;
        scrView.contentSize = CGSizeMake( rect.size.width, rect.origin.y + rect.size.height );
    }
}

//==========================================================================================================//
//==========================================================================================================//
//==========================================================================================================//

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"nShowCount:%lu",(unsigned long)_nShowCount);
    return _nShowCount;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSMutableDictionary     *dicQuestionInfo = [_arrayQuestionsList objectAtIndex:indexPath.row];
    QATableViewCell         *cell;

    NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"QATableViewCell" owner:nil options:nil];
    cell = [arr objectAtIndex:0];

    [cell setQuestionInfo:dicQuestionInfo forIndex:indexPath.row];
    cell.parentViewController = self;

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//      NSMutableDictionary         *dicQuestionInfo = [arrMatchesItems objectAtIndex:indexPath.row-1];
//    dicSelectedQuestionInfo = [_arrayQuestionsList objectAtIndex:indexPath.row];
   
}
- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    if (self.selectedNum >= 5) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Sorry",) message:NSLocalizedString(@"You can only ask 5 questions everyday.",@"QAView") delegate:self cancelButtonTitle:NSLocalizedString(@"OK",) otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        return nil;
    }else{
        
        NSDictionary *sourceDict = [NSDictionary dictionaryWithDictionary:[_arrayQuestionsList objectAtIndex:indexPath.row]];
        NSString *questionTitle= [sourceDict objectForKey:@"question"];
        NSString *row_id = [sourceDict objectForKey:@"row_id"];
        [kindsArrItems addObject:questionTitle];
        
//        [dicSelectedQuestionInfo addEntriesFromDictionary:sourceDict];
        [dicSelectedQuestionInfo setObject:sourceDict forKey:row_id];
        
        NSLog(@"kindsArrItems == %@",kindsArrItems);
        NSLog(@"dicQuestionInfo == %@",dicSelectedQuestionInfo);
        
        self.selectedNum++;
        return indexPath;

    }
    
}

- (NSIndexPath *)tableView:(UITableView *)tableView willDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *sourceDict = [NSDictionary dictionaryWithDictionary:[_arrayQuestionsList objectAtIndex:indexPath.row]];
    NSString *questionTitle= [sourceDict objectForKey:@"question"];
    NSString *row_id = [sourceDict objectForKey:@"row_id"];
    [kindsArrItems removeObject:questionTitle];
    [dicSelectedQuestionInfo removeObjectForKey:row_id];
    
    NSLog(@"kindsArrItems == %@",kindsArrItems);
    NSLog(@"dicQuestionInfo == %@",dicSelectedQuestionInfo);
    self.selectedNum--;
    return indexPath;
    
}
- (void)addQuestionToFavourite:(NSInteger)rowIndex
{
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(addQuestionToFavouriteProc:) withObject:[NSNumber numberWithInteger:rowIndex] afterDelay:0.1];
}

- (void)addQuestionToFavouriteProc:(NSNumber*)index
{
    NSInteger   rowIndex = [index integerValue];

    [DSBezelActivityView removeViewAnimated:NO];
    
    NSDictionary    *item = [_arrayQuestionsList objectAtIndex:rowIndex];
    NSString        *questionID = [item objectForKey:@"row_id"];
    
    NSDictionary    *result = [UtilComm addQuestionToFavourite:questionID];
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"eSynchrony"];
        return;
    }
    
    NSString    *response = [result objectForKey:@"response"];
    if( ![response isKindOfClass:[NSString  class]] )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Error",)];
        return;
    }
    
    if( [response hasPrefix:@"ERROR"] )
    {
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
        return;
    }
    
    [item setValue:[NSNumber numberWithBool:YES] forKey:@"bFav"];

    QATableViewCell*    cell = (QATableViewCell*)[tblView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:rowIndex inSection:0]];
    [cell setQuestionInfo:item forIndex:rowIndex];
}

- (void)removeQuestionFromFavourite:(NSInteger)rowIndex
{
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(removeQuestionFromFavouriteProc:) withObject:[NSNumber numberWithInteger:rowIndex] afterDelay:0.1];
}

- (void)removeQuestionFromFavouriteProc:(NSNumber*)index
{
    NSInteger   rowIndex = [index integerValue];
    
    [DSBezelActivityView removeViewAnimated:NO];
    
    NSDictionary    *item = [_arrayQuestionsList objectAtIndex:rowIndex];
    NSString        *questionID = [item objectForKey:@"row_id"];
    
    NSDictionary    *result = [UtilComm removeQuestionFromFavourite:questionID];
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"eSynchrony"];
        return;
    }
    
    NSString    *response = [result objectForKey:@"response"];
    if( ![response isKindOfClass:[NSString  class]] )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Error",)];
        return;
    }
    
    if( [response hasPrefix:@"ERROR"] )
    {
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
        return;
    }

    [item setValue:[NSNumber numberWithBool:NO] forKey:@"bFav"];
    
    QATableViewCell*    cell = (QATableViewCell*)[tblView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:rowIndex inSection:0]];
    [cell setQuestionInfo:item forIndex:rowIndex];
}

//==========================================================================================================//
//==========================================================================================================//
//==========================================================================================================//

- (IBAction)onClickBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSMutableArray*)getSelectedQuestions
{
//    NSArray* selectedItems = tblView.indexPathsForSelectedRows;
//    
//    if( selectedItems == nil || [selectedItems count] == 0 )
//        return nil;
//    
//    NSMutableArray  *arrayQuestions = [NSMutableArray arrayWithCapacity:0];
//    self.questionlistStr = @"";
//    for( int i = 0; i < [selectedItems count]; i++ )
//    {
//        NSIndexPath     *indexPath = [selectedItems objectAtIndex:i];
//        NSDictionary    *questionInfo = [_arrayQuestionsList objectAtIndex:indexPath.row];
//        NSString *row_id = [[questionInfo objectForKey:@"row_id"]stringByAppendingString:@"|"];
//        self.questionlistStr = [self.questionlistStr stringByAppendingString:row_id];
//        [arrayQuestions addObject:questionInfo];
//    }
//    if ([selectedItems count]==1) {
//        self.questionlistStr = [self.questionlistStr stringByReplacingOccurrencesOfString:@"|" withString:@""];
//    }
//    return arrayQuestions;
    
    if( kindsArrItems == nil || [kindsArrItems count] == 0 )
        return nil;
    
    NSMutableArray  *arrayQuestions = [NSMutableArray arrayWithCapacity:0];
    self.questionlistStr = @"";

    NSEnumerator *keys1 = [dicSelectedQuestionInfo keyEnumerator];

    for (NSObject *keys2 in keys1) {
//            NSLog(@"obj: %@", keys2);
        //通过KEY找到value
//            NSObject *object = [dicSelectedQuestionInfo objectForKey:keys2];
        NSDictionary *questionInfo = [dicSelectedQuestionInfo objectForKey:keys2];
        NSString *row_id = [[questionInfo objectForKey:@"row_id"]stringByAppendingString:@"|"];
        
        self.questionlistStr = [self.questionlistStr stringByAppendingString:row_id];
        [arrayQuestions addObject:questionInfo];
//            NSLog(@"object==%@",object);

    }
    
    if ([kindsArrItems count]==1) {
        self.questionlistStr = [self.questionlistStr stringByReplacingOccurrencesOfString:@"|" withString:@""];
    }
    return arrayQuestions;
}
-(void)requestAnsweredQuestions{
    
    NSLog(@"self.questionlistStr:%@",self.questionlistStr);
    NSLog(@"self.arrayQuestions:%@",self.arrayQuestions);
    if (self.questionlistStr.length==0) {
        return;
    }
    NSDictionary    *result = [UtilComm retrievePreviousAnswer:self.questionlistStr];
    [DSBezelActivityView removeViewAnimated:YES];
    if (result == nil) {
        return;
    }
    NSDictionary *response = [result objectForKey:@"response"];
    id item = [response objectForKey:@"item"];
    NSArray *arrAnswers;
    if([item isKindOfClass:[NSArray class]]){
        arrAnswers = item;
    }else if ([item isKindOfClass:[NSString class]]) {
        arrAnswers = [NSArray arrayWithObject:item];
    }else{
        return;
    }
    
    AnswerViewController* viewController = [[[AnswerViewController alloc] initWithNibName:@"AnswerViewController" bundle:nil] autorelease];
    
    viewController.arrayQuestionsList = self.arrayQuestions;
    viewController.dicMatchInfo = self.dicMatchInfo;
    viewController.arrAnswers = arrAnswers;
    viewController.bAnswer = NO;
    viewController.bPastQuestions = NO;
    viewController.prevViewController = self;
    
    [self.navigationController pushViewController:viewController animated:YES];
    
}
- (IBAction)onClickNext:(id)sender
{
    self.arrayQuestions = [self getSelectedQuestions];
    
    if( self.arrayQuestions == nil )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Please select at least 1 question to ask!",@"QAView") withTitle:@"eSynchrony"];
        return;
    }
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(requestAnsweredQuestions) withObject:nil afterDelay:0.1];

}

- (IBAction)onClickQuestions:(id)sender
{
}

-(void)p_clickBttonQA:(UIButton *)button
{
    [button becomeFirstResponder];
    [button setBackgroundImage:[UIImage imageNamed:@"whitebtn_n.png"] forState:UIControlStateNormal];
    [button setBackgroundImage:[UIImage imageNamed:@"greenbtn_n.png"] forState:UIControlStateSelected];
    tag = button.tag;
    
    [self refreshQuestionsList];
}

#pragma button kinds action
- (IBAction)onClickHobby:(id)sender {
//    NSLog(@"on click hobby");
 
    [btnHobby setSelected:YES];
    [btnPersonal setSelected:NO];
    [btnFamily setSelected:NO];
    [btnLove setSelected:NO];
    [btnDatingCriteria setSelected:NO];
    
    [self p_clickBttonQA:btnHobby];

}

- (IBAction)onClickLove:(id)sender {
//    NSLog(@"on click love");
    
    [btnHobby setSelected:NO];
    [btnPersonal setSelected:NO];
    [btnFamily setSelected:NO];
    [btnLove setSelected:YES];
    [btnDatingCriteria setSelected:NO];
    
    [self p_clickBttonQA:btnLove];
    
}

- (IBAction)onClickDatingCriteria:(id)sender {
//    NSLog(@"on click dating criteria");
    [btnHobby setSelected:NO];
    [btnPersonal setSelected:NO];
    [btnFamily setSelected:NO];
    [btnLove setSelected:NO];
    [btnDatingCriteria setSelected:YES];

    [self p_clickBttonQA:btnDatingCriteria];
}

- (IBAction)onClickPersonal:(id)sender {
//    NSLog(@"on click personal");
    [btnHobby setSelected:NO];
    [btnPersonal setSelected:YES];
    [btnFamily setSelected:NO];
    [btnLove setSelected:NO];
    [btnDatingCriteria setSelected:NO];

     [self p_clickBttonQA:btnPersonal];
}

- (IBAction)onClickFamily:(id)sender {
//    NSLog(@"on click family");

    [btnHobby setSelected:NO];
    [btnPersonal setSelected:NO];
    [btnFamily setSelected:YES];
    [btnLove setSelected:NO];
    [btnDatingCriteria setSelected:NO];
    [self p_clickBttonQA:btnFamily];
    
}

- (IBAction)onClickPastQues:(id)sender
{
    AnswerViewController* viewController = [[[AnswerViewController alloc] initWithNibName:@"AnswerViewController" bundle:nil] autorelease];

    viewController.dicMatchInfo = self.dicMatchInfo;
    viewController.bAnswer = NO;
    viewController.bPastQuestions = YES;
    viewController.prevViewController = self;

    [self.navigationController pushViewController:viewController animated:NO];
}

- (IBAction)onClickViewMore:(id)sender
{
    NSInteger   nOldCount = _nShowCount;
    
    _nShowCount = MIN( _nShowCount+10, [_arrayQuestionsList count] );
    
#if 1
    NSMutableArray  *arrayIndexes = [NSMutableArray arrayWithCapacity:0];

    for( NSInteger i = nOldCount; i < _nShowCount; i++ )
    {
        [arrayIndexes addObject:[NSIndexPath indexPathForItem:i inSection:0]];
    }
    
    [tblView insertRowsAtIndexPaths:arrayIndexes withRowAnimation:UITableViewRowAnimationFade];
#else
    [tblView reloadData];
#endif
    [self refreshContentSize];
}

- (void)dealloc
{
    [lblAskTitle release];
    [btnQuestions release];
    [btnPastQues release];
    [tblView release];
    [self.questionlistStr release];
    [self.arrayQuestions release];
    [_arrayQuestionsList release];
    [dicSelectedQuestionInfo release];
    [kindsArrItems release];
    
    [btnHobby release];
    [btnPersonal release];
    [btnFamily release];
    [btnLove release];
    [btnDatingCriteria release];
    [super dealloc];
}
@end

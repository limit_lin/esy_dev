
#import <UIKit/UIKit.h>

@interface QAViewController : GAITrackedViewController<UITableViewDelegate,UITableViewDataSource>
{
    IBOutlet UILabel        *lblAskTitle;
    
    IBOutlet UIButton       *btnQuestions;
    IBOutlet UIButton       *btnPastQues;
    
    IBOutlet UIScrollView   *scrView;
    IBOutlet UITableView    *tblView;
    IBOutlet UIView         *viewFooter;
//append 0811
    IBOutlet UIButton *btnHobby;
    IBOutlet UIButton *btnPersonal;
    IBOutlet UIButton *btnFamily;
    IBOutlet UIButton *btnLove;
    IBOutlet UIButton *btnDatingCriteria;
    NSInteger               tag;
//end
        
    NSMutableArray          *_arrayQuestionsList;
    NSInteger               _nShowCount;
    
}
@property int selectedNum;
@property (nonatomic, retain) NSString                  *questionlistStr;
@property (nonatomic, retain) NSMutableArray            *arrayQuestions;
@property (nonatomic, assign) NSMutableDictionary       *dicMatchInfo;

@property (nonatomic, assign) UIViewController          *mprofileViewController;
//append 0909
@property(nonatomic, strong)NSMutableArray              *kindsArrItems;
@property(nonatomic, strong)NSMutableDictionary         *dicSelectedQuestionInfo;
//end
- (void)refreshQuestionsList;

- (IBAction)onClickBack:(id)sender;
- (IBAction)onClickNext:(id)sender;

- (IBAction)onClickQuestions:(id)sender;
- (IBAction)onClickPastQues:(id)sender;

- (IBAction)onClickViewMore:(id)sender;

//append button action
- (IBAction)onClickHobby:(id)sender;
- (IBAction)onClickPersonal:(id)sender;
- (IBAction)onClickFamily:(id)sender;
- (IBAction)onClickLove:(id)sender;
- (IBAction)onClickDatingCriteria:(id)sender;
//ending

- (void)addQuestionToFavourite:(NSInteger)rowIndex;
- (void)removeQuestionFromFavourite:(NSInteger)rowIndex;

- (void)removeQuestionFromFavouriteProc:(NSNumber*)index;
- (void)addQuestionToFavouriteProc:(NSNumber*)index;
@end


#import "PastQuestionTableCell.h"
#import "Global.h"

@implementation PastQuestionTableCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

+ (float)calcHeightFromInfo:(NSDictionary*)dicQuestionInfo
{
    NSArray     *arrayInfos = [dicQuestionInfo objectForKey:@"anon"];
    NSString    *strQuestion = [arrayInfos objectAtIndex:0];
    UIFont      *font = [UIFont fontWithName:@"HelveticaNeueLTStd-MdCn" size:14.0f];

    CGSize  constrainedSize = CGSizeMake(280, 500);
    CGSize  size = [strQuestion sizeWithFont:font constrainedToSize:constrainedSize];
    
//    NSDictionary *attribute = @{NSFontAttributeName:font};
//    CGSize  size= [strQuestion boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:attribute context:nil].size;
    
    return size.height + 100;
}

- (void)setQuestionInfo:(NSDictionary*)dicQuestionInfo matchName:(NSString*)strMatchName forIndex:(NSInteger)rowIndex
{
    lblNumber.text = [NSString stringWithFormat:@"%d.", (int)(rowIndex+1) ];
    [lblNumber sizeToFit];
    
    NSArray     *arrayInfos = [dicQuestionInfo objectForKey:@"anon"];
    NSString    *strQuestion = [arrayInfos objectAtIndex:0];
    
    lblViewQuestion.text = strQuestion;
    lblViewQuestion.textColor = TITLE_TEXT_COLLOR;
    
    lblMyAnswer.text = [NSString stringWithFormat:@"You : %@", [arrayInfos objectAtIndex:2]];
    lblMatchAnswer.text = [NSString stringWithFormat:@"%@ : %@", strMatchName, [arrayInfos objectAtIndex:1]];
    
    [lblViewQuestion sizeToFit];
    
    CGRect  frame = lblViewQuestion.frame;
    
    CGRect rect1 = viewAnswer.frame;
    rect1.origin.y = frame.origin.y + frame.size.height;
    viewAnswer.frame = rect1;
    
    frame = lblNumber.frame;
    frame.origin.x = 27 - frame.size.width;
    lblNumber.frame = frame;
}

- (void)dealloc
{
    [lblNumber release];
    [lblViewQuestion release];
    [lblMyAnswer release];
    [lblMatchAnswer release];
    [viewAnswer release];
    
    [super dealloc];
}

@end

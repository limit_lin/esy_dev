
#import <UIKit/UIKit.h>
#import "ProfileBaseViewController.h"

#import "MyMatchesProposedCell.h"
#import "MatchSettingViewController.h"


@interface MatchProfileViewController : ProfileBaseViewController<UIAlertViewDelegate>
{
    IBOutlet UILabel        *userNamelabel;
    
    IBOutlet UIButton       *btnTop1;
    IBOutlet UIButton       *btnTop2;
    IBOutlet UIButton       *btnTop3;
    
    // Basic
    IBOutlet UILabel        *lblName;
    IBOutlet UILabel        *lblName1;
    IBOutlet UILabel        *lblNation;
    IBOutlet UILabel        *lblPercent;
    
    IBOutlet UIImageView    *imgViewMainPhoto;
    IBOutlet UIImageView    *imgIconLove;
    
    IBOutlet UIImageView    *imgViewUserPhoto;
    IBOutlet UIImageView    *imgViewMatchPhoto;

    IBOutlet UILabel        *lblMatchDisc;
    IBOutlet UILabel        *lblUserDisc;
    IBOutlet UIButton       *btnAskPhoto;

    //verify
    NSString *edu_verify;
    NSString *id_verify;
    NSString *income_verify;
    NSString *work_verify;
    NSString *marriage_verify;
    
    NSString *edu_status;
    NSString *id_status;
    NSString *income_status;
    NSString *work_status;
}
@property (nonatomic, assign) id delegateListViewCtrl;
@property (nonatomic, strong) NSMutableDictionary* dicMatchInfo;
@property (nonatomic) BOOL  m_bTopButton;
@property (nonatomic) BOOL  m_bDismissSelf;

@property (nonatomic,retain)MyMatchesProposedCell *myMatchesProposedCell;
@property (nonatomic, strong)NSString *toName;

@property BOOL isAlreadyDecline;
@property BOOL isPhotoAllowed;
@property BOOL isFit;
+ (MatchProfileViewController*)createWithMatchInfo:(NSMutableDictionary*)dicMatchInfo;

- (IBAction)didClickBackBtn:(id)sender;
- (IBAction)didClickQA:(id)sender;
- (IBAction)didClickAskPhoto:(id)sender;
- (IBAction)didClickPersonality:(id)sender;

- (IBAction)didClickDecline:(id)sender;
- (IBAction)didClickSkip:(id)sender;
- (IBAction)didClickApprove:(id)sender;

- (IBAction)didClickMoreBtn:(id)sender;

@end

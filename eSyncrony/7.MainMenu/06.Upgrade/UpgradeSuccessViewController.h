
#import <UIKit/UIKit.h>

@interface UpgradeSuccessViewController : GAITrackedViewController
{
    IBOutlet UILabel    *lblName;
    IBOutlet UILabel    *lblPurchaseTitle;
    IBOutlet UILabel    *lblPurchaseValue;
}

@property (nonatomic, assign) NSUInteger nPurchaseIndex;
@property (nonatomic, assign) NSString *packagePrice;
- (IBAction)onClickOk:(id)sender;

@property(nonatomic,retain) IBOutlet UIView *successView;
@end

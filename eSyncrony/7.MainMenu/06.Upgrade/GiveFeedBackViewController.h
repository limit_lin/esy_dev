//
//  GiveFeedBackViewController.h
//  eSyncrony
//
//  Created by iosdeveloper on 14-6-11.
//  Copyright (c) 2014年 WonMH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataSelectViewController.h"
@interface GiveFeedBackViewController : GAITrackedViewController<DataSelectViewDelegate,UITextViewDelegate>
{
    float   offset;
    
}

@property BOOL nLove;
@property int selectedIdx;
@property (nonatomic, strong)   NSArray* chooseArray;
@property (retain, nonatomic) IBOutlet UIView *contentView;
@property (retain, nonatomic) IBOutlet UILabel *lblChoose;
@property (retain, nonatomic) IBOutlet UITextView *textView;
@property (retain, nonatomic) IBOutlet UIButton *btnSelect;

- (IBAction)didChooseOne:(id)sender;

- (IBAction)didSend:(id)sender;


@end

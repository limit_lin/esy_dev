//
//  GiveFeedBackViewController.m
//  eSyncrony
//
//  Created by iosdeveloper on 14-6-11.
//  Copyright (c) 2014年 WonMH. All rights reserved.
//

#import "GiveFeedBackViewController.h"
#import "UtilComm.h"
#import "DSActivityView.h"
#import "eSyncronyAppDelegate.h"
#import "mainMenuViewController.h"
@interface GiveFeedBackViewController ()

@end

@implementation GiveFeedBackViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.screenName = @"GiveFeedBackView——Problems";
    self.chooseArray = [[NSArray alloc]initWithObjects:NSLocalizedString(@"App Features",@"GiveFeedBackView"),NSLocalizedString(@"App Bugs",@"GiveFeedBackView"), NSLocalizedString(@"Our Services",@"GiveFeedBackView"),NSLocalizedString(@"Date Packages",@"GiveFeedBackView"),NSLocalizedString(@"My Matches",@"GiveFeedBackView"),NSLocalizedString(@"My Account",@"GiveFeedBackView"),NSLocalizedString(@"Report User",@"GiveFeedBackView"),nil];
    
}
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    self.btnSelect.hidden = YES;
    CGRect rect = [textView convertRect:textView.bounds toView:self.view];
//    CGRect rect = textView.bounds;
    offset = self.view.frame.size.height - (rect.size.height + 240);
    offset = offset - rect.origin.y;
    
    if( offset > 0 )
        return;
    
    rect = self.view.frame;
    rect.origin.y = rect.origin.y + offset;
    
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:0.30];
    
    self.view.frame = rect;
    
    [UIView commitAnimations];
    
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        if( offset < 0 ){
            CGRect rect = self.view.frame;
            rect.origin.y = rect.origin.y - offset;
            
            [UIView beginAnimations:@"ResizeForContentView" context:nil];
            [UIView setAnimationDuration:0.30];
            self.view.frame = rect;
            [UIView commitAnimations];
        }
        [textView resignFirstResponder];
        self.btnSelect.hidden = NO;
        return NO;
    }
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
//    if( offset > 0 ){
//        CGRect rect = self.contentView.frame;
//        rect.origin.y = rect.origin.y - offset;
//        self.contentView.frame = rect;
//    }
    return YES;
}


- (void)DataSelectView:(DataSelectViewController *)dataSelectView didSelectItem:(int)itemIndex withTag:(int)tag
{
    self.selectedIdx = itemIndex;

    self.lblChoose.text = [self.chooseArray objectAtIndex:self.selectedIdx];
    [dataSelectView release];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)didChooseOne:(id)sender {
    [self.view endEditing:YES];
    
    DataSelectViewController*   selectView = [DataSelectViewController createWithDataList:self.chooseArray selectIndex:self.selectedIdx withTag:0];
    
    selectView.delegate = self;
    [self.contentView addSubview:selectView.view];
    selectView.view.frame = self.contentView.bounds;
}

-(void)sendFeedback{
    //http://www.cocoachina.com/bbs/read.php?tid=124996&page=1#727678
    //float platfrom=[[[UIDevice currentDevice]systemVersion]floatValue];//系统号
    
    //http://blog.sina.com.cn/s/blog_5df876f30100xlwo.html
        NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
        CFShow(infoDictionary);
        NSString *app_Version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
       // NSLog(@"version==%@",app_Version);
    //手机系统版本
    NSString* phoneVersion = [[UIDevice currentDevice] systemVersion];
    
    if (phoneVersion.length == 0) {
        //     if (phoneVersion == nil) {
        phoneVersion = @"Unknown";
    }
    //NSLog(@"手机系统版本: %@", phoneVersion);
    NSString* phonePlatform=[NSString stringWithFormat:@"IOS %@/eSync %@",phoneVersion,app_Version];
    NSLog(@"phonePlatform==%@",phonePlatform);

    //手机型号
    NSString* phoneModel = [[UIDevice currentDevice] model];
    if (phoneModel.length == 0) {
//    if (phoneModel == nil) {
        phoneModel=@"Unknown";
    }
    //NSLog(@"手机型号: %@",phoneModel );
    
    int reason_id = self.selectedIdx+1;
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:[NSNumber numberWithInt:reason_id] forKey:@"reason_id"];
    [params setObject:self.textView.text forKey:@"fdback_details"];
    [params setObject:@"updateFeedbackApp" forKey:@"cmd"];
  
    [params setObject:phonePlatform forKey:@"platform"];
    [params setObject:phoneModel forKey:@"model"];
    
    NSDictionary*    result = [UtilComm newGiveFeedback:params];
    [DSBezelActivityView removeViewAnimated:NO];
    if( result == nil )
    {
        return;
    }
    
    id response = [result objectForKey:@"response"];
//    [result release];
    if( [response isKindOfClass:[NSString class]] ){
        if( [response isEqualToString:@"OK"] ){
            
            [ErrorProc alertMessage:NSLocalizedString(@"Thank you for your valuable feedback!",@"GiveFeedBackView") withTitle:NSLocalizedString(@"Feedback  Submitted",@"GiveFeedBackView")];
            [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl goMainMenu];//fore
//            [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl procMyMatchProposed];//android jump into the interface
            if (self.nLove) {
                NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
                [defaults setObject:[NSNumber numberWithBool:YES] forKey:@"NoThanks"];
            }
            [self dismissViewControllerAnimated:YES completion:nil];
            
        }else if( [response hasPrefix:@"ERROR"] ){
            [ErrorProc alertMessage:NSLocalizedString(@"Update Failed",@"GiveFeedBackView") withTitle:NSLocalizedString(@"Error",)];
        }
    }else {
        [ErrorProc alertMessage:NSLocalizedString(@"Update Failed",@"GiveFeedBackView") withTitle:NSLocalizedString(@"Error",)];
    }
    
}
- (IBAction)didSend:(id)sender {
    if ([self.lblChoose.text isEqualToString:NSLocalizedString( @"Choose One",@"GiveFeedBackView")]) {
        [ErrorProc alertMessage:NSLocalizedString(@"Please choose the type of problem you want feedback, so that we can better improve.",@"GiveFeedBackView") withTitle:NSLocalizedString(@"Info",)];
        return;
    }
    if (self.textView.text.length==0) {
        [ErrorProc alertMessage:NSLocalizedString(@"You haven't fill in the content of the feedback.",@"GiveFeedBackView") withTitle:NSLocalizedString(@"Info",)];
        return;
    }
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Sending...",)];
    [self performSelector:@selector(sendFeedback) withObject:nil afterDelay:0.01];
    
}


- (void)dealloc {
    
//    [self.chooseArray release];
    [_chooseArray release];
    [_textView release];
    [_lblChoose release];
    [_contentView release];
    [_btnSelect release];
    [super dealloc];
}
@end

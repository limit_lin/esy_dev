//
//  PackageListCell.h
//  eSyncrony
//
//  Created by iosdeveloper on 14-8-4.
//  Copyright (c) 2014年 WonMH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PackageListCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UILabel *lblBenefit;
@property (retain, nonatomic) IBOutlet UILabel *lblPrice;

@end


#import "UpgradeSuccessViewController.h"
#import "DSActivityView.h"
#import "Global.h"
#import "UtilComm.h"

#import "eSyncronyAppDelegate.h"
#import "mainMenuViewController.h"
#import "eSyncronyViewController.h"

@interface UpgradeSuccessViewController ()
{
    IBOutlet UILabel *pLabDear;
}
@end

extern float _pay_amount_list[];

@implementation UpgradeSuccessViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"UpgradeSuccessView";
    //NSLog(@"daolezheli");
    CGSize pLabDearSize = [pLabDear.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:pLabDear.font,NSFontAttributeName, nil]];
    pLabDear.frame = CGRectMake(10, 12, pLabDearSize.width, 20);
    lblName.frame = CGRectMake(10 + pLabDearSize.width +1 , 12, 153, 20);
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if( self.nPurchaseIndex == 0 )
        lblPurchaseTitle.text = NSLocalizedString(@"12 Months Unlimited Date Package",@"UpgradeSuccessView");
    else if( self.nPurchaseIndex == 1 )
        lblPurchaseTitle.text = NSLocalizedString(@"6 Months Unlimited Date Package",@"UpgradeSuccessView");
    else if( self.nPurchaseIndex == 2 )
        lblPurchaseTitle.text = NSLocalizedString(@"4 Months Unlimited Date Package",@"UpgradeSuccessView");
    else
        lblPurchaseTitle.text = NSLocalizedString(@"2 Months Unlimited Date Package",@"UpgradeSuccessView");
    
    //0: Singapore, 1: Malaysia, 2: HONG KONG
    int     nCountryID = [eSyncronyAppDelegate sharedInstance].countryId;

    
    NSString    *currency = nil;
    
    if( nCountryID == 0 )
        currency = @"SGD";
    else if( nCountryID == 1 )
        currency = @"MYR";
    else if( nCountryID == 7)
        currency = @"USD";
    else if (nCountryID == 202)
        currency = @"THB";
    else
        currency = @"HKD";
    
    lblPurchaseValue.text = [NSString stringWithFormat:NSLocalizedString(@"Purchased Price: %@ %@",@"UpgradeSuccessView"), currency, self.packagePrice];
    lblName.text = [NSString stringWithFormat:@"%@,", [eSyncronyAppDelegate sharedInstance].strName];
}

- (IBAction)onClickOk:(id)sender
{
    if ([[eSyncronyAppDelegate sharedInstance].paying isEqualToString:@"no"]) {
        [eSyncronyAppDelegate sharedInstance].paying = @"yes";
        [[eSyncronyAppDelegate sharedInstance] saveLoginInfo];
    }

    if ([[eSyncronyAppDelegate sharedInstance].isVip isEqualToString:@"true"] && [[eSyncronyAppDelegate sharedInstance].checkVipStroy isEqualToString:@"false"]) {//avoid twice 
        [[eSyncronyAppDelegate sharedInstance].viewController enterVIPStoryStepsView:NO];
    }else
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl procUpgrade];
}

- (void)dealloc {
    [pLabDear release];
    [super dealloc];
}
@end

//
//  PackageListCell.m
//  eSyncrony
//
//  Created by iosdeveloper on 14-8-4.
//  Copyright (c) 2014年 WonMH. All rights reserved.
//

#import "PackageListCell.h"

@implementation PackageListCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_lblBenefit release];
    [_lblPrice release];
    [super dealloc];
}
@end


#import <UIKit/UIKit.h>
#import "PayPalMobile.h"
#import "ZZFlipsideViewController.h"
#import "UpgradeSuccessViewController.h"
@class  mainMenuViewController;
#define SandBox @"https://www.sandbox.paypal.com"
#define liveSite @"https://www.paypal.com"

@interface UpgradeViewController : GAITrackedViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIAlertViewDelegate,PayPalPaymentDelegate, PayPalFuturePaymentDelegate, PayPalProfileSharingDelegate, ZZFlipsideViewControllerDelegate, UIPopoverControllerDelegate>//,UIWebViewDelegate
{
    IBOutlet UIScrollView   *scrlView;
    IBOutlet UIView         *datePlansView;
    IBOutlet UIView         *benefitsView;
    IBOutlet UIView         *vipDatePlansView;
    
    
    IBOutlet UIView *VipLcView;
    IBOutlet UIView *VipEbookView;
    
    IBOutlet UIButton       *datePlansBtn;
    IBOutlet UIButton       *benefitsBtn;

    IBOutlet UILabel        *datePlansBtnLabel;
    IBOutlet UILabel        *benefitsBtnLabel;
    
    IBOutlet UILabel        *lblTotalMoney1;
    IBOutlet UILabel        *lblTotalMoney2;
    IBOutlet UILabel        *lblTotalMoney3;
    IBOutlet UILabel        *lblTotalMoney4;
 //vip package payment
    IBOutlet UILabel        *lblTotalMoney5;
    IBOutlet UILabel        *lblTotalMoney6;
    IBOutlet UILabel        *lblTotalMoney7;
    IBOutlet UILabel        *lblTotalVIPMoney;
    IBOutlet UIButton       *btnHyperLink;
    
    IBOutlet UILabel        *lblItemName1;
    IBOutlet UILabel        *lblItemName2;
    IBOutlet UILabel        *lblItemName3;
    
    
    IBOutlet UILabel        *lblMonthMoney1;
    IBOutlet UILabel        *lblMonthMoney2;
    IBOutlet UILabel        *lblMonthMoney3;
    IBOutlet UILabel        *lblMonthMoney4;//on join 4 crash,but interface hideen

    IBOutlet UILabel        *lblPhoneAddr;

    BOOL                isSelecyedDatePlans;
    
    NSUInteger                 _justPayIndex;
    
    NSString            *_packagePrice;
    NSString            *_justPayKey;
    NSString            *valid;
    
    NSString            *address;
    
    BOOL isPurchased;
    
    int     nCountryID;
    NSString *Currency;
    
    NSMutableString *os0;
    
}

@property (retain, nonatomic) NSMutableArray *arrTitle;
@property (retain, nonatomic) NSMutableArray *arrBenefits;
@property (retain, nonatomic) NSArray * arrProductIdentifiers;
@property (retain, nonatomic) NSArray * arrItemNames;
@property (assign, nonatomic) UIButton *buyButton;

@property (retain, nonatomic) IBOutlet UIView *viewList;
@property (retain, nonatomic) IBOutlet UIImageView *imageTitle;
@property (retain, nonatomic) IBOutlet UIImageView *imageStory;
@property (retain, nonatomic) IBOutlet UIView *balckView;
@property (retain, nonatomic) IBOutlet UIView *readMoreView;
@property (retain, nonatomic) IBOutlet UITableView *benefitsListTableView;
@property (retain, nonatomic) IBOutlet UILabel *lblMonthPlan;
@property (retain, nonatomic) IBOutlet UILabel *lblMonth1;
@property (retain, nonatomic) IBOutlet UILabel *lblMonth2;
@property (retain, nonatomic) IBOutlet UILabel *lblMonth3;
@property (retain, nonatomic) IBOutlet UILabel *lblMonth4;
@property (retain, nonatomic) IBOutlet UIButton *btnJoin1;
@property (retain, nonatomic) IBOutlet UIButton *btnJoin2;
@property (retain, nonatomic) IBOutlet UIButton *btnJoin3;
@property (retain, nonatomic) IBOutlet UIButton *btnJoin4;
@property (retain, nonatomic) IBOutlet UITextField *txtPromoCode;

//paypal start
@property(nonatomic, strong, readwrite) NSString *environment;
@property(nonatomic, assign, readwrite) BOOL acceptCreditCards;
@property(nonatomic, strong, readwrite) NSString *resultText;

@property(nonatomic,retain)UpgradeSuccessViewController *upgradeSuccessViewController;
@property(nonatomic,strong)mainMenuViewController *mainMenuViewController;
//paypal end

@property (retain, nonatomic) IBOutlet UIView *hidenView;

- (IBAction)didClickDatePlans:(id)sender;
- (IBAction)didClickBenefits:(id)sender;
- (IBAction)didOpenReadmore:(id)sender;
- (IBAction)didCloseReadmore:(id)sender;

- (IBAction)onClickJoin12:(id)sender;
- (IBAction)onClickJoin6:(id)sender;
- (IBAction)onClickJoin4:(id)sender;
- (IBAction)onClickJoin2:(id)sender;

- (IBAction)onClickPhone:(id)sender;
- (IBAction)onClickEmail:(id)sender;
- (IBAction)onClickHelpTopics:(id)sender;

- (IBAction)didClickSeeMoreCouples:(id)sender;

//append referral
- (IBAction)ShareEventClicked:(id)sender;

//vip package payment

- (IBAction)didClickCancelled:(id)sender;
- (IBAction)didClickHyperlink:(id)sender;


//- (void)requestProducts;
//- (id)initWithProductIdentifiers:(NSSet *)productIdentifiers;
@end

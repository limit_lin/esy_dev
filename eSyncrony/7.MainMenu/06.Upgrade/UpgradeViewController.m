#import <Foundation/NSObject.h>
#import <CommonCrypto/CommonDigest.h>//生成随机的字符串

#import "UpgradeViewController.h"
#import "mainMenuViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "DSActivityView.h"
#import "Global.h"
#import "UtilComm.h"
#import "eSyncronyAppDelegate.h"

#import "ourSuccessStoriesViewController.h"
#import <CoreText/CoreText.h>
#import "PhoneUtil.h"
#import "PackageListCell.h"

#import "UpgradeSuccessViewController.h"

#import <ShareSDK/ShareSDK.h>

//paypal refrence
//https://www.paypal-apps.com/user/my-account/applications
//https://developer.paypal.com/docs/
//https://github.com/paypal/PayPal-iOS-SDK.git
//http://www.docin.com/p-521557396.html
//29
//http://www.docin.com/p-520989779.html


// Set the environment:
// - For live charges, use PayPalEnvironmentProduction (default).
// - To use the PayPal sandbox, use PayPalEnvironmentSandbox.
// - For testing, use PayPalEnvironmentNoNetwork.
//#define kPayPalEnvironment PayPalEnvironmentProduction//move to UtilComm

float _pay_amount_list[] = { 899, 699, 550, 299, 899, 699, 550, 299, 5200, 4200, 3600, 1800 };
//float _pay_amount_list[] = { 89, 69, 55, 29, 89, 69, 55, 29, 520, 420, 360, 180 };
@interface UpgradeViewController (){
    
    IBOutlet UIButton *VIPpackageBtn;
    IBOutlet UIButton *VIPlunchBtn;
    IBOutlet UIButton *VIPebookBtn;
    
    IBOutlet UILabel *plabel_1;
    IBOutlet UILabel *plabel_2;
    IBOutlet UILabel *plabel_3;
    IBOutlet UILabel *plabel_4;
    
    
    IBOutlet UILabel *plabel_5;
    IBOutlet UILabel *plabel_6;
    IBOutlet UILabel *plabel_7;
    
    IBOutlet UILabel *plabel_8;
    IBOutlet UILabel *plabel_9;
    IBOutlet UILabel *plabel_10;
    
    IBOutlet UILabel *plabel_11;
    IBOutlet UILabel *plabel_12;
    
    IBOutlet UILabel *plabel_13;
    
    IBOutlet UILabel *plabel_14;
    IBOutlet UIButton *pcontanctEmail;
    IBOutlet UILabel *plabel_16;
    IBOutlet UILabel *plabel_17;
    
    IBOutlet UILabel *plabel_18;
    IBOutlet UIButton *phelpTocips;
    IBOutlet UILabel *plabel_20;
    
    IBOutlet UILabel *pBenLabel;
    
    BOOL proceedPayment;
    NSMutableDictionary *p_params;
    int buttonTag;
    
    NSInteger numPrice1;
    NSInteger numPrice2;
    NSInteger numPrice3;
    NSInteger numPrice4;
    NSInteger numPrice5;
    NSInteger numPrice6;
    NSInteger numPrice7;
    NSInteger numToatlPrice;
    IBOutlet UIButton *proceedBtn;
}

@property(nonatomic, strong, readwrite) IBOutlet UIButton *payNowButton;
@property(nonatomic, strong, readwrite) IBOutlet UIButton *payFutureButton;

@property (nonatomic,strong,readwrite)PayPalConfiguration *payPalConfig;
- (IBAction)checkVIPpackageClick:(UIButton *)sender;
- (IBAction)checkVIPlunchClick:(UIButton *)sender;
- (IBAction)checkVIPebookClick:(UIButton *)sender;

- (IBAction)onClickProceedPayment:(id)sender;

@end

@implementation UpgradeViewController
- (BOOL)acceptCreditCards {
    return self.payPalConfig.acceptCreditCards;
}
//- (BOOL) shouldAutorotateToInterfaceOrientation:
//(UIInterfaceOrientation)toInterfaceOrientation {
//    return (toInterfaceOrientation == UIInterfaceOrientationLandscapeRight);
//}
//- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
//    return UIInterfaceOrientationMaskLandscapeRight;
//}
-(void)p_layout
{
    //why choose esync
    CGSize plabel1Size = [plabel_1.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:plabel_1.font,NSFontAttributeName, nil]];
    CGSize plabel2Size = [plabel_2.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:plabel_2.font,NSFontAttributeName, nil]];
    CGSize plabel3Size = [plabel_3.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:plabel_3.font,NSFontAttributeName, nil]];
    CGSize plabel4Size = [plabel_4.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:plabel_4.font,NSFontAttributeName, nil]];
    plabel_1.frame = CGRectMake(22, 71, plabel1Size.width, 22);
    plabel_2.frame = CGRectMake(plabel1Size.width+23, 71, plabel2Size.width, 22);
    plabel_3.frame = CGRectMake(plabel1Size.width+plabel2Size.width+25, 71, plabel3Size.width,22);
    plabel_4.frame = CGRectMake(plabel1Size.width+plabel2Size.width+plabel3Size.width+27, 71, plabel4Size.width, 22);
    
    
    CGSize plabel5Size = [plabel_5.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:plabel_5.font,NSFontAttributeName, nil]];
    CGSize plabel6Size = [plabel_6.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:plabel_6.font,NSFontAttributeName, nil]];
    CGSize plabel7Size = [plabel_7.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:plabel_7.font,NSFontAttributeName, nil]];
    plabel_5.frame = CGRectMake(22, 93, plabel5Size.width, 22);
    plabel_6.frame = CGRectMake(plabel5Size.width+23,93, plabel6Size.width, 22);
    plabel_7.frame = CGRectMake(plabel5Size.width+plabel6Size.width+25, 93, plabel7Size.width,22);
 
    CGSize plabel8Size = [plabel_8.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:plabel_8.font,NSFontAttributeName, nil]];
    CGSize plabel9Size = [plabel_9.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:plabel_9.font,NSFontAttributeName, nil]];
    CGSize plabel10Size = [plabel_10.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:plabel_10.font,NSFontAttributeName, nil]];
    plabel_8.frame = CGRectMake(22, 115, plabel8Size.width, 22);
    plabel_9.frame = CGRectMake(plabel8Size.width+23, 115, plabel9Size.width, 22);
    plabel_10.frame = CGRectMake(plabel8Size.width+plabel9Size.width+25,115, plabel10Size.width,22);
    
    CGSize plabel11Size = [plabel_11.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:plabel_11.font,NSFontAttributeName, nil]];
    CGSize plabel12Size = [plabel_12.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:plabel_12.font,NSFontAttributeName, nil]];
    plabel_11.frame = CGRectMake(22, 137, plabel11Size.width, 22);
    plabel_12.frame = CGRectMake(plabel11Size.width+23,137, plabel12Size.width, 22);
    
   //contanct
    
    CGSize plabel13Size = [plabel_13.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:plabel_13.font,NSFontAttributeName, nil]];
    plabel_13.frame = CGRectMake(89, 72, plabel13Size.width, 20);
    
    CGSize plabel14Size = [plabel_14.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:plabel_14.font,NSFontAttributeName, nil]];
//    CGSize plabel16Size = [plabel_16.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:plabel_16.font,NSFontAttributeName, nil]];
    CGSize pcontanctEmailSize = [pcontanctEmail.titleLabel.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:pcontanctEmail.titleLabel.font,NSFontAttributeName, nil]];
    CGSize plabel17Size = [plabel_17.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:plabel_17.font,NSFontAttributeName, nil]];
    
    plabel_14.frame = CGRectMake(89, 95, plabel14Size.width, 20);
    pcontanctEmail.frame = CGRectMake(plabel14Size.width+90, 94, pcontanctEmailSize.width,20);
    plabel_16.frame = CGRectMake(plabel14Size.width+90, 94+20, pcontanctEmailSize.width, 1);
    plabel_17.frame = CGRectMake(plabel14Size.width+pcontanctEmailSize.width+91,95, plabel17Size.width, 20);

    CGSize plabel18Size = [plabel_18.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:plabel_18.font,NSFontAttributeName, nil]];
//    CGSize plabel20Size = [plabel_20.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:plabel_20.font,NSFontAttributeName, nil]];
    CGSize phelpTocipsSize = [phelpTocips.titleLabel.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:phelpTocips.titleLabel.font,NSFontAttributeName, nil]];
    
    plabel_18.frame = CGRectMake(89, 117, plabel18Size.width, 20);
    phelpTocips.frame = CGRectMake(plabel18Size.width+90,116, phelpTocipsSize.width, 20);
    plabel_20.frame = CGRectMake(plabel18Size.width+90,116+20,phelpTocipsSize.width,1);
    
    
    CGSize pBenLabelSize = [pBenLabel.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:pBenLabel.font,NSFontAttributeName, nil]];
    pBenLabel.frame = CGRectMake(5, 9, pBenLabelSize.width, 21);
    
    return;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"UpgradeView";
    isSelecyedDatePlans = YES;

    [self p_layout];

    [scrlView addSubview:datePlansView];
    scrlView.contentSize = datePlansView.frame.size;
    self.viewList.layer.cornerRadius = 10.0;
    self.viewList.layer.shadowOffset = CGSizeMake(3,3);
    self.viewList.layer.shadowColor = [UIColor blackColor].CGColor;
    self.viewList.layer.shadowOpacity = 0.6;
    
    self.imageTitle.layer.shadowOffset = CGSizeMake(0,2);
    self.imageTitle.layer.shadowColor = [UIColor blackColor].CGColor;
    self.imageTitle.layer.shadowOpacity = 0.6;
    
    self.imageStory.layer.shadowOffset = CGSizeMake(0, 3);
    self.imageStory.layer.shadowColor = [UIColor blackColor].CGColor;
    self.imageStory.layer.shadowOpacity = 0.8;
    
    isPurchased = NO; // default.
    
    
    //0: Singapore, 1: Malaysia, 2: HONG KONG, 7:Indonesia
    nCountryID = [eSyncronyAppDelegate sharedInstance].countryId;
    
    Currency=[[NSString alloc]init];

    os0 = [[NSMutableString alloc]init];
    
    if( nCountryID == 0 )
        Currency=@"SGD";
    else if( nCountryID == 1 )
        Currency=@"MYR";
    else if( nCountryID == 7)
        Currency = @"USD";
    else if(nCountryID == 202)
        Currency = @"THB";
    else
        Currency=@"HKD";

    proceedBtn.layer.borderWidth = 1.5;
    proceedBtn.layer.cornerRadius = 4.5;
    proceedBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    // Set up payPalConfig
    _payPalConfig = [[PayPalConfiguration alloc] init];
//#if HAS_CARDIO
    _payPalConfig.acceptCreditCards = YES;
//#else
//    _payPalConfig.acceptCreditCards = NO;
//#endif
    _payPalConfig.merchantName = @"Awesome Shirts, Inc.";
    _payPalConfig.merchantPrivacyPolicyURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/privacy-full"];
    _payPalConfig.merchantUserAgreementURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/useragreement-full"];
    _payPalConfig.languageOrLocale = [NSLocale preferredLanguages][0];
    
    _payPalConfig.payPalShippingAddressOption = PayPalShippingAddressOptionNone;
    
    // use default environment, should be Production in real life
    self.environment = kPayPalEnvironment;
    
  // NSLog(@"PayPal iOS SDK version: %@", [PayPalMobile libraryVersion]);
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    //0: Singapore, 1: Malaysia, 2: HONG KONG 7: Indonesia
    
    if( nCountryID == 0 )
    {
        lblPhoneAddr.text = @"6337 5503";
    }
    else if( nCountryID == 1 )
    {
        lblPhoneAddr.text = @"2283 6686";
    }
    else
    {
        lblPhoneAddr.text = @"2524 1200";
    }
    
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(loadPackageList) withObject:nil afterDelay:0.01];
    
    // Preconnect to PayPal early
    [self setPayPalEnvironment:self.environment];
}

- (void)loadPackageList
{
    proceedPayment = NO;
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", @"\\w+\\d{4}$"];
    if ([regextestmobile evaluateWithObject:self.txtPromoCode.text]) {
        [params setObject:self.txtPromoCode.text forKey:@"promocode"];

    }else{
        [params setObject:@"" forKey:@"promocode"];
    }
 
    NSDictionary* result = [UtilComm packagePrice:params];
//    NSLog(@"打印打印打印检查结果＝＝＝＝%@",result);
    
    if( result == nil )
    {
        return;
    }
    
    if ([[result objectForKey:@"response"] isEqual:@""]) {
        
        [DSBezelActivityView removeView];
        _hidenView.hidden = NO;
        [datePlansView removeFromSuperview];
        [scrlView addSubview:_hidenView];
        scrlView.contentSize = _hidenView.frame.size;
        //[ErrorProc alertMessage:NSLocalizedString(@"You can contact to eSynchrony,thank you!",) withTitle:NSLocalizedString(@"Unkown error",@"Upgrade")];
        return;
    }
    
    
    NSDictionary *response = [result objectForKey:@"response"];
    NSArray *item = [response objectForKey:@"item"];
    NSLog(@"count = %ld",(unsigned long)item.count);
    
    NSDictionary *price1 = [item objectAtIndex:0];
    NSDictionary *price2 = [item objectAtIndex:1];
    NSDictionary *price3 = [item objectAtIndex:2];
    NSDictionary *price4 = [item objectAtIndex:3];
    
    
//    Currency = [price1 objectForKey:@"currency"];

    lblTotalMoney1.text = [price1 objectForKey:@"Price"];
    lblMonthMoney1.text = [price1 objectForKey:@"perMth"];
//    self.lblMonth1.text = [price1 objectForKey:@"Months"];
    self.lblMonth1.text = NSLocalizedString([price1 objectForKey:@"Months"], @"UpgradeView");
    
    lblTotalMoney2.text = [price2 objectForKey:@"Price"];
    lblMonthMoney2.text = [price2 objectForKey:@"perMth"];
//    self.lblMonth2.text = [price2 objectForKey:@"Months"];
    self.lblMonth2.text = NSLocalizedString([price2 objectForKey:@"Months"], @"UpgradeView");
    
    lblTotalMoney3.text = [price3 objectForKey:@"Price"];
    lblMonthMoney3.text = [price3 objectForKey:@"perMth"];
//    self.lblMonth3.text = [price3 objectForKey:@"Months"];
    self.lblMonth3.text = NSLocalizedString([price3 objectForKey:@"Months"], @"UpgradeView");
    
    lblTotalMoney4.text = [price4 objectForKey:@"Price"];
    lblMonthMoney4.text = [price4 objectForKey:@"perMth"];
//    self.lblMonth4.text = [price4 objectForKey:@"Months"];
    self.lblMonth4.text = NSLocalizedString([price4 objectForKey:@"Months"], @"UpgradeView");
    
    if (item.count == 4) {
        proceedPayment = YES;
    }
    
    //vip package payment
    if (item.count > 4) {//when then user is th or id,the vip package payment will disappear
        
        NSDictionary *price5 = [item objectAtIndex:4];
        NSDictionary *price6 = [item objectAtIndex:5];
        NSDictionary *price7 = [item objectAtIndex:6];
        
        lblTotalMoney5.text = [price5 objectForKey:@"Price"];
        lblItemName1.text  =  NSLocalizedString([price5 objectForKey:@"item_name"],@"UpgradeView");
        
        lblTotalMoney6.text = [price6 objectForKey:@"Price"];
        lblItemName2.text  = NSLocalizedString([price6 objectForKey:@"item_name"],@"UpgradeView");
        btnHyperLink.titleLabel.text = [price6 objectForKey:@"hyperlink"];
        
        lblTotalMoney7.text = [price7 objectForKey:@"Price"];
        lblItemName3.text  =  NSLocalizedString([price7 objectForKey:@"item_name"], @"UpgradeView");
        
        numPrice1 = [[price1 objectForKey:@"numPrice"]integerValue];
        numPrice2 = [[price2 objectForKey:@"numPrice"]integerValue];
        numPrice3 = [[price3 objectForKey:@"numPrice"]integerValue];
        numPrice4 = [[price4 objectForKey:@"numPrice"]integerValue];
        numPrice5 = [[price5 objectForKey:@"numPrice"]integerValue];
        numPrice6 = [[price6 objectForKey:@"numPrice"]integerValue];
        numPrice7 = [[price7 objectForKey:@"numPrice"]integerValue];
    }

    self.arrProductIdentifiers = [[NSArray alloc]initWithObjects:[price1 objectForKey:@"ios_id"],[price2 objectForKey:@"ios_id"],[price3 objectForKey:@"ios_id"],[price4 objectForKey:@"ios_id"], nil];
    self.arrItemNames = [[NSArray alloc]initWithObjects:[price1 objectForKey:@"item_name"],[price2 objectForKey:@"item_name"],[price3 objectForKey:@"item_name"],[price4 objectForKey:@"item_name"], nil];
    
    [DSBezelActivityView removeView];
}
#pragma paypalEnvironment set
- (void)setPayPalEnvironment:(NSString *)environment {
    
    self.environment = environment;
    [PayPalMobile preconnectWithEnvironment:environment];
}

- (IBAction)didClickDatePlans:(id)sender
{
    if( !isSelecyedDatePlans )
    {
        isSelecyedDatePlans = YES;
        
        [datePlansBtn setSelected:YES];
        [benefitsBtn setSelected:NO];
        
        datePlansBtnLabel.textColor = [UIColor whiteColor];
        benefitsBtnLabel.textColor = RGBColor(12, 102, 124);

        
//        for( UIView *v in [scrlView subviews] )
//        {
////            if (v.tag == 2002)
//            if( v.tag != 2001 )
//            {
//                [v removeFromSuperview];
////                break;
//            }else
//                break;
//        }
        
#if 0
        for( UIView* v in self.view.subviews )
        {
            if( [v isKindOfClass:[UIWebView class]])
            {
                [v removeFromSuperview];
                return;
            }
        }
#endif
        [benefitsView removeFromSuperview];
        
        [scrlView addSubview:datePlansView];
        scrlView.contentSize = datePlansView.frame.size;
    }
}

-(void)getPackageBenefitsList{
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:valid forKey:@"valid"];
    if (self.txtPromoCode.text.length==0) {
        [params setObject:@"" forKey:@"promocode"];
    }else{
        NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", @"\\w+\\d{4}$"];
        if (![regextestmobile evaluateWithObject:self.txtPromoCode.text]) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Invalid Code",@"UpgradeView") message:NSLocalizedString(@"Please enter a valid promo code.",@"UpgradeView") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",) otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            [DSBezelActivityView removeView];
            return;
        }
        [params setObject:self.txtPromoCode.text forKey:@"promocode"];
    }
    
    NSDictionary* result = [UtilComm packageBenefitsList:params];//repair
    
   // NSDictionary* result = [UtilComm packagePrice:params];
    
    if( result == nil )
    {
        [DSBezelActivityView removeView];
        return;
    }
    if (![[result objectForKey:@"response"]isKindOfClass:[NSDictionary class]]) {
        return;
    }

//    //append
//    if ([[result objectForKey:@"response"]isKindOfClass:[NSDictionary class]]) {
//        
//        [DSBezelActivityView removeView];
//        NSLog(@"response=%@",result);
//        return;
//    }
    //0421
    NSDictionary *response = [result objectForKey:@"response"];
    NSArray *item = [response objectForKey:@"item"];
    
    self.arrTitle = [NSMutableArray array];
    self.arrBenefits = [NSMutableArray array];
    
//    for (NSDictionary *dic in item) {
//        for (NSArray * benefits in dic.allValues ) {
//            [self.arrTitle addObject:[benefits objectAtIndex:0]];
//            for (NSString *benefit in benefits) {
//                [self.arrBenefits addObject:benefit];
//            }
//        }
//    }
    
    for (NSDictionary *dic in item) {
        for (NSArray * benefits in dic.allValues ) {
//            [self.arrTitle addObject:[benefits objectAtIndex:0]];
            [self.arrTitle addObject:NSLocalizedString([benefits objectAtIndex:0], @"UpgradeView")];
            for (NSString *benefit in benefits) {
//                [self.arrBenefits addObject:benefit];
                [self.arrBenefits addObject:NSLocalizedString(benefit, @"UpgradeView")];
            }
        }
    }
    
    [self.arrTitle removeObjectAtIndex:0];
    self.lblMonthPlan.text = [self.arrBenefits objectAtIndex:0];
    [self.arrBenefits removeObjectAtIndex:0];
    NSLog(@"arrTitle:%@\narrBenefits:%@",self.arrTitle,self.arrBenefits);
    [self.benefitsListTableView reloadData];
    self.balckView.hidden = NO;
    self.readMoreView.hidden = NO;
    [DSBezelActivityView removeView];
    
}

- (IBAction)didOpenReadmore:(id)sender {
    
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    
    UIButton *readmoreBtn =(UIButton *)sender;
    
    switch (readmoreBtn.tag) {
        case 512:{
            valid = @"12";
            break;
        }
        case 506:{
            valid = @"6";
            break;
        }
        case 504:{
            valid = @"4";
            break;
        }
        case 502:{
            valid = @"2";
            break;
        }
        default:
            break;
    }
    
    [self performSelector:@selector(getPackageBenefitsList) withObject:nil afterDelay:0.01];
}

- (IBAction)didCloseReadmore:(id)sender{
    self.balckView.hidden = YES;
    self.readMoreView.hidden = YES;
}

- (IBAction)didClickBenefits:(id)sender
{

    if( isSelecyedDatePlans )
    {
        isSelecyedDatePlans = NO;
        
        [datePlansBtn setSelected:NO];
        [benefitsBtn setSelected:YES];

        datePlansBtnLabel.textColor = RGBColor(12, 102, 124);
        benefitsBtnLabel.textColor = [UIColor whiteColor];
        
//        for( UIView *v in [scrlView subviews] )
//         {
////          if( v.tag == 2001 )
//             if (v.tag != 2002)
//             {
//                [v removeFromSuperview];
////                break;
//             }else
//                 break;
//        }
        
#if 0
        for( UIView* v in self.view.subviews )
        {
            if( [v isKindOfClass:[UIWebView class]])
            {
                [v removeFromSuperview];
                break;
            }
        }
#endif 
        [datePlansView removeFromSuperview];
        
        [scrlView addSubview:benefitsView];
        scrlView.contentSize = benefitsView.frame.size;
    }
}

#pragma mark - UITableViewDelegate,UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    if (self.txtPromoCode.text.length==0) {
        return self.arrBenefits.count-1;
    }else{
        return self.arrBenefits.count-2;
    }
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    PackageListCell *cell;
    NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"PackageListCell" owner:nil options:nil];
    cell = [arr objectAtIndex:0];
    NSString *benefit = [self.arrBenefits objectAtIndex:indexPath.row];
    if ([benefit hasPrefix:@"SGD"]||[benefit hasPrefix:@"RM"]||[benefit hasPrefix:@"HK"]||[benefit hasPrefix:@"MYR"]||[benefit hasPrefix:@"USD"]||[benefit hasPrefix:@"THB"]) {
        cell.lblBenefit.text = NSLocalizedString(@"Subtotal:",@"UpgradeView");
        cell.lblPrice.text = benefit;
    }else if ([benefit isEqualToString:@"Total"]){
        [cell.lblBenefit setFont:[UIFont boldSystemFontOfSize:17]];
        cell.lblBenefit.text = benefit;
        cell.lblPrice.text = [self.arrBenefits objectAtIndex:indexPath.row+1];
    }else if ([self.arrTitle containsObject:benefit]){
        [cell.lblBenefit setFont:[UIFont boldSystemFontOfSize:17]];
        cell.lblBenefit.text = benefit;
        cell.lblPrice.text = @"";
    }else{
        cell.lblBenefit.text = [NSString stringWithFormat:@"- %@", benefit];
        cell.lblPrice.text = @"";
    }
    if (self.txtPromoCode.text.length>0) {
        if (indexPath.row==(self.arrBenefits.count-3)) {
            [cell.lblBenefit setFont:[UIFont boldSystemFontOfSize:17]];
            cell.lblBenefit.text = NSLocalizedString(@"PromoPrice",@"UpgradeView");
            cell.lblPrice.text = [self.arrBenefits objectAtIndex:indexPath.row+2];
        }
    }
    return cell;
}

//-----------------------------------paypal----------------------------------------//

- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController
{
    NSLog(@"PayPal Payment Cancelled");
    self.resultText = nil;
    
     for( UIView *v in [datePlansView subviews] )
     {
         if( v.tag == 2003 )
         {
             [v removeFromSuperview];
             break;
         }
     }

     [DSBezelActivityView removeView];
  
    [self.navigationController setNavigationBarHidden:NO];
   [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl dismissViewControllerAnimated:YES completion:nil];


}
- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController
                 didCompletePayment:(PayPalPayment *)completedPayment
{
    NSLog(@"PayPal Payment Success!");
    self.resultText = [completedPayment description];
    
    
    [self sendCompletedPaymentToServer:completedPayment]; // Payment was processed successfully; send to server for verification and fulfillment
    
    [DSBezelActivityView newActivityViewForView:paymentViewController.view withLabel:NSLocalizedString(@"Loading...",)];
    [self showSuccess];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
#pragma mark Proof of payment validation

- (void)sendCompletedPaymentToServer:(PayPalPayment *)completedPayment {
    // TODO: Send completedPayment.confirmation to server
    NSLog(@"Here is your proof of payment:\n\n%@\n\nSend this to your server for confirmation and fulfillment.", completedPayment.confirmation);
    
    NSDictionary *result = completedPayment.confirmation;
    id response = [result objectForKey:@"response"];
    if( [response isKindOfClass:[NSDictionary class]])
    {
        _justPayKey=[response objectForKey:@"id"];
        NSLog(@"justPayKey===%@",_justPayKey);
    }

}
#pragma mark - Authorize Future Payments

- (IBAction)getUserAuthorizationForFuturePayments:(id)sender {
    
    PayPalFuturePaymentViewController *futurePaymentViewController = [[PayPalFuturePaymentViewController alloc] initWithConfiguration:self.payPalConfig delegate:self];
    [self presentViewController:futurePaymentViewController animated:YES completion:nil];
}


#pragma mark PayPalFuturePaymentDelegate methods

- (void)payPalFuturePaymentViewController:(PayPalFuturePaymentViewController *)futurePaymentViewController
                didAuthorizeFuturePayment:(NSDictionary *)futurePaymentAuthorization {
    NSLog(@"PayPal Future Payment Authorization Success!");
    self.resultText = [futurePaymentAuthorization description];
    [self showSuccess];
    
    [self sendFuturePaymentAuthorizationToServer:futurePaymentAuthorization];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)payPalFuturePaymentDidCancel:(PayPalFuturePaymentViewController *)futurePaymentViewController {
    NSLog(@"PayPal Future Payment Authorization Canceled");
    //self.successView.hidden = YES;
    
    _upgradeSuccessViewController.successView.hidden = YES;
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)sendFuturePaymentAuthorizationToServer:(NSDictionary *)authorization {
    // TODO: Send authorization to server
    NSLog(@"Here is your authorization:\n\n%@\n\nSend this to your server to complete future payment setup.", authorization);
}

#pragma mark - Authorize Profile Sharing

- (IBAction)getUserAuthorizationForProfileSharing:(id)sender {
    
    NSSet *scopeValues = [NSSet setWithArray:@[kPayPalOAuth2ScopeOpenId, kPayPalOAuth2ScopeEmail, kPayPalOAuth2ScopeAddress, kPayPalOAuth2ScopePhone]];
    
    PayPalProfileSharingViewController *profileSharingPaymentViewController = [[PayPalProfileSharingViewController alloc] initWithScopeValues:scopeValues configuration:self.payPalConfig delegate:self];
    [self presentViewController:profileSharingPaymentViewController animated:YES completion:nil];
}


#pragma mark PayPalProfileSharingDelegate methods

- (void)payPalProfileSharingViewController:(PayPalProfileSharingViewController *)profileSharingViewController
             userDidLogInWithAuthorization:(NSDictionary *)profileSharingAuthorization {
    NSLog(@"PayPal Profile Sharing Authorization Success!");
    self.resultText = [profileSharingAuthorization description];
    [self showSuccess];
    
    [self sendProfileSharingAuthorizationToServer:profileSharingAuthorization];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)userDidCancelPayPalProfileSharingViewController:(PayPalProfileSharingViewController *)profileSharingViewController {
    NSLog(@"PayPal Profile Sharing Authorization Canceled");
    _upgradeSuccessViewController.successView.hidden = YES;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)sendProfileSharingAuthorizationToServer:(NSDictionary *)authorization {
    // TODO: Send authorization to server
    NSLog(@"Here is your authorization:\n\n%@\n\nSend this to your server to complete profile sharing setup.", authorization);
}


#pragma mark - Helpers

- (void)showSuccess {
    
//    self.successView.hidden = NO;
//    self.successView.alpha = 1.0f;
    /*
    upgradeSuccessViewController.successView.hidden = NO;
    upgradeSuccessViewController.successView.alpha = 1.0f;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelay:2.0];
    //self.successView.alpha = 0.0f;
    upgradeSuccessViewController.successView.alpha = 0.0f;
     */
    [self performSelector:@selector(requestAddPaypal) withObject:nil afterDelay:0.5];
//    [UIView commitAnimations];
}

#pragma mark - Flipside View Controller

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"pushSettings"]) {
        [[segue destinationViewController] setDelegate:(id)self];
    }
}

#pragma mark - Proof of payment validation

- (void)ShowActivity
{

}

- (void)requestAddPaypal
{
//    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:_justPayKey forKey:@"order_num"];
    [params setObject:[self.arrItemNames objectAtIndex:_justPayIndex] forKey:@"item_name"];
    if (os0.length == 0) {
        os0 = [NSMutableString stringWithString:@""];
    }
    [params setObject:os0 forKey:@"os0"];
    [params setObject:@"Completed" forKey:@"payment_status"];
    
    [params setObject:_packagePrice forKey:@"amount"];

    [params setObject:Currency forKey:@"currency"];
    
    NSDictionary *result = nil;
    
    for( int i = 0; i < 5; i++ )
    {
        result = [UtilComm addPaypal:params];
        if( result != nil )
            break;
    }
    
//    for( UIView *v in [datePlansView subviews] )
//    {
//        if( v.tag == 2003 )
//        {
//            [v removeFromSuperview];
//            break;
//        }
//    }
    [DSBezelActivityView removeViewAnimated:YES];
    [DSBezelActivityView removeView];
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Error"];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
        return;
    }
//    if ([response isKindOfClass:[NSString class]]&& [response hasPrefix:@"true"]) {//append for sandbox
//        
//        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl procUpgradeSuccess:_justPayIndex packagePrice:_packagePrice];
//        
//        [_packagePrice release];
//        return;
//    }
    
    if([response isKindOfClass:[NSString class]] && [response isEqualToString:@"true"]){
        if (os0.length != 0) {
            NSRange subRange =[os0 rangeOfString:@"1|"];
            if (subRange.location != NSNotFound) {
                [eSyncronyAppDelegate sharedInstance].isVip = @"true";
                [[eSyncronyAppDelegate sharedInstance] saveLoginInfo];
            }else{

            }
        }
    }
    
    if (_mainMenuViewController.curretItemIdx != MENU_UPGRADE) {//add for jumping issue
        _mainMenuViewController.curretItemIdx = MENU_UPGRADE;
    }
//    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl presentViewController:paymentViewController animated:YES completion:nil];
//    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl dismissModalViewControllerAnimated:YES];//append
    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl dismissViewControllerAnimated:YES completion:nil];
    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl procUpgradeSuccess:_justPayIndex packagePrice:_packagePrice];

    [_packagePrice release];
    
}
#pragma mark vip package payment
-(void)upgrade_addVipView:(UIButton *)Sender withPrice:(NSInteger)Price{

    [VIPpackageBtn setImage:[UIImage imageNamed:@"checkBox_n.PNG"] forState:UIControlStateNormal];
    [VIPpackageBtn setImage:[UIImage imageNamed:@"checkBox_d.PNG"] forState:UIControlStateSelected];
    
    [VIPlunchBtn setImage:[UIImage imageNamed:@"checkBox_n.PNG"] forState:UIControlStateNormal];
    [VIPlunchBtn setImage:[UIImage imageNamed:@"checkBox_d.PNG"] forState:UIControlStateSelected];
    
    [VIPebookBtn setImage:[UIImage imageNamed:@"checkBox_n.PNG"] forState:UIControlStateNormal];
    [VIPebookBtn setImage:[UIImage imageNamed:@"checkBox_d.PNG"] forState:UIControlStateSelected];
    
    if (nCountryID == 7 || nCountryID == 202) {
        VipLcView.hidden = YES;
        VipEbookView.hidden = YES;
        VIPpackageBtn.selected = YES;
        VIPlunchBtn.selected = NO;
        VIPebookBtn.selected = NO;
        p_params = [[NSMutableDictionary alloc]init];
        [p_params setObject:@"1|" forKey:@"VIPpackage"];
        
    }else
    {
        VipLcView.hidden = NO;
        VipEbookView.hidden = NO;
        VIPpackageBtn.selected = YES;
        VIPlunchBtn.selected = YES;
        VIPebookBtn.selected = YES;
        p_params = [[NSMutableDictionary alloc]init];
        [p_params setObject:@"1|" forKey:@"VIPpackage"];
        [p_params setObject:@"2|" forKey:@"VIPlunch"];
        [p_params setObject:@"3|" forKey:@"VIPebook"];
    }

    numToatlPrice = Price + numPrice5 + numPrice6 + numPrice7;
    
//设置动画效果
//    [UIView transitionWithView:datePlansView
//                      duration:0.5
//                       options:UIViewAnimationOptionTransitionFlipFromRight //any animation
//                    animations:^ { [datePlansView addSubview:vipDatePlansView]; }
//                    completion:nil];
    
//    vipDatePlansView.frame = CGRectMake(0 , Sender.frame.origin.y + Sender.frame.size.width , 315, 427);//设置向下偏移
//    [scrlView addSubview:vipDatePlansView];
    [datePlansView addSubview:vipDatePlansView];
    
}
- (IBAction)didClickCancelled:(id)sender {
    
    [vipDatePlansView removeFromSuperview];
}

- (IBAction)didClickHyperlink:(id)sender {
    
//    //CGSize size = scrlView.contentSize;
//    //UIWebView *web = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
//    UIWebView *web = [[UIWebView alloc] initWithFrame:scrlView.frame];
//    web.delegate = self;
//    web.scalesPageToFit = YES;
////    [scrlView addSubview:web];
//    [self.view addSubview:web];
//
//    NSURL       *_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",btnHyperLink.titleLabel.text]];
//    
//    NSURLRequest* _urlRequest = [NSURLRequest requestWithURL:_url];
//    [web loadRequest:_urlRequest];
//    
//    [DSBezelActivityView newActivityViewForView:web withLabel:NSLocalizedString(@"Loading...",)];
    
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",btnHyperLink.titleLabel.text]]];
    
}

//make lunchclick is a single interfance
//- (void)webViewDidFinishLoad:(UIWebView *)webView
//{
//    [DSBezelActivityView removeViewAnimated:NO];
//}
//
//- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
//{
//    [DSBezelActivityView removeViewAnimated:NO];
////    [web removeFromSuperview];
//    UIAlertView* av = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"Loading failed!",) delegate:nil cancelButtonTitle:NSLocalizedString(@"Close",) otherButtonTitles:nil, nil];
//    [av show];
//    [av release];
//}

- (IBAction)checkVIPpackageClick:(UIButton *)sender {
    
    sender.selected = !sender.selected;
    
    if (sender.selected) {
        [p_params setObject:@"1|" forKey:@"VIPpackage"];
        numToatlPrice  = numToatlPrice + numPrice5;

    }else{
        [p_params removeObjectForKey:@"VIPpackage"];
        numToatlPrice  = numToatlPrice - numPrice5;
     }
}

- (IBAction)checkVIPlunchClick:(UIButton *)sender {
    
    sender.selected = !sender.selected;
    if (sender.selected) {
        [p_params setObject:@"2|" forKey:@"VIPlunch"];
        numToatlPrice = numToatlPrice + numPrice6;

    }else{
        [p_params removeObjectForKey:@"VIPlunch"];
        numToatlPrice = numToatlPrice - numPrice6;
    }
}

- (IBAction)checkVIPebookClick:(UIButton *)sender {
    
    sender.selected = !sender.selected;
    
    if (sender.selected) {
        [p_params setObject:@"3|" forKey:@"VIPebook"];
        numToatlPrice = numToatlPrice + numPrice7;
    }else{
        [p_params removeObjectForKey:@"VIPebook"];
        numToatlPrice = numToatlPrice - numPrice7;
    }
}

- (IBAction)onClickProceedPayment:(id)sender {
    
    proceedPayment = YES;
    os0 = [[NSMutableString alloc]init];
    NSEnumerator * enumeratorValue = [p_params objectEnumerator];
    for (NSObject *object in enumeratorValue) {
        [os0 appendString:(NSString *)object];
        NSLog(@"os0 == %@",os0);
    }
    
    
    switch (buttonTag) {
        case 12:
        {
            _justPayIndex = 0;
            NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:0];
            [params setObject:@"12 VIP Months Unlimited Date Package" forKey:@"item_name"];
            [params setObject:[NSString stringWithFormat:@"%ld",(long)numToatlPrice] forKey:@"total_price"];
            [params setObject:[self.arrItemNames objectAtIndex:_justPayIndex] forKey:@"sku"];
            [self performSelector:@selector(onCheckVIPPayment:) withObject:params afterDelay:0.1];
        }
            break;
        case 6:
        {
            _justPayIndex = 1;
            NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:0];
            [params setObject:@"6 VIP Months Unlimited Date Package" forKey:@"item_name"];
            [params setObject:[NSString stringWithFormat:@"%ld",(long)numToatlPrice] forKey:@"total_price"];
            [params setObject:[self.arrItemNames objectAtIndex:_justPayIndex] forKey:@"sku"];
            [self performSelector:@selector(onCheckVIPPayment:) withObject:params afterDelay:0.1];
        }
            break;
        case 4:
        {
            _justPayIndex = 2;
            NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:0];
            [params setObject:@"4 VIP Months Unlimited Date Package" forKey:@"item_name"];
            [params setObject:[NSString stringWithFormat:@"%ld",(long)numToatlPrice] forKey:@"total_price"];
            [params setObject:[self.arrItemNames objectAtIndex:_justPayIndex] forKey:@"sku"];
            [self performSelector:@selector(onCheckVIPPayment:) withObject:params afterDelay:0.1];
        }
            break;
        case 2:
        {
            _justPayIndex = 3;
            NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:0];
            [params setObject:@"2 VIP Months Unlimited Date Package" forKey:@"item_name"];
            [params setObject:[NSString stringWithFormat:@"%ld",(long)numToatlPrice] forKey:@"total_price"];
            [params setObject:[self.arrItemNames objectAtIndex:_justPayIndex] forKey:@"sku"];
            [self performSelector:@selector(onCheckVIPPayment:) withObject:params afterDelay:0.1];
        }
            break;
        default:
            break;
    }
    return;
}

-(void)onCheckVIPPayment:(NSMutableDictionary *)params{
    
//    [vipDatePlansView removeFromSuperview];
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    
    // Remove our last completed payment, just for demo purposes.
    self.resultText = nil;
    
    // Optional: include multiple items
    
    NSString *totalPrice = [[params objectForKey:@"total_price"] substringFromIndex:0];
    
    PayPalItem *item1 = [PayPalItem itemWithName:[params objectForKey:@"item_name"]
                                    withQuantity:1
                                       withPrice:[NSDecimalNumber decimalNumberWithString:totalPrice]
                                    withCurrency:Currency
                                         withSku:[params objectForKey:@"sku"]];
    
    
    
    
    NSArray *items = @[item1];
    NSDecimalNumber *subtotal = [PayPalItem totalPriceForItems:items];
    
    // Optional: include payment details
    NSDecimalNumber *shipping = [[NSDecimalNumber alloc] initWithString:@"0"];
    NSDecimalNumber *tax = [[NSDecimalNumber alloc] initWithString:@"0"];
    PayPalPaymentDetails *paymentDetails = [PayPalPaymentDetails paymentDetailsWithSubtotal:subtotal
                                                                               withShipping:shipping
                                                                                    withTax:tax];
    
    NSDecimalNumber *total = [[subtotal decimalNumberByAdding:shipping] decimalNumberByAdding:tax];
    
    PayPalPayment *payment = [[PayPalPayment alloc] init];
    payment.amount = total;
    payment.currencyCode = Currency;
    payment.shortDescription = @"Purchase price";
    payment.items = items;  // if not including multiple items, then leave payment.items as nil
    payment.paymentDetails = paymentDetails; // if not including payment details, then leave payment.paymentDetails as nil
    
    _packagePrice=(NSString *)total;
    //NSLog(@"_packagePrice===%@",_packagePrice);
    
    if (!payment.processable) {
        // This particular payment will always be processable. If, for
        // example, the amount was negative or the shortDescription was
        // empty, this payment wouldn't be processable, and you'd want
        // to handle that here.
    }
    
    // Update payPalConfig re accepting credit cards.
    self.payPalConfig.acceptCreditCards = self.acceptCreditCards;
    
    PayPalPaymentViewController *paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:[payment retain]
                                                                                                configuration:self.payPalConfig
                                                                                                     delegate:self];
    
    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl presentViewController:paymentViewController animated:YES completion:nil];
    return;
}

//join now
- (IBAction)onClickJoin12:(id)sender
{
    if (proceedPayment) {
        
        [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
        
        self.buyButton = self.btnJoin1;
        _justPayIndex = 0;
        
        // NSLog(@"lblTotalMoney1.text==%@\nlblMonthMoney1.text==%@\nself.lblMonth1.text==%@\n",lblTotalMoney1.text,lblMonthMoney1.text,self.lblMonth1.text);
        
        
        // Remove our last completed payment, just for demo purposes.
        self.resultText = nil;
        
        // Optional: include multiple items
        
        NSString *totalPrice = [lblTotalMoney1.text substringFromIndex:3];
        
        PayPalItem *item1 = [PayPalItem itemWithName:@"12 Months Unlimited Date Package"
                                        withQuantity:1
                                           withPrice:[NSDecimalNumber decimalNumberWithString:totalPrice]
                                        withCurrency:Currency
                                             withSku:@"date_10_12"];
        
        
        
        
        NSArray *items = @[item1];
        NSDecimalNumber *subtotal = [PayPalItem totalPriceForItems:items];
        
        // Optional: include payment details
        NSDecimalNumber *shipping = [[NSDecimalNumber alloc] initWithString:@"0"];
        NSDecimalNumber *tax = [[NSDecimalNumber alloc] initWithString:@"0"];
        PayPalPaymentDetails *paymentDetails = [PayPalPaymentDetails paymentDetailsWithSubtotal:subtotal
                                                                                   withShipping:shipping
                                                                                        withTax:tax];
        
        NSDecimalNumber *total = [[subtotal decimalNumberByAdding:shipping] decimalNumberByAdding:tax];
        
        PayPalPayment *payment = [[PayPalPayment alloc] init];
        payment.amount = total;
        payment.currencyCode = Currency;
        payment.shortDescription = @"Purchase price";
        payment.items = items;  // if not including multiple items, then leave payment.items as nil
        payment.paymentDetails = paymentDetails; // if not including payment details, then leave payment.paymentDetails as nil
        
        _packagePrice=(NSString *)total;
        //NSLog(@"_packagePrice===%@",_packagePrice);
        
        if (!payment.processable) {
            // This particular payment will always be processable. If, for
            // example, the amount was negative or the shortDescription was
            // empty, this payment wouldn't be processable, and you'd want
            // to handle that here.
        }
        
        // Update payPalConfig re accepting credit cards.
        self.payPalConfig.acceptCreditCards = self.acceptCreditCards;
        
        PayPalPaymentViewController *paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:[payment retain]
                                                                                                    configuration:self.payPalConfig
                                                                                                         delegate:self];
        
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl presentViewController:paymentViewController animated:YES completion:nil];
        return;
    }else
    {
        buttonTag = 12;
        [self upgrade_addVipView:(UIButton *)sender withPrice:numPrice1];
    }
}
- (IBAction)onClickJoin6:(id)sender
{
    if (proceedPayment) {
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    
    self.buyButton = self.btnJoin2;
    _justPayIndex = 1;
    
      //NSLog(@"lblTotalMoney2.text==%@\nlblMonthMoney2.text==%@\nself.lblMonth2.text==%@\n",lblTotalMoney2.text,lblMonthMoney2.text,self.lblMonth2.text);
    
    // Remove our last completed payment, just for demo purposes.
    self.resultText = nil;
    
    NSString *totalPrice = [lblTotalMoney2.text substringFromIndex:3];
    
    PayPalItem *item2 = [PayPalItem itemWithName:@"6 Months Unlimited Date Package"
                                    withQuantity:1
                                       withPrice:[NSDecimalNumber decimalNumberWithString:totalPrice]
                                    withCurrency:Currency
                                         withSku:[self.arrItemNames objectAtIndex:_justPayIndex]];
    

    
    NSArray *items = @[item2];
    NSDecimalNumber *subtotal = [PayPalItem totalPriceForItems:items];
    
    // Optional: include payment details
    NSDecimalNumber *shipping = [[NSDecimalNumber alloc] initWithString:@"0"];
    NSDecimalNumber *tax = [[NSDecimalNumber alloc] initWithString:@"0"];
    PayPalPaymentDetails *paymentDetails = [PayPalPaymentDetails paymentDetailsWithSubtotal:subtotal
                                                                               withShipping:shipping
                                                                                    withTax:tax];
    
    NSDecimalNumber *total = [[subtotal decimalNumberByAdding:shipping] decimalNumberByAdding:tax];
    
    PayPalPayment *payment = [[PayPalPayment alloc] init];
    payment.amount = total;
    payment.currencyCode = Currency;
    payment.shortDescription = @"Purchase price";
    payment.items = items;  // if not including multiple items, then leave payment.items as nil
    payment.paymentDetails = paymentDetails; // if not including payment details, then leave payment.paymentDetails as nil
    
    _packagePrice=(NSString *)total;
    //NSLog(@"_packagePrice===%@",_packagePrice);
    
    if (!payment.processable) {
        // This particular payment will always be processable. If, for
        // example, the amount was negative or the shortDescription was
        // empty, this payment wouldn't be processable, and you'd want
        // to handle that here.
    }
    
    // Update payPalConfig re accepting credit cards.
    self.payPalConfig.acceptCreditCards = self.acceptCreditCards;
    
    PayPalPaymentViewController *paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment
                                                                                                configuration:self.payPalConfig
                                                                                                     delegate:self];
    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl presentViewController:paymentViewController animated:YES completion:nil];
    return;
    }else
    {
        buttonTag = 6;
        [self upgrade_addVipView:(UIButton *)sender withPrice:numPrice2];
    }
}

- (IBAction)onClickJoin4:(id)sender;
{
    if (proceedPayment) {
        [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
        
        self.buyButton = self.btnJoin3;
        _justPayIndex = 2;
        // Remove our last completed payment, just for demo purposes.
        
        // NSLog(@"lblTotalMoney3.text==%@\nlblMonthMoney3.text==%@\nself.lblMonth3.text==%@\n",lblTotalMoney3.text,lblMonthMoney3.text,self.lblMonth3.text);
        
        self.resultText = nil;
        
        NSString *totalPrice = [lblTotalMoney3.text substringFromIndex:3];
        
        PayPalItem *item3 = [PayPalItem itemWithName:@"4 Months Unlimited Date Package"
                                        withQuantity:1
                                           withPrice:[NSDecimalNumber decimalNumberWithString:totalPrice]//122.3
                                        withCurrency:Currency
                                             withSku:@"date_3_4"];
        
        
        NSArray *items = @[item3];
        NSDecimalNumber *subtotal = [PayPalItem totalPriceForItems:items];
        
        // Optional: include payment details
        NSDecimalNumber *shipping = [[NSDecimalNumber alloc] initWithString:@"0"];
        NSDecimalNumber *tax = [[NSDecimalNumber alloc] initWithString:@"0"];
        PayPalPaymentDetails *paymentDetails = [PayPalPaymentDetails paymentDetailsWithSubtotal:subtotal
                                                                                   withShipping:shipping
                                                                                        withTax:tax];
        
        NSDecimalNumber *total = [[subtotal decimalNumberByAdding:shipping] decimalNumberByAdding:tax];
        
        PayPalPayment *payment = [[PayPalPayment alloc] init];
        payment.amount = total;
        payment.currencyCode = Currency;
        payment.shortDescription = @"Purchase price";
        payment.items = items;  // if not including multiple items, then leave payment.items as nil
        payment.paymentDetails = paymentDetails; // if not including payment details, then leave payment.paymentDetails as nil
        
        _packagePrice=(NSString *)total;
        //NSLog(@"_packagePrice===%@",_packagePrice);
        
        if (!payment.processable) {
            // This particular payment will always be processable. If, for
            // example, the amount was negative or the shortDescription was
            // empty, this payment wouldn't be processable, and you'd want
            // to handle that here.
        }
        // Update payPalConfig re accepting credit cards.
        self.payPalConfig.acceptCreditCards = self.acceptCreditCards;
        
        PayPalPaymentViewController *paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment
                                                                                                    configuration:self.payPalConfig
                                                                                                         delegate:self];
        
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl presentViewController:paymentViewController animated:YES completion:nil];
        
        return;
    }else
    {
        buttonTag = 4;
        [self upgrade_addVipView:(UIButton *)sender withPrice:numPrice3];
    }
  
}

- (IBAction)onClickJoin2:(id)sender
{
    if (proceedPayment) {
        [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
        
        self.buyButton = self.btnJoin4;
        _justPayIndex = 3;
        
        //     NSLog(@"lblTotalMoney4.text==%@\nlblMonthMoney4.text==%@\nself.lblMonth4.text==%@\n",lblTotalMoney4.text,lblMonthMoney4.text,self.lblMonth4.text);
        
        // Remove our last completed payment, just for demo purposes.
        self.resultText = nil;
        
        NSString *totalPrice = [lblTotalMoney4.text substringFromIndex:3];
        
        PayPalItem *item4 = [PayPalItem itemWithName:@"2 Months Unlimited Date Package"
                                        withQuantity:1
                                           withPrice:[NSDecimalNumber decimalNumberWithString:totalPrice]
                                        withCurrency:Currency withSku:@"date_1_2"];
        
        NSArray *items = @[item4];
        NSDecimalNumber *subtotal = [PayPalItem totalPriceForItems:items];
        
        // Optional: include payment details
        NSDecimalNumber *shipping = [[NSDecimalNumber alloc] initWithString:@"0"];
        NSDecimalNumber *tax = [[NSDecimalNumber alloc] initWithString:@"0"];
        PayPalPaymentDetails *paymentDetails = [PayPalPaymentDetails paymentDetailsWithSubtotal:subtotal
                                                                                   withShipping:shipping
                                                                                        withTax:tax];
        
        NSDecimalNumber *total = [[subtotal decimalNumberByAdding:shipping] decimalNumberByAdding:tax];
        
        PayPalPayment *payment = [[PayPalPayment alloc] init];
        payment.amount = total;
        payment.currencyCode = Currency;
        payment.shortDescription = @"Purchase price";
        payment.items = items;  // if not including multiple items, then leave payment.items as nil
        payment.paymentDetails = paymentDetails; // if not including payment details, then leave payment.paymentDetails as nil
        
        _packagePrice=(NSString *)total;
        //NSLog(@"_packagePrice===%@",_packagePrice);
        
        if (!payment.processable) {
            // This particular payment will always be processable. If, for
            // example, the amount was negative or the shortDescription was
            // empty, this payment wouldn't be processable, and you'd want
            // to handle that here.
        }
        // Update payPalConfig re accepting credit cards.
        self.payPalConfig.acceptCreditCards = self.acceptCreditCards;
        
        PayPalPaymentViewController *paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment
                                                                                                    configuration:self.payPalConfig
                                                                                                         delegate:self];
        
//        [paymentViewController.view addSubview:_payFutureButton];
        
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl presentViewController:paymentViewController animated:YES completion:nil];
        
        return;
    }else
    {
        buttonTag = 2;
        [self upgrade_addVipView:(UIButton *)sender withPrice:numPrice4];
    }
}
- (IBAction)onClickPhone:(id)sender
{
    //0: Singapore, 1: Malaysia, 2: HONG KONG
    nCountryID = [eSyncronyAppDelegate sharedInstance].countryId;
    
    if( nCountryID == 0 )
        
        [PhoneUtil sendCall:@"63375503"];
    
    else if( nCountryID == 1 )
        
        [PhoneUtil sendCall:@"22836686"];
    
    else
        [PhoneUtil sendCall:@"25241200"];
}

- (IBAction)onClickEmail:(id)sender
{
//    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.esynchrony.com/contact-us.php"]];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",WEBSERVICE_EMAILCONTANCT]]];
}

- (IBAction)onClickHelpTopics:(id)sender
{
//    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.esynchrony.com/help.php"]];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",WEBSERVICE_HELPTOPICS]]];
}
- (IBAction)didClickSeeMoreCouples:(id)sender {
    
    ourSuccessStoriesViewController *ourSucsStrView = [[[ourSuccessStoriesViewController alloc] initWithNibName:@"ourSuccessStoriesViewController" bundle:nil] autorelease];
    ourSucsStrView.isTable = YES;
    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.navigationController pushViewController:ourSucsStrView animated:YES];
}


#pragma shared
//对数字进行MD5散列
-(NSString*)md5:(NSString*)str{
    const char *cStr = [str UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5( cStr,(int)strlen(cStr), result );
    
    return [NSString stringWithFormat:
            @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
    
}
//触发分享的事件
- (IBAction)ShareEventClicked:(id)sender {
    
    NSString *imagePath = [[NSBundle mainBundle] pathForResource:@"ShareSDK" ofType:@"jpg"];
    
    //随机字符串
//    int val= abs(rand())%0xFFFF;
//    NSString *str = [NSString stringWithFormat:@"%x", val];
    
    int val = arc4random() % 10;//随机生成0-9的数字
    NSString *str = [self md5:[NSString stringWithFormat:@"%x", val]];
//    NSLog(@"%@",str);
    
    NSString *acc_no=[eSyncronyAppDelegate sharedInstance].strAccNo;
    NSString *pay =[eSyncronyAppDelegate sharedInstance].paying;
    
    NSDate* senddate = [NSDate date];
    NSDateFormatter  *dateformatter=[[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"yyyy-MM-dd"];
    NSString *locationString=[dateformatter stringFromDate:senddate];
//    NSLog(@"locationString:%@",locationString);
    [dateformatter release];
    /*
      https://www.esynchrony.com/index.php?reg=1&hsf=5&r=c17c946ddd3ae35a9fb0ea47e60c803d|1137|N|2015-08-13

     > r=c17c946ddd3ae35a9fb0ea47e60c803d|1137|N|2015-08-13
     第一个是随机字符串，没啥用
     第二个是用户的acc_no
     第三个为当天日期
     */
    
//    NSString *url = [NSString stringWithFormat:@"https://www.esynchrony.com/index.php?reg=1&hsf=5&r=%@|%@|%@|%@",str,acc_no,pay,locationString];
    NSString *url = [NSString stringWithFormat:@"%@/index.php?reg=1&hsf=5&r=%@|%@|%@|%@",HOST,str,acc_no,pay,locationString];
    
    //1、构造分享内容
//    id<ISSContent> publishContent = [ShareSDK content:url
//                                       defaultContent:url
//                                                image:[ShareSDK imageWithPath:imagePath]
//                                                title:@"eSynchrony"
//                                                  url:@"http://www.mob.com"
//                                          description:@"test"
//                                            mediaType:SSPublishContentMediaTypeNews];
    id<ISSContent> publishContent = [ShareSDK content:url
                                       defaultContent:url
                                                image:[ShareSDK imageWithPath:imagePath]
                                                title:@"eSynchrony"
                                                  url:url
                                          description:@"share"
                                            mediaType:SSPublishContentMediaTypeNews];
    //1+创建弹出菜单容器（iPad必要）
    id<ISSContainer> container = [ShareSDK container];
    [container setIPadContainerWithView:sender arrowDirect:UIPopoverArrowDirectionUp];
    
    //2、弹出分享菜单
    [ShareSDK showShareActionSheet:container
                         shareList:nil
                           content:publishContent
                     statusBarTips:YES
                       authOptions:nil
                      shareOptions:nil
                            result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                                
                                //可以根据回调提示用户。
                                if (state == SSResponseStateSuccess)
                                {
                                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Shared Successful"
                                                                                    message:nil
                                                                                   delegate:self
                                                                          cancelButtonTitle:@"OK"
                                                                          otherButtonTitles:nil, nil];
                                    [alert show];
                                }
                                else if (state == SSResponseStateFail)
                                {
                                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Shared Failure"
                                                                                    message:[NSString stringWithFormat:@"Failure Description：%@",[error errorDescription]]
                                                                                   delegate:self
                                                                          cancelButtonTitle:@"OK"
                                                                          otherButtonTitles:nil, nil];
                                    [alert show];
                                }
                            }];
    
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{//promo code
    
    float offset =226-([ UIScreen mainScreen ].bounds.size.height-scrlView.frame.origin.y -75- self.txtPromoCode.frame.origin.y-self.txtPromoCode.frame.size.height);
    
  // NSLog(@"\nscrlView.frame.origin.y:%f\nself.txtPromoCode.frame.origin.y:%f\n[ UIScreen mainScreen ].bounds.size.height:%f\noffset:%f",scrlView.frame.origin.y,self.txtPromoCode.frame.origin.y,[ UIScreen mainScreen ].bounds.size.height,offset);//
    
    [scrlView setContentOffset:CGPointMake(0, offset)  animated:NO];
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string//promo code
{
    if ([string isEqualToString:@"\n"]) {
        
        [textField resignFirstResponder];
       // [scrlView setContentOffset:CGPointMake(0, 0)  animated:NO];
        NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", @"\\w+\\d{4}$"];
        if ([regextestmobile evaluateWithObject:self.txtPromoCode.text]) {
            
            [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
            [self performSelector:@selector(loadPackageList) withObject:nil afterDelay:0.01];
        
        }else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Invalid Code",@"UpgradeView") message:NSLocalizedString(@"Please enter a valid promo code.",@"UpgradeView") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",) otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
        }
        return NO;
    }
    return YES;
}

//调用UIAlertViewDelegate中的方法，使得界面不会block，从而可以继续后面的操作
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex//append 0417
{
    [DSBezelActivityView removeViewAnimated:NO];
    return;
}
- (void)dealloc
{
    [scrlView release];
    [datePlansView release];
    [benefitsView release];
    [datePlansBtn release];
    [benefitsBtn release];
    [datePlansBtnLabel release];
    [benefitsBtnLabel release];

    [_viewList release];
    [_imageTitle release];
    [_imageStory release];
    [_balckView release];
    [_readMoreView release];
    [_benefitsListTableView release];
    [_lblMonthPlan release];
    [_lblMonth1 release];
    [_lblMonth2 release];
    [_lblMonth3 release];
    [_lblMonth4 release];
    
    [_btnJoin1 release];
    [_btnJoin2 release];
    [_btnJoin3 release];
    [_btnJoin4 release];
    [_arrProductIdentifiers release];

    [_txtPromoCode release];
    [_hidenView release];
    [lblMonthMoney4 release];
    [plabel_1 release];
    [plabel_2 release];
    [plabel_3 release];
    [plabel_3 release];
    [plabel_4 release];
    [plabel_5 release];
    [plabel_6 release];
    [plabel_7 release];
    [plabel_8 release];
    [plabel_9 release];
    [plabel_10 release];

    [plabel_11 release];
    [plabel_12 release];
    [plabel_13 release];
    [plabel_14 release];
    [plabel_16 release];
    [plabel_17 release];
    [plabel_18 release];
    [plabel_20 release];
    [pcontanctEmail release];
    [phelpTocips release];
    [pBenLabel release];
    [vipDatePlansView release];
    [VIPpackageBtn release];
    [VIPlunchBtn release];
    [VIPebookBtn release];
    [lblTotalMoney5 release];
    [lblTotalMoney6 release];
    [lblTotalMoney7 release];
    [lblItemName1 release];
    [lblItemName2 release];
    [lblItemName3 release];
    [lblTotalVIPMoney release];
    [btnHyperLink release];
    [VipLcView release];
    [VipEbookView release];
    [proceedBtn release];
    [super dealloc];
}


@end


#import <UIKit/UIKit.h>

@interface NewDatesUpcomingCell : UITableViewCell
{
    IBOutlet UIImageView    *photoBGView;
    IBOutlet UIImageView    *photoImgView;
    IBOutlet UIImageView    *photoFrameView;
    
    IBOutlet UIImageView    *rightView;
    
    IBOutlet UILabel        *lblName;
    IBOutlet UILabel        *lblAge;
    IBOutlet UILabel        *lblEthnicity;
    IBOutlet UILabel        *lblDate;
    IBOutlet UILabel        *lblVenue;
    IBOutlet UILabel        *lblStatus;
    
    IBOutlet UILabel        *lblDateTitle;
    IBOutlet UILabel        *lblVenueTitle;


    NSMutableDictionary     *_dicMatchesInfo;
}

- (void)setMatchesInfo:(NSMutableDictionary*)dicMatchesInfo;

@end

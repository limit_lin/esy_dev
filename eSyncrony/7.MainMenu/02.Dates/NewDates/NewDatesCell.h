

#import <UIKit/UIKit.h>

@interface NewDatesCell : UITableViewCell
{
    IBOutlet UIImageView    *photoBGView;
    IBOutlet UIImageView    *photoImgView;
    IBOutlet UIImageView    *photoFrameView;
    
    IBOutlet UIImageView    *rightView;
    
    IBOutlet UILabel        *lblName;
    IBOutlet UILabel        *lblAge;
    IBOutlet UILabel        *lblEthnicity;
    IBOutlet UILabel        *lblStatus;
    
    IBOutlet UIButton *btnGoPay;
    NSMutableDictionary     *_dicMatchesInfo;
}

- (IBAction)didClickGoPay:(id)sender;
- (void)setMatchesInfo:(NSMutableDictionary*)dicMatchesInfo;

@end


#import <UIKit/UIKit.h>

@interface newDatesViewController : GAITrackedViewController
{
    IBOutlet UITableView    *listTblView;
    IBOutlet UILabel        *lblNoMatches;
    
    NSMutableArray          *arrMatchesItems;
}

@property (nonatomic) BOOL  isSelectedCoor;

- (void)didClickBeingCoordinated;
- (void)didClickUpcoming;

@end

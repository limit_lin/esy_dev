
#import "Global.h"
#import "UtilComm.h"
#import "DSActivityView.h"
#import "ImageUtil.h"
#import "mainMenuViewController.h"
#import "NewDatesCell.h"
#import "eSyncronyAppDelegate.h"
#import "UIImageView+WebCache.h"
@implementation NewDatesCell

- (void)loadDefaultImage{
    if( [eSyncronyAppDelegate sharedInstance].gender == 1 ){
        photoImgView.image = [UIImage imageNamed:@"male1.png"];
    }else {
        photoImgView.image = [UIImage imageNamed:@"female1.png"];
    }
}

- (BOOL)downloadMatchUserPhoto
{
    NSString        *macc_no = [_dicMatchesInfo objectForKey:@"acc_no"];
    if( macc_no == nil )
        macc_no = [_dicMatchesInfo objectForKey:@"Acc_no"];
    if( macc_no == nil )
        return FALSE;

    NSString    *view_photo = [_dicMatchesInfo objectForKey:@"view_photo"];
    
    if( [view_photo isEqualToString:@"photo view not allowed."] )
        return FALSE;
    
    NSString *filename = [_dicMatchesInfo objectForKey:@"filename"];
    NSString*   imageURL = [NSString stringWithFormat:@"%@%@", WEBSERVICE_PHOTO_BASEURI, filename];
    NSString*   imagePath = [ImageUtil loadImagePathFromURL:imageURL];
    
    [self performSelectorOnMainThread:@selector(NewDatesCell_onDownLoadImage:) withObject:imagePath waitUntilDone:YES];
    
    return TRUE;
}

- (void)NewDatesCell_onDownLoadImage:(NSString*)imagePath
{
    UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
    if( image != nil )
    {
        photoFrameView.hidden = NO;
        photoImgView.image = image;
        [_dicMatchesInfo setObject:imagePath forKey:@"localphoto_path"];
    }
    else
        [_dicMatchesInfo setObject:@"nophoto" forKey:@"localphoto_path"];
}

- (IBAction)didClickGoPay:(id)sender {
    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl procUpgrade];
}

- (void)setMatchesInfo:(NSMutableDictionary*)dicMatchesInfo
{
    _dicMatchesInfo = dicMatchesInfo;
    
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
    
    photoBGView.layer.cornerRadius = 5;
    photoImgView.layer.cornerRadius = 45;
    photoImgView.clipsToBounds = YES;
    photoFrameView.hidden = YES;
    
    rightView.layer.cornerRadius = 5;

    lblName.text = [dicMatchesInfo objectForKey:@"nname"];
//    [lblName sizeToFit];
    CGSize lblNameSize = [lblName.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:lblName.font,NSFontAttributeName, nil]];
    lblName.frame = CGRectMake(120, 25, lblNameSize.width,lblNameSize.height);
    
    CGFloat xPos = lblName.frame.origin.x + lblName.frame.size.width;
    CGFloat yPos = lblName.frame.origin.y;
    
    lblAge.text = [NSString stringWithFormat:@", %@", [dicMatchesInfo objectForKey:@"age"]];
    CGRect frame = lblAge.frame;
    frame.origin.x = xPos;
    frame.origin.y = yPos;
    
    lblAge.frame = frame;
    [lblAge sizeToFit];
//    lblEthnicity.text = [dicMatchesInfo objectForKey:@"Ethnicity"];
    //===============international treatment=========0716===========//
    if ([[eSyncronyAppDelegate sharedInstance].language hasPrefix:@"zh-Hant"]||[[eSyncronyAppDelegate sharedInstance].language hasPrefix:@"zh-HK"]) {//zh-Hant
        
        NSString *_strPath =[[NSBundle mainBundle] pathForResource:@"ethnicity_zhHant" ofType:@"plist"];
//        NSString *_strPath =[[NSBundle mainBundle] pathForResource:@"ethnicity_zhHant" ofType:@"xml"];
        NSDictionary *_dicCountry = [NSDictionary dictionaryWithContentsOfFile:_strPath];
        NSDictionary *p_ethnicity=[_dicCountry objectForKey:@"ethnicity"];
        lblEthnicity.text = [p_ethnicity objectForKey:[dicMatchesInfo objectForKey:@"Ethnicity"]];
    }else{
        lblEthnicity.text = [dicMatchesInfo objectForKey:@"Ethnicity"];//no international treatment
    }
    //========================================================//

    NSString *strPayment = [dicMatchesInfo objectForKey:@"payment"];
    
    if( [strPayment isEqualToString:@"1"]){
        
        lblStatus.text = NSLocalizedString(@"Status:Arranging Date",@"NewDatesCell");
        btnGoPay.hidden = YES;

    }else{

        btnGoPay.hidden = NO;
        lblStatus.text = NSLocalizedString(@"Click here to arrange this date for you.",@"NewDatesCell");
    }
    
    NSString *strImage = nil;
    if( ![[_dicMatchesInfo objectForKey:@"view_photo"] isEqualToString:@"photo view not allowed."] ){
        strImage = [NSString stringWithFormat:@"%@%@", WEBSERVICE_PHOTO_BASEURI, [_dicMatchesInfo objectForKey:@"filename"]];
    }
    
    NSString *strPlaceholder;
    if( [eSyncronyAppDelegate sharedInstance].gender == 1 ){
        strPlaceholder = @"male1.png";
    }else {
        strPlaceholder = @"female1.png";
    }
    
    [photoImgView sd_setImageWithURL:[NSURL URLWithString:strImage] placeholderImage:[UIImage imageNamed:strPlaceholder] ];

//    if( [eSyncronyAppDelegate sharedInstance].gender == 1 )
//        photoImgView.image = [UIImage imageNamed:@"male1.png"];
//
//    NSString    *imagePath = [_dicMatchesInfo objectForKey:@"localphoto_path"];
//    
//    if( imagePath != nil )
//    {
//        UIImage     *image = [UIImage imageWithContentsOfFile:imagePath];
//        if( image != nil )
//            photoImgView.image = image;
//    }
//    else
//    {
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,
//                                                 (unsigned long)NULL), ^(void) {
//            [_dicMatchesInfo retain];
//            if( [self downloadMatchUserPhoto] == FALSE )
//            {
//                [_dicMatchesInfo setObject:@"nophoto" forKey:@"localphoto_path"];
//            }
//            [dicMatchesInfo release];
//        });
//    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)dealloc
{
    [photoBGView release];
    [photoImgView release];
    [photoFrameView release];
    [rightView release];
    [lblName release];
    [lblAge release];
    [lblStatus release];

    [btnGoPay release];
    [lblEthnicity release];
    [super dealloc];
}

@end

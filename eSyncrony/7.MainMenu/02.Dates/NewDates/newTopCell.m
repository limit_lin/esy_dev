
#import "newTopCell.h"
#import "newDatesViewController.h"
#import "Global.h"

@implementation newTopCell

//@synthesize coordinatedBtn, coordinatedLabel, upcomingDatesBtn, upcomingDatesLabel, newDateViewCtl, isSelectedCoor;
@synthesize coordinatedBtn, coordinatedLabel, upcomingDatesBtn, upcomingDatesLabel, freshDateViewCtl, isSelectedCoor;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
    }
    return self;
}
- (void)awakeFromNib
{
    // Initialization code
//    self.lblInstructions.textAlignment = NSTextAlignmentCenter;
//    self.lblInstructions.numberOfLines = 0;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)refreshButtonStatus
{
    if( self.isSelectedCoor )
    {
        [coordinatedBtn setSelected:YES];
        [coordinatedLabel setTextColor:[UIColor whiteColor]];
        
        [upcomingDatesBtn setSelected:NO];
        [upcomingDatesLabel setTextColor:TITLE_TEXT_COLLOR];
    }
    else
    {
        [coordinatedBtn setSelected:NO];
        [coordinatedLabel setTextColor:TITLE_TEXT_COLLOR];
        
        [upcomingDatesBtn setSelected:YES];
        [upcomingDatesLabel setTextColor:[UIColor whiteColor]];
    }

    //[coordinatedLabel sizeToFit];
    //[upcomingDatesLabel sizeToFit];
}

- (IBAction)didClickBeingCoordinated:(id)sender
{
    if( isSelectedCoor )
        return;
    
    isSelectedCoor = YES;
    [self refreshButtonStatus];
    
//    [newDateViewCtl didClickBeingCoordinated];
    [freshDateViewCtl didClickBeingCoordinated];
    
}

- (IBAction)didClickUpcomingDates:(id)sender
{
    if( !isSelectedCoor )
        return;

    isSelectedCoor = NO;
    [self refreshButtonStatus];
    
//    [newDateViewCtl didClickUpcoming];
    [freshDateViewCtl didClickUpcoming];
}

- (void)dealloc
{
    [coordinatedBtn release];
    [coordinatedLabel release];
    [upcomingDatesBtn release];
    [upcomingDatesLabel release];
    
    [_lblInstructions release];
    [super dealloc];
}

@end

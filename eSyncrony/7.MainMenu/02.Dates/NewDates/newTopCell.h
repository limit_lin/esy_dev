

#import <UIKit/UIKit.h>

@class newDatesViewController;

@interface newTopCell : UITableViewCell
{

}
//@property (nonatomic, strong) newDatesViewController    *newDateViewCtl;
@property (nonatomic, strong) newDatesViewController    *freshDateViewCtl;
@property (strong, nonatomic) IBOutlet UIButton         *coordinatedBtn;
@property (strong, nonatomic) IBOutlet UIButton         *upcomingDatesBtn;
@property (strong, nonatomic) IBOutlet UILabel          *coordinatedLabel;
@property (strong, nonatomic) IBOutlet UILabel          *upcomingDatesLabel;
@property (nonatomic, assign) BOOL                      isSelectedCoor;
@property (retain, nonatomic) IBOutlet UILabel *lblInstructions;

- (void)refreshButtonStatus;

- (IBAction)didClickBeingCoordinated:(id)sender;
- (IBAction)didClickUpcomingDates:(id)sender;

@end

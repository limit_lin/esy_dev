
#import "Global.h"
#import "DSActivityView.h"
#import "eSyncronyAppDelegate.h"
#import "mainMenuViewController.h"

#import "newDatesViewController.h"
#import "newTopCell.h"
#import "NewDatesCell.h"
#import "NewDatesUpcomingCell.h"
#import "MatchProfileViewController.h"

#import "eSyncronyAppDelegate.h"

@interface newDatesViewController ()

@end

@implementation newDatesViewController

@synthesize isSelectedCoor;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"NewDatesView";
    isSelectedCoor = YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self refreshContent];
}

- (void)refreshContent
{
    lblNoMatches.hidden = YES;

    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(esyncNewDates_loadInfos) withObject:nil afterDelay:0.1];
}

- (void)esyncNewDates_loadInfos
{
    NSString*   type;

    if( isSelectedCoor )
        type = @"1";
    else
        type = @"2";
    
    NSDictionary* result = [UtilComm viewDates:type];
    [DSBezelActivityView removeViewAnimated:NO];
    
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Error"];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
        return;
    }
    
    [arrMatchesItems release];
    arrMatchesItems = [[NSMutableArray alloc] initWithCapacity:0];
    
    NSDictionary*   responseDic = (NSDictionary*)response;
    if( [responseDic isKindOfClass:[NSDictionary class]] )
    {
        NSArray *arrItems = [responseDic objectForKey:@"item"];
        
        if( [arrItems isKindOfClass:[NSDictionary class]] )
        {
            NSDictionary* item = (NSDictionary*)arrItems;
            NSMutableDictionary *mutItem = [NSMutableDictionary dictionaryWithDictionary:item];
            
            [arrMatchesItems addObject:mutItem];
        }
        else if( [arrItems isKindOfClass:[NSArray class]] )
        {
            for( int i = 0; i < [arrItems count]; i++ )
            {
                NSDictionary        *item = [arrItems objectAtIndex:i];
                NSMutableDictionary *mutItem = [NSMutableDictionary dictionaryWithDictionary:item];
                
                [arrMatchesItems addObject:mutItem];
            }
        }
    }
    
    [listTblView reloadData];
    if( [arrMatchesItems count] == 0 )
    {
        lblNoMatches.hidden = NO;
        listTblView.scrollEnabled = NO;
    }
    else
    {
        lblNoMatches.hidden = YES;
        listTblView.scrollEnabled = YES;
    }
}

#pragma mark --- Table Functions ---
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if( arrMatchesItems == nil )
        return 1;

    return [arrMatchesItems count]+1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( indexPath.row == 0 )
        return 97;
    
    return 130;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( indexPath.row == 0 )
    {
        newTopCell *cell;
        NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"newTopCell" owner:nil options:nil];
        cell = [arr objectAtIndex:0];
        
        if( isSelectedCoor ){
            cell.lblInstructions.text = NSLocalizedString(@"These are the dates that are currently being coordinated for you by our dating consultants.",@"newDatesView");
        }else{
            cell.lblInstructions.text = NSLocalizedString(@"These are the upcoming dates that are being arranged for you.",@"newDatesView");
        }

//        cell.newDateViewCtl = self;
        cell.freshDateViewCtl = self;
        cell.isSelectedCoor = isSelectedCoor;
        
        [cell refreshButtonStatus];
        
        return cell;
    }

    if( isSelectedCoor )
    {
        NSMutableDictionary     *dicMatchesInfo = [arrMatchesItems objectAtIndex:indexPath.row-1];
        NewDatesCell            *cell;
        
//        NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"NewDatesCell" owner:nil options:nil];
//        cell = [arr objectAtIndex:0];
        
        static NSString *CellIdentifier = @"NewDatesIdentifier";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            UINib *nib = [UINib nibWithNibName:@"NewDatesCell" bundle:nil];
            [tableView registerNib:nib forCellReuseIdentifier:CellIdentifier];
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        }
        
        //cell.delegate = self;
        [cell setMatchesInfo:dicMatchesInfo];

        return cell;
    }
    else
    {
        NSMutableDictionary     *dicMatchesInfo = [arrMatchesItems objectAtIndex:indexPath.row-1];
        NewDatesUpcomingCell    *cell;
        
//        NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"NewDatesUpcomingCell" owner:nil options:nil];
//        cell = [arr objectAtIndex:0];
        
        static NSString *CellIdentifier = @"DatesUpcomingIdentifier";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            UINib *nib = [UINib nibWithNibName:@"NewDatesUpcomingCell" bundle:nil];
            [tableView registerNib:nib forCellReuseIdentifier:CellIdentifier];
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        }
        
        [cell setMatchesInfo:dicMatchesInfo];

        return cell;
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( indexPath.row == 0 )
        return;

    NSMutableDictionary             *dicMatchesInfo = [arrMatchesItems objectAtIndex:indexPath.row-1];
    MatchProfileViewController      *matchInfoViewCtrl = [MatchProfileViewController createWithMatchInfo:dicMatchesInfo];
    
    matchInfoViewCtrl.m_bTopButton = NO;
    matchInfoViewCtrl.m_bDismissSelf = YES;

    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.navigationController pushViewController:matchInfoViewCtrl animated:YES];
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (void)didClickBeingCoordinated
{
    isSelectedCoor = YES;

    [self refreshContent];
}

- (void)didClickUpcoming
{
    isSelectedCoor = NO;
    
    [self refreshContent];
}

- (void)dealloc
{
    [listTblView release];
    [lblNoMatches release];
    
    
    [arrMatchesItems release];
    
    [super dealloc];
}


@end


#import <UIKit/UIKit.h>

@interface PastDatesViewController : GAITrackedViewController
{
    IBOutlet UITableView    *listTblView;
    IBOutlet UILabel        *lblNoMatches;

    NSMutableArray          *arrMatchesItems;
}

- (void)didClickFeebbackWithInfo:(NSMutableDictionary*)dicMatchInfo;
- (void)refreshContent;
@end


#import "Global.h"
#import "DSActivityView.h"
#import "eSyncronyAppDelegate.h"
#import "mainMenuViewController.h"

#import "PastDatesViewController.h"
#import "PastDatesCell.h"
#import "NoMatchCell.h"
#import "MatchProfileViewController.h"

#import "eSyncronyAppDelegate.h"
#import "FeedbackSettingViewController.h"

@interface PastDatesViewController ()

@end

@implementation PastDatesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"PastDatesView";
    [self initUI];
}

- (void)initUI
{
    // IOS7
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    int nDisp = (screenHeight - 480) * 1.02;
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height + nDisp);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self refreshContent];
}

- (void)refreshContent
{
    lblNoMatches.hidden = YES;
    
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(esyncPastDates_loadInfos) withObject:nil afterDelay:0.1];
}

- (void)esyncPastDates_loadInfos
{
    NSDictionary* result = [UtilComm viewDates:@"4"];
    [DSBezelActivityView removeViewAnimated:NO];
    
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Error"];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
        return;
    }
    
    [arrMatchesItems release];
    arrMatchesItems = [[NSMutableArray alloc] initWithCapacity:0];
    
    NSDictionary*   responseDic = (NSDictionary*)response;
    if( [responseDic isKindOfClass:[NSDictionary class]] )
    {
        NSArray *arrItems = [responseDic objectForKey:@"item"];
        
        if( [arrItems isKindOfClass:[NSDictionary class]] )
        {
            NSDictionary* item = (NSDictionary*)arrItems;
            NSMutableDictionary *mutItem = [NSMutableDictionary dictionaryWithDictionary:item];
            
            [arrMatchesItems addObject:mutItem];
        }
        else if( [arrItems isKindOfClass:[NSArray class]] )
        {
            for( int i = 0; i < [arrItems count]; i++ )
            {
                NSDictionary        *item = [arrItems objectAtIndex:i];
                NSMutableDictionary *mutItem = [NSMutableDictionary dictionaryWithDictionary:item];
                
                [arrMatchesItems addObject:mutItem];
            }
        }
    }
    
    [listTblView reloadData];
    if( [arrMatchesItems count] == 0 )
    {
        lblNoMatches.hidden = NO;
        listTblView.scrollEnabled = NO;
    }
    else
    {
        lblNoMatches.hidden = YES;
        listTblView.scrollEnabled = YES;
    }
}

#pragma mark --- Table Functions ---
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if( arrMatchesItems == nil )
        return 0;
    
    return [arrMatchesItems count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 130;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary     *dicMatchesInfo = [arrMatchesItems objectAtIndex:indexPath.row];
    PastDatesCell           *cell;
    
//    NSArray                 *arr = [[NSBundle mainBundle] loadNibNamed:@"PastDatesCell" owner:nil options:nil];
//    cell = [arr objectAtIndex:0];
    
    static NSString *CellIdentifier = @"PastDatesIdentifier";
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        UINib *nib = [UINib nibWithNibName:@"PastDatesCell" bundle:nil];
        [tableView registerNib:nib forCellReuseIdentifier:CellIdentifier];
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    }
    
    [cell setMatchesInfo:dicMatchesInfo];
    cell.pastDatesViewController = self;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary         *dicMatchesInfo = [arrMatchesItems objectAtIndex:indexPath.row];
    MatchProfileViewController  *matchInfoViewCtrl = [MatchProfileViewController createWithMatchInfo:dicMatchesInfo];
    
    matchInfoViewCtrl.m_bTopButton = NO;
    matchInfoViewCtrl.m_bDismissSelf = YES;

    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.navigationController pushViewController:matchInfoViewCtrl animated:YES];
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (void)didClickFeebbackWithInfo:(NSMutableDictionary*)dicMatchInfo
{

    FeedbackSettingViewController* feedbackSettingViewCtrl = [[[FeedbackSettingViewController alloc] initWithNibName:@"FeedbackSettingViewController" bundle:nil] autorelease];
    
    feedbackSettingViewCtrl.curDicInfo = dicMatchInfo;
    feedbackSettingViewCtrl.pastDatesDelegate = self;
    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.navigationController pushViewController:feedbackSettingViewCtrl animated:YES];
}

- (void)dealloc
{
    [listTblView release];
    [lblNoMatches release];
    
    [arrMatchesItems release];
    
    [super dealloc];
}

@end


#import "Global.h"
#import "UtilComm.h"
#import "DSActivityView.h"
#import "ImageUtil.h"
#import "UIImageView+WebCache.h"
#import "eSyncronyAppDelegate.h"
#import "PastDatesCell.h"
#import "PastDatesViewController.h"

@implementation PastDatesCell

- (void)loadDefaultImage{
    if( [eSyncronyAppDelegate sharedInstance].gender == 1 ){
        photoImgView.image = [UIImage imageNamed:@"male1.png"];
    }else {
        photoImgView.image = [UIImage imageNamed:@"female1.png"];
    }
}

- (BOOL)downloadMatchUserPhoto
{
    NSString        *macc_no = [_dicMatchesInfo objectForKey:@"acc_no"];
    if( macc_no == nil )
        macc_no = [_dicMatchesInfo objectForKey:@"Acc_no"];
    if( macc_no == nil )
        return FALSE;

    NSString    *view_photo = [_dicMatchesInfo objectForKey:@"view_photo"];
    
    
    if( [view_photo isEqualToString:@"photo view not allowed."] )
        return FALSE;
    
    NSString *filename = [_dicMatchesInfo objectForKey:@"filename"];
    NSString*   imageURL = [NSString stringWithFormat:@"%@%@", WEBSERVICE_PHOTO_BASEURI, filename];
    NSString*   imagePath = [ImageUtil loadImagePathFromURL:imageURL];
    
    [self performSelectorOnMainThread:@selector(PastDatesCell_onDownLoadImage:) withObject:imagePath waitUntilDone:YES];
    
    return TRUE;
}

- (void)PastDatesCell_onDownLoadImage:(NSString*)imagePath
{
    UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
    if( image != nil )
    {
        photoFrameView.hidden = NO;
        photoImgView.image = image;
        [_dicMatchesInfo setObject:imagePath forKey:@"localphoto_path"];
    }
    else
        [_dicMatchesInfo setObject:@"nophoto" forKey:@"localphoto_path"];
}

- (void)setMatchesInfo:(NSMutableDictionary*)dicMatchesInfo
{
    _dicMatchesInfo = dicMatchesInfo;
    
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
    
    photoBGView.layer.cornerRadius = 5;
    photoImgView.layer.cornerRadius = 45;
    photoImgView.clipsToBounds = YES;
    photoFrameView.hidden = YES;
    
    rightView.layer.cornerRadius = 5;
    
    lblName.text = [dicMatchesInfo objectForKey:@"nname"];
   
//    CGSize lblNameSize = [lblName.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:lblName.font,NSFontAttributeName, nil]];
//    lblName.frame = CGRectMake(120, 25, lblNameSize.width,lblNameSize.height);
//    
//    CGFloat xPos = lblName.frame.origin.x + lblName.frame.size.width;
    [lblName sizeToFit];
    
    lblAge.text = [NSString stringWithFormat:@", %@", [dicMatchesInfo objectForKey:@"age"]];
    CGRect frame = lblAge.frame;
//    frame.origin.x = xPos;
    lblAge.frame = frame;
    [lblAge sizeToFit];
    
    NSString*   dateDT = [dicMatchesInfo objectForKey:@"DateDT"];
    if( dateDT == nil || ![dateDT isKindOfClass:[NSString class]] )
    {
        lblDate.hidden = YES;
        lblDateTitle.hidden = YES;
        
        lblVenue.hidden = YES;
        lblVenueTitle.hidden = YES;
        
        // move status to venue position
        CGFloat toY = lblDateTitle.frame.origin.y;
        CGRect  frame = lblFeedBackDateTitle.frame;
        frame.origin.y = toY;
        lblFeedBackDateTitle.frame = frame;
        
        frame = lblFeedBackDate.frame;
        frame.origin.y = toY;
        lblFeedBackDate.frame = frame;
    }
    else
    {
        lblDate.hidden = NO;
        lblDateTitle.hidden = NO;
        lblVenue.hidden = NO;
        lblVenueTitle.hidden = NO;
        NSDate* date = [Global convertStringToDate:dateDT withFormat:@"yyyy-MM-dd HH:mm:ss"];
        lblDate.text = [Global getDateTime:date withFormat:@"dd MMM yyyy"];
    }
    
    [lblDate sizeToFit];
    [lblDateTitle sizeToFit];
    
    id venue = [dicMatchesInfo objectForKey:@"Venue"];
    if( venue != nil && [venue isKindOfClass:[NSString class]] )
        lblVenue.text = [dicMatchesInfo objectForKey:@"Venue"];
    else
        lblVenue.text = @"";
    
    [lblVenueTitle sizeToFit];
    [lblVenue sizeToFit];
    
    CGRect rect = lblVenue.frame;
    frame = lblFeedBackDateTitle.frame;
    frame.origin.y = MAX( frame.origin.y, rect.origin.y + rect.size.height + 2 );
    lblFeedBackDateTitle.frame = frame;
    
    frame = lblFeedBackDate.frame;
    frame.origin.y = MAX( frame.origin.y, rect.origin.y + rect.size.height + 2 );
    lblFeedBackDate.frame = frame;
    
    NSString*   feedbackDate = [dicMatchesInfo objectForKey:@"InputDT"];
    
    if( feedbackDate == nil || ![feedbackDate isKindOfClass:[NSString class]]||[feedbackDate isEqualToString:@""] )
    {
        lblFeedBackDate.hidden = YES;
        lblFeedBackDateTitle.hidden = YES;
        
        btnFeedBack.hidden = NO;
    }
    else
    {
        lblFeedBackDate.hidden = NO;
        lblFeedBackDateTitle.hidden = NO;
        
        btnFeedBack.hidden = YES;
        
        NSDate* date = [Global convertStringToDate:feedbackDate withFormat:@"yyyy-MM-dd HH:mm:ss"];
        lblFeedBackDate.text = [Global getDateTime:date withFormat:@"dd MMM yyyy"];
    }
    
    [lblFeedBackDate sizeToFit];
    [lblFeedBackDateTitle sizeToFit];
    
    NSString *strImage = nil;
    if( ![[_dicMatchesInfo objectForKey:@"view_photo"] isEqualToString:@"photo view not allowed."] ){
        strImage = [NSString stringWithFormat:@"%@%@", WEBSERVICE_PHOTO_BASEURI, [_dicMatchesInfo objectForKey:@"filename"]];
    }
    
    NSString *strPlaceholder;
    if( [eSyncronyAppDelegate sharedInstance].gender == 1 ){
        strPlaceholder = @"male1.png";
    }else {
        strPlaceholder = @"female1.png";
    }
    
    [photoImgView sd_setImageWithURL:[NSURL URLWithString:strImage] placeholderImage:[UIImage imageNamed:strPlaceholder] ];
    
//    if( [eSyncronyAppDelegate sharedInstance].gender == 1 )
//        photoImgView.image = [UIImage imageNamed:@"male1.png"];
//    
//    NSString    *imagePath = [_dicMatchesInfo objectForKey:@"localphoto_path"];
//    
//    if( imagePath != nil )
//    {
//        UIImage     *image = [UIImage imageWithContentsOfFile:imagePath];
//        if( image != nil )
//            photoImgView.image = image;
//    }
//    else
//    {
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,
//                                                 (unsigned long)NULL), ^(void) {
//            [_dicMatchesInfo retain];
//            if( [self downloadMatchUserPhoto] == FALSE )
//            {
//                [_dicMatchesInfo setObject:@"nophoto" forKey:@"localphoto_path"];
//            }
//            [dicMatchesInfo release];
//        });
//    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (IBAction)onClickFeedBack:(id)sender
{
    [self.pastDatesViewController didClickFeebbackWithInfo:_dicMatchesInfo];
}

- (void)dealloc
{
    [photoBGView release];
    [photoImgView release];
    [photoFrameView release];
    [rightView release];
    [lblName release];
    [lblAge release];
    [lblDate release];
    [lblVenue release];
    [lblFeedBackDate release];
    
    [lblDateTitle release];
    [lblVenueTitle release];
    [lblFeedBackDateTitle release];
    
    [super dealloc];
}
@end


#import <UIKit/UIKit.h>

@class PastDatesViewController;
@interface PastDatesCell : UITableViewCell
{
    IBOutlet UIImageView    *photoBGView;
    IBOutlet UIImageView    *photoImgView;
    IBOutlet UIImageView    *photoFrameView;
    
    IBOutlet UIImageView    *rightView;
    
    IBOutlet UILabel        *lblName;
    IBOutlet UILabel        *lblAge;
    IBOutlet UILabel        *lblDate;
    IBOutlet UILabel        *lblVenue;
    IBOutlet UILabel        *lblFeedBackDate;
    
    IBOutlet UILabel        *lblDateTitle;
    IBOutlet UILabel        *lblVenueTitle;
    IBOutlet UILabel        *lblFeedBackDateTitle;
    
    IBOutlet UIButton       *btnFeedBack;
    
    NSMutableDictionary     *_dicMatchesInfo;
}

@property (nonatomic, retain) PastDatesViewController *pastDatesViewController;

- (void)setMatchesInfo:(NSMutableDictionary*)dicMatchesInfo;

- (IBAction)onClickFeedBack:(id)sender;

@end

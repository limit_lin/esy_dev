
#import <UIKit/UIKit.h>
@class eSyncronyViewController;

@class MyMatchesProposedViewController;
@class PendingViewController;
@class ApprovedViewController;
@class CancelledViewController;
@class ReportViewController;
@class newDatesViewController;
@class PastDatesViewController;
@class FeedbackViewController;
@class DatingReportViewController;
@class ProfileViewController;
@class PersonalityViewController;
@class MyAccountViewController;
@class UpgradeViewController;
@class UpgradeSuccessViewController;
@class PhotoExchangeViewController;
@class GiveFeedBackViewController;
@class eSynchronyFilterViewController;
@class MyMatchesFavoritedViewController;//matches favorite list

@interface mainMenuViewController : UIViewController<UIAlertViewDelegate,UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet    UIScrollView    *mainScrl;
    IBOutlet    UIView          *leftMenuView;
    IBOutlet    UIView          *menuItemView;
    IBOutlet    UITableView     *listTblView;
    IBOutlet    UIView          *contentMaskView;
    
    IBOutlet    UIView          *rightMenuView;
    IBOutlet    UITableView     *ptblListView;
    IBOutlet    UIView          *pRightContentMaskView;
    
    UIViewController            *_curViewController;
    
    float       scrlOffsetX;
    float       leftMenuWidth;
    float       menuItemWidth;
    float       rightMenuWidth;
    float       scrlOffRightSetX;
    
    NSArray     *menuIconImgArray;
    NSArray     *menuIconTitleArray;//0623
   // NSMutableArray     *menuIconTitleArray;
    
    BOOL        isExpandedMatch;
    BOOL        isExpandedDate;
    BOOL        isExpandedProfile;
    
    BOOL        bInitSubViewFrames;
    BOOL        bInitialize;

    //////////////////////////////
    MyMatchesProposedViewController *myMatchProposeViewCtrl;
    PendingViewController           *pendingViewCtrl;
    ApprovedViewController          *approvedViewCtrl;
    CancelledViewController         *cancelledViewCtrl;
    ReportViewController            *reportViewCtrl;
    newDatesViewController          *newDateViewCtl;
    PastDatesViewController         *pastDatesViewCtrl;
    FeedbackViewController          *feedbackViewCtrl;
    DatingReportViewController      *datingReportViewCtrl;
    ProfileViewController           *profileViewCtrl;
    PersonalityViewController       *personViewCtrl;
    MyAccountViewController         *myAccountViewCtrl;
    UpgradeViewController           *upgradeViewCtrl;
    UpgradeSuccessViewController    *upgradeSuccessViewCtrl;
    PhotoExchangeViewController     *photoExchangeViewCtrl;
    GiveFeedBackViewController      *giveFeedback;
    eSynchronyFilterViewController  *filterViewCtrl;
    MyMatchesFavoritedViewController *myMatchFavoriteViewCtrl;
    
}

@property(nonatomic, strong)eSyncronyViewController* parentView;//151110

@property (nonatomic, assign) NSInteger curretItemIdx;

@property (nonatomic, strong) IBOutlet UIView   *containerView;
@property (nonatomic, strong) IBOutlet UILabel  *itemTitle;
@property (retain, nonatomic) IBOutlet UIButton *mainRightLogoBtn;
@property (retain, nonatomic) IBOutlet UIButton *mainMenuRightLogoBtn;

- (void)onServerConnectionError:(NSString*)error;

- (IBAction)didClickLogoBtn:(id)sender;
- (IBAction)didClickRightLogoBtn:(id)sender;


- (void)goMainMenu;
- (void)procItemClickEventWithID:(NSInteger)idx;

- (void)procProfile;
- (void)procPersonality;

- (void)procMyAccount:(BOOL)bPsdChangeMode;

- (void)procNewDates;
- (void)procUpgrade;
- (void)procUpgradeSuccess:(NSUInteger)nIndex packagePrice:(NSString *)price;
- (void)procGiveFeedback:(BOOL)nLove;
- (void)procPhotoExchange;
- (void)procMyMatchApproved;
- (void)procMyMatchProposed;
- (void)changeContentViewController:(UIViewController*)viewController;

@end

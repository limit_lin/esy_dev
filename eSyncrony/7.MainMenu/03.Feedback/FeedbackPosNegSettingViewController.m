
#import "FeedbackPosNegSettingViewController.h"
#import "RegisterTableViewCell.h"
#import "FeedbackSettingViewController.h"
#import "Global.h"

@interface FeedbackPosNegSettingViewController ()

@end

@implementation FeedbackPosNegSettingViewController

@synthesize itemArray, parent, titleString, tblView, isAllowMultySel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"FeedbackPosNegSettingView";
    selView.layer.cornerRadius = 5;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self initSetting];
}

- (void)dealloc
{
    [titleLabel release];
    [selView release];
    [tblView release];
    [deviderView release];
    [okBtn release];
    [okLabel release];
    [itemArray release];
    [titleString release];
    
    [parent release];
    
    [super dealloc];
}

- (void)initSetting
{
    //CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    //self.view.frame = CGRectMake(0, 0, 320, screenHeight);

    // remove selview
    for (UIView *v in [self.view subviews]) {
        if (v.tag == 100) {
            [v removeFromSuperview];
        }
    }
    
    titleLabel.text = self.titleString;

    tblView.allowsMultipleSelection = isAllowMultySel;
    
    // resizing of subview
    NSInteger itemCnt = [self.itemArray count];
    
    NSInteger showItemCnt = (itemCnt < 6)? itemCnt: 6;
    float      tblViewHeight;
    
    tblViewHeight = 42 * showItemCnt;

    tblView.frame = CGRectMake(tblView.frame.origin.x, tblView.frame.origin.y, tblView.frame.size.width, tblViewHeight);
    deviderView.frame = CGRectMake(deviderView.frame.origin.x, tblView.frame.origin.y + tblViewHeight + 3, deviderView.frame.size.width, 2);
    okBtn.frame = CGRectMake(okBtn.frame.origin.x, deviderView.frame.origin.y + 21, okBtn.frame.size.width, okBtn.frame.size.height);
    okLabel.frame = CGRectMake(okLabel.frame.origin.x, okBtn.frame.origin.y + 11, okLabel.frame.size.width, okLabel.frame.size.height);
    
    selView.frame = CGRectMake(20, (self.view.frame.size.height - okBtn.frame.origin.y)/2, selView.frame.size.width, okBtn.frame.origin.y + 44);
    
    // reloading of table
    [tblView reloadData];
    
    for (NSString *str in parent.editItemArray) {
        for (int i = 0; i < [itemArray count]; i++) {
            if ([str isEqualToString:[itemArray objectAtIndex:i]]) {
                [tblView selectRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
                break;
            }
        }
    }
    
    //
    
    [tblView setContentOffset:CGPointMake(0, 0)];
    
    // add subview
    [self.view performSelector:@selector(addSubview:) withObject:selView afterDelay:0.1];
}

#pragma mark --- Table Functions ---
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [self.itemArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 42;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RegisterTableViewCell *cell;
    NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"RegisterTableViewCell" owner:nil options:nil];
    cell = [arr objectAtIndex:0];
    cell.lbContent.text = [self.itemArray objectAtIndex:indexPath.row];
    cell.selectedItemArray = parent.editItemArray;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *selecedIndexPathArry = [tblView indexPathsForSelectedRows];

    if ([selecedIndexPathArry count] > 3) {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning",) message:NSLocalizedString(@"3 item can be selected.",@"FeedbackPosNegSettingView") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",) otherButtonTitles:nil, nil];
        [av show];
        [av release];
        
        [tblView deselectRowAtIndexPath:indexPath animated:NO];
    }
}

- (IBAction)didClickOKBtn:(id)sender
{
    NSArray *selecedIndexPathArry = [tblView indexPathsForSelectedRows];
    
    [parent.editItemArray removeAllObjects];
    
    NSIndexPath *indexPath;
    
    for (int i = 0; i < [selecedIndexPathArry count]; i++) {
        indexPath = [selecedIndexPathArry objectAtIndex:i];
        [parent.editItemArray addObject:[itemArray objectAtIndex:indexPath.row]];
    }
    
    [parent receiveEditResult];
    
    [self.view removeFromSuperview];
}


@end

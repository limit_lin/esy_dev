
#import "Global.h"
#import "FeedBackCell.h"
#import "FeedbackViewController.h"

@implementation FeedBackCell

@synthesize feedbackViewCtrl;

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)setMatchesInfo:(NSMutableDictionary*)dicMatchesInfo
{
    _dicMatchesInfo = dicMatchesInfo;
    
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
    
    lblMember.text = [NSString stringWithFormat:@"%@", [_dicMatchesInfo objectForKey:@"nname"]];
    
    NSString*   dateDT = [dicMatchesInfo objectForKey:@"DateDT"];
    if( dateDT == nil || [dateDT isEqualToString:@""] )
        lblDate.text = @"";
    else
    {
        NSDate* date = [Global convertStringToDate:dateDT withFormat:@"yyyy-MM-dd HH:mm:ss"];
        lblDate.text = [Global getDateTime:date withFormat:@"dd MMM yyyy hh:mm a"];
    }
}

- (IBAction)didClickFeedback:(id)sender
{
    [feedbackViewCtrl didClickFeebbackWithInfo:_dicMatchesInfo];
}

- (IBAction)didClickName:(id)sender
{
    [feedbackViewCtrl didClickNameWithInfo:_dicMatchesInfo];
}

-(void)dealloc
{
    [lblMember release];
    [lblDate release];
    
    [super dealloc];
}

@end

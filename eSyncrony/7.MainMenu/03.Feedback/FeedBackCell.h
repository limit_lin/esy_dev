
#import <UIKit/UIKit.h>

@class FeedbackViewController;

@interface FeedBackCell : UITableViewCell
{
    IBOutlet UILabel        *lblMember;
    IBOutlet UILabel        *lblDate;
    
    NSMutableDictionary     *_dicMatchesInfo;
}

@property (nonatomic, strong) FeedbackViewController *feedbackViewCtrl;

- (void)setMatchesInfo:(NSMutableDictionary*)dicMatchesInfo;

- (IBAction)didClickFeedback:(id)sender;
- (IBAction)didClickName:(id)sender;

@end

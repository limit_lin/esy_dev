
#import "FeedbackSettingViewController.h"
#import "FeedbackPosNegSettingViewController.h"
#import "RadioboxViewController.h"
#import "FeedbackFriendRadioGroupViewController.h"
#import "DSActivityView.h"
#import "Global.h"

#import "mainMenuViewController.h"
#import "eSyncronyAppDelegate.h"
#import "FeedbackViewController.h"
#import "PastDatesViewController.h"
@interface FeedbackSettingViewController ()

@end

@implementation FeedbackSettingViewController

@synthesize positiveLabel;
@synthesize negativeLabel;
@synthesize restaurantLabel;
@synthesize considerLabel;
@synthesize presentLabel;
@synthesize confidenceLabel;
@synthesize interestLabel;
@synthesize conversLabel;
@synthesize diningLabel;
@synthesize contactLabel;
@synthesize lastLabel;
@synthesize experiencLabel;
@synthesize feelLabel;
@synthesize secondDateLabel;
@synthesize itemArray;
@synthesize editItemArray, friendArray, radioViewArray;
@synthesize editItemType;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"FeedbackSettingView";
    
    aspectTv.delegate = self;
    feedbackTv.delegate = self;

    settingViewCtrl = [[FeedbackPosNegSettingViewController alloc] initWithNibName:@"FeedbackPosNegSettingViewController" bundle:nil];
    radioboxViewCtrl = [[RadioboxViewController alloc] initWithNibName:@"RadioboxViewController" bundle:nil];
    
    editItemArray = [[NSMutableArray alloc] init];
    itemArray = [[NSMutableArray alloc] init];
    friendArray = [[NSMutableArray alloc] init];
    radioViewArray = [[NSMutableArray alloc] init];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    radioboxViewCtrl.view.frame = self.view.bounds;
    settingViewCtrl.view.frame = self.view.bounds;

    [self refreshContent];
}

- (void)refreshContent
{
    [self showBusyDialogWithTitle:NSLocalizedString(@"Loading...",)];
    //[DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(loadDatesInfo) withObject:nil afterDelay:0.01f];
}

- (void)procVerifyAccountError:(NSString *)error
{
    if ([error isEqualToString:@"ERROR 0"]) {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error",) message:NSLocalizedString(@"Empty parameter value.",) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",) otherButtonTitles:nil, nil];
        [av show];
        [av release];
    }
    else if ([error isEqualToString:@"ERROR 1"]) {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error",) message:NSLocalizedString(@"Account does not exist.",) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",) otherButtonTitles:nil, nil];
        [av show];
        [av release];
    }
    else if ([error isEqualToString:@"ERROR 2"]) {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"eSynchrony" message:NSLocalizedString(@"User has not logged in.",) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",) otherButtonTitles:nil, nil];
        [av show];
        [av release];
    }
    else if ([error isEqualToString:@"ERROR 3"]) {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"eSynchrony" message:NSLocalizedString(@"Token key does not match.",) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",) otherButtonTitles:nil, nil];
        [av show];
        [av release];
    }else
    {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"eSynchrony" message:NSLocalizedString(@"Unkown error",) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",) otherButtonTitles:nil, nil];
        [av show];
        [av release];
    }
}


- (void) loadDatesInfo
{
    //[DSBezelActivityView removeViewAnimated:NO];
    [self hideBusyDialog];
    NSDictionary* result = [UtilComm viewDates:@"4"];
    
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Error"];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"ERROR",)];
        return;
    }
    
    NSDictionary*   responseDic = (NSDictionary*)response;
    if( ![responseDic isKindOfClass:[NSDictionary class]] )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"ERROR",)];
        return;
    }
    
    [arrDatesItems release];
    arrDatesItems = [[NSMutableArray alloc] initWithCapacity:0];
    
    NSArray *arrItems = [responseDic objectForKey:@"item"];
    
    NSString *str = [NSString stringWithFormat:@"%@", response];
    NSRange range = [str rangeOfString:@");"];
    
    if (range.location == NSNotFound)
    {
        [arrDatesItems addObject:arrItems];
    }
    else
    {
        if ([arrItems isKindOfClass:[NSArray class]]) {
            for( int i = 0; i < [arrItems count]; i++ )
            {
                NSDictionary        *item = [arrItems objectAtIndex:i];
                NSMutableDictionary *mutItem = [NSMutableDictionary dictionaryWithDictionary:item];
                
                [arrDatesItems addObject:mutItem];
            }
        }
        if ([arrItems isKindOfClass:[NSDictionary class]]) {
            NSDictionary        *item = (NSDictionary *)arrItems;
            NSMutableDictionary *mutItem = [NSMutableDictionary dictionaryWithDictionary:item];
            [arrDatesItems addObject:mutItem];
        }
        
    }
    
    [self initSetting];
}

- (void)initSetting
{
    positiveLabel.text = @"";
    negativeLabel.text = @"";
    
    NSString *infoStr = NSLocalizedString(@"Select your answer here",@"FeedbackSettingView");
    
    restaurantLabel.text = infoStr;
    considerLabel.text = infoStr;
    presentLabel.text = infoStr;
    confidenceLabel.text = infoStr;
    interestLabel.text = infoStr;
    conversLabel.text = infoStr;
    diningLabel.text = infoStr;
    contactLabel.text = infoStr;
    lastLabel.text = infoStr;
    experiencLabel.text = infoStr;
    feelLabel.text = infoStr;
    secondDateLabel.text = infoStr;
    
    isCheck = NO;
    [checkBtn setSelected:NO];
    
    // 어디선가 받아야 함
    [friendArray removeAllObjects];
    for (int i = 0; i < arrDatesItems.count; i ++)
    {
        [friendArray addObject:[arrDatesItems[i] objectForKey:@"nname"]];
        
//        if ([[arrDatesItems[i] objectForKey:@"Acc_no"]isEqualToString:[self.curDicInfo objectForKey:@"Acc_no"]]) {
//            [friendArray addObject:@"match"];
//        }else{
//            [friendArray addObject:[arrDatesItems[i] objectForKey:@"nname"]];
//        }
        
    }
    
    if (arrDatesItems.count>1) {
        [self insertRadioGroup];
        
    }else {
        contentViewHeight = contentsView.frame.size.height;
    }
    
    
    [scrlView addSubview:contentsView];
    scrlView.contentSize = contentsView.frame.size;
    
    contentsView.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [contentsView addGestureRecognizer:singleFingerTap];
    [singleFingerTap release];
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    [aspectTv resignFirstResponder];
    [feedbackTv resignFirstResponder];
    //Do stuff here...
}

- (void)releaseRadioViewArray
{
    NSInteger friendCnt = [radioViewArray count], i;
    
    if (friendCnt == 0)
        return;
    
    for (i = 0; i < friendCnt; i++) {
        id rv = [radioViewArray objectAtIndex:i];
        [rv release];
    }

    [radioViewArray removeAllObjects];
}

- (void)insertRadioGroup
{
    NSInteger friendCnt = [friendArray count], i;
    
    [self releaseRadioViewArray];
    
    float   y = 1390;
    
    contentsView.frame = CGRectMake(0, 0, 320, y + 46 * friendCnt + 330 + 100);
    
    for (i = 0; i < friendCnt; i++) {
        FeedbackFriendRadioGroupViewController *rv = [[FeedbackFriendRadioGroupViewController alloc] initWithNibName:@"FeedbackFriendRadioGroupViewController" bundle:nil];
        
//        NSLog(@"name = %@", [friendArray objectAtIndex:i]);
        
        rv.nameStr = [friendArray objectAtIndex:i];
        rv.idx = i;
        rv.parent = self;

        rv.view.frame = CGRectMake(0, y, 320, 46);
        
        [radioViewArray addObject:rv];
        
        y += 46;
        if (rv.nameStr.length>0) {
            [contentsView addSubview:rv.view];
        }
        
    }
    
    y += 9;
    devideBar_1.frame = CGRectMake(devideBar_1.frame.origin.x, y, devideBar_1.frame.size.width, devideBar_1.frame.size.height);
    y += 20;
    additionalLabel.frame = CGRectMake(additionalLabel.frame.origin.x, y, additionalLabel.frame.size.width, additionalLabel.frame.size.height);
    y += 20;
    areThereLabel.frame = CGRectMake(areThereLabel.frame.origin.x, y, areThereLabel.frame.size.width, areThereLabel.frame.size.height);
    y += 20;
    feedbackBg.frame = CGRectMake(feedbackBg.frame.origin.x, y, feedbackBg.frame.size.width, feedbackBg.frame.size.height);
    feedbackTv.frame = CGRectMake(feedbackTv.frame.origin.x, y, feedbackTv.frame.size.width, feedbackTv.frame.size.height);
    y += 148;
    checkBtn.frame = CGRectMake(checkBtn.frame.origin.x, y, checkBtn.frame.size.width, checkBtn.frame.size.height);
    y += 7;
    IwouldLabel.frame = CGRectMake(IwouldLabel.frame.origin.x, y, IwouldLabel.frame.size.width, IwouldLabel.frame.size.height);
    y += 35;
    devideBar_2.frame = CGRectMake(devideBar_2.frame.origin.x, y, devideBar_2.frame.size.width, 1);
    y += 15;
    updateBtn.frame = CGRectMake(updateBtn.frame.origin.x, y, updateBtn.frame.size.width, updateBtn.frame.size.height);
    y += 7;
    updateBtnLabel.frame = CGRectMake(updateBtnLabel.frame.origin.x, y, updateBtnLabel.frame.size.width, updateBtnLabel.frame.size.height);
    y += 45;
    
    contentViewHeight = y;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [scrlView release];
    [contentsView release];

    [aspectTv release];
    [positiveBtn release];
    [negativeBtn release];
    [additionalLabel release];
    [areThereLabel release];
    [IwouldLabel release];
    [checkBtn release];
    [updateBtn release];
    [updateBtnLabel release];
    [devideBar_1 release];
    [devideBar_2 release];
    [feedbackBg release];
    [feedbackTv release];
    
    [positiveLabel release];
    [negativeLabel release];
    [restaurantLabel release];
    [considerLabel release];
    [presentLabel release];
    [confidenceLabel release];
    [interestLabel release];
    [conversLabel release];
    [diningLabel release];
    [contactLabel release];
    [lastLabel release];
    [experiencLabel release];
    [feelLabel release];
    [secondDateLabel release];
    [itemArray release];
    [editItemArray release];
    
    [friendArray release];
    
    [self releaseRadioViewArray];
    [radioViewArray release];
    
    [settingViewCtrl release];
    
    [super dealloc];
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if (textView.tag == 100) {
        [scrlView scrollRectToVisible:CGRectMake(0, 990, 320, 415) animated:YES];
    }
    else{
        
        //self.view.center = CGPointMake(160, 40);
        if (arrDatesItems.count>1) {
            [scrlView scrollRectToVisible:CGRectMake(0, contentViewHeight+415, 320, 415) animated:YES];
        }else{
            [scrlView setContentOffset:CGPointMake(0, feedbackTv.frame.origin.y-50) animated:YES];
        }
        
    }
    
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    if (textView.tag == 100) {
        //[scrlView scrollRectToVisible:CGRectMake(0, 990, 320, 415) animated:YES];
    }
    else{
        [scrlView scrollRectToVisible:CGRectMake(0, contentViewHeight-415, 320, 415) animated:YES];
        //self.view.center = CGPointMake(160, 230);
    }
    return YES;
}

- (void)loadItemArrayWithName:(NSString *)name
{
    NSString *_strPath =[[NSBundle mainBundle] pathForResource:name ofType:@"plist"];
    NSDictionary *_dicCountry = [NSDictionary dictionaryWithContentsOfFile:_strPath];
    
    [itemArray removeAllObjects];    
    [itemArray addObjectsFromArray:[_dicCountry objectForKey:@"Array"]];
    
    [editItemArray removeAllObjects];
    
    NSString *str = nil;
    switch (editItemType) {
        case FEEDBACK_POSITIVE:
            str = positiveLabel.text;
            break;
        case FEEDBACK_NEGATIVE:
            str = negativeLabel.text;
            break;
        case FEEDBACK_RESTURANT:
            str = restaurantLabel.text;
            break;
        case FEEDBACK_CONSIDER:
            str = considerLabel.text;
            break;
        case FEEDBACK_PRESENT:
            str = presentLabel.text;
            break;
        case FEEDBACK_CONFIDENCE:
            str = confidenceLabel.text;
            break;
        case FEEDBACK_INTEREST:
            str = interestLabel.text;
            break;
        case FEEDBACK_CONVERSATION:
            str = conversLabel.text;
            break;
        case FEEDBACK_DINING:
            str = diningLabel.text;
            break;
        case FEEDBACK_CONTACT:
            str = contactLabel.text;
            break;
        case FEEDBACK_LAST:
            str = lastLabel.text;
            break;
        case FEEDBACK_EXPERIENC:
            str = experiencLabel.text;
            break;
        case FEEDBACK_FEEL:
            str = feelLabel.text;
            break;
        case FEEDBACK_SECONDDATE:
            str = secondDateLabel.text;
            break;
    }
    
    if (str && [str length] > 0)
        [editItemArray addObjectsFromArray:[str componentsSeparatedByString:@", "]];
}

- (void)receiveEditResult
{
    NSString *str;
    NSInteger i, editItemCnt;
    
    editItemCnt = [editItemArray count];
    if (editItemCnt == 0) {
        str = @"";
    }
    else {
        str = [editItemArray objectAtIndex:0];
        
        for (i = 1; i < editItemCnt; i++) {
            str = [NSString stringWithFormat:@"%@, %@", str, [editItemArray objectAtIndex:i]];
        }
    }
    
    switch (editItemType) {
        case FEEDBACK_POSITIVE:
            positiveLabel.text = str;
            break;
        case FEEDBACK_NEGATIVE:
            negativeLabel.text = str;
            break;
        case FEEDBACK_RESTURANT:
            restaurantLabel.text = str;
            break;
        case FEEDBACK_CONSIDER:
            considerLabel.text = str;
            break;
        case FEEDBACK_PRESENT:
            presentLabel.text = str;
            break;
        case FEEDBACK_CONFIDENCE:
            confidenceLabel.text = str;
            break;
        case FEEDBACK_INTEREST:
            interestLabel.text = str;
            break;
        case FEEDBACK_CONVERSATION:
            conversLabel.text = str;
            break;
        case FEEDBACK_DINING:
            diningLabel.text = str;
            break;
        case FEEDBACK_CONTACT:
            contactLabel.text = str;
            break;
        case FEEDBACK_LAST:
            lastLabel.text = str;
            break;
        case FEEDBACK_EXPERIENC:
            experiencLabel.text = str;
            break;
        case FEEDBACK_FEEL:
            feelLabel.text = str;
            break;
        case FEEDBACK_SECONDDATE:
            secondDateLabel.text = str;
            break;
    }
}   

- (void)addSettingView:(UIButton *)button
{
    [settingViewCtrl initSetting];
    
    [button setHighlighted:NO];

    [self.view performSelector:@selector(addSubview:) withObject:settingViewCtrl.view afterDelay:0.1];
}

- (IBAction)didClickBackbtn:(id)sender
{
    [aspectTv resignFirstResponder];
    [feedbackTv resignFirstResponder];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)didClickUpdateBtn:(id)sender
{
    [aspectTv resignFirstResponder];
    [feedbackTv resignFirstResponder];
    
    [self GiveFeedBack];
}

- (IBAction)didClickPositiveBtn:(id)sender
{
    [aspectTv resignFirstResponder];
    [feedbackTv resignFirstResponder];

    self.editItemType = FEEDBACK_POSITIVE;
    [self loadItemArrayWithName:@"positive_list"];
    
    settingViewCtrl.itemArray = itemArray;
    settingViewCtrl.parent = self;
    settingViewCtrl.titleString = NSLocalizedString(@"My Positives are", @"FeedbackSettingView");
    settingViewCtrl.isAllowMultySel = YES;
    
    [positiveBtn setHighlighted:YES];
    
    [self performSelector:@selector(addSettingView:) withObject:positiveBtn afterDelay:0.1];
}

- (IBAction)didClickNegativeBtn:(id)sender
{
    [aspectTv resignFirstResponder];
    [feedbackTv resignFirstResponder];

    self.editItemType = FEEDBACK_NEGATIVE;
    [self loadItemArrayWithName:@"negative_list"];
    
    settingViewCtrl.itemArray = itemArray;
    settingViewCtrl.parent = self;
    settingViewCtrl.titleString = NSLocalizedString(@"My Negatives are", @"FeedbackSettingView");
    settingViewCtrl.isAllowMultySel = YES;
    
    [negativeBtn setHighlighted:YES];
    
    [self performSelector:@selector(addSettingView:) withObject:negativeBtn afterDelay:0.1];
}

- (IBAction)didClickRestaurBtn:(id)sender
{
    [aspectTv resignFirstResponder];
    [feedbackTv resignFirstResponder];
    
    self.editItemType = FEEDBACK_RESTURANT;
    [self loadItemArrayWithName:@"order_list"];
    
    radioboxViewCtrl.itemArray = itemArray;
    radioboxViewCtrl.parent = self;

    [radioboxViewCtrl initSetting];
    
    [self.view performSelector:@selector(addSubview:) withObject:radioboxViewCtrl.view afterDelay:0.1];
}

- (IBAction)didClickConsiderBtn:(id)sender
{
    [aspectTv resignFirstResponder];
    [feedbackTv resignFirstResponder];
    
    self.editItemType = FEEDBACK_CONSIDER;
    [self loadItemArrayWithName:@"order_list"];
    
    radioboxViewCtrl.itemArray = itemArray;
    radioboxViewCtrl.parent = self;
    
    [radioboxViewCtrl initSetting];
    
    [self.view performSelector:@selector(addSubview:) withObject:radioboxViewCtrl.view afterDelay:0.1];
}

- (IBAction)didClickPresentBtn:(id)sender
{
    [aspectTv resignFirstResponder];
    [feedbackTv resignFirstResponder];
    
    self.editItemType = FEEDBACK_PRESENT;
    [self loadItemArrayWithName:@"order_list"];
    
    radioboxViewCtrl.itemArray = itemArray;
    radioboxViewCtrl.parent = self;
    
    [radioboxViewCtrl initSetting];
    
    [self.view performSelector:@selector(addSubview:) withObject:radioboxViewCtrl.view afterDelay:0.1];
}

- (IBAction)didClickConfidenceBtn:(id)sender
{
    [aspectTv resignFirstResponder];
    [feedbackTv resignFirstResponder];
    
    self.editItemType = FEEDBACK_CONFIDENCE;
    [self loadItemArrayWithName:@"order_list"];
    
    radioboxViewCtrl.itemArray = itemArray;
    radioboxViewCtrl.parent = self;
    
    [radioboxViewCtrl initSetting];
    
    [self.view performSelector:@selector(addSubview:) withObject:radioboxViewCtrl.view afterDelay:0.1];
}

- (IBAction)didClickInterestBtn:(id)sender
{
    [aspectTv resignFirstResponder];
    [feedbackTv resignFirstResponder];
    
    self.editItemType = FEEDBACK_INTEREST;
    [self loadItemArrayWithName:@"order_list"];
    
    radioboxViewCtrl.itemArray = itemArray;
    radioboxViewCtrl.parent = self;
    
    [radioboxViewCtrl initSetting];
    
    [self.view performSelector:@selector(addSubview:) withObject:radioboxViewCtrl.view afterDelay:0.1];
}

- (IBAction)didClickConversBtn:(id)sender
{
    [aspectTv resignFirstResponder];
    [feedbackTv resignFirstResponder];
    
    self.editItemType = FEEDBACK_CONVERSATION;
    [self loadItemArrayWithName:@"order_list"];
    
    radioboxViewCtrl.itemArray = itemArray;
    radioboxViewCtrl.parent = self;
    
    [radioboxViewCtrl initSetting];
    
    [self.view performSelector:@selector(addSubview:) withObject:radioboxViewCtrl.view afterDelay:0.1];
}

- (IBAction)didClickDiningBtn:(id)sender
{
    [aspectTv resignFirstResponder];
    [feedbackTv resignFirstResponder];
    
    self.editItemType = FEEDBACK_DINING;
    [self loadItemArrayWithName:@"order_list"];
    
    radioboxViewCtrl.itemArray = itemArray;
    radioboxViewCtrl.parent = self;
    
    [radioboxViewCtrl initSetting];
    
    [self.view performSelector:@selector(addSubview:) withObject:radioboxViewCtrl.view afterDelay:0.1];
}

- (IBAction)didClickContactBtn:(id)sender
{
    [aspectTv resignFirstResponder];
    [feedbackTv resignFirstResponder];
    
    self.editItemType = FEEDBACK_CONTACT;
    [self loadItemArrayWithName:@"boolean_list"];
    
    radioboxViewCtrl.itemArray = itemArray;
    radioboxViewCtrl.parent = self;
    
    [radioboxViewCtrl initSetting];
    
    [self.view performSelector:@selector(addSubview:) withObject:radioboxViewCtrl.view afterDelay:0.1];
}

- (IBAction)didClickLastBtn:(id)sender
{
    [aspectTv resignFirstResponder];
    [feedbackTv resignFirstResponder];
    
    self.editItemType = FEEDBACK_LAST;
    [self loadItemArrayWithName:@"time_list"];
    
    radioboxViewCtrl.itemArray = itemArray;
    radioboxViewCtrl.parent = self;
    
    [radioboxViewCtrl initSetting];
    
    [self.view performSelector:@selector(addSubview:) withObject:radioboxViewCtrl.view afterDelay:0.1];
}

- (IBAction)didClickExperiencBtn:(id)sender
{
    [aspectTv resignFirstResponder];
    [feedbackTv resignFirstResponder];
    
    self.editItemType = FEEDBACK_EXPERIENC;
    [self loadItemArrayWithName:@"order_list"];
    
    radioboxViewCtrl.itemArray = itemArray;
    radioboxViewCtrl.parent = self;
    
    [radioboxViewCtrl initSetting];
    
    [self.view performSelector:@selector(addSubview:) withObject:radioboxViewCtrl.view afterDelay:0.1];
}

- (IBAction)didClickFeelBtn:(id)sender
{
    [aspectTv resignFirstResponder];
    [feedbackTv resignFirstResponder];
    
    self.editItemType = FEEDBACK_FEEL;
    [self loadItemArrayWithName:@"boolean_list"];
    
    radioboxViewCtrl.itemArray = itemArray;
    radioboxViewCtrl.parent = self;
    
    [radioboxViewCtrl initSetting];
    
    [self.view performSelector:@selector(addSubview:) withObject:radioboxViewCtrl.view afterDelay:0.1];
}

- (IBAction)didClickSecondDateBtn:(id)sender
{
    [aspectTv resignFirstResponder];
    [feedbackTv resignFirstResponder];
    
    self.editItemType = FEEDBACK_SECONDDATE;
    [self loadItemArrayWithName:@"boolean_list"];
    
    radioboxViewCtrl.itemArray = itemArray;
    radioboxViewCtrl.parent = self;
    
    [radioboxViewCtrl initSetting];
    
    [self.view performSelector:@selector(addSubview:) withObject:radioboxViewCtrl.view afterDelay:0.1];
}

- (IBAction)didClickCheckBtn:(id)sender
{
    isCheck = !isCheck;
    [checkBtn setSelected:isCheck];
}

- (void)procFreindRadioBtn:(NSInteger)idx
{
    [aspectTv resignFirstResponder];
    [feedbackTv resignFirstResponder];
    
    for (int i = 0; i < [radioViewArray count]; i++) {
        FeedbackFriendRadioGroupViewController *rv = [radioViewArray objectAtIndex:i];
        
        UIImage *img;
        
        if (i == idx) {
            img = [UIImage imageNamed:@"btnRadio_p"];
            rv.bIsSel = YES;
        }
        else {
            img = [UIImage imageNamed:@"btnRadio_n"];
            rv.bIsSel = NO;
        }
        
        [rv.radioBtnImg setImage:img];
    }
}

- (void)GiveFeedBack
{
    if ([positiveLabel.text isEqualToString: @""])
    {
        [scrlView scrollRectToVisible:CGRectMake(0, 0, 320, 415) animated:YES];
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Field Required",) message:NSLocalizedString(@"Please select your Positive.",@"FeedbackSettingView") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",) otherButtonTitles:nil];
        [av show];
        [av release];
        return;
    }
    NSString *strPositive = @"1,2,3";
    
    if ([negativeLabel.text isEqualToString:@""])
    {
        [scrlView scrollRectToVisible:CGRectMake(0, 0, 320, 415) animated:YES];
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Field Required",) message:NSLocalizedString(@"Please select your Negative.",@"FeedbackSettingView") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",) otherButtonTitles:nil];
        [av show];
        [av release];
        return;
    }
    
    if ([restaurantLabel.text isEqualToString:@""] || [restaurantLabel.text hasPrefix:@"Select"])
    {
        [scrlView scrollRectToVisible:CGRectMake(0, 0, 320, 415) animated:YES];
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Field Required",) message:NSLocalizedString(@"Please select the restaurant overall.",@"FeedbackSettingView") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",) otherButtonTitles:nil];
        [av show];
        [av release];
        return;
    }
    
    if ([considerLabel.text isEqualToString:@""] || [considerLabel.text hasPrefix:@"Select"])
    {
        [scrlView scrollRectToVisible:CGRectMake(0, considerLabel.frame.origin.y - 50, 320, 415) animated:YES];
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Field Required",) message:NSLocalizedString(@"Please select consideration.",@"FeedbackSettingView") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",) otherButtonTitles:nil];
        [av show];
        [av release];
        return;
    }
    
    if ([presentLabel.text isEqualToString:@""] || [presentLabel.text hasPrefix:@"Select"])
    {
        [scrlView scrollRectToVisible:CGRectMake(0, presentLabel.frame.origin.y - 50, 320, 415) animated:YES];
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Field Required",) message:NSLocalizedString(@"Please select presentation.",@"FeedbackSettingView") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",) otherButtonTitles:nil];
        [av show];
        [av release];
        return;
    }
    
    if ([confidenceLabel.text isEqualToString:@""] || [confidenceLabel.text hasPrefix:@"Select"])
    {
        [scrlView scrollRectToVisible:CGRectMake(0, confidenceLabel.frame.origin.y - 50, 320, 415) animated:YES];
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Field Required",) message:NSLocalizedString(@"Please select confidence.",@"FeedbackSettingView") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",) otherButtonTitles:nil];
        [av show];
        [av release];
        return;
    }
    
    if ([interestLabel.text isEqualToString:@""] || [interestLabel.text hasPrefix:@"Select"])
    {
        [scrlView scrollRectToVisible:CGRectMake(0, interestLabel.frame.origin.y - 50, 320, 415) animated:YES];
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Field Required",) message:NSLocalizedString(@"Please select interest level.",@"FeedbackSettingView") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",) otherButtonTitles:nil];
        [av show];
        [av release];
        return;
    }
    
    if ([conversLabel.text isEqualToString:@""] || [conversLabel.text hasPrefix:@"Select"])
    {
        [scrlView scrollRectToVisible:CGRectMake(0, conversLabel.frame.origin.y - 50, 320, 415) animated:YES];
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Field Required",) message:NSLocalizedString(@"Please select conversation.",@"FeedbackSettingView") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",) otherButtonTitles:nil];
        [av show];
        [av release];
        return;
    }
    
    if ([diningLabel.text isEqualToString:@""] || [diningLabel.text hasPrefix:@"Select"])
    {
        [scrlView scrollRectToVisible:CGRectMake(0, diningLabel.frame.origin.y - 50, 320, 415) animated:YES];
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Field Required",) message:NSLocalizedString(@"Please select dining etiquette.",@"FeedbackSettingView") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",) otherButtonTitles:nil];
        [av show];
        [av release];
        return;
    }
    
    if ([contactLabel.text isEqualToString:@""] || [contactLabel.text hasPrefix:@"Select"])
    {
        [scrlView scrollRectToVisible:CGRectMake(0, contactLabel.frame.origin.y - 50, 320, 415) animated:YES];
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Field Required",) message:NSLocalizedString(@"Please select goodbyes1." ,@"FeedbackSettingView")delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",) otherButtonTitles:nil];
        [av show];
        [av release];
        return;
    }
    
    if ([lastLabel.text isEqualToString:@""] || [lastLabel.text hasPrefix:@"Select"])
    {
        [scrlView scrollRectToVisible:CGRectMake(0, lastLabel.frame.origin.y - 50, 320, 415) animated:YES];
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Field Required",) message:NSLocalizedString(@"Please select goodbye2.",@"FeedbackSettingView") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",) otherButtonTitles:nil];
        [av show];
        [av release];
        return;
    }
    
    if ([experiencLabel.text isEqualToString:@""] || [experiencLabel.text hasPrefix:@"Select"])
    {
        [scrlView scrollRectToVisible:CGRectMake(0, experiencLabel.frame.origin.y - 50, 320, 415) animated:YES];
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Field Required",) message:NSLocalizedString(@"Please select overall experience.",@"FeedbackSettingView") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",) otherButtonTitles:nil];
        [av show];
        [av release];
        return;
    }
    
    if ([aspectTv.text isEqualToString:@""])
    {
        [scrlView scrollRectToVisible:CGRectMake(0, aspectTv.frame.origin.y - 50, 320, 415) animated:YES];
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Field Required",) message:NSLocalizedString(@"Please Input Overall Text.",) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",) otherButtonTitles:nil];
        [av show];
        [av release];
        return;
    }
    
    if ([feelLabel.text isEqualToString:@""] || [feelLabel.text hasPrefix:@"Select"])
    {
        [scrlView scrollRectToVisible:CGRectMake(0, feelLabel.frame.origin.y - 50, 320, 415) animated:YES];
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Field Required",) message:NSLocalizedString(@"Please select afterthoughts1",@"FeedbackSettingView") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",) otherButtonTitles:nil];
        [av show];
        [av release];
        return;
    }
    
    if ([secondDateLabel.text isEqualToString:@""] || [secondDateLabel.text hasPrefix:@"Select"])
    {
        [scrlView scrollRectToVisible:CGRectMake(0, secondDateLabel.frame.origin.y - 50, 320, 415) animated:YES];
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Field Required",) message:NSLocalizedString(@"Please select afterthoughts2",@"FeedbackSettingView") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",) otherButtonTitles:nil];
        [av show];
        [av release];
        return;
    }
    
    NSString *strRadioText = nil;
    for (int i = 0; i < radioViewArray.count; i ++)
    {
        FeedbackFriendRadioGroupViewController *rv = radioViewArray[i];
        
        if (rv.bIsSel == YES)
            strRadioText = [arrDatesItems[i] objectForKey:@"Acc_no"];
    }
    if (arrDatesItems.count==1) {
        strRadioText = @"";
    }
    if( strRadioText == nil )
    {
        [scrlView scrollRectToVisible:CGRectMake(0, devideBar_1.frame.origin.y - 100, 320, 415) animated:YES];
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Field Required",) message:NSLocalizedString(@"Please select the 2nd Date",@"FeedbackSettingView") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",) otherButtonTitles:nil];
        [av show];
        [av release];
        return;
    }

    if ([feedbackTv.text isEqualToString:@""])
    {
        [scrlView scrollRectToVisible:CGRectMake(0, feedbackTv.frame.origin.y - 50, 320, 415) animated:YES];
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Field Required",) message:NSLocalizedString(@"Please Input Additional Feedback.",@"FeedbackSettingView") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",) otherButtonTitles:nil];
        [av show];
        [av release];
        return;
    }
    
    NSString *strCheck = @"0";
    if (isCheck == YES)
        strCheck = @"1";
    
    NSString *strMaccno = [self.curDicInfo objectForKey:@"Acc_no"];
    if (strMaccno==nil) {
        strMaccno = [self.curDicInfo objectForKey:@"acc_no"];
    }
    
    
    NSDictionary* result = [UtilComm giveFeedback:strMaccno Param1:strPositive Param2:strPositive Param3:restaurantLabel.text Param4:considerLabel.text Param5:presentLabel.text Param6:confidenceLabel.text Param7:diningLabel.text Param8:conversLabel.text Param9:interestLabel.text Param10:contactLabel.text Param11:lastLabel.text Param12:aspectTv.text Param13:experiencLabel.text Param14:feelLabel.text Param15:secondDateLabel.text Param16:strRadioText Param17:feedbackTv.text Param18:strCheck];
    
    id response = [result objectForKey:@"response"];
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [self procVerifyAccountError:response];
        return;
    }
    
    if ([response hasPrefix:@"OK"])
    {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"eSynchrony"
                                                     message:NSLocalizedString(@"Thank you for your feedback.",@"FeedbackSettingView")
                                                    delegate:self
                                           cancelButtonTitle:NSLocalizedString(@"OK",)
                                           otherButtonTitles:nil];
        [av show];
        [av release];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if( [alertView.title isEqualToString:@"eSynchrony"] )
    {
        [aspectTv resignFirstResponder];
        [feedbackTv resignFirstResponder];

        [self.navigationController popViewControllerAnimated:YES];
        if (self.feedbackDelegate!=nil) {
            [self.feedbackDelegate refreshContent];
            return;
        }
        if (self.pastDatesDelegate!=nil) {
            [self.pastDatesDelegate refreshContent];
            return;
        }
    }
}

@end

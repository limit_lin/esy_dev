
#import <UIKit/UIKit.h>

@class FeedbackSettingViewController;

@interface FeedbackFriendRadioGroupViewController : UIViewController
{
}
@property (nonatomic, strong) IBOutlet UIImageView    *radioBtnImg;
@property (nonatomic, strong) IBOutlet UILabel        *nameLabel;
@property (nonatomic, strong) IBOutlet NSString        *nameStr;
@property (nonatomic, strong) FeedbackSettingViewController *parent;
@property (nonatomic) NSInteger     idx;
@property (nonatomic) BOOL  bIsSel;

- (IBAction)didClickBtn:(id)sender;

@end

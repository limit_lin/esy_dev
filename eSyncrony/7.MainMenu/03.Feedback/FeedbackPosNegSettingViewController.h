
#import <UIKit/UIKit.h>

@class FeedbackSettingViewController;

@interface FeedbackPosNegSettingViewController : GAITrackedViewController
{
    IBOutlet UIView         *selView;
    IBOutlet UIImageView    *deviderView;
    IBOutlet UIButton       *okBtn;
    IBOutlet UILabel        *okLabel;
    IBOutlet UILabel        *titleLabel;
}

@property (nonatomic, strong) NSMutableArray *itemArray;
@property (nonatomic, strong) FeedbackSettingViewController *parent;
@property (nonatomic, strong) NSString *titleString;
@property (nonatomic, strong) IBOutlet UITableView *tblView;
@property (nonatomic) BOOL isAllowMultySel;

- (IBAction)didClickOKBtn:(id)sender;

- (void)initSetting;

@end

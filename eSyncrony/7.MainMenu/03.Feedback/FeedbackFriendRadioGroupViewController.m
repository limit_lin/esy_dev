
#import "FeedbackFriendRadioGroupViewController.h"
#import "FeedbackSettingViewController.h"

@interface FeedbackFriendRadioGroupViewController ()

@end

@implementation FeedbackFriendRadioGroupViewController

@synthesize radioBtnImg, nameLabel, nameStr, idx, parent, bIsSel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    self.screenName = @"FeedbackFriendRadioGroupView";
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];//151125
    nameLabel.text = nameStr;
    bIsSel = NO;
}

- (void)dealloc
{
    [radioBtnImg release];
    [nameLabel release];
    [nameStr release];
    [parent release];
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)didClickBtn:(id)sender
{
    [parent procFreindRadioBtn:self.idx];
}

@end

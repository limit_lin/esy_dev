
#import <UIKit/UIKit.h>

@class FeedbackSettingViewController;

@interface RadioboxViewController : GAITrackedViewController<UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UITableView    *tbleView;    
}

@property (nonatomic, strong) FeedbackSettingViewController *parent;
@property (nonatomic, strong) NSMutableArray *itemArray;

- (void)initSetting;

@end

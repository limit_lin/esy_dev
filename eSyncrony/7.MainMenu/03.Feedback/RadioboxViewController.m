
#import "RadioboxViewController.h"
#import "FeedbackSettingViewController.h"
#import "RadioBoxCell.h"

@interface RadioboxViewController ()

@end

@implementation RadioboxViewController

@synthesize parent, itemArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"RadioboxView";
    tbleView.layer.cornerRadius = 5;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];//151125
    [self initSetting];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [tbleView release];
    [parent release];
    [itemArray release];
    
    [super dealloc];
}

- (void)initSetting
{
    // remove selview
    for (UIView *v in [self.view subviews]) {
        if (v.tag == 100) {
            [v removeFromSuperview];
        }
    }
    
    // resizing of subview
    NSInteger itemCnt = [self.itemArray count];
    
    NSInteger showItemCnt = (itemCnt < 7)? itemCnt: 7;
    float      tblViewHeight;
    
    tblViewHeight = 55 * showItemCnt;
    NSInteger tableOrgY = 60;
    
    //if (showItemCnt < 7) {
    //    tableOrgY = (self.view.frame.size.height - tblViewHeight)/2;
    //}
    
    tableOrgY = (self.view.frame.size.height - tblViewHeight)/2;
    
    tbleView.frame = CGRectMake(tbleView.frame.origin.x, tableOrgY, tbleView.frame.size.width, tblViewHeight);
    
    // reloading of table
    [tbleView reloadData];
    
    // add subview
    [self.view performSelector:@selector(addSubview:) withObject:tbleView afterDelay:0.1];
}

#pragma mark --- Table Functions ---
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [itemArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RadioBoxCell *cell;
    NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"RadioBoxCell" owner:nil options:nil];
    cell = [arr objectAtIndex:0];
    
    cell.reasonLabel.text = [itemArray objectAtIndex:indexPath.row];
    
    UIImage *img;
    if ([cell.reasonLabel.text isEqualToString:[parent.editItemArray objectAtIndex:0]]) {
        img = [UIImage imageNamed:@"btnRadio_p"];
//        [cell.radioImgView initWithImage:img];
        cell.radioImgView.image = img;
    }
    else {
        img = [UIImage imageNamed:@"btnRadio_n"];
//        [cell.radioImgView initWithImage:img];
        cell.radioImgView.image = img;
    }
    
    return cell;
}

- (void)procTblSelectCell
{
    [self.view removeFromSuperview];
    
    [parent receiveEditResult];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [parent.editItemArray removeAllObjects];
    
    [parent.editItemArray addObject:[itemArray objectAtIndex:indexPath.row]];
    
    [self performSelector:@selector(procTblSelectCell) withObject:nil afterDelay:0.2];
}

@end


#import <UIKit/UIKit.h>
#import "HttpBaseViewController.h"

@class FeedbackPosNegSettingViewController;
@class RadioboxViewController;
@class FeedbackViewController;
@class PastDatesViewController;
@interface FeedbackSettingViewController : HttpBaseViewController <UITextViewDelegate>
{
    IBOutlet UIScrollView *scrlView;
    IBOutlet UIView *contentsView;
    
    IBOutlet UITextView    *aspectTv;
    
    IBOutlet UIButton       *positiveBtn;
    IBOutlet UIButton       *negativeBtn;
    
    // to move
    IBOutlet UILabel        *additionalLabel;
    IBOutlet UILabel        *areThereLabel;
    IBOutlet UILabel        *IwouldLabel;
    IBOutlet UIButton       *checkBtn;
    IBOutlet UIButton       *updateBtn;
    IBOutlet UILabel        *updateBtnLabel;
    IBOutlet UIImageView    *devideBar_1;
    IBOutlet UIImageView    *devideBar_2;
    IBOutlet UIImageView    *feedbackBg;
    
    IBOutlet UITextView     *feedbackTv;
    
    FeedbackPosNegSettingViewController *settingViewCtrl;
    RadioboxViewController              *radioboxViewCtrl;
    
    BOOL                    isCheck;
    float                   contentViewHeight;
    
    NSMutableArray          *arrDatesItems;
}
@property (nonatomic, assign) PastDatesViewController* pastDatesDelegate;
@property (nonatomic, assign) FeedbackViewController*  feedbackDelegate;

@property (nonatomic, strong) IBOutlet UILabel        *positiveLabel;
@property (nonatomic, strong) IBOutlet UILabel        *negativeLabel;
@property (nonatomic, strong) IBOutlet UILabel        *restaurantLabel;
@property (nonatomic, strong) IBOutlet UILabel        *considerLabel;
@property (nonatomic, strong) IBOutlet UILabel        *presentLabel;
@property (nonatomic, strong) IBOutlet UILabel        *confidenceLabel;
@property (nonatomic, strong) IBOutlet UILabel        *interestLabel;
@property (nonatomic, strong) IBOutlet UILabel        *conversLabel;
@property (nonatomic, strong) IBOutlet UILabel        *diningLabel;
@property (nonatomic, strong) IBOutlet UILabel        *contactLabel;
@property (nonatomic, strong) IBOutlet UILabel        *lastLabel;
@property (nonatomic, strong) IBOutlet UILabel        *experiencLabel;
@property (nonatomic, strong) IBOutlet UILabel        *feelLabel;
@property (nonatomic, strong) IBOutlet UILabel        *secondDateLabel;

@property (nonatomic, strong) NSMutableArray          *itemArray;
@property (nonatomic, strong) NSMutableArray          *editItemArray;
@property (nonatomic, strong) NSMutableArray          *friendArray;
@property (nonatomic, strong) NSMutableArray          *radioViewArray;
@property (nonatomic)  NSInteger                       editItemType;
@property (nonatomic, strong) NSMutableDictionary     *curDicInfo;

- (IBAction)didClickBackbtn:(id)sender;
- (IBAction)didClickUpdateBtn:(id)sender;
- (IBAction)didClickPositiveBtn:(id)sender;
- (IBAction)didClickNegativeBtn:(id)sender;
- (IBAction)didClickRestaurBtn:(id)sender;
- (IBAction)didClickConsiderBtn:(id)sender;
- (IBAction)didClickPresentBtn:(id)sender;
- (IBAction)didClickConfidenceBtn:(id)sender;
- (IBAction)didClickInterestBtn:(id)sender;
- (IBAction)didClickConversBtn:(id)sender;
- (IBAction)didClickDiningBtn:(id)sender;
- (IBAction)didClickContactBtn:(id)sender;
- (IBAction)didClickLastBtn:(id)sender;
- (IBAction)didClickExperiencBtn:(id)sender;
- (IBAction)didClickFeelBtn:(id)sender;
- (IBAction)didClickSecondDateBtn:(id)sender;
- (IBAction)didClickCheckBtn:(id)sender;

- (void)initSetting;
- (void)insertRadioGroup;
- (void)loadItemArrayWithName:(NSString *)name;
- (void)receiveEditResult;
- (void)procFreindRadioBtn:(NSInteger)idx;

@end


#import "FeedbackViewController.h"
#import "FeedBackCell.h"
#import "FeedbackSettingViewController.h"
#import "MatchProfileViewController.h"

#import "DSActivityView.h"
#import "UtilComm.h"
#import "JSON.h"

#import "mainMenuViewController.h"
#import "eSyncronyAppDelegate.h"

@interface FeedbackViewController ()

@end

@implementation FeedbackViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"FeedbackView";
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [self refreshContent];
}

- (void)refreshContent
{

    
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(loadDatesInfo) withObject:nil afterDelay:0.1];
}

- (void)loadDatesInfo
{
    NSDictionary* result = [UtilComm viewDates:@"3"];
    [DSBezelActivityView removeViewAnimated:NO];
    
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Error"];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
        return;
    }
    
    [arrMatchesItems release];
    arrMatchesItems = [[NSMutableArray alloc] initWithCapacity:0];
    
    NSDictionary*   responseDic = (NSDictionary*)response;
    if( [responseDic isKindOfClass:[NSDictionary class]] )
    {
        NSArray *arrItems = [responseDic objectForKey:@"item"];
        
        if( [arrItems isKindOfClass:[NSDictionary class]] )
        {
            NSDictionary* item = (NSDictionary*)arrItems;
            NSMutableDictionary *mutItem = [NSMutableDictionary dictionaryWithDictionary:item];
            
            [arrMatchesItems addObject:mutItem];
        }
        else if( [arrItems isKindOfClass:[NSArray class]] )
        {
            for( int i = 0; i < [arrItems count]; i++ )
            {
                NSDictionary        *item = [arrItems objectAtIndex:i];
                NSMutableDictionary *mutItem = [NSMutableDictionary dictionaryWithDictionary:item];
                
                [arrMatchesItems addObject:mutItem];
            }
        }
    }
    
    [listTblView reloadData];
    if( [arrMatchesItems count] == 0 )
    {

        listTblView.scrollEnabled = NO;
    }
    else
    {

        listTblView.scrollEnabled = YES;
    }
}

#pragma mark --- Table Functions ---
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if( arrMatchesItems == nil )
        return 0;
    
    return [arrMatchesItems count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary     *dicMatchesInfo = [arrMatchesItems objectAtIndex:indexPath.row];
    FeedBackCell    *cell;
    NSArray         *arr = [[NSBundle mainBundle] loadNibNamed:@"FeedBackCell" owner:nil options:nil];
    cell = [arr objectAtIndex:0];
    
    [cell setMatchesInfo:dicMatchesInfo];
    cell.feedbackViewCtrl = self;

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)didClickNameWithInfo:(NSMutableDictionary*)dicMatchInfo
{
    MatchProfileViewController  *matchInfoViewCtrl = [MatchProfileViewController createWithMatchInfo:dicMatchInfo];
    
    matchInfoViewCtrl.m_bTopButton = NO;
    matchInfoViewCtrl.m_bDismissSelf = YES;

    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.navigationController pushViewController:matchInfoViewCtrl animated:YES];
}

- (void)didClickFeebbackWithInfo:(NSMutableDictionary*)dicMatchInfo
{
    FeedbackSettingViewController* feedbackSettingViewCtrl = [[[FeedbackSettingViewController alloc] initWithNibName:@"FeedbackSettingViewController" bundle:nil] autorelease];

    feedbackSettingViewCtrl.curDicInfo = dicMatchInfo;
    feedbackSettingViewCtrl.feedbackDelegate = self;
    
    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.navigationController pushViewController:feedbackSettingViewCtrl animated:YES];
}

- (void)dealloc
{
    [listTblView release];

    
    [arrMatchesItems release];
    
    [super dealloc];
}

@end

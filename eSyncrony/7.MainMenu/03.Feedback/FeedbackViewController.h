
#import <UIKit/UIKit.h>

@interface FeedbackViewController : GAITrackedViewController <UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UITableView    *listTblView;

    
    NSMutableArray          *arrMatchesItems;
}

- (void)refreshContent;

- (void)didClickNameWithInfo:(NSMutableDictionary*)dicMatchInfo;
- (void)didClickFeebbackWithInfo:(NSMutableDictionary*)dicMatchInfo;

@end

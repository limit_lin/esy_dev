
#import "Global.h"
#import "UtilComm.h"
#import "DSActivityView.h"

#import "mainMenuViewController.h"
#import "MemberLoginViewController.h"
#import "MenuItemCell.h"
#import "MenuDatesCell.h"
#import "MenuMatchCell.h"
#import "MenuProfileCell.h"
#import "MyMatchesProposedViewController.h"
#import "PendingViewController.h"
#import "ApprovedViewController.h"
#import "CancelledViewController.h"
#import "ReportViewController.h"
#import "newDatesViewController.h"
#import "PastDatesViewController.h"
#import "FeedbackViewController.h"
#import "DatingReportViewController.h"
#import "ProfileViewController.h"
#import "PersonalityViewController.h"
#import "MyAccountViewController.h"
#import "UpgradeViewController.h"
#import "UpgradeSuccessViewController.h"
#import "PhotoExchangeViewController.h"
#import "MatchProfileViewController.h"
#import "GiveFeedBackViewController.h"
#import "DocCertViewController.h"
#import "eSynchronyFilterViewController.h"
#import "MyMatchesFavoritedViewController.h"
#import "eSyncronyAppDelegate.h"
#import "XMLParser.h"

#import "CustomCellMessageRight.h"
#import "CustomMessageChat.h"

@interface mainMenuViewController ()

@end

@implementation mainMenuViewController

@synthesize curretItemIdx, containerView, itemTitle;
@synthesize mainRightLogoBtn;

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if( buttonIndex == 0 )
    {
        [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Logging out...",@"mainMenuView")];
        [self performSelector:@selector(procLogout) withObject:nil afterDelay:0.1];
    }else
    {
        return;
    }
}

- (void)onServerConnectionError:(NSString*)error
{
    
    if( [error isEqualToString:@"ERROR 0"] )
    {
        [ErrorProc alertMessage:ERROR_EMPTY_STRING withTitle:NSLocalizedString(@"Info",)];
        
    }
    else if( [error isEqualToString:@"ERROR 1"] )
    {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"eSynchrony"
                                                     message:ERROR_Not_Login
                                                    delegate:self
                                           cancelButtonTitle:NSLocalizedString(@"OK",)
                                           otherButtonTitles:nil, nil];
        [av show];
        [av release];
    }
    else if( [error isEqualToString:@"ERROR 2"] )
    {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Sorry",)
                                                     message:ERROR_Session_Expired
                                                    delegate:self
                                           cancelButtonTitle:NSLocalizedString(@"OK",)
                                           otherButtonTitles:nil, nil];
        [av show];
        [av release];
    }
    else if( [error isEqualToString:@"ERROR 3"] )
    {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Sorry",)
                                                     message:ERROR_NEED_RELOGIN
                                                    delegate:self
                                           cancelButtonTitle:NSLocalizedString(@"OK",)
                                           otherButtonTitles:nil, nil];
        [av show];
        [av release];
    }
    else
    {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Sorry",)
                                                     message:ERROR_NEED_RELOGIN
                                                    delegate:self
                                           cancelButtonTitle:NSLocalizedString(@"OK",)
                                           otherButtonTitles:nil, nil];
        [av show];
        [av release];
    }
}

//=====================================================================================================//
//=====================================================================================================//
//=====================================================================================================//

- (void)initVariables
{
    menuIconImgArray = [[NSArray alloc] initWithObjects:@"icon_match", @"icon_date",  @"icon_dating_intelligence", @"icon_mydetail", @"icon_setting", @"icon_upgrade", @"icon_feedback",@"icon_photo_exchange",@"icon_date", nil];
    
    menuIconTitleArray = [[NSArray alloc] initWithObjects:@"My Matches", @"Dates",NSLocalizedString(@"Dating Report",), @"My Profile",NSLocalizedString(@"Setting",),NSLocalizedString(@"Upgrade",),NSLocalizedString(@"Feedback",),NSLocalizedString(@"Photo Exchange",),NSLocalizedString(@"Log Out",),nil];

    
    isExpandedMatch = NO;
    isExpandedDate = NO;
    isExpandedProfile = NO;
    
    scrlOffsetX = 0;
    scrlOffRightSetX = 0;
    curretItemIdx = 0;
    
    leftMenuWidth   = leftMenuView.frame.size.width;
    menuItemWidth   = menuItemView.frame.size.width;
    rightMenuWidth  = rightMenuView.frame.size.width;
    
    bInitSubViewFrames = FALSE;
    
    _curViewController = nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl = self;

    bInitialize = NO;

    [self initVariables];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnContentView:)];
//    UITapGestureRecognizer *tapRecognizer = [[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnContentView:)]autorelease];
    [contentMaskView addGestureRecognizer:tapRecognizer];
}

//=====================================================================================================//
//=====================================================================================================//
//=====================================================================================================//

- (void)initSubViewFrames
{
    contentMaskView.hidden = YES;
    
//    _mainMenuRightLogoBtn.hidden = NO;
    
    [mainScrl addSubview:leftMenuView];
    [mainScrl addSubview:menuItemView];
    [mainScrl addSubview:rightMenuView];//append socket.io
    
    scrlOffsetX = leftMenuView.frame.size.width;
    mainScrl.contentOffset = CGPointMake(scrlOffsetX, 0);
    
    //append socket.io 16/04/07
//    scrlOffRightSetX = leftMenuView.frame.size.width + rightMenuView.frame.size.width;
//    mainScrl.contentOffset = CGPointMake(scrlOffRightSetX, 0);
    
    float height = mainScrl.bounds.size.height;
    
    CGRect  frame = leftMenuView.frame;
    frame.size.height = height;
    leftMenuView.frame = frame;
    
    menuItemView.frame = CGRectMake(leftMenuWidth, 0, 320, height);
    rightMenuView.frame = CGRectMake(leftMenuWidth + menuItemWidth, 0, rightMenuWidth, height);//append socket.io
    mainScrl.contentSize = CGSizeMake(leftMenuWidth + menuItemWidth + rightMenuWidth, height);
    
    
    scrlOffsetX = leftMenuWidth;
    mainScrl.contentOffset = CGPointMake(scrlOffsetX, 0);
    
    bInitSubViewFrames = TRUE;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
//    
//    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];  // Get the tracker object.
//    [tracker set:[GAIFields customDimensionForIndex:1]
//           value:@"premium"];
//    [super viewDidAppear:animated];   // Custom dimension value will be sent with the screen view.
    
    
    if( bInitSubViewFrames == FALSE )
        [self initSubViewFrames];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if( bInitialize == NO )
    {
        [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
        [self performSelector:@selector(loadInfos) withObject:nil afterDelay:0.1];
    }
}

//=====================================================================================================//
//=====================================================================================================//
//=====================================================================================================//

- (BOOL)loadInfos
{
    [Global loadBasicProfileInfoAndSaveToClient];

    NSDictionary* result = [UtilComm retrieveNumberMatches];
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Error"];
        return FALSE;
    }
    
    NSString*   response = [result objectForKey:@"response"];
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [self onServerConnectionError:response];
        return FALSE;
    }
    
    [Global sharedGlobal].nProposedMatches = [response intValue];
    
    result = nil;
    response = nil;
    
    result = [UtilComm retrieveNumberDates];
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Error"];
        return FALSE;
    }
    
    response = [result objectForKey:@"response"];
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [self onServerConnectionError:response];
        return FALSE;
    }
    
    [Global sharedGlobal].nDates = [response intValue];
    [listTblView reloadData];
    
    bInitialize = YES;
    
//     dispatch_async(dispatch_get_main_queue(), ^{//append 151222
    [self procMyMatchProposed];
//     });
    [DSBezelActivityView removeViewAnimated:NO];
    
    return TRUE;
}

//=====================================================================================================//
//=====================================================================================================//
//=====================================================================================================//

- (void)procLogout
{
//    NSString *_strUrl = [NSString stringWithFormat:@"%@logout?acc_no=%@&token_key=%@",WEBSERVICE_BASEURI, [eSyncronyAppDelegate sharedInstance].strAccNo, [eSyncronyAppDelegate sharedInstance].strTokenKey];
//
//    _strUrl = [_strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [[eSyncronyAppDelegate sharedInstance] logOut];
    [DSBezelActivityView removeViewAnimated:NO];

    [self.navigationController popToRootViewControllerAnimated:YES ];
    [eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl = nil;
    //[[eSyncronyAppDelegate sharedInstance] logOut];
}

- (void)logOut
{
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"eSynchrony" message:NSLocalizedString(@"Do you want to log out?",@"mainMenuView") delegate:self cancelButtonTitle:NSLocalizedString(@"YES",) otherButtonTitles:NSLocalizedString(@"NO",), nil];
    [av show];
    [av release];
}

//=====================================================================================================//
#pragma mark --- Table Functions ---
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView.tag == 0){
        return 1;
    }else
        return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag == 0) {
        return menuIconTitleArray.count;
    }else
        return 1;
	
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 0) {
        if( indexPath.row == 0 && isExpandedMatch == YES )
        {
            return 350;//match favorite list 300 -->350
        }
        
        if( indexPath.row == 1 && isExpandedDate == YES )
        {
            return 200;
            
        }
        if( indexPath.row == 2 && (![[eSyncronyAppDelegate sharedInstance].membership isEqualToString:@"Premium"]))
        {
            return 0;
        }
        if( indexPath.row == 3 && isExpandedProfile == YES )
        {
            if ([[eSyncronyAppDelegate sharedInstance].membership isEqualToString:@"Premium"]){
                return 200;
            }else{
                return 150;
            }
        }
        
        return 50;
    }else
        return 123;

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 0) {
        if( indexPath.row == 0 )
        {
            MenuMatchCell *cell;
            NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"MenuMatchCell" owner:nil options:nil];
            cell = [arr objectAtIndex:0];
            cell.mainMenuView = self;
            
            [cell refreshContent];
            
            return cell;
        }
        else if( indexPath.row == 1 )
        {
            MenuDatesCell *cell;
            NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"MenuDatesCell" owner:nil options:nil];
            cell = [arr objectAtIndex:0];
            cell.mainMenuView = self;
            
            [cell refreshContent];
            
            return cell;
        }
        else if( indexPath.row == 3 )
        {
            MenuProfileCell *cell;
            NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"MenuProfileCell" owner:nil options:nil];
            cell = [arr objectAtIndex:0];
            cell.mainMenuView = self;
            [cell refreshContent];
            return cell;
        }
        
        MenuItemCell *cell;
        NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"MenuItemCell" owner:nil options:nil];
        cell = [arr objectAtIndex:0];
        
        cell.itemTitleLabel.text = [menuIconTitleArray objectAtIndex:indexPath.row];
        cell.icnImgView.image = [UIImage imageNamed:[menuIconImgArray objectAtIndex:indexPath.row]];
        cell.idx = (indexPath.row + 1) * 100;
        cell.mainMenuView = self;
        
        return cell;
    }else
    {
        CustomCellMessageRight *cell;
        NSArray *arr = [[NSBundle mainBundle]loadNibNamed:@"CustomCellMessageRight" owner:nil options:nil];
        cell = [arr objectAtIndex:0];
        return cell;
    }

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if (tableView.tag != 0) {
        NSLog(@"click here");
       CustomCellMessageRight* cell = (CustomCellMessageRight *)[tableView cellForRowAtIndexPath:indexPath];
        cell.selected = NO;
        CustomMessageChat *customMessageChat = [[CustomMessageChat alloc]initWithNibName:@"CustomMessageChat" bundle:nil];
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.navigationController pushViewController:customMessageChat animated:YES];
        
    }
  

}

//=====================================================================================================//

- (void)changeContentViewController:(UIViewController*)viewController
{
    scrlOffsetX = leftMenuView.frame.size.width;
    //mainScrl.contentOffset = CGPointMake(scrlOffsetX, 0);
    
    [UIView animateWithDuration:0.25
                          delay:0
                        options:0
                     animations:^{
                         mainScrl.contentOffset = CGPointMake(scrlOffsetX, 0);
                     }
                     completion:^(BOOL finished) {
                         contentMaskView.hidden = YES;
                     }
    ];

    if( _curViewController == viewController )
        return;
    
    [_curViewController.view removeFromSuperview];

    viewController.view.frame = containerView.bounds;
    [containerView addSubview:viewController.view];
    [self.view sendSubviewToBack:containerView];
    _curViewController = viewController;
}

- (void)procMyMatchProposed//修改导航标题
{
    if( curretItemIdx != MYMATCHES_PROPOSED )
    {
        itemTitle.text = NSLocalizedString(@"PROPOSED MATCHES",@"mainMenuView");

        if( !myMatchProposeViewCtrl )
            myMatchProposeViewCtrl = [[MyMatchesProposedViewController alloc] initWithNibName:@"MyMatchesProposedViewController" bundle:nil];
        
        myMatchProposeViewCtrl.mainMenuView = self;
    }

    [self changeContentViewController:myMatchProposeViewCtrl];
}
- (void)procMyMatchFavorited//修改导航标题
{
    if( curretItemIdx != MYMATCHES_FAVORITED)
    {
        itemTitle.text = NSLocalizedString(@"FAVORITE MATCHES",@"mainMenuView");
        
        if( !myMatchFavoriteViewCtrl )
            myMatchFavoriteViewCtrl = [[MyMatchesFavoritedViewController alloc] initWithNibName:@"MyMatchesFavoritedViewController" bundle:nil];
        
        myMatchFavoriteViewCtrl.mainMenuView = self;
    }
    
    [self changeContentViewController:myMatchFavoriteViewCtrl];
}

-(void)procMyMatchFilter{
    
    if( curretItemIdx != MYMATCHES_FILTER )
    {
        itemTitle.text = NSLocalizedString(@"Filtering",@"mainMenuView");
        if( !filterViewCtrl )
            filterViewCtrl = [[eSynchronyFilterViewController alloc] initWithNibName:@"eSynchronyFilterViewController " bundle:nil];
        
    }
    [self changeContentViewController:filterViewCtrl];
}

- (void)procMyMatchPending
{
    if( curretItemIdx != MYMATCHES_PENDING )
    {
        itemTitle.text = NSLocalizedString(@"PENDING MATCHES",@"mainMenuView");
        if( !pendingViewCtrl )
            pendingViewCtrl = [[PendingViewController alloc] initWithNibName:@"PendingViewController" bundle:nil];
        
        //pendingViewCtrl.mainMenuView = self;
        
    }
    
    [self changeContentViewController:pendingViewCtrl];
}

- (void)procMyMatchApproved
{
    if( curretItemIdx != MYMATCHES_APPROVED )
    {
        itemTitle.text = NSLocalizedString(@"APPROVED MATCHES",@"mainMenuView");
        
        if( !approvedViewCtrl )
            approvedViewCtrl = [[ApprovedViewController alloc] initWithNibName:@"ApprovedViewController" bundle:nil];
        
        approvedViewCtrl.mainMenuView = self;
        
    }
    
    [self changeContentViewController:approvedViewCtrl];
}

- (void)procMyMatchCancelled
{
    if( curretItemIdx != MYMATCHES_CANCELLED )
    {
        itemTitle.text = NSLocalizedString(@"CANCELLED MATCHES",@"mainMenuView");

        if( !cancelledViewCtrl )
            cancelledViewCtrl = [[CancelledViewController alloc] initWithNibName:@"CancelledViewController" bundle:nil];

        cancelledViewCtrl.mainMenuView = self;
        
    }
    
    [self changeContentViewController:cancelledViewCtrl];
}

- (void)procMyMatchReport
{
    if( curretItemIdx != MYMATCHES_REPORT )
    {
        itemTitle.text = NSLocalizedString(@"Match Report",@"mainMenuView");
        
        if( !reportViewCtrl )
            reportViewCtrl = [[ReportViewController alloc] initWithNibName:@"ReportViewController" bundle:nil];
        reportViewCtrl.mainMenuView = self;
    }

    [self changeContentViewController:reportViewCtrl];
}

- (void)procNewDates
{
    if( curretItemIdx != DATES_NEW_DATES )
    {
        itemTitle.text = NSLocalizedString(@"NEW DATES",@"mainMenuView");
        
        if( !newDateViewCtl )
            newDateViewCtl = [[newDatesViewController alloc] initWithNibName:@"newDatesViewController" bundle:nil];
    }
    
    [self changeContentViewController:newDateViewCtl];
}

- (void)procPastDates
{
    if( curretItemIdx != DATES_PAST_DATES )
    {
        itemTitle.text = NSLocalizedString(@"PAST DATES",@"mainMenuView");
        
        if( !pastDatesViewCtrl )
            pastDatesViewCtrl = [[PastDatesViewController alloc] initWithNibName:@"PastDatesViewController" bundle:nil];
    }
    
    [self changeContentViewController:pastDatesViewCtrl];
}

- (void)procFeedback
{
    if( curretItemIdx != DATE_FEEDBACK )
    {
        itemTitle.text = NSLocalizedString(@"Date Feedback",@"mainMenuView");
        
        if( !feedbackViewCtrl )
            feedbackViewCtrl = [[FeedbackViewController alloc] initWithNibName:@"FeedbackViewController" bundle:nil];
    }
    
    [self changeContentViewController:feedbackViewCtrl];
}

- (void)procDatingReport
{
    if( curretItemIdx != MENU_DATINGREPORT )
    {
        itemTitle.text = NSLocalizedString(@"Dating Intelligence",@"mainMenuView");
        
        if( !datingReportViewCtrl )
            datingReportViewCtrl = [[DatingReportViewController alloc] initWithNibName:@"DatingReportViewController" bundle:nil];
    }
    
    [self changeContentViewController:datingReportViewCtrl];
}

- (void)procProfile
{
    if( curretItemIdx != PROFILE_PROFILE )
    {
        itemTitle.text = NSLocalizedString(@"My Profile",@"mainMenuView");
        
        if( !profileViewCtrl )
            profileViewCtrl = [[ProfileViewController alloc] initWithNibName:@"ProfileViewController" bundle:nil];
        profileViewCtrl.view.frame = containerView.bounds;
    }
    
    [self changeContentViewController:profileViewCtrl];
}

- (void)procPersonality
{
    if( curretItemIdx != PROFILE_REPORT )
    {
        itemTitle.text = NSLocalizedString(@"Personality",@"mainMenuView"); // My name
        
        if( !personViewCtrl )
            personViewCtrl = [[PersonalityViewController alloc] initWithNibName:@"PersonalityViewController" bundle:nil];
        personViewCtrl.view.frame = containerView.bounds;
    }
    
    [self changeContentViewController:personViewCtrl];
}

- (void)procMyAccount:(BOOL)bPsdChangeMode
{
    if( curretItemIdx != PROFILE_ACCOUNT )
    {
        itemTitle.text = NSLocalizedString(@"My Account",@"mainMenuView"); // My name
        
        if( !myAccountViewCtrl )
            myAccountViewCtrl = [[MyAccountViewController alloc] initWithNibName:@"MyAccountViewController" bundle:nil];
        curretItemIdx = PROFILE_ACCOUNT;
    }
    
    myAccountViewCtrl.bPsdChangeMode = bPsdChangeMode;
    [self changeContentViewController:myAccountViewCtrl];
}

- (void)procUpgrade
{
    if( curretItemIdx != MENU_UPGRADE )
    {
        itemTitle.text = NSLocalizedString(@"Upgrade",@"mainMenuView"); // My name
        
        if( !upgradeViewCtrl )
            upgradeViewCtrl = [[UpgradeViewController alloc] initWithNibName:@"UpgradeViewController" bundle:nil];
        curretItemIdx = MENU_UPGRADE;
    }
    
    [self changeContentViewController:upgradeViewCtrl];
}

- (void)procUpgradeSuccess:(NSUInteger)nIndex packagePrice:(NSString *)price
{
    NSLog(@"curretItemIdx == %ld",(long)curretItemIdx);
    if( curretItemIdx != MENU_UPGRADE_SUCCESS )
    {
        itemTitle.text = NSLocalizedString(@"THANK YOU",@"mainMenuView");
        
        if( !upgradeSuccessViewCtrl )
            upgradeSuccessViewCtrl = [[UpgradeSuccessViewController alloc] initWithNibName:@"UpgradeSuccessViewController" bundle:nil];
        upgradeSuccessViewCtrl.nPurchaseIndex = nIndex;
        upgradeSuccessViewCtrl.packagePrice = price;
        curretItemIdx = MENU_UPGRADE_SUCCESS;
    }
    
    [self changeContentViewController:upgradeSuccessViewCtrl];
}
- (void)procGiveFeedback:(BOOL)nLove
{
    if( curretItemIdx != MENU_FEEDBACK )
    {
        itemTitle.text = NSLocalizedString(@"Give Feedback",@"mainMenuView"); // My name
        
        if( !giveFeedback )
            giveFeedback = [[GiveFeedBackViewController alloc]initWithNibName:@"GiveFeedBackViewController" bundle:nil];
        giveFeedback.nLove = nLove;
        curretItemIdx = MENU_FEEDBACK;
    }
    
    [self changeContentViewController:giveFeedback];
    
}
- (void)procPhotoExchange
{
    if( curretItemIdx != MENU_PHOTO_EXCHANGE )
    {
        itemTitle.text = NSLocalizedString(@"Photo Exchange",@"mainMenuView"); // My name

        if( !photoExchangeViewCtrl )
            photoExchangeViewCtrl = [[PhotoExchangeViewController alloc] initWithNibName:@"PhotoExchangeViewController" bundle:nil];
    }
    
    [self changeContentViewController:photoExchangeViewCtrl];
}

- (void)procItemClickEventWithID:(NSInteger)idx
{
    contentMaskView.hidden = YES;
    
    switch( idx )
    {
        case MENU_MY_MATCHES:
            isExpandedMatch = !isExpandedMatch;
            [listTblView reloadData];
            contentMaskView.hidden = NO;
            break;
        case MYMATCHES_PROPOSED:
            [self procMyMatchProposed];
            break;
        case MYMATCHES_PENDING:
            [self procMyMatchPending];
            break;
        case MYMATCHES_APPROVED:
            [self procMyMatchApproved];
            break;
        case MYMATCHES_CANCELLED:
            [self procMyMatchCancelled];
            break;
        case MYMATCHES_REPORT:
            [self procMyMatchReport];
            break;
        case MYMATCHES_FILTER://match list filtering
            [self procMyMatchFilter];
            break;
        case MYMATCHES_FAVORITED://match favorite list
        {
            if ([[eSyncronyAppDelegate sharedInstance].isVip isEqualToString:@"false"]) {
                [self procUpgrade];
            }else
               [self procMyMatchFavorited];
        }
            break;
        case MENU_DATES:
            isExpandedDate = !isExpandedDate;
            [listTblView reloadData];
            break;
        case DATES_NEW_DATES:
            [self procNewDates];
            break;
        case DATES_PAST_DATES:
            [self procPastDates];
            break;
        case DATE_FEEDBACK:{
            
            [self procFeedback];
            break;
        }
        case MENU_FEEDBACK:
            [self procGiveFeedback:NO];
            break;
        case MENU_DATINGREPORT:{
           
            [self procDatingReport];
            
            break;
        }
        case MENU_MYPROFILE:
            isExpandedProfile = !isExpandedProfile;
            [listTblView reloadData];
            contentMaskView.hidden = NO;
            break;
        case PROFILE_PROFILE:
            [self procProfile];
            break;
        case PROFILE_REPORT:
            [self procPersonality];
            break;
            
        case PROFILE_ACCOUNT:
        case MENU_SETTING:
            [self procMyAccount:NO];
            break;
            
        case MENU_UPGRADE:
            [self procUpgrade];
            break;
        case MENU_PHOTO_EXCHANGE:{
            
            if ([[eSyncronyAppDelegate sharedInstance].membership isEqualToString:@"Free"]) {
                [self procUpgrade];
                
            }else{
                
                [self procPhotoExchange];
            }
            break;
        }
        case MENU_LOGOUT:
            [self logOut];
            break;
    }
    
    curretItemIdx = idx;
}
- (void)goMainMenu
{
    if( scrlOffsetX == 0 )
        scrlOffsetX = leftMenuView.frame.size.width;
    else
    {
        contentMaskView.hidden = NO;
        scrlOffsetX = 0;
    }
    
    [UIView animateWithDuration:0.25
                          delay:0
                        options:0
                     animations:^{
                         mainScrl.contentOffset = CGPointMake(scrlOffsetX, 0);
                     }
                     completion:^(BOOL finished) {
                         if( scrlOffsetX != 0 )
                             contentMaskView.hidden = YES;
                     }
     ];
}

- (IBAction)didClickLogoBtn:(id)sender
{
    scrlOffRightSetX = 0;
    if( scrlOffsetX == 0 )
        scrlOffsetX = leftMenuView.frame.size.width;
    else
    {
        contentMaskView.hidden = NO;
        scrlOffsetX = 0;
    }

    [UIView animateWithDuration:0.25
                          delay:0
                        options:0
                     animations:^{
                         mainScrl.contentOffset = CGPointMake(scrlOffsetX, 0);
                     }
                     completion:^(BOOL finished) {
                         if( scrlOffsetX != 0 )
                             contentMaskView.hidden = YES;
                     }
     ];
}

- (IBAction)didClickRightLogoBtn:(id)sender {
    
//    NSLog(@"float = %f",scrlOffRightSetX);
    mainRightLogoBtn.hidden = YES;
    
    if( scrlOffRightSetX == 0 || scrlOffRightSetX == scrlOffsetX){
//       scrlOffRightSetX = leftMenuView.frame.size.width + rightMenuView.frame.size.width;
//        pRightContentMaskView.hidden = YES;
         scrlOffRightSetX = leftMenuView.frame.size.width + rightMenuView.frame.size.width - 45;
    }else
    {
        contentMaskView.hidden = NO;
        scrlOffRightSetX = leftMenuView.frame.size.width;
    }
    
    [UIView animateWithDuration:0.25
                          delay:0
                        options:0
                     animations:^{
                         mainScrl.contentOffset = CGPointMake(scrlOffRightSetX, 0);
                     }
                     completion:^(BOOL finished) {
                         if( scrlOffRightSetX != 0 )
                             contentMaskView.hidden = YES;
                     }
     ];
    
}

- (void)tapOnContentView:(UITapGestureRecognizer *)gesture
{
    scrlOffsetX = leftMenuView.frame.size.width;

    [UIView animateWithDuration:0.25
                          delay:0
                        options:0
                     animations:^{
                         mainScrl.contentOffset = CGPointMake(scrlOffsetX, 0);
                     }
                     completion:^(BOOL finished) {
                         contentMaskView.hidden = YES;
                     }
     ];
}

- (void)dealloc
{
    [mainScrl release];
    [listTblView release];
    [itemTitle release];
    
    [leftMenuView release];
    [menuItemView release];
    [containerView release];
    [contentMaskView release];
    
    [menuIconImgArray release];
    [menuIconTitleArray release];
    
    [myMatchProposeViewCtrl release];
    
    [pendingViewCtrl release];
    [approvedViewCtrl release];
    [cancelledViewCtrl release];
    
    [reportViewCtrl release];
    [newDateViewCtl release];
    [pastDatesViewCtrl release];
    [feedbackViewCtrl release];
    [datingReportViewCtrl release];
    [profileViewCtrl release];
    [personViewCtrl release];
    [myAccountViewCtrl release];
    [upgradeViewCtrl release];
    [photoExchangeViewCtrl release];
    
    [rightMenuView release];
    [ptblListView release];
    [pRightContentMaskView release];
    [mainRightLogoBtn release];
    [_mainMenuRightLogoBtn release];
    [super dealloc];
}

@end

//
//  eSynchronyFilterViewController.m
//  eSyncrony
//
//  Created by ESYNSZ-Limit on 16/1/29.
//  Copyright © 2016年 WonMH. All rights reserved.
//


#import "Global.h"
#import "eSyncronyAppDelegate.h"
#import "eSynchronyFilterViewController.h"
#import "MyMatchesProposedViewController.h"
#import "mainMenuViewController.h"
#import "eSyncronyViewController.h"

@interface eSynchronyFilterViewController ()

@end

@implementation eSynchronyFilterViewController

@synthesize filterAgeLabel;
@synthesize filterHeightLabel;
@synthesize filterEducationLabel;
@synthesize filterMarriageLabel;
@synthesize filterIncomeLabel;
@synthesize filterJobLabel;
@synthesize filterReligionLabel;
@synthesize filterEthnicityLabel;
@synthesize editItemArray, editItemType;

- (void)viewDidLoad {
    [super viewDidLoad];
    [scrlView addSubview:contentsView];
    scrlView.contentSize = contentsView.frame.size;
    
    FilterProfileSettingViewCtrl = [[FilterSettingViewController alloc] initWithNibName:@"FilterSettingViewController" bundle:nil];
    _mainMenuViewController = [[mainMenuViewController alloc]initWithNibName:@"mainMenuViewController" bundle:nil];
    editItemArray = [[NSMutableArray alloc] init];
    itemArray = [[NSMutableArray alloc] init];

}
- (void)getItemArrayFromPlist:(NSString *)name
{
    if( [name isEqualToString:@"income"] )
    {
        int nCtID = [eSyncronyAppDelegate sharedInstance].countryId;
        
        NSLog(@"Edit profile nCtID == %d",nCtID);
        
        if( nCtID == 0 )
            name = [name stringByAppendingString:@"_sg"];
        else if( nCtID == 1 )
            name = [name stringByAppendingString:@"_my"];
        else if( nCtID == 2 )
            name = [name stringByAppendingString:@"_hk"];
        else if( nCtID == 7)
            name = [name stringByAppendingString:@"_id"];
        else if (nCtID == 202)
            name = [name stringByAppendingString:@"_th"];
        else
            name = [name stringByAppendingString:@"_hk"];
    }
    NSString *_strPath =[[NSBundle mainBundle] pathForResource:name ofType:@"plist"];
    NSDictionary *_dicCountry = [NSDictionary dictionaryWithContentsOfFile:_strPath];
    
    [itemArray removeAllObjects];
    [itemArray addObjectsFromArray:[_dicCountry objectForKey:@"Array"]];
}

- (void)getEditItemArrayType:(NSInteger)type
{
    BOOL        bSingleSelect = NO;
    NSString    *str = @"";
    
    switch (type) {
        case FILTER_SETTING_AGE:
            bSingleSelect = YES;
            str = filterAgeLabel.text;
            break;
        case FILTER_SETTING_HEIGHT:
            bSingleSelect = YES;
            str = filterHeightLabel.text;
            break;
        case FILTER_SETTING_EDUCATION:
            bSingleSelect = YES;
            str = filterEducationLabel.text;
            break;
        case FILTER_SETTING_MARRIAGE:
            str = filterMarriageLabel.text;
            break;
        case FILTER_SETTING_INCOME:
            bSingleSelect = YES;
            str = filterIncomeLabel.text;
            break;
        case FILTER_SETTING_JOB:
            bSingleSelect = YES;
            str = filterJobLabel.text;
            break;
        case FILTER_SETTING_RELIGION:
            bSingleSelect = YES;
            str = filterReligionLabel.text;
            break;
        case FILTER_SETTING_ETHNICITY:
            bSingleSelect = YES;
            str = filterEthnicityLabel.text;
            break;
        }
    
    if( str && [str length] > 0 )
    {
        if( bSingleSelect == YES )
        {
            [editItemArray addObject:str];
            return;
        }
        
        NSArray *sarray_1 = [str componentsSeparatedByString:@", "];
        NSArray *sarray_2 = [str componentsSeparatedByString:@","];
        
        if( [sarray_1 count] >= [sarray_2 count] )
            [editItemArray addObjectsFromArray:sarray_1];
        else
            [editItemArray addObjectsFromArray:sarray_2];
    }
}

- (void)loadItemArrayWithName:(NSString *)name
{
    [editItemArray removeAllObjects];
    
    [self getItemArrayFromPlist:name];
    if( !itemArray || [itemArray count] < 1 )
        return;
    
    [self getEditItemArrayType:editItemType];
}

- (void)filter_receiveEditResultWithSelectedItemNumString:(NSString *)selNumStr
{
    NSString    *str;
    NSInteger   i, editItemCnt;
    
    editItemCnt = [editItemArray count];
    if( editItemCnt == 0 )
    {
        str = @"";
    }
    else
    {
        str = [editItemArray objectAtIndex:0];
        
        for( i = 1; i < editItemCnt; i++ ) {
            str = [NSString stringWithFormat:@"%@, %@", str, [editItemArray objectAtIndex:i]];
        }
    }
    
    switch( editItemType )
    {
        case FILTER_SETTING_AGE:
            filterAgeLabel.text = str;
            self.filterAgeSelStr = selNumStr;
            break;
        case FILTER_SETTING_HEIGHT:
            filterHeightLabel.text = str;
            self.filterHeightSelStr = selNumStr;
            break;
        case FILTER_SETTING_EDUCATION:
            filterEducationLabel.text = str;
            self.filterEducationSelStr = selNumStr;
            break;
        case FILTER_SETTING_MARRIAGE:
            filterMarriageLabel.text = str;
            self.filterMarriageSelStr = selNumStr;
            break;
        case FILTER_SETTING_INCOME:
            filterIncomeLabel.text = str;
            self.filterIncomeSelStr = selNumStr;
            break;
        case FILTER_SETTING_JOB:
            filterJobLabel.text = str;
            self.filterJobSelStr = selNumStr;
            break;
        case FILTER_SETTING_RELIGION:
            filterReligionLabel.text = str;
            self.filterReligionSelStr = selNumStr;
            break;
        case FILTER_SETTING_ETHNICITY:
            filterEthnicityLabel.text = str;
            self.filterEthnicitySelStr = selNumStr;
            break;
        }
}



//button action
- (IBAction)FilteronClickCancelled:(id)sender {
    
//    [self.navigationController popViewControllerAnimated:YES];
//    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.navigationController popViewControllerAnimated:YES];
    
//    [eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.mainRightLogoBtn.hidden = NO;
//    [eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.mainMenuRightLogoBtn.hidden = YES;
    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)FilteronClickNext:(id)sender {
    NSLog(@"going to commit data");
    
    //------------------ Get Profession Match Index ------------------//
    
    int             nProfession = [self.filterJobSelStr intValue];
    if (self.filterJobLabel.text.length == 0) {
        self.filterJobSelStr = nil;
    }
    //    NSLog(@"nProfeesion1 == %d",nProfession);
    else{
        if (nProfession != 32) {
            
            NSArray*        arrayProfession = [Global loadQuestionArray:@"profession"];
            NSArray*        arrayProfessionM = [Global loadQuestionArray:@"profession1"];
            NSString*       strProf = [arrayProfession objectAtIndex:nProfession];
            
            for( int k = 0; k < [arrayProfessionM count]; k++ )
            {
                NSString*   strItem = [arrayProfessionM objectAtIndex:k];
                if( [strItem isEqualToString:strProf] )
                {
                    if( k >= 25 )
                        nProfession = k+1;
                    else
                        nProfession = k;
                    break;
                }
            }
            
        }
    }
 
    [eSyncronyAppDelegate sharedInstance].strAge         = self.filterAgeLabel.text;
    [eSyncronyAppDelegate sharedInstance].strHeight      = self.filterHeightLabel.text;
    [eSyncronyAppDelegate sharedInstance].strEducation   = self.filterEducationSelStr;
    [eSyncronyAppDelegate sharedInstance].strMstatus     = self.filterMarriageSelStr;
    if (self.filterJobLabel.text.length == 0) {
        [eSyncronyAppDelegate sharedInstance].strJobtitle = [NSNumber numberWithInt:-1];
    }else
        [eSyncronyAppDelegate sharedInstance].strJobtitle    = [NSNumber numberWithInt:nProfession];
    
    [eSyncronyAppDelegate sharedInstance].strIncome      =  self.filterIncomeSelStr;
    [eSyncronyAppDelegate sharedInstance].strReligion    =  self.filterReligionSelStr;
    [eSyncronyAppDelegate sharedInstance].strEthnicity   =  self.filterEthnicitySelStr;
    
    [[eSyncronyAppDelegate sharedInstance] saveLoginInfo];
    
    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl dismissViewControllerAnimated:YES completion:nil];

}

- (IBAction)FilteronClickSave:(id)sender {
    //------------------ Get Profession Match Index ------------------//
    
    int             nProfession = [self.filterJobSelStr intValue];
    if (self.filterJobLabel.text.length == 0) {
        self.filterJobSelStr = nil;
    }
    //    NSLog(@"nProfeesion1 == %d",nProfession);
    else{
        if (nProfession != 32) {
            
            NSArray*        arrayProfession = [Global loadQuestionArray:@"profession"];
            NSArray*        arrayProfessionM = [Global loadQuestionArray:@"profession1"];
            NSString*       strProf = [arrayProfession objectAtIndex:nProfession];
            
            for( int k = 0; k < [arrayProfessionM count]; k++ )
            {
                NSString*   strItem = [arrayProfessionM objectAtIndex:k];
                if( [strItem isEqualToString:strProf] )
                {
                    if( k >= 25 )
                        nProfession = k+1;
                    else
                        nProfession = k;
                    break;
                }
            }
            
        }
    }
    
    [eSyncronyAppDelegate sharedInstance].strAge         = self.filterAgeLabel.text;
    [eSyncronyAppDelegate sharedInstance].strHeight      = self.filterHeightLabel.text;
    [eSyncronyAppDelegate sharedInstance].strEducation   = self.filterEducationSelStr;
    [eSyncronyAppDelegate sharedInstance].strMstatus     = self.filterMarriageSelStr;
    if (self.filterJobLabel.text.length == 0) {
        [eSyncronyAppDelegate sharedInstance].strJobtitle = [NSNumber numberWithInt:-1];
    }else
        [eSyncronyAppDelegate sharedInstance].strJobtitle    = [NSNumber numberWithInt:nProfession];
    
    [eSyncronyAppDelegate sharedInstance].strIncome      =  self.filterIncomeSelStr;
    [eSyncronyAppDelegate sharedInstance].strReligion    =  self.filterReligionSelStr;
    [eSyncronyAppDelegate sharedInstance].strEthnicity   =  self.filterEthnicitySelStr;
    
    [[eSyncronyAppDelegate sharedInstance] saveLoginInfo];
    
    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl dismissViewControllerAnimated:YES completion:nil];
//    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.navigationController popViewControllerAnimated:YES];

}

- (void)filter_addSettingView:(UIButton *)button
{
    [FilterProfileSettingViewCtrl initSetting];
    
    [button setHighlighted:NO];
    
    [self.view performSelector:@selector(addSubview:) withObject:FilterProfileSettingViewCtrl.view afterDelay:0.1];
}

-(void)filter_loadTableViewInfo:(id)sender withTitle:(NSString *)titleString{
    
    FilterProfileSettingViewCtrl.itemArray = itemArray;
    FilterProfileSettingViewCtrl.parent = self;
    FilterProfileSettingViewCtrl.titleString = NSLocalizedString(titleString,@"EditProfileView");
    FilterProfileSettingViewCtrl.isAllowMultySel = NO;
    
    [sender setHighlighted:YES];
    
    [self performSelector:@selector(filter_addSettingView:) withObject:sender afterDelay:0.1];
    
}

- (IBAction)filterdidClickAgeBtn:(id)sender {
    
//    NSLog(@"click age");
    self.editItemType = FILTER_SETTING_AGE;
    [self loadItemArrayWithName:@"filterAge"];
    [self filter_loadTableViewInfo:sender withTitle:@"Age"];
}

- (IBAction)filterdidClickHeihtBtn:(id)sender {
//    NSLog(@"click height");
    self.editItemType = FILTER_SETTING_HEIGHT;
    [self loadItemArrayWithName:@"filterHeight"];
    [self filter_loadTableViewInfo:sender withTitle:@"Height"];
}

- (IBAction)filterdidClickEducationBtn:(id)sender {
//    NSLog(@"click education");
    self.editItemType = FILTER_SETTING_EDUCATION;
    [self loadItemArrayWithName:@"education"];
    [self filter_loadTableViewInfo:sender withTitle:@"Education"];
}

- (IBAction)filterdidClickMarriageBtn:(id)sender {
//    NSLog(@"click marriage");
    self.editItemType = FILTER_SETTING_MARRIAGE;
    [self loadItemArrayWithName:@"marital_status"];
    [self filter_loadTableViewInfo:sender withTitle:@"Marriage Status"];
    
}
- (IBAction)filterdidClickIncomeBtn:(id)sender {
//    NSLog(@"click income");
    self.editItemType = FILTER_SETTING_INCOME;
    [self loadItemArrayWithName:@"income"];
    [self filter_loadTableViewInfo:sender withTitle:@"Income Level"];
}

- (IBAction)filterdidClickJobBtn:(id)sender {
//    NSLog(@"click job btn");
    self.editItemType = FILTER_SETTING_JOB;
    [self loadItemArrayWithName:@"profession"];
    [self filter_loadTableViewInfo:sender withTitle:@"Job Title"];
}

- (IBAction)filterdidClickReligionBtn:(id)sender {
//    NSLog(@"click religion");
    self.editItemType = FILTER_SETTING_RELIGION;
    [self loadItemArrayWithName:@"religion"];
    [self filter_loadTableViewInfo:sender withTitle:@"Religion"];
}

- (IBAction)filterdidClickEthnicityBtn:(id)sender {
//    NSLog(@"click ethnicity");
    self.editItemType = FILTER_SETTING_ETHNICITY;
    [self loadItemArrayWithName:@"ethnicity"];
    [self filter_loadTableViewInfo:sender withTitle:@"Ethnicity"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)dealloc {
    [scrlView release];
    [contentsView release];
    
    [filterAgeLabel release];
    [filterHeightLabel release];
    [filterEducationLabel release];
    [filterMarriageLabel release];
    [filterIncomeLabel release];
    [filterJobLabel release];
    [filterReligionLabel release];
    [filterEthnicityLabel release];
    
    
    
    [_filterAgeSelStr release];
    [_filterHeightSelStr release];
    [_filterEducationSelStr release];
    [_filterMarriageSelStr release];
    [_filterIncomeSelStr release];
    [_filterJobSelStr release];
    [_filterReligionSelStr release];
    [_filterEthnicitySelStr release];
    
    editItemArray = nil;
    [editItemArray release];
    
    
    [filterAgeBtn release];
    [filterHeightBtn release];
    [filterEducationBtn release];
    [filterMarriageBtn release];
    [filterIncomeBtn release];
    [filterJobBtn release];
    [filterReligionBtn release];
    [filterEthnicityBtn release];
    [super dealloc];
    
}


@end

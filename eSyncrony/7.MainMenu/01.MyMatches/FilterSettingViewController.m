//
//  FilterSettingViewController.m
//  eSyncrony
//
//  Created by ESYNSZ-Limit on 16/2/1.
//  Copyright © 2016年 WonMH. All rights reserved.
//
#import "eSynchronyFilterViewController.h"
#import "RegisterTableViewCell.h"
#import "FilterSettingViewController.h"
#import "Global.h"

@interface FilterSettingViewController ()

@end

@implementation FilterSettingViewController
@synthesize itemArray, parent, titleString, tblView, isAllowMultySel, selNumStr;


- (void)viewDidLoad {
    [super viewDidLoad];
    selView.layer.cornerRadius = 5;
    
    selNumStr = [[NSString alloc] init];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self initSetting];
}
- (void)initSetting
{
    // remove selview
    for( UIView *v in [self.view subviews] ) {
        if( v.tag == 100 ) {
            [v removeFromSuperview];
        }
    }
    
    titleLabel.text = self.titleString;
    
    tblView.allowsMultipleSelection = isAllowMultySel;
    
    // resizing of subview
    NSInteger itemCnt = [self.itemArray count];
    
    NSInteger showItemCnt = (itemCnt < 6)? itemCnt: 6;
    float      tblViewHeight;
    
    tblViewHeight = 42 * showItemCnt;
    
    tblView.frame = CGRectMake(tblView.frame.origin.x, tblView.frame.origin.y, tblView.frame.size.width, tblViewHeight);
    deviderView.frame = CGRectMake(deviderView.frame.origin.x, tblView.frame.origin.y + tblViewHeight + 3, deviderView.frame.size.width, 2);
    okBtn.frame = CGRectMake(okBtn.frame.origin.x, deviderView.frame.origin.y + 21, okBtn.frame.size.width, okBtn.frame.size.height);
    
    selView.frame = CGRectMake(20, (self.view.frame.size.height - okBtn.frame.origin.y - 44)/2, selView.frame.size.width, okBtn.frame.origin.y + 44);
    
    // reloading of table
    [tblView reloadData];
    
    for( NSString *str in parent.editItemArray )
    {
        for( int i = 0; i < [itemArray count]; i++ )
        {
            if( [str isEqualToString:[itemArray objectAtIndex:i]] )
            {
                [tblView selectRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
                break;
            }
        }
    }
    
    [tblView setContentOffset:CGPointMake(0, 0)];
    
    // add subview
    [self.view performSelector:@selector(addSubview:) withObject:selView afterDelay:0.1];
    
}
#pragma mark --- Table Functions ---
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.itemArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 42;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RegisterTableViewCell *cell;
    NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"RegisterTableViewCell" owner:nil options:nil];
    cell = [arr objectAtIndex:0];
    cell.lbContent.text = [self.itemArray objectAtIndex:indexPath.row];
    cell.selectedItemArray = parent.editItemArray;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)IndexPath
{
//    NSArray*    arrIndPaths = tableView.indexPathsForSelectedRows;
//    if (arrIndPaths != nil) {
//        //取消选中状态
//        [tableView deselectRowAtIndexPath:IndexPath animated:YES];
//    }
 
}
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{

//    NSArray*    arrIndPaths = tableView.indexPathsForSelectedRows;
    
}
- (IBAction)didClickOKBtn:(id)sender
{
    NSArray *selecedIndexPathArry = [tblView indexPathsForSelectedRows];
    
    [parent.editItemArray removeAllObjects];
    
    BOOL        isOneSel = YES;
    
    NSIndexPath *indexPath;
    
    self.selNumStr = @"";
    
    for( int i = 0; i < [selecedIndexPathArry count]; i++ )
    {
        indexPath = [selecedIndexPathArry objectAtIndex:i];
        [parent.editItemArray addObject:[itemArray objectAtIndex:indexPath.row]];
        
        if( i == 0 )
        {
            self.selNumStr = [NSString stringWithFormat:@"%d", (int)indexPath.row];
        }
        else
        {
            isOneSel = NO;
            self.selNumStr = [NSString stringWithFormat:@"%@|%d", selNumStr, (int)indexPath.row];
        }
    }
    
    if( !isOneSel )
        self.selNumStr = [NSString stringWithFormat:@"%@|", selNumStr];
    NSLog(@"selNumStr == %@",selNumStr);
    
    [parent filter_receiveEditResultWithSelectedItemNumString:selNumStr];
    
    [self.view removeFromSuperview];
}

- (IBAction)didClickCancelledBtn:(id)sender {
    
    NSIndexPath *indexPath = nil;
    [tblView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self.view removeFromSuperview];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)dealloc {
    [selView release];
    [titleLabel release];
    [tblView release];
    [deviderView release];
    [okBtn release];
    [itemArray release];
    [titleString release];
    [selNumStr release];
    
    [parent release];
    
    [super dealloc];
}
@end

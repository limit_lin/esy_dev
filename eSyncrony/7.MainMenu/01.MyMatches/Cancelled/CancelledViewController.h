
#import <UIKit/UIKit.h>

#import "mainMenuViewController.h"
#import "MyMatchesCancelledCell.h"

@interface CancelledViewController : GAITrackedViewController <MyMatchesCancelledCellDelegate>
{
    IBOutlet UITableView    *listTblView;
    IBOutlet UILabel        *lblNoMatches;
    
    NSMutableArray          *arrMatchesItems;
}

@property (nonatomic, assign) mainMenuViewController    *mainMenuView;

- (void)refreshContent;

@end

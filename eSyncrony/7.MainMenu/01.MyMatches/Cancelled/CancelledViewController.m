
#import "Global.h"
#import "DSActivityView.h"
#import "eSyncronyAppDelegate.h"

#import "MyMatchesCancelledCell.h"
#import "mainMenuViewController.h"
#import "MatchProfileViewController.h"
#import "CancelledViewController.h"
#import "eSyncronyAppDelegate.h"

//http://stackoverflow.com/questions/4317413/incorrect-nsstringencoding-value-0x0000-detected
@interface CancelledViewController ()

@end

@implementation CancelledViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"CancelledMatchesView";
    arrMatchesItems = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self refreshContent];
}

- (void)refreshContent
{
    lblNoMatches.hidden = YES;
    
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(esyncCancelled_loadInfos) withObject:nil afterDelay:0.1];
}

- (void)esyncCancelled_loadInfos
{
    NSDictionary* result = [UtilComm loadMatchesInfoWithTypeNew:@"4"];
    [DSBezelActivityView removeViewAnimated:NO];
    
    if( result == nil )
    {
//        [ErrorProc alertToCheckInternetStateTitle:@"Error"];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [self.mainMenuView onServerConnectionError:response];
        return;
    }
    
    [arrMatchesItems release];
    
    arrMatchesItems = [[NSMutableArray alloc] initWithCapacity:0];
    
    NSDictionary*   responseDic = (NSDictionary*)response;
    if( [responseDic isKindOfClass:[NSDictionary class]] )
    {
        NSArray *arrItems = [responseDic objectForKey:@"item"];

        if( [arrItems isKindOfClass:[NSDictionary class]] )
        {
            NSDictionary* item = (NSDictionary*)arrItems;
            NSMutableDictionary *mutItem = [NSMutableDictionary dictionaryWithDictionary:item];
            
            [arrMatchesItems addObject:mutItem];
        }
        else if( [arrItems isKindOfClass:[NSArray class]] )
        {
            for( int i = 0; i < [arrItems count]; i++ )
            {
                id dicanon = [arrItems objectAtIndex:i];
                if ([dicanon isKindOfClass:[NSDictionary class]]) {
                    NSMutableArray *arranon = [NSMutableArray new];
                    if ([[dicanon objectForKey:@"anon"] isKindOfClass:[NSArray class]]) {
                        arranon = (NSMutableArray *)[dicanon objectForKey:@"anon"];
                    }else{
                        NSDictionary *dic = [dicanon objectForKey:@"anon"];
                        [arranon addObject:dic];
                    }
                    //NSLog(@"%d",arranon.count);
                    
                    for( int i = 0; i < [arranon count]; i++ )
                    {
                        NSDictionary        *item = [arranon objectAtIndex:i];
                        NSMutableDictionary *mutItem = [NSMutableDictionary dictionaryWithDictionary:item];
                        
                        [arrMatchesItems addObject:mutItem];
                    }//for
                }//if
            }//for
        }//elseif
    }//if

    [listTblView reloadData];
    
    if( [arrMatchesItems count] == 0 )
    {
        lblNoMatches.hidden = NO;
        listTblView.scrollEnabled = NO;
    }
    else
    {
        listTblView.scrollEnabled = YES;
        lblNoMatches.hidden = YES;
    }
}

#pragma mark --- Table Functions ---
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if( arrMatchesItems == nil )
        return 0;
    
    return [arrMatchesItems count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 130;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary     *dicMatchesInfo = [arrMatchesItems objectAtIndex:indexPath.row];
    MyMatchesCancelledCell *cell;
    
//    NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"MyMatchesCancelledCell" owner:nil options:nil];
//    cell = [arr objectAtIndex:0];
    
    static NSString *CellIdentifier = @"CancelledIdentifier";
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        UINib *nib = [UINib nibWithNibName:@"MyMatchesCancelledCell" bundle:nil];
        [tableView registerNib:nib forCellReuseIdentifier:CellIdentifier];
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    }
    
    cell.delegate = self;
    [cell setMatchesInfo:dicMatchesInfo];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary             *dicMatchesInfo = [arrMatchesItems objectAtIndex:indexPath.row];
    MatchProfileViewController      *matchInfoViewCtrl = [MatchProfileViewController createWithMatchInfo:dicMatchesInfo];
    
    matchInfoViewCtrl.m_bTopButton = YES;
    matchInfoViewCtrl.m_bDismissSelf = YES;
    matchInfoViewCtrl.isAlreadyDecline = YES;
    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.navigationController pushViewController:matchInfoViewCtrl animated:YES];

    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (void)dealloc
{
    [listTblView release];
    [lblNoMatches release];
    
    [arrMatchesItems release];
    
    [super dealloc];
}

@end

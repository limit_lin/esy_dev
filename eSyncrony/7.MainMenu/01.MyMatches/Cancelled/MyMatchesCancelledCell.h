
#import <UIKit/UIKit.h>

@protocol MyMatchesCancelledCellDelegate <NSObject>

@optional

- (void)refreshContent;

@end

@interface MyMatchesCancelledCell : UITableViewCell
{
    IBOutlet UIImageView    *photoBGView;
    IBOutlet UIImageView    *photoImgView;
    IBOutlet UIImageView    *photoFrameView;

    IBOutlet UIImageView    *rightView;

    IBOutlet UILabel        *nameLabel;
    IBOutlet UILabel        *ageLabel;
    
    IBOutlet UILabel        *lblCommentTitle;
    IBOutlet UILabel        *lblComment1;

    
    NSMutableDictionary*   _dicMatchesInfo;

    BOOL    bGotoUpgrade;
}
@property (retain, nonatomic) IBOutlet UILabel *lblClick;
@property (retain, nonatomic) IBOutlet UIButton *btnClick;

@property (nonatomic, assign) id<MyMatchesCancelledCellDelegate> delegate;

- (void)setMatchesInfo:(NSMutableDictionary*)dicMatchesInfo;

- (IBAction)didClickHere:(id)sender;

@end

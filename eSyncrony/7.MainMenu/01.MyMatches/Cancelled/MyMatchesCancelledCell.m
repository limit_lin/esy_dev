
#import "Global.h"
#import "UtilComm.h"
#import "DSActivityView.h"
#import "ImageUtil.h"
#import "UIImageView+WebCache.h"
#import "MyMatchesCancelledCell.h"
#import "mainMenuViewController.h"

#import "eSyncronyAppDelegate.h"


@implementation MyMatchesCancelledCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)loadDefaultImage{
    if( [eSyncronyAppDelegate sharedInstance].gender == 1 ){
        photoImgView.image = [UIImage imageNamed:@"male1.png"];
    }else {
        photoImgView.image = [UIImage imageNamed:@"female1.png"];
    }
}

- (BOOL)downloadMatchUserPhoto
{
    NSString        *macc_no = [_dicMatchesInfo objectForKey:@"acc_no"];
    if( macc_no == nil )
        macc_no = [_dicMatchesInfo objectForKey:@"Acc_no"];
    if( macc_no == nil )
        return FALSE;
  
    NSString    *view_photo = [_dicMatchesInfo objectForKey:@"view_photo"];
    
    
    if( [view_photo isEqualToString:@"photo view not allowed."] )
        return FALSE;
    
    NSString *filename = [_dicMatchesInfo objectForKey:@"filename"];
    NSString*   imageURL = [NSString stringWithFormat:@"%@%@", WEBSERVICE_PHOTO_BASEURI, filename];
    NSString*   imagePath = [ImageUtil loadImagePathFromURL:imageURL];
    
    [self performSelectorOnMainThread:@selector(CancelledCell_onDownLoadImage:) withObject:imagePath waitUntilDone:YES];
    
    return TRUE;
}

- (void)CancelledCell_onDownLoadImage:(NSString*)imagePath
{
    UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
    if( image != nil )
    {
        photoFrameView.hidden = NO;
        photoImgView.image = image;
        [_dicMatchesInfo setObject:imagePath forKey:@"localphoto_path"];
    }
    else
        [_dicMatchesInfo setObject:@"nophoto" forKey:@"localphoto_path"];
}

- (void)setMatchesInfo:(NSMutableDictionary*)dicMatchesInfo
{
    _dicMatchesInfo = dicMatchesInfo;
    
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
    
    photoBGView.layer.cornerRadius = 5;
    photoImgView.layer.cornerRadius = 45;
    photoImgView.clipsToBounds = YES;
    photoFrameView.hidden = YES;
    
    rightView.layer.cornerRadius = 5;
    
    nameLabel.text = [dicMatchesInfo objectForKey:@"nname"];
//    [nameLabel sizeToFit];
    CGSize nameLabelSize = [nameLabel.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:nameLabel.font,NSFontAttributeName, nil]];
    nameLabel.frame = CGRectMake(120, 20, nameLabelSize.width, nameLabelSize.height);
    
    CGFloat xPos = nameLabel.frame.origin.x + nameLabel.frame.size.width;
    CGFloat yPos = nameLabel.frame.origin.y;
    
    ageLabel.text = [NSString stringWithFormat:@", %@", [dicMatchesInfo objectForKey:@"age"]];
    CGRect frame = ageLabel.frame;
    frame.origin.x = xPos;
    frame.origin.y = yPos;
    ageLabel.frame = frame;
    [ageLabel sizeToFit];
    
    [lblCommentTitle sizeToFit];
    lblComment1.text =NSLocalizedString([dicMatchesInfo objectForKey:@"message"],@"CancelledMatchesView");
//    [lblComment1 sizeToFit];
    lblComment1.adjustsFontSizeToFitWidth = YES;
    if ([[dicMatchesInfo objectForKey:@"message"]hasPrefix:@"You have rejected this match"]) {
        self.lblClick.hidden = NO;
        self.btnClick.hidden = NO;
    }else{
        self.lblClick.hidden = YES;
        self.btnClick.hidden = YES;
    }
    
    NSString *strImage = nil;
    if( ![[_dicMatchesInfo objectForKey:@"view_photo"] isEqualToString:@"photo view not allowed."] ){
        strImage = [NSString stringWithFormat:@"%@%@", WEBSERVICE_PHOTO_BASEURI, [_dicMatchesInfo objectForKey:@"filename"]];
    }
    
    NSString *strPlaceholder;
    if( [eSyncronyAppDelegate sharedInstance].gender == 1 ){
        strPlaceholder = @"male1.png";
    }else {
        strPlaceholder = @"female1.png";
    }
    
    [photoImgView sd_setImageWithURL:[NSURL URLWithString:strImage] placeholderImage:[UIImage imageNamed:strPlaceholder] ];
    
    
//    if( [eSyncronyAppDelegate sharedInstance].gender == 1 )
//        photoImgView.image = [UIImage imageNamed:@"male1.png"];
//
//    NSString    *imagePath = [_dicMatchesInfo objectForKey:@"localphoto_path"];
//    
//    if( imagePath != nil )
//    {
//        UIImage     *image = [UIImage imageWithContentsOfFile:imagePath];
//        if( image != nil )
//            photoImgView.image = image;
//    }
//    else
//    {
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,
//                                                 (unsigned long)NULL), ^(void) {
//            [_dicMatchesInfo retain];
//            if( [self downloadMatchUserPhoto] == FALSE )
//            {
//                [_dicMatchesInfo setObject:@"nophoto" forKey:@"localphoto_path"];
//            }
//            [dicMatchesInfo release];
//        });
//    }
}

- (void)CancellView_requestApproveToSever
{
    [DSBezelActivityView removeViewAnimated:NO];

    NSString        *macc_no = [_dicMatchesInfo objectForKey:@"acc_no"];
    if( macc_no == nil )
        macc_no = [_dicMatchesInfo objectForKey:@"Acc_no"];
    if( macc_no == nil )
        return;

    NSDictionary    *result = [UtilComm approveToMatch:macc_no];
    
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"eSynchrony"];
        return;
    }
    
    NSString    *response = [result objectForKey:@"response"];
    if( ![response isKindOfClass:[NSString  class]] )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Error",)];
        return;
    }
    
    if( [response hasPrefix:@"ERROR"] )
    {
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
        return;
    }
    
    if( [response isEqualToString:@"YOU GOT A MATCH"]||[response isEqualToString:@"You have a mutual match!!"] )
    {
        bGotoUpgrade = FALSE;
                
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"The page at https://www.esynchrony.com says:",@"MyMatchesPendingCell")
                                                     message:[NSString stringWithFormat:NSLocalizedString(@"Confirm to arrange date with %@",@"MyMatchesPendingCell"),nameLabel.text]
                                                    delegate:self
                                           cancelButtonTitle:NSLocalizedString(@"OK",)
                                           otherButtonTitles:NSLocalizedString(@"Cancel",), nil];
        
        [av show];
        [av release];
    }
    else
    {
        bGotoUpgrade = TRUE;
        
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"eSynchrony"
                                                     message:response
                                                    delegate:self
                                           cancelButtonTitle:NSLocalizedString(@"OK",)
                                           otherButtonTitles:nil, nil];
        [av show];
        [av release];
    }

}

- (IBAction)didClickHere:(id)sender
{
    UIView  *parentView = [eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.view;
    [DSBezelActivityView newActivityViewForView:parentView withLabel:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(CancellView_requestApproveToSever) withObject:nil afterDelay:0.1];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if( bGotoUpgrade == FALSE )
        [self.delegate refreshContent];
    else
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl procUpgrade];
}

-(void)dealloc
{
    [photoImgView release];
    [rightView release];
    
    [nameLabel release];
    [ageLabel release];
    
    [lblCommentTitle release];
    [lblComment1 release];

    
    [_lblClick release];
    [_btnClick release];
    [super dealloc];
}

@end

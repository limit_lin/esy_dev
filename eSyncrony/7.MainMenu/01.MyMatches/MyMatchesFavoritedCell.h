//
//  MyMatchesFavoritedCell.h
//  eSyncrony
//
//  Created by ESYNSZ-Limit on 16/2/14.
//  Copyright © 2016年 WonMH. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MyMatchesFavoritedCellDelegate <NSObject>

@optional

- (void)refreshContent;

@end

@interface MyMatchesFavoritedCell : UITableViewCell
{
    
    IBOutlet UIImageView *photoBGView;
    IBOutlet UIImageView *photoFrameView;
    IBOutlet UIImageView *photoImgView;
    
    IBOutlet UIImageView *rightView;
    
    IBOutlet UILabel *matchedIdxLabel;
    
    NSMutableDictionary*   _dicMatchesInfo;
    NSMutableArray          *arrMatchesItems;
    
    BOOL    bGotoUpgrade;
}


@property (retain, nonatomic) IBOutlet UILabel *nameLabel;

@property (nonatomic, assign) id<MyMatchesProposedCellDelegate> delegate;
- (void)setMatchesFavoriteInfo:(NSMutableDictionary*)dicMatchesInfo;
@property (nonatomic, assign) id delegateListViewCtrl;
@property (nonatomic,strong)NSString *name;



- (IBAction)didClickView:(id)sender;
- (IBAction)didClickApprove:(id)sender;
- (IBAction)didClickCancellFav:(id)sender;


@end

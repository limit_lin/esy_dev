
#import "PendingTopCell.h"
#import "PendingViewController.h"
#import "Global.h"

@implementation PendingTopCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)dealloc
{
    [myApprovalBtn release];
    [approvalBtn release];
    [lblApprove release];
    [lblMyApprove release];


    [super dealloc];
}

- (void)selectLeft
{
    [myApprovalBtn setSelected:YES];
    [approvalBtn setSelected:NO];
    
    lblApprove.textColor = TITLE_TEXT_COLLOR;
    lblMyApprove.textColor = [UIColor whiteColor];
}

- (void)selectRight
{
    [myApprovalBtn setSelected:NO];
    [approvalBtn setSelected:YES];
    
    lblMyApprove.textColor = TITLE_TEXT_COLLOR;
    lblApprove.textColor = [UIColor whiteColor];
}

- (IBAction)didClickWaitingForMyApproal:(id)sender
{
    [self selectLeft];
    
    [self.pendingViewCtrl didClickWaitingForMyApproal];
}

- (IBAction)didClickWaitingForApproal:(id)sender
{
    [self selectRight];

    [self.pendingViewCtrl didClickWaitingForApproal];
}

@end


#import <UIKit/UIKit.h>
#import "PendingViewController.h"

@interface PendingTopCell : UITableViewCell
{
    IBOutlet UIButton   *myApprovalBtn;
    IBOutlet UIButton   *approvalBtn;
    
    IBOutlet UILabel    *lblMyApprove;
    IBOutlet UILabel    *lblApprove;
}

@property (nonatomic, assign) PendingViewController *pendingViewCtrl;


- (void)selectLeft;
- (void)selectRight;

- (IBAction)didClickWaitingForMyApproal:(id)sender;
- (IBAction)didClickWaitingForApproal:(id)sender;

@end


#import "Global.h"
#import "DSActivityView.h"
#import "eSyncronyAppDelegate.h"
#import "mainMenuViewController.h"
#import "MatchProfileViewController.h"

#import "PendingViewController.h"
#import "PendingTopCell.h"
#import "PendingSecondCell.h"
#import "MyMatchesPendingCell.h"
//#import "MyMatchesProposedCell.h"
#import "MyMatchesPendingProposedCell.h"

@implementation PendingViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"PendingMatchesView";
    arrMatchesItems = nil;
    isSelectedMyApproval = YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self refreshContent];
}

- (void)refreshContent
{

    listTblView.separatorColor = [UIColor clearColor];
    
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(esyncPending_loadInfos) withObject:nil afterDelay:0.1];
}

- (void)esyncPending_loadInfos
{
    NSString*   matchType;
    if( isSelectedMyApproval )
        matchType = @"2a";
    else
        matchType = @"2b";

    NSDictionary* result = [UtilComm loadMatchesInfoWithTypeNew:matchType];
    
    
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Error"];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
        return;
    }
    
    [arrMatchesItems release];
    arrMatchesItems = [[NSMutableArray alloc] initWithCapacity:0];
    
    if( [response isKindOfClass:[NSDictionary class]] )
    {
        NSArray *arrItems = [response objectForKey:@"item"];
        
        if( [arrItems isKindOfClass:[NSDictionary class]] )
        {
            NSDictionary* item = (NSDictionary*)arrItems;
            NSMutableDictionary *mutItem = [NSMutableDictionary dictionaryWithDictionary:item];
            
            [arrMatchesItems addObject:mutItem];
        }
        else if( [arrItems isKindOfClass:[NSArray class]] )
        {
            for( int i = 0; i < [arrItems count]; i++ )
            {
                NSDictionary  *item = [arrItems objectAtIndex:i];
                
                
                [arrMatchesItems addObject:item];
            }
        }
        
    }
    
    [listTblView reloadData];
    [DSBezelActivityView removeViewAnimated:NO];
    
    if( [arrMatchesItems count] == 0 )
    {
        self.lblNoMatches.hidden = NO;
        listTblView.scrollEnabled = NO;
        listTblView.separatorColor = [UIColor clearColor];
    }
    else
    {
        self.lblNoMatches.hidden = YES;
        listTblView.scrollEnabled = YES;
        listTblView.separatorColor = [UIColor lightGrayColor];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if( arrMatchesItems == nil ){

        return 2;
    }else{

        return [arrMatchesItems count]+2;
    }
   
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( indexPath.row == 0 )
        return 62;
    if( indexPath.row == 1 )
        return 44;
    else
        return 130;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( indexPath.row == 0 )
    {
        PendingTopCell *cell;
        NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"PendingTopCell" owner:nil options:nil];
        cell = [arr objectAtIndex:0];
        cell.pendingViewCtrl = self;
        
        if( isSelectedMyApproval == YES )
            [cell selectLeft];
        else
            [cell selectRight];

        return cell;
    }
    if( indexPath.row == 1 )
    {
        PendingSecondCell *cell;
        NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"PendingSecondCell" owner:nil options:nil];
        cell = [arr objectAtIndex:0];
        
        
        if( isSelectedMyApproval == YES )
            cell.lblInstructions.text = NSLocalizedString(@"These are the matches who are keen to meet up with you and have not received your approval yet.",@"PendingView");
        else
            cell.lblInstructions.text = NSLocalizedString(@"You will also see matches you have approved and pending their responses.",@"PendingView");
        
        return cell;
    }
    
    if( isSelectedMyApproval == YES )
    {
        
        NSMutableDictionary     *dicMatchesInfo = [arrMatchesItems objectAtIndex:indexPath.row-2];
//        MyMatchesProposedCell   *cell;
        MyMatchesPendingProposedCell *cell;
        
//        NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"MyMatchesProposedCell" owner:nil options:nil];
//        cell = [arr objectAtIndex:0];
        
//        static NSString *CellIdentifier = @"ProposedIdentifier";
        static NSString *CellIdentifier = @"PendingProposedIdentifier";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
//            UINib *nib = [UINib nibWithNibName:@"MyMatchesProposedCell" bundle:nil];
//            [tableView registerNib:nib forCellReuseIdentifier:CellIdentifier];
//            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            cell=[[[NSBundle mainBundle]loadNibNamed:@"MyMatchesPendingProposedCell" owner:self options:nil]objectAtIndex:0];
        }
        
        cell.delegate = self;
        cell.delegateListViewCtrl = self;
        cell.cansee = YES;
//       [cell setMatchesInfo:dicMatchesInfo];
        
        [cell setMatchesPendingInfo:dicMatchesInfo];

        return cell;
    }
    
    NSMutableDictionary     *dicMatchesInfo = [arrMatchesItems objectAtIndex:indexPath.row-2];
    MyMatchesPendingCell    *cell;
    
    NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"MyMatchesPendingCell" owner:nil options:nil];
    cell = [arr objectAtIndex:0];
    
    //cell.parentViewCtrl = self;
    [cell setMatchesPendingInfo:dicMatchesInfo];
    cell.delegateListViewCtrl = self;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( indexPath.row == 0 ||indexPath.row == 1)
        return;
    
    NSMutableDictionary         *dicMatchesInfo = [arrMatchesItems objectAtIndex:indexPath.row-2];
    MatchProfileViewController  *matchInfoViewCtrl = [MatchProfileViewController createWithMatchInfo:dicMatchesInfo];
//    if( isSelectedMyApproval == YES )
//    {
//        matchInfoViewCtrl.m_bTopButton = YES;
//    }else{
//        matchInfoViewCtrl.m_bTopButton = NO;
//    }
    matchInfoViewCtrl.m_bTopButton = YES;
    matchInfoViewCtrl.m_bDismissSelf = YES;
    matchInfoViewCtrl.delegateListViewCtrl = self;
    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.navigationController pushViewController:matchInfoViewCtrl animated:YES];
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (void)didClickWaitingForMyApproal
{
    if( isSelectedMyApproval == YES )
        return;

    isSelectedMyApproval = YES;

    [self refreshContent];
}

- (void)didClickWaitingForApproal
{
    if( isSelectedMyApproval == NO )
        return;
    
    isSelectedMyApproval = NO;
    [self refreshContent];
}

- (void)dealloc
{
    [listTblView release];

    [arrMatchesItems release];
    
    [_lblNoMatches release];
    [super dealloc];
}

@end

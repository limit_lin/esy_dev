//
//  MyMatchesPendingProposedCellTableViewCell.m
//  eSyncrony
//
//  Created by ESYNSZ-Limit on 15/8/6.
//  Copyright (c) 2015年 WonMH. All rights reserved.
//
#import "Global.h"
#import "UtilComm.h"
#import "DSActivityView.h"
#import "ImageUtil.h"
#import "UIImageView+WebCache.h"

#import "MyMatchesPendingProposedCell.h"
#import "mainMenuViewController.h"
#import "MatchProfileViewController.h"

#import "MyMatchesPendingCell.h"
//#import "MyMatchesProposedViewController.h"

#import "eSyncronyAppDelegate.h"

@implementation MyMatchesPendingProposedCell
@synthesize name;

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)loadDefaultImage{
    if( [eSyncronyAppDelegate sharedInstance].gender == 1 ){
        photoImgView.image = [UIImage imageNamed:@"male1.png"];
    }else {
        photoImgView.image = [UIImage imageNamed:@"female1.png"];
    }
}

- (BOOL)downloadMatchUserPhoto
{
    NSString        *macc_no = [_dicMatchesInfo objectForKey:@"acc_no"];
    if( macc_no == nil )
        macc_no = [_dicMatchesInfo objectForKey:@"Acc_no"];
    if( macc_no == nil )
        return FALSE;
    
    if( [[_dicMatchesInfo objectForKey:@"view_photo"] isEqualToString:@"photo view not allowed."] )
        return FALSE;
//    NSLog(@"downloadMatchUserPhoto");
    NSString*   imageURL = [NSString stringWithFormat:@"%@%@", WEBSERVICE_PHOTO_BASEURI, [_dicMatchesInfo objectForKey:@"filename"]];
    NSString*   imagePath = [ImageUtil loadImagePathFromURL:imageURL];
    
    [self performSelectorOnMainThread:@selector(PendingPropoedCell_onDownLoadImage:) withObject:imagePath waitUntilDone:YES];
    
    return TRUE;
}

- (void)PendingPropoedCell_onDownLoadImage:(NSString*)imagePath
{
    UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
    
//    UIImage *image =  [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imagePath]]];
    if( image != nil )
    {
//      photoFrameView.hidden = NO;
        photoImgView.image = image;
        [_dicMatchesInfo setObject:imagePath forKey:@"localphoto_path"];

    }
    else{
        [_dicMatchesInfo setObject:@"nophoto" forKey:@"localphoto_path"];
    }

}

- (void)setMatchesPendingInfo:(NSMutableDictionary*)dicMatchesInfo
{

    _dicMatchesInfo = dicMatchesInfo;

    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];

    photoBGView.layer.cornerRadius = 5;
    photoImgView.layer.cornerRadius = 45;
    photoImgView.clipsToBounds = YES;
    photoFrameView.hidden = YES;

    rightView.layer.cornerRadius = 5;

    self.nameLabel.text = [dicMatchesInfo objectForKey:@"nname"];

    ///////////////////当view_status=@"Y"-->>have seen,字体应该不变/////////////////////////
    NSString *view_status = view_status=[dicMatchesInfo objectForKey:@"view_status"];

    //NSLog(@"dicMatchesInfo==%@",[_dicMatchesInfo objectForKey:@"nname"]);

    if ([view_status isEqualToString:@"Y"]) {
        self.nameLabel.font=[UIFont systemFontOfSize:15.0];
    }
    else if([view_status isEqualToString:@"N"])
    {
        self.nameLabel.font=[UIFont boldSystemFontOfSize:15.0];
    }

    ///////////////////////////////////////////////////////

    [self.nameLabel sizeToFit];


    CGFloat xPos = self.nameLabel.frame.origin.x + self.nameLabel.frame.size.width;
    CGFloat yPos = self.nameLabel.frame.origin.y;
    
    ageLabel.text = [NSString stringWithFormat:@", %@", [dicMatchesInfo objectForKey:@"age"]];
    CGRect frame = ageLabel.frame;
    frame.origin.x = xPos;
    
    if ([[eSyncronyAppDelegate sharedInstance].language hasPrefix:@"zh-Hant"]||[[eSyncronyAppDelegate sharedInstance].language hasPrefix:@"zh-HK"]) {
        
        frame.origin.y = yPos;//Chinese interface optimization
    }
    
    ageLabel.frame = frame;
    [ageLabel sizeToFit];

    //matchedIdxLabel.text = [dicMatchesInfo objectForKey:@"acc_no"];
    NSString    *distance = [dicMatchesInfo objectForKey:@"distance"];
    if( distance == nil )
        distance = [dicMatchesInfo objectForKey:@"Distance"];

    matchedIdxLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Match Index : %@",@"MyMatchesPendingCell"),distance];

    [matchedIdxLabel sizeToFit];

    if ([dicMatchesInfo objectForKey:@"message"]!=nil) {
        storyLabel.text =NSLocalizedString([dicMatchesInfo objectForKey:@"message"],@"MyMatchesPendingProposedCell");
        
    }else {
        id story3 = [dicMatchesInfo objectForKey:@"story3"];

        if (story3 == nil)
        {
            story3 = [story3 stringByReplacingOccurrencesOfString:@"\r\n" withString:@" "];
            story3 = [story3 stringByReplacingOccurrencesOfString:@"\r" withString:@" "];
        }

        if( [story3 isKindOfClass:[NSString class]] )

            storyLabel.text = [NSString stringWithFormat:NSLocalizedString(@"My friends say I'm \"%@\"",@"MatchesProposedView"),story3];//0706
//          storyLabel.text = [NSString stringWithFormat:@"My friends say I'm \"%@\"", story3];
        else
            storyLabel.text = [NSString stringWithFormat:NSLocalizedString(@"My friends say I'm \" \"",@"MatchesProposedView")];//0706
//            storyLabel.text = [NSString stringWithFormat:@"My friends say I'm \" \""];
    }


    CGRect rect = storyLabel.frame;
    CGSize size = CGSizeMake(180.f, 40);
    CGSize actualsize = [storyLabel.text sizeWithFont:storyLabel.font constrainedToSize:size lineBreakMode:NSLineBreakByTruncatingTail];

    rect.size = actualsize;
    rect.size.height = rect.size.height+4;
    storyLabel.frame = rect;

    NSString *strImage = nil;
    if( ![[_dicMatchesInfo objectForKey:@"view_photo"] isEqualToString:@"photo view not allowed."] ){
        strImage = [NSString stringWithFormat:@"%@%@", WEBSERVICE_PHOTO_BASEURI, [_dicMatchesInfo objectForKey:@"filename"]];
    }

    NSString *strPlaceholder;
    if( [eSyncronyAppDelegate sharedInstance].gender == 1 ){
        strPlaceholder = @"male1.png";
    }else {
        strPlaceholder = @"female1.png";
    }

    [photoImgView sd_setImageWithURL:[NSURL URLWithString:strImage] placeholderImage:[UIImage imageNamed:strPlaceholder] ];

}


- (IBAction)didClickView:(id)sender
{
    MatchProfileViewController  *matchInfoViewCtrl = [MatchProfileViewController createWithMatchInfo:_dicMatchesInfo];
    
    matchInfoViewCtrl.m_bTopButton = YES;
    matchInfoViewCtrl.m_bDismissSelf = YES;
    matchInfoViewCtrl.delegateListViewCtrl = self.delegateListViewCtrl;
    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.navigationController pushViewController:matchInfoViewCtrl animated:YES];
    
//    if (self.cansee) {
//        MatchProfileViewController  *matchInfoViewCtrl = [MatchProfileViewController createWithMatchInfo:_dicMatchesInfo];
//        
//        matchInfoViewCtrl.m_bTopButton = YES;
//        matchInfoViewCtrl.m_bDismissSelf = YES;
//        matchInfoViewCtrl.delegateListViewCtrl = self.delegateListViewCtrl;
//        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.navigationController pushViewController:matchInfoViewCtrl animated:YES];
//    }else {
//        bGotoUpgrade = TRUE;
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"eSynchrony" message:[NSString stringWithFormat:@"To view and go on dates with %@, subscribe to our date package now.",self.nameLabel.text] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//
//        [alert show];
//        [alert release];
//    }
    
    ///////////////////当view_status=@"Y"-->>have seen,字体应该变细/////////////////////////
    name=self.nameLabel.text;
   // NSLog(@"%@",name);
   // NSLog(@"dicMatchesInfo==%@",[_dicMatchesInfo objectForKey:@"nname"]);
    
   // if ([name isEqualToString:self.nameLabel.text]) {
    if ([name isEqualToString:[_dicMatchesInfo objectForKey:@"nname"]]) {
 
        self.nameLabel.font=[UIFont systemFontOfSize:15.0];
        
        NSString *view_status = [_dicMatchesInfo objectForKey:@"view_status"];
       // NSLog(@"view_status==%@",view_status);
        if([view_status isEqualToString:@"N"])
        {
            view_status=@"Y";
           // NSLog(@"view_statusview_status==%@",view_status);
             [_dicMatchesInfo setValue:view_status forKey:@"view_status"];
        }
        
    }
    ///////////////////////////////////////////////////////


}

- (void)PendingView_requestApproveToSever
{
    NSString        *macc_no = [_dicMatchesInfo objectForKey:@"acc_no"];
    NSDictionary    *result = [UtilComm approveToMatch:macc_no];
    [DSBezelActivityView removeViewAnimated:NO];

    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"eSynchrony"];
        return;
    }
    
    NSString    *response = [result objectForKey:@"response"];
    if( ![response isKindOfClass:[NSString  class]] )
    {
        [ErrorProc alertMessage:@"Unknown Error" withTitle:@"Error"];
        return;
    }
    
    if( [response hasPrefix:@"ERROR"] )
    {
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
        return;
    }
    
    if( [response isEqualToString:@"YOU GOT A MATCH"]||[response isEqualToString:@"You have a mutual match!!"]||[response isEqualToString:@"您有相互的配對!!"])//You have a mutual match
    {
        bGotoUpgrade = FALSE;

        UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"The page at https://www.esynchrony.com says:", @"MyMatchesPendingProposedCell")
                                                     message:[NSString stringWithFormat:NSLocalizedString(@"Confirm to arrange date with %@",@"MyMatchesPendingProposedCell"),_nameLabel.text]
                                                    delegate:self
                                           cancelButtonTitle:NSLocalizedString(@"OK",@"MyMatchesPendingCell")
                                           otherButtonTitles:NSLocalizedString(@"Cancel",@"MyMatchesPendingCell"), nil];
        
        
        [av show];
        [av release];
    }
    else
    {
        bGotoUpgrade = TRUE;
        
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"eSynchrony"
                                                     message:response
                                                    delegate:self
                                           cancelButtonTitle:NSLocalizedString(@"OK",)
                                           otherButtonTitles:nil, nil];
        [av show];
        [av release];
    }
}

- (IBAction)didClickApprove:(id)sender
{
    UIView  *parentView = [eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.view;
    [DSBezelActivityView newActivityViewForView:parentView withLabel:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(PendingView_requestApproveToSever) withObject:nil afterDelay:0.1];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0) {
        if( bGotoUpgrade == FALSE )
            [self.delegate refreshContent];
        else
            [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl procUpgrade];   
    }
    
}

- (void)dealloc
{
    [photoImgView release];
    [rightView release];
    
    [self.nameLabel release];
    [ageLabel release];
    [matchedIdxLabel release];
    [storyLabel release];

    [super dealloc];
}

@end

//
//  MyMatchesPendingProposedCellTableViewCell.h
//  eSyncrony
//
//  Created by ESYNSZ-Limit on 15/8/6.
//  Copyright (c) 2015年 WonMH. All rights reserved.
//
#import <UIKit/UIKit.h>


@class MatchProfileViewController;
//@class MyMatchesProposedViewController;

@protocol MyMatchesPendingProposedCellDelegate <NSObject>

@optional

- (void)refreshContent;

@end

@interface MyMatchesPendingProposedCell : UITableViewCell <UIAlertViewDelegate>
{
    IBOutlet UIImageView    *photoBGView;
    IBOutlet UIImageView    *photoImgView;
    IBOutlet UIImageView    *photoFrameView;

    IBOutlet UIImageView    *rightView;
    
    
    IBOutlet UILabel        *ageLabel;
    IBOutlet UILabel        *matchedIdxLabel;
    IBOutlet UILabel        *storyLabel;
    
    NSMutableDictionary*   _dicMatchesInfo;
    NSMutableArray          *arrMatchesItems;
    
    BOOL    bGotoUpgrade;
}

@property (retain, nonatomic) IBOutlet UILabel *nameLabel;
@property (nonatomic, assign) id<MyMatchesPendingProposedCellDelegate> delegate;
@property (nonatomic, assign) id delegateListViewCtrl;

@property (nonatomic,strong)NSString *name;


@property BOOL cansee ;
@property int limit;

- (void)setMatchesPendingInfo:(NSMutableDictionary*)dicMatchesInfo;

- (IBAction)didClickView:(id)sender;
- (IBAction)didClickApprove:(id)sender;


@end


#import <UIKit/UIKit.h>
//#import "MyMatchesProposedCell.h"
#import "MyMatchesPendingProposedCell.h"

@interface PendingViewController : GAITrackedViewController <UITableViewDataSource, UITableViewDelegate, MyMatchesPendingProposedCellDelegate>
{
    IBOutlet UITableView    *listTblView;

    
    NSMutableArray          *arrMatchesItems;
    
    BOOL    isSelectedMyApproval;
}
@property (retain, nonatomic) IBOutlet UILabel *lblNoMatches;
- (void)refreshContent;
- (void)didClickWaitingForMyApproal;
- (void)didClickWaitingForApproal;

@end

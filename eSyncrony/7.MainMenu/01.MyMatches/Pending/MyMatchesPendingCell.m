
#import "Global.h"
#import "UtilComm.h"
#import "DSActivityView.h"
#import "ImageUtil.h"
#import "UIImageView+WebCache.h"
#import "MyMatchesPendingCell.h"
#import "mainMenuViewController.h"
#import "MatchProfileViewController.h"
#import "eSyncronyAppDelegate.h"

//#import "PendingViewController.h"

@implementation MyMatchesPendingCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)loadDefaultImage{
    if( [eSyncronyAppDelegate sharedInstance].gender == 1 ){
        photoImgView.image = [UIImage imageNamed:@"male1.png"];
    }else {
        photoImgView.image = [UIImage imageNamed:@"female1.png"];
    }
}

- (BOOL)downloadMatchUserPhoto
{
    NSString        *macc_no = [_dicMatchesInfo objectForKey:@"Acc_no"];
    if( macc_no == nil )
        return FALSE;

    NSString    *view_photo = [_dicMatchesInfo objectForKey:@"view_photo"];
    
    
    if( [view_photo isEqualToString:@"photo view not allowed."] )
        return FALSE;
    
    NSString *filename = [_dicMatchesInfo objectForKey:@"filename"];
    NSString*   imageURL = [NSString stringWithFormat:@"%@%@", WEBSERVICE_PHOTO_BASEURI, filename];
    NSString*   imagePath = [ImageUtil loadImagePathFromURL:imageURL];
    
    [self performSelectorOnMainThread:@selector(PendingCell_onDownLoadImage:) withObject:imagePath waitUntilDone:YES];
    
    return TRUE;
}

- (void)PendingCell_onDownLoadImage:(NSString*)imagePath
{
    UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
    if( image != nil )
    {
        photoFrameView.hidden = NO;
        photoImgView.image = image;
        [_dicMatchesInfo setObject:imagePath forKey:@"localphoto_path"];
    }
    else
        [_dicMatchesInfo setObject:@"nophoto" forKey:@"localphoto_path"];
}

- (void)setMatchesPendingInfo:(NSMutableDictionary*)dicMatchesInfo
{
    _dicMatchesInfo = dicMatchesInfo;
    
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
    
    photoBGView.layer.cornerRadius = 5;
    photoImgView.layer.cornerRadius = 45;
    photoImgView.clipsToBounds = YES;
    photoFrameView.hidden = YES;
    
    rightView.layer.cornerRadius = 5;
    
    nameLabel.text = [dicMatchesInfo objectForKey:@"nname"];
    [nameLabel sizeToFit];
//    CGSize nameLabelSize = [nameLabel.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:nameLabel.font,NSFontAttributeName, nil]];
//    nameLabel.frame = CGRectMake(120, 14, nameLabelSize.width, 22);
    
    CGFloat xPos = nameLabel.frame.origin.x + nameLabel.frame.size.width;
    CGFloat yPos = nameLabel.frame.origin.y;
    
    ageLabel.text = [NSString stringWithFormat:@", %@", [dicMatchesInfo objectForKey:@"age"]];
    CGRect frame = ageLabel.frame;
    frame.origin.x = xPos;
    if ([[eSyncronyAppDelegate sharedInstance].language hasPrefix:@"zh-Hant"]||[[eSyncronyAppDelegate sharedInstance].language hasPrefix:@"zh-HK"]) {
        
        frame.origin.y = yPos;//Chinese interface optimization
    }
    ageLabel.frame = frame;
    [ageLabel sizeToFit];
    
    //matchedIdxLabel.text = [dicMatchesInfo objectForKey:@"acc_no"];
    matchedIdxLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Match Index : %@",@"MyMatchesPendingCell"), [dicMatchesInfo objectForKey:@"Distance"]];
    [matchedIdxLabel sizeToFit];


//    [dicMatchesInfo objectForKey:@"anon"];
    NSArray *anon = [dicMatchesInfo objectForKey:@"anon"];
    storyLabel.text =NSLocalizedString([anon objectAtIndex:6],@"MyMatchesPendingCell");
//    storyLabel.text = NSLocalizedString(@"(SMS reminder sent!)",@"MyMatchesPendingCell");
//    storyLabel.text = NSLocalizedString(@"(Email reminder sent!)",@"MyMatchesPendingCell");
    
    [storyLabel sizeToFit];
    
    NSString *strImage = nil;
    if( ![[_dicMatchesInfo objectForKey:@"view_photo"] isEqualToString:@"photo view not allowed."] ){
        strImage = [NSString stringWithFormat:@"%@%@", WEBSERVICE_PHOTO_BASEURI, [_dicMatchesInfo objectForKey:@"filename"]];
    }
    
    NSString *strPlaceholder;
    if( [eSyncronyAppDelegate sharedInstance].gender == 1 ){
        strPlaceholder = @"male1.png";
    }else {
        strPlaceholder = @"female1.png";
    }
    
    [photoImgView sd_setImageWithURL:[NSURL URLWithString:strImage] placeholderImage:[UIImage imageNamed:strPlaceholder] ];
    

//    if( [eSyncronyAppDelegate sharedInstance].gender == 1 )
//        photoImgView.image = [UIImage imageNamed:@"male1.png"];
//    
//    NSString    *imagePath = [_dicMatchesInfo objectForKey:@"localphoto_path"];
//    
//    if( imagePath != nil )
//    {
//        UIImage     *image = [UIImage imageWithContentsOfFile:imagePath];
//        if( image != nil )
//            photoImgView.image = image;
//    }
//    else
//    {
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,
//                                                 (unsigned long)NULL), ^(void) {
//            [_dicMatchesInfo retain];
//            if( [self downloadMatchUserPhoto] == FALSE )
//            {
//                [_dicMatchesInfo setObject:@"nophoto" forKey:@"localphoto_path"];
//            }
//            [dicMatchesInfo release];
//        });
//    }
}

- (IBAction)didClickView:(id)sender
{
    MatchProfileViewController  *matchInfoViewCtrl = [MatchProfileViewController createWithMatchInfo:_dicMatchesInfo];
    
    matchInfoViewCtrl.m_bTopButton = YES;
    matchInfoViewCtrl.m_bDismissSelf = YES;
    matchInfoViewCtrl.delegateListViewCtrl = self.delegateListViewCtrl;
    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.navigationController pushViewController:matchInfoViewCtrl animated:YES];
}

- (void)requestRemindToSever
{
    NSString        *macc_no = [_dicMatchesInfo objectForKey:@"Acc_no"];
    NSDictionary    *result = [UtilComm sendReminderToMatch:macc_no];
    [DSBezelActivityView removeViewAnimated:NO];

    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"eSynchrony"];
        return;
    }
    
    NSString    *response = [result objectForKey:@"response"];
    if( ![response isKindOfClass:[NSString  class]] )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Send Reminder",)];
        return;
    }
    
    if( [response hasPrefix:@"ERROR"] )
    {
        if( [response isEqualToString:@"ERROR 4"] )
        {
          /*  UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Send Reminder",@"MyMatchesPendingCell")
                                                         message:NSLocalizedString(@"Oops! You have run out of dates. Please take a look at our date-package plans.",@"MyMatchesPendingCell")
                                                        delegate:self
                                               cancelButtonTitle:NSLocalizedString(@"OK",)
                                               otherButtonTitles:nil, nil];
           */
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"The page at https://www.esynchrony.com says:",@"MyMatchesPendingCell")
                                                         message:[NSString stringWithFormat:NSLocalizedString(@"Confirm to arrange date with %@",@"MyMatchesPendingCell"),nameLabel.text]
                                                        delegate:self
                                               cancelButtonTitle:NSLocalizedString(@"OK",)
                                               otherButtonTitles:NSLocalizedString(@"Cancel",), nil];
            [av show];
            [av release];
        }
        
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
        return;
    }
    
    if( [response isEqualToString:@"OK"] )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Email has been sent.",@"MyMatchesPendingCell") withTitle:NSLocalizedString(@"Send Reminder",@"MyMatchesPendingCell")];
    }
    else
    {
        [ErrorProc alertMessage:response withTitle:NSLocalizedString(@"Send Reminder",@"MyMatchesPendingCell")];
    }
}

- (IBAction)didClickRemind:(id)sender
{
    UIView  *parentView = [eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.view;
    [DSBezelActivityView newActivityViewForView:parentView withLabel:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(requestRemindToSever) withObject:nil afterDelay:0.1];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //if( bGotoUpgrade == FALSE )
    //    [self.parentViewCtrl refreshContent];
    //else
    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl procUpgrade];
}

- (IBAction)didClickNext:(id)sender
{

}

-(void)dealloc
{
    [photoImgView release];
    [nameLabel release];
    [ageLabel release];
    [matchedIdxLabel release];
    [storyLabel release];
    
    [super dealloc];
}

@end

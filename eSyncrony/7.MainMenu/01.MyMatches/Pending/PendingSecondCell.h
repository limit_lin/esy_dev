//
//  PendingSecondCell.h
//  eSyncrony
//
//  Created by iosdeveloper on 14-7-1.
//  Copyright (c) 2014年 WonMH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PendingSecondCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UILabel *lblInstructions;

@end

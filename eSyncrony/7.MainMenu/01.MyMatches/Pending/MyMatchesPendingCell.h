
#import <UIKit/UIKit.h>

@interface MyMatchesPendingCell : UITableViewCell
{
    IBOutlet UIImageView    *photoBGView;
    IBOutlet UIImageView    *photoImgView;
    IBOutlet UIImageView    *photoFrameView;
    
    IBOutlet UIImageView    *rightView;
    
    IBOutlet UILabel        *nameLabel;
    IBOutlet UILabel        *ageLabel;
    IBOutlet UILabel        *matchedIdxLabel;
    IBOutlet UILabel        *storyLabel;
    
    NSMutableDictionary*   _dicMatchesInfo;
    
    BOOL    bGotoUpgrade;
}

//@property (nonatomic, assign) PendingViewController *parentViewCtrl;
@property (nonatomic, assign) id delegateListViewCtrl;
- (void)setMatchesPendingInfo:(NSMutableDictionary*)dicMatchesInfo;

- (IBAction)didClickView:(id)sender;
- (IBAction)didClickRemind:(id)sender;
- (IBAction)didClickNext:(id)sender;

@end

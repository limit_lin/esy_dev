//
//  PendingSecondCell.m
//  eSyncrony
//
//  Created by iosdeveloper on 14-7-1.
//  Copyright (c) 2014年 WonMH. All rights reserved.
//

#import "PendingSecondCell.h"

@implementation PendingSecondCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_lblInstructions release];
    [super dealloc];
}
@end

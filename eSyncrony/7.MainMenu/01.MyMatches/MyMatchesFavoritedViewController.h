//
//  MyMatchesFavoritedViewController.h
//  eSyncrony
//
//  Created by ESYNSZ-Limit on 16/2/14.
//  Copyright © 2016年 WonMH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyMatchesFavoritedCell.h"
#import "FlickrPaginator.h"

@class mainMenuViewController;

@interface MyMatchesFavoritedViewController : GAITrackedViewController<UITableViewDataSource,UITableViewDelegate,MyMatchesProposedCellDelegate,NMPaginatorDelegate,UIScrollViewDelegate>
{
    IBOutlet UITableView *favListTblView;
    IBOutlet UIImageView *imageBackGround;
    IBOutlet UILabel *lblNoMatches;
    
    //    NSMutableArray          *arrMatchesItems;
    
}
@property (nonatomic, assign) mainMenuViewController    *mainMenuView;
@property (nonatomic,retain)  MyMatchesFavoritedCell *myMatchesFavoritedCell;

@property (nonatomic, strong) FlickrPaginator *esyncFlickrPaginator;
@property (nonatomic, assign)UIView *footerView;
@property (nonatomic, assign)UIView *headerView;//append

@property (nonatomic, strong) UILabel *headerLabel;
@property (nonatomic, strong) UILabel *footerLabel;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;

@end

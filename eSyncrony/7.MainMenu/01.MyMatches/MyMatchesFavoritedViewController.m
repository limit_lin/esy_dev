//
//  MyMatchesFavoritedViewController.m
//  eSyncrony
//
//  Created by ESYNSZ-Limit on 16/2/14.
//  Copyright © 2016年 WonMH. All rights reserved.
//
#import "Global.h"
#import "DSActivityView.h"
#import "eSyncronyAppDelegate.h"
#import "mainMenuViewController.h"
#import "MatchProfileViewController.h"
#import "MyMatchesFavoritedViewController.h"
#import "JSON.h"

@interface MyMatchesFavoritedViewController ()

@end

@implementation MyMatchesFavoritedViewController
@synthesize footerView;
@synthesize headerView;

@synthesize myMatchesFavoritedCell;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.screenName = @"FavoritedMatchesView";
    //    arrMatchesItems = nil;
    favListTblView.dataSource = self;
    favListTblView.delegate   = self;
    
    //    self.esyncFlickrPaginator = [[eSyncFlickrPaginator alloc] initWithPageSize:20 delegate:self];
    self.esyncFlickrPaginator = [[FlickrPaginator alloc]initWithPageSize:20 delegate:self];
    
    [self setupTableViewHeader];
    [self setupTableViewFooter];
    
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self refreshContent];
}

- (void)refreshContent
{
    lblNoMatches.hidden = YES;
    
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    //    [self esyncFavorited_loadInfos];
    [self performSelector:@selector(esyncFavorited_loadInfos) withObject:nil afterDelay:0.1];
}

-(void)esyncFavorited_loadInfos{
    
    [self.esyncFlickrPaginator fetchFirstPage:0];
    
    if (_esyncFlickrPaginator.favorPage==0) {
        
        favListTblView.tableHeaderView.hidden = YES;
        favListTblView.tableFooterView.hidden = NO;
    }else
    {
        favListTblView.tableFooterView.hidden = NO;
        favListTblView.tableHeaderView.hidden = NO;
    }
}
- (void)fetchNextPage:(int)type
{
    [self.esyncFlickrPaginator fetchNextPage:0];
    [self.activityIndicator startAnimating];
    //NSLog(@"pageNext===%d",_flickrPaginator.page);
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];//append
    
    favListTblView.tableHeaderView.hidden=NO;
    
}

- (void)fetchBackPage:(int)type
{
    [self.esyncFlickrPaginator fetchBackPage:0];
    
    [self.activityIndicator startAnimating];
    // NSLog(@"pageNext===%d",_flickrPaginator.page);
    
    //    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];//append
    favListTblView.tableFooterView.hidden=NO;
}

- (void)setupTableViewHeader
{
    // set up label
    headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    headerView.backgroundColor = [UIColor clearColor];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 320, 44)];
    label.font = [UIFont boldSystemFontOfSize:16];
    label.textColor = [UIColor lightGrayColor];
    label.textAlignment = NSTextAlignmentCenter;
    
    self.headerLabel = label;
    [headerView addSubview:label];
    
    // set up activity indicator
    UIActivityIndicatorView *activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicatorView.center = CGPointMake(40,22);
    activityIndicatorView.hidesWhenStopped = YES;
    
    self.activityIndicator = activityIndicatorView;
    [headerView addSubview:activityIndicatorView];
    
    favListTblView.tableHeaderView= headerView;
}

- (void)setupTableViewFooter
{
    // set up label
    footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    footerView.backgroundColor = [UIColor clearColor];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 320, 44)];
    label.font = [UIFont boldSystemFontOfSize:16];
    label.textColor = [UIColor lightGrayColor];
    label.textAlignment = NSTextAlignmentCenter;
    self.footerLabel = label;
    [footerView addSubview:label];
    
    // set up activity indicator
    //append 0727
    UIActivityIndicatorView *activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicatorView.center = CGPointMake(70, 22);
    activityIndicatorView.hidesWhenStopped = YES;
    
    self.activityIndicator = activityIndicatorView;
    [footerView addSubview:activityIndicatorView];
    
    favListTblView.tableFooterView = footerView;
}
#pragma mark - Paginator delegate methods
- (void)updateTableViewFooter
{
    if ([self.esyncFlickrPaginator.results count] != 0)
    {
        self.footerLabel.text = [NSString stringWithFormat:@"%d matches out of %d", (int)[self.esyncFlickrPaginator.results count], (int)self.esyncFlickrPaginator.favorTotal];
    } else
    {
        self.footerLabel.text = @"";
    }
    
    [self.footerLabel setNeedsDisplay];
}
- (void)updateTableViewHeader
{
    if ([self.esyncFlickrPaginator.results count] != 0)
    {
        self.headerLabel.text = NSLocalizedString(@"Pull Down To Show More",@"MatchesProposedView");
    } else
    {
        self.headerLabel.text = @"";
    }
    [self.headerLabel setNeedsDisplay];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    //when reaching top,load a back page
    if (-scrollView.contentOffset.y/favListTblView.frame.size.height>0.1){
        // NSLog(@"reach first page......");
        if(![self.esyncFlickrPaginator reachedFirstPage:0])
        {
            // fetch back page of results
            [self fetchBackPage:0];
            
        }else
            favListTblView.tableHeaderView.hidden=YES;
    }
    // when reaching bottom, load a new page
    if (scrollView.contentOffset.y == scrollView.contentSize.height - scrollView.bounds.size.height)
    {
        //NSLog(@"reach last page.......");
        
        if(![self.esyncFlickrPaginator reachedLastPage:0])
        {
            // fetch next page of results
            [self fetchNextPage:0];
        }
        else
        {
            favListTblView.tableFooterView.hidden=YES;
            return;
        }
    }
}
- (void)paginator:(id)paginator didReceiveResults:(NSArray *)results//分页处理
{
    // update tableview footer
    [self updateTableViewFooter];
    [self.activityIndicator stopAnimating];
    [DSBezelActivityView removeViewAnimated:YES];
    
    if (self.esyncFlickrPaginator.favorPage==1) {
        
        if( [results count] == 0 )
        {
            lblNoMatches.hidden = NO;
            favListTblView.scrollEnabled = NO;
            favListTblView.separatorColor = [UIColor clearColor];
            return;
        }
        else
        {
            lblNoMatches.hidden = YES;
            favListTblView.scrollEnabled = YES;
            favListTblView.separatorColor = [UIColor clearColor];
        }
    }else
        [self updateTableViewHeader];
    
    [DSBezelActivityView removeViewAnimated:YES];
    [favListTblView reloadData];
    [favListTblView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
    
}
- (void)paginatorDidReset:(id)paginator
{
    [favListTblView reloadData];
    [self updateTableViewHeader];
    [self updateTableViewFooter];
}

- (void)paginatorDidFailToRespond:(id)paginator
{
    // Todo
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    //    if( arrMatchesItems == nil )
    //        return 0;
    //    return [arrMatchesItems count];
    return [self.esyncFlickrPaginator.results count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 130;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary     *dicMatchesInfo = [self.esyncFlickrPaginator.results objectAtIndex:indexPath.row];
    MyMatchesFavoritedCell *cell;
    //create my data cell
    static NSString *CellIdentifier = @"FavoritedIdentifier";
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        UINib *nib = [UINib nibWithNibName:@"MyMatchesFavoritedCell" bundle:nil];
        [tableView registerNib:nib forCellReuseIdentifier:CellIdentifier];
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        //        cell=[[MyMatchesFavoritedCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.delegate = self;
    cell.delegateListViewCtrl = self;
    [cell setMatchesFavoriteInfo:dicMatchesInfo];
    
    return cell;
}
//set row selected
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSMutableDictionary         *dicMatchesInfo = [self.esyncFlickrPaginator.results objectAtIndex:indexPath.row];
    
    //========be selected ,change the status of 'view_ststus"===============//
    NSString *view_status = [dicMatchesInfo objectForKey:@"view_status"];
    if ([view_status isEqualToString:@"N"]) {
        view_status=@"Y";
        [dicMatchesInfo setValue:view_status forKey:@"view_status"];
        self.myMatchesFavoritedCell.nameLabel.font=[UIFont systemFontOfSize:15.0];
    }
    
    MatchProfileViewController  *matchInfoViewCtrl = [MatchProfileViewController createWithMatchInfo:dicMatchesInfo];
    
    matchInfoViewCtrl.m_bTopButton = YES;
    matchInfoViewCtrl.m_bDismissSelf = YES;
    matchInfoViewCtrl.delegateListViewCtrl = self;
    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.navigationController pushViewController:matchInfoViewCtrl animated:YES];
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    [tableView reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (void)dealloc {
    [favListTblView release];
    [lblNoMatches release];
    [imageBackGround release];
    [super dealloc];
}
@end

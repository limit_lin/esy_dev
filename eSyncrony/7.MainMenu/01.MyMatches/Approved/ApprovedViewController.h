

#import <UIKit/UIKit.h>
#import "mainMenuViewController.h"

@interface ApprovedViewController : GAITrackedViewController
{
    IBOutlet UITableView    *listTblView;
    IBOutlet UILabel        *lblNoMatches;
    
    NSMutableArray          *arrMatchesItems;
}

@property (nonatomic, assign) mainMenuViewController    *mainMenuView;
- (void)refreshContent;
@end

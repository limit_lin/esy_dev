
#import "Global.h"
#import "DSActivityView.h"
#import "eSyncronyAppDelegate.h"

#import "NoMatchCell.h"
#import "ApprovedTopCell.h"
#import "ApprovedViewController.h"
#import "MyMatchesApprovedCell.h"
#import "MatchProfileViewController.h"

#import "eSyncronyAppDelegate.h"


@interface ApprovedViewController ()

@end

@implementation ApprovedViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"ApprovedMatchesView";
    arrMatchesItems = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self refreshContent];
}

- (void)refreshContent
{
    lblNoMatches.hidden = YES;
    
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(esyncApproved_loadInfos) withObject:nil afterDelay:0.1];
}

- (void)esyncApproved_loadInfos
{
  
    NSDictionary* result = [UtilComm loadMatchesInfoWithTypeNew:@"3"];
    
    
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Error"];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [self.mainMenuView onServerConnectionError:response];
        return;
    }
    
    [arrMatchesItems release];
    arrMatchesItems = [[NSMutableArray alloc] initWithCapacity:0];
    
    if( [response isKindOfClass:[NSDictionary class]] )
    {
        NSArray *arrItems = [response objectForKey:@"item"];
        if( [arrItems isKindOfClass:[NSDictionary class]] )
        {
            NSDictionary* item = (NSDictionary*)arrItems;
            NSMutableDictionary *mutItem = [NSMutableDictionary dictionaryWithDictionary:item];
            
            [arrMatchesItems addObject:mutItem];
        }
        else if( [arrItems isKindOfClass:[NSArray class]] )
        {
            for( int i = 0; i < [arrItems count]; i++ )
            {
                NSDictionary  *item = [arrItems objectAtIndex:i];
                
                
                [arrMatchesItems addObject:item];
            }
        }
        
    }
    
    [listTblView reloadData];
    [DSBezelActivityView removeViewAnimated:NO];
    if( [arrMatchesItems count] == 0 )
    {
        lblNoMatches.hidden = NO;
        listTblView.scrollEnabled = NO;
    }
    else
    {
        lblNoMatches.hidden = YES;
        listTblView.scrollEnabled = YES;
    }
}

#pragma mark --- Table Functions ---
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if( arrMatchesItems == nil )
        return 1;
    
    return [arrMatchesItems count]+1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( indexPath.row == 0 )
        return 105;
    
    return 130;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( indexPath.row == 0 )
    {
        ApprovedTopCell *cell;
        NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"ApprovedTopCell" owner:nil options:nil];
        cell = [arr objectAtIndex:0];
        
        return cell;
    }
    
    NSMutableDictionary     *dicMatchesInfo = [arrMatchesItems objectAtIndex:indexPath.row-1];
    
    MyMatchesApprovedCell   *cell;
    
//    NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"MyMatchesApprovedCell" owner:nil options:nil];
//    cell = [arr objectAtIndex:0];
    
    static NSString *CellIdentifier = @"ApprovedIdentifier";
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        UINib *nib = [UINib nibWithNibName:@"MyMatchesApprovedCell" bundle:nil];
        [tableView registerNib:nib forCellReuseIdentifier:CellIdentifier];
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    }
    
    [cell setMatchesInfo:dicMatchesInfo];

    return cell;
}

- (void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary         *dicMatchesInfo = [arrMatchesItems objectAtIndex:indexPath.row-1];
    MatchProfileViewController  *matchInfoViewCtrl = [MatchProfileViewController createWithMatchInfo:dicMatchesInfo];
    
    matchInfoViewCtrl.m_bTopButton = NO;//Remove feature: Reject mutual agreed match
    matchInfoViewCtrl.m_bDismissSelf = YES;
    matchInfoViewCtrl.delegateListViewCtrl = self;
    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.navigationController pushViewController:matchInfoViewCtrl animated:YES];

    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (void)dealloc
{
    [listTblView release];
    [lblNoMatches release];
    
    [arrMatchesItems release];
    
    [super dealloc];
}

@end
#import "Global.h"
#import "UtilComm.h"
#import "DSActivityView.h"
#import "ImageUtil.h"
#import "mainMenuViewController.h"
#import "MyMatchesApprovedCell.h"
#import "eSyncronyAppDelegate.h"
#import "UIImageView+WebCache.h"
#import "DocCertViewController.h"
#import "UpgradeViewController.h"

@implementation MyMatchesApprovedCell

@synthesize segName;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)loadDefaultImage{
    if( [eSyncronyAppDelegate sharedInstance].gender == 1 ){
        photoImgView.image = [UIImage imageNamed:@"male1.png"];
    }else {
        photoImgView.image = [UIImage imageNamed:@"female1.png"];
    }
}

- (BOOL)downloadMatchUserPhoto
{
    NSString        *macc_no = [_dicMatchesInfo objectForKey:@"acc_no"];
    if( macc_no == nil )
        macc_no = [_dicMatchesInfo objectForKey:@"Acc_no"];
    
       // NSLog(@"%@",macc_no);
    
    if( macc_no == nil )
        return FALSE;
    
    NSString    *view_photo = [_dicMatchesInfo objectForKey:@"view_photo"];
    
    
    if( [view_photo isEqualToString:@"photo view not allowed."] )
        return FALSE;
    
    NSString*   filename = [_dicMatchesInfo objectForKey:@"filename"];
    NSString*   imageURL = [NSString stringWithFormat:@"%@%@", WEBSERVICE_PHOTO_BASEURI, filename];
    NSString*   imagePath = [ImageUtil loadImagePathFromURL:imageURL];
    
    [self performSelectorOnMainThread:@selector(ApprovedCell_onDownLoadImage:) withObject:imagePath waitUntilDone:YES];

    return TRUE;
}

- (void)ApprovedCell_onDownLoadImage:(NSString*)imagePath
{
    UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
    if( image != nil )
    {
        photoFrameView.hidden = NO;
        photoImgView.image = image;
        [_dicMatchesInfo setObject:imagePath forKey:@"localphoto_path"];
    }
    else
        [_dicMatchesInfo setObject:@"nophoto" forKey:@"localphoto_path"];
}

- (IBAction)ClickGoUpgrade:(id)sender {
    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl procUpgrade]; 
}

- (void)setMatchesInfo:(NSMutableDictionary*)dicMatchesInfo
{
    _dicMatchesInfo = dicMatchesInfo;
    
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
    
    photoBGView.layer.cornerRadius = 5;
    photoImgView.layer.cornerRadius = 45;
    photoImgView.clipsToBounds = YES;
    photoFrameView.hidden = YES;
    
    rightView.layer.cornerRadius = 5;
    
    nameLabel.text = [dicMatchesInfo objectForKey:@"nname"];
//    [nameLabel sizeToFit];
    CGSize nameLabelSize = [nameLabel.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:nameLabel.font,NSFontAttributeName, nil]];
    nameLabel.frame = CGRectMake(120, 20, nameLabelSize.width, nameLabelSize.height);
    
    CGFloat xPos = nameLabel.frame.origin.x + nameLabel.frame.size.width;
    CGFloat yPos = nameLabel.frame.origin.y;
    
    ageLabel.text = [NSString stringWithFormat:@", %@", [dicMatchesInfo objectForKey:@"age"]];
    CGRect frame = ageLabel.frame;
    frame.origin.x = xPos;
    frame.origin.y = yPos;
    ageLabel.frame = frame;
    [ageLabel sizeToFit];
    
    NSString *strEthnicity = [dicMatchesInfo objectForKey:@"Ethnicity"];
    
    if( [strEthnicity isEqualToString:@""] )
        lblEthnicity.text = @"";
    else
    {
        NSArray*    ethnicityList = [Global loadQuestionArray:@"ethnicity"];
        int index = [strEthnicity intValue];
        
        if( index >= 0 && index < [ethnicityList count] )
            lblEthnicity.text = [ethnicityList objectAtIndex:index];
        else
            lblEthnicity.text = @"";
    }
    [lblEthnicity sizeToFit];
    BOOL payment = [[_dicMatchesInfo objectForKey:@"payment"]boolValue];
    if (payment) {
        lblComment.text = NSLocalizedString(@"Waiting to be coordinated.",@"MyMatchesApprovedCell");
//        self.DateSet.hidden = NO;
        self.btnGoUpgrade.hidden = YES;
        
    }else{
        self.btnGoUpgrade.hidden = NO;
        //lblComment.text = NSLocalizedString(@"Click here to allow us to arrange your date.",@"MyMatchesApprovedCell");
        lblComment.text =@"";
        
    }
//    [lblComment sizeToFit];
    
    
    NSString *strImage = nil;
    if( ![[_dicMatchesInfo objectForKey:@"view_photo"] isEqualToString:@"photo view not allowed."] ){
        strImage = [NSString stringWithFormat:@"%@%@", WEBSERVICE_PHOTO_BASEURI, [_dicMatchesInfo objectForKey:@"filename"]];
    }
    
    NSString *strPlaceholder;
    if( [eSyncronyAppDelegate sharedInstance].gender == 1 ){
        strPlaceholder = @"male1.png";
    }else {
        strPlaceholder = @"female1.png";
    }
    
    [photoImgView sd_setImageWithURL:[NSURL URLWithString:strImage] placeholderImage:[UIImage imageNamed:strPlaceholder] ];
    
//    if( [eSyncronyAppDelegate sharedInstance].gender == 1 )
//        photoImgView.image = [UIImage imageNamed:@"male1.png"];
//    
//    NSString    *imagePath = [_dicMatchesInfo objectForKey:@"localphoto_path"];
//    
//    if( imagePath != nil )
//    {
//        UIImage     *image = [UIImage imageWithContentsOfFile:imagePath];
//        if( image != nil )
//            photoImgView.image = image;
//    }
//    else
//    {
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,
//                                                 (unsigned long)NULL), ^(void) {
//            [_dicMatchesInfo retain];
//            if( [self downloadMatchUserPhoto] == FALSE )
//            {
//                [_dicMatchesInfo setObject:@"nophoto" forKey:@"localphoto_path"];
//            }
//            [dicMatchesInfo release];
//        });
//    }
}

-(void)judgeVerify
{
    
    NSDictionary* result = [UtilComm loadMatchesInfoWithTypeNew:@"3"];
    
    NSLog(@"%@",result);
    
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Error"];
        
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
       
        return;
    }
        _idVerify=[response objectForKey:@"IDVerify"];
        NSLog(@"%@",_idVerify);
    
//    _idVerify = 0;//use for testing
    
    if ([_idVerify isEqualToString:@"1"]||[_idVerify isEqualToString:@"P"]) {
        
        segName=nameLabel.text;
        NSString        *macc_no = [_dicMatchesInfo objectForKey:@"acc_no"];
        if( macc_no == nil )
            macc_no = [_dicMatchesInfo objectForKey:@"Acc_no"];
        //    NSLog(@"%@",macc_no);
        if( macc_no == nil )
        {
            UIAlertView *alter=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"ERROR",@"MyMatchesApprovedCell") message:NSLocalizedString(@"INFO ERROR,Please connact eSynchrony",@"MyMatchesApprovedCell") delegate:self cancelButtonTitle:nil otherButtonTitles:nil, nil];
            [alter show];
            [alter release];
            return;
        }
        ApproveDates *datesDetail= [[[ApproveDates alloc] initWithNibName:@"ApproveDates" bundle:nil] autorelease];
        
        datesDetail.toName = segName;
        datesDetail.maccNo=macc_no;
        
        
        //    NSLog(@"datesDetail.toName==%@",datesDetail.toName);
        
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.navigationController pushViewController:datesDetail animated:YES];
    }
    else
    {
        UIAlertView *alter=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"eSynchrony",)
                                                     message:NSLocalizedString(@"Please Verify your ID before we can arrange this date",@"MyMatchesApprovedCell")
                                                    delegate:self
                                           cancelButtonTitle:NSLocalizedString(@"OK",@"MyMatchesApprovedCell")
                                           otherButtonTitles:NSLocalizedString(@"Cancel",@"MyMatchesApprovedCell"), nil];
        
        alter.tag=111;
        [alter show];
        [alter release];
        return;
        
    }
   
    return;
    
}
-(NSString *)esync_pselfCoordChk
{
    [DSBezelActivityView newActivityViewForView:self withLabel:NSLocalizedString(@"Loading...",)];
    
    NSString        *macc_no = [_dicMatchesInfo objectForKey:@"acc_no"];
    if( macc_no == nil )
        macc_no = [_dicMatchesInfo objectForKey:@"Acc_no"];
    //    NSLog(@"%@",macc_no);
    if( macc_no == nil )
    {
        UIAlertView *alter=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"ERROR",@"MyMatchesApprovedCell") message:NSLocalizedString(@"INFO ERROR,Please connact eSynchrony",@"MyMatchesApprovedCell") delegate:self cancelButtonTitle:NSLocalizedString(@"OK",@"MyMatchesApprovedCell") otherButtonTitles:nil, nil];
        [alter show];
        [alter release];
        return nil;
    }
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:macc_no forKey:@"macc_no"];
    
//for dev
//    [params setObject:@"xxx" forKey:@"token_key"];
//    [params setObject:@"1137" forKey:@"acc_no"];
//    [params setObject:@"20000" forKey:@"macc_no"];
//    NSLog(@"%@",params);
    
    NSDictionary*   result = [UtilComm CheckSelfcoorddates:params];
    
    if( result == nil )
    {
        [ErrorProc alertToCheckInternetStateTitle:@"INFO ERROR,Please connact eSynchrony"];
        return nil;
    }
    
    id response = [result objectForKey:@"response"];
    if( response == nil )
    {
        return nil;
    }
    if ([response isKindOfClass:[NSString class]]) {
        if ([response isEqualToString:@"DONE"]) {
            return response;
        }
        if ([response isEqualToString:@"OK"]) {
            return response;
        }
    }

    return nil;

}
-(void)esync_ppopUpWindow{
    int p_countryId = [eSyncronyAppDelegate sharedInstance].countryId +1;
//    NSLog(@"%d",p_countryId);
//    p_countryId = 8;//for test
    
    NSString *emailAddr = @"info@esynchrony.com";
    
    switch (p_countryId) {
        case 1:
            emailAddr = @"info@esynchrony.com";
            break;
        case 2:
            emailAddr = @"info.my@esynchrony.com";
            break;
        case 3:
            emailAddr = @"info.hk@esynchrony.com";
            break;
        case 8:
            emailAddr = @"support.id@esynchrony.com";
            break;
        default:
            emailAddr = @"info@esynchrony.com";
            break;
    }

    UIAlertView *alterView=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"eSynchrony",)
                                                     message:[NSString stringWithFormat:NSLocalizedString(@"Please contact our coordinator %@ if you need to update your dating schedule",@"MyMatchesApprovedCell"),emailAddr]
                                                    delegate:self
                                           cancelButtonTitle:nil
                                           otherButtonTitles:NSLocalizedString(@"OK",@"MyMatchesApprovedCell"), nil];
    
    alterView.tag=113;
    
    [alterView show];
    [alterView release];
    
    return;

    
}
- (IBAction)SetDates:(UIButton *)sender {
    
  //bolck user from entering self-coordination
  NSString* checkStatus = [self esync_pselfCoordChk];
    
    [DSBezelActivityView removeViewAnimated:NO];
    
    if ([checkStatus isEqualToString:@"DONE"]) {
        
        [self esync_ppopUpWindow];
   
    }

    if ([checkStatus isEqualToString:@"OK"]) {
        judge = NO;
        
        [DSBezelActivityView newActivityViewForView:self withLabel:NSLocalizedString(@"Loading...",)];
        
        
        NSLog(@"%@",[eSyncronyAppDelegate sharedInstance].paying);
        _payment=[eSyncronyAppDelegate sharedInstance].paying;
        
        //        NSLog(@"%@",_payment);
        //        _payment=@"yes";//use for testing
        
        if ([_payment isEqualToString:@"no"]) {
            
            UIAlertView *alterView=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"eSynchrony",)
                                                             message:NSLocalizedString(@"Please paying for a eSynchrony member before we can arrange this date",@"MyMatchesApprovedCell")
                                                            delegate:self
                                                   cancelButtonTitle:NSLocalizedString(@"Cancel",@"MyMatchesApprovedCell")
                                                   otherButtonTitles:NSLocalizedString(@"OK",@"MyMatchesApprovedCell"), nil];
            
            alterView.tag=112;
            
            [alterView show];
            [alterView release];
            
            [DSBezelActivityView removeViewAnimated:NO];
            return;
            
        }
        if ([_payment isEqualToString:@"yes"]) {
            judge=YES;
        }
        
        [DSBezelActivityView removeViewAnimated:NO];
        
        if (judge) {
            [self judgeVerify];
        }
    }

}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ((alertView.tag == 111)) {
        
        if (buttonIndex == 0) {
  
            DocCertViewController *docCertView=[[DocCertViewController alloc]initWithNibName:@"DocCertViewController" bundle:nil];
            
            [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.navigationController pushViewController:docCertView animated:YES];
  
        }
        
    }
    if ((alertView.tag == 112)) {
        
        if (buttonIndex == 1) {
            
            judge=YES;
            
            [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl procUpgrade];
        }
        
    }

}

-(void)dealloc
{
    [photoImgView release];
    [rightView release];

    [nameLabel release];
    [ageLabel release];
    [lblEthnicity release];
    [lblComment release];
    
    [_btnGoUpgrade release];
    [_DateSet release];
    [super dealloc];
}

@end

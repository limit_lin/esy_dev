//
//  ApproveDates.h
//  eSyncrony
//
//  Created by iosdeveloper on 15/5/15.
//  Copyright (c) 2015年 WonMH. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "eSyncronyViewController.h"
#import "mainMenuViewController.h"
#import "ApprovedetailaCell.h"

@class MyMatchesApprovedCell;

@interface ApproveDates : UIViewController<UIScrollViewDelegate,UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UIScrollView *scrView;
    IBOutlet UIScrollView *cuisineScroll;
    
    IBOutlet UITableView *cuisineTableview;
   
    IBOutlet UIView *pickerView;

    UIView *addView;
    NSString *match_date;
    NSString *match_date2;
    NSString *match_date3;
    BOOL choosedate;
    BOOL choosedate1;
    BOOL choosedate2;
   
}
@property (retain, nonatomic) IBOutlet UIView *Datesdetail;

@property (retain, nonatomic) IBOutlet UIView *CuisineDetail;

@property (retain, nonatomic) IBOutlet UITextField *testDate1Field;

@property (retain, nonatomic) IBOutlet UITextField *testDate2Field;

@property (retain, nonatomic) IBOutlet UITextField *testDate3Field;

@property(retain,nonatomic)MyMatchesApprovedCell *mymatchApproveCell;
@property(retain,nonatomic)UINavigationController*  navigationController;
@property (retain,nonatomic)NSString *toName;
@property(retain,nonatomic)NSString *maccNo;
@property(retain,nonatomic)UIDatePicker *datePicker;


@property (retain,nonatomic)NSString *date;
@property (retain,nonatomic)NSMutableArray *food;
@property (retain,nonatomic)NSArray* cuisinesName;

@property (retain,nonatomic)mainMenuViewController *mainMenuviewController;

- (IBAction)didclickBack:(id)sender;
- (IBAction)didclickSave:(id)sender;



- (void)chooseDate:(UIDatePicker *)sender;
- (void)chooseDate1:(UIDatePicker *)sender;
- (void)chooseDate2:(UIDatePicker *)sender;

-(IBAction)btnCacelClicked:(id)sender;
-(IBAction)btnDoneClicked:(id)sender;



@end

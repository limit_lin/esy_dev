//
//  ApproveDates.m
//  eSyncrony
//
//  Created by iosdeveloper on 15/5/15.
//  Copyright (c) 2015年 WonMH. All rights reserved.
//
#import "Global.h"
#import "UtilComm.h"
#import "ApproveDates.h"
#import "MyMatchesApprovedCell.h"
#import "ApprovedViewController.h"
#import "eSyncronyAppDelegate.h"
#import "mainMenuViewController.h"
#import "newDatesViewController.h"
#import "eSyncronyViewController.h"
#import "ApprovedetailaCell.h"

//http://blog.csdn.net/x844010689/article/details/24730553

@interface ApproveDates ()

@end

@implementation ApproveDates

@synthesize CuisineDetail;
@synthesize maccNo;
@synthesize food;
@synthesize cuisinesName;

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    self.screenName = @"ApproveDates";
//    NSLog(@"macc======%@",maccNo);
    choosedate=NO;
    choosedate1=NO;
    choosedate2=NO;
    
    food=[[NSMutableArray alloc]init];
    
    //多次设置如果多行显示不出来，有可能是大小的问题
    UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(5, 83, 310,70)];
   
    label.text=[NSString stringWithFormat: NSLocalizedString(@"Set my available dates to meet my match(%@)",@"ApproveDates"),_toName];
    label.textAlignment= NSTextAlignmentLeft;//default left
    //label.lineBreakMode= NSLineBreakByCharWrapping;
    label.textColor=TITLE_TEXT_COLLOR;
    label.numberOfLines = 0;//set mutil-lines,0 represent mutil
  
    [self.view addSubview:label];
    
    cuisinesName=[[NSArray alloc]initWithObjects:NSLocalizedString(@"Chinese",@"ApproveDates"),NSLocalizedString(@"Indian",@"ApproveDates"),NSLocalizedString(@"Italian",@"ApproveDates"),NSLocalizedString(@"Japanese",@"ApproveDates"),NSLocalizedString(@"Korean",@"ApproveDates"),NSLocalizedString(@"Mediterranean",@"ApproveDates"),NSLocalizedString(@"Mexican",@"ApproveDates"),NSLocalizedString(@"Western",@"ApproveDates"), nil];
//    NSLog(@"%@",cuisinesName);

    scrView.contentSize = _Datesdetail.frame.size;
    
    [scrView addSubview:_Datesdetail];
    scrView.contentSize = _Datesdetail.frame.size;
    
    cuisineScroll.contentSize=CuisineDetail.frame.size;
    [cuisineScroll addSubview:CuisineDetail];
    
    self.testDate1Field.delegate=self;
    self.testDate2Field.delegate=self;
    self.testDate3Field.delegate=self;
    
    [self matchSetDates];

    
}
-(void)matchSetDates
{
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:maccNo forKey:@"macc_no"];
    
    NSDictionary*   result = [UtilComm saveSelfcoorddates:params];
    if( result == nil )
    {
        [ErrorProc alertToCheckInternetStateTitle:@"Your match doesn't set dates."];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( response == nil )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"eSynchrony",) withTitle:NSLocalizedString(@"Your match doesn't set dates.",)];
        return;
    }
    
    
    if ([response isKindOfClass:[NSDictionary class]]) {
        match_date=[response objectForKey:@"match_date"];
        match_date2=[response objectForKey:@"match_date_2"];
        match_date3=[response objectForKey:@"match_date_3"];
        self.testDate1Field.text=match_date;
        self.testDate2Field.text=match_date2;
        self.testDate3Field.text=match_date3;
    }
    return;
}
- (void)chooseDate:(UIDatePicker *)sender{
  
    NSDate *selectedDate = sender.date;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    NSString *dateString =[formatter stringFromDate:selectedDate];
    self.testDate1Field.text = dateString;
    [formatter release];
}
- (void)chooseDate1:(UIDatePicker *)sender {

    NSDate *selectedDate = sender.date;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    NSString *dateString =[formatter stringFromDate:selectedDate];
    self.testDate2Field.text = dateString;
    [formatter release];

}
- (void)chooseDate2:(UIDatePicker *)sender {
    
    NSDate *selectedDate = sender.date;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    NSString *dateString =[formatter stringFromDate:selectedDate];
    self.testDate3Field.text = dateString;
    [formatter release];
}
-(void)initWithDatepicker
{
    //  UIDatePicker
    self.datePicker=[[UIDatePicker alloc]init];
    // self.datePicker.datePickerMode=UIDatePickerModeDateAndTime;
    self.datePicker.datePickerMode=UIDatePickerModeDate;
    self.datePicker.minuteInterval=30;
    
    return;
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    switch (textField.tag) {
        case 700:
            
            [self initWithDatepicker];
            choosedate=YES;
            [self.datePicker addTarget:self action:@selector(chooseDate:) forControlEvents:UIControlEventValueChanged];
            
            break;
            
        case 701:
          
            [self initWithDatepicker];
            choosedate1=YES;
            [self.datePicker addTarget:self action:@selector(chooseDate1:) forControlEvents:UIControlEventValueChanged];
            
            break;
            
        case 702:
         
            [self initWithDatepicker];
            choosedate2=YES;
            [self.datePicker addTarget:self action:@selector(chooseDate2:) forControlEvents:UIControlEventValueChanged];
            
        default:
            break;
    }
    //UIDatePicker以及在当前视图上就不用再显示了
    if (self.datePicker.superview == nil) {
        //close all keyboard or data picker visible currently
        if (textField.tag == 700) {
            
            [self.testDate2Field resignFirstResponder];
            [self.testDate3Field resignFirstResponder];
        }
        if (textField.tag == 701) {
            
            [self.testDate1Field resignFirstResponder];
            [self.testDate2Field resignFirstResponder];
        }
        if (textField.tag == 703) {
            
            [self.testDate1Field resignFirstResponder];
            [self.testDate2Field resignFirstResponder];
        }
        //此处将Y坐标设在最底下，为了一会动画的展示
        self.datePicker.frame=CGRectMake(0,35, 320, 235);
        addView=[[UIView alloc]initWithFrame:CGRectMake(0, 300, 320,270)];//append
        
        //NSDate *currentDate=[self.datePicker date];
        
        NSDate *datenow=[NSDate dateWithTimeIntervalSinceNow:1*24*60*60];
        
        //NSLog(@"%@ %@",currentDate,datenow);
        
        self.datePicker.minimumDate=datenow;
        
        self.datePicker.backgroundColor=[UIColor whiteColor];
        [pickerView addSubview:self.datePicker];
        
      [addView addSubview:pickerView];//append
      [self.view addSubview:addView];//append
        
        //[self.view addSubview:pickerView];//原来
        
//        [UIView beginAnimations:nil context:nil];
//        [UIView setAnimationDuration:0.3f];
//        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
//        
//        self.datePicker.bottom -= self.datePicker.height;
//        
//        pickerView.contentScaleFactor -=pickerView.bounds.size.height;
//        
//        [UIView commitAnimations];
    }
    return NO;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
//设置表视图中数据的行数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    NSLog(@"%d",cuisinesName.count);
    return cuisinesName.count;
}
//设置每个单元格的内容
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ApprovedetailaCell *cell;
    //create my data cell
    static NSString *CellIdentifier = @"ApprovedetailaCell";
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        UINib *nib = [UINib nibWithNibName:@"ApprovedetailaCell" bundle:nil];
        [tableView registerNib:nib forCellReuseIdentifier:CellIdentifier];
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    }
    cell.cuisineLabel.text=cuisinesName[indexPath.row];
    return cell;
    
}
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger row=[indexPath row];
    [food addObject:cuisinesName[row]];
   // NSLog(@"%@",food);
    
}
//当用户单击选择某行数据时执行此协议方法
- (void)tableView:(UITableView *)curTableView didSelectRowAtIndexPath:(NSIndexPath *)newIndexPath
{
    
    NSString *s=cuisinesName[newIndexPath.row];
    //NSLog(@"you clicked:%@",s);
    [food addObject:s];
   // NSLog(@"%@",food);

}

- (void)tableView:(UITableView *)curTableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *s=cuisinesName[indexPath.row];
   // NSLog(@"you clicked:%@",s);
    [food removeObject:s];
   // NSLog(@"%@",food);

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)didclickBack:(id)sender {
    
    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.navigationController popViewControllerAnimated:YES];

}

- (IBAction)didclickSave:(id)sender {
 
    NSDate *currentDate=[NSDate date];
    //NSLog(@"%@",currentDate);
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    NSString *dateString =[formatter stringFromDate:currentDate];
   // NSLog(@"%@",dateString);
    
   [formatter release];
    
    if (([self.testDate1Field.text compare:dateString]==NSOrderedAscending)||([self.testDate2Field.text compare:dateString]==NSOrderedAscending)||([self.testDate3Field.text compare:dateString]==NSOrderedAscending)) {
        
        UIAlertView *alter=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Info",@"ApproveDates") message:NSLocalizedString(@"Please select a date in future.",@"ApproveDates") delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK",), nil];
        [alter show];
        [alter release];
        return;
        
    }
    if ([self.testDate1Field.text isEqualToString:dateString]||[self.testDate2Field.text isEqualToString:dateString]||[self.testDate3Field.text isEqualToString:dateString]) {
        
        UIAlertView *alter=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Info",@"ApproveDates") message:NSLocalizedString(@"Please select a date in future.",@"ApproveDates") delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK",), nil];
        [alter show];
        [alter release];
        return;
    }
    if ((self.testDate1Field.text.length == 0)||(self.testDate1Field.text.length == 0)||(self.testDate1Field.text.length == 0)) {
        
        UIAlertView *alter=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Info",@"ApproveDates") message:NSLocalizedString(@"pls ensure dates have been chosen,thank you.",@"ApproveDates") delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK",), nil];
        [alter show];
        [alter release];
        return;
    }
    if ([self.testDate1Field.text isEqualToString:self.testDate2Field.text]||[self.testDate2Field.text isEqualToString:self.testDate3Field.text]||[self.testDate1Field.text isEqualToString:self.testDate3Field.text]) {
        
        UIAlertView *alter=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Info",@"ApproveDates") message:NSLocalizedString(@"Please select a differrent available date.",@"ApproveDates") delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK",), nil];
        [alter show];
        [alter release];
        return;
        
    }
    if (food.count==0) {
        
        UIAlertView *alter=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Info",@"ApproveDates") message:NSLocalizedString(@"Please select at least 1 preferred cuisine.",@"ApproveDates") delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK",), nil];
        [alter show];
        [alter release];
        return;
        
    }

    [self performSelector:@selector(requestSaveToSever) withObject:nil afterDelay:0.1];
}

- (void)requestSaveToSever
{
    
//    NSLog(@"%d",food.count);
    NSString *cuisine=[NSString new];
    NSString *cuisines=[NSString new];
    for (int i=0; i< food.count; i++) {
        cuisine=[NSString stringWithFormat:@"%@|",[food objectAtIndex:i]];
        cuisines=[cuisines stringByAppendingString:cuisine];
    }
    
//    NSLog(@"%@",cuisines);

    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:maccNo forKey:@"macc_no"];
    [params setObject:self.testDate1Field.text forKey:@"date1"];
    [params setObject:self.testDate2Field.text forKey:@"date2"];
    [params setObject:self.testDate3Field.text forKey:@"date3"];
    [params setObject:cuisines forKey:@"cuisine"];
  
    NSDictionary*    result = [UtilComm saveSelfcoord:params];
//    [params release];
    
    if( result == nil )
    {
        [ErrorProc alertToCheckInternetStateTitle:@"Saving Failure"];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( response == nil )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Saving Failure",)];
        return;
    }
    if ([response isKindOfClass:[NSString class]] &&[response isEqualToString:@"ERROR 0"]) {
        UIAlertView *alter=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"ERROR",) message:NSLocalizedString(@"pls choose less than 1",@"ApproveDates") delegate:self cancelButtonTitle:nil otherButtonTitles:nil, nil];
        [alter show];
        [alter release];
        
    }
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        NSLog(@"%@",response);
    }
    else//response is ok
    {
        //下面两行代码结合起来可以同时跳转到newdates界面上去，不过需要重新刷新界面，数据才会出现
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl procNewDates];//可以跳转到newDates界面，不过会出现需要按下返回键才可以
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.navigationController popViewControllerAnimated:YES];//做修改，改到　newdates upcoming的界面才可以
        
    }
}
-(IBAction)btnCacelClicked:(id)sender
{
    if (pickerView.superview) {
        
         NSLog(@"removeremove");
       [addView removeFromSuperview];
        
    }

}
-(IBAction)btnDoneClicked:(id)sender
{
    if (pickerView.superview) {
        
       [addView removeFromSuperview];
        
    }
    
}
- (void)dealloc {
   
    [_Datesdetail release];
    [scrView release];
    [_testDate1Field release];
    [_testDate2Field release];
    [_testDate3Field release];
    [CuisineDetail release];
    [cuisineTableview release];
    [pickerView release];
    [cuisineScroll release];
    [super dealloc];
}

@end

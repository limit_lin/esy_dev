//
//  ApprovedetailaCell.m
//  eSyncrony
//
//  Created by iosdeveloper on 15/5/22.
//  Copyright (c) 2015年 WonMH. All rights reserved.
//

#import "ApprovedetailaCell.h"

@implementation ApprovedetailaCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_cuisineLabel release];
    [super dealloc];
}
@end

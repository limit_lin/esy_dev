
#import <UIKit/UIKit.h>
#import "MyMatchesProposedViewController.h"
#import "eSyncronyViewController.h"
#import "ApproveDates.h"

@interface MyMatchesApprovedCell : UITableViewCell<UIAlertViewDelegate>
{
    IBOutlet UIImageView    *photoBGView;
    IBOutlet UIImageView    *photoImgView;
    IBOutlet UIImageView    *photoFrameView;
    
    IBOutlet UIImageView    *rightView;

    IBOutlet UILabel        *nameLabel;
    IBOutlet UILabel        *ageLabel;
    IBOutlet UILabel        *lblEthnicity;
    IBOutlet UILabel        *lblComment;
    
    NSMutableDictionary*   _dicMatchesInfo;
    BOOL judge;


}
@property (retain, nonatomic) IBOutlet UIButton *DateSet;

@property (retain, nonatomic) IBOutlet UIButton *btnGoUpgrade;
@property (nonatomic, assign) id delegateListViewCtrl;

//ApproveDates
@property (retain,nonatomic)NSString *segName;
@property (retain,nonatomic)NSString *idVerify;

@property (strong,nonatomic)NSString *payment;

@property(retain,nonatomic)UINavigationController*  navigationController;
@property(retain,nonatomic)eSyncronyViewController* eSyncronyView;


- (IBAction)ClickGoUpgrade:(id)sender;
- (void)setMatchesInfo:(NSMutableDictionary*)dicMatchesInfo;
- (IBAction)SetDates:(UIButton *)sender;

-(void)judgeVerify;

@end

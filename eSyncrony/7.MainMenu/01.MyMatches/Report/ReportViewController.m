

#import "Global.h"
#import "DSActivityView.h"
#import "eSyncronyAppDelegate.h"

#import "ReportViewController.h"

@interface ReportViewController ()

@end

@implementation ReportViewController

-(void)p_MatchReportViewLayout
{
    CGSize youAcceptedPercLabelSize = [youAcceptedPercLabel.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:youAcceptedPercLabel.font,NSFontAttributeName, nil]];
    youAcceptedPercLabel.frame = CGRectMake(231, 142, youAcceptedPercLabelSize.width, 21);
    
    
    CGSize acceptedYouPercLabelSize = [acceptedYouPercLabel.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:acceptedYouPercLabel.font,NSFontAttributeName, nil]];
    acceptedYouPercLabel.frame = CGRectMake(257, 296, acceptedYouPercLabelSize.width, 21);
    
    
    CGSize mutualMatchPercLabelSize = [mutualMatchPercLabel.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:mutualMatchPercLabel.font,NSFontAttributeName, nil]];
    mutualMatchPercLabel.frame = CGRectMake(214, 392, mutualMatchPercLabelSize.width, 21);

    return;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"ReportView";
    [scrlView addSubview:contentsView];
    scrlView.contentSize = contentsView.frame.size;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self refreshContent];
}

- (void)refreshContent
{
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(esyncMatchesReport_loadInfos) withObject:nil afterDelay:0.1];
}

- (void)esyncMatchesReport_loadInfos
{
    NSDictionary* result = [UtilComm getMatchReports];
    [DSBezelActivityView removeViewAnimated:NO];

    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Error"];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [self.mainMenuView onServerConnectionError:response];
        return;
    }

    NSDictionary    *dicResponse = (NSDictionary*)response;
    NSArray         *items = [dicResponse objectForKey:@"item"];
    if( items == nil || ![items isKindOfClass:[NSArray class]] )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"ERROR",)];
        return;
    }
    
    if( [items count] < 10 )
        return;

    // total
    matchNumLabel.text = [items objectAtIndex:0];
    
    // you accept
    NSString*   acceptStr = [items objectAtIndex:1];
    
    youAcceptedNumLabel.text = [acceptStr substringToIndex:1];
    youAcceptedPercLabel.text = [acceptStr substringFromIndex:3];

    
    // accept you
    NSString*   acceptedStr = [items objectAtIndex:3];
    
    acceptedYouNumLabel.text = [acceptedStr substringToIndex:2];
    acceptedYouPercLabel.text = [acceptedStr substringFromIndex:3];
    
    // mutual
    NSString*   mutualStr = [items objectAtIndex:5];

    mutualMatchNumLabel.text = [mutualStr substringToIndex:1];
    mutualMatchPercLabel.text = [mutualStr substringFromIndex:3];

    // resulted match
    NSString*   resultedStr = [items objectAtIndex:7];
        
    resultedMatchNumLabel.text = [resultedStr substringToIndex:1];
    resultedMatchPercLabel.text = [resultedStr substringFromIndex:3];

    rejecReasonLabel.text = NSLocalizedString([items objectAtIndex:8],@"ReportView");
    mostrejectReasonLabel.text = NSLocalizedString([items objectAtIndex:9],@"ReportView");
//    rejecReasonLabel.text = [items objectAtIndex:8];
//    mostrejectReasonLabel.text = [items objectAtIndex:9];
    
    [self p_MatchReportViewLayout];//only needed in zh-Hant,zh-HK 
    
}

- (void)dealloc
{
    [scrlView release];
    [contentsView release];
    
    [matchNumLabel release];
    [youAcceptedNumLabel release];
    [youAcceptedPercLabel release];
    [acceptedYouNumLabel release];
    [acceptedYouPercLabel release];
    [mutualMatchNumLabel release];
    [mutualMatchPercLabel release];
    [resultedMatchNumLabel release];
    [resultedMatchPercLabel release];
    [rejecReasonLabel release];
    [mostrejectReasonLabel release];
    
    [super dealloc];
}

@end

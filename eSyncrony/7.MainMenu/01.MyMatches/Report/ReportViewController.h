

#import <UIKit/UIKit.h>
#import "mainMenuViewController.h"

@interface ReportViewController : GAITrackedViewController
{
    IBOutlet UIScrollView   *scrlView;
    IBOutlet UIView         *contentsView;
    
    IBOutlet UILabel        *matchNumLabel;
    IBOutlet UILabel        *youAcceptedNumLabel;
    IBOutlet UILabel        *youAcceptedPercLabel;
    IBOutlet UILabel        *acceptedYouNumLabel;
    IBOutlet UILabel        *acceptedYouPercLabel;
    IBOutlet UILabel        *mutualMatchNumLabel;
    IBOutlet UILabel        *mutualMatchPercLabel;
    IBOutlet UILabel        *resultedMatchNumLabel;
    IBOutlet UILabel        *resultedMatchPercLabel;
    IBOutlet UILabel        *rejecReasonLabel;
    IBOutlet UILabel        *mostrejectReasonLabel;
}

@property (nonatomic, assign) mainMenuViewController    *mainMenuView;

@end

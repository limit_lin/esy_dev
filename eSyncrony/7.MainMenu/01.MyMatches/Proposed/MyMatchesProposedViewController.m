
#import "Global.h"
#import "DSActivityView.h"
#import "eSyncronyAppDelegate.h"

#import "MyMatchesProposedViewController.h"
#import "MyMatchesProposedCell.h"
#import "mainMenuViewController.h"
#import "MatchProfileViewController.h"
#import "GiveFeedBackViewController.h"
#import "MatchSettingViewController.h"
#import "eSynchronyFilterViewController.h"
#import "TTTAttributedLabel.h"

@interface MyMatchesProposedViewController ()
{
    
    MyMatchesProposedCell *loadMoreCell;
    //    NSMutableArray *items;
    BOOL memberPay;//It used to determine whether a member
    NSDictionary *p_result;//append to imporve the network
}
@end

@implementation MyMatchesProposedViewController
@synthesize footerView;
@synthesize headerView;
@synthesize num;
@synthesize myMatchesProposedCell;

@synthesize image;
@synthesize login_time;
@synthesize tmp_time;

//@synthesize login_dt;
//@synthesize tmp_dt;
//@synthesize paying;
//@synthesize verified;
////获取图片高度
//- (CGFloat)getImageViewHeight: (float)width_ andHeigeht_:(float)height_
//{
//    return 260 * height_ / width_;
//}

-(void)p_registerNotification{
    // system >=IOS8.0
    if([[[UIDevice currentDevice]systemVersion]floatValue] >=8.0)//151023
    {
        [[UIApplication sharedApplication]registerUserNotificationSettings:[UIUserNotificationSettings
                                                                            
                                                                            settingsForTypes:(UIUserNotificationTypeSound|UIUserNotificationTypeAlert|UIUserNotificationTypeBadge)
                                                                            
                                                                            categories:nil]];
        
        [[UIApplication sharedApplication]registerForRemoteNotifications];
        
    }else{//system < IOS8.0
        
        //        [[UIApplication sharedApplication]registerForRemoteNotificationTypes:
        //
        //         (UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeSound|UIRemoteNotificationTypeBadge)];
        [[UIApplication sharedApplication]registerForRemoteNotifications];
    }
    
    return;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.screenName = @"MatchesProposedView";
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [self p_registerNotification];
        
    });
    
    self.flickrPaginator = [[FlickrPaginator alloc] initWithPageSize:20 delegate:self];
    memberPay = NO;
    //    if (self.flickrPaginator.total < 20) {
    //        listTblView.tableFooterView.hidden = YES;
    //        listTblView.tableHeaderView.hidden = YES;
    //    }else
    //    {
    if ([[eSyncronyAppDelegate sharedInstance].paying isEqualToString:@"yes"]) {
        memberPay = YES;
        [self setupTableViewHeader];
        [self setupTableViewFooter];
    }
    //    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self refreshContent];
}
- (void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
}
//=================renewwal PopUp===================//
-(void)renewalPopUp
{
    int     nCountryID = [eSyncronyAppDelegate sharedInstance].countryId;
    //    NSLog(@"nCountryID == %d",nCountryID);
    //renewal
    NSDictionary* results = [UtilComm renewalPopup];
    id responses = [results objectForKey:@"response"];
    //    NSLog(@"responses===%@",responses);
    if( [responses isKindOfClass:[NSString class]] )
    {
        if ([responses isEqualToString:@"1"]) {
            
            switch (nCountryID) {
                case 0:
                {
                    UIAlertView *alter=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"eSynchrony",) message:NSLocalizedString(@"Meet more dates now at SG$250 cheaper. Only available for 3 days",@"MatchesProposedView") delegate:self cancelButtonTitle:NSLocalizedString(@"OK",@"MatchesProposedView") otherButtonTitles:NSLocalizedString(@"Close",@"MatchesProposedView"), nil];
                    
                    alter.tag=2005;
                    [alter show];
                    [alter release];
                }
                    break;
                case 1:
                {
                    UIAlertView *alter=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"eSynchrony",)
                                                                 message:NSLocalizedString(@"Meet more dates now at RM250 cheaper. Only available for 3 days",@"MatchesProposedView")
                                                                delegate:self cancelButtonTitle:NSLocalizedString(@"OK",@"MatchesProposedView") otherButtonTitles:NSLocalizedString(@"Close",@"MatchesProposedView"), nil];
                    
                    alter.tag=2005;
                    [alter show];
                    [alter release];
                }
                    break;
                case 2:
                {
                    UIAlertView *alter=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"eSynchrony",)
                                                                 message:NSLocalizedString(@"Meet more dates now at HK$1500 cheaper. Only available for 3 days",@"MatchesProposedView")
                                                                delegate:self
                                                       cancelButtonTitle:NSLocalizedString(@"OK",@"MatchesProposedView") otherButtonTitles:NSLocalizedString(@"Close",@"MatchesProposedView"), nil];
                    
                    alter.tag=2005;
                    [alter show];
                    [alter release];
                }
                    break;
                    
                default:
                    break;
            }
        }
        else if ([responses isEqualToString:@"2"]) {
            
            switch (nCountryID) {
                case 0:
                {
                    UIAlertView *alter=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"eSynchrony",)
                                                                 message:NSLocalizedString(@"Renew earlier to get SG$200 off from original price.",@"MatchesProposedView")
                                                                delegate:self
                                                       cancelButtonTitle:NSLocalizedString(@"OK",@"MatchesProposedView")
                                                       otherButtonTitles:NSLocalizedString(@"Close",@"MatchesProposedView"), nil];
                    alter.tag=2006;
                    [alter show];
                    [alter release];
                }
                    break;
                case 1:
                {
                    UIAlertView *alter=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"eSynchrony",)
                                                                 message:NSLocalizedString(@"Renew earlier to get RM200 off from original price.",@"MatchesProposedView")
                                                                delegate:self
                                                       cancelButtonTitle:NSLocalizedString(@"OK",@"MatchesProposedView")
                                                       otherButtonTitles:NSLocalizedString(@"Close",@"MatchesProposedView"), nil];
                    alter.tag=2006;
                    [alter show];
                    [alter release];
                }
                    break;
                case 2:
                {
                    UIAlertView *alter=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"eSynchrony",)
                                                                 message:NSLocalizedString(@"Renew earlier to get HK$1200 off from original price.",@"MatchesProposedView")
                                                                delegate:self
                                                       cancelButtonTitle:NSLocalizedString(@"OK",@"MatchesProposedView")
                                                       otherButtonTitles:NSLocalizedString(@"Close",@"MatchesProposedView"), nil];
                    alter.tag=2006;
                    [alter show];
                    [alter release];
                }
                    break;
                    
                default:
                    break;
            }
        }
        else if ([responses isEqualToString:@"3"]) {
            
        }
        else
        {
            
        }
    }
    
}
//===============matchTips===============//
-(void)matchTips
{
    NSDictionary* results = [UtilComm matchTips];
    //    if ([results isEqual:nil]) {
    //        return;
    //    }
    id responses = [results objectForKey:@"response"];
    //    NSLog(@"matchTips responses===%@",responses);
    
    if ([responses isKindOfClass:[NSString class]]&& [responses isEqualToString:@"null"]) {//append by 0803
        return;
    }
    
    NSString *show=[responses objectForKey:@"show"];
    //    NSLog(@"show == %@",show);
    
    if ([show isEqualToString:@"Y"]) {
        
        UIAlertView *alter=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"eSynchrony",) message:NSLocalizedString(@"You may want to review your matching criteria to let us reconmmend more matches for you.Click here to review now.",@"MatchesProposedView") delegate:self cancelButtonTitle:NSLocalizedString(@"Close",@"MatchesProposedView") otherButtonTitles:NSLocalizedString(@"Here",@"MatchesProposedView"), nil];
        
        alter.tag = 2007;
        [alter show];
        [alter  release];
    }else
    {
        return;
    }
    
}
- (void)refreshContent
{
    //    DSBezelActivityView *des
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    dispatch_async(dispatch_get_main_queue(), ^{//append 151023
        [self performSelector:@selector(loadInfo) withObject:nil afterDelay:0.1];
    });
}

- (void)loadInfo
{
    //[self setupTableViewFooter];
    [self.flickrPaginator fetchFirstPage:1];
    //NSLog(@"page=========%d",_flickrPaginator.page);
    
    //renewal popup
    if ([eSyncronyAppDelegate sharedInstance].show==1) {
        
        [self renewalPopUp];//leaks 47.37%
        
        [eSyncronyAppDelegate sharedInstance].show = 2;
        
    }
    //match tips
    if ([eSyncronyAppDelegate sharedInstance].show == 2) {
        
        [self matchTips];//leaks 52.63%
    }
    
    //    if (self.flickrPaginator.numTotal <= 20) {
    //
    //        listTblView.tableHeaderView.hidden=YES;
    //        listTblView.tableFooterView.hidden=YES;
    //    }else{
    //append judge
    if ([[eSyncronyAppDelegate sharedInstance].paying isEqualToString:@"yes"]) {
        memberPay = YES;
        if (_flickrPaginator.page==0) {
            
            listTblView.tableHeaderView.hidden = YES;
            listTblView.tableFooterView.hidden = NO;
        }
        if (_flickrPaginator.page==10) {
            listTblView.tableHeaderView.hidden = NO;
            listTblView.tableFooterView.hidden=YES;
        }
    }
    else if ([[eSyncronyAppDelegate sharedInstance].paying isEqualToString:@"no"])
    {
        memberPay = NO;
        listTblView.tableHeaderView.hidden=YES;
        listTblView.tableFooterView.hidden=YES;
        
    }
    //    }
}
-(void)popupPrompts
{
    UIAlertView *alter1=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"eSynchrony",)
                                                  message:NSLocalizedString(@"You may want to review your matching criteria to let us recommend more relevant matches for you. Click here to review now.",@"MatchesProposedView")
                                                 delegate:self
                                        cancelButtonTitle:NSLocalizedString(@"Close",@"MatchesProposedView")
                                        otherButtonTitles:NSLocalizedString(@"Here",@"MatchesProposedView"), nil];
    
    alter1.tag = 2008;
    [alter1 show];
    [alter1  release];
    
}
#pragma mark - Actions

- (void)fetchNextPage:(int)type
{
    
    [self.flickrPaginator fetchNextPage:1];
    [self.activityIndicator startAnimating];
    //NSLog(@"pageNext===%d",_flickrPaginator.page);
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];//append
    
    listTblView.tableHeaderView.hidden=NO;
    
    if (_flickrPaginator.page >=9) {
        
        listTblView.tableFooterView.hidden=YES;
    }
}

-(void)fetchCommentPage:(int)type{
    
    [self.flickrPaginator fetchCommentPage:1];
    
    [self.activityIndicatorHeder startAnimating];
    //    listTblView.tableHeaderView.hidden = NO;
    
}

- (void)fetchBackPage:(int)type
{
    [self.flickrPaginator fetchBackPage:1];
    
    [self.activityIndicatorHeder startAnimating];
    // NSLog(@"pageNext===%d",_flickrPaginator.page);
    
    //    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];//append
    
    listTblView.tableFooterView.hidden = YES;
}
- (void)setupTableViewHeader
{
    // set up label
    headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    headerView.backgroundColor = [UIColor clearColor];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 320, 44)];
    label.font = [UIFont boldSystemFontOfSize:16];
    label.textColor = [UIColor lightGrayColor];
    label.textAlignment = NSTextAlignmentCenter;
    
    self.headerLabel = label;
    [headerView addSubview:label];
    
    // set up activity indicator
    UIActivityIndicatorView *activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicatorView.center = CGPointMake(40,22);
    activityIndicatorView.hidesWhenStopped = YES;
    
    self.activityIndicatorHeder = activityIndicatorView;
    [headerView addSubview:activityIndicatorView];
    
    listTblView.tableHeaderView= headerView;
}

- (void)setupTableViewFooter
{
    // set up label
    footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    footerView.backgroundColor = [UIColor clearColor];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 320, 44)];
    label.font = [UIFont boldSystemFontOfSize:16];
    label.textColor = [UIColor lightGrayColor];
    label.textAlignment = NSTextAlignmentCenter;
    self.footerLabel = label;
    [footerView addSubview:label];
    
    // set up activity indicator
    //append 0727
    UIActivityIndicatorView *activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicatorView.center = CGPointMake(70, 22);
    activityIndicatorView.hidesWhenStopped = YES;
    
    self.activityIndicator = activityIndicatorView;
    [footerView addSubview:activityIndicatorView];
    
    listTblView.tableFooterView = footerView;
}
- (void)updateTableViewHeader
{
    if ([self.flickrPaginator.results count] != 0)
    {
        self.headerLabel.text = NSLocalizedString(@"Pull Down To Show More",@"MatchesProposedView");
    } else
    {
        self.headerLabel.text = @"";
    }
    [self.headerLabel setNeedsDisplay];
}

- (void)updateTableViewFooter
{
    if ([self.flickrPaginator.results count] != 0)
    {
        //self.footerLabel.text = [NSString stringWithFormat:@"%d matches out of %d,please pulling up  to  load next page", (int)[self.flickrPaginator.results count], (int)self.flickrPaginator.total];
        
        self.footerLabel.text = NSLocalizedString(@"Loading...",@"MatchesProposedView");
        
        image=[[UIImageView alloc]initWithFrame:CGRectMake(82,5, 25, 40)];
        image.image=[UIImage imageNamed:@"pull_up.png"];
        
        UIImageView *imageRight = [[UIImageView alloc]initWithFrame:CGRectMake(200, 5, 25, 40)];
        imageRight.image =[UIImage imageNamed:@"pull_up.png"];
        
        [footerView addSubview:image];
        [footerView addSubview:imageRight];
        
        listTblView.tableFooterView = footerView;
        [image release];
        //            self.footerLabel.text = NSLocalizedString(@"Pull Up To Show More",@"MatchesProposedView");
        
    } else
    {
        self.footerLabel.text = @"";
        [self setupTableViewFooter];
    }
    
    [self.footerLabel setNeedsDisplay];
}
- (void)clearButtonPressed:(id)sender
{
    [self.flickrPaginator fetchFirstPage:1];
}

#pragma mark - Paginator delegate methods

- (void)paginator:(id)paginator didReceiveResults:(NSArray *)results//paginator
{
    [DSBezelActivityView removeViewAnimated:NO];//append
    [self updateTableViewHeader];
    if (self.flickrPaginator.numTotal >20) {
        if (results.count >= 20) {
            listTblView.tableFooterView.hidden = NO;
            [self updateTableViewFooter];
        }else
            listTblView.tableFooterView.hidden = YES;
    }else
        listTblView.tableFooterView.hidden = YES;
    
    [self.activityIndicatorHeder stopAnimating];
    [self.activityIndicator stopAnimating];
    
    [DSBezelActivityView removeViewAnimated:NO];//no work
    
    if (self.flickrPaginator.page==1) {
        login_dt = [self.flickrPaginator.login_dt retain];
        tmp_dt = [self.flickrPaginator.tmp_dt retain];
        paying = [self.flickrPaginator.paying retain];
        verified = [self.flickrPaginator.verified retain];
        
        if( [results count] == 0 )
        {
            lblInstructions.hidden = NO;
            lblInstructions1.hidden = NO;
            lblInstructions.font = [UIFont fontWithName:@"HelveticaNeueLTStd-BdCn" size:lblInstructions.font.pointSize];
            imageBackGround.hidden =YES;
            listTblView.hidden = YES;
            
            lblInstructions1.text = NSLocalizedString(@"These are the matches that we have found to be compatible with you.", @"MatchesProposedView");
            if ([[eSyncronyAppDelegate sharedInstance].isVip isEqualToString:@"true"]) {//append filter
                
                lblInstructions1.text = NSLocalizedString(@"These are the matches that we have found to be compatible with you. (Click here to filter)", @"MatchesProposedView");
                
                NSRange subRange =[lblInstructions1.text rangeOfString:NSLocalizedString(@"Click here to filter",@"ProfileView")];
                
                if (subRange.location != NSNotFound) {
                    btnInstructions.hidden = NO;
                }else
                {
                    btnInstructions.hidden = YES;
                }
            }
            
            NSString *strInstructions = NSLocalizedString(@"No matches at the moment.\nWe will drop you an email once suitable \nmatches are found.\n\nTo help you further, you may want to view our help section or adjust your matching criteria.", @"MatchesProposedView");
            lblInstructions.delegate =self;
            
            [lblInstructions setText:strInstructions afterInheritingLabelAttributesAndConfiguringWithBlock:^NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString) {
                NSRange Range1 = [[mutableAttributedString string] rangeOfString:NSLocalizedString(@"help section",@"MatchesProposedView") options:NSCaseInsensitiveSearch];
                [mutableAttributedString addAttribute:NSForegroundColorAttributeName value:TITLE_REG_COLOR range:Range1];
                
                NSRange Range2 = [[mutableAttributedString string] rangeOfString:NSLocalizedString(@"matching criteria",@"MatchesProposedView") options:NSCaseInsensitiveSearch];
                [mutableAttributedString addAttribute:NSForegroundColorAttributeName value:TITLE_REG_COLOR range:Range2];
                
                return mutableAttributedString;
            }];
            
            NSRange range1 = [strInstructions rangeOfString:NSLocalizedString(@"help section",@"MatchesProposedView")];
            [lblInstructions addLinkToURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",WEBSERVICE_HELPSECTION]] withRange:range1];
            
            NSRange range2 = [strInstructions rangeOfString:NSLocalizedString(@"matching criteria",@"MatchesProposedView")];
            [lblInstructions addLinkToURL:nil withRange:range2];
            
            CGRect rect = lblInstructions.frame;
            //            CGSize size = CGSizeMake(300.f, 400);
            //            CGSize actualsize = [strInstructions sizeWithFont:lblInstructions.font constrainedToSize:size lineBreakMode:NSLineBreakByWordWrapping];
            
            CGSize actualsize = [strInstructions boundingRectWithSize:CGSizeMake(300.f, 400)
                                                              options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading
                                                           attributes:@{NSFontAttributeName:lblInstructions.font}
                                                              context:nil].size;
            
            rect.size = actualsize;
            rect.size.height = rect.size.height+40;
            lblInstructions.frame = rect;
            
            return;
        }
        else
        {
            lblInstructions.hidden = YES;
            lblInstructions1.hidden = NO;
            imageBackGround.hidden = NO;
            listTblView.hidden = NO;
            listTblView.scrollEnabled = YES;
            
            listTblView.tableHeaderView.hidden=YES;
            
            lblInstructions1.text = NSLocalizedString(@"These are the matches that we have found to be compatible with you.", @"MatchesProposedView");
            
            if ([[eSyncronyAppDelegate sharedInstance].isVip isEqualToString:@"true"]) {//append filter
                
                lblInstructions1.text = NSLocalizedString(@"These are the matches that we have found to be compatible with you. (Click here to filter)", @"MatchesProposedView");
                
                NSRange subRange =[lblInstructions1.text rangeOfString:NSLocalizedString(@"Click here to filter",@"ProfileView")];
                
                if (subRange.location != NSNotFound) {
                    btnInstructions.hidden = NO;
                }else
                {
                    btnInstructions.hidden = YES;
                }
            }
        }
        
        [DSBezelActivityView removeViewAnimated:NO];//no work
        
        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
        BOOL NoThanks = [[defaults objectForKey:@"NoThanks"]boolValue];
        if (!NoThanks) {
            BOOL shouldAsk=NO;
            if (tmp_dt.length==0) {
                shouldAsk = YES;
                NSLog(@"tmp_dt.length==0");
            }else{
                int loginyear =  [[login_dt substringWithRange:NSMakeRange(0,4)] intValue];
                int loginmonth =  [[login_dt substringWithRange:NSMakeRange(5,2)] intValue];
                int loginday =  [[login_dt substringWithRange:NSMakeRange(8,2)] intValue];
                //NSLog(@"%@",[NSString stringWithFormat:@"loginyear:%d\nloginmonth:%d\nloginday:%d",loginyear,loginmonth,loginday]);
                int tmpyear =  [[tmp_dt substringWithRange:NSMakeRange(0,4)] intValue];
                int tmpmonth =  [[tmp_dt substringWithRange:NSMakeRange(5,2)] intValue];
                int tmpday =  [[tmp_dt substringWithRange:NSMakeRange(8,2)] intValue];
                
                
                if (loginyear>tmpyear) {
                    shouldAsk = YES;
                    NSLog(@"loginyear>tmpyear");
                }else if(loginyear==tmpyear&&loginmonth>tmpmonth){
                    shouldAsk = YES;
                    NSLog(@"loginmonth>tmpmonth");
                }else {
                    if(loginyear==tmpyear&&loginmonth==tmpmonth&&loginday>tmpday){
                        shouldAsk = YES;
                        NSLog(@"loginday>tmpday");
                    }
                }
            }
            
            if ((self.flickrPaginator.results.count>=3)&&shouldAsk) {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Do you find eSynchrony useful?",@"MatchesProposedView") message:nil delegate:self cancelButtonTitle:NSLocalizedString(@"No",) otherButtonTitles:NSLocalizedString(@"Yes",), nil];
                alert.tag = 2000;
                [alert show];
                [alert release];
            }
        }
    }
    
    // update tableview content
    // easy way : call [tableView reloadData];
    // nicer way : use insertRowsAtIndexPaths:withAnimation:
    
    //  [listTblView beginUpdates];
    //  [listTblView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationMiddle];
    //  [listTblView endUpdates];
    
    [listTblView reloadData];
    [listTblView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
}

- (void)onClickFilter:(UIButton *)sender{
    
    eSynchronyFilterViewController *filterViewCtrl = [[[eSynchronyFilterViewController alloc] initWithNibName:@"eSynchronyFilterViewController" bundle:nil] autorelease];
    
    //    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.navigationController pushViewController:filterViewCtrl  animated:YES];
    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl presentViewController:filterViewCtrl animated:YES completion:nil];
    
}

- (void)paginatorDidReset:(id)paginator
{
    [listTblView reloadData];
    [self updateTableViewHeader];
    [self updateTableViewFooter];
}

- (void)paginatorDidFailToRespond:(id)paginator
{
    // Todo
}

#pragma mark - UIScrollViewDelegate Methods
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    //when reaching top,load a back page
    if (-scrollView.contentOffset.y/listTblView.frame.size.height>0.1){
        // NSLog(@"reach first page......");
        if(![self.flickrPaginator reachedFirstPage:1])
        {
            // fetch back page of results
            [self fetchBackPage:1];
            
        }else
            listTblView.tableHeaderView.hidden=YES;
    }
    // when reaching bottom, load a new page
    if (scrollView.contentOffset.y == scrollView.contentSize.height - scrollView.bounds.size.height)
    {
        //NSLog(@"reach last page.......");
        if (memberPay) {
            
            if(![self.flickrPaginator reachedLastPage:1])
            {
                // fetch next page of results
                [self fetchNextPage:1];
            }
            else
            {
                listTblView.tableFooterView.hidden=YES;
                return;
            }
        }
        else
        {
            return;
        }
    }
}

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url {
    NSString *strurl = [NSString stringWithFormat:@"%@",url];
    NSLog(@"didSelectLinkWithURL:%@",strurl);
    //    if ([strurl isEqualToString:@"https://www.esynchrony.com/helptop2.php?ans=7"]) {
    if ([strurl isEqualToString:[NSString stringWithFormat:@"%@",WEBSERVICE_HELPSECTION]]) {
        [[UIApplication sharedApplication] openURL:url];
        return;
    }
    if (url==nil) {
        
        MatchSettingViewController *matchSettingViewCtrl = [[[MatchSettingViewController alloc] initWithNibName:@"MatchSettingViewController" bundle:nil] autorelease];
        
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.navigationController pushViewController:matchSettingViewCtrl animated:YES];
    }
}
-(void)updateTemdt{
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    
    NSDictionary*    result = [UtilComm updateTmpdate:params];
    
    [DSBezelActivityView removeViewAnimated:NO];
    if( result == nil )
    {
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( [response isKindOfClass:[NSString class]] ){
        if( [response isEqualToString:@"ture"] ){
            
            
        }else if([response isEqualToString:@"false"]|| [response hasPrefix:@"ERROR"] ){
            
            [ErrorProc alertMessage:@"Send Failed" withTitle:@"Error"];
        }
    }else {
        
        [ErrorProc alertMessage:@"Send Failed" withTitle:@"Error"];
    }
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 2000) {
        if (buttonIndex==0) {
            [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
            [self performSelector:@selector(updateTemdt) withObject:nil afterDelay:0.01];
            [self.mainMenuView procGiveFeedback:YES];
            //            GiveFeedBackViewController *giveFeedback = [[GiveFeedBackViewController alloc]initWithNibName:@"GiveFeedBackViewController" bundle:nil];
            //            giveFeedback.nLove = YES;
            //            [self.mainMenuView presentViewController:giveFeedback animated:YES completion:nil];
            //            [giveFeedback release];
            
        }else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Thank You",@"MatchesProposedView")
                                                           message:NSLocalizedString(@"We are so happy to hear that you love eSynchrony! It'd be really helpful if you rated us in the App Store.\nThanks so much for spending some time with us.",@"MatchesProposedView")
                                                          delegate:self
                                                 cancelButtonTitle:NSLocalizedString(@"No Thanks",@"MatchesProposedView") otherButtonTitles:NSLocalizedString(@"Rate eSynchrony",@"MatchesProposedView"),NSLocalizedString(@"Remind Me Later",@"MatchesProposedView"), nil];
            alert.tag = 2001;
            [alert show];
            [alert release];
        }
    }else if(alertView.tag == 2001){
        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
        switch (buttonIndex) {
            case 0:{
                
                [defaults setObject:[NSNumber numberWithBool:YES] forKey:@"NoThanks"];
                break;
            }
            case 1:{
                //657708325 893845592
                //itms-apps://itunes.apple.com/app/idYOUR_APP_ID
                //itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=YOUR_APP_ID
                NSString *appleID = @"893845592";
                
                NSString *str ;
                if ([[[UIDevice currentDevice]systemVersion]floatValue] >= 7.0) {
                    str = [NSString stringWithFormat:
                           @"itms-apps://itunes.apple.com/app/id%@",
                           appleID ];
                }else{
                    str = [NSString stringWithFormat:
                           @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%@",
                           appleID ];
                }
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
                [defaults setObject:[NSNumber numberWithBool:YES] forKey:@"NoThanks"];
                break;
            }
            case 2:{
                [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
                [self performSelector:@selector(updateTemdt) withObject:nil afterDelay:0.01];
                break;
            }
            default:
                break;
        }
    }else if(alertView.tag == 2004){
        if (buttonIndex==0) {
            [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl procUpgrade];
        }
    }
    else if(alertView.tag == 2005){
        if (buttonIndex==0) {
            [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl procUpgrade];
        }
    }
    else if(alertView.tag == 2006){
        if (buttonIndex==0) {
            [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl procUpgrade];
        }
    }
    else if(alertView.tag == 2007)
    {
        
        
        if (buttonIndex == 0) {
            [eSyncronyAppDelegate sharedInstance].show = 3;
        }
        if (buttonIndex==1) {
            
            [eSyncronyAppDelegate sharedInstance].show = 3;
            
            MatchSettingViewController *matchSettingViewCtrl = [[[MatchSettingViewController alloc] initWithNibName:@"MatchSettingViewController" bundle:nil] autorelease];
            
            [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.navigationController pushViewController:matchSettingViewCtrl animated:YES];
            
        }
    }
    
}

#pragma mark --- Table Functions ---
//实现分区数目
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    
}
//set tableview rows
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //NSLog(@"%d",[self.flickrPaginator.results count]);
    return [self.flickrPaginator.results count];
}

//set tableview rows height
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 130;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSMutableDictionary     *dicMatchesInfo = [self.flickrPaginator.results objectAtIndex:indexPath.row];
    MyMatchesProposedCell *cell;
    //create my data cell
    static NSString *CellIdentifier = @"ProposedIdentifier";
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        UINib *nib = [UINib nibWithNibName:@"MyMatchesProposedCell" bundle:nil];
        [tableView registerNib:nib forCellReuseIdentifier:CellIdentifier];
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        //cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.delegate = self;
    cell.delegateListViewCtrl = self;
    cell.cellPage = _flickrPaginator.page;
    [cell setMatchesInfo:dicMatchesInfo];
    if (![[eSyncronyAppDelegate sharedInstance].isVip isEqualToString:@"true"]) {
        cell.faroviteBtn.hidden = YES;
        cell.cancelFavoriteBtn.hidden = YES;
    }
    cell.cansee = YES;
    
    //  if ([paying isEqualToString:@"no"]) {
    //      if ([verified isEqualToString:@"yes"]) {
    
    //         if (indexPath.row>9) {
    //             cell.cansee = NO;
    //        }
    //     }else{
    
    //         if (indexPath.row>2) {
    //            cell.cansee = NO;
    //          }
    //       }
    //   }
    
    return cell;
}

//set row selected
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    MyMatchesProposedCell *cell = (MyMatchesProposedCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    NSMutableDictionary         *dicMatchesInfo = [self.flickrPaginator.results objectAtIndex:indexPath.row];
    
    //========be selected ,change the status of 'view_ststus"===============//
    NSString *view_status = [dicMatchesInfo objectForKey:@"view_status"];
    //NSLog(@"dicMatchesInfo==%@",[dicMatchesInfo objectForKey:@"nname"]);
    if ([view_status isEqualToString:@"N"]) {
        view_status=@"Y";
        [dicMatchesInfo setValue:view_status forKey:@"view_status"];
        myMatchesProposedCell.nameLabel.font=[UIFont systemFontOfSize:15.0];
        //        myMatchesProposedCell.nameLabel.font=[UIFont systemFontOfSize:15.0];
    }
    
    MatchProfileViewController  *matchInfoViewCtrl = [MatchProfileViewController createWithMatchInfo:dicMatchesInfo];
    
    matchInfoViewCtrl.m_bTopButton = YES;
    matchInfoViewCtrl.m_bDismissSelf = YES;
    matchInfoViewCtrl.delegateListViewCtrl = self;
    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.navigationController pushViewController:matchInfoViewCtrl animated:YES];
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    [tableView reloadData];
    
    //    BOOL cansee = NO;
    //    if ([paying isEqualToString:@"yes"]) {
    //        cansee = YES;
    //    } else {
    //
    //        if ([verified isEqualToString:@"yes"]) {
    //
    //            if (indexPath.row<10) {
    //                cansee = YES;
    //            }
    //        }else{
    //
    //            if (indexPath.row<3) {
    //                cansee = YES;
    //            }
    //        }
    //    }
    //    if (cansee) {
    //        NSMutableDictionary         *dicMatchesInfo = [self.flickrPaginator.results objectAtIndex:indexPath.row];
    //        MatchProfileViewController  *matchInfoViewCtrl = [MatchProfileViewController createWithMatchInfo:dicMatchesInfo];
    //
    //        matchInfoViewCtrl.m_bTopButton = YES;
    //        matchInfoViewCtrl.m_bDismissSelf = YES;
    //        matchInfoViewCtrl.delegateListViewCtrl = self;
    //        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.navigationController pushViewController:matchInfoViewCtrl animated:YES];
    //
    //        [tableView deselectRowAtIndexPath:indexPath animated:NO];
    //    }else{
    //        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"eSynchrony" message:[NSString stringWithFormat:@"To view and go on dates with %@,subscribe to our date package now.",cell.nameLabel.text] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    //        alert.tag = 2004;
    //        [alert show];
    //        [alert release];
    //
    //    }
}

//#pragma mark Memory management
//-(void)didReceiveMemoryWarning{
//    [super didReceiveMemoryWarning];
//    if ([[UIDevice currentDevice].systemVersion floatValue] >= 6.0) {
//        if ([self isViewLoaded]&& self.view.window == nil) {//是否是正在使用的视图
//            // Add code to preserve data stored in the views that might be
//            // needed later.
//
//            // Add code to clean up other strong references to the view in
//            // the view hierarchy
//            self.view = nil;//The objective is to re-enter call viewDidLoad function can be reloaded.
//        }
////        arrMatchesItems = nil;
//    }
//}

- (void)dealloc
{
    listTblView = nil;
    [listTblView release];
    
    //    _nmPaginator.results = nil;
    //    [_nmPaginator.results release];
    
    _flickrPaginator.arrMatchesItems = nil;
    [_flickrPaginator.arrMatchesItems release];
    [arrMatchesItems release];
    [_activityIndicator release];
    [_activityIndicatorHeder release];
    [_footerLabel release];
    [footerView release];
    [_headerLabel release];
    [headerView release];
    p_result = nil;
    [p_result release];
    [image release];
    
    
    [login_dt release];
    [tmp_dt release];
    [paying release];
    [verified release];
    [imageBackGround release];
    [lblInstructions release];
    [lblInstructions1 release];
    //    [self.view release];
    [super dealloc];
}

@end


#import <UIKit/UIKit.h>
//#import "mainMenuViewController.h"
#import "MyMatchesProposedCell.h"
#import "StoreKit/SKStoreProductViewController.h"
#import "FlickrPaginator.h"
#import "MyMatchesApprovedCell.h"
@class mainMenuViewController;

@interface MyMatchesProposedViewController : GAITrackedViewController<UITableViewDataSource, UITableViewDelegate, MyMatchesProposedCellDelegate,UIAlertViewDelegate,SKStoreProductViewControllerDelegate,NMPaginatorDelegate, UIScrollViewDelegate,TTTAttributedLabelDelegate>
{
    IBOutlet UITableView    *listTblView;
    IBOutlet TTTAttributedLabel        *lblInstructions;
    IBOutlet UILabel *lblInstructions1;
    IBOutlet UIImageView    *imageBackGround;
    
    IBOutlet UIButton       *btnInstructions;
    
    NSMutableArray          *arrMatchesItems;
    NSString *login_dt;
    NSString *tmp_dt;
    NSString *paying;
    NSString *verified;
    BOOL                    nibsRegistered;
}

@property (nonatomic, assign) mainMenuViewController    *mainMenuView;
@property (nonatomic, strong) FlickrPaginator *flickrPaginator;
//@property (nonatomic, strong) NMPaginator *nmPaginator;

@property (nonatomic, strong) UILabel *footerLabel;
//@property (nonatomic, strong) UIButton *btn;

@property (nonatomic, strong) UILabel *headerLabel;
@property (nonatomic, strong) UIImageView *image;

@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicatorHeder;
@property (nonatomic, assign)UIView *footerView;
@property (nonatomic, assign)UIView *headerView;//append
@property (nonatomic, assign)int num;
@property (nonatomic, assign)NSString *login_time;
@property (nonatomic, assign)NSString *tmp_time;


@property (nonatomic, assign)NSString *pay;
//@property (nonatomic, assign) NSString *login_dt;
//@property (nonatomic, assign) NSString *tmp_dt;
//@property (nonatomic, assign) NSString *paying;
//@property (nonatomic, assign) NSString *verified;

@property (nonatomic,retain)MyMatchesProposedCell *myMatchesProposedCell;
- (IBAction)onClickFilter:(UIButton *)sender;

-(void)refreshContent;
- (void)fetchBackPage:(int)type;
-(void)fetchCommentPage:(int)type;

-(void)renewalPopUp;
@end



#import <UIKit/UIKit.h>

@class MatchProfileViewController;
@class MyMatchesProposedViewController;

@protocol MyMatchesProposedCellDelegate <NSObject>

@optional

- (void)refreshContent;
- (void)fetchBackPage:(int)type;

@end

@interface MyMatchesProposedCell : UITableViewCell <UIAlertViewDelegate>
{
    IBOutlet UIImageView    *photoBGView;
    IBOutlet UIImageView    *photoImgView;
    IBOutlet UIImageView    *photoFrameView;
    
    IBOutlet UIImageView    *rightView;
    
    
    IBOutlet UILabel        *ageLabel;
    IBOutlet UILabel        *matchedIdxLabel;
    IBOutlet UILabel        *storyLabel;
    
    NSMutableDictionary*   _dicMatchesInfo;
    NSMutableArray          *arrMatchesItems;
    
    BOOL    bGotoUpgrade;
}
@property (retain, nonatomic) IBOutlet UIButton *faroviteBtn;
@property (retain, nonatomic) IBOutlet UIButton *cancelFavoriteBtn;

@property (retain, nonatomic) IBOutlet UILabel *nameLabel;
@property (nonatomic, assign) id<MyMatchesProposedCellDelegate> delegate;
@property (nonatomic, assign) id delegateListViewCtrl;

@property (nonatomic,strong)NSString *name;
@property (nonatomic,assign)NSInteger cellPage;

@property BOOL cansee ;
@property int limit;
- (void)setMatchesInfo:(NSMutableDictionary*)dicMatchesInfo;

- (IBAction)didClickView:(id)sender;
- (IBAction)didClickApprove:(id)sender;
- (IBAction)didClickFavorite:(id)sender;
- (IBAction)didClickCancellFavorite:(id)sender;

@end

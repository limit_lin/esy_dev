//
//  eSynchronyFilterViewController.h
//  eSyncrony
//
//  Created by ESYNSZ-Limit on 16/1/29.
//  Copyright © 2016年 WonMH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "eSyncronyAppDelegate.h"
#import "FilterSettingViewController.h"

@interface eSynchronyFilterViewController : UIViewController
{
    
    IBOutlet UIScrollView *scrlView;
    IBOutlet UIView *contentsView;
    
    IBOutlet UIButton *filterAgeBtn;
    IBOutlet UIButton *filterHeightBtn;
    IBOutlet UIButton *filterEducationBtn;
    IBOutlet UIButton *filterMarriageBtn;
    IBOutlet UIButton *filterIncomeBtn;
    IBOutlet UIButton *filterJobBtn;
    IBOutlet UIButton *filterReligionBtn;
    IBOutlet UIButton *filterEthnicityBtn;
    
    FilterSettingViewController   *FilterProfileSettingViewCtrl;
    NSMutableArray                  *itemArray;
    
   
}

@property(nonatomic,strong)mainMenuViewController *mainMenuViewController;
//match criteria label
@property (retain, nonatomic) IBOutlet UILabel *filterAgeLabel;
@property (retain, nonatomic) IBOutlet UILabel *filterHeightLabel;
@property (retain, nonatomic) IBOutlet UILabel *filterEducationLabel;
@property (retain, nonatomic) IBOutlet UILabel *filterMarriageLabel;
@property (retain, nonatomic) IBOutlet UILabel *filterIncomeLabel;
@property (retain, nonatomic) IBOutlet UILabel *filterJobLabel;
@property (retain, nonatomic) IBOutlet UILabel *filterReligionLabel;
@property (retain, nonatomic) IBOutlet UILabel *filterEthnicityLabel;


@property (nonatomic, strong) NSString          *filterAgeSelStr;
@property (nonatomic, strong) NSString          *filterHeightSelStr;
@property (nonatomic, strong) NSString          *filterEducationSelStr;
@property (nonatomic, strong) NSString          *filterMarriageSelStr;
@property (nonatomic, strong) NSString          *filterIncomeSelStr;
@property (nonatomic, strong) NSString          *filterJobSelStr;
@property (nonatomic, strong) NSString          *filterReligionSelStr;
@property (nonatomic, strong) NSString          *filterEthnicitySelStr;

@property (nonatomic, strong) NSMutableArray    *editItemArray;
@property (nonatomic, assign) NSInteger         editItemType;

- (IBAction)FilteronClickCancelled:(id)sender;
- (IBAction)FilteronClickNext:(id)sender;
- (IBAction)FilteronClickSave:(id)sender;


//match Criteria button action
- (IBAction)filterdidClickAgeBtn:(id)sender;
- (IBAction)filterdidClickHeihtBtn:(id)sender;
- (IBAction)filterdidClickEducationBtn:(id)sender;
- (IBAction)filterdidClickMarriageBtn:(id)sender;
- (IBAction)filterdidClickIncomeBtn:(id)sender;
- (IBAction)filterdidClickJobBtn:(id)sender;
- (IBAction)filterdidClickReligionBtn:(id)sender;
- (IBAction)filterdidClickEthnicityBtn:(id)sender;

- (void)filter_receiveEditResultWithSelectedItemNumString:(NSString *)selNumStr;
@end

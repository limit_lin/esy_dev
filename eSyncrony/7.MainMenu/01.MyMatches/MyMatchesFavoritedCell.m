//
//  MyMatchesFavoritedCell.m
//  eSyncrony
//
//  Created by ESYNSZ-Limit on 16/2/14.
//  Copyright © 2016年 WonMH. All rights reserved.
//
#import "Global.h"
#import "UtilComm.h"
#import "DSActivityView.h"
#import "ImageUtil.h"
#import "UIImageView+WebCache.h"
#import "MatchProfileViewController.h"
#import "mainMenuViewController.h"
#import "MyMatchesFavoritedCell.h"

#import "eSyncronyAppDelegate.h"

@implementation MyMatchesFavoritedCell
@synthesize name;

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
- (void)loadDefaultImage{
    if( [eSyncronyAppDelegate sharedInstance].gender == 1 ){
        photoImgView.image = [UIImage imageNamed:@"male1.png"];
    }else {
        photoImgView.image = [UIImage imageNamed:@"female1.png"];
    }
}

- (BOOL)downloadMatchUserPhoto
{
    NSString        *macc_no = [_dicMatchesInfo objectForKey:@"acc_no"];
    if( macc_no == nil )
        macc_no = [_dicMatchesInfo objectForKey:@"Acc_no"];
    if( macc_no == nil )
        return FALSE;
    
    if( [[_dicMatchesInfo objectForKey:@"view_photo"] isEqualToString:@"photo view not allowed."] )
        return FALSE;
    //    NSLog(@"downloadMatchUserPhoto");
    
    NSString*   imageURL = [NSString stringWithFormat:@"%@%@", WEBSERVICE_PHOTO_BASEURI, [_dicMatchesInfo objectForKey:@"filename"]];
    NSString*   imagePath = [ImageUtil loadImagePathFromURL:imageURL];
    
    //    NSLog(@"imagePath == %@\n",imagePath);
    [self performSelectorOnMainThread:@selector(FavototedCell_onDownLoadImage:) withObject:imagePath waitUntilDone:YES];
    
    return TRUE;
}

- (void)FavototedCell_onDownLoadImage:(NSString*)imagePath
{
    UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
    //    UIImage *image =  [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imagePath]]];
    if( image != nil )
    {
        //      photoFrameView.hidden = NO;
        photoImgView.image = image;
        [_dicMatchesInfo setObject:imagePath forKey:@"localphoto_path"];
    }
    else{
        [_dicMatchesInfo setObject:@"nophoto" forKey:@"localphoto_path"];
    }
    
}
- (void)setMatchesFavoriteInfo:(NSMutableDictionary*)dicMatchesInfo
{
    _dicMatchesInfo = dicMatchesInfo;
    
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
    
    photoBGView.layer.cornerRadius = 5;
    photoImgView.layer.cornerRadius = 45;
    photoFrameView.hidden = YES;
    
    rightView.layer.cornerRadius = 5;
    
    self.nameLabel.text = [dicMatchesInfo objectForKey:@"nname"];
    
    ///////////////////当view_status=@"Y"-->>have seen,字体应该不变/////////////////////////
    NSString *view_status = [dicMatchesInfo objectForKey:@"view_status"];
    
    //NSLog(@"dicMatchesInfo==%@",[_dicMatchesInfo objectForKey:@"nname"]);
    
    if ([view_status isEqualToString:@"Y"]) {
        self.nameLabel.font=[UIFont systemFontOfSize:15.0];
    }
    else if([view_status isEqualToString:@"N"])
    {
        self.nameLabel.font=[UIFont boldSystemFontOfSize:15.0];
    }
    
    [self.nameLabel sizeToFit];
    
    NSString    *distance = [dicMatchesInfo objectForKey:@"distance"];
    if( distance == nil )
        distance = [dicMatchesInfo objectForKey:@"Distance"];
    
    matchedIdxLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Match Index : %@",@"MyMatchesPendingCell"),distance];
    
    [matchedIdxLabel sizeToFit];
    
    NSString *strImage = nil;
    
    if( ![[_dicMatchesInfo objectForKey:@"view_photo"] isEqualToString:@"photo view not allowed."]){
        strImage = [NSString stringWithFormat:@"%@%@", WEBSERVICE_PHOTO_BASEURI, [_dicMatchesInfo objectForKey:@"filename"]];
    }
    
    NSString *strPlaceholder;
    if( [eSyncronyAppDelegate sharedInstance].gender == 1 ){
        strPlaceholder = @"male1.png";
    }else {
        strPlaceholder = @"female1.png";
    }
    
    photoImgView.layer.cornerRadius = photoImgView.frame.size.width/2;
    photoImgView.layer.masksToBounds = true;
    photoImgView.layer.borderColor = [UIColor whiteColor].CGColor;
    photoImgView.layer.borderWidth = 1.0;
    
    [photoImgView sd_setImageWithURL:[NSURL URLWithString:strImage] placeholderImage:[UIImage imageNamed:strPlaceholder]];//0901
    
}

- (IBAction)didClickView:(id)sender {
    
    MatchProfileViewController  *matchInfoViewCtrl = [MatchProfileViewController createWithMatchInfo:_dicMatchesInfo];
    
    matchInfoViewCtrl.m_bTopButton = YES;
    matchInfoViewCtrl.m_bDismissSelf = YES;
    matchInfoViewCtrl.delegateListViewCtrl = self.delegateListViewCtrl;
    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.navigationController pushViewController:matchInfoViewCtrl animated:YES];
    
    ///////////////////当view_status=@"Y"-->>if user have seen,fonts should be thinned/////////////////////////
    name=self.nameLabel.text;
    // NSLog(@"%@",name);
    // NSLog(@"dicMatchesInfo==%@",[_dicMatchesInfo objectForKey:@"nname"]);
    
    // if ([name isEqualToString:self.nameLabel.text]) {
    if ([name isEqualToString:[_dicMatchesInfo objectForKey:@"nname"]]) {
        
        self.nameLabel.font=[UIFont systemFontOfSize:15.0];
        
        NSString *view_status = [_dicMatchesInfo objectForKey:@"view_status"];
        // NSLog(@"view_status==%@",view_status);
        if([view_status isEqualToString:@"N"])
        {
            view_status=@"Y";
            // NSLog(@"view_statusview_status==%@",view_status);
            [_dicMatchesInfo setValue:view_status forKey:@"view_status"];
        }
        
    }
    ///////////////////////////////////////////////////////
    
    
}

- (IBAction)didClickApprove:(id)sender {
    UIView  *parentView = [eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.view;
    [DSBezelActivityView newActivityViewForView:parentView withLabel:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(Favorite_requestApproveToSever) withObject:nil afterDelay:0.1];
}

- (void)Favorite_requestApproveToSever{
    NSString        *macc_no = [_dicMatchesInfo objectForKey:@"acc_no"];
    NSDictionary    *result = [UtilComm approveToMatch:macc_no];
    [DSBezelActivityView removeViewAnimated:NO];
    
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"eSynchrony"];
        return;
    }
    
    NSString    *response = [result objectForKey:@"response"];
    if( ![response isKindOfClass:[NSString  class]] )
    {
        [ErrorProc alertMessage:@"Unknown Error" withTitle:@"Error"];
        return;
    }
    
    if( [response hasPrefix:@"ERROR"] )
    {
        [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl onServerConnectionError:response];
        return;
    }
    
    if( [response isEqualToString:@"YOU GOT A MATCH"]||[response isEqualToString:@"You have a mutual match!!"] )
    {
        bGotoUpgrade = FALSE;
        
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"The page at https://www.esynchrony.com says:",@"MyMatchesPendingCell")
                                                     message:[NSString stringWithFormat:NSLocalizedString(@"Confirm to arrange date with %@",@"MyMatchesPendingCell"),_nameLabel.text]
                                                    delegate:self
                                           cancelButtonTitle:NSLocalizedString(@"OK",@"MyMatchesPendingCell")
                                           otherButtonTitles:NSLocalizedString(@"Cancel",@"MyMatchesPendingCell"), nil];
        
        [av show];
        [av release];
    }
    else
    {
        bGotoUpgrade = TRUE;
        
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"eSynchrony"
                                                     message:response
                                                    delegate:self
                                           cancelButtonTitle:NSLocalizedString(@"OK",)
                                           otherButtonTitles:nil, nil];
        [av show];
        [av release];
    }
    
}
-(void)Favorite_requestCancellFavoriteToSever
{
    NSLog(@"cancel favorite  hahha...");
    NSDictionary* result = [UtilComm removeFavoritedMatch:[_dicMatchesInfo objectForKey:@"acc_no"]];
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Error"];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if ([response isKindOfClass:[NSString class]] && [response isEqualToString:@"SUCCESS"]) {
        
        [self.delegate refreshContent];
    }
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"])//ERROR4
    {
        UIAlertView *alter = [[UIAlertView alloc]initWithTitle:@"eSynchrony" message:NSLocalizedString(@"Request cancel favorite this matches is occured error!", @"MyMatchesFavoritedCell") delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alter show];
        [alter release];
        return;
    }
    else
        return;
}

- (IBAction)didClickCancellFav:(id)sender {
    
    [self performSelector:@selector(Favorite_requestCancellFavoriteToSever) withObject:nil afterDelay:0.1];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0) {
        if( bGotoUpgrade == FALSE )
            [self.delegate refreshContent];
        else
            [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl procUpgrade];
    }
    
}

- (void)dealloc {
    [photoBGView release];
    [photoImgView release];
    [photoFrameView release];
    [rightView release];
    [matchedIdxLabel release];
    [_nameLabel release];
    [super dealloc];
}
@end

//
//  FilterSettingViewController.h
//  eSyncrony
//
//  Created by ESYNSZ-Limit on 16/2/1.
//  Copyright © 2016年 WonMH. All rights reserved.
//

#import <UIKit/UIKit.h>
@class eSynchronyFilterViewController;

@interface FilterSettingViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UIView *selView;
    IBOutlet UILabel *titleLabel;
    IBOutlet UIImageView *deviderView;
    IBOutlet UIButton       *okBtn;

}
@property (nonatomic, strong) NSMutableArray            *itemArray;
@property (nonatomic, strong) eSynchronyFilterViewController *parent;
@property (nonatomic, strong) NSString                  *titleString;
@property (nonatomic, strong) IBOutlet UITableView      *tblView;
@property (nonatomic, assign) BOOL                      isAllowMultySel;
@property (nonatomic, strong) NSString                  *selNumStr;

- (IBAction)didClickOKBtn:(id)sender;
- (IBAction)didClickCancelledBtn:(id)sender;

- (void)initSetting;

@end

#import <Parse/Parse.h>
#import <FacebookSDK/FacebookSDK.h>
#import "MemberLoginViewController.h"
#import "eSyncronyViewController.h"
#import "eSyncronyAppDelegate.h"
#import "forgotPasswordViewController.h"
#import "DSActivityView.h"
#import "Global.h"
#import "XMLParser.h"
#import "UtilComm.h"
#import "CreateAccountMainViewController.h"
//#include <Windows.h>
//#include <iostream>

@interface MemberLoginViewController ()
{
    
    IBOutlet UIButton *pBackBtn;
    IBOutlet UILabel *labelCapLock;
}
@end

@implementation MemberLoginViewController
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"MemberLoginView";
    emailAddrTxtFld.delegate = self;    
    pwTxtFld.delegate = self;
    
    if ([[eSyncronyAppDelegate sharedInstance].language hasPrefix:@"id"]) {
        pBackBtn.frame = CGRectMake(0, 20, 75, 53);
    }
#if 1
//    emailAddrTxtFld.text = @"test030102@qq.com";
//    emailAddrTxtFld.text = @"test030203@qq.com";
    emailAddrTxtFld.text = @"limitlin@lunchactually.com";
//    emailAddrTxtFld.text = @"zam@eteract.com";
    pwTxtFld.text = @"12345678";
    
#endif
//    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnTableView:)];//leaks
    UITapGestureRecognizer *tapRecognizer = [[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnTableView:)]autorelease];
    tapRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapRecognizer];
}

- (void)tapOnTableView:(UITapGestureRecognizer *)gesture
{
    [self.view endEditing:YES];
    [self restoreViewPosition];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if( textField.tag != 100 )
    {
        if (([string compare:@"A"] == NSOrderedSame||[string compare:@"A"]==NSOrderedDescending)&&([string compare:@"Z"] == NSOrderedSame||[string compare:@"Z"]==NSOrderedAscending)) {
            labelCapLock.hidden = NO;
        }else
        {
            labelCapLock.hidden = YES;
        }
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if( textField.tag == 100 )
    {
        [pwTxtFld becomeFirstResponder];
        return YES;
    }

    [textField resignFirstResponder];
    [self restoreViewPosition];
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if( textField.tag != 100 )
    {
        CGRect rect = [textField convertRect:textField.bounds toView:self.view];
        
        float   offset;
        
        offset = self.view.frame.size.height - (rect.size.height + 240);
        offset = offset - rect.origin.y;
        
        if( offset > 0 )
            return;
        
        rect = self.view.frame;
        rect.origin.y = offset;
        
        self.view.frame = rect;
    }
    else
        [self restoreViewPosition];
    
}
- (BOOL)isValidEmailAddress:(NSString *)emailAddress//judge email address 0723
{
    
    BOOL stricterFilter = YES;
    //NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-].+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *stricterFilterString = @"[\\._%+-=*/?,;:'{}].+@([_%+-=*/?,;:']+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",emailRegex];
    NSRange rng1 = [emailAddress rangeOfString:@"@"];
//    NSRange rng2 = [emailAddress rangeOfString:@".com"];
//    NSLog(@"%lu ",rng1.location);
    
    BOOL bFlag1 = [emailTest evaluateWithObject:emailAddress];
    BOOL bFlag2 = rng1.location == NSNotFound;
//    BOOL bFlag3 = rng2.location == NSNotFound;
//    BOOL bFlag3 = rng2.location == emailAddress.length-4;
    BOOL judge = bFlag1||bFlag2;
    return judge;
//    return bFlag1 || bFlag2 || !bFlag3;
//    return YES;
}

- (BOOL)isValidLoginInfo
{
    if( [emailAddrTxtFld.text isEqualToString:@""] ) {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Field Required",) message:NSLocalizedString(@"Please enter email address.",@"MemberLogin") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",) otherButtonTitles:nil, nil];
        [av show];
        [av release];
        
        return NO;
    }

    if([self isValidEmailAddress:emailAddrTxtFld.text] ) {//!--->remove
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Invalid Format",) message:NSLocalizedString(@"Please enter a valid email address.",@"MemberLogin") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",) otherButtonTitles:nil, nil];
        [av show];
        [av release];

        return NO;
    }
    
    if( [pwTxtFld.text isEqualToString:@""] ) {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Field Required",) message:NSLocalizedString(@"Please enter login password.",@"MemberLogin") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",) otherButtonTitles:nil, nil];
        [av show];
        [av release];

        return NO;
    }
    
    return YES;
}

- (void)didLogins
{
    [self.navigationController popViewControllerAnimated:NO];

//    [[eSyncronyAppDelegate sharedInstance].viewController checkUserStep];
    [[eSyncronyAppDelegate sharedInstance].viewController showBusyDialogWithTitle:NSLocalizedString(@"Loading...",)];
    [[eSyncronyAppDelegate sharedInstance].viewController performSelector:@selector(procCheckUserStep) withObject:nil afterDelay:0.1];
}

- (IBAction)didClickBackBtn:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#define APP_ID @"226737744171417"
- (IBAction)didClickLoginWithFaceBook:(id)sender
{
//    NSArray *permissionsArray = @[ @"email", @"user_about_me", @"user_relationships", @"user_birthday", @"user_location"];

    [_activityIndicator startAnimating]; // Show loading indicator until login is finished

    // Login PFUser using facebook
    /*
    [PFFacebookUtils logInWithPermissions:permissionsArray block:^(PFUser *user, NSError *error) {
        [_activityIndicator stopAnimating]; // Hide loading indicator
    
        if( user ) {
            [self loginWithFaceBook];
        }
    }];*/

    [FBSession openActiveSessionWithReadPermissions:@[@"email",@"user_birthday", @"user_location",@"user_education_history"]
                                       allowLoginUI:YES
                                  completionHandler:
     ^(FBSession *session,
       FBSessionState state, NSError *error) {
         if( state == FBSessionStateOpen ){
             NSLog(@"StateOpen--->loginWithFaceBook");
             [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Authenticating...",)];
             [self loginWithFaceBook];
             
         }else{
             NSLog(@"loginWithFaceBook Something went wrong");
             NSString *alertText = nil;
             NSString *alertTitle = nil;
             // If the error requires people using an app to make an action outside of the app in order to recover
             
             NSDictionary *errorInformation = [[[error.userInfo objectForKey:@"com.facebook.sdk:ParsedJSONResponseKey"] objectForKey:@"body"] objectForKey:@"error"];
             
             if ([FBErrorUtility shouldNotifyUserForError:error] == YES){
                 alertTitle = NSLocalizedString(@"Something went wrong",);
                 alertText = [FBErrorUtility userMessageForError:error];

             } else {
                 
                 // If the user cancelled login, do nothing
                 if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
                     alertTitle = NSLocalizedString(@"Info",);
                     alertText = NSLocalizedString(@"User cancelled login.",);

                     // Handle session closures that happen outside of the app
                 } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession){
                     alertTitle = NSLocalizedString(@"Session Error",);
                     alertText = NSLocalizedString(@"Your current session is no longer valid. Please log in again.",);

                     
                     // Here we will handle all other errors with a generic error message.
                     // We recommend you check our Handling Errors guide for more information
                     // https://developers.facebook.com/docs/ios/errors/
                 } else {
                     //Get more error information from the error
 
                     // Show the user an error message
                     alertTitle = NSLocalizedString(@"Please retry.",);
                     alertText = [NSString stringWithFormat:NSLocalizedString(@"If the problem persists contact us and mention this error code: %@",@"MemberLogin"), [errorInformation objectForKey:@"message"]];

                 }
                 
             }
             if ([errorInformation objectForKey:@"message"]) {
                 UIAlertView *av = [[UIAlertView alloc] initWithTitle:alertTitle
                                                              message:alertText
                                                             delegate:nil
                                                    cancelButtonTitle:NSLocalizedString(@"OK",)
                                                    otherButtonTitles:nil, nil];
                 [av show];
                 [av release];
                 
             }
             [[eSyncronyAppDelegate sharedInstance]logoutFB];
         }
     }];
}

- (void)loginWithFaceBook
{
    // Send request to Facebook
    FBRequest *request = [FBRequest requestForMe];
    [request startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        // handle response
        if( !error ) {
            // Parse the data received
            NSDictionary *userData = (NSDictionary *)result;
            NSLog(@"Facebook userData:%@",userData);
            self.face_bookId = userData[@"id"];
            self.email = userData[@"email"];
            self.email = [self.email stringByReplacingOccurrencesOfString:@"%%40" withString:@"@"];
            self.birthday =userData[@"birthday"];
            
            self.gender = userData[@"gender"];
            if( [self.gender isEqualToString:@"male"] )
                self.gender = @"M";
            else
                self.gender = @"F";
            
            self.firstname = userData[@"first_name"];
            self.lastname = userData[@"last_name"];
            self.username = userData[@"first_name"];
            
            self.location = [[userData objectForKey:@"location"]objectForKey:@"name"];
            if ([self.location rangeOfString:@"Málai"].location != NSNotFound){
                self.countryid = 1;
            }else if([self.location rangeOfString:@"Hong Kong"].location != NSNotFound){
                self.countryid = 2;
            }else if([self.location rangeOfString:@"Indonesia"].location != NSNotFound){
                self.countryid = 7;
            }else if([self.location rangeOfString:@"Thailand"].location != NSNotFound){

                self.countryid = 202;//203-1
            }
            else{
                self.countryid = 0;
            }

            if (userData[@"email"]==nil) {
                [DSBezelActivityView removeViewAnimated:NO];
                UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Authentication Failure",)
                                                             message:NSLocalizedString(@"Your Email has not been verified by Facebook.",@"MemberLogin")
                                                            delegate:nil
                                                   cancelButtonTitle:NSLocalizedString(@"OK",)
                                                   otherButtonTitles:nil, nil];
                [av show];
                [av release];
                [[eSyncronyAppDelegate sharedInstance]logoutFB];
            }else{
//                [DSBezelActivityView removeViewAnimated:NO];
            [self performSelector:@selector(procLoginWithFB) withObject:nil afterDelay:0.01];
            }
        }else{
            [DSBezelActivityView removeViewAnimated:NO];
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Sorry",)
                                                         message:NSLocalizedString(@"Facebook login Failed,please try again later.",@"MemberLogin")
                                                        delegate:nil
                                               cancelButtonTitle:NSLocalizedString(@"OK",)
                                               otherButtonTitles:nil, nil];
            [av show];
            [av release];
            NSLog(@"error:%@",error);
            
        }
    }];
}

- (void)procLoginWithFB
{
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    [params setObject:self.face_bookId forKey:@"facebook_id"];
    [params setObject:self.email forKey:@"email"];
    NSDictionary*    result = [UtilComm loginWithFaceBook:params];
    [DSBezelActivityView removeViewAnimated:NO];
    
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Authentication Failure"];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( response == nil )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Saving Failure",)];
        return;
    }

    if( [response isKindOfClass:[NSString class]]  )
    {
        if ([response isEqualToString:@"Not Registered"]) {
            
            CreateAccountMainViewController *createAccountView = [[[CreateAccountMainViewController alloc] initWithNibName:@"CreateAccountMainViewController" bundle:nil] autorelease];
            
            createAccountView.nStep = 1;
            createAccountView.parentView = [eSyncronyAppDelegate sharedInstance].viewController;
            createAccountView.Reg_Name = self.firstname;
            createAccountView.Reg_Gender = self.gender;
            createAccountView.Reg_Email = self.email;
            createAccountView.Reg_CountryId = self.countryid;
            createAccountView.Reg_facebookId =self.face_bookId;
            createAccountView.birthday = self.birthday;
            [self.navigationController pushViewController:createAccountView animated:YES];
        }
//        [ErrorProc alertLoginError:response];
    }
    else
    {
        [eSyncronyAppDelegate sharedInstance].strFaceBookID = self.face_bookId;
        [eSyncronyAppDelegate sharedInstance].strLoginEmail     = self.email;
        [eSyncronyAppDelegate sharedInstance].strLoginPassword  = @"";
        [eSyncronyAppDelegate sharedInstance].strName = [response objectForKey:@"nname"];
        [eSyncronyAppDelegate sharedInstance].membership = [response objectForKey:@"membership"];
        self.gender = [response objectForKey:@"gender"];
        if( [self.gender isEqualToString:@"M"] ){
            [eSyncronyAppDelegate sharedInstance].gender = 0;
        }
        else{
            [eSyncronyAppDelegate sharedInstance].gender = 1;
        }

        [eSyncronyAppDelegate sharedInstance].strAccNo = [response objectForKey:@"acc_no"];
        [eSyncronyAppDelegate sharedInstance].strTokenKey = [response objectForKey:@"token_key"];
        [eSyncronyAppDelegate sharedInstance].countryId = [[response objectForKey:@"cty_id"]intValue]-1;
        
        [eSyncronyAppDelegate sharedInstance].isNoNric = @"";//append verify nric,photo
        [eSyncronyAppDelegate sharedInstance].isNoPhoto = @"";
        
        [eSyncronyAppDelegate sharedInstance].isVip = [response objectForKey:@"is_vip"];
        [eSyncronyAppDelegate sharedInstance].checkVipStroy = [response objectForKey:@"set_vip_story"];
        [eSyncronyAppDelegate sharedInstance].paying = [response objectForKey:@"paying"];
        
        [eSyncronyAppDelegate sharedInstance].bIsLoggedIn = YES;
        [[eSyncronyAppDelegate sharedInstance] saveLoginInfo];
        
        [self didLogins];
    }
}

- (void)processloginResult:(id)result
{
    
}

- (IBAction)didClickLoginBtn:(id)sender
{
    if( ![self isValidLoginInfo] )
        return;

    [eSyncronyAppDelegate sharedInstance].strLoginEmail     = emailAddrTxtFld.text;
    [eSyncronyAppDelegate sharedInstance].strLoginPassword  = pwTxtFld.text;

    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Authenticating...",)];
    
    [self performSelector:@selector(procLogin) withObject:nil afterDelay:0.01];
}

- (void)procLogin
{
    NSString*   email = emailAddrTxtFld.text;
    NSString*   password = pwTxtFld.text;
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    [params setObject:email forKey:@"email"];
    [params setObject:password forKey:@"password"];
    
    NSDictionary*    result = [UtilComm loginWithParams:params];
    [DSBezelActivityView removeViewAnimated:NO];
    
    if( result == nil )
    {
        [ErrorProc alertAuthenFailureMessage];
        return;
    }
    id response = [result objectForKey:@"response"];
    if( response == nil )
    {
        //[ErrorProc alertMessage:@"Unknown Error" withTitle:@"Saving Failure"];
//        [self didLogins];
        
        [ErrorProc alertAuthenFailureMessage];
        return;
    }

    if ([response isKindOfClass:[NSString class]]) {
        if( [response isEqualToString:@"Not Registered"] || [response hasPrefix:@"ERROR"] )
        {
            [ErrorProc alertLoginError:response];
            return;
        }
    }else if ([[response objectForKey:@"status"] isEqualToString:@"X"]||[[response objectForKey:@"status"] isEqualToString:@"U"]) {//0907
        
        UIAlertView *av= [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"The page at https://www.esynchrony.com says:",) message:NSLocalizedString(@"You might previously unsubscribed from our service. Please contact us to reactive your account.",) delegate:self cancelButtonTitle:NSLocalizedString(@"OK",) otherButtonTitles:nil, nil];
        [av show];
        [av release];
        
        //interface jump
        
    }else{
        [eSyncronyAppDelegate sharedInstance].strFaceBookID     = @"";
        [eSyncronyAppDelegate sharedInstance].strLoginEmail     = email;
        [eSyncronyAppDelegate sharedInstance].strLoginPassword  = password;
        [eSyncronyAppDelegate sharedInstance].strAccNo = [response objectForKey:@"acc_no"];
        [eSyncronyAppDelegate sharedInstance].strTokenKey = [response objectForKey:@"token_key"];
        [eSyncronyAppDelegate sharedInstance].strName = [response objectForKey:@"nname"];
        [eSyncronyAppDelegate sharedInstance].membership = [response objectForKey:@"membership"];
        [eSyncronyAppDelegate sharedInstance].countryId = [[response objectForKey:@"cty_id"]intValue]-1;
        
        [eSyncronyAppDelegate sharedInstance].isNoNric = [response objectForKey:@"noNric"];//append verify nric,photo
        [eSyncronyAppDelegate sharedInstance].isNoPhoto = [response objectForKey:@"noPhoto"];
        
        [eSyncronyAppDelegate sharedInstance].isVip = [response objectForKey:@"is_vip"];
        
        [eSyncronyAppDelegate sharedInstance].checkVipStroy = [response objectForKey:@"set_vip_story"];
        
        [eSyncronyAppDelegate sharedInstance].paying = [response objectForKey:@"paying"];
        
        if( [[response objectForKey:@"gender"] isEqualToString:@"M"] ){
            [eSyncronyAppDelegate sharedInstance].gender = 0;
        }
        else{
            [eSyncronyAppDelegate sharedInstance].gender = 1;
        }
        [eSyncronyAppDelegate sharedInstance].bIsLoggedIn = YES;
        [[eSyncronyAppDelegate sharedInstance] saveLoginInfo];

//         NSLog(@"memberLogin isNoNric == %@ isNoPhoto == %@",[eSyncronyAppDelegate sharedInstance].isNoNric,[eSyncronyAppDelegate sharedInstance].isNoPhoto);
        [self didLogins];
    }
}

- (IBAction)didClickForgotPwBtn:(id)sender
{
    forgotPasswordViewController *forgotPwView = [[[forgotPasswordViewController alloc] initWithNibName:@"forgotPasswordViewController" bundle:nil] autorelease];

    [self.navigationController pushViewController:forgotPwView animated:YES];
}

- (void)dealloc
{
    [emailAddrTxtFld release];
    [pwTxtFld release];

    [self.face_bookId release];
    [self.email release];
    [self.username release];
    [self.firstname release];
    [self.lastname release];
    [self.location release];
    [self.gender release];
    [self.birthday release];
    [labelCapLock release];
    [pBackBtn release];
    [super dealloc];
}

@end



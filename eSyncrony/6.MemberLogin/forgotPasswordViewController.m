
#import "forgotPasswordViewController.h"

#import "DSActivityView.h"
#import "Global.h"
#import "UtilComm.h"

#import "eSyncronyAppDelegate.h"
#import "mainMenuViewController.h"

@interface forgotPasswordViewController ()

@end

@implementation forgotPasswordViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"ForgotPasswordView";
    userEmailTf.delegate = self;
}

- (void)dealloc
{
    [userEmailTf release];
    
    [super dealloc];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [self restoreViewPosition];

    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGRect rect = [textField convertRect:textField.bounds toView:self.view];
    
    float   offset;
    
    offset = self.view.frame.size.height - (rect.size.height + 240);
    offset = offset - rect.origin.y;
    
    if( offset > 0 )
        return;
    
    rect = self.view.frame;
    rect.origin.y = offset;
    
    self.view.frame = rect;
}

- (BOOL)isValidEmailAddress:(NSString *)emailAddress
{
    return YES;
}

- (IBAction)didClickBackBtn:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)isValidLoginInfo
{
    if( [userEmailTf.text isEqualToString:@""] )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Please Input Email!",@"forgotPassword") withTitle:NSLocalizedString(@"ERROR",)];
        return NO;
    }

    if( ![self isValidEmailAddress:userEmailTf.text] )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Please enter a valid email address.",@"forgotPassword") withTitle:NSLocalizedString(@"Invalid Format",)];
        return NO;
    }

    return YES;
}

- (IBAction)didClickSubmitBtn:(id)sender
{
    if (![self isValidLoginInfo])
        return;

    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(requestPasswordProc) withObject:nil afterDelay:0.1];
}

- (void)requestPasswordProc
{
    NSString        *strEmail = userEmailTf.text;
    NSDictionary    *result = [UtilComm retrieveNewPassword:strEmail];

    [DSBezelActivityView removeView];
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"ERROR"];
        return;
    }
    
    id response = [result objectForKey:@"response"];//Update title for reset password notification
    //    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    //    {
    //        [ErrorProc alertMessage:NSLocalizedString(@"Email is not valid",@"forgotPassword") withTitle:NSLocalizedString(@"ERROR",)];
    //        return;
    ////        [NSString stringWithFormat:NSLocalizedString(@"Dear %@,",@"DocCert"),[eSyncronyAppDelegate sharedInstance].strName]
    //    }
    //    An email will be sent to your mail address. Please get the new password from there.
    //    [ErrorProc alertMessage:NSLocalizedString(@"Your password has successfuly sent to your email.",@"forgotPassword") withTitle:NSLocalizedString(@"ERROR",)];
    if( [response isKindOfClass:[NSString class]])
    {
        if ([response hasPrefix:@"ERROR"]) {
            [ErrorProc alertMessage:NSLocalizedString(@"Email is not valid",@"forgotPassword") withTitle:NSLocalizedString(@"ERROR",)];
            return;
        }else{
            [ErrorProc alertMessage:[NSString stringWithFormat:NSLocalizedString(@"%@",@"forgotPassword"),response] withTitle:@"eSynchrony"];
            return;
        }
    }
    [ErrorProc alertMessage:NSLocalizedString(@"Your password has successfuly sent to your email.",@"forgotPassword") withTitle:@"eSynchrony"];
}

@end
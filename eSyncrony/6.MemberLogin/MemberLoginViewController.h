
#import <UIKit/UIKit.h>
#import "InputBaseViewController.h"

@interface MemberLoginViewController : InputBaseViewController <UITextFieldDelegate>
{
    IBOutlet UITextField *emailAddrTxtFld;
    IBOutlet UITextField *pwTxtFld;
}

@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) NSString  *face_bookId;
@property (nonatomic, strong) NSString  *email;
@property (nonatomic, strong) NSString  *username;
@property (nonatomic, strong) NSString  *gender;
@property (nonatomic, strong) NSString  *firstname;
@property (nonatomic, strong) NSString  *lastname;
@property (nonatomic, strong) NSString  *birthday;
@property (nonatomic, strong) NSString  *location;
//@property (nonatomic, strong) NSString  *eduLevel;
//@property (nonatomic, strong) NSString  *nickname;

@property int countryid;
- (void)didLogins;

- (IBAction)didClickBackBtn:(id)sender;
- (IBAction)didClickLoginWithFaceBook:(id)sender;
- (IBAction)didClickLoginBtn:(id)sender;
- (IBAction)didClickForgotPwBtn:(id)sender;

@end

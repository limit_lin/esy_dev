
#import <UIKit/UIKit.h>
#import "InputBaseViewController.h"

@interface forgotPasswordViewController : InputBaseViewController <UITextFieldDelegate>
{
    IBOutlet UITextField *userEmailTf;
}

- (IBAction)didClickBackBtn:(id)sender;
- (IBAction)didClickSubmitBtn:(id)sender;

@end

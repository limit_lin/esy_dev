//
//  CustomMessageChat.m
//  eSyncrony
//
//  Created by ESYNSZ-Limit on 16/3/9.
//  Copyright © 2016年 WonMH. All rights reserved.
//
#import "Global.h"
#import "UtilComm.h"

#import "CustomMessageChat.h"
#import "SocketIO.h"
#import "eSyncronyAppDelegate.h"
#import "mainMenuViewController.h"
#import "SocketIOPacket.h"

#import "DSActivityView.h"

#define kMaxLength 255
@interface CustomMessageChat ()
{
    CGFloat     _fOrgViewFrameTop;
    BOOL _bInitOrgYPos;
    int m_curKeyboardHeight;
}
@end

@implementation CustomMessageChat

@synthesize chatMsgs;
@synthesize headerImg;

-(void)p_registerNotification{
    // system >=IOS8.0
    if([[[UIDevice currentDevice]systemVersion]floatValue] >=8.0)//151023
    {
        [[UIApplication sharedApplication]registerUserNotificationSettings:[UIUserNotificationSettings
                                                                            
                                                                            settingsForTypes:(UIUserNotificationTypeSound|UIUserNotificationTypeAlert|UIUserNotificationTypeBadge)
                                                                            
                                                                            categories:nil]];
        
        [[UIApplication sharedApplication]registerForRemoteNotifications];
        
    }else{//system < IOS8.0
//        registerForRemoteNotifications and registerUserNotificationSettings:
        
        [[UIApplication sharedApplication]registerForRemoteNotifications];
//        [[UIApplication sharedApplication]registerForRemoteNotificationTypes:
//
//         (UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeSound|UIRemoteNotificationTypeBadge)];
    }
    
    return;
}


-(void)reconnect
{
    if (socketIO == nil) {
        // create socket.io client instance
        socketIO = [[SocketIO alloc] initWithDelegate:self];
    }
    [socketIO disconnect];
    // connect to the socket.io server
    [socketIO connectToHost:SOCKETHOST onPort:CHATPORT];
}

- (void)keyboardWillShow:(NSNotification *)aNotification
{
    if ([messageTxt isFirstResponder] == NO)
    {
        return;
    }
    
    
    CGRect keyboardRect = [[[aNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    
    NSTimeInterval animationDuration = [[[aNotification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
//    CGRect bottomframe = m_bottomImage.frame;
//    bottomframe.origin.y -= keyboardRect.size.width - m_curKeyboardHeight;
    
    CGRect fieldframe = messageTxt.frame;
    fieldframe.origin.y -= keyboardRect.size.width - m_curKeyboardHeight;
    
    
    
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
//    m_bottomImage.frame = bottomframe;
    messageTxt.frame = fieldframe;
    
    [UIView commitAnimations];
    
    
    
    m_curKeyboardHeight = keyboardRect.size.width;
}

- (void)keyboardWillHide:(NSNotification *)aNotification
{
    if ([messageTxt isFirstResponder] == NO)
    {
        return;
    }
    
    NSTimeInterval animationDuration = [[[aNotification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
//    CGRect bottomframe = m_bottomImage.frame;
//    bottomframe.origin.y += m_curKeyboardHeight;
    
    CGRect fieldframe = messageTxt.frame;
    fieldframe.origin.y+= m_curKeyboardHeight;
    
    
    
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
//    m_bottomImage.frame = bottomframe;
    messageTxt.frame = fieldframe;
    
    [UIView commitAnimations];
    
    m_curKeyboardHeight = 0;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [firstView addSubview:topView];
    
    counslantName.text = [NSString stringWithFormat:@"%@ On Duty",@"Counslant"];
    m_curKeyboardHeight = 0;
    
//    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(kbFrmWillChange:) name:UIKeyboardWillChangeFrameNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];

    socketIO = [[SocketIO alloc] initWithDelegate:self];

    [socketIO connectToHost:SOCKETHOST onPort:CHATPORT];
    
    UITapGestureRecognizer *tapRecognizer = [[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(p_tapOnTableView:)]autorelease];
    tapRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapRecognizer];
    _bInitOrgYPos = FALSE;
    
    messageTxt.placeholder = @"Type your message here";
    [messageTxt setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [messageTxt setValue:[UIFont boldSystemFontOfSize:12] forKeyPath:@"_placeholderLabel.font"];
    [self p_registerNotification];
    
    
    chatMsgs = [[NSMutableArray alloc]init];
    id response = [UtilComm askSocketHistory];

    if (response == nil) {
        chatMsgs = nil;
//        [DSBezelActivityView removeViewAnimated:NO];
        return;
        
    }else if([response isKindOfClass:[NSArray class]])
    {
        NSMutableArray *arr = [response copy];
        
        //        NSLog(@"response.count  = %ld",arr.count);
        for (int i = 0 ; i < arr.count; i++) {
            [chatMsgs addObject:arr[i]];
            //            NSLog(@"chatMsgs include1 = %@",chatMsgs);
        }
        [self didReceiveResults:chatMsgs];
        
    }else{
        [chatMsgs addObject:response];
        //        NSLog(@"chatMsgs include2 = %@",chatMsgs);
        [self didReceiveResults:chatMsgs];
    }
    
    
//    [DSBezelActivityView removeViewAnimated:NO];//avoid blocking
    
}
-(void)reloadDataWithText
{
    if ((self.chatMsgs.count -1 )> 0) {
        NSIndexPath *lastPath = [NSIndexPath indexPathForRow:self.chatMsgs.count -1 inSection:0];
        [self.tableView beginUpdates];
        [self.tableView scrollToRowAtIndexPath:lastPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        //    [self.tableView scrollToRowAtIndexPath:lastPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
        [self.tableView endUpdates];
    }
    return;
}

- (void)didReceiveResults:(NSMutableArray *)results{
    [self reloadDataWithText];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    headerImg.layer.cornerRadius = headerImg.frame.size.width /2;
    headerImg.layer.masksToBounds= true;
    headerImg.layer.borderColor = [UIColor whiteColor].CGColor;
    headerImg.layer.borderWidth = 1.0;
    
//    [headerImg sd_setImageWithURL:[NSURL URLWithString:strImage] placeholderImage:[UIImage imageNamed:strPlaceholder]];
    
    sizeHeight = 0.0;
    
    if( _bInitOrgYPos == TRUE )
        return;
    
    _fOrgViewFrameTop = self.tableView.frame.origin.y;
    _bInitOrgYPos = TRUE;
    
//    [DSBezelActivityView newActivityViewForView:self.view];
}

# pragma mark -
# pragma mark socket.IO-objc delegate methods
- (void) socketIODidConnect:(SocketIO *)socket
{
    NSLog(@"socket.io connected.");
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:0];

    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"room"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strName forKey:@"username"];
    [socketIO sendEvent:@"userConnect" withData:params];
}

- (void) socketIO:(SocketIO *)socket didReceiveEvent:(SocketIOPacket *)packet
{
    NSLog(@"didReceiveEvent()->%@",packet.name);
    NSLog(@"packet.args == %@",packet.args);
    if ([packet.name isEqualToString:@"user joined"]) {

        NSString *createTime = [Global getCurrentDate];
//        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(100, sizeHeight+10, 200, 30)];
//        label.font = [UIFont systemFontOfSize:12];
//        label.textColor = RGBColor(2, 95, 108);
//        label.numberOfLines = 0;
////        label.textAlignment = NSTextAlignmentCenter;
//        label.text = [NSString stringWithFormat:@"chat started...\n%@",createTime];
//        [self.tableView addSubview:label];
        
        if ([[[packet.args objectAtIndex:0] objectForKey:@"acc_no"] isEqualToString:[eSyncronyAppDelegate sharedInstance].strAccNo]) {
            NSLog(@"chat started...");
            typingLab.text = [NSString stringWithFormat:@"chat started...\n%@",createTime];
        }else
        {
            NSString *joinName = [[packet.args objectAtIndex:0]objectForKey:@"username"];
            NSLog(@"this is %@ join",joinName);
        }
        
    }

    if ([packet.name isEqualToString:@"typing"]) {
        NSLog(@"consulant is typing...");
        typingLab.text = @"is typing...";
    }
    if ([packet.name isEqualToString:@"stop typing"]) {
        NSLog(@"consulant info appear");
        typingLab.text = nil;
        
    }
    if ([packet.name isEqualToString:@"user left"]) {
        
        NSLog(@"the consulant left");
        
        NSString *joinName = [[packet.args objectAtIndex:0]objectForKey:@"username"];
        NSLog(@"%@ left",joinName);
        
        
    }
    if ([packet.name isEqualToString:@"new message"]){
                
        [chatMsgs addObject:packet.args[0]];
        [self.tableView reloadData];
        [self reloadDataWithText];
        
//        NSIndexPath *lastPath = [NSIndexPath indexPathForRow:self.chatMsgs.count - 1 inSection:0];
//        [self.tableView scrollToRowAtIndexPath:lastPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }
}

- (void)socketIO:(SocketIO *)socket onError:(NSError *)error
{
    NSLog(@"onError() %@", error);
    
//    if ([eSyncronyAppDelegate sharedInstance].strAccNo.length != 0) {
//        if (![socketIO isConnected]) {
//            [self reconnect];
//        }
//    }
}

- (void) socketIODidDisconnect:(SocketIO *)socket disconnectedWithError:(NSError *)error
{
    NSLog(@"socket.io disconnected. did error occur? %@", error);
    NSLog(@"something error ");
//    if ([eSyncronyAppDelegate sharedInstance].strAccNo.length != 0) {
//        if (![socketIO isConnected]) {
//            [self reconnect];
//        }
//    }
}

//泡泡文本
- (UIView *)bubbleView:(NSString *)text from:(BOOL)fromSelf withPosition:(int)position{
    
    //计算大小
    UIFont *font = [UIFont systemFontOfSize:14];
//    CGSize size = [text sizeWithFont:font constrainedToSize:CGSizeMake(180.0f, 20000.0f) lineBreakMode:NSLineBreakByWordWrapping];
    CGSize size = [text boundingRectWithSize:CGSizeMake(180.0f, 20000.0f) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:font} context:nil].size;

    // build single chat bubble cell with given text
    UIView *returnView = [[UIView alloc] initWithFrame:CGRectZero];
    returnView.backgroundColor = [UIColor clearColor];
    
    //背影图片
    UIImage *bubble = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:fromSelf?@"SenderAppNodeBkg_HL":@"ReceiverTextNodeBkg" ofType:@"png"]];
    
    UIImageView *bubbleImageView = [[UIImageView alloc] initWithImage:[bubble stretchableImageWithLeftCapWidth:floorf(bubble.size.width/2) topCapHeight:floorf(bubble.size.height/2)]];
//    NSLog(@"bubbleImageView  size.width =%f,size.height =%f",size.width,size.height);

    
    //添加文本信息
    UILabel *bubbleText = [[UILabel alloc] initWithFrame:CGRectMake(fromSelf?15.0f:22.0f, 20.0f, size.width+10, size.height+10)];
    bubbleText.backgroundColor = [UIColor clearColor];
    bubbleText.font = font;
    bubbleText.numberOfLines = 0;
    bubbleText.lineBreakMode = NSLineBreakByWordWrapping;
    bubbleText.text = text;
    
    bubbleImageView.frame = CGRectMake(0.0f, 14.0f, bubbleText.frame.size.width+30.0f, bubbleText.frame.size.height+20.0f);
    
    if(fromSelf)
        returnView.frame = CGRectMake(320-position-(bubbleText.frame.size.width+30.0f), 0.0f, bubbleText.frame.size.width+30.0f, bubbleText.frame.size.height+30.0f);
    else
        returnView.frame = CGRectMake(position, 0.0f, bubbleText.frame.size.width+30.0f, bubbleText.frame.size.height+30.0f);
    
//    sizeHeight = sizeHeight + returnView.frame.size.height;
//    NSLog(@"sizeHeight = %f",sizeHeight);
    [returnView addSubview:bubbleImageView];
    [returnView addSubview:bubbleText];
    
    return returnView;
}
//泡泡语音
- (UIView *)yuyinView:(NSInteger)logntime from:(BOOL)fromSelf withIndexRow:(NSInteger)indexRow  withPosition:(int)position{
    
    //根据语音长度
    int yuyinwidth = 66+fromSelf;
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.tag = indexRow;
    if(fromSelf)
        button.frame =CGRectMake(320-position-yuyinwidth, 10, yuyinwidth, 54);
    else
        button.frame =CGRectMake(position, 10, yuyinwidth, 54);
    
    //image偏移量
    UIEdgeInsets imageInsert;
    imageInsert.top = -10;
    imageInsert.left = fromSelf?button.frame.size.width/3:-button.frame.size.width/3;
    button.imageEdgeInsets = imageInsert;
    
    [button setImage:[UIImage imageNamed:fromSelf?@"SenderVoiceNodePlaying":@"ReceiverVoiceNodePlaying"] forState:UIControlStateNormal];
    UIImage *backgroundImage = [UIImage imageNamed:fromSelf?@"SenderVoiceNodeDownloading":@"ReceiverVoiceNodeDownloading"];
    backgroundImage = [backgroundImage stretchableImageWithLeftCapWidth:20 topCapHeight:0];
    [button setBackgroundImage:backgroundImage forState:UIControlStateNormal];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(fromSelf?-30:button.frame.size.width, 0, 30, button.frame.size.height)];
    label.text = [NSString stringWithFormat:@"%ld''",(long)logntime];
    label.textColor = [UIColor grayColor];
    label.font = [UIFont systemFontOfSize:13];
    label.textAlignment = NSTextAlignmentCenter;
    label.backgroundColor = [UIColor clearColor];
    [button addSubview:label];
    
    return button;
}

#pragma mark 表格的数据源
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict = [chatMsgs objectAtIndex:indexPath.row];
    
    if ([[chatMsgs objectAtIndex:indexPath.row] isKindOfClass:[NSArray class]]) {
        dict = [[chatMsgs objectAtIndex:indexPath.row] objectAtIndex:0];
        if ([[[chatMsgs objectAtIndex:indexPath.row] objectAtIndex:0] isKindOfClass:[NSArray class]]) {
            dict =[[[chatMsgs objectAtIndex:indexPath.row] objectAtIndex:0] objectAtIndex:1];
        }
    }
    
    UIFont *font = [UIFont systemFontOfSize:14];
//    CGSize size = [[dict objectForKey:@"message"] sizeWithFont:font constrainedToSize:CGSizeMake(180.0f, 20000.0f) lineBreakMode:NSLineBreakByWordWrapping];
    
    CGRect textRect = [[dict objectForKey:@"message"] boundingRectWithSize:CGSizeMake(180.0f, 20000.0f)
                                             options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading
                                          attributes:@{NSFontAttributeName:font}
                                             context:nil];
    return textRect.size.height+44;

//    return size.height+44; 
    
}
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
//    
//    return 2;
//}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return chatMsgs.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }else{
        for (UIView *cellView in cell.subviews){
            [cellView removeFromSuperview];
        }
    }
    
    NSDictionary *dict = [chatMsgs objectAtIndex:indexPath.row];
    
    
    if ([[chatMsgs objectAtIndex:indexPath.row] isKindOfClass:[NSArray class]]) {
        dict = [[chatMsgs objectAtIndex:indexPath.row] objectAtIndex:0];
        if ([[[chatMsgs objectAtIndex:indexPath.row] objectAtIndex:0] isKindOfClass:[NSArray class]]) {
            dict =[[[chatMsgs objectAtIndex:indexPath.row] objectAtIndex:0] objectAtIndex:0];
        }
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    //创建头像
    UIImageView *photo ;
    if ([[dict objectForKey:@"acc_no"]isEqualToString:[eSyncronyAppDelegate sharedInstance].strAccNo]) {
        photo = [[UIImageView alloc]initWithFrame:CGRectMake(320-60, 10, 50, 50)];
        [cell addSubview:photo];
        photo.image = [UIImage imageNamed:@"male1.png"];
        
        if ([[dict objectForKey:@"message"] isEqualToString:@"0"]) {
            [cell addSubview:[self yuyinView:1 from:YES withIndexRow:indexPath.row withPosition:65]];
            
            
        }else{
            [cell addSubview:[self bubbleView:[dict objectForKey:@"message"] from:YES withPosition:65]];
        }
        
    }else{
        photo = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 50, 50)];
        [cell addSubview:photo];
        photo.image = [UIImage imageNamed:@"icon.png"];
        
        if ([[dict objectForKey:@"message"] isEqualToString:@"0"]) {
            [cell addSubview:[self yuyinView:1 from:NO withIndexRow:indexPath.row withPosition:65]];
        }else{
            [cell addSubview:[self bubbleView:[dict objectForKey:@"message"] from:NO withPosition:65]];
        }
    }
    
    return cell;
    
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self.view endEditing:YES];
}

- (void)p_tapOnTableView:(UITapGestureRecognizer *)gesture
{
    [self.view endEditing:YES];
    [self restoreViewPosition];
}
- (void)restoreViewPosition
{
    CGRect  frame = self.tableView.frame;
    frame.origin.y = _fOrgViewFrameTop;
    self.tableView.frame = frame;

//    [UIView animateWithDuration:0.3 animations:^{
//        CGRect frame = buttomView.frame;
//        frame.origin.y = 0.0;
//        buttomView.frame = frame;
//    }];
    
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    NSString *thistext = messageTxt.text;    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:0];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"room"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strName forKey:@"username"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:thistext forKey:@"message"];
    [socketIO sendEvent:@"new message" withData:params];
    messageTxt.text=nil;
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{

    CGRect rect = [textField convertRect:textField.bounds toView:self.tableView];
    float   offset;
    
    offset = self.tableView.frame.size.height - (rect.size.height + 216+50);
    offset = offset - rect.origin.y;
    
    if( offset > 0 )
        return;
    
    rect = self.tableView.frame;
    rect.origin.y = offset;
    
    self.tableView.frame = rect;
    
    [self.view bringSubviewToFront:firstView];
    [self.view bringSubviewToFront:typingLab];

//    float   offset;
//    offset = buttomView.frame.size.height - (textField.frame.origin.y +textField.frame.size.height + 216+50);
//    
//    if( offset > 0 )
//        return;
//    
//    [UIView animateWithDuration:0.3 animations:^{
//        CGRect frame = buttomView.frame;
//        frame.origin.y = offset;
//        buttomView.frame = frame;
//     }];
//    

//    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:0];
//    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"room"];
//    [params setObject:[eSyncronyAppDelegate sharedInstance].strName forKey:@"username"];
//    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
//    [params setObject:@"typing..." forKey:@"message"];
//    [socketIO sendEvent:@"typing" withData:params];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)onClickBack:(id)sender {

    SocketIOCallback cb = ^(id argsData) {
        NSDictionary *response = argsData;
        // do something with response
        NSLog(@"ack arrived: %@", response);

        // test forced disconnect
        [socketIO disconnectForced];
    };
    [socketIO sendMessage:@"hello back!" withAcknowledge:cb];
    
    [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.navigationController popViewControllerAnimated:YES];
    [eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl.mainRightLogoBtn.hidden = NO;
    
}

-(int)convertToInt:(NSString*)strtemp {//判断注册时候的字节数
    
    int strlength = 0;
    char* p = (char*)[strtemp cStringUsingEncoding:NSUnicodeStringEncoding];
    for (int i=0; i<[strtemp lengthOfBytesUsingEncoding:NSUnicodeStringEncoding];i++) {
        if (*p) {
            p++;
            strlength++;
        }
        else {
            p++;
        }
    }
    return strlength;
    
}

- (IBAction)onClickSend:(id)sender {
    
    NSString *thistext = messageTxt.text;
//    NSLog(@"sendBubbleMessage:%@",thistext);

    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:0];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"room"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strName forKey:@"username"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:thistext forKey:@"message"];
    [socketIO sendEvent:@"new message" withData:params];
//    [chatMsgs addObject:params];
//    [self.tableView reloadData];
    messageTxt.text=nil;
}

//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
//{  //string就是此时输入的那个字符textField就是此时正在输入的那个输入框返回YES就是可以改变输入框的值NO相反
//    
//    if ([string isEqualToString:@"\n"])  //按会车可以改变
//    {
//        return YES;
//    }
//    
//    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string]; //得到输入框的内容
//    
//    if (self.myTextField == textField)  //判断是否时我们想要限定的那个输入框
//    {
//        if ([toBeString length] > 20) { //如果输入框内容大于20则弹出警告
//            textField.text = [toBeString substringToIndex:20];
//            UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:nil message:@"超过最大字数不能输入了" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] autorelease];
//            [alert show];
//            return NO;
//        }
//    }
//    return YES;
//}
////设置键盘类型
//self.textField.keyboardType = UIKeyboardTypeASCIICapable;
//define kAlphaNum @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
////判断是否是数字，不是的话就输入失败
//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
//{
//    NSCharacterSet *cs;
//    cs = [[NSCharacterSet characterSetWithCharactersInString:kAlphaNum] invertedSet];
//    
//    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""]; //按cs分离出数组,数组按@""分离出字符串
//    
//    BOOL canChange = [string isEqualToString:filtered];
//    
//    return self.textField.text.length>=5?NO: canChange;
//    
//}
- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
    [counslantName release];
    [_tableView release];
    [messageTxt release];
    [firstView release];
    [buttomView release];
    [typingLab release];
    [topView release];
    [headerImg release];
    [super dealloc];
}
@end

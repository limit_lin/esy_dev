//
//  CustomMessageChat.h
//  eSyncrony
//
//  Created by ESYNSZ-Limit on 16/3/9.
//  Copyright © 2016年 WonMH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "eSyncronyAppDelegate.h"
#import "InputBaseViewController.h"
#import "SocketIO.h"

@class eSyncronyAppDelegate;

@interface CustomMessageChat : UIViewController<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,SocketIODelegate>
{
    IBOutlet UITextField *messageTxt;

    IBOutlet UILabel *counslantName;
    SocketIO *socketIO;
    CGFloat sizeHeight;
    
    IBOutlet UIView *firstView;
    IBOutlet UIView *topView;
    IBOutlet UIView *buttomView;
    IBOutlet UILabel *typingLab;
    
}

@property (retain, nonatomic) IBOutlet UIImageView *headerImg;

@property (retain, nonatomic) IBOutlet UITableView *tableView;
@property (retain, strong)NSMutableArray *chatMsgs;


- (IBAction)onClickBack:(id)sender;
- (IBAction)onClickSend:(id)sender;

@end


#import "eSyncronyViewController.h"
#import "eSyncronyAppDelegate.h"
#import "ourSuccessStoriesViewController.h"
#import "MemberLoginViewController.h"
#import "CreateAccountMainViewController.h"
#import "BasicStepViewsController.h"
#import "LifeStyleStepsViewController.h"
#import "DisplayCompleteProgrsesViewController.h"
#import "MatchCriteriaStepsViewController.h"
#import "AppearanceStepsViewController.h"
#import "InterestStepsViewController.h"
#import "MyValueStepsViewController.h"
#import "RelationShipStepsViewController.h"
#import "CultureStepsViewController.h"
#import "OpennessStepsViewController.h"
#import "AgreeableStepsViewController.h"
#import "MoneyStepsViewController.h"
#import "AdmirationStepsViewController.h"
#import "PersonalityStepsViewController.h"
#import "MyStoryStepsViewController.h"
#import "VIPStoryStepViewController.h"//VIP additional story

#import "SMSVerifyViewController.h"
#import "NRICVerifyViewController.h"
#import "DocCertViewController.h"
#import "PhotoUploadViewController.h"
#import "IntimacyViewController.h"
#import "WelcomeViewController.h"
#import "Global.h"
#import "DSActivityView.h"
#import "mainMenuViewController.h"
#import "ReasonsTableViewController.h"
#import "ThreeStepsViewController.h"
#import "DVSlideViewController.h"
#import "HCYoutubeParser.h"
#import <MediaPlayer/MediaPlayer.h>


#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
//https://www.whatismyip.com/ about ip address
@interface eSyncronyViewController ()
{
    
    
    int CityCode;
//    int Countrycode;
    IBOutlet UILabel *pHowLabel;
    IBOutlet UILabel *pOurLabel;
    
    IBOutlet UILabel *pStep3Label;
    NSTimer *timer;
}

@end

@implementation eSyncronyViewController

+ (NSString *)externalIPAddress {
    // Check if we have an internet connection then try to get the External IP Address
//    
//    if (![self connectedViaWiFi] && ![self connectedVia3G]) {
//        // Not connected to anything, return nil
//        return nil;
//    }
//     https://www.whatismyip.com/ip-address-lookup/
    // Get the external IP Address based on dynsns.org
//
    NSError *error = nil;
//    http://www.dyndns.org/cgi-bin/check_ip.cgi
    NSString *theIpHtml = [NSString stringWithContentsOfURL:[NSURL URLWithString:@"http://checkip.dyndns.com/"]
                                                   encoding:NSUTF8StringEncoding
                                                      error:&error];
   
    if (!error) {
        NSUInteger  an_Integer;
        NSArray *ipItemsArray;
        NSString *externalIP = nil;
        NSScanner *theScanner;
        NSString *text = nil;
        
        theScanner = [NSScanner scannerWithString:theIpHtml];
        
        while ([theScanner isAtEnd] == NO) {
            
            // find start of tag
            [theScanner scanUpToString:@"<" intoString:NULL] ;
            
            // find end of tag
            [theScanner scanUpToString:@">" intoString:&text] ;
            
            // replace the found tag with a space
            //(you can filter multi-spaces out later if you wish)
            theIpHtml = [theIpHtml stringByReplacingOccurrencesOfString:
                         [ NSString stringWithFormat:@"%@>", text]
                                                             withString:@" "] ;
            ipItemsArray = [theIpHtml  componentsSeparatedByString:@" "];
            an_Integer = [ipItemsArray indexOfObject:@"Address:"];
            externalIP =[ipItemsArray objectAtIndex:++an_Integer];
        }
        // Check that you get something back
        if (externalIP == nil || externalIP.length <= 0) {
            // Error, no address found
            return nil;
        }
        // Return External IP
        return externalIP;
    } else {
        // Error, no address found
        return nil;
    }
}
-(void)p_btnHiddenOrAppear{
    
    switch (CityCode) {
        case 0:
        {
            btnFBFollow.hidden = YES;
            btnWHFollow.hidden = YES;
            btnYTBFollow.hidden = YES;
            //for test
//            btnFBFollow.hidden = NO;
//            btnWHFollow.hidden = NO;
//            btnYTBFollow.hidden = NO;
        }
            break;
        case 1://sg
        {
            btnFBFollow.hidden = NO;
            btnWHFollow.hidden = YES;
            btnYTBFollow.hidden = NO;
        }
            break;
        case 2://my
        {
            btnFBFollow.hidden = NO;
            btnWHFollow.hidden = YES;
            btnYTBFollow.hidden = NO;
        }
            break;
        case 3://hk
        {
            btnFBFollow.hidden = NO;
            btnWHFollow.hidden = NO;
            btnYTBFollow.hidden = NO;
        }
            break;
        case 8://id
        {
            btnFBFollow.hidden = NO;
            btnWHFollow.hidden = YES;
            btnYTBFollow.hidden = NO;
        }
            break;
        default:
        {
            btnFBFollow.hidden = YES;
            btnWHFollow.hidden = YES;
            btnYTBFollow.hidden = YES;
        }
            break;
    }

}
-(int)getCountryCode
{
    //US
    NSString *pCountryId =[[NSLocale currentLocale] objectForKey:NSLocaleCountryCode];
    if ([pCountryId hasPrefix:@"SG"]) {
        Countrycode = 1;
    }else if ([pCountryId hasPrefix:@"MY"]){
        Countrycode = 2;
    }else if ([pCountryId hasPrefix:@"HK"]){
        Countrycode = 3;
    }else if ([pCountryId hasPrefix:@"ID"]){
        Countrycode = 8;
    }else if ([pCountryId hasPrefix:@"CN"])//added test
    {
        Countrycode = 3;
    }else{
        Countrycode = 0;
    }
    return Countrycode;
}
-(void)p_CountryCode
{    
    NSString *ipAddress = [eSyncronyViewController externalIPAddress];
    NSLog(@"ipAddress = %@",ipAddress);
    if  (ipAddress == nil){
        ipAddress = @"";
        CityCode= [self getCountryCode];
    }else
    {
        NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
        [params setObject:ipAddress forKey:@"ip"];
        NSDictionary*    result = [UtilComm getCountryCode:params];
        if( result == nil )
        {
            ////[ErrorProc alertToCheckInternetStateTitle:@"Authentication Failure"];
            CityCode= [self getCountryCode];
            return;
        }
        id response = [result objectForKey:@"response"];
        if( response == nil )
        {
            //[ErrorProc alertMessage:@"Unknown Error" withTitle:@"Saving Failure"];
//            CityCode =0;
            CityCode= [self getCountryCode];
        }
        if ([response isKindOfClass:[NSString class]] &&[response isEqualToString:@"ERROR 0"]) {
            CityCode= [self getCountryCode];
        }else{//[response isKindOfClass:[NSString class]]&&
            if ([response hasPrefix:@"1"]) {
                CityCode = 1;
            }else if ([response hasPrefix:@"2"]){
                CityCode = 2;
            }else if ([response hasPrefix:@"3"]){
                CityCode = 3;
            }else if ([response hasPrefix:@"8"]){
                CityCode = 8;
            }else{
//                CityCode = 0;
                CityCode= [self getCountryCode];
            }
//            CityCode =(int)response;
//            CityCode = [response intValue];
            
        }
    }
//   NSArray *arr=[NSLocale ISOCountryCodes];
//    NSLog(@"%@",arr);
    NSLog(@"CityCode == %d****Countrycode = %d\n",CityCode,Countrycode);
    return;
}
-(void)p_EsyncViewlayout
{
    NSRange  colorRange[2];
    
    if([[eSyncronyAppDelegate sharedInstance].language hasPrefix:@"zh-Hant"]||[[eSyncronyAppDelegate sharedInstance].language hasPrefix:@"zh-HK"]){
        colorRange[0] = NSMakeRange(0, 2);
        colorRange[1] = NSMakeRange(0, 2);
    }
    else//en,id,th
    {
        colorRange[0]=[pOurLabel.text rangeOfString:@"Our"];
        colorRange[1]=[pHowLabel.text rangeOfString:@"How"];
    }
    NSMutableAttributedString *str = [[[NSMutableAttributedString alloc]initWithString:pHowLabel.text]autorelease];
    [str addAttribute:NSForegroundColorAttributeName value:RGBColor(00, 00, 00) range:colorRange[1]];
    pHowLabel.attributedText = str;
    
    NSMutableAttributedString* string = [[[NSMutableAttributedString alloc]initWithString:pOurLabel.text]autorelease];
    [string addAttribute:NSForegroundColorAttributeName value:RGBColor(00, 00, 00) range:colorRange[0]];
    pOurLabel.attributedText = string;

    return;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.screenName = @"eSynchronyLandingView";
    CityCode =0;
    Countrycode = 0;
    
    if ([[eSyncronyAppDelegate sharedInstance].language hasPrefix:@"id"]) {
        
        pHowLabel.font = [UIFont systemFontOfSize:27];
        pStep3Label.frame = CGRectMake(0, 422, 146, 26);
    }
#if 0
    [self p_CountryCode];
    [self p_btnHiddenOrAppear];
#endif
    
    [self p_EsyncViewlayout];
    
    [scrlView addSubview:mainView];
    scrlView.contentSize = mainView.frame.size;
    self.navigationController.navigationBarHidden = YES;
}

-(CABasicAnimation *)opacityForever_Animation:(float)time
{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"opacity"];//必须写opacity才行。
    animation.fromValue = [NSNumber numberWithFloat:1.0f];
    animation.toValue = [NSNumber numberWithFloat:0.0f];//这是透明度。
    animation.autoreverses = YES;
    animation.duration = time;
//    animation.repeatCount = MAXFLOAT;
    animation.repeatCount = 3;
    animation.removedOnCompletion = NO;
    animation.fillMode = kCAFillModeForwards;
    animation.timingFunction=[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];///没有的话是均匀的动画。
    return animation;
}
#pragma mark =====横向、纵向移动===========
-(CABasicAnimation *)moveX:(float)time X:(NSNumber *)x
{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.translation.x"];///.y的话就向下移动。
    animation.toValue = x;
    animation.duration = time;
//    animation.removedOnCompletion = NO;//yes的话，又返回原位置了。
    animation.removedOnCompletion = YES;//yes的话，又返回原位置了。
//    animation.repeatCount = MAXFLOAT;
    animation.repeatCount = 2;
    animation.fillMode = kCAFillModeForwards;
    return animation;
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
//    [registerAnimationLab.layer addAnimation:[self opacityForever_Animation:0.5] forKey:nil];
//     [registerAnimationLab.layer addAnimation:[self moveX:1.0f X:[NSNumber numberWithFloat:200.f]] forKey:nil];
#if 0
    [UIView setAnimationsEnabled:NO];
    
    CATransition *animation = [CATransition animation];
    
    animation.duration = 5.0f; //动画时长
    
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    
    animation.delegate = self;
    
    animation.type = kCATransitionFade; //过度效果
    animation.subtype=kCATransitionFromLeft;
    [self.view.layer addAnimation:animation forKey:@"animation"];
#endif
}
//-(BOOL)isExistenceNetwork
//{
//    BOOL isExistenceNetwork;
//    Reachability *r = [Reachability reachabilityWithHostName:@"http://www.apple.com"];
//    switch ([r currentReachabilityStatus]) {
//        case NotReachable:
//            isExistenceNetwork = FALSE;
//            break;
//         case ReachableViaWWAN:
//            isExistenceNetwork = TRUE;
//            break;
//        case ReachableViaWiFi:
//            isExistenceNetwork = TRUE;
//    }
//    return isExistenceNetwork;
//}

- (void)procLoginError:(NSString *)error
{
    if ([error isEqualToString:@"ERROR 1"]) {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:ERROR_Authentication_Failure message:NSLocalizedString(@"Email address or password is empty.",@"eSyncronyView") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",) otherButtonTitles:nil, nil];
        [av show];
        [av release];
        return;
    }
    else if ([error isEqualToString:@"ERROR 2"]) {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:ERROR_Authentication_Failure message:ERROR_Account_Not_Exist delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",) otherButtonTitles:nil, nil];
        [av show];
        [av release];
        return;
    }
    else if ([error isEqualToString:@"ERROR 3"]) {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:ERROR_Authentication_Failure message:NSLocalizedString(@"Invalid Password.",) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",) otherButtonTitles:nil, nil];
        [av show];
        [av release];
    }
    return;
}

- (void)processLoginResult:(NSDictionary*)result
{
    if( result == nil )
    {
        ////[ErrorProc alertToCheckInternetStateTitle:@"Authentication Failure"];
        [ErrorProc alertAuthenFailureMessage];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( response == nil )
    {
        //[ErrorProc alertMessage:@"Unknown Error" withTitle:@"Authentication Failure"];
        [ErrorProc alertAuthenFailureMessage];
        return;
    }
    
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [ErrorProc alertLoginError:response];
        return;
    }
    else
    {
        [eSyncronyAppDelegate sharedInstance].strAccNo = [response objectForKey:@"acc_no"];
        [eSyncronyAppDelegate sharedInstance].strTokenKey = [response objectForKey:@"token_key"];
        [eSyncronyAppDelegate sharedInstance].strName = [response objectForKey:@"nname"];
        [eSyncronyAppDelegate sharedInstance].membership = [response objectForKey:@"membership"];
        [eSyncronyAppDelegate sharedInstance].countryId = [[response objectForKey:@"cty_id"]intValue]-1;
        if( [[response objectForKey:@"gender"] isEqualToString:@"M"] ){
            [eSyncronyAppDelegate sharedInstance].gender = 0;
        }
        else{
            [eSyncronyAppDelegate sharedInstance].gender = 1;
        }
        [eSyncronyAppDelegate sharedInstance].bIsLoggedIn = YES;
        [[eSyncronyAppDelegate sharedInstance] saveLoginInfo];

        
//        [self checkUserStep];
        [self showBusyDialogWithTitle:NSLocalizedString(@"Loading...",)];
        [self performSelector:@selector(procCheckUserStep) withObject:nil afterDelay:0.1];
    }
}

- (void)loginProc
{
    [self hideBusyDialog];
    
//    NSLog(@"%@   %@",[eSyncronyAppDelegate sharedInstance].strLoginEmail,[eSyncronyAppDelegate sharedInstance].strLoginPassword);
    
    NSString*   email = [eSyncronyAppDelegate sharedInstance].strLoginEmail;
    NSString*   password = [eSyncronyAppDelegate sharedInstance].strLoginPassword;
//    NSLog(@"%@ %@",email,password);
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];

    [params setObject:email forKey:@"email"];
    [params setObject:password forKey:@"password"];
    
    NSDictionary*    result = [UtilComm loginWithParams:params];

    [self processLoginResult:result];
}

- (void)loginProcWithFB
{
    [self hideBusyDialog];

    NSString*   facebook_id = [eSyncronyAppDelegate sharedInstance].strFaceBookID;
    NSString*   email = [eSyncronyAppDelegate sharedInstance].strLoginEmail;
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    [params setObject:facebook_id forKey:@"facebook_id"];
    [params setObject:email forKey:@"email"];
    NSDictionary*    result = [UtilComm loginWithFaceBook:params];
    
    [self processLoginResult:result];
}

- (void)procCheckUserStepOK:(NSString *)strRetVal
{
    int step = [strRetVal intValue];
    switch ( step ) {
        case 3:
            [self enterCriteriaView];
            break;
        case 4:
            [self enterAppearanceView];
            break;
        case 5:
            [self enterInterestView];
            break;
        case 6:
            [self enterMyValueView];
            break;
        case 7:
            [self enterRelationStepsView];
            break;
        case 8:
            [self enterCultureStepsView];
            break;
        case 9:
            [self enterOpennessStepsView];
            break;
        case 10:
            [self enterAgreeableStepsView];
            break;
        case 11:
            [self enterMoneyStepsView];
            break;
        case 12:
            [self enterAdmirationStepsView];
            break;
        case 13:
            [self enterPersonalityStepsView];
            break;
        case 14:
            [self enterIntimacyStepsView];
            break;
        case 15:
        {
//            [self enterNricVerifyStep];
//            [self enterUploadPhotoStep];
            if ([[eSyncronyAppDelegate sharedInstance].isNoNric isEqualToString:@"true"]) {
                [self enterNricVerifyStep];
            }else if ([[eSyncronyAppDelegate sharedInstance].isNoPhoto isEqualToString:@"true"])
            {
//                [self enterUploadPhotoStep];
                [self showUploadPhotoStep];
            }else
                [self enterStoryStepsView];
        }
            break;
        default:
//            [self showUploadPhotoStep];
//            [self enter20ReasonsView];
        {
            if ([[eSyncronyAppDelegate sharedInstance].checkVipStroy isEqualToString:@"false"] && [[eSyncronyAppDelegate sharedInstance].isVip isEqualToString:@"true"]) {
                [self enterVIPStoryStepsView:YES];
            }else{
                [self enterDocCertStep];
            }
        }
            break;
    }
}

- (void)procCheckUserStep
{
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    NSDictionary*    result = [UtilComm retrieveStepNo:params];
    
    [self hideBusyDialog];
    
    if( result == nil )
    {
        [ErrorProc alertToCheckInternetStateTitle:ERROR_Authentication_Failure];

        return;
    }

    id response = [result objectForKey:@"response"];
    if( response == nil )
    {
        [ErrorProc alertToCheckInternetStateTitle:ERROR_Authentication_Failure];
        return;
    }
    
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
        [ErrorProc alertDefaultError:response withTitle:nil];
    }else if ([response isKindOfClass:[NSString class]] && [response isEqualToString:@"Enter quiz page"]){//新的注册方式
//        [self enterSMSVerifyStep];
         [self enterWelcomeView];
    }else if ([response isKindOfClass:[NSString class]] && [response isEqualToString:@"Enter verify page"]){
        [self enterVerifyStep];
    }
    else
    {
        [self procCheckUserStepOK:response];
    }
}

- (void)checkUserStep
{

    NSString    *strLoginEmail = [eSyncronyAppDelegate sharedInstance].strLoginEmail;
    NSString    *strLoginPassword = [eSyncronyAppDelegate sharedInstance].strLoginPassword;
    
    NSString    *strFaceBookID = [eSyncronyAppDelegate sharedInstance].strFaceBookID;
    

        if( strFaceBookID != nil && ![strFaceBookID isEqualToString:@""] )
        {
            [self showBusyDialogWithTitle:NSLocalizedString(@"Authenticating...",)];
            [self performSelector:@selector(loginProcWithFB) withObject:nil afterDelay:0.1];
        }
        else if( strLoginEmail != nil && ![strLoginEmail isEqualToString:@""] &&
            strLoginPassword != nil && ![strLoginPassword isEqualToString:@""] )
        {
            [self showBusyDialogWithTitle:NSLocalizedString(@"Authenticating...",)];
            [self performSelector:@selector(loginProc) withObject:nil afterDelay:0.1];
        }

}

- (void)initSetting
{
#ifdef OFFLINE_TEST_MODE
    //[self showDocVerifyStep:nil];
    [self enterMainWindow];
#else
    //[self enterStoryStepsView];
    [self checkUserStep];
#endif
}
- (void)enterWelcomeView{//和sms对调
    
    WelcomeViewController *welcomeView = [[[WelcomeViewController alloc] initWithNibName:@"WelcomeViewController" bundle:nil] autorelease];
    welcomeView .parentView = self;
    [self.navigationController pushViewController:welcomeView  animated:YES];

}
- (void)enterBasicStepView:(int)st
{
    if( !basicStepView )
        basicStepView = [[BasicStepViewsController alloc] initWithNibName:@"BasicStepViewsController" bundle:nil];

    basicStepView.nStep = 2;
    basicStepView.parentView = self;

    [self.navigationController pushViewController:basicStepView animated:YES];
}

- (void)enterBasicStepWithLifeStyle:(int)step Values:(NSMutableDictionary *)values;
{
    if( !basicStepView )
        basicStepView = [[BasicStepViewsController alloc] initWithNibName:@"BasicStepViewsController" bundle:nil];
    
    basicStepView.nStep = 2;
    basicStepView.parentView = self;
    //NSLog(@"%@",values);
    basicStepView.dicLifeStyleData = [[NSMutableDictionary alloc] init];
    basicStepView.dicLifeStyleData = [values retain];
    
    [self.navigationController pushViewController:basicStepView animated:YES];
}

- (void)enter20ReasonsView{
    ReasonsTableViewController *reasonsView = [[[ReasonsTableViewController alloc] initWithNibName:@"ReasonsTableViewController" bundle:nil] autorelease];
    
    reasonsView.parentView = self;
    [self.navigationController pushViewController:reasonsView  animated:YES];
}

- (void)enterLifeStyleView
{
    if( basicView )
    {
        [basicView release];
        basicView = nil;
    }
    
    if( basicStepView )
    {
        [basicStepView release];
        basicStepView = nil;
    }

    LifeStyleStepsViewController *lifeStyleView = [[[LifeStyleStepsViewController alloc] initWithNibName:@"LifeStyleStepsViewController" bundle:nil] autorelease];
    
    lifeStyleView.step = 0;
    lifeStyleView.parentView = self;

    [self.navigationController pushViewController:lifeStyleView animated:YES];
}

- (void)enterCriteriaView
{
    MatchCriteriaStepsViewController *criteriaStepView = [[[MatchCriteriaStepsViewController alloc] initWithNibName:@"MatchCriteriaStepsViewController" bundle:nil] autorelease];
    
    criteriaStepView.step = 1;
    criteriaStepView.parentView = self;
    
    [self.navigationController pushViewController:criteriaStepView animated:YES];
}

- (void)enterAppearanceView
{
    AppearanceStepsViewController *appearanceStepView = [[[AppearanceStepsViewController alloc] initWithNibName:@"AppearanceStepsViewController" bundle:nil] autorelease];
    
    appearanceStepView.parentView = self;
    appearanceStepView.step = 1;
    
    [self.navigationController pushViewController:appearanceStepView animated:YES];
}

- (void)enterInterestView
{
    InterestStepsViewController *interestStepView = [[[InterestStepsViewController alloc] initWithNibName:@"InterestStepsViewController" bundle:nil] autorelease];

    interestStepView.parentView = self;
    interestStepView.step = 1;
    
    [self.navigationController pushViewController:interestStepView animated:YES];
}

- (void)enterMyValueView
{
    MyValueStepsViewController *valueStepsView = [[[MyValueStepsViewController alloc] initWithNibName:@"MyValueStepsViewController" bundle:nil] autorelease];

    valueStepsView.parentView = self;
    valueStepsView.step = 1;

    [self.navigationController pushViewController:valueStepsView animated:YES];
}

- (void)enterRelationStepsView
{
    RelationShipStepsViewController *relationStepsView = [[[RelationShipStepsViewController alloc] initWithNibName:@"RelationShipStepsViewController" bundle:nil] autorelease];

    relationStepsView.parentView = self;
    relationStepsView.step = 1;
    
    [self.navigationController pushViewController:relationStepsView animated:YES];
}

- (void)enterCultureStepsView
{
    CultureStepsViewController *cultureStepsView = [[[CultureStepsViewController alloc] initWithNibName:@"CultureStepsViewController" bundle:nil] autorelease];
    
    cultureStepsView.parentView = self;
    cultureStepsView.step = 1;
    
    [self.navigationController pushViewController:cultureStepsView animated:YES];
}

- (void)enterOpennessStepsView
{
    OpennessStepsViewController *opennessStepsView = [[[OpennessStepsViewController alloc] initWithNibName:@"OpennessStepsViewController" bundle:nil] autorelease];
    
    opennessStepsView.parentView = self;
    opennessStepsView.step = 1;
    
    [self.navigationController pushViewController:opennessStepsView animated:YES];
    
}

- (void)enterAgreeableStepsView
{
    AgreeableStepsViewController *agreeableStepsView = [[[AgreeableStepsViewController alloc] initWithNibName:@"AgreeableStepsViewController" bundle:nil] autorelease];
    
    agreeableStepsView.parentView = self;
    agreeableStepsView.step = 1;
    
    [self.navigationController pushViewController:agreeableStepsView animated:YES];
}

- (void)enterMoneyStepsView
{
    MoneyStepsViewController *moneyStepsView = [[[MoneyStepsViewController alloc] initWithNibName:@"MoneyStepsViewController" bundle:nil] autorelease];

    moneyStepsView.parentView = self;
    moneyStepsView.step = 1;

    [self.navigationController pushViewController:moneyStepsView animated:YES];
}

- (void)enterAdmirationStepsView
{
    AdmirationStepsViewController *admirationStepsView = [[[AdmirationStepsViewController alloc] initWithNibName:@"AdmirationStepsViewController" bundle:nil] autorelease];
    
    admirationStepsView.parentView = self;
    admirationStepsView.step = 1;
    
    [self.navigationController pushViewController:admirationStepsView animated:YES];
}
- (void)enterThreeStepsView{

    ThreeStepsViewController *page1 = [[[ThreeStepsViewController alloc] initWithNibName:@"ThreeStepsViewController" bundle:nil] autorelease];
    page1.pagenum = 0;
    
    ThreeStepsViewController *page2 = [[[ThreeStepsViewController alloc] initWithNibName:@"ThreeStepsViewController" bundle:nil] autorelease];
    page2.pagenum = 1;
    
    ThreeStepsViewController *page3 = [[[ThreeStepsViewController alloc] initWithNibName:@"ThreeStepsViewController" bundle:nil] autorelease];
    page3.pagenum = 2;
    
    
    DVSlideViewController *slideView = [[[DVSlideViewController alloc] init]autorelease];
    slideView.viewControllers = [NSMutableArray arrayWithObjects:page1,page2,page3, nil];
    slideView.parentView = self;
    page1.slide = slideView;
    page2.slide = slideView;
    page3.slide = slideView;
    
    [self.navigationController pushViewController:slideView animated:YES];
}

- (void)enterPersonalityStepsView
{
    PersonalityStepsViewController *personalityStepsView = [[[PersonalityStepsViewController alloc] initWithNibName:@"PersonalityStepsViewController" bundle:nil] autorelease];
    
    personalityStepsView.parentView = self;
    personalityStepsView.round = 0;
    personalityStepsView.step = 1;
    
    [self.navigationController pushViewController:personalityStepsView animated:YES];
    
}
- (void)enterIntimacyStepsView
{
    IntimacyViewController *intimacyStepsView = [[[IntimacyViewController alloc] initWithNibName:@"IntimacyViewController" bundle:nil] autorelease];
    intimacyStepsView.parentView = self;
    intimacyStepsView.step = 1;
    
    [self.navigationController pushViewController:intimacyStepsView animated:YES];
    
}
- (void)enterStoryStepsView
{
    MyStoryStepsViewController *mystoryStepsView = [[[MyStoryStepsViewController alloc] initWithNibName:@"MyStoryStepsViewController" bundle:nil] autorelease];
    
    mystoryStepsView.parentView = self;
    mystoryStepsView.step = 1;

    [self.navigationController pushViewController:mystoryStepsView animated:YES];
}

- (void)enterVIPStoryStepsView:(BOOL)isMemberLogin
{
    VIPStoryStepViewController *myVIPstoryStepsView = [[[VIPStoryStepViewController alloc] initWithNibName:@"VIPStoryStepViewController" bundle:nil] autorelease];
    
    myVIPstoryStepsView.parentView = self;
    myVIPstoryStepsView.step = 1;
    myVIPstoryStepsView.ismemberLogin = isMemberLogin;
    
    [self.navigationController pushViewController:myVIPstoryStepsView animated:YES];
}

- (void)enterSMSVerifyStep
{
   SMSVerifyViewController *smsVerifyStepView = [[[SMSVerifyViewController alloc] initWithNibName:@"SMSVerifyViewController" bundle:nil] autorelease];

    smsVerifyStepView.parentView = self;
    smsVerifyStepView.step = 1;

    [self.navigationController pushViewController:smsVerifyStepView animated:YES];
}

- (void)enterNricVerifyStep
{
    NRICVerifyViewController *nricVerifyStepView = [[[NRICVerifyViewController alloc] initWithNibName:@"NRICVerifyViewController" bundle:nil] autorelease];

    nricVerifyStepView.parentView = self;
//    nricVerifyStepView.step = 2;
    [self.navigationController pushViewController:nricVerifyStepView animated:YES];
}

- (void)enterNricVerifyStepWithValues:(NSMutableDictionary *)values
{
    NRICVerifyViewController *nricVerifyStepView = [[[NRICVerifyViewController alloc] initWithNibName:@"NRICVerifyViewController" bundle:nil] autorelease];
    
    nricVerifyStepView.parentView = self;
//    nricVerifyStepView.step = 1;
    nricVerifyStepView.dicBaseInfo = [values retain];
    
    [self.navigationController pushViewController:nricVerifyStepView animated:YES];
}

- (void)enterVerifyStep
{
    NSDictionary*   result = [UtilComm checkAccountVerify];
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Authentication Failure"];
        return;
    }

    id response = [result objectForKey:@"response"];
    if( response == nil )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:ERROR_Authentication_Failure];
        return;
    }
//    dispatch_async(dispatch_get_main_queue(), ^{//151126 改为异步操作
       [Global loadBasicProfileInfoAndSaveToClient];
//    });

    if( [response isEqualToString:@"true"] )
    {
        [self enterCriteriaView];
       // [self enter20ReasonsView];
//        [self enterDocCertStep];
    }
    // SMS verify step
    else if( [response isEqualToString:@"Mobile not verified"]||[response isEqualToString:@"Mobile exists but not verified"])
    {
        //[Global loadBasicProfileInfoAndSaveToClient];
        [self enterSMSVerifyStep];
    }
    else if( [response isEqualToString:@"NRIC not verified"] )
    {
        //[Global loadBasicProfileInfoAndSaveToClient];
        [self enterNricVerifyStep];
    }else{
        return;
    }
}

- (void)showDocVerifyStep:(NSDictionary*)docCertInfo
{
    //[Global loadBasicProfileInfoAndSaveToClient];

    DocCertViewController* docCertView = [[[DocCertViewController alloc] initWithNibName:@"DocCertViewController" bundle:nil] autorelease];
    
    docCertView.parentView = self;
    docCertView.docCertInfo = docCertInfo;

    [self.navigationController pushViewController:docCertView animated:YES];
}

- (void)enterDocCertStep
{
    NSDictionary* result = [UtilComm getDocCertVerifyStatus];
    NSDictionary* response = [result objectForKey:@"response"];
    if( result == nil || response == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Authentication Failure"];
        return;
    }
    
    NSString*   eduVerify = [response objectForKey:@"EDU_VERIFY"];
    NSString*   idVerify = [response objectForKey:@"ID_VERIFY"];
    NSString*   incomeVerify = [response objectForKey:@"INCOME_VERIFY"];
    NSString*   workVerify = [response objectForKey:@"WORK_VERIFY"];
    
    if( [idVerify isEqualToString:@"0"] )
        [eSyncronyAppDelegate sharedInstance].nIDVerify = 0;
    else
        [eSyncronyAppDelegate sharedInstance].nIDVerify = 1;
    [[eSyncronyAppDelegate sharedInstance] saveLoginInfo];
    
    // if there is exist item which not verified
    if( [eduVerify isEqualToString:@"0"] || [idVerify isEqualToString:@"0"] ||
       [incomeVerify isEqualToString:@"0"] || [workVerify isEqualToString:@"0"] )
    {
        [self showDocVerifyStep:response];
        return;
    }
    
    [self enterMainWindow];
//    [self enterUploadPhotoStep];
}

- (void)showUploadPhotoStep
{
    //[Global loadBasicProfileInfoAndSaveToClient];
    
    PhotoUploadViewController* photoUploadView = [[[PhotoUploadViewController alloc] initWithNibName:@"PhotoUploadViewController" bundle:nil] autorelease];

    photoUploadView.parentView = self;

    [self.navigationController pushViewController:photoUploadView animated:YES];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [DSBezelActivityView removeViewAnimated:NO];
#pragma modify 0310 need studied
//    if( buttonIndex == 0 )
//    {
//        [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Logging out...",)];
//        [self performSelector:@selector(procLogout) withObject:nil afterDelay:0.1];
//    }else
//    {
//        return;
//    }
}
- (void)procLogout
{
//    NSString *_strUrl = [NSString stringWithFormat:@"%@logout?acc_no=%@&token_key=%@",WEBSERVICE_BASEURI, [eSyncronyAppDelegate sharedInstance].strAccNo, [eSyncronyAppDelegate sharedInstance].strTokenKey];
//    
//    _strUrl = [_strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [[eSyncronyAppDelegate sharedInstance] logOut];
    [DSBezelActivityView removeViewAnimated:NO];
    
    [self.navigationController popToRootViewControllerAnimated:YES];

    //[[eSyncronyAppDelegate sharedInstance] logOut];
}
- (void)enterUploadPhotoStep
{
    NSDictionary*   result = [UtilComm retrievePrimaryPhotoFilename];
    NSString*       response = [result objectForKey:@"response"];
    if( result == nil || response == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Authentication Failure"];
        return;
    }

    if( [response isEqualToString:@"ERROR 0"] ||
       [response isEqualToString:@"ERROR 1"] ||
       [response isEqualToString:@"ERROR 2"] ||
       [response isEqualToString:@"ERROR 3"] )
    {
        [ErrorProc alertDefaultError:response withTitle:NSLocalizedString(@"Error",)];
        return;
    }
    else if( [response isEqualToString:@"no photo uploaded yet"] )
    {
        [self showUploadPhotoStep];
        return;
    }
    
//    [self enterMainWindow];//start quiz 0113
    [self enterStoryStepsView];
}

- (void)enterMainWindow
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"userPhotoName"];
    
    mainMenuViewController *mainMenuView = [[[mainMenuViewController alloc] initWithNibName:@"mainMenuViewController" bundle:nil] autorelease];
    mainMenuView.parentView = self;
    [self.navigationController pushViewController:mainMenuView animated:YES];
    
}

- (void)enterDisplayComplete:(int)step
{
    DisplayCompleteProgrsesViewController *completePrgView = [[[DisplayCompleteProgrsesViewController alloc] initWithNibName:@"DisplayCompleteProgrsesViewController" bundle:nil] autorelease];

    completePrgView.parentView = self;
    completePrgView.step = step;
    
    [self.navigationController pushViewController:completePrgView animated:YES];
}

#pragma followUs
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [DSBezelActivityView removeViewAnimated:NO];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [DSBezelActivityView removeViewAnimated:NO];
    UIAlertView* av = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"Loading failed!",) delegate:nil cancelButtonTitle:NSLocalizedString(@"Close",) otherButtonTitles:nil, nil];
    [av show];
    [av release];
    [webView removeFromSuperview];//append 0826
}
- (IBAction)didClickFbFollowus:(id)sender {

//    NSLog(@"getCountryCode == %d",[eSyncronyAppDelegate sharedInstance].CountryCode);
    
    //CGSize size = scrlView.contentSize;
    //UIWebView *web = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
//   CityCode =[ eSyncronyAppDelegate sharedInstance].CountryCode;
    
    UIWebView *web = [[UIWebView alloc] initWithFrame:scrlView.frame];
    web.delegate = self;
    web.scalesPageToFit = YES;
    //[scrlView addSubview:web];
    [self.view addSubview:web];

    
    NSString *_strUrl = @" https://www.facebook.com/eSynchronySG?fref=ts";

    switch (CityCode) {
        case 1://sg
        {
            _strUrl = @" https://www.facebook.com/eSynchronySG?fref=ts";//SG
        }
            break;
        case 2://my
        {
            _strUrl = @" https://www.facebook.com/esynchronyMY";//MY
        }
            break;
        case 3://hk
        {
            _strUrl = @" https://www.facebook.com/eSynchronyHK?fref=ts";//hk
        }
            break;
        case 8://id
        {
            _strUrl = @"  https://www.facebook.com/pages/ESynchrony-Indonesia/1655590734674198";//JKT
        }
            break;
        default:
        {
            //        _strUrl = nil;//for test
            _strUrl = @" https://www.facebook.com/eSynchronySG?fref=ts";//SG
        }
            break;
    }
    
    NSURL       *_url = [NSURL URLWithString:_strUrl];
    
    NSURLRequest* _urlRequest = [NSURLRequest requestWithURL:_url];
    [web loadRequest:_urlRequest];
    
    [DSBezelActivityView newActivityViewForView:web withLabel:NSLocalizedString(@"Loading...",)];

}
- (IBAction)didClickYoutubeFollowus:(id)sender {
    
//    NSLog(@"getCountryCode == %d",[eSyncronyAppDelegate sharedInstance].CountryCode);
//    CityCode =[ eSyncronyAppDelegate sharedInstance].CountryCode;
    
    //CGSize size = scrlView.contentSize;
    //UIWebView *web = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    
    UIWebView *web = [[UIWebView alloc] initWithFrame:scrlView.frame];
    web.delegate = self;
    web.scalesPageToFit = YES;
    //[scrlView addSubview:web];
    [self.view addSubview:web];
    
    NSString *_strUrl = @" https://www.youtube.com/user/eteract";//all is this url
    
    NSURL       *_url = [NSURL URLWithString:_strUrl];
    
    NSURLRequest* _urlRequest = [NSURLRequest requestWithURL:_url];
    [web loadRequest:_urlRequest];
    
    [DSBezelActivityView newActivityViewForView:web withLabel:NSLocalizedString(@"Loading...",)];

}
- (IBAction)didClickWhatsappFollowus:(id)sender {//only hk
    
//    NSLog(@"getCountryCode == %d",[eSyncronyAppDelegate sharedInstance].CountryCode);
//    CityCode =[ eSyncronyAppDelegate sharedInstance].CountryCode;
    
    //CGSize size = scrlView.contentSize;
    //UIWebView *web = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];

    UIWebView *web = [[UIWebView alloc] initWithFrame:scrlView.frame];
    web.delegate = self;
    web.scalesPageToFit = YES;
    //[scrlView addSubview:web];
    [self.view addSubview:web];
    
//    if (CityCode == 3) {
      NSString *_strUrl = @" https://www.facebook.com/eSynchronyHK?fref=ts";//hk151023
//    }
 
    NSURL       *_url = [NSURL URLWithString:_strUrl];
    
    NSURLRequest* _urlRequest = [NSURLRequest requestWithURL:_url];
    [web loadRequest:_urlRequest];
    
    [DSBezelActivityView newActivityViewForView:web withLabel:NSLocalizedString(@"Loading...",)];
    
}

- (void)timeEnough:(UIButton *)sender{
    
//    NSLog(@"sender.tag = %ld",(long)sender.tag);
//    UIButton *btn=(UIButton*)[self.view viewWithTag:sender.tag];
    sender.selected=NO;
    [timer invalidate];
//    NSLog(@"timer == %@",timer);
    timer=nil;
}

- (IBAction)didClickCreateAnAccount:(UIButton *)sender
{
    if (sender.selected) {
        return;
    }
   // sender.tag = 1501;
    sender.selected = YES;
    [self performSelector:@selector(timeEnough:) withObject:sender afterDelay:0.5]; //0.5秒后又可以处理点击事件了
    
    CreateAccountMainViewController *createAccountView = [[[CreateAccountMainViewController alloc] initWithNibName:@"CreateAccountMainViewController" bundle:nil] autorelease];
    
//    for (UIView *view in createAccountView.superclass) {
//        
//        [view removeFromSuperview];
//        
//    }
    createAccountView.nStep = 1;
    createAccountView.parentView = self;

    [self.navigationController pushViewController:createAccountView animated:YES];
}

- (IBAction)didClickMemberLogin:(UIButton *)sender
{
    if (sender.selected) {
    return;
    }
    //sender.tag = 1502;
    sender.selected = YES;
    [self performSelector:@selector(timeEnough:) withObject:sender afterDelay:0.5]; //0.5秒后又可以处理点击事件了

    MemberLoginViewController   *memberLoginView = [[[MemberLoginViewController alloc] initWithNibName:@"MemberLoginViewController" bundle:nil] autorelease];
    
    [self.navigationController pushViewController:memberLoginView animated:YES];
}

- (IBAction)didClickPullup:(id)sender {
//    scrlView.contentSize = CGSizeMake(320, 562);
    
//    [scrlView setContentInset:UIEdgeInsetsMake(-562, 0, 0, 0)];

}

- (IBAction)didClickOurSuccessStories:(id)sender
{
    ourSuccessStoriesViewController *ourSucsStrView = [[[ourSuccessStoriesViewController alloc] initWithNibName:@"ourSuccessStoriesViewController" bundle:nil] autorelease];
    ourSucsStrView.isTable = YES;
    [self.navigationController pushViewController:ourSucsStrView animated:YES];
}

- (IBAction)didPlayVedio:(id)sender {
    
    NSString *strUrl = @"http://www.youtube.com/watch?v=pM7ZO7XBoTE";
//    NSString *strUrl = @"http://www.youtube.com/v/pM7ZO7XBoTE";
    NSURL*youtubeURL=[NSURL URLWithString:strUrl];

//    if(![[UIApplication sharedApplication]openURL:[NSURL URLWithString:strUrl]] ){
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Can't open YouTube" delegate:nil cancelButtonTitle:@"确认" otherButtonTitles: nil] ;
//        [alert show] ;
//    }
//    return;

    [self showBusyDialogWithTitle:NSLocalizedString(@"Loading...",)];
        [HCYoutubeParser h264videosWithYoutubeURL:youtubeURL completeBlock:^(NSDictionary *videoDictionary, NSError *error) {
            
            NSDictionary *qualities = videoDictionary;

            NSString *URLString = nil;
            if ([qualities objectForKey:@"small"] != nil) {
                URLString = [qualities objectForKey:@"small"];
            }
            else if ([qualities objectForKey:@"medium"] != nil) {
                URLString = [qualities objectForKey:@"medium"];
            }
            else if ([qualities objectForKey:@"hd720"] != nil) {
                URLString = [qualities objectForKey:@"hd720"];
            }
            else if ([qualities objectForKey:@"live"] != nil) {
                URLString = [qualities objectForKey:@"live"];
            }
            
            videoPathURL = [NSURL URLWithString:URLString];
            [self hideBusyDialog];
            MPMoviePlayerViewController *moviePlayer =[[MPMoviePlayerViewController alloc] initWithContentURL:videoPathURL];
            
            [moviePlayer.moviePlayer prepareToPlay];
            moviePlayer.moviePlayer.shouldAutoplay=YES;
            [self presentMoviePlayerViewControllerAnimated:moviePlayer]; // 这里是presentMoviePlayerViewControllerAnimated
            
            [moviePlayer.moviePlayer setControlStyle:MPMovieControlStyleFullscreen];
            
            [moviePlayer.view setBackgroundColor:[UIColor clearColor]];
            
            [moviePlayer.view setFrame:self.view.bounds];
            
            [[NSNotificationCenter defaultCenter] addObserver:self
             
                                                     selector:@selector(movieFinishedCallback:)
             
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
             
                                                       object:moviePlayer.moviePlayer];
            
            [moviePlayer release];
            
        }];


//    videoPathURL = [NSURL URLWithString:@"http://r1---sn-a5m7lne7.googlevideo.com/videoplayback?itag=36&sver=3&key=yt5&expire=1411655098&upn=OE-bPDdO9Eg&signature=4300EA35B1AC955A7A620B87D6A15CBFD57737C2.2D3F6D79C591ED64EEF61AD96F8EE45FFA866ED6&mt=1411633422&initcwndbps=12923750&ip=67.229.56.106&ms=au&mv=m&source=youtube&id=o-AMPEmxDb6bAyN21pHjtcMa4Y0G1VIzcxtuxF0f0msJox&mm=31&ipbits=0&fexp=900234,914082,916633,927622,930666,931983,932404,934030,936118,946012,947209,952302,953800,953801&sparams=id,initcwndbps,ip,ipbits,itag,mm,ms,mv,source,upn,expire&signature=36"];

    
}

-(void)movieFinishedCallback:(NSNotification*)notify {
    NSLog(@"movieFinishedCallback");

    MPMoviePlayerController* theMovie = [notify object];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
     
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
     
                                                  object:theMovie];
    
    [self dismissMoviePlayerViewControllerAnimated];
    
}
- (void)dealloc
{
    [scrlView release];
    [mainView release];
    
    [basicView release];
    [basicStepView release];
    
    [btnFBFollow release];
    [btnYTBFollow release];
    [btnWHFollow release];
    [mianBgImageView release];
    [pHowLabel release];
    [pOurLabel release];
    [pStep3Label release];
    [registerAnimationLab release];
    [super dealloc];
}

@end

#import <Parse/Parse.h>
#import "eSyncronyAppDelegate.h"

#import "eSyncronyViewController.h"
#import "MemberLoginViewController.h"
#import "mainMenuViewController.h"
#import "Global.h"

#import "DetailMathProfileViewController.h"
#import "BaseNavigationController.h"

#import "ACTReporter.h"

#import "PayPalMobile.h"
#import "CardIO.h"

//ShareSDK
#import <ShareSDK/ShareSDK.h>
//WeChatSDK
#import "WXApi.h"
#import <WhatsAppConnection/WhatsAppConnection.h>
#import <SMSConnection/SMSConnection.h>

#import "SocketIOPacket.h"

static NSString *const kAllowTracking = @"allowTracking";

eSyncronyAppDelegate *kAppDelegate;

@implementation eSyncronyAppDelegate

@synthesize mainMenuViewCtrl;

@synthesize strLoginEmail;
@synthesize strLoginPassword;
@synthesize strFaceBookID;
@synthesize strAccNo;
@synthesize strTokenKey;
@synthesize strHandphone;
@synthesize isNoNric;
@synthesize isNoPhoto;
@synthesize strName;
@synthesize strNric;
@synthesize bIsLoggedIn;
@synthesize gender;
@synthesize countryId;
@synthesize nIDVerify;
@synthesize birthDay;
@synthesize membership;
@synthesize years;//0824
@synthesize bVersion;//append
@synthesize versionShow;
@synthesize CountryCode;
@synthesize strAge, strHeight, strEducation, strMstatus,strJobtitle, strEthnicity,strIncome, strReligion;
@synthesize isVip,checkVipStroy,paying;
@synthesize dicProfileMatch;

//https://itunesconnect.apple.com/WebObjects/iTunesConnect.woa/wa/signOutCompleted
//http://www.samliew.com/icgen/
//http://qdpm.esynchrony.com/index.php/tasksComments?projects_id=7&tasks_id=2627
//S0677679E
//S8143513H
//S6215167F
//S2882288Z
//S0480909B
//S8457869Z
//S2828747Z
//S0611592F
//S0492438Z
//S2622935I
//S6717529H

+ (eSyncronyAppDelegate*)sharedInstance
{
    return kAppDelegate;
}

- (void)dealloc
{
    [_window release];
    [_viewController release];
    [mainMenuViewCtrl release];

    [super dealloc];
}

// app启动完毕时调用
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    // Override point for customization after application launch.
    
   // DetailMathProfileViewController* viewController = [[[DetailMathProfileViewController alloc] initWithNibName:@"DetailMathProfileViewController" bundle:nil] autorelease];
    
    //viewController.dicMatchInfo = self.dicMatchInfo;
    //self.viewController = viewController;

    self.viewController = [[[eSyncronyViewController alloc] initWithNibName:@"eSyncronyViewController" bundle:nil] autorelease];
    
    //self.window.rootViewController = self.viewController;
    
    self.window.rootViewController = [[[BaseNavigationController alloc] initWithRootViewController:self.viewController] autorelease];
    [self.window makeKeyAndVisible];
    self.window.backgroundColor = [UIColor whiteColor];

    // Fill in with your Parse credentials:
    [Parse setApplicationId:@"NdavOb4LmQJS3uARu1fP0MKBn6RO94yqVyEoEXqL" clientKey:@"EhsPVUU39Bd9xRiTl1pTF4PtB9ttHf7Db3RCzvse"];
    
#pragma paypal
    [PayPalMobile initializeWithClientIdsForEnvironments:@{PayPalEnvironmentProduction : @"AftnVBAGZIEBP473FL4GLOY-zAKVBVlMT1jAddUQW9ecVi0gSRQI_4svKH6i",
                                                           PayPalEnvironmentSandbox :@"AXi-rlzaNYhVQApobgpFUMW0GJNon7eulrDDQMDsz7wZnVRlVbJRufwzkcSGYmOxmjN_QGxIHLgxapeL"}];
#pragma newRelic
    [NewRelicAgent startWithApplicationToken:@"AA71c8443e16a44bc14b2a672385a015c6b4a33c25"];
    
    // Your Facebook application id is configured in Info.plist.
    [PFFacebookUtils initializeFacebook];
    
    self.language = [[NSLocale preferredLanguages] objectAtIndex:0];//获取国际化语言的选择是是什么
    NSLog(@"self.language===%@\n",self.language);//id-CN  zh-Hant-CN
    
    
    // system >=IOS8.0
    if([[[UIDevice currentDevice]systemVersion]floatValue] >=8.0)
     {
        //NSLog(@"向APNS注册1111...");
         
         [[UIApplication sharedApplication]registerUserNotificationSettings:[UIUserNotificationSettings
         
         settingsForTypes:(UIUserNotificationTypeSound|UIUserNotificationTypeAlert|UIUserNotificationTypeBadge)
         
         categories:nil]];
         
         [[UIApplication sharedApplication]registerForRemoteNotifications];
     
     }else{//system < IOS8.0
     //推送的形式：标记，声音，提示
     //注册启用push
         //NSLog(@"向APNS注册2222...");
//         [[UIApplication sharedApplication]registerForRemoteNotificationTypes:
//         
//         (UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeSound|UIRemoteNotificationTypeBadge)];
         [[UIApplication sharedApplication]registerForRemoteNotifications];
     }

    //NSString *version = [[NSBundle mainBundle]objectForInfoDictionaryKey:(NSString *)kCFBundleVersionKey];//build
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];//iTunes
    
    //renewal&matchtips
    _show = 1;
    //decline count
    _declineTotal = 0;
    //version
    versionShow = 0;
    
    isVip = @"false";
    checkVipStroy = @"false";
    paying = @"no";
#pragma Google
    //Google Analytics 初始化
    // 1 send uncaught exceptions to Google Analytics
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    // 2 Optional: set Logger to VERBOSE for debug information.
    //[[GAI sharedInstance].logger setLogLevel:kGAILogLevelError];
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
    
    // 3 Sampling Rate 20 seconds
    [GAI sharedInstance].dispatchInterval = 20;
    // 4 Initialize tracker
//    [[GAI sharedInstance] trackerWithTrackingId:@"UA-47764920-1"];//原来建立的google adwords by before
    
    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithName:@"altTracker" trackingId:@"UA-47764920-1"];
    id<GAITracker> tracker2 = [[GAI sharedInstance] trackerWithTrackingId:@"UA-61653439-1"];//google adwords　for Limit
    // Enable IDFA collection.
    tracker.allowIDFACollection = YES;
    tracker2.allowIDFACollection = YES;
//    [[GAI sharedInstance] setDryRun:YES];
    
    //Sampling Rate
    //Ensure tracker sampling data consistency, to ensure that in any version at any time to maintain a fixed sampling rate: update the tracker + current version number
    [tracker set:kGAIAppVersion value:version];//这段代码将会自动从Info.plist文件中获取当前应用的版本号，把版本号设置到跟踪器对象中
    [tracker set:kGAISampleRate value:@"50.0"];//samping rate of 50%
    
    [tracker2 set:kGAIAppVersion value:version];//这段代码将会自动从Info.plist文件中获取当前应用的版本号，把版本号设置到跟踪器对象中
    [tracker2 set:kGAISampleRate value:@"50.0"];
    
    [tracker set:kGAIScreenName value:@"Begin"];//append 1127
        
    [[GAI sharedInstance] dispatch];
    
    kAppDelegate = self;
    
    
    //socket
//    socketApi=[SocoketAPI sharedInstance];
    
    //message
//    SVMessages *svMessage = [[SVMessages alloc] init];
//    appDelegate().dicConversations = [NSMutableDictionary dictionaryWithDictionary:[svMessage getJsonFromLocal]];
//    NSError *messageError;
//    appDelegate().conversationMessageModel=[[SVMessageModel alloc]initWithDictionary:[svMessage getJsonFromLocal] error:&messageError];

    bVersion=NO;
    dispatch_async(dispatch_get_main_queue(), ^{
    
        if ( versionShow == 0) {
            
            [self checkVersion];
            
            if ( bVersion) {
                
                [self loadLoginInfo];
                [self.viewController initSetting];
            }
            
        }
        else
        {
            [self loadLoginInfo];
            [self.viewController initSetting];
            
        }
    });

    //===============enable automated usage reporting==============//
    //跟踪应用使用情况
    [ACTAutomatedUsageTracker enableAutomatedUsageReportingWithConversionID:@"982707337"];
    
    //////////////////enable automated usage reporting//////////////////////////
    [ACTAutomatedUsageTracker enableAutomatedUsageReportingWithConversionID:@"1039105568"];
    
    //ios移动应用转化
    //hk
    [ACTConversionReporter reportWithConversionID:@"982707337" label:@"ikQ0CKLxulkQidnL1AM" value:@"0.00" isRepeatable:NO];
    
    //sg
    [ACTConversionReporter reportWithConversionID:@"1039105568" label:@"O4RGCK7pxlgQoPy97wM" value:@"0.00" isRepeatable:NO];

    //version reminder
//   [self onCheckVersion];

    //append referral
    dispatch_async(dispatch_get_main_queue(), ^{//leaks 100%
        //1.初始化ShareSDK应用,字符串"iosv1101"是应该换成你申请的ShareSDK应用中的Appkey
        [ShareSDK registerApp:@"99fe0a1ebe5a"];
        //2. 初始化社交平台
        //2.1 代码初始化社交平台的方法
        [self initializePlat];
        //2.2 使用后台管理初始社交平台的方法
        [self initializePlatForTrusteeship];
        //3.设置根视图控制器，如果没有使用storyboard一定要设置。
    });


    
//    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"ethnicity_zhHant" ofType:@"plist"];
//    NSMutableDictionary *usersDic = [[NSMutableDictionary alloc]initWithContentsOfFile:plistPath];
//    NSLog(@"usersDic == %@",usersDic);
    
    return YES;
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
  
    //-------------------APNS----------------------//
    // 处理推送消息
    //NSLog(@"接受到消息:%@",[userInfo descripton]);
    //NSLog(@"收到推送消息:%@",[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]);
    //NSLog(@"pushinfo=%@",userInfo);
    
    if (application.applicationState == UIApplicationStateActive) {
        NSLog(@"转换成一个本地通知，显示到通知栏");
        // 转换成一个本地通知，显示到通知栏，你也可以直接显示出一个alertView，只是那样稍显aggressive：）
#if  1
        UILocalNotification *localNotification = [[UILocalNotification alloc] init];
        localNotification.userInfo = userInfo;//为一个自定义字典对象，可以传送自定义的对象
        localNotification.soundName = UILocalNotificationDefaultSoundName;//消息提示声音
        localNotification.alertBody = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];//文字消息或多语系的字符串
        localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];//指定发送消息的时间if fireDate==nil,则消息会被马上发送出去
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];//预约消息
        
       // [[UIApplication sharedApplication]presentLocalNotificationNow:localNotification];//立即显示消息
#endif
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"eSynchrony" message:[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
#if 0
    NSDate *now = [NSDate date];
    // 设置提醒时间，倒计时以秒为单位。以下是从现在开始0秒以后通知
    appdelegate.notification.fireDate=[now dateByAddingTimeInterval:1];
    // 设置时区，使用本地时区
    appdelegate.notification.timeZone=[NSTimeZone defaultTimeZone];
    // 设置提示的文字
    appdelegate.notification.alertBody=@"家校中国有新的短消息。。。";
    // 设置提示音，使用默认的
    appdelegate.notification.soundName= UILocalNotificationDefaultSoundName;
    // 锁屏后提示文字，一般来说，都会设置与alertBody一样
    appdelegate.notification.alertAction=NSLocalizedString(@"家校中国有新的短消息。。。", nil);
    // 这个通知到时间时，你的应用程序右上角显示的数字. 获取当前的数字+1
    appdelegate.notification.applicationIconBadgeNumber=0;
    //给这个通知增加key 便于半路取消。nfkey这个key是自己随便写的，还有notificationtag也是自己定义的ID。假如你的通知不会在还没到时间的时候手动取消，那下面的两行代码你可以不用写了。取消通知的时候判断key和ID相同的就是同一个通知了。
    NSDictionary *dict =[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:1],@"nfkey",nil];
    [appdelegate.notification setUserInfo:dict];
    // 启用这个通知
    [[UIApplication sharedApplication] scheduleLocalNotification:appdelegate.notification];
#endif
}
-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    NSLog(@"notificationnotification===接受到消息：%@",notification);
    
}
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    
    NSLog(@"无法获取 token:%@",[error description]);
    NSLog(@"Registfail%@",error);
    
}
-(void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    //注册远程通知
    [application registerForRemoteNotifications];
}
-(BOOL)checkVersion
{
    NSDictionary*    result;
    result = [UtilComm Reminder];
    //NSLog(@"%@",result);
    NSString  *current_version=[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString *ios_version=[NSString new];
   // NSString *appStoreVersion=@"1.2.6";
    if( result == nil )
    {
        return YES;
    }
    id response = [result objectForKey:@"response"];
    if( response == nil )
    {
        return YES;
    
    }
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )//让程序运行下去
    {
        return YES;
    }
    if ([response isKindOfClass:[NSDictionary class]]) {
        
        ios_version=[response objectForKey:@"ios"];
    }

    if([ios_version compare:current_version options:NSNumericSearch] == NSOrderedDescending)//降序　ios>current
    {
        UIAlertView *alter=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"New version available",nil) message:NSLocalizedString(@"A newer and better version of eSynchrony is required in order to continue. Please update now.",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",nil) otherButtonTitles:NSLocalizedString(@"Update",nil), nil];
        alter.tag=10002;
        [alter show];
        [alter release];
    }else//防止程序crash
        return YES;
    return bVersion;
    
}

//http://blog.csdn.net/xiaoxuan415315/article/details/9383453
//http://blog.sina.com.cn/s/blog_5df876f30100xlwo.html
//-(void)onCheckVersion
//{
//
//    NSDictionary *infoDic = [[NSBundle mainBundle] infoDictionary];
//    CFShow(infoDic);
//    NSString *currentVersion = [infoDic objectForKey:@"CFBundleShortVersionString"];
//    NSLog(@"%@",currentVersion);
//    
//    //NSString *URL = @"http://itunes.apple.com/lookup?id=893845592";//应用程序的id893845592
//    
//    NSString *URL =@"http://itunes.apple.com/search?term=eSyncrony&entity=software";
//    
//    NSLog(@"%@",URL);
//    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
//    [request setURL:[NSURL URLWithString:URL]];
//    [request setHTTPMethod:@"POST"];
//    NSHTTPURLResponse *urlResponse = nil;
//    NSError *error = nil;
//    NSData *recervedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&error];
//    
//    NSString *results = [[NSString alloc] initWithBytes:[recervedData bytes] length:[recervedData length] encoding:NSUTF8StringEncoding];
//    NSDictionary *dic = [results JSONValue];//此处或得数据为resultCount=0,results为nil.
//    NSArray *infoArray = [dic objectForKey:@"results"];
//    if ([infoArray count]) {
//        NSDictionary *releaseInfo = [infoArray objectAtIndex:0];
//        NSString *lastVersion = [releaseInfo objectForKey:@"version"];
//        NSLog(@"%@",lastVersion);
//        
//        if (![lastVersion isEqualToString:currentVersion]) {
//            //trackViewURL = [releaseInfo objectForKey:@"trackVireUrl"];
//             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"New version available" message:@"A newer and better version of eSynchrony is required in order to continue. Please update now." delegate:self cancelButtonTitle:@"Close" otherButtonTitles:@"Update", nil];
//            alert.tag = 10000;
//            [alert show];
//        }
//        else
//        {
//           UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"New version available" message:@"The version is the updatest" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];//确定，后nil
//            alert.tag = 10001;
//            [alert show];
//        }
//    }
//}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
//    if (alertView.tag==10000) {
//        if (buttonIndex==1) {
//           // NSURL *url = [NSURL URLWithString:@"https://itunes.apple.com"];
//            NSURL *url = [NSURL URLWithString:@"https://itunes.apple.com/sg/app/real-dating-service-esynchrony/id893845592?mt=8"];
//            [[UIApplication sharedApplication]openURL:url];
//        }
//    }
    if (alertView.tag==10002) {
        if (buttonIndex==0) {
            
            bVersion=YES;
            versionShow = 1;
            
        }
        if (buttonIndex==1) {
          //  NSURL *url = [NSURL URLWithString:@"https://itunes.apple.com"];
            NSURL *url = [NSURL URLWithString:@"https://itunes.apple.com/sg/app/real-dating-service-esynchrony/id893845592?mt=8"];
            [[UIApplication sharedApplication]openURL:url];
            
            bVersion=YES;
            versionShow = 1;
            
        }
    }
}

- (void)logOut
{
    [eSyncronyAppDelegate sharedInstance].bIsLoggedIn = NO;
    
    [eSyncronyAppDelegate sharedInstance].show = 1;
    [eSyncronyAppDelegate sharedInstance].declineTotal = 0;
    
    [eSyncronyAppDelegate sharedInstance].strLoginEmail = @"";
    [eSyncronyAppDelegate sharedInstance].strLoginPassword = @"";
    [eSyncronyAppDelegate sharedInstance].strFaceBookID = @"";

    [eSyncronyAppDelegate sharedInstance].strAccNo = @"";
    [eSyncronyAppDelegate sharedInstance].strTokenKey = @"";
    
//    [eSyncronyAppDelegate sharedInstance].years = years;//append
    [eSyncronyAppDelegate sharedInstance].strName = @"";
    [eSyncronyAppDelegate sharedInstance].strNric = @"";
    [eSyncronyAppDelegate sharedInstance].birthDay = nil;
    [eSyncronyAppDelegate sharedInstance].membership = @"";
    
    [eSyncronyAppDelegate sharedInstance].isVip = @"";
    [eSyncronyAppDelegate sharedInstance].checkVipStroy = @"";
    
    [[eSyncronyAppDelegate sharedInstance] saveLoginInfo];
    [[eSyncronyAppDelegate sharedInstance] logoutFB];
}
- (void)logoutFB {
    FBSession *fbManager = [FBSession activeSession];
    [fbManager closeAndClearTokenInformation];
}

-(void)esync_clearCache
{
//    self.flickrPaginator.results
//    dispatch_release(fetchQ);
    
}

- (void)saveLoginInfo
{
    
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    
    if( strAccNo == nil )
        strAccNo = @"";
    if( strTokenKey == nil )
        strTokenKey = @"";
   
    //append 0519
    if (strHandphone==nil) {
        strHandphone=@"";
    }
    
    if( strLoginEmail == nil )
        strLoginEmail = @"";
    if( strLoginPassword == nil )
        strLoginPassword = @"";
    if( strFaceBookID == nil )
        strFaceBookID = @"";
    
    if( strName == nil )
        strName = @"";
    if( strNric == nil )
        strNric = @"";
    if( membership == nil )
        membership = @"";
    
//    if (birthDay == nil) {
//        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//        [dateFormatter setDateFormat:@"MM/dd/yyyy"];
//        birthDay = [dateFormatter dateFromString:@"01/01/1990"];
//        [dateFormatter release];
//    }
    if (years == 0) {
        years = 0;
    }
    if (isNoNric == nil) {
        isNoNric = @"true";
    }
    if (isNoPhoto == nil) {
        isNoPhoto = @"true";
    }
    //vip
    if (isVip == nil) {
        isVip = @"false";
    }
    if (checkVipStroy == nil) {
        checkVipStroy = @"false";
    }
    //match list filter
    
    if (strAge == nil) {
        strAge = @"";
    }
    if (strHeight == nil) {
        strHeight = @"";
    }
    if (strEducation == nil) {
        strEducation = @"-1";
    }
    if (strMstatus == nil) {
        strMstatus =@"-1";
    }
    if (strJobtitle == nil) {
        strJobtitle = [NSNumber numberWithInt:-1];
    }
    if (strEthnicity == nil ) {
        strEthnicity = @"-1";
    }
    if (strIncome == nil) {
        strIncome = @"-1";
    }
    if (strReligion == nil) {
        strReligion =@"-1";
    }
    
    if (paying == nil) {
        paying = @"no";
    }
    
    [defaults setObject:[NSNumber numberWithBool:bIsLoggedIn] forKey:@"blogin"];
    
    [defaults setObject:strAccNo forKey:@"AccNo"];
    [defaults setObject:strTokenKey forKey:@"Token"];
    
    [defaults setObject:strHandphone forKey:@"handphone"];//append repair 0519
    
    [defaults setObject:strLoginEmail forKey:@"Email"];
    [defaults setObject:strLoginPassword forKey:@"Passwd"];
    
    [defaults setObject:strFaceBookID forKey:@"FacebookID"];

    [defaults setObject:strName forKey:@"nName"];
    [defaults setObject:strNric forKey:@"Nric"];
    [defaults setObject:[NSNumber numberWithInt:gender] forKey:@"gender"];
    [defaults setObject:[NSNumber numberWithInt:countryId] forKey:@"countryId"];
    [defaults setObject:[NSNumber numberWithInt:nIDVerify] forKey:@"IDVerify"];
    [defaults setObject:birthDay forKey:@"birthDay"];
    [defaults setObject:membership forKey:@"membership"];
    
    [defaults setObject:[NSNumber numberWithInt:years] forKey:@"years"];//append 0825
    
    [defaults setObject:isNoNric forKey:@"noNric"];
    [defaults setObject:isNoPhoto forKey:@"noPhoto"];
    
    [defaults setObject:isVip forKey:@"is_vip"];
    [defaults setObject:checkVipStroy forKey:@"set_vip_story"];
    
 //match list filter
    [defaults setObject:strAge forKey:@"age"];
    [defaults setObject:strEducation forKey:@"education"];
    [defaults setObject:strMstatus forKey:@"mstatus"];
    [defaults setObject:strJobtitle forKey:@"jobtitle"];
    [defaults setObject:strIncome forKey:@"income"];
    [defaults setObject:strReligion forKey:@"religion"];
    [defaults setObject:strEthnicity forKey:@"ethnicity"];
    [defaults setObject:strHeight forKey:@"height"];
    
    [defaults setObject:paying forKey:@"paying"];
    
    [defaults synchronize];
}

- (void)saveBasicInfoUser
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    
    if( strLoginEmail == nil )
        strLoginEmail = @"";
    
    [defaults setObject:strLoginEmail forKey:@"basicInfoSavedEmail"];

    [defaults synchronize];
}

- (void)loadLoginInfo
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    
    bIsLoggedIn = [[defaults objectForKey:@"blogin"] boolValue];
    strLoginEmail = [defaults objectForKey:@"Email"];
    strLoginPassword = [defaults objectForKey:@"Passwd"];
    strFaceBookID = [defaults objectForKey:@"FacebookID"];
    strAccNo = [defaults objectForKey:@"AccNo"];
    strTokenKey = [defaults objectForKey:@"Token"];
    
    strHandphone=[defaults objectForKey:@"handphone"];
    
    gender = (int)[[defaults objectForKey:@"gender"] integerValue];
    countryId  = (int)[[defaults objectForKey:@"countryId"] integerValue];
    
    strName = [defaults objectForKey:@"nName"];
    strNric = [defaults objectForKey:@"Nric"];
    membership = [defaults objectForKey:@"membership"];
    birthDay = [defaults objectForKey:@"birthDay"];
    
    years = [[defaults objectForKey:@"years"]intValue];
    
    isVip = [defaults objectForKey:@"is_vip"];
    checkVipStroy = [defaults objectForKey:@"set_vip_story"];
    
    paying = [defaults objectForKey:@"paying"];
    
    if( strAccNo == nil )
        strAccNo = @"";
    if( strTokenKey == nil )
        strTokenKey = @"";

    if (strHandphone == nil) {
        strHandphone =@"";
    }
 
    if( strLoginEmail == nil )
        strLoginEmail = @"";
    if( strLoginPassword == nil )
        strLoginPassword = @"";

    if( strFaceBookID == nil )
        strFaceBookID = @"";

    if( strName == nil )
        strName = @"";
    if( strNric == nil )
        strNric = @"";
    if( membership == nil )
        membership = @"";
    
    if (isVip == nil) {
        isVip = @"false";
    }
    if (checkVipStroy == nil) {
        checkVipStroy = @"false";
    }
    if (paying == nil) {
        paying = @"no";
    }
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    //--------------------apns----------------------//
    
//    NSLog(@"My token is:%@",deviceToken);
//    NSLog(@"成功获取设备编号:%@",[deviceToken description]);
    
    //获取设备(去除掉不需要的字符< >)
//  NSString *deviceId=[[deviceToken description]substringWithRange:NSMakeRange(1, [[deviceToken description]length]-2)];
//    deviceId = [deviceId stringByReplacingOccurrencesOfString:@"" withString:@""];
//    deviceId =[deviceId stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSString *deviceId = [NSString stringWithFormat:@"%@",deviceToken];
    deviceId = [deviceId stringByReplacingOccurrencesOfString:@" " withString:@""];
    deviceId = [deviceId stringByReplacingOccurrencesOfString:@"<" withString:@""];
    deviceId = [deviceId stringByReplacingOccurrencesOfString:@">" withString:@""];
    if (deviceId == nil) {
        deviceId = @"";
    }
    
    //注册成功，将deviceToken保存到应用服务器数据库中
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    NSDictionary*    result;
    if ([eSyncronyAppDelegate sharedInstance].strAccNo==nil) {
        [eSyncronyAppDelegate sharedInstance].strAccNo = @"";
    }
    
    [params setObject:deviceId forKey:@"ios_regid"];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    
    result = [UtilComm APNRegister:params];
    if( result == nil )
    {
        [ErrorProc alertAuthenFailureMessage];
        return;
    }
    id response = [result objectForKey:@"response"];
    if( response == nil )
    {
        //[ErrorProc alertMessage:@"Unknown Error" withTitle:@"Saving Failure"];
        [ErrorProc alertAuthenFailureMessage];
        return;
    }
    if( [response isKindOfClass:[NSString class]] && [response hasPrefix:@"ERROR"] )
    {
       // [ErrorProc alertLoginError:response];//remove Null character info
    }
    else
    {
        if( [response isKindOfClass:[NSString class]] && [response hasSuffix:@"successfully."] )
        {
            NSLog(@"APN: %@",response);
        }
    }
    [self checkPush];
    
}
- (void)checkPush
{
    if ([[[UIDevice currentDevice]systemVersion]floatValue] >=8.0) {
        UIUserNotificationSettings *mySet = [[UIApplication sharedApplication] currentUserNotificationSettings];
        UIUserNotificationType types = mySet.types;
        
        if (types == UIUserNotificationTypeNone){
            NSLog(@"Notification not enabled");
        }else {
            NSLog(@"Notification Enabled");
            
        }
    }else{
//        [UIApplication isRegisteredForRemoteNotifications], or -[UIApplication currentUserNotificationSettings]
        UIRemoteNotificationType types = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
        if (types == UIUserNotificationTypeNone){
            NSLog(@"Notification not enabled");
        }else {
            NSLog(@"Notification Enabled");
        }
    }
}

// A function for parsing URL parameters
- (NSDictionary*)parseURLParams:(NSString *)query {
    
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val = [[kv objectAtIndex:1]
                         stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [params setObject:val forKey:[kv objectAtIndex:0]];
    }
    return params;

}
//append referral
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    return [ShareSDK handleOpenURL:url wxDelegate:self];
}

//- (BOOL)application:(UIApplication *)application
//            openURL:(NSURL *)url
//  sourceApplication:(NSString *)sourceApplication
//         annotation:(id)annotation
//{
//    return [ShareSDK handleOpenURL:url
//                 sourceApplication:sourceApplication
//                        annotation:annotation
//                        wxDelegate:self];
//}
- (void)initializePlat
{
    //连接短信分享
    [ShareSDK connectSMS];

    /**
     连接微信应用以使用相关功能，此应用需要引用WeChatConnection.framework和微信官方SDK
     http://open.weixin.qq.com上注册应用，并将相关信息填写以下字段
     **/
    [ShareSDK connectWeChatWithAppId:@"wxdae67c44d0993d83"
                           appSecret:@"248da6106c8cd038dc14c01eefd0c0dd"
                           wechatCls:[WXApi class]];
    
  
    
    /**
     连接Facebook应用以使用相关功能，此应用需要引用FacebookConnection.framework
     https://developers.facebook.com上注册应用，并将相关信息填写到以下字段
     **/
//    [ShareSDK connectFacebookWithAppKey:@"107704292745179"
//                              appSecret:@"38053202e1a5fe26c80c753071f0b573"];

    
    //连接拷贝
    [ShareSDK connectCopy];
    
       /**
     连接WhatsApp应用以使用相关功能，此应用需要引用WhatsAppConnection.framework库
     **/
    [ShareSDK connectWhatsApp];
    

}

/**
 * @brief  托管模式下的初始化平台
 */
- (void)initializePlatForTrusteeship
{
    //导入微信需要的外部库类型，如果不需要微信分享可以不调用此方法
    [ShareSDK importWeChatClass:[WXApi class]];
}


- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{

//    return [FBSession.activeSession handleOpenURL:url];
    BOOL wasHandled = [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
    if (wasHandled) {
        return [FBSession.activeSession handleOpenURL:url];
    }else
    {
        //GA
        NSString *urlString = [url absoluteString];
        
        id<GAITracker> tracker = [[GAI sharedInstance] trackerWithName:@"tracker"
                                                            trackingId:@"UA-47764920-1"];
        // setCampaignParametersFromUrl: parses Google Analytics campaign ("UTM")
        // parameters from a string url into a Map that can be set on a Tracker.
        GAIDictionaryBuilder *hitParams = [[GAIDictionaryBuilder alloc] init];
        
        // Set campaign data on the map, not the tracker directly because it only
        // needs to be sent once.
        [hitParams setCampaignParametersFromUrl:urlString];
        
        // Campaign source is the only required campaign field. If previous call
        // did not set a campaign source, use the hostname as a referrer instead.
        if(![hitParams get:@"&cm"] && [url host].length != 0) {
            // Set campaign data on the map, not the tracker.
            [hitParams set:@"referrer" forKey:@"&cm"];
            [hitParams set:[url host] forKey:@"&cs"];
            
        }
        NSDictionary *hitParamsDict = [hitParams build];
        // A screen name is required for a screen view.
        [tracker set:@"screen name" value:@"screen name"];
        
        NSString *referrer=@"";
        NSString *utm_source=@"";
        NSString *utm_medium=@"";
        NSString *utm_term=@"";
        NSString *utm_content=@"";
        NSString *utm_campaign=@"";
        
        [tracker send:[[[GAIDictionaryBuilder createScreenView] setAll:hitParamsDict] build]];
        
        if (![Global isNullOrNilObject:[hitParamsDict objectForKeyNotNull:@"&cs"]]) {
            utm_source=[hitParamsDict objectForKey:@"&cs"];
        }
        if (![Global isNullOrNilObject:[hitParamsDict objectForKeyNotNull:@"&cm"]]) {
            utm_medium=[hitParamsDict objectForKey:@"&cm"];
        }
        if (![Global isNullOrNilObject:[hitParamsDict objectForKeyNotNull:@"&ck"]]) {
            utm_term=[hitParamsDict objectForKey:@"&ck"];
        }
        if (![Global isNullOrNilObject:[hitParamsDict objectForKeyNotNull:@"&cc"]]) {
            utm_content=[hitParamsDict objectForKey:@"&cc"];
        }
        if (![Global isNullOrNilObject:[hitParamsDict objectForKeyNotNull:@"&cn"]]) {
            utm_campaign=[hitParamsDict objectForKey:@"&cn"];
        }
        
        NSString *referrerURL=[NSString stringWithFormat:@"referrer=%@&utm_source=%@&*utm_medium=%@&utm_term=%@&utm_content=%@&utm_campaign=%@",referrer,utm_source,utm_medium,utm_term,utm_content,utm_campaign];
        NSLog(@"referrerURL = %@",referrerURL);
        
        //        [Sessions sharedInstance].referrerURl=referrerURL;
        
        //        if (referrerURL.length>0) {
        //            appDelegate().alertShowInfo = [[TTAlertView alloc]initWithTitle:[Sessions sharedInstance].referrerURl
        //                                                                    message:@""
        //                                                                   delegate:self
        //                                                          cancelButtonTitle:@"ok"
        //                                                              withTextField:nil
        //                                                          otherButtonTitles:nil];
        //            appDelegate().alertShowInfo.tag = 1222345;
        //            [appDelegate().alertShowInfo show];
        //        }
        
    }
    return YES;

//    BOOL urlWasHandled =
//    [FBAppCall handleOpenURL:url
//           sourceApplication:sourceApplication
//             fallbackHandler:
//     ^(FBAppCall *call) {
//         //Parse the incoming URL to look for a target_url parameter
//         NSString *query = [url query];
//         NSDictionary *params = [self parseURLParams:query];
//         // Check if target URL exists
//         NSString *appLinkDataString = [params valueForKey:@"al_applink_data"];
//         if (appLinkDataString) {
//             NSError *error = nil;
//             NSDictionary *applinkData =
//             [NSJSONSerialization JSONObjectWithData:[appLinkDataString dataUsingEncoding:NSUTF8StringEncoding]
//                                             options:0
//                                              error:&error];
//             if (!error &&
//                 [applinkData isKindOfClass:[NSDictionary class]] &&
//                 applinkData[@"target_url"]) {
//                 self.refererAppLink = applinkData[@"referer_app_link"];
//                 NSString *targetURLString = applinkData[@"target_url"];
//                 // Show the incoming link in an alert
//                 // Your code to direct the user to the
//                 // appropriate flow within your app goes here
//                 [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Received link:",nil)
//                                             message:targetURLString
//                                            delegate:nil
//                                   cancelButtonTitle:NSLocalizedString(@"OK",nil)
//                                   otherButtonTitles:nil] show];
//             }
//         }
//     }];
//    
//    return urlWasHandled;
}


// This method sends any queued hits when the app enters the background.
- (void)sendHitsInBackground {
    __block BOOL taskExpired = NO;
    
    __block UIBackgroundTaskIdentifier taskId =
    [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        taskExpired = YES;
    }];
    
    if (taskId == UIBackgroundTaskInvalid) {
        return;
    }
//     __weak
    eSyncronyAppDelegate *weakSelf = self;
    self.dispatchHandler = ^(GAIDispatchResult result) {
        // Send hits until no hits are left, a dispatch error occurs, or
        // the background task expires.
        if (result == kGAIDispatchGood && !taskExpired) {
            [[GAI sharedInstance] dispatchWithCompletionHandler:weakSelf.dispatchHandler];
        } else {
            [[UIApplication sharedApplication] endBackgroundTask:taskId];
        }
    };
    
    [[GAI sharedInstance] dispatchWithCompletionHandler:self.dispatchHandler];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    if([UIApplication sharedApplication].applicationIconBadgeNumber!=0){
        NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
        NSDictionary*    result;
        if ([eSyncronyAppDelegate sharedInstance].strAccNo==nil) {
            [eSyncronyAppDelegate sharedInstance].strAccNo = @"";
        }
        [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
        [params setObject:@"2" forKey:@"platform"];
        
        result = [UtilComm MsgNum:params];
        if( result == nil )
        {
            ////[ErrorProc alertToCheckInternetStateTitle:@"Authentication Failure"];
            return;
        }
        
        id response = [result objectForKey:@"response"];
        if( response == nil )
        {
            //[ErrorProc alertMessage:@"Unknown Error" withTitle:@"Saving Failure"];
            return;
        }
        
        if( [response isKindOfClass:[NSString class]] && [response isEqualToString:@"Clear"] )
        {
            [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
        }
    }
}

//#pragma mark - SOCKET.
//-(void)reconnect
//{
//    if (socketIO == nil) {
//        // create socket.io client instance
//        socketIO = [[SocketIO alloc] initWithDelegate:self];
//    }
//    [socketIO disconnect];
//
//    // you can update the resource name of the handshake URL
//    // see https://github.com/pkyeck/socket.IO-objc/pull/80
//    // [socketIO setResourceName:@"whatever"];
//    
//    // if you want to use https instead of http
//    // socketIO.useSecure = YES;
//    
//    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:0];
//    [params setObject:[NSNumber numberWithInt:3] forKey:@"EIO"];
//    [params setObject:@"polling" forKey:@"transport"];
//    // connect to the socket.io server that is running locally at port 3000
//    //    [socketIO connectToHost:@"dev.esynchrony.com" onPort:3000];
//    [socketIO connectToHost:HOST onPort:CHATPORT withParams:params];
//    
//}

//如果整个应用的view controller不支持旋屏
- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(nullable UIWindow *)window{
    return UIInterfaceOrientationMaskPortrait;
}

// Upon receiving app call memory warning(当程序接受到内存警告时处理)
- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
//    UIAlertView *av= [[UIAlertView alloc]initWithTitle:@"eSynchrony" message:NSLocalizedString(@"Recived Memory Warning", nil) delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//    [av show];
//    [av release];
    
//    NSAutoreleasePool* pool =[[NSAutoreleasePool alloc]init];
//    [pool release];
}
// app进入后台时调用（比如按了home键）
- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
//    NSAutoreleasePool *pool =[[NSAutoreleasePool alloc]init];
//    [pool drain];
    
    [self sendHitsInBackground];//append 16/04/06
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [GAI sharedInstance].dispatchInterval = 120;//append 16/04/06
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBSettings setDefaultAppID:@"226737744171417"];
    [FBAppEvents activateApp];
    [FBAppCall handleDidBecomeActive];
    
    [GAI sharedInstance].optOut =
    ![[NSUserDefaults standardUserDefaults] boolForKey:kAllowTracking];//append 16/04/06
}
//terminate-->Terminate the program(终止程序)
- (void)applicationWillTerminate:(UIApplication *)application
{
    
//     [[ModelManager sharedInstance] deleteUnusedCacheModels];
    
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
//    http://stackoverflow.com/questions/28606967/ios-app-when-should-i-clear-my-cache-data-using-realm
    
//    [[eSyncronyAppDelegate sharedInstance] deleteUnusedCacheModels];
//    Sample code in deleteUnusedCacheModels:
//    NSDate* limitDate = [NSDate dateWithTimeIntervalSinceNow:-(60.0 * 60.0 * 24.0 * 7.0)];
//    RLMResults* modelProductListArray = [ModelProductList objectsInRealm:_cacheDb
//                                                                   where:@"modelUpdate < %@ || modelDelete = %@", limitDate, @YES];
//    [_cacheDb deleteObjects:modelProductListArray];
    
 /*   Delete cache at the following times:
    
    When the app starts. (didFinishLaunchingWithOptions)
    When the app terminates. (applicationWillTerminate)
  */
//    NSDate* limitDate = [NSDate dateWithTimeIntervalSinceNow:-(60.0 * 60.0 * 24.0 * 7.0)];
//    RLMResults* modelProductListArray = [ModelProductList objectsInRealm:_cacheDb
//                                                                   where:@"modelUpdate < %@ || modelDelete = YES", limitDate];
//    [_cacheDb deleteObjects:modelProductListArray];
    
    
}
@end
#pragma mark - Convenience Constructors

eSyncronyAppDelegate *appDelegate(void)
{
    return (eSyncronyAppDelegate *)[[UIApplication sharedApplication] delegate];
}

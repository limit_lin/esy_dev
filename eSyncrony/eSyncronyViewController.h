
#import <UIKit/UIKit.h>
#import "HttpBaseViewController.h"

#import "GAITrackedViewController.h"
//#import "Reachability.h"


@class BasicViewController;
@class BasicStepViewsController;

@interface eSyncronyViewController : HttpBaseViewController<UIScrollViewDelegate,UIWebViewDelegate,UIAlertViewDelegate>
{
    IBOutlet UIScrollView   *scrlView;
    IBOutlet UIView         *mainView;
 
    BasicViewController         *basicView;
    BasicStepViewsController    *basicStepView;
    NSURL *videoPathURL;
    
    int Countrycode;
    
    IBOutlet UIImageView *mianBgImageView;//animation
    
    IBOutlet UIButton *btnFBFollow;
    IBOutlet UIButton *btnYTBFollow;
    IBOutlet UIButton *btnWHFollow;
    
    
    IBOutlet UILabel *registerAnimationLab;
    
    
    
}

//follow us
- (IBAction)didClickFbFollowus:(id)sender;
- (IBAction)didClickYoutubeFollowus:(id)sender;
- (IBAction)didClickWhatsappFollowus:(id)sender;


- (IBAction)didClickCreateAnAccount:(UIButton *)sender;
- (IBAction)didClickMemberLogin:(UIButton *)sender;
- (IBAction)didClickPullup:(id)sender;
- (IBAction)didClickOurSuccessStories:(id)sender;
- (IBAction)didPlayVedio:(id)sender;

- (void)enterWelcomeView;
- (void)enterBasicStepView:(int)step;
- (void)enterBasicStepWithLifeStyle:(int)step Values:(NSMutableDictionary *)values;
- (void)enter20ReasonsView;
- (void)enterLifeStyleView;
- (void)enterCriteriaView;
- (void)enterAppearanceView;
- (void)enterInterestView;
- (void)enterMyValueView;
- (void)enterRelationStepsView;
- (void)enterCultureStepsView;
- (void)enterOpennessStepsView;
- (void)enterAgreeableStepsView;
- (void)enterMoneyStepsView;
- (void)enterAdmirationStepsView;
- (void)enterThreeStepsView;
- (void)enterPersonalityStepsView;
- (void)enterStoryStepsView;
- (void)enterVIPStoryStepsView:(BOOL)isMemberLogin;//vip additional story

- (void)enterIntimacyStepsView;
- (void)enterSMSVerifyStep;
- (void)enterNricVerifyStep;
- (void)enterNricVerifyStepWithValues:(NSMutableDictionary *)values;
- (void)enterVerifyStep;
- (void)procCheckUserStep;

- (void)enterDocCertStep;

- (void)enterUploadPhotoStep;
- (void)showUploadPhotoStep;//start quiz update

- (void)enterMainWindow;

//shows result after each step;
- (void)enterDisplayComplete:(int)step;

//////////////////////
- (void)initSetting;
- (void)checkUserStep;

@end

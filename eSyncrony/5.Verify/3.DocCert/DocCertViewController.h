
#import <UIKit/UIKit.h>
#import "DataSelectViewController.h"
#import "mainMenuViewController.h"

@class eSyncronyViewController;
@interface DocCertViewController : GAITrackedViewController <DataSelectViewDelegate,  UIImagePickerControllerDelegate, UINavigationControllerDelegate >
{
    IBOutlet UILabel*       lblName;
    
    IBOutlet UILabel*       lblWelcome;
    IBOutlet UIView*        viewSplitLine;
    IBOutlet UILabel*       lblNotice;
    
    IBOutlet UIView*        viewBottom;

    IBOutlet UILabel*       lblDocType;
    
    IBOutlet UIButton*      btnBack;
    IBOutlet UIButton*      btnSkip;
    
    UIImagePickerController*        _imagePickerController;

    NSMutableArray*     arrDocTypes;
    int                 nSelType;
    
    UIImage*    _photoImage;
    BOOL        _bAskMSG;
}

@property(nonatomic, strong) eSyncronyViewController* parentView;
@property (nonatomic, assign) mainMenuViewController    *mainMenuView;
@property(nonatomic, strong) NSDictionary* docCertInfo;

- (IBAction)onClickBack:(id)sender;

- (IBAction)onClickSkip:(id)sender;

- (IBAction)onClickDocType:(id)sender;

- (IBAction)onClickCapture:(id)sender;

- (IBAction)onClickAlubm:(id)sender;

@end

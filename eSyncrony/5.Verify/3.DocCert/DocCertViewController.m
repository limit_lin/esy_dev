
#import "Global.h"
#import "UtilComm.h"
#import "DSActivityView.h"
#import "eSyncronyAppDelegate.h"
#import "DocCertViewController.h"
#import "eSyncronyViewController.h"

@implementation DocCertViewController

@synthesize parentView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"DocCertVerifyView";
    if( self.parentView == nil )
    {
        if( self.docCertInfo == nil){
            [self p_showDocCert];
        }
    }
}
-(void)p_showDocCert
{
    //approve append
    //    NSDictionary* result = [UtilComm getDocCertVerifyStatus];
    //    NSDictionary* response = [result objectForKey:@"response"];
    //    if( result == nil || response == nil )
    //    {
    //        //[ErrorProc alertToCheckInternetStateTitle:@"Authentication Failure"];
    //        return;
    //    }
    
    arrDocTypes = [[NSMutableArray alloc] initWithCapacity:0];
    
    lblName.text = [NSString stringWithFormat:@"%@,",[eSyncronyAppDelegate sharedInstance].strName];
    NSLog(@"%@",lblName.text);
    
    nSelType = 0;
    
    [arrDocTypes addObject:NSLocalizedString(@"Education Certificate",@"DocCert")];
    [arrDocTypes addObject:NSLocalizedString(@"Employment Pass/Contract/Cert...",@"DocCert")];
    [arrDocTypes addObject:NSLocalizedString(@"Salary Slip/Payment Voucher",@"DocCert")];
    [arrDocTypes addObject:NSLocalizedString(@"ID/NRIC/FIN",@"DocCert")];
    
    nSelType = 0;
    lblDocType.text = [arrDocTypes objectAtIndex:nSelType];
    //    self.docCertInfo =[NSMutableDictionary dictionaryWithObject:arrDocTypes forKey:@"Arry"];
    return;
    //approve append end
}

- (void)recalcNoticeTextsPosition
{
    [lblWelcome sizeToFit];
    [lblNotice sizeToFit];
    
    CGRect rect1 = lblWelcome.frame;
    
    CGRect rect = viewSplitLine.frame;
    rect.origin.y = rect1.origin.y + rect1.size.height-3;
    viewSplitLine.frame = rect;
    
//    rect = lblNotice.frame;
//    rect.origin.y = rect1.origin.y + rect1.size.height + 6;
//    lblNotice.frame = rect;
//    
//    rect1 = rect;
//    rect = viewBottom.frame;
//    rect.origin.y = rect1.origin.y + rect1.size.height + 10;
//    viewBottom.frame = rect;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self recalcNoticeTextsPosition];
    
    NSLog(@"arrDocTypes====%@",arrDocTypes);

    if( self.parentView == nil )
    {
        btnBack.hidden = NO;
        btnSkip.hidden = YES;
    }
    else
    {
        btnBack.hidden = YES;
        btnSkip.hidden = NO;
    }
    
    if( arrDocTypes != nil )
        return;
    
    if( self.docCertInfo == nil )
        return;

    arrDocTypes = [[NSMutableArray alloc] initWithCapacity:0];
    
//    lblName.text =[NSString stringWithFormat:@"Dear %@,",[eSyncronyAppDelegate sharedInstance].strName];
    lblName.text =[NSString stringWithFormat:NSLocalizedString(@"Dear %@,",@"DocCert"),[eSyncronyAppDelegate sharedInstance].strName];
//    NSLog(@"%@",lblName.text);
    
    nSelType = 0;
    
   // NSLog(@"daolezheli %@\n",self.docCertInfo);
    
    NSString*   eduVerify = [self.docCertInfo objectForKey:@"EDU_VERIFY"];
    NSString*   idVerify = [self.docCertInfo objectForKey:@"ID_VERIFY"];
    NSString*   incomeVerify = [self.docCertInfo objectForKey:@"INCOME_VERIFY"];
    NSString*   workVerify = [self.docCertInfo objectForKey:@"WORK_VERIFY"];
    
    if( [eduVerify isEqualToString:@"0"] )
        [arrDocTypes addObject:NSLocalizedString(@"Education Certificate",@"DocCert")];
    
    if( [workVerify isEqualToString:@"0"] )
        [arrDocTypes addObject:NSLocalizedString(@"Employment Pass/Contract/Cert...",@"DocCert")];
    
    if( [incomeVerify isEqualToString:@"0"] )
        [arrDocTypes addObject:NSLocalizedString(@"Salary Slip/Payment Voucher",@"DocCert")];
    
    if( [idVerify isEqualToString:@"0"] )
        [arrDocTypes addObject:NSLocalizedString(@"ID/NRIC/FIN",@"DocCert")];
    
    nSelType = 0;
    lblDocType.text = [arrDocTypes objectAtIndex:nSelType];
    
}

- (void)DataSelectView:(DataSelectViewController *)dataSelectView didSelectItem:(int)itemIndex withTag:(int)tag
{
    nSelType = itemIndex;
    lblDocType.text = [arrDocTypes objectAtIndex:nSelType];

    [dataSelectView release];
}

- (IBAction)onClickDocType:(id)sender
{
    NSLog(@"arrDocTypes.count==%lu\n",(unsigned long)arrDocTypes.count);
    
    if( [arrDocTypes count] <= 1 )
        return;

    DataSelectViewController*   dataSelView = [DataSelectViewController createWithDataList:arrDocTypes selectIndex:nSelType withTag:0];
    
    dataSelView.delegate = self;
    [self.view addSubview:dataSelView.view];
    dataSelView.view.frame = self.view.bounds;
}

- (IBAction)onClickCapture:(id)sender
{
    if( _imagePickerController == nil )
    {
        _imagePickerController = [[UIImagePickerController alloc] init];
        _imagePickerController.delegate = self;
        _imagePickerController.allowsEditing = YES;
    }
    
    if( [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear] == FALSE )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Camera is not available",) withTitle:NSLocalizedString(@"Error",)];
        return;
    }
    
    //_imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    _imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;

    [self presentViewController:_imagePickerController animated:YES completion:nil];
}

- (IBAction)onClickAlubm:(id)sender {
    if( _imagePickerController == nil )
    {
        _imagePickerController = [[UIImagePickerController alloc] init];
        _imagePickerController.delegate = self;
#ifdef PHOTO_EDIT_ALLOW_MODE
        _imagePickerController.allowsEditing = YES;
#endif
    }
    
    _imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:_imagePickerController animated:YES completion:nil];
}

- (void)verifyDocProc
{
    NSString*   strDocType = lblDocType.text;
    int         nDocType = 1;
    
    if( [strDocType isEqualToString:NSLocalizedString(@"Education Certificate",@"DocCert")] )
        nDocType = 1;
    else if( [strDocType isEqualToString:NSLocalizedString(@"Employment Pass/Contract/Cert...",@"DocCert")] )
        nDocType = 2;
    else if( [strDocType isEqualToString:NSLocalizedString(@"Salary Slip/Payment Voucher",@"DocCert")] )
        nDocType = 3;
    else
        nDocType = 4;
    
    //--------------------------------------------------------------//
    NSDictionary*    result = [UtilComm uploadDocImage:_photoImage withType:nDocType];
    [_photoImage release];
    _photoImage = nil;
    [DSBezelActivityView removeViewAnimated:NO];

    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"Verify Document Failure"];
        return;
    }
    
    id photo = [result objectForKey:@"photo"];
    id details = [photo objectForKey:@"details"];
    id errormsg = [details objectForKey:@"errormsg"];
    if( [errormsg isKindOfClass:[NSString class]] )
    {
        if( [errormsg isEqualToString:@"'Your document has been successfully updated. '"] )
        {
            _bAskMSG = NO;
            
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"eSynchrony"
                                                         message:NSLocalizedString(@"Your document has been successfully updated",@"DocCert")
                                                        delegate:self
                                               cancelButtonTitle:NSLocalizedString(@"OK",)
                                               otherButtonTitles:nil];
            [av show];
            [av release];
        }
        else
            [ErrorProc alertMessage:errormsg withTitle:NSLocalizedString(@"Error",)];
    }
    else
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Error",)];
    }
}

- (void)moveNext
{
    [self.navigationController popViewControllerAnimated:NO];

    if( self.parentView != nil )
        [parentView enterMainWindow];
//        [parentView enterUploadPhotoStep];
//        [parentView enterStoryStepsView];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if( _bAskMSG == YES )
    {
        if( buttonIndex == 1 )
        {
            [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Uploading...",)];
            [self performSelector:@selector(verifyDocProc) withObject:nil afterDelay:0.01];
        }
        else
        {
            [_photoImage release];
            _photoImage = nil;
        }
    }
    else
    {
        [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
        [self performSelector:@selector(skipToNext) withObject:nil afterDelay:0.01];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker
        didFinishPickingImage:(UIImage *)image
                  editingInfo:(NSDictionary *)editingInfo
{
    [picker dismissViewControllerAnimated:NO completion:nil];
    
    _photoImage = [image retain];
    
    _bAskMSG = YES;

    UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Photo Upload",@"DocCert")
                                                 message:NSLocalizedString(@"Are you sure to upload?",@"DocCert")
                                                delegate:self
                                       cancelButtonTitle:NSLocalizedString(@"Cancel",)
                                       otherButtonTitles:NSLocalizedString(@"OK",), nil];
    [av show];
    [av release];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:NO completion:nil];
}

- (void)skipToNext
{
    [DSBezelActivityView removeViewAnimated:NO];
    [self moveNext];
}

- (IBAction)onClickBack:(id)sender
{
    
    [self.navigationController popViewControllerAnimated:YES];
    
   // [[eSyncronyAppDelegate sharedInstance].mainMenuViewCtrl procMyMatchApproved];


}
- (IBAction)onClickSkip:(id)sender
{
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(skipToNext) withObject:nil afterDelay:0.01];
}

- (void)dealloc
{
    [lblName release];
    
    [lblWelcome release];
    [viewSplitLine release];
    [lblNotice release];
    
    [lblDocType release];
    [_imagePickerController release];
    
    [viewBottom release];
    
    [btnBack release];
    [btnSkip release];
    
    [super dealloc];
}

@end

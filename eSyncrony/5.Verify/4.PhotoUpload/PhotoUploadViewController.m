
#import "Global.h"
#import "UtilComm.h"
#import "DSActivityView.h"
#import "eSyncronyAppDelegate.h"
#import "PhotoUploadViewController.h"
#import "eSyncronyViewController.h"

@implementation PhotoUploadViewController

@synthesize parentView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"PhotoUploadView";
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if( [eSyncronyAppDelegate sharedInstance].gender == 1 )//female
    {
        [imgViewPhoto setImage:[UIImage imageNamed:@"female1.png"]];
        labGender.text = NSLocalizedString(@"It is better to provide photos where you are well-dressed(e.g. during special events). This will increase the chances of approval as compared to those who are casually dressed by 3 times.", @"PhotoUploadView");
        
    }else//male
    {
        [imgViewPhoto setImage:[UIImage imageNamed:@"male1.png"]];
        labGender.text = NSLocalizedString(@"It is better to provide photos where you're nicely dressed. You may look into uploading photos that were taken at a formal event like a wedding reception. This will help to increase your chances of approval and have 3 times more success rate as compared to those who are dressed casually.", @"PhotoUploadView");
    }
}

- (IBAction)onClickCamera:(id)sender
{
    if( _imagePickerController == nil )
    {
        _imagePickerController = [[UIImagePickerController alloc] init];
        _imagePickerController.delegate = self;
#ifdef PHOTO_EDIT_ALLOW_MODE
        _imagePickerController.allowsEditing = YES;
#endif
    }
    
    if( [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear] == FALSE )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Camera is not available",) withTitle:NSLocalizedString(@"Error",)];
        return;
    }

    _imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:_imagePickerController animated:YES completion:nil];
}

- (IBAction)onClickAlbum:(id)sender
{
    if( _imagePickerController == nil )
    {
        _imagePickerController = [[UIImagePickerController alloc] init];
        _imagePickerController.delegate = self;
#ifdef PHOTO_EDIT_ALLOW_MODE
        _imagePickerController.allowsEditing = YES;
#endif
    }

    _imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:_imagePickerController animated:YES completion:nil];
}

- (void)verifyPhotoProc
{
    //--------------------------------------------------------------//
    NSDictionary*    result = [UtilComm uploadPrimaryPhoto:_photoImage];
    [_photoImage release];
    _photoImage = nil;
    [DSBezelActivityView removeViewAnimated:NO];

    if( result == nil )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"We are unable to process your request at this moment. Please try again later.",@"PhotoUpload") withTitle:NSLocalizedString(@"Sorry",)];
        return;
    }

    id photo = [result objectForKey:@"photo"];
    id details = [photo objectForKey:@"details"];
    id errormsg = [details objectForKey:@"errormsg"];
    if( [errormsg isKindOfClass:[NSString class]] )
    {
        if( [errormsg isEqualToString:@"'SUCCESS'"] )
        {
            //[self moveNext];
            _bAskMSG = NO;
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"eSynchrony"
                                                         message:NSLocalizedString(@"Photo uploaded successfully and it is pending for approval!",@"PhotoUpload")
                                                        delegate:self
                                               cancelButtonTitle:NSLocalizedString(@"OK",)
                                               otherButtonTitles:nil];
            [av show];
            [av release];
        }
        else
            [ErrorProc alertMessage:errormsg withTitle:NSLocalizedString(@"Error",)];
    }
    else
    {
        [ErrorProc alertMessage:NSLocalizedString(@"We are unable to process your request at this moment. Please try again later.",@"PhotoUpload") withTitle:NSLocalizedString(@"Sorry",)];
    }
}

- (void)moveNext
{
    [self.navigationController popViewControllerAnimated:NO];
//    [parentView enterMainWindow];
    [parentView enterStoryStepsView];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if( _bAskMSG == YES )
    {
        if( buttonIndex == 1 )
        {
            [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Uploading...",)];
            [self performSelector:@selector(verifyPhotoProc) withObject:nil afterDelay:0.01];
        }
        else
        {
            [_photoImage release];
            _photoImage = nil;
        }
    }
    else
    {
        [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
        [self performSelector:@selector(skipToNext) withObject:nil afterDelay:0.01];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker
        didFinishPickingImage:(UIImage *)image
                  editingInfo:(NSDictionary *)editingInfo
{
    [picker dismissViewControllerAnimated:NO completion:nil];
    
    _photoImage = [image retain];
    
    _bAskMSG = YES;
    
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Photo Upload",@"PhotoUpload")
                                                 message:NSLocalizedString(@"Are you sure to upload?",@"PhotoUpload")
                                                delegate:self
                                       cancelButtonTitle:NSLocalizedString(@"Cancel",)
                                       otherButtonTitles:NSLocalizedString(@"OK",), nil];
    [av show];
    [av release];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:NO completion:nil];
}

- (void)skipToNext
{
    [DSBezelActivityView removeViewAnimated:NO];
    [self moveNext];
}

- (IBAction)onClickSkip:(id)sender
{
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(skipToNext) withObject:nil afterDelay:0.01];
}

- (void)dealloc
{
    [imgViewPhoto release];
    [_imagePickerController release];
    
    [labGender release];
    [super dealloc];
}

@end

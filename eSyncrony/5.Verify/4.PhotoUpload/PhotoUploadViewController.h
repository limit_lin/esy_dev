
#import <UIKit/UIKit.h>

@class eSyncronyViewController;
@interface PhotoUploadViewController : GAITrackedViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate >
{
    IBOutlet UIImageView    *imgViewPhoto;    
    IBOutlet UILabel        *labGender;
    
    UIImagePickerController *_imagePickerController;
    UIImage                 *_photoImage;
    BOOL                    _bAskMSG;
}

@property(nonatomic, strong) eSyncronyViewController* parentView;

- (IBAction)onClickSkip:(id)sender;

- (IBAction)onClickCamera:(id)sender;
- (IBAction)onClickAlbum:(id)sender;

@end

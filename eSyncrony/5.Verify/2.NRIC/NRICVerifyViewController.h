
#import <UIKit/UIKit.h>
#import "DataSelectViewController.h"
#import "InputBaseViewController.h"

@class eSyncronyViewController;
@interface NRICVerifyViewController : InputBaseViewController <DataSelectViewDelegate>
{
    IBOutlet UILabel*           lblHeader;
    IBOutlet UILabel*           lblFooter;

    IBOutlet UITextField*       txtNumber;
    
    
    
    NSArray*    arrHeaderChars;
    NSArray*    arrFooterChars;
    
    int     nSelHeader, nSelFooter;
}
//@property (nonatomic, assign) int step;
@property (nonatomic, assign) int   countryId;
@property(nonatomic, strong) eSyncronyViewController* parentView;
@property (strong, nonatomic) IBOutlet UIButton*          btnBack;
@property (strong, nonatomic) IBOutlet UILabel *lblID;
@property (strong, nonatomic) IBOutlet UIImageView *imageText1;
@property (strong, nonatomic) IBOutlet UIImageView *imageSpinner1;
@property (strong, nonatomic) IBOutlet UIImageView *imageSpinner2;
@property (strong, nonatomic) IBOutlet UIButton *btnHeader;
@property (strong, nonatomic) IBOutlet UIButton *btnFooter;
@property (strong, nonatomic) IBOutlet UIImageView *imageText2;
@property (strong, nonatomic) IBOutlet UIImageView *imageText3;
@property (strong, nonatomic) IBOutlet UIImageView *imageText4;
@property (strong, nonatomic) IBOutlet UITextField *txtNumber1;
@property (strong, nonatomic) IBOutlet UITextField *txtNumber2;
@property (strong, nonatomic) IBOutlet UITextField *txtNumber3;
@property (strong, nonatomic) IBOutlet UILabel *lbl1;
@property (strong, nonatomic) IBOutlet UILabel *lbl2;

@property (strong, nonatomic) NSMutableDictionary *dicBaseInfo;

- (IBAction)onClickBack:(id)sender;

- (IBAction)onClickContinue:(id)sender;

- (IBAction)onClickHeader:(id)sender;

- (IBAction)onClickFooter:(id)sender;

@end

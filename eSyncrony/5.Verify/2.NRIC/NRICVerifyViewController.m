
#import "Global.h"
#import "UtilComm.h"
#import "DSActivityView.h"
#import "eSyncronyAppDelegate.h"
#import "NRICVerifyViewController.h"
#import "eSyncronyViewController.h"
//z687485A
@interface NRICVerifyViewController ()

@end

@implementation NRICVerifyViewController

@synthesize parentView,dicBaseInfo;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"NRICVerifyView";
    self.countryId = [eSyncronyAppDelegate sharedInstance].countryId;
    
//    if (self.step == 2) {
//        _btnBack.hidden = YES;
//    }else
//        _btnBack.hidden = NO;
    
    switch (self.countryId) {
        case 1:{
            self.lblID.text = NSLocalizedString(@"MyKad:",@"NRICVerify");
            txtNumber.hidden = YES;
            self.imageText1.hidden = YES;
            self.imageSpinner1.hidden = YES;
            self.imageSpinner2.hidden = YES;
            lblHeader.hidden = YES;
            lblFooter.hidden = YES;
            self.btnHeader.hidden = YES;
            self.btnFooter.hidden = YES;
            self.imageText2.hidden = NO;
            self.imageText3.hidden = NO;
            self.imageText4.hidden = NO;
            self.txtNumber1.hidden = NO;
            self.txtNumber2.hidden = NO;
            self.txtNumber3.hidden = NO;
            self.lbl1.hidden = NO;
            self.lbl2.hidden = NO;
            break;
        }
        case 2:{
            self.lblID.text = NSLocalizedString(@"HKID:",@"NRICVerify");
            arrHeaderChars = [[NSArray alloc] initWithObjects:@"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z", nil];
            arrFooterChars = [[NSArray alloc] initWithObjects:@"0", @"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"A", nil];
            break;
        }
        case 7:{//indonesia
           self.lblID.text = NSLocalizedString(@"IndonesiaID:",@"NRICVerify");
            txtNumber.hidden = NO;
            self.imageText1.hidden = NO;
            self.imageSpinner1.hidden = YES;
            self.imageSpinner2.hidden = YES;
            lblHeader.hidden = YES;
            lblFooter.hidden = YES;
            self.btnHeader.hidden = YES;
            self.btnFooter.hidden = YES;
            self.imageText2.hidden = YES;
            self.imageText3.hidden = YES;
            self.imageText4.hidden = YES;
            self.txtNumber1.hidden = YES;
            self.txtNumber2.hidden = YES;
            self.txtNumber3.hidden = YES;
            self.lbl1.hidden = YES;
            self.lbl2.hidden = YES;
            break;
        }
        case 202:{//thailand 0825
            self.lblID.text = NSLocalizedString(@"ThailandID:",@"NRICVerify");
            txtNumber.hidden = NO;
            self.imageText1.hidden = NO;
            self.imageSpinner1.hidden = YES;
            self.imageSpinner2.hidden = YES;
            lblHeader.hidden = YES;
            lblFooter.hidden = YES;
            self.btnHeader.hidden = YES;
            self.btnFooter.hidden = YES;
            self.imageText2.hidden = YES;
            self.imageText3.hidden = YES;
            self.imageText4.hidden = YES;
            self.txtNumber1.hidden = YES;
            self.txtNumber2.hidden = YES;
            self.txtNumber3.hidden = YES;
            self.lbl1.hidden = YES;
            self.lbl2.hidden = YES;
            break;
            
        }
        default:{
            arrHeaderChars = [[NSArray alloc] initWithObjects:@"S", @"T", @"F", @"G", nil];
            arrFooterChars = [[NSArray alloc] initWithObjects:@"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z", nil];
            break;
        }
            
    }
    
    nSelHeader = 0;
    nSelFooter = 0;

    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnTableView:)];
    tapRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapRecognizer];
}

- (void)tapOnTableView:(UITapGestureRecognizer *)gesture
{
    [self.view endEditing:YES];
    [self restoreViewPosition];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGRect rect = [textField convertRect:textField.bounds toView:self.view];
    
    float   offset;
    
    offset = self.view.frame.size.height - (rect.size.height + 240);
    offset = offset - rect.origin.y;
    
    if( offset > 0 )
        return;
    
    rect = self.view.frame;
    rect.origin.y = offset;
    
    self.view.frame = rect;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    [self restoreViewPosition];

    return YES;
}

- (IBAction)onClickBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)nricVerifyProc
{
    //NSString *accNo = [eSyncronyAppDelegate sharedInstance].strAccNo;
   // NSString *tokenKey = [eSyncronyAppDelegate sharedInstance].strTokenKey;
    
    
    NSString *nricp = nil;
    NSString *nrics = nil;
    NSString *nric = nil;
    if (self.countryId == 1) {
        nricp = self.txtNumber1.text;
        nrics = self.txtNumber2.text;
        nric = self.txtNumber3.text;
    }
    else if(self.countryId == 7) {
       
        nricp=nil;
        nrics=nil;
        nric = txtNumber.text;
    }
    else if(self.countryId == 202) {//0825
        
        nricp=nil;
        nrics=nil;
        nric = txtNumber.text;
    }else{
        nricp = lblHeader.text;
        nric = txtNumber.text;
        nrics = lblFooter.text;
    }
    
    NSString *strTotal = [NSString stringWithFormat:@"%@%@%@", nricp, nric, nrics];
    [eSyncronyAppDelegate sharedInstance].strNric = strTotal;
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    
    if(self.countryId == 7) {
        
        [params setObject:nric forKey:@"nric"];
        
    }else if (self.countryId == 202)//0825
    {
        [params setObject:nric forKey:@"nric"];
    }
    else
    {
        [params setObject:nricp forKey:@"nricp"];
        [params setObject:nric forKey:@"nric"];
        [params setObject:nrics forKey:@"nrics"];
    }

    NSDictionary*    result = [UtilComm nricVerifyNew:params];
    
    [DSBezelActivityView removeViewAnimated:NO];

    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"NRIC Verify Failure"];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( response == nil )
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Saving Failure",)];
        return;
    }    
    if( [response isEqualToString:@"OK"] )
    {
        NSString *strTotal = [NSString stringWithFormat:@"%@%@%@", nricp, nric, nrics];
        
        [eSyncronyAppDelegate sharedInstance].strNric = strTotal;
        [[eSyncronyAppDelegate sharedInstance] saveLoginInfo];

        [self pNric_moveNextWindow];
        
    }else{
        if ([response isKindOfClass:[NSString class]] && [response isEqualToString:@"NRIC Already Exist"]) {
            [ErrorProc alertMessage:NSLocalizedString(@"NRIC Already Exist",@"NRICVerify") withTitle:NSLocalizedString(@"NRIC Validation",@"NRICVerify")];
            [DSBezelActivityView removeViewAnimated:NO];
        }
        else if ([response isKindOfClass:[NSString class]] && [response isEqualToString:@"Please enter numeric values only."]) {
            [ErrorProc alertMessage:NSLocalizedString(@"Please enter numeric values only.",@"NRICVerify") withTitle:NSLocalizedString(@"NRIC Validation",@"NRICVerify")];
            [DSBezelActivityView removeViewAnimated:NO];
        }
        else if ([response isKindOfClass:[NSString class]] && [response isEqualToString:@"NRIC too short"]) {
            [ErrorProc alertMessage:NSLocalizedString(@"NRIC too short",@"NRICVerify") withTitle:NSLocalizedString(@"NRIC Validation",@"NRICVerify")];
            [DSBezelActivityView removeViewAnimated:NO];
        }
        else if ([response isKindOfClass:[NSString class]] && [response isEqualToString:@"NRIC to short"]) {
            [ErrorProc alertMessage:NSLocalizedString(@"NRIC too short",@"NRICVerify") withTitle:NSLocalizedString(@"NRIC Validation",@"NRICVerify")];
            [DSBezelActivityView removeViewAnimated:NO];
        }
        else if([response isKindOfClass:[NSString class]] && [response isEqualToString:@"ERROR! Must be a value from A to Z"]) {
            [ErrorProc alertMessage:NSLocalizedString(@"ERROR! Must be a value from A to Z",@"NRICVerify") withTitle:NSLocalizedString(@"NRIC Validation",@"NRICVerify")];
            [DSBezelActivityView removeViewAnimated:NO];
        }
        else if([response isKindOfClass:[NSString class]] && [response isEqualToString:@"Please enter numeric values only."]) {
            [ErrorProc alertMessage:NSLocalizedString(@"Please enter numeric values only.",@"NRICVerify") withTitle:NSLocalizedString(@"NRIC Validation",@"NRICVerify")];
            [DSBezelActivityView removeViewAnimated:NO];
        }
        else if([response isKindOfClass:[NSString class]] && [response isEqualToString:@"Invalid NRIC"]) {
            [ErrorProc alertMessage:NSLocalizedString(@"Invalid NRIC",@"NRICVerify") withTitle:NSLocalizedString(@"NRIC Validation",@"NRICVerify")];
            [DSBezelActivityView removeViewAnimated:NO];
        }
        else if([response isKindOfClass:[NSString class]] && [response isEqualToString:@"wrong cty_id"]) {
            [ErrorProc alertMessage:NSLocalizedString(@"wrong cty_id",@"NRICVerify") withTitle:NSLocalizedString(@"NRIC Validation",@"NRICVerify")];
            [DSBezelActivityView removeViewAnimated:NO];
        }
        else
        {
            [ErrorProc alertSetLifeStyleError:response];
            [DSBezelActivityView removeViewAnimated:NO];
        }

    }
}

-(void)pNric_moveNextWindow{
    
    [parentView showUploadPhotoStep];
}

- (IBAction)onClickContinue:(id)sender
{
    if (self.countryId == 1) {
        if ([self.txtNumber1.text isEqualToString:@""]||[self.txtNumber2.text isEqualToString:@""]||[self.txtNumber3.text isEqualToString:@""]) {
            [ErrorProc alertMessage:NSLocalizedString(@"ID Number is required.",@"NRICVerify") withTitle:NSLocalizedString(@"Field Required",)];
        }else{
            [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
            [self performSelector:@selector(nricVerifyProc) withObject:nil afterDelay:0.01];
        }
    }
   else if (self.countryId == 7) {//append
        if ([txtNumber.text isEqualToString:@""]) {
            [ErrorProc alertMessage:NSLocalizedString(@"ID Number is required.",@"NRICVerify") withTitle:NSLocalizedString(@"Field Required",)];
        }else{
            [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
            [self performSelector:@selector(nricVerifyProc) withObject:nil afterDelay:0.01];
        }
   }else if (self.countryId == 202) {//append 0825
       
       if ([txtNumber.text isEqualToString:@""]) {
           
           [ErrorProc alertMessage:NSLocalizedString(@"ID Number is required.",@"NRICVerify") withTitle:NSLocalizedString(@"Field Required",)];
       }else{
           [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
           [self performSelector:@selector(nricVerifyProc) withObject:nil afterDelay:0.01];
       }
   }
   else {
        if( [txtNumber.text isEqualToString:@""] )
        {
            [ErrorProc alertMessage:NSLocalizedString(@"Nric is required.",@"NRICVerify") withTitle:NSLocalizedString(@"Field Required",)];
        }else{
            [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
            [self performSelector:@selector(nricVerifyProc) withObject:nil afterDelay:0.01];
        }
    }
    
}

- (void)DataSelectView:(DataSelectViewController *)dataSelectView didSelectItem:(int)itemIndex withTag:(int)tag
{
    switch (self.countryId) {
        case 1:{
            break;
        }
        case 7://append
        {
            break;
        }
        case 202://append 0825
        {
            break;
        }
        default:{
            if( tag == 0 )
            {
                nSelHeader = itemIndex;
                lblHeader.text = [arrHeaderChars objectAtIndex:nSelHeader];
            }
            else
            {
                nSelFooter = itemIndex;
                lblFooter.text = [arrFooterChars objectAtIndex:nSelFooter];
            }
            break;
        }
    }
    

    [dataSelectView release];
}

- (IBAction)onClickHeader:(id)sender
{
    [self.view endEditing:YES];
    
    DataSelectViewController*   dataSelView = [DataSelectViewController createWithDataList:arrHeaderChars selectIndex:nSelHeader withTag:0];
    
    dataSelView.delegate = self;
    [self.view addSubview:dataSelView.view];
    dataSelView.view.frame = self.view.bounds;
}

- (IBAction)onClickFooter:(id)sender
{
    [self.view endEditing:YES];
    
    DataSelectViewController*   dataSelView = [DataSelectViewController createWithDataList:arrFooterChars selectIndex:nSelFooter withTag:1];
    
    dataSelView.delegate = self;
    [self.view addSubview:dataSelView.view];
    dataSelView.view.frame = self.view.bounds;
}

- (void)dealloc
{
    [arrHeaderChars release];
    [arrFooterChars release];
    
    [lblHeader release];
    [lblFooter release];

    [txtNumber release];
    [_btnBack release];
    
    [_lblID release];
    [_imageText1 release];
    [_imageSpinner1 release];
    [_imageSpinner2 release];
    [_btnHeader release];
    [_btnFooter release];
    [_imageText2 release];
    [_imageText3 release];
    [_imageText4 release];
    [_txtNumber1 release];
    [_txtNumber2 release];
    [_txtNumber3 release];
    [_lbl1 release];
    [_lbl2 release];
    [super dealloc];
}

@end


#import "Global.h"
#import "UtilComm.h"
#import "DSActivityView.h"
#import "eSyncronyAppDelegate.h"
#import "SMSVerifyViewController.h"
#import "eSyncronyViewController.h"

#import "CoreTelephony/CTCarrier.h"
#import "CoreTelephony/CTTelephonyNetworkInfo.h"
@interface SMSVerifyViewController ()

@end

@implementation SMSVerifyViewController

@synthesize parentView;
@synthesize step;

- (void)viewDidLoad
{
    [super viewDidLoad];//151125
    
    self.screenName = @"SMSVerifyView";
    
    txtSmsCode.delegate = self;
    
    btnBack.hidden = YES;
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnTableView:)];
    tapRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapRecognizer];
    //self.step = 1;
    [self setContentsToStep];
    //[scrlView addSubview:subViewStep1];
    
    if( self.step == 2 )
    {
#ifdef DEMO_VERSION
//        txtSmsCode.text = self.vcode;
#endif
    }
}

- (void)tapOnTableView:(UITapGestureRecognizer *)gesture
{
    [self.view endEditing:YES];
    [self restoreViewPosition];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if(scrlView.bounds.size.height>subViewStep1.frame.size.height){
        subViewStep1.frame = scrlView.bounds;
    }
    if(scrlView.bounds.size.height>subViewStep2.frame.size.height){
        subViewStep2.frame = scrlView.bounds;
    }else{
        scrlView.contentSize = subViewStep2 .frame.size;
    }

}
//- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
//    
//    CGFloat offset = self.view.frame.size.height - (textField.frame.origin.y + textField.frame.size.height + 216 +50);
//    if (offset <= 0) {
//        [UIView animateWithDuration:0.3 animations:^{
//            CGRect frame = self.view.frame;
//            frame.origin.y  = offset;
//            self.view.frame = frame;
//        }];
//    }
//    return YES;
//}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGRect rect = [textField convertRect:textField.bounds toView:self.view];
    
    float   offset;
    
    offset = self.view.frame.size.height - (rect.size.height + 216 + 50);
    offset = offset - rect.origin.y;
    
    if( offset > 0 )
        return;
    
    rect = self.view.frame;
    rect.origin.y = offset;
    
    self.view.frame = rect;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    [self restoreViewPosition];

    return YES;
}

- (void)setContentsToStep
{
     NSLog(@"setContentsToStep==%d",step);
    if( step == 1 )
    {
        prevView = subViewStep1;
    }
    else if( step == 2 )
    {
        prevView = subViewStep2;
        [self startTime];
    }
    
    btnBack.hidden = (step==1);
    
    [scrlView addSubview:prevView];
    
}

- (IBAction)onClickBack:(id)sender
{
    if( self.step == 1 )
        return;

    [self.navigationController popViewControllerAnimated:YES];
}
-(void)startTime{
    __block int timeout=10; //倒计时时间
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer, ^{
        if(timeout<=0){ //倒计时结束，关闭
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                [btnResend setTitle:NSLocalizedString(@"RESEND",@"SMSVerify") forState:UIControlStateNormal];
                btnResend.userInteractionEnabled = YES;
            });
        }else{
            //            int minutes = timeout / 60;
            int seconds = timeout % 60;
            NSString *strTime = [NSString stringWithFormat:@"%.2d", seconds];
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
//                NSLog(@"____%@",strTime);
                [btnResend setTitle:[NSString stringWithFormat:NSLocalizedString(@"RESEND(%@)",@"SMSVerify"),strTime] forState:UIControlStateNormal];
                btnResend.userInteractionEnabled = NO;
                
            });
            timeout--;
            
        }
    });
    dispatch_resume(_timer);
    
}
- (void)smsVerifyProc
{
    
    //--------------------------------------------------------------//
    NSString*   cty_id = [NSString stringWithFormat:@"%d", [eSyncronyAppDelegate sharedInstance].countryId+1];
    
    NSLog(@"Verify  cty_id======%@",cty_id);
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    //[params setObject:[eSyncronyAppDelegate sharedInstance].strHandphone forKey:@"handphone"];

    [params setObject:self.phoneNumber forKey:@"handphone"];
    [params setObject:cty_id forKey:@"cty_id"];
    
    NSDictionary*    result = [UtilComm smsVerify:params];
    [DSBezelActivityView removeViewAnimated:NO];

    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"SMS Verify Failture"];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    
    if( [response isKindOfClass:[NSDictionary class]] && [response objectForKey:@"vcode"] != nil )
    {
        self.vcode = [response objectForKey:@"vcode"];
        if (step==1) {
            self.nextView = [[[SMSVerifyViewController alloc] initWithNibName:@"SMSVerifyViewController" bundle:nil] autorelease];
            
            self.nextView.step = 2;
            self.nextView.vcode = self.vcode;
            self.nextView.phoneNumber = self.phoneNumber;
            self.nextView.parentView = self.parentView;
            
            [self.navigationController pushViewController:self.nextView animated:YES];
        }else{

            [self startTime];
        }
    }
    else if( [response isKindOfClass:[NSString class]] )
    {
        if( [response hasPrefix:@"ERROR"] )
            [ErrorProc alertDefaultError:response withTitle:NSLocalizedString(@"SMS Verify Failture",@"SMSVerify")];
        else
            [ErrorProc alertMessage:response withTitle:NSLocalizedString(@"eSynchrony",)];
    }
    else
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Error",)];
    }
}

- (IBAction)onClickVerify:(id)sender
{
    self.phoneNumber = txtPhoneNumber.text;
    int countryId = [eSyncronyAppDelegate sharedInstance].countryId;
    NSString *countryname;
    NSString *MOBILE;
    //0: Singapore, 1: Malaysia, 2: HONG KONG
    
    NSLog(@"Verify  countryId=====%d",countryId);
    
    switch (countryId) {
        case 0:{//SG
            MOBILE = @"^([89])\\d{7}$";
            countryname = @"Singapore";
            break;
        }
        case 1:{//MY
            MOBILE = @"^(01)\\d{8,9}$";
            countryname = @"Malaysia";
            break;
        }
        case 7://append indonesia
        {
            MOBILE = @"^(08)\\d{7,10}$";//11-->9~12
            countryname = @"Indonesia";
            break;
        }
        case 202:{//TH
            MOBILE = @"^([0])\\d{9}$";
            countryname = @"Thailand";
            break;
        }
        default:{//HK
            MOBILE = @"^([5689])\\d{7}$";
            countryname = @"HONG KONG";
            break;
        }
    }
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    if (![regextestmobile evaluateWithObject:self.phoneNumber]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Info",) message:[NSString stringWithFormat:NSLocalizedString(@"Please fill in the phone number with the correct format for %@ !",@"SMSVerify"),countryname] delegate:self cancelButtonTitle:NSLocalizedString(@"OK",) otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        return;
    }
    
//    [self performSelector:@selector(smsCreatAccount) withObject:nil afterDelay:0.01];
    [self performSelector:@selector(sendSMSbySelf) withObject:nil afterDelay:0.01];
}
-(void)sendSMSbySelf{
    
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(smsVerifyProc) withObject:nil afterDelay:0.01];
    return;
    //舍弃本机发短信
    MFMessageComposeViewController *picker = [[[MFMessageComposeViewController alloc] init]autorelease];
    BOOL canSendSMS = [MFMessageComposeViewController canSendText];
    CTTelephonyNetworkInfo *info = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *carrier = [info subscriberCellularProvider];
    NSString *code = [carrier mobileNetworkCode];
    
    NSLog(@"can send SMS [%d]\nmobileNetworkCode:%@",canSendSMS,code);
    
    if (canSendSMS&&code.length>0) {
        [info release];
        [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    
        NSString*   cty_id = [NSString stringWithFormat:@"%d", [eSyncronyAppDelegate sharedInstance].countryId+1];
        
        NSLog(@"cty_idcty_id=%@",cty_id);
        
        NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
        
        [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
        [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
        [params setObject:self.phoneNumber forKey:@"handphone"];
        [params setObject:cty_id forKey:@"cty_id"];
        [params setObject:@"no" forKey:@"sendServerSMS"];
        
        NSDictionary*    result = [UtilComm smsVerify:params];
        [DSBezelActivityView removeViewAnimated:NO];
        
        if( result == nil )
        {
            //[ErrorProc alertToCheckInternetStateTitle:@"SMS Verify Failture"];
            return;
        }
        
        id response = [result objectForKey:@"response"];
        
        if( [response isKindOfClass:[NSDictionary class]] && [response objectForKey:@"vcode"] != nil )
        {
            self.vcode = [response objectForKey:@"vcode"];
            picker.messageComposeDelegate = self;
            
            picker.navigationBar.tintColor = [UIColor blackColor];
            
            picker.body = [NSString stringWithFormat:NSLocalizedString(@"Your eSynchrony.com Activation code:%@",@"SMSVerify"),self.vcode];
            
            picker.recipients = [NSArray arrayWithObject:self.phoneNumber];
            
            [self presentViewController:picker animated:YES completion:nil];
            
           
        }
        else if( [response isKindOfClass:[NSString class]] )
        {
            if( [response hasPrefix:@"ERROR"] )
                [ErrorProc alertDefaultError:response withTitle:NSLocalizedString(@"SMS Verify Failture",@"SMSVerify")];
            else
               [ErrorProc alertMessage:response withTitle:NSLocalizedString(@"eSynchrony",)];
        }
        else
        {
            [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Error",)];
        }
        
    }else{
        NSLog(@"本机无法发送，改由服务器发送短信");
        [info release];
        [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
        [self performSelector:@selector(smsVerifyProc) withObject:nil afterDelay:0.01];
    }
    
}
//append for saving mobile No
-(void)smsCreatAccount
{
    //    NSString*   accNo = [eSyncronyAppDelegate sharedInstance].strAccNo;
    //    NSString*   tokenKey = [eSyncronyAppDelegate sharedInstance].strTokenKey;
    
    //--------------------------------------------------------------//
    NSString*   cty_id = [NSString stringWithFormat:@"%d", [eSyncronyAppDelegate sharedInstance].countryId+1];
    NSLog(@"append cty_idcty_id=%@",cty_id);
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
    [params setObject:cty_id forKey:@"cty_id"];
    
    [params setObject:self.phoneNumber forKey:@"handphone"];
   
    //[params setObject:txtSmsCode.text forKey:@"smscode"];
    
    NSDictionary*    result = [UtilComm smsVerify:params];
    [DSBezelActivityView removeViewAnimated:NO];
    
    if( result == nil )
    {
        [ErrorProc alertToCheckInternetStateTitle:@"SMS Verify repeat"];
        return;
    }
    
    id response = [result objectForKey:@"response"];
    if( [response isKindOfClass:[NSString class]] )
    {
    /*   
     if( [response isEqualToString:@"true"] )
        {
            //[self moveNext];
        }
        else if( [response hasPrefix:@"ERROR"] )
            [ErrorProc alertDefaultError:response withTitle:NSLocalizedString(@"SMS Verify Code Failture",@"SMSVerify")];
        else
            [ErrorProc alertMessage:NSLocalizedString(@"The activation code is not verified, please try again.",@"SMSVerify") withTitle:NSLocalizedString(@"Verification",@"SMSVerify")];
     */
    }
    else
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Error",)];
    }
}
- (void)smsVerifyCodeProc
{
//    NSString*   accNo = [eSyncronyAppDelegate sharedInstance].strAccNo;
//    NSString*   tokenKey = [eSyncronyAppDelegate sharedInstance].strTokenKey;
//    NSString*   cty_id = [NSString stringWithFormat:@"%d", [eSyncronyAppDelegate sharedInstance].countryId+1];

    //--------------------------------------------------------------//
    
    NSMutableDictionary*    params = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [params setObject:[eSyncronyAppDelegate sharedInstance].strAccNo forKey:@"acc_no"];
    [params setObject:[eSyncronyAppDelegate sharedInstance].strTokenKey forKey:@"token_key"];
//    [params setObject:cty_id forKey:@"city_id"];//append 151208
    [params setObject:self.phoneNumber forKey:@"handphone"];
    [params setObject:txtSmsCode.text forKey:@"smscode"];

    NSDictionary*    result = [UtilComm smsVerifyCode:params];
    [DSBezelActivityView removeViewAnimated:NO];
    
    if( result == nil )
    {
        //[ErrorProc alertToCheckInternetStateTitle:@"SMS Verify Code Failture"];
        return;
    }

    id response = [result objectForKey:@"response"];
    if( [response isKindOfClass:[NSString class]] )
    {
        if( [response isEqualToString:@"true"] )
        {
            [self moveNext];
        }
        else if( [response hasPrefix:@"ERROR"] )
            [ErrorProc alertDefaultError:response withTitle:NSLocalizedString(@"SMS Verify Code Failture",@"SMSVerify")];
        else
            [ErrorProc alertMessage:NSLocalizedString(@"The activation code is not verified, please try again.",@"SMSVerify") withTitle:NSLocalizedString(@"Verification",@"SMSVerify")];
    }
    else
    {
        [ErrorProc alertMessage:NSLocalizedString(@"Unknown Error",) withTitle:NSLocalizedString(@"Error",)];
    }
}

- (void)moveNext
{
  //  [self.navigationController popToRootViewControllerAnimated:NO];
    //[parentView enterNricVerifyStep];
    [parentView enterWelcomeView];
}

- (IBAction)onClickVerifyCode:(id)sender
{
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"Loading...",)];
    [self performSelector:@selector(smsVerifyCodeProc) withObject:nil afterDelay:0.01];
    
}

- (IBAction)onClickResend:(id)sender {
    
    [self sendSMSbySelf];
}
//MFMessageComposeViewControllerDelegate
-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
   
    //Notifies users about errors associated with the interface
    NSString *error = nil;
    switch (result) {
            
        case MessageComposeResultCancelled:
            error = NSLocalizedString(@"Message cancelled",@"SMSVerify");
            break;
            
        case MessageComposeResultSent:
            break;
        
            
        case MessageComposeResultFailed:
            error = NSLocalizedString(@"Message failed",@"SMSVerify");
            break;
            
        default:
            break;
            
    }
    if (error!=nil) {
        NSLog(@"本机发送短信失败，改由服务器发送短信");
//        [self.navigationController popToViewController:self animated:YES];
        [self performSelector:@selector(smsVerifyProc) withObject:nil afterDelay:0.01];
    }else{
        [controller dismissViewControllerAnimated:YES completion:nil];
        self.nextView = [[[SMSVerifyViewController alloc] initWithNibName:@"SMSVerifyViewController" bundle:nil] autorelease];
        
        self.nextView.step = 2;
        self.nextView.vcode = self.vcode;
        self.nextView.phoneNumber = self.phoneNumber;
        self.nextView.parentView = self.parentView;
        
        [self.navigationController pushViewController:self.nextView animated:YES];
    }
}
- (void)dealloc
{
    [scrlView release];
    [subViewStep1 release];
    [subViewStep2 release];
    [txtPhoneNumber release];
    [txtSmsCode release];
    
    [btnBack release];
    
    //[self.phoneNumber release];
    //[self.vcode release];

    [btnResend release];
    [super dealloc];
}

@end

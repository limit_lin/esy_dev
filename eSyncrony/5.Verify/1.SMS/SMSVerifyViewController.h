
#import <UIKit/UIKit.h>
#import "InputBaseViewController.h"
#import"WelcomeViewController.h"

#import <MessageUI/MFMessageComposeViewController.h>
@class eSyncronyViewController;
@interface SMSVerifyViewController : InputBaseViewController<MFMessageComposeViewControllerDelegate,UITextFieldDelegate>
{
    IBOutlet UIScrollView*      scrlView;
    IBOutlet UIView*            subViewStep1;
    IBOutlet UIView*            subViewStep2;
    
    IBOutlet UITextField*       txtPhoneNumber;
    IBOutlet UITextField*       txtSmsCode;
    
    IBOutlet UIButton          *btnResend;
    IBOutlet UIButton*          btnBack;
    
    UIView*     prevView;
}

@property (nonatomic, assign) int step;
@property (nonatomic, assign) SMSVerifyViewController* nextView;
@property (nonatomic, assign) eSyncronyViewController* parentView;

@property (nonatomic, strong) NSString*   phoneNumber;
@property (nonatomic, strong) NSString*   vcode;
@property (nonatomic, strong) NSMutableDictionary *dicRegisterInfo;

-(void)startTime;

- (IBAction)onClickBack:(id)sender;

- (IBAction)onClickVerify:(id)sender;

- (IBAction)onClickVerifyCode:(id)sender;

- (IBAction)onClickResend:(id)sender;

@end


#import <UIKit/UIKit.h>

#import "eSyncronyAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([eSyncronyAppDelegate class]));
    }
}
